﻿using System;

namespace PFR_INVEST.LoadTesting
{
    public class FinishedPercentChangedEventArgs : EventArgs
    {
        public double Percent;
        public FinishedPercentChangedEventArgs(double _percent)
        {
            Percent = _percent;
        }
    }

    public class StepChangedEventArgs : EventArgs
    {
        public string Step;
        public StepChangedEventArgs(string _step)
        {
            Step = _step;
        }
    }

    public class ErrorCountChangedEventArgs : EventArgs
    {
        public int ErrorCount;
        public int ID;
        public ErrorCountChangedEventArgs(int _errorCount, int id)
        {
            ErrorCount = _errorCount;
            ID = id;
        }
    }

    public class ComplexScenarioErrorEventArgs : EventArgs
    {
        public string Text;
        public ComplexScenarioErrorEventArgs(string text)
        {
            Text = text;
        }
    }

    public class ExceptionOccuredEventArgs : EventArgs
    {
        public Exception Exception;
        public ExceptionOccuredEventArgs(Exception ex)
        {
            Exception = ex;
        }
    }

    public class MeasureArgs : EventArgs
    {
        public string Text;
        public double Value;
        public MeasureArgs(string text, double value)
        {
            Text = text;
            Value = value;
        }
    }

    public interface ILoadScenario
    {
        event EventHandler FinishedPercentChanged;
        event EventHandler StepChanged;
        event EventHandler ErrorCountChanged;
        event EventHandler<ExceptionOccuredEventArgs> ExceptionOccured;
        event EventHandler ScenarioFinished;
        event EventHandler ComplexScenarioError;
        event EventHandler MeasureInvoked;

        void RunScenario();
        string ScenarioReport { get; }
        string ScenarioName { get; }
        int InstanceID { get; set; }
        int LatencyDelay { get; set; }
        bool IsComplexScenario { get; set; }
        bool IsLatencyDelayEnabled { get; set; }
        int CycleCount { get; set; }
        DateTime? LaunchTime { get; set; }
    }
}
