﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.LoadTesting.Scenarios
{
	public class AddReturnScenario : ScenarioBase
	{
		public override string ScenarioName
		{
			get { return "Бэк-офис - Расходы по СЧ ИЛС - Добавить возврат"; }
		}

		public override void Run()
		{
			base.Login();
			var rnd = new Random();

			base.RaiseStepChanged("Загрузка модели списка");
			var modelList = new SchilsCostsListViewModel();

			var item = modelList.List.Where(n => n.Operation2Type ==
					  SchilsCostsListItem.SchilsCostsListItemOpertion2Type.Direction).ToList().RandomItem(rnd);

			base.RaiseStepChanged(0.2, "Загрузка модели формы");
			var model = new ReturnViewModel() { State = ViewModelState.Create };
			model.LoadBudget(item.DirectionID);

			base.RaiseStepChanged(0.5, "Заполнение формы");
			model.Account = (new PortfoliosListViewModel()).PortfolioList.RandomItem(rnd);
			model.Number = "Тестовой " + rnd.Next(1000);
			model.SelectedRecipient = model.RecipientsList.RandomItem(rnd);
			model.Sum = 0.01M * rnd.Next(10000);
			model.Comment = "Тестовой " + rnd.Next(1000);

			base.RaiseStepChanged(0.7, "Сохранение формы");
			if (model.CanExecuteSave())
				model.SaveCard.Execute(null);
			else
				throw new Exception("Ошибка при проверке параметров для сохранения!");

			base.RaiseFinishedPercentChanged(1);
		}

	}
}
