﻿using System;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class AddEnrollmentPercentsScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Бэк-офис - Счета - Добавить зачисление процентов по счетам"; }
        }

        public override void Run()
        {
            base.Login();
            var rnd = new Random();

			//base.RaiseStepChanged("Загрузка модели списка СПН");
			//var modelList = new ACCListViewModel();

            base.RaiseStepChanged(0.2, "Загрузка модели формы");
            var model = new EnrollmentPercentsViewModel() { State = ViewModelState.Create };

            base.RaiseStepChanged(0.5, "Заполнение формы");
            model.Account = (new PortfoliosListViewModel()).PortfolioList.RandomItem(rnd);
            //model.SelectedCurrencyID = model.CurrencyList.RandomItem(rnd).ID;
            model.Summ = 0.01M * rnd.Next(10000);

            base.RaiseStepChanged(0.7, "Сохранение формы");
            if (model.CanExecuteSave())
                model.SaveCard.Execute(null);
            else
                throw new Exception("Ошибка при проверке параметров для сохранения!");

            base.RaiseFinishedPercentChanged(1);
        }
    }
}
