﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.LoadTesting.Scenarios
{
	public class SchilsCostsListScenario : ScenarioBase
	{
		public override string ScenarioName
		{
			get { return "Бэк-офис - Расходы по СЧ ИЛС - Расходы на ведение СЧ ИЛС"; }
		}

		public override void Run()
		{
			base.Login();
			var rnd = new Random();

			base.RaiseStepChanged("Загрузка модели списка");
			var modelList = new SchilsCostsListViewModel();


			var count = rnd.Next(5) + 5;
			base.RaiseFinishedPercentChanged(0.2);
			for (int i = 0; i < count; i++)
			{
				var list = modelList.List.Where(n => n.Operation2Type ==
					  SchilsCostsListItem.SchilsCostsListItemOpertion2Type.BudgetCreation).ToList();
				var item = list.RandomItem(rnd);
				base.RaiseStepChanged("Карточка Бюджет " + (i + 1).ToString());
				var recordID = item.BudgetID;
				var card = new BudgetViewModel() { State = ViewModelState.Edit };
				card.State = ViewModelState.Edit;
				card.Load(recordID);
			}

			count = rnd.Next(5) + 5;
			base.RaiseFinishedPercentChanged(0.3);
			for (int i = 0; i < count; i++)
			{
				var list = modelList.List.Where(n => n.Operation2Type ==
					  SchilsCostsListItem.SchilsCostsListItemOpertion2Type.BudgetCorrection).ToList();
				var item = list.RandomItem(rnd);
				base.RaiseStepChanged("Карточка Уточнение бюджета " + (i + 1).ToString());
				var recordID = item.ID;
				var card = new BudgetCorrectionViewModel() { State = ViewModelState.Edit };
				card.Load(recordID);
			}

			count = rnd.Next(5) + 5;
			base.RaiseFinishedPercentChanged(0.4);
			for (int i = 0; i < count; i++)
			{
				var list = modelList.List.Where(n => n.Operation2Type ==
					  SchilsCostsListItem.SchilsCostsListItemOpertion2Type.Direction).ToList();
				var item = list.RandomItem(rnd);
				base.RaiseStepChanged("Карточка Распоряжение " + (i + 1).ToString());
				var recordID = item.DirectionID;
				var card = new DirectionViewModel() { State = ViewModelState.Edit };

				card.Load(recordID);
			}

			count = rnd.Next(5) + 5;
			base.RaiseFinishedPercentChanged(0.6);
			for (int i = 0; i < count; i++)
			{
				var list = modelList.List.Where(n => n.Operation2Type ==
					  SchilsCostsListItem.SchilsCostsListItemOpertion2Type.Return).ToList();
				var item = list.RandomItem(rnd);
				base.RaiseStepChanged("Карточка Возврат " + (i + 1).ToString());
				var recordID = item.ID;
				var card = new ReturnViewModel() { State = ViewModelState.Edit };
				card.Load(recordID);
			}

			count = rnd.Next(5) + 5;
			base.RaiseFinishedPercentChanged(0.8);
			for (int i = 0; i < count; i++)
			{
				var list = modelList.List.Where(n => n.Operation2Type ==
					  SchilsCostsListItem.SchilsCostsListItemOpertion2Type.Transfer).ToList();
				var item = list.RandomItem(rnd);
				base.RaiseStepChanged("Карточка  Перечисление" + (i + 1).ToString());
				var recordID = item.ID;
				var card = new CostsViewModel() { State = ViewModelState.Edit };
				card.Load(recordID);
			}


			base.RaiseFinishedPercentChanged(1);
		}
	}
}
