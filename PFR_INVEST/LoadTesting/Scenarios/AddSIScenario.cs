﻿using System;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class AddSIScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Справочники - Добавление СИ"; }
        }

        public override void Run()
        {
            Login();
            var rnd = new Random();

            //RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.1));
            //RaiseStepChanged(new StepChangedEventArgs("Загрузка модели"));
            //RaiseScenarioFinished(new EventArgs());

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.1));
            RaiseStepChanged(new StepChangedEventArgs("Загрузка модели списка"));
            var lmodel = new SIListViewModel(false);

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.3));
            RaiseStepChanged(new StepChangedEventArgs("Загрузка модели формы"));
            var model = new SIViewModel(0, ViewModelState.Create);

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.5));
            RaiseStepChanged(new StepChangedEventArgs("Заполнение формы"));
            model.FullName = "Test model for NPF";
            model.FormalizedName = "Test model";
            model.ShortName = "Test";
            model.DeclarationName = "Test declaration";
            model.RegistrationNum = "123";
            model.Registrator = "Test registrator";
            model.SelectedType = model.TypesList[rnd.Next(0, model.TypesList.Length - 1)];
            model.LegalAddress = "Test legal adress";
            model.PostAddress = "Test post adress";
            model.StreetAddress = "Test street adress";
            model.Email = "a@a.ru";
            model.Phone = "1234567";
            model.Fax = "1234567";
            model.HeadFullName = "Test name";
            model.HeadPosition = "Test position";
            model.INN = "1234567890";
            model.OKPP = "123456789";

            model.LicUKNumber = "123";
            model.LicUKDuration = 10;
            model.LetterWho = "Test for who";

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.7));
            RaiseStepChanged(new StepChangedEventArgs("Сохранение формы"));
            model.SaveCard.Execute(null);

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.9));
            RaiseStepChanged(new StepChangedEventArgs("Обновление списка"));
            lmodel.RefreshList.Execute(null);

            //RaiseStepChanged(new StepChangedEventArgs("Удаление формы"));
            //model.ExecuteDeleteTest(DocOperation.Archive);

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(1.0));
        }
    }
}
