﻿using System;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class ACCListScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Бэк-офис - Счета - Счета"; }
        }

        public override void Run()
        {
            base.Login();
            var rnd = new Random();

            base.RaiseStepChanged("Загрузка модели списка СПН");
            var modelList = new ACCListViewModel();


            base.RaiseFinishedPercentChanged(1);
        }
    }
}
