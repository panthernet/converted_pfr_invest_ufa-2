﻿using System;
using System.Linq;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class ZLMovementsListScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Работа с СИ - Перечисления средств - Журнал движения ЗЛ"; }
        }

        public override void Run()
        {
            base.Login();
            var rnd = new Random();
            base.RaiseStepChanged(0.1, "Загрузка модели списка");
            var model = new ZLMovementsListViewModel();

            var list = model.ZLMovements;
            var item = list.Skip(rnd.Next(list.Count - 1)).First();

            base.RaiseStepChanged(0.5, "Список перечислений");

            var recordID = item.ID;
            var card2 = new TransferViewModel(recordID, ViewModelState.Edit);


            base.RaiseScenarioFinished();
        }
    }
}
