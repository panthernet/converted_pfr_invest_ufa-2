﻿using System;
using System.Linq;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class AddFinregisterScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Работа с НПФ - Добавление финреестра"; }
        }

        public override void Run()
        {
            Login();
            var rnd = new Random();

            //RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.1));
            //RaiseStepChanged(new StepChangedEventArgs("Загрузка модели"));
            //RaiseScenarioFinished(new EventArgs());

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.1));
            RaiseStepChanged(new StepChangedEventArgs("Загрузка модели списка"));
            var lmodel = new RegistersListViewModel();

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.2));
            RaiseStepChanged(new StepChangedEventArgs("Выбор случайного реестра"));
            var list = lmodel.List.Where(item => !string.IsNullOrEmpty(item.RegisterKind)).ToList();
            var reg_id = list[rnd.Next(0, list.Count - 1)].RegisterID;
            //var rmodel = new RegisterViewModel(reg_id, VIEWMODEL_STATES.Read);

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.3));
            RaiseStepChanged(new StepChangedEventArgs("Загрузка модели формы"));
            var model = new FinRegisterViewModel(reg_id, ViewModelState.Create) { State = ViewModelState.Create, IsDataChanged = true };

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.5));
            RaiseStepChanged(new StepChangedEventArgs("Заполнение формы"));
            model.SelectedNPF = model.NPFList[rnd.Next(0, model.NPFList.Count - 1)];
            model.ZLCount = 10;
            model.Count = 100;

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.7));
            RaiseStepChanged(new StepChangedEventArgs("Сохранение формы"));
            if (model.CanExecuteSave())
                model.SaveCard.Execute(null);
            else throw new Exception("Ошибка при проверке параметров для сохранения!");

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.9));
            RaiseStepChanged(new StepChangedEventArgs("Обновление списка"));
            lmodel.RefreshList.Execute(null);
            //rmodel.RefreshFinregWithNPFList();

            //RaiseStepChanged(new StepChangedEventArgs("Удаление формы"));
            //model.ExecuteDeleteTest(DocOperation.Archive);

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(1.0));
        }
    }
}
