﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.LoadTesting.Scenarios
{
	public class AddBudgetScenario : ScenarioBase
	{
		public override string ScenarioName
		{
			get { return "Бэк-офис - Расходы по СЧ ИЛС - Добавить бюджет"; }
		}

		public override void Run()
		{
			base.Login();
			var rnd = new Random();

			base.RaiseStepChanged("Загрузка модели списка");
			var modelList = new SchilsCostsListViewModel();

			base.RaiseStepChanged(0.2, "Загрузка модели формы");
			var model = new BudgetViewModel() { State = ViewModelState.Create };

			base.RaiseStepChanged(0.5, "Заполнение формы");
			model.SelectedPortfolio = model.PortfolioList.RandomItem(rnd);
			model.Sum = 0.01M * rnd.Next(10000);
			model.Comment = "Тестовой " + rnd.Next(1000);

			base.RaiseStepChanged(0.7, "Сохранение формы");
			if (model.CanExecuteSave())
				model.SaveCard.Execute(null);
			else
				throw new Exception("Ошибка при проверке параметров для сохранения!");

			base.RaiseFinishedPercentChanged(1);
		}
	}
}
