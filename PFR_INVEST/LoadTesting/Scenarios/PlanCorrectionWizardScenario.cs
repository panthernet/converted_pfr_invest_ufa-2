﻿using System;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class PlanCorrectionWizardScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Работа с СИ - Перечисления средств - Мастер корректировки годового плана"; }
        }

        public override void Run()
        {
            Login();
            var rnd = new Random();

            RaiseFinishedPercentChanged(0.1);

            RaiseStepChanged(0.1, "Загрузка модели");
            var model = new SIPlanCorrectionWizardViewModel(8, 1) { State = ViewModelState.Edit };

            RaiseStepChanged(0.2, "Выбор случайного года перечислений");
            model.SelectedYear = model.YearsList.RandomItem(rnd);
            RaiseStepChanged(string.Format("Выборан {0} год", model.SelectedYear.Name));
            RaiseFinishedPercentChanged(0.6);

            RaiseStepChanged("Выбор случайного месяца перечислений");
            model.SelectedMonth = model.MonthsList.RandomItem(rnd);
            RaiseStepChanged(string.Format("Выборан месяц {0}", model.SelectedMonth.Name));
            RaiseFinishedPercentChanged(0.7);

            if (model.AvailableContractsList != null)
            {
                RaiseStepChanged("Заполнение сумм");
                model.AvailableContractsList.ForEach(item => item.PlanSum += 0.01M * rnd.Next(10000));
                RaiseStepChanged("Заполнено");
                RaiseFinishedPercentChanged(0.9);
                RaiseStepChanged("Сохранение");
                model.UpdateUKPlanSums();
                RaiseStepChanged("Сохранено");
            }

            RaiseFinishedPercentChanged(1.0);
        }
    }
}
