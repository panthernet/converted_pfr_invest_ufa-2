﻿using System;
using System.Collections.Generic;

namespace PFR_INVEST.LoadTesting.Scenarios
{
	public class ComplexBOScenario : ScenarioBase
	{
		public override string ScenarioName
		{
			get { return "Комплексный - Бэк-офис"; }
		}

		public override void Run()
		{
			Login();
			var rnd = new Random();

			List<ScenarioBase> scenarios = new List<ScenarioBase>();
			
			scenarios.Add(new AddDueScenario());
			scenarios.Add(new AddDuePrecise());
			scenarios.Add(new AddPrepaymentScenario());
			scenarios.Add(new AddPrepaymentPreciseScenario());
			scenarios.Add(new AddDueDeadScenario());
			scenarios.Add(new AddDueDeadPreciseScenario());
			scenarios.Add(new AddDueExcessScenario());
			scenarios.Add(new AddDueExcessPreciseScenario());
			scenarios.Add(new AddDueUndistributedScenario());
			scenarios.Add(new AddDueUndistributedPreciseScenario());
			scenarios.Add(new AddTreasurityDocScenario());
			
			scenarios.Add(new AddEnrollmentPercentsScenario());
			scenarios.Add(new AddEnrollmentOtherScenario());
			scenarios.Add(new AddWidthdrawScenario());
			scenarios.Add(new AddTransferScenario());
			scenarios.Add(new CostsListScenario());
			scenarios.Add(new AddCostScenario());
			scenarios.Add(new SchilsCostsListScenario());
			scenarios.Add(new AddBudgetCorrectionScenario());
			scenarios.Add(new AddDirectionScenario());
			scenarios.Add(new AddTransferSchilsScenario());
			scenarios.Add(new AddReturnScenario());
			scenarios.Add(new BalanceListScenario());
			scenarios.Add(new TempAllocationListScenario());
			scenarios.Add(new TransferToNPFListScenario());
			scenarios.Add(new UKTransferListScenario());


			RunInnerScenarioCycle(scenarios);
		}
	}
}
