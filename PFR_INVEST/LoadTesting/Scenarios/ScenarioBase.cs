﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public abstract class ScenarioBase : ILoadScenario
    {

        protected int ErrorCount;
        private readonly StringBuilder m_ScenarioReportBuilder = new StringBuilder();

        public string ScenarioReport
        {
            get
            {
                return m_ScenarioReportBuilder.ToString();
            }
        }

        private int instanceid;
        public int InstanceID
        {
            get
            {
                return instanceid;
            }
            set
            {
                instanceid = value;
            }
        }

        protected Random InnerRandom = new Random();

        /// <summary>
        /// Имя сценария
        /// </summary>
        public abstract string ScenarioName { get; }
        /// <summary>
        /// Запуск сценария
        /// </summary>
        public abstract void Run();
        /// <summary>
        /// Является ли сценарий частью сложного сценария
        /// </summary>
        public bool IsComplexScenario { get; set; }

        private int cyclecount = 1;
        /// <summary>
        /// Кол-во циклов прогона сценария
        /// </summary>
        public int CycleCount { get { return cyclecount; } set { cyclecount = value; } }
        /// <summary>
        /// Счетсик циклов для документирования
        /// </summary>
        protected int CycleCounter = 0;

        /// <summary>
        /// Системное время для общего запуска тестов после коннекта (null = сразу)
        /// </summary>
        public DateTime? LaunchTime { get; set; }

        protected PerfomanceTracker PerfTracker = new PerfomanceTracker();

        /// <summary>
        /// Задержка между шагами сценария
        /// </summary>
        private int m_LatencyDelay = 0;
        public int LatencyDelay
        {
            get
            {
                return m_LatencyDelay;
            }
            set
            {
                m_LatencyDelay = value;
            }
        }

        /// <summary>
        /// Включена ли задержка между сценариями
        /// </summary>
        public bool IsLatencyDelayEnabled { get; set; }

        public virtual void RunScenario()
        {
            try
            {
                if (LaunchTime != null)
                {
                    Login();
                    while (LaunchTime.Value > DateTime.Now)
                        Thread.Sleep(200);
                }

                for (int i = 0; i < CycleCount; i++)
                {
                    CycleCounter++;
                    var perf = new PerfomanceTracker();
                    this.Run();
                    RaiseMeasure("Завершено!", false, false);
                    RaiseMeasure(ScenarioName, perf.GetMSeconds() / 1000.0, false, true);
                }
            }
            catch (Exception ex)
            {
                RaiseException(ex);
                if (IsComplexScenario)
                    RaiseComplexScenarioError(ex);
            }
            finally
            {
                if (!IsComplexScenario)
                {
                    try
                    {
                        RaiseScenarioFinished(EventArgs.Empty);
                    }
                    catch
                    {
                    }
                }
            }
        }

        private static bool m_bInited;
        protected void Login()
        {
            if (IsComplexScenario) return;
            lock (typeof(ScenarioBase))
            {
                if (m_bInited)
                    return;

                CreateClient();
                m_bInited = true;
            }
        }

        protected void OnComplexScenarioError(object sender, EventArgs e)
        {
            var args = e as ComplexScenarioErrorEventArgs;
            if (e == null) return;

            RaiseException(new Exception(args.Text));
        }

        #region CreateClient()
        private void CreateClient()
        {
            RaiseStepChanged(new StepChangedEventArgs("Подключение"));
            try
            {
                App.ShowMessageBoxOnError = false;
                PFR_INVEST.DataAccess.Client.ServiceSystem.GetDataServiceDelegate = WCFClient.GetDataServiceFunc;
                WCFClient.UseAutoRepair = false;
                WCFClient.ClientCredentials = new System.ServiceModel.Description.ClientCredentials();
                //TODO вынести в настройки
                WCFClient.ClientCredentials.Windows.ClientCredential.Domain = "";
                WCFClient.ClientCredentials.Windows.ClientCredential.UserName = "";
                WCFClient.ClientCredentials.Windows.ClientCredential.Password = "";
                WCFClient.CreateClientByCredentials();
                WCFClient.UseAutoRepair = true;
            }
            catch (ThreadAbortException)
            {

            }
            catch (Exception ex)
            {
                RaiseException(ex);
            }
            RaiseStepChanged(new StepChangedEventArgs("Ожидание запуска..."));
        }
        #endregion

        protected void RaiseException(Exception ex)
        {
            if (ex is System.Threading.ThreadAbortException) return;
            m_ScenarioReportBuilder.AppendFormat("***Исключение: {0}.\r\nStack:\r\n{1}", ex.InnerException == null ? ex.Message : ex.InnerException.Message, ex.StackTrace ?? string.Empty);
            RaiseErrorCountChanged(new ErrorCountChangedEventArgs(++ErrorCount, InstanceID));
            RaiseExceptionOccured(new ExceptionOccuredEventArgs(ex));
            // Thread.Sleep(5000);
        }

        protected void RunInnerScenario(ILoadScenario scenario)
        {
            scenario.IsComplexScenario = true;
            RaiseStepChanged(new StepChangedEventArgs(ScenarioExtension.GetPrivateScenarioName(scenario.ScenarioName)));
            scenario.ComplexScenarioError += this.OnComplexScenarioError;
            var perf = new PerfomanceTracker();
            scenario.RunScenario();
            RaiseMeasure(scenario.ScenarioName, perf.GetMSeconds() / 1000.0, true, true);
            scenario.ComplexScenarioError -= this.OnComplexScenarioError;
        }

        protected void RunInnerScenarioCycle(List<ScenarioBase> scenarios)
        {
            double steep = (1.0 / scenarios.Count) / (double)CycleCount;
            for (int i = 0; i < scenarios.Count; i++)
            {
                RaiseFinishedPercentChanged((i + ((CycleCounter - 1) * scenarios.Count)) * steep);
                RunInnerScenario(scenarios[i]);
            }

            if (CycleCount <= CycleCounter) RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(1.0));
        }

        public event EventHandler ComplexScenarioError;
        protected void RaiseComplexScenarioError(Exception ex)
        {
            if (ComplexScenarioError != null)
                ComplexScenarioError(this, new ComplexScenarioErrorEventArgs(this.ScenarioName + ": " + ex.Message + "\r\nStack:\r\n" + ex.StackTrace ?? string.Empty));
            RaiseExceptionOccured(new ExceptionOccuredEventArgs(ex));
        }

        public event EventHandler FinishedPercentChanged;

        protected void RaiseFinishedPercentChanged(double percent)
        {
            this.RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(percent));
        }

        public event EventHandler MeasureInvoked;

        protected void RaiseMeasure(string text, bool ForComplex, bool Common)
        {
            RaiseMeasure(text, PerfTracker, ForComplex, Common);
        }
        protected void RaiseMeasure(string text, PerfomanceTracker perf, bool ForComplex, bool Common)
        {
            RaiseMeasure(text, perf.GetMSeconds() / 1000.0, ForComplex, Common);
            //ScenarioDelay = perf.GetMSeconds() / 1000.0; ;
        }

        protected void RaiseMeasure(string text, double value, bool ForComplex, bool Common)
        {
            if (Common) text = string.Format("Общий: {0}", text);
            if ((IsComplexScenario && !ForComplex) || (!IsComplexScenario && ForComplex)) return;
            if (MeasureInvoked != null)
                MeasureInvoked(this, new MeasureArgs(text, value));

        }

        protected void RaiseFinishedPercentChanged(FinishedPercentChangedEventArgs ea)
        {
            m_ScenarioReportBuilder.AppendFormat("Процент выполнения: {0:p2}.\r\n", ea.Percent);
            if (FinishedPercentChanged != null)
                FinishedPercentChanged(this, ea);
        }

        public event EventHandler StepChanged;

        protected void RaiseStepChanged(double percent, string message)
        {
            this.RaiseFinishedPercentChanged(percent);
            this.RaiseStepChanged(message);
        }

        protected void RaiseStepChanged(string message)
        {
            this.RaiseStepChanged(new StepChangedEventArgs(message));
        }

        protected void RaiseStepChanged(StepChangedEventArgs ea)
        {
            m_ScenarioReportBuilder.AppendFormat("Шаг[{0}]: {1}.\r\n", CycleCounter, ea.Step);
            if (StepChanged != null)
            {
                ea.Step = string.Format("[{0}]: {1}", CycleCounter, ea.Step);
                RaiseMeasure(ea.Step, false, false);
                StepChanged(this, ea);

                if (IsLatencyDelayEnabled && !ea.Step.Contains("Подключение") && !ea.Step.Contains("Ожидание запуска"))
                {
                    //ScenarioDelay = InnerRandom.Next(0, DelayMaxNumber);
                    Thread.Sleep(LatencyDelay);
                }
                PerfTracker.Clear();
            }
        }

        public event EventHandler ErrorCountChanged;
        protected void RaiseErrorCountChanged(ErrorCountChangedEventArgs ea)
        {
            m_ScenarioReportBuilder.AppendFormat("Кол-во ошибок: {0}.\r\n", ea.ErrorCount);
            if (ErrorCountChanged != null)
                ErrorCountChanged(this, ea);
        }

        public event EventHandler<ExceptionOccuredEventArgs> ExceptionOccured;
        protected void RaiseExceptionOccured(ExceptionOccuredEventArgs ea)
        {
            if (ExceptionOccured != null)
                ExceptionOccured(this, ea);
        }

        public event EventHandler ScenarioFinished;
        protected void RaiseScenarioFinished()
        {
            this.RaiseFinishedPercentChanged(1.0);
            this.RaiseScenarioFinished(EventArgs.Empty);
        }
        protected void RaiseScenarioFinished(EventArgs ea)
        {
            try
            {
                // RaiseMeasure("Завершено!", false, false);
                m_ScenarioReportBuilder.AppendFormat("Сценарий завершен. Кол-во ошибок : {0}.", ErrorCount);
                if (ScenarioFinished != null)
                    ScenarioFinished(this, ea);
            }
            catch { }
        }

        protected void RiseEmptySequence()
        {
            this.RaiseStepChanged("Нет элементов для которых можно продолжать тест");
            this.RaiseScenarioFinished(EventArgs.Empty);
        }
    }

    public static class ScenarioExtension
    {
        public static T RandomItem<T>(this IList<T> list, Random random)
        {
            if (list != null && list.Count > 0)
            {
                int c = list.Count;
                int index = random.Next(c - 1);
                return list[index];
            }
            else
                return default(T);
        }

        public static string GetPrivateScenarioName(string text)
        {
            if (!text.Contains(" - ")) return text;
            var items = text.Split(new string[] { " - " }, StringSplitOptions.RemoveEmptyEntries);
            var result = items[1] + (items.Length > 2 ? string.Format(" - {0}", items[2]) : "");
            return result;
        }
    }
}
