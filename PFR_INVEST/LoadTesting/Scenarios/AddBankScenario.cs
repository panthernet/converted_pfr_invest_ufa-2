﻿using System;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class AddBankScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Справочники - Добавление банка"; }
        }

        public override void Run()
        {
            Login();
            var rnd = new Random();

            //RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.1));
            //RaiseStepChanged(new StepChangedEventArgs("Загрузка модели"));
            //RaiseScenarioFinished(new EventArgs());

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.1));
            RaiseStepChanged(new StepChangedEventArgs("Загрузка модели списка"));
            var lmodel = new BanksListViewModel();

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.3));
            RaiseStepChanged(new StepChangedEventArgs("Загрузка модели формы"));
            var model = new BankViewModel(0) { State = ViewModelState.Create, IsDataChanged = true };

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.5));
            RaiseStepChanged(new StepChangedEventArgs("Заполнение формы"));
            model.Bank.FullName = "Test model for Bank";
            model.Bank.FormalizedName = "Test model";
            model.Bank.ShortName = "Test";
            model.Bank.RegistrationNum = "123";
            model.Bank.RegistrationDate = DateTime.Now;
            model.Bank.EndDate = DateTime.Now.AddDays(30);
            model.Bank.Registrator = "Test registrator";
            model.Bank.LegalAddress = "Test legal adress";
            model.Bank.StreetAddress = "Test real adress";
            model.Bank.PostAddress = "Test postal adress";
            model.Bank.Phone = "1234567";
            model.Bank.Fax = "1234567";
            model.License.Number = "1234567890";
            model.License.RegistrationDate = DateTime.Now;
            model.Bank.INN = "1234567890";
            model.Bank.OKPP = "123456789";
            model.Bank.BIK = "123456789";
            model.Bank.CorrAcc = "12345678901234567890";

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.7));
            RaiseStepChanged(new StepChangedEventArgs("Сохранение формы"));
            if (model.CanExecuteSave())
                model.SaveCard.Execute(null);
            else throw new Exception("Ошибка при проверке параметров для сохранения!");

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.9));
            RaiseStepChanged(new StepChangedEventArgs("Обновление списка"));
            lmodel.RefreshList.Execute(null);

            //RaiseStepChanged(new StepChangedEventArgs("Удаление формы"));
            //model.ExecuteDeleteTest(DocOperation.Archive);

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(1.0));

        }
    }
}
