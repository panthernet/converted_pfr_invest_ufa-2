﻿using System.Collections.Generic;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class ComplexNPFScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Комплексный - Работа с НПФ"; }
        }

        public override void Run()
        {
            Login();

            var scenarios = new List<ScenarioBase>();
            scenarios.Add(new AddDeadERZLScenario());
            scenarios.Add(new AddERZLNotifyScenario());
            scenarios.Add(new AddFinregisterScenario());
            scenarios.Add(new AddNotifyERZLRedistScenario());
            scenarios.Add(new AddRegisterScenario());
            scenarios.Add(new OpenRegisterArchiveScenario());

            RunInnerScenarioCycle(scenarios);
        }
    }
}
