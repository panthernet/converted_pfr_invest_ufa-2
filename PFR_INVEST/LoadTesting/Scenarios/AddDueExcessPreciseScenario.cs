﻿using System;
using System.Linq;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class AddDueExcessPreciseScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Бэк-офис - Страховые взносы - Добавить уточнение излишне учтенных страховых  взносов"; }
        }

        public override void Run()
        {
            base.Login();
            var rnd = new Random();

            base.RaiseStepChanged("Загрузка модели списка СПН");
			var modelList = new DueListViewModel(new DataObjects.ServiceItems.PortfolioTypeFilter() { Types = new DataObjects.Portfolio.Types[] { Portfolio.Types.SPN } });

            var item = modelList.DueList.Where(d => d.Operation == DueDocKindIdentifier.DueExcess).ToList().RandomItem(rnd);

            base.RaiseStepChanged(0.2, "Загрузка модели формы");
            var modelCard = new DueExcessViewModel(PortfolioIdentifier.PortfolioTypes.SPN) { State = ViewModelState.Edit };
            modelCard.Load(item.ID);
            var model = new DueExcessAccurateViewModel(modelCard) { State = ViewModelState.Create };

            base.RaiseStepChanged(0.5, "Заполнение формы");
            model.DocDate = DateTime.Now;
            model.RegNum = "Тестовой " + rnd.Next(1000);
            model.Summ = 0.01M * rnd.Next(10000) - 50M;
            var temp = model.ChSumm; // дёрганье для вычисления

            RaiseStepChanged(0.7, "Сохранение формы");
            if (model.CanExecuteSave())
                model.SaveCard.Execute(null);
            else
                throw new Exception("Ошибка при проверке параметров для сохранения!");

            RaiseStepChanged(0.9, "Обновление списка");
            modelList.RefreshList.Execute(null);
            base.RaiseFinishedPercentChanged(1);
        }
    }
}
