﻿using System;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class AddCBPaymentScenario : ScenarioBase
    {
        //        public const string ScenarioName = "Добавление НПФ";

        public override string ScenarioName
        {
            get { return "Справочники - Добавление выплат по ЦБ"; }
        }

        public override void Run()
        {

            Login();
            var rnd = new Random();

            //RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.1));
            //RaiseStepChanged(new StepChangedEventArgs("Загрузка модели"));
            //RaiseScenarioFinished(new EventArgs());

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.1));
            RaiseStepChanged(new StepChangedEventArgs("Загрузка модели списка"));
            var lmodel = new SecuritiesListViewModel();

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.3));
            RaiseStepChanged(new StepChangedEventArgs("Выбор случайной ЦБ"));
            var cb = lmodel.SecuritiesList[rnd.Next(0, lmodel.SecuritiesList.Count - 1)];
            var cbmodel = new SecurityViewModel(cb.ID, ViewModelState.Read);

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.4));
            RaiseStepChanged(new StepChangedEventArgs("Создание платежа по ЦБ"));
            var model = new PaymentViewModel(cbmodel.CB, ViewModelState.Create) { IsDataChanged = true };

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.5));
            RaiseStepChanged(new StepChangedEventArgs("Заполнение формы"));
            model.Payment.CouponPeriod = 10;
            model.Payment.PaymentDate = DateTime.Now;
            model.Payment.Coupon = 5;
            model.Payment.CouponSize = 5;
            model.Payment.SecurityId = cbmodel.CB.ID;

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.7));
            RaiseStepChanged(new StepChangedEventArgs("Сохранение формы"));
            if (model.CanExecuteSave())
                model.SaveCard.Execute(null);
            else throw new Exception("Проверка на сохранение не пройдена!");

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.9));
            RaiseStepChanged(new StepChangedEventArgs("Обновление списка"));
            lmodel.RefreshList.Execute(null);

            //RaiseStepChanged(new StepChangedEventArgs("Удаление формы"));
            //model.ExecuteDeleteTest(DocOperation.Archive);

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(1.0));

        }
    }
}
