﻿using System;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class ControlSIDocumentsListScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Работа с СИ - Корреспонденция - Контроль"; }
        }

        public override void Run()
        {
            Login();
            var rnd = new Random();
            base.RaiseFinishedPercentChanged(0.1);
            base.RaiseStepChanged("Загрузка модели списка");
            var model = new CorrespondenceSIControlListViewModel();

            var list = model.List;


            var count = rnd.Next(5) + 5;
            base.RaiseFinishedPercentChanged(0.2);
            for (int i = 0; i < count; i++)
            {
                var item = list.RandomItem(rnd);
                base.RaiseStepChanged("Открытие карточки " + (i + 1).ToString());
                var recordID = item.ID;
                var card = new DocumentSIViewModel(recordID, ViewModelState.Edit) { State = ViewModelState.Edit };
            }


            base.RaiseFinishedPercentChanged(1.0);
        }
    }
}
