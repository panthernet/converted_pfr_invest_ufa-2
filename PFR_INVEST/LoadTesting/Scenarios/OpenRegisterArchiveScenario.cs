﻿using System;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class OpenRegisterArchiveScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Работа с НПФ - Открытие архива реестров"; }
        }

        public override void Run()
        {
            Login();
            var rnd = new Random();

            //RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.1));
            //RaiseStepChanged(new StepChangedEventArgs("Загрузка модели"));
            //RaiseScenarioFinished(new EventArgs());

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.3));
            RaiseStepChanged(new StepChangedEventArgs("Загрузка модели списка"));
            var lmodel = new RegistersArchiveListViewModel();

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.7));
            RaiseStepChanged(new StepChangedEventArgs("Обновление списка"));
            lmodel.RefreshList.Execute(null);

            //RaiseStepChanged(new StepChangedEventArgs("Удаление формы"));
            //model.ExecuteDeleteTest(DocOperation.Archive);

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(1.0));
        }
    }
}
