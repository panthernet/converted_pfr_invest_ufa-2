﻿using System;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class TransferToNPFListScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Бэк-офис - Перечисление средств - Перечисление средств в НПФ"; }
        }

        public override void Run()
        {
            base.Login();
            var rnd = new Random();

            base.RaiseStepChanged("Загрузка модели списка");
            var modelList = new TransferFromOrToNPFListViewModel(true);

            base.RaiseFinishedPercentChanged(1);
        }
    }
}
