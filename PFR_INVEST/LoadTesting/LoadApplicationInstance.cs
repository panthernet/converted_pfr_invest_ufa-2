﻿using System;
using System.Threading;
using PFR_INVEST.Common.Logger;

namespace PFR_INVEST.LoadTesting
{
    public class LoadApplicationInstance : MarshalByRefObject, ILoadAplicationInstance
    {
        private ILoadApplicationController Controller;
        private Thread worker;

        public int InstanceID { get; private set; }

        private ILoadScenario Scenario;
        public string ScenarioReport
        {
            get
            {
                return Scenario.ScenarioReport;
            }
        }

        public LoadApplicationInstance()
        {
            if (App.log == null)
                App.log = new Log("error.log");
        }

        public void RunScenario(ILoadApplicationController controller, Type scenarioType, int ID, int CycleCount, DateTime? LaunchTime, bool delayEnabled, int LatencyDelay)
        {
            Controller = controller;
            InstanceID = ID;

            Scenario = (ILoadScenario)Activator.CreateInstance(scenarioType);
            Scenario.ErrorCountChanged += Scenario_ErrorCountCallback;
            Scenario.ExceptionOccured += Scenario_ExceptionOccuredCallback;
            Scenario.StepChanged += Scenario_StepCallback;
            Scenario.FinishedPercentChanged += Scenario_FinishedPercentCallback;
            Scenario.ScenarioFinished += Scenario_ScenarioFinished;
            Scenario.MeasureInvoked += Measure_Callback;
            ((PFR_INVEST.LoadTesting.Scenarios.ScenarioBase)Scenario).InstanceID = ID;
            Scenario.CycleCount = CycleCount;
            Scenario.LaunchTime = LaunchTime;
            Scenario.IsLatencyDelayEnabled = delayEnabled;
            Scenario.LatencyDelay = LatencyDelay;

            worker = new Thread(Scenario.RunScenario);
            worker.Start();
        }

        public void Stop()
        {
            worker.Abort();
        }

        void Scenario_ExceptionOccuredCallback(object sender, ExceptionOccuredEventArgs e)
        {
            Controller.ExceptionCallback(this, e.Exception);
        }

        void Measure_Callback(object sender, EventArgs e)
        {
            var args = e as MeasureArgs;
            Controller.MeasureCallback(this, args.Text, args.Value);
        }

        void Scenario_ScenarioFinished(object sender, EventArgs e)
        {
            Controller.ScenarioFinishedCallback(this);
        }

        void Scenario_ErrorCountCallback(object sender, EventArgs e)
        {
            try
            {
                Controller.ErrorCallback(this, (e as ErrorCountChangedEventArgs).ErrorCount, (e as ErrorCountChangedEventArgs).ID);
            }
            catch { }
        }

        void Scenario_StepCallback(object sender, EventArgs e)
        {
            Controller.StepCallback(this, (e as StepChangedEventArgs).Step);
        }

        void Scenario_FinishedPercentCallback(object sender, EventArgs e)
        {
            Controller.FinishedPercentCallback(this, (e as FinishedPercentChangedEventArgs).Percent);
        }

    }

}
