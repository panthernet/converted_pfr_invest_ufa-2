﻿using System;

namespace PFR_INVEST.LoadTesting
{
    public interface ILoadApplicationController
    {
        void StepCallback(ILoadAplicationInstance instance, string stepName);
        void ErrorCallback(ILoadAplicationInstance instance, int errorCount, int ID);
        void ExceptionCallback(ILoadAplicationInstance instance, Exception ex);
        void FinishedPercentCallback(ILoadAplicationInstance instance, double percent);
        void ScenarioFinishedCallback(ILoadAplicationInstance instance);
        void MeasureCallback(ILoadAplicationInstance instance, string text, double value);
    }
}
