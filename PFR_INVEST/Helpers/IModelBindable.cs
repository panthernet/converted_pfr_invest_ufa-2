﻿using System;

namespace PFR_INVEST.Helpers
{
    /// <summary>
    /// Интерфейс, указывающий, что представление связано с моделью
    /// Позволяет уйти от старого метода связки представления и модели
    /// </summary>
    public interface IModelBindable
    {
        Type ModelType { get; }
    }

    /// <summary>
    /// Интерфейс, указывающий, что представление связано с моделью, требующей входной параметр Id
    /// Позволяет уйти от старого метода связки представления и модели
    /// </summary>
    public interface IModelBindableWithId: IModelBindable
    {
        bool IsIdOnly { get; }
    }

    /// <summary>
    /// Интерфейс, указывающий, что представление связано с моделью, требующей входные параметры
    /// Позволяет уйти от старого метода связки представления и модели
    /// </summary>
    public interface IModelBindableWithParams : IModelBindable
    {
        object[] Params { get; }
    }
}
