﻿using System.Windows.Controls;
using System.Windows;

namespace PFR_INVEST.Helpers
{
    /// <summary>
    /// Хелпер для связи кода вьюхи с моделью
    /// В коде выставляем Value 
    /// В разметке биндим Value на нужное свойство модели
    /// </summary>
    public class BindingHelper:Control
    {
        public object Value
        {
            get { return (object)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }
        
        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register("Value", typeof(object), typeof(BindingHelper), new UIPropertyMetadata(null));        
    }
}
