﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Windows;
using System.Windows.Controls;
using DevExpress.Xpf.Bars;
using DevExpress.Xpf.Core.Native;
using DevExpress.Xpf.Docking;
using DevExpress.Xpf.Grid;
using DevExpress.XtraPrinting;
using Microsoft.Win32;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Views;

namespace PFR_INVEST.Helpers
{
    public static class DevExpressHelper
    {
        public static LoginMessageItem ROPSRetriesWrapper(int retriesCount, Func<LoginMessageItem> method)
        {
            //-2146233087
            var count = 0;
            var result = false;
            while (!result)
            {
                try
                {
                    return method();
                }
                catch
                {
                    Thread.Sleep(500);
                    count++;
                    Debug.WriteLine("ROPS FAIL: " + count);
                    result = count > retriesCount;
                }
            }
            return null;
        }

        public static void PrintGridControl(GridControl grid)
        {
            var view = grid.View as TableView;
            view?.ShowPrintPreviewDialog(MainWindow.Instance);
        }

        public static void ExportGridControl(GridControl grid)
        {
            
            var view = grid.View as TableView;
            var dlg = new SaveFileDialog { Filter = "Файл Excel (*.xlsx)|*.xlsx" };
            if (dlg.ShowDialog() != true) return;
            view?.ExportToXlsx(dlg.FileName, new XlsxExportOptions
            {
                ExportMode = XlsxExportMode.SingleFile,
                TextExportMode = TextExportMode.Text,
            });
        }

        public static void AppendUrlEncoded(this StringBuilder sb, string name, string value, bool moreValues = true)
        {
            sb.Append(HttpUtility.UrlEncode(name));
            sb.Append("=");
            sb.Append(HttpUtility.UrlEncode(value));
            if (moreValues)
                sb.Append("&");
        }

        public const double MDEF_SIZE = 1000d;
        /// <summary>
        /// Указывает, что ширина панели будет выичсляться автоматически
        /// </summary>
        public const double PWIDTH_AUTO = 0;
        /// <summary>
        /// Указывает, что ширина панели будет вычислена автоматически единожды
        /// Исключает авторазмер при растягивания полей вода из-за больших значений.
        /// </summary>
        public const double PWIDTH_ONETIME_AUTO = -1;

        /// <summary>
        /// Устанавливает размер родительской панели, в заисимости от размера содержимого
        /// </summary>
        /// <param name="ctrl">Контрол, представляющий содержимое панели</param>
        /// <param name="measure">Ссылка набазовый метод MeasureOverride</param>
        /// <param name="desiredWidth">Предпочтительная ширина панели</param>
        /// <param name="desiredHeight"></param>
        public static Size MeasureParentPanel(UserControl ctrl, Func<Size, Size> measure, double desiredWidth, double desiredHeight = 0)
        {
            var offset = Size.Empty;
            const bool setMinSizeAlso = true;

            Size defaultSize;
            var item = ctrl.Parent as BaseLayoutItem;
            bool isNotAPanelParent = item == null;

            //пропускаем, если уже идет высчтывание размера
            if (_isUpdateSizeToContentInProcess) return item == null ? ctrl.DesiredSize : item.DesiredSize;
            _isUpdateSizeToContentInProcess = true;
            try
            {
                if (offset == Size.Empty)
                    offset = new Size();

                Size correctSize;
                //если авторазмер или одиночный авторазмер, выставляем Size для измерений
                if (desiredWidth == PWIDTH_AUTO || desiredWidth == PWIDTH_ONETIME_AUTO)
                {
                    correctSize = new Size(MDEF_SIZE, MDEF_SIZE);
                    //пропускаем, если режим ожиночного авторазмера и размер уже установлен
                    if (!isNotAPanelParent)
                    {
                        if (desiredWidth == PWIDTH_ONETIME_AUTO && (!double.IsNaN(item.MinWidth) || !double.IsInfinity(item.MinWidth)) && item.MinWidth > 0)
                        {
                            var prefSize = item.RenderSize.Width > item.DesiredSize.Width ? item.RenderSize : item.DesiredSize;
                            MeasureUIChildren(ctrl, prefSize);
                            return measure(prefSize);
                        }
                    }
                    else
                    {
                        if (desiredWidth == PWIDTH_ONETIME_AUTO && (!double.IsNaN(ctrl.MinWidth) || !double.IsInfinity(ctrl.MinWidth)))
                        {
                            var prefSize = new Size(ctrl.MinWidth == 0 ? MDEF_SIZE : ctrl.MinWidth, ctrl.MinHeight == 0 ? MDEF_SIZE : ctrl.MinHeight);
                            MeasureUIChildren(ctrl, prefSize);
                            return measure(prefSize);
                        }
                    }
                }
                else correctSize = new Size(desiredWidth, desiredHeight == 0 ? MDEF_SIZE : desiredHeight);

                //проводим измерение контента
                defaultSize = measure(correctSize);
                MeasureUIChildren(ctrl, defaultSize);
                // defaultSize = ctrl.DesiredSize;

                var sz = new Size(defaultSize.Width + 25 + offset.Width, defaultSize.Height + 25 + offset.Height);
                if (desiredWidth != PWIDTH_AUTO && desiredWidth != PWIDTH_ONETIME_AUTO)
                    sz.Width = desiredWidth;
                if (!isNotAPanelParent)
                {
                    //выставляем размеры панели хоста
                    if (setMinSizeAlso)
                    {
                        item.MinWidth = sz.Width;
                        item.MinHeight = sz.Height;
                    }
                    item.Parent.FloatSize = sz;
                    item.FloatSize = sz;
                    //item.InvalidateMeasure();
                   // item.Arrange(new Rect(0, 0, sz.Width, sz.Height));
                    //item.ItemHeight = new GridLength(sz.Height);
                    item.UpdateLayout();
                }
                else
                {
                    ctrl.MinWidth = sz.Width;
                    ctrl.MinHeight = sz.Height;
                }

            }
            finally
            {
                _isUpdateSizeToContentInProcess = false;
            }
            return defaultSize;
        }

        private static void MeasureUIChildren(UserControl ctrl, Size size)
        {
            ctrl.Measure(size);
            foreach (var child in ctrl.VisualChildren().OfType<UIElement>())
            {
                if (child.Visibility != Visibility.Visible) continue;
                child.Measure(ctrl.DesiredSize);
            }
        }

        /// <summary>
        /// Убирает отображение пункта меню по выбору и показу группировки колонок в контекстном меню колонок
        /// </summary>
        /// <param name="e">Параметры обработчика события Grid.View.ShowGridMenu </param>
        public static void HideGroupingFromColumnMenu(GridMenuEventArgs e)
        {
            if (e.MenuType != GridMenuType.Column) return;
            e.Customizations.Add(new RemoveBarItemAndLinkAction { ItemName = DefaultColumnMenuItemNames.GroupColumn });
            e.Customizations.Add(new RemoveBarItemAndLinkAction { ItemName = DefaultColumnMenuItemNames.GroupBox });
        }
        /// <summary>
        /// Убирает отображение пункта меню Выбор колонок в контекстном меню колонок
        /// </summary>
        /// <param name="e">Параметры обработчика события Grid.View.ShowGridMenu </param>
        public static void HideColumnChooserFromColumnMenu(GridMenuEventArgs e)
        {
            if (e.MenuType != GridMenuType.Column) return;
            e.Customizations.Add(new RemoveBarItemAndLinkAction { ItemName = DefaultColumnMenuItemNamesBase.ColumnChooser });
        }

        private static bool _isUpdateSizeToContentInProcess;

        /// <summary>
        /// Открывает родительские группы в гриде по хендлу строки. Хендл строки можно взять:
        /// grid.GetRowHandleByListIndex(index), где index это индекс элемента данных в списке данных ItemsSource
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="handle"></param>
        public static void ExpandParentGroupsByHandle(this GridControl grid, int handle)
        {
            var handles = new List<int>();
            int phandle = handle;
            while (grid.IsValidRowHandle(phandle))
            {
                phandle = grid.GetParentRowHandle(phandle);
               // var v = grid.GetGroupRowValue(phandle);
                handles.Insert(0, phandle);
            }
            handles.ForEach(a => grid.ExpandGroupRow(a));
        }

        /// <summary>
        /// Устанавливает фокус на строку и раскрывает вышестоящую группу
        /// Девекспресс иногда не открывает группу сам при установке фокуса :(
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="handle"></param>
        public static void ExpandFocusRow(this GridControl grid, int handle)
        {
            grid.View.FocusedRowHandle = handle;
            var ph = grid.GetParentRowHandle(handle);
            if (grid.IsValidRowHandle(ph))
                grid.ExpandGroupRow(ph);
        }

    }
}
