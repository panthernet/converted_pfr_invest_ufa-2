﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Helpers
{
    public sealed class QuarkDisplayHelper
    {
        private static Lazy<List<Quark>> _quarkSource;

        static QuarkDisplayHelper()
        {
            _quarkSource = new Lazy<List<Quark>>(() => DataContainerFacade.GetList<Quark>());
        }

        public static string GetQuarkDisplayValue(int quarkNum)
        {
            var result = "[не определено]";
            switch (quarkNum)
            {
                case 1:
                    result = "Квартал I";
                    break;
                case 2:
                    result = "Квартал II";
                    break;
                case 3:
                    result = "Квартал III";
                    break;
                case 4:
                    result = "Квартал IV";
                    break;
            }
            return result;
        }
    }
}
