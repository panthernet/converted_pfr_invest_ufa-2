﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using db2connector;
using db2connector.Contract;
using PFR_INVEST.Auth.ClientData;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.Auth.SharedData;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Common.MessageCompressor;

namespace PFR_INVEST
{
	public static class WCFClient
	{
		private static DataServiceClient _wcfClient;
		public static bool UseAutoRepair;
		private static readonly System.Timers.Timer PingTimer = new System.Timers.Timer();
		private static bool _isTimerBusy;

		//public static event Action ConnectionRestoredEvent;
		public static ClientCredentials CurrentClientCredentials => _wcfClient.ClientCredentials;
	    public static ClientCredentials ClientCredentials { get; set; }
	    public static ClientAuthType AuthType;

		static WCFClient()
		{
		    switch (ConfigurationManager.AppSettings["ClientAuthType"])
		    {
                case "ECASA":
                    AuthType = ClientAuthType.ECASA;
		            break;
                case "ECASAHTTPS":
                    AuthType = ClientAuthType.ECASAHTTPS;
                    break;
                default:
                    AuthType = ClientAuthType.None;
                    break;
		    }

            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, errors) => true;

            ServiceSystem.GetDataServiceDelegate = GetDataServiceFunc;
			AuthServiceSystem.GetDataServiceDelegate = GetDataServiceFunc;
			BLServiceSystem.GetDataServiceDelegate = GetDataServiceFunc;
			PingTimer.AutoReset = true;
			PingTimer.Interval = 30000;
			PingTimer.Elapsed += PingTimer_Elapsed;
		}

	    private static void PingTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			//Если предыдущий вызов не закончился ещё(из за проблем), то пропускаем
			if (!_isTimerBusy)
			{
			    try
			    {
			        _isTimerBusy = true;
			        if (App.IsClosing) return;
			        if (_wcfClient != null)
			            WCFServiceQueryManager.Query();
			    }
			    catch (ProtocolException ex)
			    {
                    App.log.WriteException(ex, "При обращении к WCF сервису возникла ошибка.");
			        var iEx = ex.InnerException as WebException;
			        if (iEx != null)
                        App.log.WriteLine("Доп: " + iEx.ExtractResponseString());
			            
                    _wcfClient?.Close();
                }
				catch (Exception ex)
				{
					_wcfClient?.Close();
					App.log.WriteException(ex, "При обращении к WCF сервису возникла ошибка.");
				}
				finally
				{
					_isTimerBusy = false;
				}
			}

			try
			{
				App.UserSettingsManager.Ping();
			}
			catch
			{
			    // ignored
			}
		}

        private static string ExtractResponseString(this WebException webException)
        {
            var responseStream = webException?.Response?.GetResponseStream() as MemoryStream;

            if (responseStream == null)
                return null;

            var responseBytes = responseStream.ToArray();

            var responseString = Encoding.UTF8.GetString(responseBytes);
            return responseString;
        }

		private static bool RepairConnection()
		{
			//пропускаем восстановление связи если приложение уже закрывается, больше обращений к сервису не требуется
			if (App.IsClosing) return false;
			var bError = false;
			try
			{
				if (_wcfClient.State == CommunicationState.Faulted)
					bError = true;
				else
				{
					//Simple call to check connection state
					_wcfClient.GetChannel().GetServiceVersion();
				}
			}
			catch (Exception ex)
			{
				bError = true;
				App.log.WriteException(ex, "Возникла ошибка при восстановлении соединения к WCF сервису.");
			}

		    if (_wcfClient.State == CommunicationState.Opened && !bError) return true;
		    if (ClientCredentials != null)
		        CreateClientByCredentials(true);
		    else return false;

		    return true;
		}

	    internal static void CreatLightClient(string username)
	    {
			_wcfClient = ServiceSystem.CreateNewDataClient(AuthType);
            _wcfClient.ClientCredentials.Windows.ClientCredential.UserName = ClientCredentials.Windows.ClientCredential.UserName;
            var behavior = _wcfClient.Endpoint.Behaviors.Find<IDB2Connector>();
            if (behavior == null)
                _wcfClient.Endpoint.Behaviors.Add(new MessageCompressionAttribute(Compress.Reply | Compress.Request));
            _wcfClient.ClientCredentials.SupportInteractive = false;
            _wcfClient.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode =
                System.ServiceModel.Security.X509CertificateValidationMode.None;
			_wcfClient.Open();
        }


        internal static void CloseLightClient()
        {
            _wcfClient.Close();
            _wcfClient = null;
        }

        public static void CreateClientByCredentials(bool pBRestore = false, string sessionId = null)
		{
			PingTimer.Stop();


	        if (App.ShowMessageBoxOnError)
			    Loading.ShowWindow(!pBRestore ? "Авторизация пользователя..." : "Восстановление соединения...");

			if (AppSettings.IsAuthDebugEnabled)
				App.log.WriteLine("Before create new data client");
		    try
		    {
		        _wcfClient = ServiceSystem.CreateNewDataClient(AuthType);

		        if (ClientCredentials != null)
		        {
		            if (AppSettings.IsAuthDebugEnabled)
		                App.log.WriteLine("ClientCredentials != null");
		            _wcfClient.ClientCredentials.Windows.ClientCredential.UserName = ClientCredentials.Windows.ClientCredential.UserName;
		            _wcfClient.ClientCredentials.Windows.ClientCredential.Password = ClientCredentials.Windows.ClientCredential.Password;
		            _wcfClient.ClientCredentials.Windows.AllowedImpersonationLevel = ClientCredentials.Windows.AllowedImpersonationLevel;

		            if (AppSettings.IsAuthDebugEnabled)
		                App.log.WriteLine("Before find endpoint");
		            var behavior = _wcfClient.Endpoint.Behaviors.Find<IDB2Connector>();
		            if (behavior == null)
		            {
		                if (AppSettings.IsAuthDebugEnabled)
		                    App.log.WriteLine("FOUND endpoint!");
		                _wcfClient.Endpoint.Behaviors.Add(new MessageCompressionAttribute(Compress.Reply | Compress.Request));
		            }

		            // Скрытие системного окна аутентификации при неверном логине/пароле
		            _wcfClient.ClientCredentials.SupportInteractive = false;
		            _wcfClient.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode =
		                System.ServiceModel.Security.X509CertificateValidationMode.None;
		            if (AppSettings.IsAuthDebugEnabled)
		                App.log.WriteLine("Before client open");
		            _wcfClient.Open();
		            if (AppSettings.IsAuthDebugEnabled)
		                App.log.WriteLine("Before assign credentials");
		            ClientCredentials = CurrentClientCredentials;

		            // устанавливаем некоторые параметры журналирования.
		            Client.SetCurrentUserInformation(
		                ClientCredentials.Windows.ClientCredential.UserName,
		                Environment.UserName,
		                Environment.MachineName);
		            try
		            {
		                if (App.ShowMessageBoxOnError)
		                    Loading.SetPublicMessage("Инициализация AUTH коннектора...");
		                if (AppSettings.IsAuthDebugEnabled)
		                    App.log.WriteLine("AUTH: Start init ");

		                var iresult = Client.InitService(new object[]
		                {
		                    ClientCredentials.Windows.ClientCredential.UserName,
		                    ClientCredentials.Windows.ClientCredential.Password,
		                    sessionId
		                });
		                if (iresult.Response != AuthMsg.Success)
		                    throw iresult.Response == AuthMsg.UserMustChangePassword
		                        ? (Exception) new UserMustChangePasswordException((string) iresult.ResultValue)
		                        : new InvalidLoginOrPasswordException((string) iresult.ResultValue);


		                AP.Provider = new AuthProvider(ClientCredentials.Windows.ClientCredential.UserName);

		                if (AppSettings.IsAuthDebugEnabled)
		                {
		                    var rstring = AP.Provider.CurrentUserSecurity.GetRoles().Aggregate(string.Empty, (current, tmp) => current + ("|" + tmp));
		                    App.log.WriteLine($"Role: {DOKIP_ROLE_TYPE.User} in   {rstring}");
		                }

		                if (!AP.Provider.CurrentUserSecurity.CheckUserInRole(DOKIP_ROLE_TYPE.User))
		                {
		                    if (AppSettings.IsAuthDebugEnabled)
		                        App.log.WriteLine("User exception!");
		                    throw new NotUserException();
		                }

		                if (AppSettings.IsAuthDebugEnabled) App.log.WriteLine("AUTH: CheckUserInRole() success");
		            }
		            catch (NotUserException)
		            {
		                throw;
		            }
		            catch (Exception e)
		            {
                        App.log.WriteLine(e.ToString());
                        throw new AuthInitException {Ex = e};
                    }
		        }

		        PingTimer.Start();
		        _wcfClient.InnerChannel.Faulted += (sender, e) =>
		        {
		            if (App.IsClosing) return;
		            var state = _wcfClient.State;
		            App.log.WriteLine($"Возникло исключение при работе с WCF сервисом. Состояние канала сервиса {state.ToString()}. Производится попытка переинициализировать канал.");
		            RepairConnection();
		        };
		    }
		    catch (Exception ex)
		    {
                App.log.WriteException(ex, "Connection error!");
		    }
		    finally
		    {
		        Loading.CloseWindow();
		    }
		}


		
		public static IDB2Connector Client
		{
			get
			{
				if (_wcfClient == null && App.IsClosing) return null;
				if (!UseAutoRepair) return _wcfClient?.GetChannel();
				if (ClientCredentials == null) return _wcfClient?.GetChannel();
				UseAutoRepair = false;
				try
				{
					RepairConnection();
				}
				finally
				{
					UseAutoRepair = true;
				}
				return _wcfClient?.GetChannel();
			}
		}

		public static IDB2Connector GetDataServiceFunc()
		{
			return Client;
		}

		public static void CloseClient()
		{
			try
			{
				if (_wcfClient.State != CommunicationState.Opened) return;
				// WCFClient.Client.CloseDB2Connection();
				// WCFClient.wcfClient.Close();
				//сервис закроет канал и тут будет исключение, пропускаем его
				try
				{
					Client.CloseConnection();
				}
				catch
				{
				    // ignored
				}
			    App.log.WriteLine("Канал к WCF Сервису был закрыт успешно!");
			}
			catch (Exception ex)
			{
				App.log.WriteLine("Возникла ошибка при попытке закрыть канал к WCF сервису.");
				App.log.WriteException(ex);
			}

		}

		//static void WCFClient_ConnectionRestoredEvent()
		//{
		//    if (AP.Provider != null)
		//    {
		//        try
		//        {
		//            AP.Provider.RefreshUserSecurity();
		//        }
		//        catch (Exception ex) {
		//            App.log.WriteException(ex);
		//        }
		//    }
		//}

		/// <summary>
		/// Установить пароль пользователя после сброса администратором
		/// </summary>
		/// <param name="password">Новый пароль</param>
		/// <param name="oldPassword">Старый пароль (опционально, если указан будет смена пароля, а не установка)</param>
        public static ServiceAuthMsg SetUserPassword(string password, string oldPassword = null)
		{
            //PermissiveCertificatePolicy.Enact("CN=ServiceModelSamples-HTTPS-Server");
            //App.log.WriteLine(string.Format("U: {0} D: {1} P: {2}", ClientCredentials.Windows.ClientCredential.UserName, password, ClientCredentials.Windows.ClientCredential.Domain));
            ServiceAuthMsg result = null;
		    if (!string.IsNullOrEmpty(oldPassword))
		    {
		        result = Client.ChangeCurrentUserPassword(
		            ClientCredentials.Windows.ClientCredential.UserName,
		            oldPassword,
		            password);

		        if (result.Response == AuthMsg.Success)
		            ClientCredentials.Windows.ClientCredential.Password = password;
		    }
		    return result;
		}
	}
}
