﻿using System;
using System.Windows.Data;
using System.Windows;
using System.Globalization;

namespace PFR_INVEST.Helpers
{
    [ValueConversion(typeof(String), typeof(String))]
    public class StringFormatConverter : DependencyObject, IValueConverter
    {
        public static readonly DependencyProperty FormatStringProperty =
          DependencyProperty.Register("StringFormat", typeof(String),
          typeof(StringFormatConverter));

        public String StringFormat
        {
            get
            {
                return (String)GetValue(FormatStringProperty);
            }
            set
            {
                SetValue(FormatStringProperty, value);
            }
        }
        public static readonly DependencyProperty CultureProperty =
         DependencyProperty.Register("CultureName", typeof(String),
         typeof(StringFormatConverter));

        public String CultureName
        {
            get
            {
                return (String)GetValue(CultureProperty);
            }
            set
            {
                SetValue(CultureProperty, value);
            }
        }

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            System.Xml.XmlAttribute attribute;
            CultureInfo _culture;
            double doubleValue;

            if ((attribute = value as System.Xml.XmlAttribute) != null)
                value = attribute.Value;

            if (String.IsNullOrEmpty(CultureName))
                _culture = culture;
            else
                _culture = new CultureInfo(CultureName);

            if (value != null && double.TryParse(value.ToString(), out doubleValue))

                return String.Format(_culture, StringFormat, doubleValue);

            return String.Format(_culture, StringFormat, value);
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            if (value == null)
                return null;

            return value.ToString();
        }
        #endregion
    }

    [ValueConversion(typeof(String), typeof(String))]
    public class EmptySummConverter : DependencyObject, IValueConverter
    {

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            System.Xml.XmlAttribute attribute;
            double doubleValue;

            if ((attribute = value as System.Xml.XmlAttribute) != null)
                value = attribute.Value;

            if (value != null && double.TryParse(value.ToString(), out doubleValue))
                if (doubleValue == 0)
                    return null;
                else return value;
            if (value == null || string.IsNullOrEmpty(value.ToString())) return null;

            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            if (value == null)
                return null;

            return value.ToString();
        }
        #endregion
    }
}