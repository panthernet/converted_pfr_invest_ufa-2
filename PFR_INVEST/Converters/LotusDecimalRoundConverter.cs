﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace PFR_INVEST.Converters
{

    /// <summary>
    /// Округление для контролов как в лотусе. 
    /// К меньшему при 0.5
    /// Разрядность 2 знака
    /// </summary>
    public class LotusDecimalRoundConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is decimal)
            {
                var original = (decimal)value;

                var round = Math.Floor(original * 100 + 0.499999M) / 100;//как в лотусе (К меньшему при 0.5)
                return round;
            }
            else
                return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }
}
