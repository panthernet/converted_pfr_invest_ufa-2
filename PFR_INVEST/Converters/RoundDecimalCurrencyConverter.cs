﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace PFR_INVEST.Converters
{
    public class RoundDecimalCurrencyConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is decimal)
            {
                return Math.Round((decimal)value, 2, MidpointRounding.ToEven);
            }
            if (value is string)
            {
                decimal castedValue;

                if (decimal.TryParse((string)value, NumberStyles.Any, new CultureInfo("ru-RU")//CultureInfo.InvariantCulture
                    , out castedValue))
                {
                    return Math.Round(castedValue, 2, MidpointRounding.ToEven);
                }
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
