﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Converters
{

    public class CBReportApproveToGeometryConverter: IValueConverter
    {
        public static Geometry OkImage =
            Geometry.Parse("M12,2A10,10 0 0,1 22,12A10,10 0 0,1 12,22A10,10 0 0,1 2,12A10,10 0 0,1 12,2M11,16.5L18,9.5L16.59,8.09L11,13.67L7.91,10.59L6.5,12L11,16.5Z");

        public static Geometry FailImage =
            Geometry.Parse(
                "M12,2C17.53,2 22,6.47 22,12C22,17.53 17.53,22 12,22C6.47,22 2,17.53 2,12C2,6.47 6.47,2 12,2M15.59,7L12,10.59L8.41,7L7,8.41L10.59,12L7,15.59L8.41,17L12,13.41L15.59,17L17,15.59L13.41,12L17,8.41L15.59,7Z");

        public static Geometry InProcessImage = Geometry.Parse("M17,13H7V11H17M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2Z");

        static CBReportApproveToGeometryConverter()
        {
            OkImage.Freeze();
            FailImage.Freeze();
            InProcessImage.Freeze();
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is int == false) return null;
            switch ((int)value)
            {
                case 0:
                    return FailImage;
                case 1:
                    return OkImage;
                case 2:
                    return InProcessImage;
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }

    public class CBReportApproveToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is int == false) return null;
            switch ((int)value)
            {
                case 0:
                    return Brushes.Red;
                case 1:
                    return Brushes.LimeGreen;
                case 2:
                    return Brushes.DarkGray;
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }


    public class AuctionStepToGeometryConverter : IValueConverter
    {
        public static Geometry StartImage =
            Geometry.Parse("M12,22A10,10 0 0,1 2,12A10,10 0 0,1 12,2A10,10 0 0,1 22,12A10,10 0 0,1 12,22M17,14L12,9L7,14H17Z");

        public static Geometry CompleteImage =
            Geometry.Parse(
                "M10,17L5,12L6.41,10.58L10,14.17L17.59,6.58L19,8M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2Z");

        public static Geometry InProcessImage = Geometry.Parse("M17,13H7V11H17M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2Z");

        static AuctionStepToGeometryConverter()
        {
            StartImage.Freeze();
            CompleteImage.Freeze();
            InProcessImage.Freeze();
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is int == false) return null;
            switch ((int)value)
            {
                case 0:
                    return StartImage;
                case 2:
                    return CompleteImage;
                default:
                    return InProcessImage;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }

    public class AuctionStepToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is int == false) return null;
            switch ((int)value)
            {
                case 0:
                    return Brushes.DeepSkyBlue;
                case 2:
                    return Brushes.Green;
                default:
                    return Brushes.DarkGray;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
