﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Markup;
using System.Windows.Data;

namespace PFR_INVEST.Converters
{

    public class IsTextMatchConverter : MarkupExtension, IValueConverter, IMultiValueConverter
    {
        public IsTextMatchConverter() { }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var str = value as string;
            var par = parameter as string;
            if (string.IsNullOrWhiteSpace(str) || string.IsNullOrWhiteSpace(par))
                return false;
            else if (str.ToUpper().Contains(par.ToUpper()))
                return true;
            else
                return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values.Length < 2)
                throw new NotSupportedException();

            var str = values[0] as string;
            var par = values[1] as string;
            if (string.IsNullOrWhiteSpace(str) || string.IsNullOrWhiteSpace(par))
                return false;
            //Для уменьшения шума
            if (par.Length < 3)
                return false;
            else if (str.ToUpper().Contains(par.ToUpper()))
                return true;
            else
                return false;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
