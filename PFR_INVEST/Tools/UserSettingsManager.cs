﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Xml.Linq;
using DevExpress.Xpf.Core;
using PFR_INVEST.SettingsService.Contract;

namespace PFR_INVEST.Tools
{
    public enum ContainerType
    {
        None,
        View,
        Dialog
    }

    public class UserSettingsManager
    {
        private const string rootElementName = "userSettings";
        private const string viewsElementName = "views";
        private const string viewElementName = "view";
        private const string viewTypeAttributeName = "type";
        private const string viewNameAttributeName = "name";
        private const string viewScaleXName = "scaleX";
        private const string viewScaleYName = "scaleY";
        private const string viewWidthName = "width";
        private const string viewHeightName = "height";
        private const string viewPointXName = "pointX";
        private const string viewPointYName = "pointY";
        private const string gridsElementName = "grids";
        private const string gridElementName = "grid";
        private const string gridNameAttributeName = "name";
        private const string clientThemeElementName = "clientTheme";
        private const string layoutElementName = "layout";
        const double DEFAULT = -1;
        //const double DEFAULT_WIDTH = 800;
        //const double DEFAULT_HEIGHT = 600;

        public bool IsEnabled { get; private set; }

        public UserSettingsManager(bool isEnabled)
        {
            IsEnabled = isEnabled;
        }

        public static string EndPointConfigName = "WS2007HttpBinding_ISettingsService";
        private UserSettingsServiceClient m_SettingsService;
        protected UserSettingsServiceClient SettingsService
        {
            get
            {
                bool needReconnect = false;
                try
                {
                    Ping();
                }
                catch
                {
                    needReconnect = true;
                }

                if (m_SettingsService == null || needReconnect)
                    m_SettingsService = new UserSettingsServiceClient(EndPointConfigName);
                return m_SettingsService;
            }
        }

        private static XDocument settingsDocument;

        public UserSetting GetUserSetting(ContainerType containerType, string containerName)
        {
            if (!IsEnabled) return null;
            var rootElement = settingsDocument.Element(rootElementName);

            if (rootElement == null)
                return null;

            var viewsParentElement = rootElement.Element(viewsElementName);

            if (viewsParentElement == null)
                return null;

            var viewElement =
                (from elm in viewsParentElement.Elements(viewElementName)
                 where (int)elm.Attribute(viewTypeAttributeName) == (int)containerType
                       && (string)elm.Attribute(viewNameAttributeName) == containerName
                 select elm).FirstOrDefault() ?? viewsParentElement.Element(viewElementName);

            if (viewElement == null)
                return null;

            var scaleX = GetDoubleFromXml(viewElement, viewScaleXName, 1.0);
            var scaleY = GetDoubleFromXml(viewElement, viewScaleYName, 1.0);
            var width = GetDoubleFromXml(viewElement, viewWidthName, DEFAULT);
            var height = GetDoubleFromXml(viewElement, viewHeightName, DEFAULT);
            var pointX = GetDoubleFromXml(viewElement, viewPointXName, DEFAULT);
            var pointY = GetDoubleFromXml(viewElement, viewPointYName, DEFAULT);

            var emptySize = (width == DEFAULT || height == DEFAULT) || (Double.IsInfinity(width) || Double.IsInfinity(height));
            var emptyPoint = (pointX == 0.0 || pointY == 0.0);

            var gridsElement = viewElement.Element(gridsElementName);

            if (gridsElement == null)
                return null;

            var result = new UserSetting
                             {
                                 PanelName = containerName,
                                 PanelType = containerType,
                                 ScaleX = scaleX,
                                 ScaleY = scaleY,
                                 Size = emptySize ? Size.Empty : new Size(width, height),
                                 Point = emptyPoint ? new Point() : new Point(pointX, pointY)
                             };

            foreach (var gridElement in gridsElement.Elements(gridElementName))
            {
                result.GridSettingsList.Add((string)gridElement.Attribute(gridNameAttributeName), gridElement.FirstNode.ToString());
            }

            return result;
        }

        private double GetDoubleFromXml(XElement viewElement, string viewName, double defaultValue)
        {
            var viewChildElement = viewElement.Element(viewName);
            var culture = CultureInfo.CreateSpecificCulture("ru-RU");

            double value;
            if (viewChildElement == null || !double.TryParse(viewChildElement.FirstNode.ToString(), NumberStyles.Float, culture, out value))
                value = defaultValue;
            return value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userSetting"></param>
        public void AppendSetting(UserSetting userSetting)
        {
            if (!IsEnabled) return;
            var rootElement = settingsDocument.Element(rootElementName)
                              ?? new XElement(rootElementName);

            var viewsParentElement = rootElement.Element(viewsElementName);

            if (viewsParentElement == null)
            {
                viewsParentElement = new XElement(viewsElementName);
                rootElement.Add(viewsParentElement);
            }

            var viewElementList =
                from elm in viewsParentElement.Elements(viewElementName)
                where (int)elm.Attribute(viewTypeAttributeName) == (int)userSetting.PanelType
                      && (string)elm.Attribute(viewNameAttributeName) == userSetting.PanelName
                select elm;

            var viewElement = viewElementList.FirstOrDefault();

            var culture = CultureInfo.CreateSpecificCulture("ru-RU");

            var newViewElement = new XElement(viewElementName,
                                              new XAttribute(viewTypeAttributeName, (int)userSetting.PanelType),
                                              new XAttribute(viewNameAttributeName, userSetting.PanelName),
                                              new XElement(viewScaleXName, userSetting.ScaleX.ToString(culture)),
                                              new XElement(viewScaleYName, userSetting.ScaleY.ToString(culture)),
                                              new XElement(viewWidthName, userSetting.Size.Width.ToString(culture)),
                                              new XElement(viewHeightName, userSetting.Size.Height.ToString(culture)),
                                              new XElement(viewPointXName, userSetting.Point.X.ToString(culture)),
                                              new XElement(viewPointYName, userSetting.Point.Y.ToString(culture))
                );

            var gridsElement = new XElement(gridsElementName);
            newViewElement.Add(gridsElement);

            foreach (var gridData in userSetting.GridSettingsList)
            {
                gridsElement.Add(
                    new XElement(gridElementName,
                                 XElement.Parse(gridData.Value),
                                 new XAttribute(gridNameAttributeName, gridData.Key))
                    );
            }

            if (viewElement == null)
                viewsParentElement.Add(newViewElement);
            else
                viewElement.ReplaceWith(newViewElement);
        }

        public void SetThemeIndex(int clientThemeIndex)
        {
            if (!IsEnabled) return;
            var rootElement = settingsDocument.Element(rootElementName)
                              ?? new XElement(rootElementName);

            var clientThemeElement = rootElement.Element(clientThemeElementName);

            var newClientThemeElement =
                new XElement(clientThemeElementName, clientThemeIndex);

            if (clientThemeElement == null)
            {
                rootElement.Add(clientThemeElement);
            }
            else
                clientThemeElement.ReplaceWith(newClientThemeElement);
        }

        public int GetThemeIndex()
        {
            if (!IsEnabled)
                return -1;

            if (settingsDocument == null)
                return -1;

            var rootElement = settingsDocument.Element(rootElementName)
                              ?? new XElement(rootElementName);

            var clientThemeElement = rootElement.Element(clientThemeElementName);

            int themeIndex;
            if (clientThemeElement == null || !int.TryParse(clientThemeElement.Value, out themeIndex))
                themeIndex = -1;

            return themeIndex;
        }

        public Stream GetDockLayoutManagerData()
        {
            if (!IsEnabled) return null;
            var rootElement = settingsDocument.Element(rootElementName)
                              ?? new XElement(rootElementName);

            var layoutElement = rootElement.Element(layoutElementName);

            if (layoutElement == null || !layoutElement.HasElements)
                return null;

            var memoryStream = new MemoryStream();

            var resultString = layoutElement.FirstNode.ToString();

            if (string.IsNullOrEmpty(resultString))
                return null;

            var buffer = Encoding.UTF8.GetBytes(resultString);

            foreach (byte b in buffer)
            {
                memoryStream.WriteByte(b);
            }

            memoryStream.Seek(0, SeekOrigin.Begin);

            return memoryStream;
        }

        public void SetDockLayoutManagerData(Stream stream)
        {
            if (!IsEnabled) return;
            string layoutData;

            if (stream.Position > 0)
                stream.Seek(0, SeekOrigin.Begin);

            using (var sr = new StreamReader(stream))
            {
                layoutData = sr.ReadToEnd();
            }

            if (string.IsNullOrEmpty(layoutData))
                return;

            var newLayoutElement = new XElement(
                layoutElementName,
                XElement.Parse(layoutData)
                );

            var rootElement = settingsDocument.Element(rootElementName)
                              ?? new XElement(rootElementName);

            var layoutElement = rootElement.Element(layoutElementName);

            if (layoutElement == null)
            {
                layoutElement = newLayoutElement;
                rootElement.Add(layoutElement);
            }
            else
                layoutElement.ReplaceWith(newLayoutElement);
        }

        public void Initialize()
        {
            if (!IsEnabled)
                return;

            try
            {
                CreateEmptyDocument();
                var userSettings = SettingsService.Client.GetUserSettings(
                WCFClient.CurrentClientCredentials.Windows.ClientCredential.Domain,
                WCFClient.CurrentClientCredentials.Windows.ClientCredential.UserName);

                if (string.IsNullOrEmpty(userSettings.Settings))
                    CreateEmptyDocument();
                else
                    settingsDocument = XDocument.Parse(userSettings.Settings);
            }
            catch
            {
                //var error = SettingsService.GetChannel().GetLastError();
                DXMessageBox.Show(
                    "Ошибка при получении пользовательских настроек. Сервис временно недоступен.",
                    "Настройки пользователя", MessageBoxButton.OK, MessageBoxImage.Information);
                CreateEmptyDocument();
            }
        }

        private void CreateEmptyDocument()
        {
            settingsDocument = new XDocument(
                    new XDeclaration("1.0", "utf-8", "yes"),
                    new XElement(rootElementName,
                        new XElement(clientThemeElementName),
                        new XElement(layoutElementName),
                        new XElement(viewsElementName))
                    );
        }

        public void Ping()
        {
            if (!IsEnabled)
                return;

            m_SettingsService?.Client.Ping();
        }

        public void Save()
        {
            if (!IsEnabled)
                return;

            try
            {
                if (settingsDocument != null && !SettingsService.Client.SetUserSettings(
                    new UserSettings
                        {
                            Domain = WCFClient.CurrentClientCredentials.Windows.ClientCredential.Domain,
                            UserName = WCFClient.CurrentClientCredentials.Windows.ClientCredential.UserName,
                            Settings = settingsDocument.ToString()
                        }))
                {
                    var error = SettingsService.Client.GetLastError();
                    DXMessageBox.Show(
                        string.Format("Ошибка при сохранении настроек пользователя. {0}", error),
                        "Настройки пользователя", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch// (Exception err)
            {
                DXMessageBox.Show(
                    "Ошибка при сохранении настроек пользователя. Сервис временно недоступен.",
                    "Настройки пользователя", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        public void Reset()
        {
            if (!IsEnabled) return;

            CreateEmptyDocument();

            for (int intCounter = App.Current.Windows.Count - 1; intCounter >= 0; intCounter--)
                ThemesTool.SetCurrentTheme(App.Current.Windows[intCounter]);
        }

        private UserSettingsSimple _UserSettings = null;
        public UserSettingsSimple UserSettings
        {
            get
            {
                if (this._UserSettings == null)                
                    this._UserSettings = new UserSettingsSimple();
                
                return this._UserSettings;
            }
        }
    }


}
