﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Editors;
using DevExpress.Xpf.Xmas.Features;
using DXCommonThemes;
using PFR_INVEST.Core.BusinessLogic;

namespace PFR_INVEST.Tools
{
    public static class ThemesTool
    {

        public static void SetCurrentTheme(DependencyObject window)
        {
            var themeNum = GetActualTheme();
            ThemeManager.SetThemeName(window, themeNum >= ThemeItem.ThemesList.Count ? ThemeItem.DEFAULT_THEME : ThemeItem.ThemesList[themeNum].StringId);
        }

        public static void ApplyGlobalTheme(int index)
        {
            ThemeManager.ApplicationThemeName = index >= ThemeItem.ThemesList.Count ? ThemeItem.DEFAULT_THEME : ThemeItem.ThemesList[index].StringId;

            for (var intCounter = App.Current.Windows.Count - 1; intCounter >= 0; intCounter--)
            {
                SetCurrentTheme(App.Current.Windows[intCounter]);
            }
        }

        public static void ApplyGlobalTheme()
        {
            ApplyGlobalTheme(GetActualTheme());
        }

        /// <summary>
        /// Возвращает номер выбранной цветовой гаммы, учитывая доступность сервиса настроек
        /// </summary>
        /// <returns></returns>
        public static int GetActualTheme()
        {
            int sindex = App.UserSettingsManager.GetThemeIndex();
			int mindex = Core.Properties.Settings.Default.ClientTheme;

            return sindex == -1 ? mindex : sindex;
        }

        #region CUSTOM THEMES

        public static void UpdateLogo(Image splashImage)
        {
            var index = Core.Properties.Settings.Default.ClientTheme;
            var theme = index >= ThemeItem.ThemesList.Count ? ThemeItem.DEFAULT_THEME : ThemeItem.ThemesList[index].StringId;
            switch (theme)
            {
                case "Xmas": //Xmas
                    splashImage.Width = 472;
                    splashImage.Height = 300;
                    splashImage.Source = new BitmapImage(new Uri("/DevExpress.Xpf.Themes.Xmas.v13.1;component/Images/splash.png", UriKind.RelativeOrAbsolute)) { CacheOption = BitmapCacheOption.OnLoad };
                    break;
            }
        }

        public static IDXThemeFeature UpdateFeature(Canvas featCanvas)
        {
            var index = Core.Properties.Settings.Default.ClientTheme;
            var theme = index >= ThemeItem.ThemesList.Count ? ThemeItem.DEFAULT_THEME : ThemeItem.ThemesList[index].StringId;
            switch (theme)
            {
                case "Xmas": //Xmas
                    return new SnowGenerator(featCanvas);
                default:
                    return null;
            }
        }

        static ThemesTool()
        {
            //регистрация новых нестандартных тем
            Theme.RegisterTheme(new Theme("Xmas", "DevExpress.Xpf.Themes.Xmas.v13.1") { AssemblyName = "DevExpress.Xpf.Themes.Xmas.v13.1" });
        }

        public static void UpdateLoadingIndicator(Canvas canvas, Image image)
        {
            var index = Core.Properties.Settings.Default.ClientTheme;
            var theme = index >= ThemeItem.ThemesList.Count ? ThemeItem.DEFAULT_THEME : ThemeItem.ThemesList[index].StringId;
            switch (theme)
            {
                case "Xmas": //Xmas
                    if (canvas != null)
                    {
                        canvas.Children.Clear();
                        var bitmap = new BitmapImage(new Uri("/DevExpress.Xpf.Themes.Xmas.v13.1;component/Images/snowflake2.png", UriKind.RelativeOrAbsolute)) { CacheOption = BitmapCacheOption.OnLoad };
                        var iWidth = 15;
                        var iHeight = 15;
                        for (int i = 0; i < 9; i++)
                            canvas.Children.Add(new Image { Source = bitmap, Width = iWidth, Height = iHeight, RenderTransform = new ScaleTransform(0, 0, 7.5, 7.5) });
                    }
                    if (image != null)
                        image.Source = new BitmapImage(new Uri("/DevExpress.Xpf.Themes.Xmas.v13.1;component/Images/globe2.png", UriKind.RelativeOrAbsolute)) { CacheOption = BitmapCacheOption.OnLoad };
                    break;
            }
        }

        public static void UpdateLoadingTextTheme(Border txtMessageBorder, Label txtMessage)
        {
            var index = Core.Properties.Settings.Default.ClientTheme;
            var theme = index >= ThemeItem.ThemesList.Count ? ThemeItem.DEFAULT_THEME : ThemeItem.ThemesList[index].StringId;
            switch (theme)
            {
                case "Xmas": //Xmas
                    txtMessageBorder.Background = Brushes.LightBlue;
                    txtMessageBorder.BorderThickness = new Thickness(1);
                    txtMessageBorder.BorderBrush = Brushes.DeepSkyBlue;
                    return;
            }
        }


        public static void Ping()
        {
            //needed to load the custom themes on startup
        }

        #endregion


    }
}
