﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PFR_INVEST.Tools
{
    public class UserSettingsSimple
    {
        private Dictionary<string, object> Settings { get; set; }

        public UserSettingsSimple()
        {
            this.Settings = new Dictionary<string, object>();
            this.Load();
        }
                
        public T Get<T>(string key)
        {
            return (T)(this.Get(key) ?? default(T));
        }

        public object Get(string key)
        {
            if (this.Settings.ContainsKey(key))
                return this.Settings[key];
            return null;
        }

        public void Set(string key, object value)
        {
            this.Set(key, value, false);
        }

        public void Set(string key, object value, bool saveImmediate)
        {
            this.Settings[key] = value;
            if (saveImmediate)
                this.Save();
        }


        public void Load()
        {
#warning Загрузка временно не реализована;
        }
        public void Save()
        {
#warning Сохранение временно не реализовано;
        }
    }
}
