﻿using System;
using System.Collections.Generic;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataAccess.Client;

namespace PFR_INVEST.Tools
{
    /// <summary>
    /// Пользовательские настройки для бизнес-логики, хранятся в базе
    /// Заготовка, пока используется только для сообщений об окончании даты доверености
    /// </summary>
    public class BlUserSettings
    {
        private static BlUserSettings _current;
        public static BlUserSettings Current
        {
            get { return _current ?? (_current = new BlUserSettings()); }
        }

        private IList<BlUserSetting> Settings { get;  set; }
       
        public void Load()
        {
            try
            {
                Settings = DataContainerFacade.GetBlUserSettings();
            }
            catch (Exception ex)
            {
                App.log.WriteException(ex, "ОШИБКА при попытке считывания настроек пользователя с сервиса настроек!");
                Settings = new List<BlUserSetting>();
            }
        }
    }
}
