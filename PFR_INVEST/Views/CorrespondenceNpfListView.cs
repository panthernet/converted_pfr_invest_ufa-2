﻿using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.ViewModelsCard;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.Views
{
    public class CorrespondenceNpfListView : CorrespondenceListView
    {
        public CorrespondenceNpfListView()
        {
            c8.Visible = false;
        }

        protected override void OpenEdit(CorrespondenceListItemNew item)
        {
            if (item.IsAttach)
                App.DashboardManager.OpenNewTab<LinkedDocumentView, LinkedDocumentNpfViewModel>(CorrespondenceListItem.s_AttachDoc, ViewModelState.Edit, item.ID, ViewModelState.Edit);
            else
                App.DashboardManager.OpenNewTab<DocumentNpfView, DocumentNpfViewModel>(CorrespondenceListItem.s_Doc, ViewModelState.Edit, item.ID, ViewModelState.Edit);
        }
    }
}
