﻿using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for INDateListView.xaml
    /// </summary>
    public partial class YieldOFZListView
    {
        public YieldOFZListViewModel VM => DataContext as YieldOFZListViewModel;
        public YieldOFZListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, BDateGrid);
            Loaded += (sender, args) =>
            {
                VM.OnDataRefreshing += (o, eventArgs) =>
                {
                    BDateGrid.RefreshData();
                };
            };
        }
    }
}
