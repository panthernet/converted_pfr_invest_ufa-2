﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for DepclaimMaxListView.xaml
    /// </summary>
    public partial class DepClaim2OfferListView : UserControl
    {
        private DepClaim2OfferListViewModel Model => DataContext as DepClaim2OfferListViewModel;

        public DepClaim2OfferListView()
        {
            InitializeComponent();
            Grid.Loaded += Grid_Loaded;
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            if (Model != null)
                Model.OnSelectGridRowDelegate += Model_OnSelectGridRowDelegate;
        }

        private void Model_OnSelectGridRowDelegate(long rid)
        {
            if (rid == 0 || Grid.VisibleRowCount == 0)
                return;

            try
            {
                int rowHandle = Grid.FindRowByValue("Offer.ID", rid);

                var handles = new List<int>();

                while (Grid.IsValidRowHandle(rowHandle))
                {
                    handles.Add(rowHandle);
                    rowHandle = Grid.GetParentRowHandle(rowHandle);
                }

                if (handles.Count > 1)
                {
                    Grid.View.FocusedRowHandle = handles[handles.Count - 2];
                }
            }
            catch (Exception ex)
            {
                App.log.WriteException(ex);
            }
        }

        private void tableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {            
            var row = tableView.FocusedRow as DepClaimOfferListItem;
            if (row != null)
            {
                App.DashboardManager.OpenNewTab(typeof(DepClaimOfferView), ViewModelState.Edit, row.Offer.ID, false, "Оферта");
            }
        }

        private void tableView_FocusedRowHandleChanged(object sender, FocusedRowHandleChangedEventArgs e)
        {
            {
                var x = tableView.FocusedRowData.Row as DepClaimOfferListItem;

                if (x != null)
                {
                    SelectedAuctionIdHelper.Value = x.Auction.ID;
                    SelectedAuctionHelper.Value = x.Auction;
                }
                else
                {
                    SelectedAuctionIdHelper.Value = null;
                    SelectedAuctionHelper.Value = null;
                }
            }

            {
                var x = tableView.FocusedRow as DepClaimOfferListItem;

                if (x != null)
                {
                    SelectedOfferHelper.Value = x.Offer.ID;
                }
                else
                    SelectedOfferHelper.Value = null;
            }
        }
    }
}
