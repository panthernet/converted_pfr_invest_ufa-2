﻿using System.Windows.Controls;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for FinregisterPropsInputView.xaml
    /// </summary>
    public partial class FinregisterPropsInputView : UserControl
    {
        public FinregisterPropsInputView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }
    }
}
