﻿using System.Windows.Controls;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for DopSPNView.xaml
    /// </summary>
    public partial class PrecisePenaltyView : UserControl
    {
        public PrecisePenaltyView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }
    }
}
