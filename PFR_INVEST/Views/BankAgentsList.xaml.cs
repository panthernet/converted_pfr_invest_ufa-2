﻿using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for ContragentsList.xaml
    /// </summary>
    public partial class BankAgentsList
    {
        public BankAgentsList()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, npfListGrid);
        }

        private void tableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (npfListGrid.View.GetRowElementByMouseEventArgs(e) == null) return;

            if (npfListGrid.View.FocusedRowHandle >= 0)
            {
                var value = npfListGrid.GetCellValue(npfListGrid.View.FocusedRowHandle, "ID");

                App.DashboardManager.OpenNewTab(typeof(BankAgentView),
                                                ViewModelState.Edit,
                                                (long)
                                                value, "Банк-агент");
            }
            /* else
                    App.dashboardManager.OpenNewTab("Контрагент", typeof (ContragentView),
                                                    PFR_INVEST.VIEWMODEL_STATES.Edit,
                                                    (long)
                                                    this.npfListGrid.GetCellValue(
                                                        this.npfListGrid.View.FocusedRowHandle, "ContragentID"));*/
        }
    }
}
