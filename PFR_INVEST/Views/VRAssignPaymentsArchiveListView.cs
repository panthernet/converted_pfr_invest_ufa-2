﻿using System;
using System.Collections.Generic;
using DevExpress.Xpf.Grid;

namespace PFR_INVEST.Views
{
    public class VRAssignPaymentsArchiveListView : AssignPaymentsArchiveListView
    {
        private readonly long _initialRecordId;
        public VRAssignPaymentsArchiveListView(long recordId = 0)
        {
            if (recordId > 0) _initialRecordId = recordId;

            Grid.Columns.Add(new GridColumn()
            {
                Header = "Тип операции",
                FieldName = "OperationType",
                GroupIndex = 0
            });
            Loaded += VRAssignPaymentsArchiveListView_Loaded;
        }

        void VRAssignPaymentsArchiveListView_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            if (_initialRecordId < 1) return;
            Model_OnSelectGridRowDelegate(_initialRecordId);
        }

        protected override void Model_OnSelectGridRowDelegate(long rid)
        {
            if (rid == 0 || Grid.VisibleRowCount == 0)
                return;

            try
            {
                //int rowHandle = Grid.View.FocusedRowHandle + 1;
                int rowHandle = Grid.FindRowByValue("RegisterID", rid);

                var handles = new List<int>(7);
                while (Grid.IsValidRowHandle(rowHandle))
                {
                    handles.Add(rowHandle);
                    rowHandle = Grid.GetParentRowHandle(rowHandle);
                }
                //2 уровень вложения по групповым вкладкам
                Grid.View.FocusedRowHandle = handles.Count > 2 ? handles[handles.Count - 2] : rowHandle;
            }
            catch (Exception ex)
            {
                App.log.WriteException(ex);
            }
        }}
}
