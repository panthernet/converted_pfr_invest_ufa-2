﻿using System.Windows.Controls;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for F40View.xaml
    /// </summary>
    public partial class F40View : UserControl
    {
        public F40View()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }
    }
}
