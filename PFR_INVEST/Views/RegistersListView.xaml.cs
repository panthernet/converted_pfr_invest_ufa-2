﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using DevExpress.Data;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for FinRegistersListView.xaml
    /// </summary>
    public partial class RegistersListView
    {
        public RegistersListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, registersListGrid);
            DataContextChanged += RegistersListView_DataContextChanged;
            Loaded += RegistersListView_Loaded;
        }
        private int _currentGroupHandle = 0;
        private void RegistersListView_Loaded(object sender, RoutedEventArgs e)
        {
            //Сохранение и возобнавление фокуса строки в гриде с группировками
            Model.PropertyChanged += (a, b) =>
            {
                if (b.PropertyName.Equals("ListRefreshing"))
                {
                    var grid = registersListGrid;
                    _currentGroupHandle = grid.View.FocusedRowData.RowHandle.Value;
                }
                else if (b.PropertyName.Equals("ListRefreshed"))
                {
                    registersListGrid.View.FocusedRowHandle = _currentGroupHandle;
                }
            };
        }

        private void RegistersListView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var value = e.NewValue as IRequestCustomAction;
            if(value != null)
                value.RequestCustomAction += RegistersListView_RequestCustomAction;
        }

        private void RegistersListView_RequestCustomAction(object sender, EventArgs e)
        {
            var isFr = (bool) ((object[]) sender)[0];
            var id = (long)((object[])sender)[1];
            SelectRow(id, isFr);
        }

        private void SelectRow(long id, bool isFinregister = true)
        {
            try
            {
                var item = Model.List.FirstOrDefault(a => (isFinregister ? a.ID : a.RegisterID) == id);
                if (item == null) return;
                registersListGrid.ExpandFocusRow(registersListGrid.GetRowHandleByListIndex(Model.List.IndexOf(item)));                
            }
            catch (Exception ex)
            {
                App.log.WriteException(ex);
            }
        }

        public RegistersListViewModel Model => DataContext as RegistersListViewModel;

        public RegistersListItem SelectedFinRegister
        {
            get
            {
                var item = registersListGrid.GetFocusedRow() as RegistersListItem;
                return item?.RegisterKind != null && IsFinRegisterSelected() ? item : null;
            }
        }

        public RegistersListItem SelectedRegister
        {
            get
            {
                var item = registersListGrid.GetFocusedRow() as RegistersListItem;
                return item?.RegisterKind != null && IsRegisterSelected() ? item : null;
            }
        }

        private void grid_CustomSummaryExists(object sender, CustomSummaryExistEventArgs e)
        {
            e.Exists = e.GroupLevel == 2;
        }

        public long GetCurrFinRegID()
        {
            return Convert.ToInt64(registersListGrid.GetCellValue(registersListGrid.View.FocusedRowHandle, "ID"));
        }

        public bool CanCreateRequest()
        {
            if (!IsRegisterSelected()) return false;
            try
            {
                var regId = (long) registersListGrid.GetFocusedRowCellValue("RegisterID");
                var regList = new List<RegistersListItem>(from RegistersListItem rli in Model.List
                    where rli.RegisterID == regId
                    select rli);
                return regList.Count > 0 && regList.All(item => item != null && item.Count > 0);
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Если реестр содержит только финреестры в статусе Не передан
        /// </summary>
        public bool IsOnlyNotTransferred()
        {
            var regId = (long) registersListGrid.GetFocusedRowCellValue("RegisterID");
            var list = Model.List.Where(a => a.RegisterID == regId && a.ContragentID > 0).ToList();
            return list.All(a => RegisterIdentifier.FinregisterStatuses.IsNotTransferred(a.Status));
        }

        public string GetCurrFinregRegisterKind()
        {
            return Convert.ToString(registersListGrid.GetCellValue(registersListGrid.View.FocusedRowHandle, "RegisterKind"));
        }

        private void TableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (registersListGrid.View.GetRowElementByMouseEventArgs(e) == null) return;
            if (IsRegisterSelected())
            {
                var lID = GetSelectedRegisterID();
                if (lID > 0)
                    App.DashboardManager.OpenNewTab(typeof(RegisterView), ViewModelState.Edit, lID, "Реестр");
            }
            else if (IsFinRegisterSelected())
            {
                var lID = GetSelectedID();
                if (lID > 0)
                    App.DashboardManager.OpenNewTab(typeof(FinRegisterView), ViewModelState.Edit, lID, "Финреестр");
            }
        }

        public long GetSelectedRegisterID()
        {
            return Convert.ToInt64(registersListGrid.GetCellValue(registersListGrid.View.FocusedRowHandle, "RegisterID"));
        }

        public string GetSelectedRegisterKind()
        {
            return Convert.ToString(registersListGrid.GetCellValue(registersListGrid.View.FocusedRowHandle, "RegisterKind"));
        }
        public long GetSelectedID()
        {
            return Convert.ToInt64(registersListGrid.GetCellValue(registersListGrid.View.FocusedRowHandle, "ID"));
        }

        public bool IsRegisterSelected()
        {
            var lvl = registersListGrid.View.FocusedRowData.Level;
            if (lvl < registersListGrid.GetGroupedColumns().Count)
                return (registersListGrid.GetGroupedColumns()[lvl].FieldName == "RegisterID");
            return false;
        }

        public bool IsFinRegisterSelected()
        {
            var lvl = registersListGrid.View.FocusedRowData.Level;
            var gCols = registersListGrid.GetGroupedColumns();
            return lvl == gCols.Count && (registersListGrid.CurrentItem != null && ((RegistersListItem) registersListGrid.CurrentItem).ID != 0);
        }

		public RegistersListItem SelectedItem => registersListGrid.CurrentItem as RegistersListItem;


        private void registersListGrid_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            const string dispText = "{0} \tдокумент: {1}     \r\n\t№ {2} от: {3}";
            if (!registersListGrid.IsValidRowHandle(e.RowHandle))
                return;

            var currRow = (e.Row as RegistersListItem);
            if (currRow != null)
            {
                var columnName = e.Column.FieldName;
                if (string.IsNullOrEmpty(currRow.ContragentName) && (columnName == "ContragentName" || columnName == "Status"))
                {
                    e.DisplayText = RegistersListViewModel.EMPTY_REGISTER_TEXT;
                    return;
                } 
                
                if (columnName != "RegisterID") return;

                e.DisplayText = string.Format(dispText,
                    currRow.RegisterID > 0 ? currRow.RegisterID.ToString() : string.Empty,
                    !string.IsNullOrEmpty(currRow.ApproveDocName) ? currRow.ApproveDocName : string.Empty,
                    !string.IsNullOrEmpty(currRow.RegNum?.Trim()) ? currRow.RegNum.Trim() : string.Empty,
                    currRow.RegDate != null && currRow.RegDate != DateTime.MinValue ? currRow.RegDate.Value.ToShortDateString() : string.Empty
                    );
            } else
                e.DisplayText = string.Empty;
        }

        #region PagedLoad

        private void RaiseMessageOnFault(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Contains("IsFaulted") && Model.IsFaulted)
                DXMessageBox.Show("Произошла ошибка при загрузке страницы данных");
        }

        private void registersListGrid_Loaded(object sender, RoutedEventArgs e)
        {
            if (Model == null)
                return;
            Model.PropertyChanged += RaiseMessageOnFault;
        }

        #endregion
    }

    public class IntoToColorConverter : MarkupExtension, IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((bool)value)
                return Brushes.LightPink;
            return Brushes.White;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }

        #endregion

        public override object ProvideValue(System.IServiceProvider serviceProvider)
        {
            return this;
        }
    }

    public class IntConverter : MarkupExtension, IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || System.Convert.ToInt64(value) == 0) return null;
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || System.Convert.ToInt64(value) == 0) return null;
            else return (long?)System.Convert.ToInt64(value);
        }

        #endregion

        public override object ProvideValue(System.IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
