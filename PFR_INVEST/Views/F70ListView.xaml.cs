﻿using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for ContragentsList.xaml
    /// </summary>
    public partial class F70ListView
    {
        public F70ListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshData(this, f70Grid, "List");
            ModelInteractionHelper.SubscribeForPageLoadModel(this);
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, f70Grid);
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        private void tableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (f70Grid.View.GetRowElementByMouseEventArgs(e) == null)
                return;
            if (f70Grid.View.FocusedRowHandle < 0)
                return;
            object value = f70Grid.GetCellValue(f70Grid.View.FocusedRowHandle, "ID");

            App.DashboardManager.OpenNewTab(typeof(F70View),
                ViewModelState.Edit,
                (long)
                    value, "Отчёт о доходах");
        }

        public long GetSelectedID()
        {
            if (f70Grid.View.FocusedRowHandle <= -10000)
                return 0;
            if (f70Grid.View.FocusedRowHandle >= 0)
                return (long)f70Grid.GetCellValue(f70Grid.View.FocusedRowHandle, "ID");
            return 0;
        }
    }
}
