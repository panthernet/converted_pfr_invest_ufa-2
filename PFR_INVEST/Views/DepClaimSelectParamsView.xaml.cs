﻿using System;
using PFR_INVEST.BusinessLogic;
using DevExpress.Xpf.Editors;
using PFR_INVEST.Extensions;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for DepClaimSelectParamsView.xaml
    /// </summary>
    public partial class DepClaimSelectParamsView
    {
        private DepClaimSelectParamsViewModel Model => DataContext as DepClaimSelectParamsViewModel;

        public DepClaimSelectParamsView()
        {
            InitializeComponent();
        }

		private void UpdateReturnDate(object sender, EventArgs e)
		{
            try
            {
                Model.ReturnDate = Model.PlacementDate.AddDays(Model.Period);
            }
            catch (ArgumentOutOfRangeException)
            {
                Model.ReturnDate = null;
            }
            OnLostFocus(sender, e);
		}

		#region Numeric field edit fix
		private bool _isManualChange;

		private void OnGotFocus(object sender, EventArgs e)
		{
			_isManualChange = true;
		}

		private void OnLostFocus(object sender, EventArgs e)
		{
			_isManualChange = false;
		}

		private void OnEditValueChanged(object sender, EditValueChangedEventArgs e)
		{
			if (_isManualChange)
				Dispatcher.RemoveTextLeadingZero(sender, e);
		}
		#endregion

	}
}
