﻿using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for INDateListView.xaml
    /// </summary>
    public partial class INDateListView
    {
        public INDateListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, INDateGrid);
        }
    }
}
