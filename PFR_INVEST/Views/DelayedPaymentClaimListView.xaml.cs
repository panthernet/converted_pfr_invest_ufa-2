﻿using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    using DataObjects;
    using BusinessLogic;
    /// <summary>
    /// Interaction logic for DepclaimMaxListView.xaml
    /// </summary>
    public partial class DelayedPaymentClaimListView
    {
        public DelayedPaymentClaimListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, Grid);
        }

        private void tableView_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var item = Grid.CurrentItem as DelayedPaymentClaim;
            if (item != null)
                App.DashboardManager.OpenNewTab<DelayedPaymentClaimView, DelayedPaymentClaimViewModel>("Задержанная выплата", ViewModelState.Edit, item.ID, ViewModelState.Edit);
        }
    }
}
