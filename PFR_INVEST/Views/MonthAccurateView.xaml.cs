﻿using System.Windows.Controls;
using System;
using DevExpress.Xpf.Editors;
using PFR_INVEST.Extensions;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for MonthAccurateView.xaml
    /// </summary>
    public partial class MonthAccurateView : UserControl
    {
        public MonthAccurateView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
            ModelInteractionHelper.SignUpForCloseRequest(this, true);
        }

		#region Numeric field edit fix
		private bool _isManualChange;

		private void OnGotFocus(object sender, EventArgs e)
		{
			_isManualChange = true;
		}

		private void OnLostFocus(object sender, EventArgs e)
		{
			_isManualChange = false;
		}

		private void OnEditValueChanged(object sender, EditValueChangedEventArgs e)
		{
			if (_isManualChange)
			{
				Dispatcher.RemoveTextLeadingZero(sender, e);
			}
		}
		#endregion    
	}
}
