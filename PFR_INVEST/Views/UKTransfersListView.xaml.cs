﻿using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    public partial class UKTransfersListView
    {
        public UKTransfersListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, UKGrid);
        }

        public bool IsTransferSelected()
        {
            return UKGrid.View.FocusedRowData.Level >= UKGrid.GetGroupedColumns().Count && UKGrid.SelectedItem != null && ((UKListItem) UKGrid.CurrentItem).ContractNumber != null;
        }

        public UKListItem GetSelectedTransfer()
        {
            if (!IsTransferSelected())
                return null;
            return UKGrid.CurrentItem as UKListItem;
        }
    }
}
