﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using System.Windows.Markup;
using System.Windows.Data;
using System.Windows.Media;
using DevExpress.Xpf.Grid;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for DepositsListView.xaml
    /// </summary>
    public partial class DepositsListView
    {
        public DepositsListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, ListGrid);
            ListGrid.CustomColumnSort += (sender, e) =>
            {
                e.Result = ComponentHelper.OnColumnCustomSort(sender, e);
                e.Handled = true;
            };
            tableView.ShowFilterPopup += tableView_ShowFilterPopup;
        }

        private static void tableView_ShowFilterPopup(object sender, FilterPopupEventArgs e)
        {
            if (e.Column.FieldName == "Month")
            {
                var firstItem = (CustomComboBoxItem)((List<object>)e.ComboBoxEdit.ItemsSource).First();
                var filterItems = new List<CustomComboBoxItem> { firstItem };
                ComponentHelper.GetMonthList().ForEach(a => filterItems.Add(new CustomComboBoxItem { DisplayValue = a, EditValue = a }));
                e.ComboBoxEdit.ItemsSource = filterItems;
            }
            else if (e.Column.FieldName == "Day")
            {
                e.ComboBoxEdit.ItemsSource = ((List<object>)e.ComboBoxEdit.ItemsSource).OrderBy(a =>
                {
                    DateTime dt;
                    return DateTime.TryParse(((CustomComboBoxItem)a).EditValue as string, out dt) ? dt.Date : DateTime.MinValue;
                }).ToList();
            }
        }

        private void ListGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ListGrid.View.GetRowElementByMouseEventArgs(e) == null) return;
            if (ListGrid.View.FocusedRowHandle >= 0)
            {
                App.DashboardManager.OpenNewTab(typeof(DepositView), ViewModelState.Edit,
                    GetSelectedID(), "Депозит");
            }
        }

        public long GetSelectedID()
        {
            if (ListGrid.View.FocusedRowHandle <= -10000) return 0;
            if (ListGrid.View.FocusedRowHandle < 0) return 0;
            try
            {
                return (long)ListGrid.GetCellValue(ListGrid.View.FocusedRowHandle, "ID");
            }
            catch
            {
                return 0;
            }
        }

        public long GetSelectedPortfolioID()
        {
            if (ListGrid.View.FocusedRowHandle > -10000)
            {
                try
                {
                    return (long)ListGrid.GetCellValue(ListGrid.View.FocusedRowHandle, "PortfolioID");
                }
                catch
                {
                    return 0;
                }
            }
            return 0;
        }

		private void ListGrid_CustomColumnSort(object sender, CustomColumnSortEventArgs e)
		{
			e.Result = ComponentHelper.OnColumnCustomSort(sender, e);

			e.Handled = true;
		}
    }

    public class DLColorConverter : MarkupExtension, IValueConverter
    {

        public DLColorConverter() { }

        #region IValueConverter Members

        public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ((bool)value) ? Brushes.Red : Brushes.White;
        }

        public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new System.NotImplementedException();
        }

        #endregion

        public override object ProvideValue(System.IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
