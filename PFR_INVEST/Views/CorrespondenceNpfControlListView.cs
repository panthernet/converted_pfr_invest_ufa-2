﻿using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.ViewModelsCard;

namespace PFR_INVEST.Views
{
    using DataObjects.ListItems;

    public class CorrespondenceNpfControlListView : CorrespondenceControlListView
    {
        public CorrespondenceNpfControlListView()
        {
            c9.Visible = false;
        }

        protected override void OpenItem(CorrespondenceListItemNew item)
        {
            if (item.IsAttach)
                App.DashboardManager.OpenNewTab<LinkedDocumentView, LinkedDocumentNpfViewModel>(CorrespondenceListItem.s_AttachDoc, ViewModelState.Edit, item.ID, ViewModelState.Edit);
            else
                App.DashboardManager.OpenNewTab<DocumentNpfView, DocumentNpfViewModel>(CorrespondenceListItem.s_Doc, ViewModelState.Edit, item.ID, ViewModelState.Edit);
        }
    }
}
