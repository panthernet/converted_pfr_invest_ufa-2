﻿using System.Windows.Controls;
using DevExpress.Xpf.Grid;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for DeadZLAddView.xaml
    /// </summary>
    public partial class DeadZLAddView : UserControl 
    {
        public DeadZLAddView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        private void tableView_CellValueChanging(object sender, CellValueChangedEventArgs e)
		{
			(sender as TableView)?.PostEditor();
		}
    }
}
