﻿using System.Windows;
using System.Windows.Input;
using DevExpress.Data;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    public partial class DepositReturnListView
    {
        private DepositReturnListViewModel Model => (DepositReturnListViewModel)DataContext;

        public DepositReturnListView()
        {
            InitializeComponent();
            Grid.View.ShowGridMenu += View_ShowGridMenu;
            ModelInteractionHelper.SignUpForCustomAction(this, o =>  Grid.UpdateGroupSummary());
        }

        private static void View_ShowGridMenu(object sender, GridMenuEventArgs e)
        {
            DevExpressHelper.HideGroupingFromColumnMenu(e);
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            App.DashboardManager.DockManager.ActiveDockItem.Closed = true;
        }

        void grid_CustomSummary(object sender, CustomSummaryEventArgs e)
        {
            if (!e.IsGroupSummary) return;
            if (e.SummaryProcess == CustomSummaryProcess.Start)
            {

            }
            if (e.SummaryProcess == CustomSummaryProcess.Calculate)
            {
                e.TotalValue = "Итого: ";
            }
        }


        private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var h = tableView.CalcHitInfo(e.OriginalSource as DependencyObject);

            int rowHandle = tableView.GetRowHandleByMouseEventArgs(e);
            if (rowHandle == DataControlBase.InvalidRowHandle) return;

            if (h.HitTest == TableViewHitTest.Row)
            {
                var x = (DepositReturnListItem)Grid.GetRow(h.RowHandle);
                Model.EditSum(x);
            }

            if (h.HitTest == TableViewHitTest.GroupValue || h.HitTest == TableViewHitTest.GroupRow || h.HitTest == TableViewHitTest.GroupSummary)
            {
                var x = (DepositReturnListItem)Grid.GetRow(h.RowHandle);
                Model.EditSum(x);
            }

            Grid.UpdateGroupSummary();

            e.Handled = true;
        }

		
    }
}
