﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    public partial class YearPlanView : UserControl
    {
        public YearPlanView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        private void GridControl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (Grid.View.GetRowHandleByMouseEventArgs(e) == DevExpress.Xpf.Grid.DataControlBase.InvalidRowHandle) return;
            long lID = GetSelectedUKPlanID();
            if (lID > 0)
                App.DashboardManager.OpenNewTab(typeof(UKPaymentPlanView), ViewModelState.Edit, lID, "План выплат по УК");
        }

        public long GetSelectedUKPlanID()
        {
            return Convert.ToInt64(Grid.GetCellValue(Grid.View.FocusedRowHandle, "UKPlanID"));
        }
    }
}
