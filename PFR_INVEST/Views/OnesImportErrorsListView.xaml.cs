﻿using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for OnesImportJournalListView.xaml
    /// </summary>
    public partial class OnesImportErrorsListView
    {
        public OnesImportErrorsListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, listGrid);
            ModelInteractionHelper.SubscribeForPageLoadModel(this);
            listGrid.CustomColumnDisplayText += listGrid_CustomColumnDisplayText;
        }

        public OnesImportErrorsListViewModel Model => DataContext as OnesImportErrorsListViewModel;

        private void listGrid_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            const string dispText = "Сессия номер: {0}";
            if (!listGrid.IsValidRowHandle(e.RowHandle))
                return;

            if (e.Column.FieldName == "SessionID")
            {
                var currRow = (e.Row as OnesErrorJournalListItem);
                if (currRow != null)
                    e.DisplayText = string.Format(dispText, currRow.SessionID);
            }

            if (e.Column.FieldName == "FileID")
            {
                var currRow = (e.Row as OnesErrorJournalListItem);
                if (currRow != null && currRow.FileID == 0)
                    e.DisplayText = string.Format(dispText, "Не указан");
            }
        }
    }
}
