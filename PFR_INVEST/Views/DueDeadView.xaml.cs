﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for DueDeadView.xaml
    /// </summary>
    public partial class DueDeadView : UserControl
    {
        public DueDeadView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        public DueDeadViewModel Model => DataContext as DueDeadViewModel;

        public string GetSelectedDop()
        {
            if (Dops.View.FocusedRowHandle >= 0 && Dops.View.FocusedRowHandle > -1000)
                return Dops.GetCellValue(Dops.View.FocusedRowHandle, "ID").ToString();

            return string.Empty;
        }

        private void TableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (Dops.View.FocusedRow == null) return;
            long dopID = Convert.ToInt64(GetSelectedDop());
            if (dopID > 0)
                App.DashboardManager.OpenNewTab(typeof(DueDeadAccurateView), Model.State == ViewModelState.Read ? ViewModelState.Read : ViewModelState.Edit, dopID, Model, DueDocKindIdentifier.DueDeadAccurate);
        }
    }
}
