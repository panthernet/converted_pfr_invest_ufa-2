﻿namespace PFR_INVEST.Views
{
    using BusinessLogic;

    internal class InsuranceArchiveVRListView:InsuranceArchiveListView
    {
        public InsuranceArchiveVRListView()
        {
            grdInsuranceList.Columns.GetColumnByName("c2").Header = "ГУК ВР";
        }

        protected override void OpenItem(long id)
        {
            App.DashboardManager.OpenNewTab(typeof(InsuranceVRView), ViewModelState.Edit, id, "Договор страхования ВР");
        }
    }
}
