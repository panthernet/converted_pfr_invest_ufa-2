﻿using System.Windows.Controls;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for PensionNotificationView.xaml
    /// </summary>
    public partial class PensionNotificationListView : UserControl
    {
        public PensionNotificationListView()
        {
            InitializeComponent();
        }

        public long GetSelectedID()
        {
            if (this.Grid.View.FocusedRowHandle >= 0)
                return (long)this.Grid.GetCellValue(this.Grid.View.FocusedRowHandle, "ID");

            return 0;
        }

        private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (Grid.View.GetRowElementByMouseEventArgs(e) == null) return;
            long selectedID = this.GetSelectedID();

            if (selectedID > 0)
                App.DashboardManager.OpenNewTab(typeof(PensionNotificationView), ViewModelState.Edit, selectedID, "Уведомление о назначении НЧТП");
        }
        
    }
}
