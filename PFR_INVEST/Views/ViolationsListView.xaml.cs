﻿using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for ViolationsListView.xaml
    /// </summary>
    public partial class ViolationsListView
    {
        public ViolationsListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, violationGrid);
        }

        private void tableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (violationGrid.View.GetRowElementByMouseEventArgs(e) == null) return;

            if (violationGrid.View.FocusedRowHandle < 0)
                return;
            object value = violationGrid.GetCellValue(violationGrid.View.FocusedRowHandle, "ID");

            App.DashboardManager.OpenNewTab(typeof(ViolationView),
                ViewModelState.Edit,
                (long)
                    value, "Нарушение УК");
        }

        public long GetSelectedID()
        {
            if (violationGrid.View.FocusedRowHandle <= -10000)
                return 0;
            if (violationGrid.View.FocusedRowHandle >= 0)
                return (long)violationGrid.GetCellValue(violationGrid.View.FocusedRowHandle, "ID");
            return 0;
        }
    }
}
