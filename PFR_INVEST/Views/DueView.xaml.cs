﻿using System.Windows.Controls;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Extensions;
using System;
using DevExpress.Xpf.Editors;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for DueView.xaml
    /// </summary>
    public partial class DueView : UserControl
    {
        private DueViewModel Model => DataContext as DueViewModel;

        public DueView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        public DopSPNListItem GetSelectedDopSPN()
        {
            return Dops.SelectedItem as DopSPNListItem;
        }

        private void TableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (Dops.View.GetRowElementByMouseEventArgs(e) == null)
                return;

            var item = GetSelectedDopSPN();
            if (item == null)
                return;

            var modelState = Model.State != ViewModelState.Read ? ViewModelState.Edit : ViewModelState.Read;

            if (item.IsKDoc)
                App.DashboardManager.OpenNewTab(typeof(TreasurityView), modelState, item.ID, DueDocKindIdentifier.Treasurity);
            else
            {
                App.DashboardManager.OpenNewTab(typeof (MonthAccurateView), modelState, item.ID,
                    Model.Type == PortfolioIdentifier.PortfolioTypes.DSV ? DueDocKindIdentifier.DueDSVAccurate : DueDocKindIdentifier.DueAccurate);
            }
        }

		#region Numeric field edit fix
		private bool _isManualChange;

		private void OnGotFocus(object sender, EventArgs e)
		{
			_isManualChange = true;
		}

		private void OnLostFocus(object sender, EventArgs e)
		{
			_isManualChange = false;
		}

		private void OnEditValueChanged(object sender, EditValueChangedEventArgs e)
		{
			if (_isManualChange)
			{
				Dispatcher.RemoveTextLeadingZero(sender, e);
			}
		}
		#endregion
	}
}
