﻿using System;
using DevExpress.Xpf.Bars;
using System.Threading.Tasks;
using PFR_INVEST.Helpers;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.Views.Statusbar
{
	public class StatusbarManager
	{
		public Bar StatusBar { get; }
		public BarStaticItem ProgressItem { get; }
		public BarStaticItem ErrorMessageItem { get; }
		public BarStaticItem CBApprovementMessageItem { get; }


		public StatusbarManager(Bar control)
		{
			StatusBar = control;
			ProgressItem = (BarStaticItem)StatusBar.Manager.FindName("biProgress");
			ErrorMessageItem = (BarStaticItem)StatusBar.Manager.FindName("biErrorMesage");
			CBApprovementMessageItem = (BarStaticItem)StatusBar.Manager.FindName("biCBAppMesage");
		}

        /// <summary>
        /// Проказывает прогресбар, при достижении 100% снова прячет через 2 секунды
        /// </summary>
        public void SetProgress(string text, double value, double maximum = 100.0, double minimum = 0.0)
		{

			StatusBar.Dispatcher.Invoke(new Action(() =>
			{
				ProgressItem.IsVisible = true;
				ProgressItem.DataContext = new { Text = text, Value = value, Minimum = minimum, Maximum = maximum };
			}), null);

			if (value == maximum)
			{
				Task.Factory.StartNew(() =>
				{
					TaskHelper.Delay(2000).Wait();
					StatusBar.Dispatcher.Invoke(new Action(() =>
					{
						ProgressItem.IsVisible = false;
					}), null);

				});
			}
		}


        /// <summary>
        /// Показывает критическое уведомление в статусбаре
        /// </summary>
        /// <param name="message">Сообщение</param>
        public void SetErrorMessage(LoginMessageItem message)
		{
			StatusBar.Dispatcher.Invoke(new Action(() =>
			{
				ErrorMessageItem.IsVisible = message != null;
				ErrorMessageItem.DataContext = message;
			}), null);
		}

        /// <summary>
        /// Показывает уведомление о наличии несогласованных отчетов ЦБ
        /// </summary>
        /// <param name="message">Сообщение</param>
        public void SetCBReportAppMessage(LoginMessageItem message)
        {
            StatusBar.Dispatcher.Invoke(new Action(() =>
            {
                CBApprovementMessageItem.IsVisible = message != null;
                CBApprovementMessageItem.DataContext = message;
            }), null);
        }
    }
}
