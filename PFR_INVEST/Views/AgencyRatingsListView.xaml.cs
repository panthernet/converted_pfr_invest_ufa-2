﻿using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for RatingsList.xaml
    /// </summary>
    public partial class AgencyRatingsListView
    {
        public AgencyRatingsListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, ratingsListGrid);
        }

        private void TableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ratingsListGrid.View.GetRowElementByMouseEventArgs(e) == null) return;
            if (ratingsListGrid.View.FocusedRowHandle >= 0)
                App.DashboardManager.OpenNewTab(typeof(AgencyRatingView), ViewModelState.Edit,
                    (long)ratingsListGrid.GetCellValue(ratingsListGrid.View.FocusedRowHandle, "MultiplierID"), "Рейтинг агентства");
        }
    }
}
