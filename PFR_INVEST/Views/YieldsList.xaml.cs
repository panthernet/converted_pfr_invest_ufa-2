﻿using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    using BusinessLogic.ViewModelsList;

    /// <summary>
    /// Interaction logic for ContragentsList.xaml
    /// </summary>
    public partial class YieldsListView
    {
        public YieldsListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, yGrid);
        }

        private void tableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (yGrid.View.GetRowElementByMouseEventArgs(e) == null)
                return;
            if (yGrid.View.FocusedRowHandle < 0)
                return;
            var value = yGrid.GetCellValue(yGrid.View.FocusedRowHandle, "ID");
            if (DataContext is YieldsSIListViewModel)
                App.DashboardManager.OpenNewTab(typeof(YieldSIView),
                    ViewModelState.Edit,
                    (long)value, "Доходность СИ");
            else
                App.DashboardManager.OpenNewTab(typeof(YieldVRView),
                    ViewModelState.Edit,
                    (long)value, "Доходность ВР");
        }
    }
}
