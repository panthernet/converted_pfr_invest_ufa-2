﻿using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    public partial class KIPErrorLogListView
    {
		public KIPErrorLogListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, Grid);
        }
    }
}
