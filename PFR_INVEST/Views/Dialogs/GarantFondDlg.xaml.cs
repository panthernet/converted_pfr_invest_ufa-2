﻿using System.Windows;
using PFR_INVEST.Tools;

namespace PFR_INVEST.Views.Dialogs
{
    public partial class GarantFondDlg
    {
        public GarantFondDlg()
        {
            InitializeComponent();
            ThemesTool.SetCurrentTheme(this);
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
