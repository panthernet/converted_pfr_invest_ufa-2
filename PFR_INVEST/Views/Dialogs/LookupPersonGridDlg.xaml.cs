﻿using System;
using System.Windows;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for BanksVerificationDlg.xaml
    /// </summary>
    public partial class LookupPersonGridDlg
    {
        public LookupPersonGridDlg()
        {
            InitializeComponent();            
        }

        private void gridView_DoubleClick(object sender, EventArgs e) {
            DialogResult = true;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
