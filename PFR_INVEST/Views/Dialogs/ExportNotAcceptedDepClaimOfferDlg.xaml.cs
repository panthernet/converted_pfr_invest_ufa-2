﻿namespace PFR_INVEST.Views.Dialogs
{
    using DevExpress.Xpf.Core;
    using System.Linq;
    using PFR_INVEST.BusinessLogic.ViewModelsList;
    using PFR_INVEST.Constants.Identifiers;

    /// <summary>
    /// Interaction logic for ExportDepClaimMaxListDlg.xaml
    /// </summary>
    public partial class ExportNotAcceptedDepClaimOfferDlg : DXWindow
    {
        public ExportNotAcceptedDepClaimOfferDlg()
        {
            InitializeComponent();           
        }

        private void OKButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
