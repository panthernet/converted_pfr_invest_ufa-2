﻿using System.Windows;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;

namespace PFR_INVEST.Views.Dialogs
{
    public partial class BankPersonsExpiringDlg
    {
        public BankPersonsExpiringModel Model => DataContext as BankPersonsExpiringModel;

        public BankPersonsExpiringDlg()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = Model.ShowNextTime;
        }

        private void DXWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            DialogResult = Model.ShowNextTime;
        }
    }
}
