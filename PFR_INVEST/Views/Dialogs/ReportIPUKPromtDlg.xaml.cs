﻿using System.Windows;
using DevExpress.Xpf.Core;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for RenameSI.xaml
    /// </summary>
    public partial class ReportIPUKPromtDlg : DXWindow
    {
        public bool IsNameChanged { get; private set; }
		public bool IsReadOnly { get; private set; }

       
		public ReportIPUKPromtDlg(bool isTotal = false)
        {
            InitializeComponent();
		    if (isTotal)
		    {
		        Width = 400;
		        Height = 173;
		    }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
       
    }
}
