﻿using System.Windows;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for RenameSI.xaml
    /// </summary>
    public partial class OnesPkipPathSettingsDlg
    {
        public OnesPkipPathSettingsDlg(int setting)
        {
            InitializeComponent();
            switch(setting)
            {
                case 0: //1C
                    Title = "Настройка путей 1C";
                    break;
                case 1: //PKIP
                    Title = "Настройка путей ПТК КИП";
                    break;
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {       
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }    
    }
}
