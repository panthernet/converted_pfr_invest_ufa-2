﻿namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for BanksVerificationDlg.xaml
    /// </summary>
    public partial class BanksVerificationDlg
    {
        public BanksVerificationDlg()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            DialogResult = false;
        }

    }
}
