﻿namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for ExportAuctionInfoDlg.xaml
    /// </summary>
	public partial class ExportAuctionInfoDlg
    {
		public ExportAuctionInfoDlg()
        {
            InitializeComponent();           
        }

        private void OKButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
