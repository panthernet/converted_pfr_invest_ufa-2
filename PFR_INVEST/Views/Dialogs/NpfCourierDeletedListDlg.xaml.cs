﻿using System.Windows.Input;
using PFR_INVEST.DataObjects;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;

namespace PFR_INVEST.Views.Dialogs
{
    public partial class NpfCourierDeletedListDlg
    {
        public NPFCourierDeletedListViewModel Model => DataContext as NPFCourierDeletedListViewModel;

        public NpfCourierDeletedListDlg()
        {
            InitializeComponent();
        }

        private void CouriersGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var x = CourierGrid.GetFocusedRow() as LegalEntityCourier;
            if (x == null)
                return;

            Model.ViewCourier(x);
        }
    }
}
