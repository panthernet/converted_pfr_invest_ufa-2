﻿using System.Windows;
using DevExpress.Xpf.Editors;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for RenameSI.xaml
    /// </summary>
    public partial class RenameSIDlg
    {
        public bool IsNameChanged { get; private set; }
		public bool IsReadOnly { get; private set; }

        public RenameSIDialogViewModel Model => DataContext as RenameSIDialogViewModel;

        public RenameSIDlg()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            var le = DataContainerFacade.GetByID<LegalEntity, long>(Model.LEID);
            le.FormalizedName = formalizedName.Text;
            le.FullName = fullName.Text;
            le.ShortName = shortName.Text;
            if (!string.IsNullOrWhiteSpace(inn.Text))
                le.INN = inn.Text;
            DataContainerFacade.Save<LegalEntity, long>(le);
            Model.Save();
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void Name_EditValueChanged(object sender, EditValueChangedEventArgs e)
        {
            IsNameChanged = Model.IsNameChanged;
            OKButton.IsEnabled = Model.IsNameChanged;
        }
    }
}
