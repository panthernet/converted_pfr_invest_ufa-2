﻿using System;
using System.Collections;
using System.Windows;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for RoleView.xaml
    /// </summary>
    public partial class OnesDefSettingsDlg
    {
        private OnesDefSettingsDlgViewModel Model => DataContext as OnesDefSettingsDlgViewModel;

        public OnesDefSettingsDlg()
        {
            InitializeComponent();

            Loaded += OnesDefSettingsDlg_Loaded;
            Closing += OnesDefSettingsDlg_Closing;
        }

        private void OnesDefSettingsDlg_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (Model != null && Model.IsDataChanged && !ViewModelBase.DialogHelper.ShowConfirmation("При закрытии окна, все изменения на выбранной вкладке сбросятся. Продолжить?", "Внимание"))
                e.Cancel = true;
        }

        private void OnesDefSettingsDlg_Loaded(object sender, RoutedEventArgs e)
        {
            cboxNpfContent.EditValueChanging += cboxDepositContent_EditValueChanging;
            cboxUkContent.EditValueChanging += cboxDepositContent_EditValueChanging;
            cboxDepositContent.EditValueChanging += cboxDepositContent_EditValueChanging;

            cboxNpfContent.SelectedIndexChanged += ContentOnSelectedIndexChanged;
            cboxUkContent.SelectedIndexChanged += ContentOnSelectedIndexChanged;
            cboxDepositContent.SelectedIndexChanged += ContentOnSelectedIndexChanged;
        }

        private void cboxDepositContent_EditValueChanging(object sender, DevExpress.Xpf.Editors.EditValueChangingEventArgs e)
        {
            if (Model == null || Model.IsDataChanged && !ViewModelBase.DialogHelper.ShowConfirmation("При смене типа сведений, все изменения на выбранной вкладке сбросятся. Продолжить?", "Внимание"))
            {
                e.IsCancel = true;
                e.Handled = true;
            }
        }

        private void ContentOnSelectedIndexChanged(object sender, RoutedEventArgs routedEventArgs)
        {
            if (Model == null) return;
            RefreshDataContext(GetCurrentPage());
        }

        private void Window_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            RefreshDataContext();
        }

        private void RefreshDataContext(string page = null)
        {
            Model.UpdateDefinitions(page);

            if (page == null)
            {
                listGrid1.DataContext = Model.NpfSelectedOperations;
                listGrid2.DataContext = Model.NpfLeftOperations;
                listGrid1uk.DataContext = Model.UkSelectedOperations;
                listGrid2uk.DataContext = Model.UkLeftOperations;
                listGrid1depo.DataContext = Model.DepositSelectedOperations;
                listGrid2depo.DataContext = Model.DepositLeftOperations;
            }
            else
            {
                switch (page)
                {
                    case OnesSettingsDef.Groups.NPF:
                        listGrid1.DataContext = Model.NpfSelectedOperations;
                        listGrid2.DataContext = Model.NpfLeftOperations;
                        break;
                    case OnesSettingsDef.Groups.UK:
                        listGrid1uk.DataContext = Model.UkSelectedOperations;
                        listGrid2uk.DataContext = Model.UkLeftOperations;
                        break;
                    case OnesSettingsDef.Groups.DEPOSIT:
                        listGrid1depo.DataContext = Model.DepositSelectedOperations;
                        listGrid2depo.DataContext = Model.DepositLeftOperations;
                        break;
                }
            }
        }

        private void ButtonAddNpf_Click(object sender, RoutedEventArgs e)
        {
            if (Model == null) return;
            var items = GetCurrentItems(false);
            if (items.Count == 0) return;
            Model.AddDefinitions(items, GetCurrentPage());
        }

        private void ButtonRemoveNpf_Click(object sender, RoutedEventArgs e)
        {
            if (Model == null) return;
            var items = GetCurrentItems(true);
            if (items.Count == 0) return; Model.RemoveDefinitions(items, GetCurrentPage());
        }

        private IList GetCurrentItems(bool selected)
        {
            switch (tabControl.SelectedIndex)
            {
                case 0:
                    return  selected ? listGrid1.SelectedItems : listGrid2.SelectedItems;
                case 1:
                    return selected ? listGrid1uk.SelectedItems : listGrid2uk.SelectedItems;
                case 2:
                    return selected ? listGrid1depo.SelectedItems : listGrid2depo.SelectedItems;
            }
            throw new Exception("Неизвестный тип страницы настроек!");
        }


        private string GetCurrentPage()
        {
            switch (tabControl.SelectedIndex)
            {
                case 0:
                    return OnesSettingsDef.Groups.NPF;
                case 1:
                    return OnesSettingsDef.Groups.UK;
                case 2:
                    return OnesSettingsDef.Groups.DEPOSIT;
            }
            throw new Exception("Неизвестный тип страницы настроек!");
        }

        private void Cancel_OnClick(object sender, RoutedEventArgs e)        
        {
            Close();
        }
    }
}