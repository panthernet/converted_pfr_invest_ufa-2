﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for SendEmailsToUKDlg.xaml
    /// </summary>
    public partial class SendEmailsToUKDlg
    {
        private const string OPEN_EMAIL_CLIENT_ERROR = "Не удалось запустить почтовый клиент";

        public readonly List<long> SelectedValues = new List<long>();

        private SendEmailsToUKDialogModel Model => DataContext as SendEmailsToUKDialogModel;

        public SendEmailsToUKDlg()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            var emails = (from c in Model.Contacts
                               where SelectedValues.Contains(c.ContactID)
                               select c.Email).ToArray();

            var mailto = string.Join(";", emails);

            try
            {
                Process.Start("mailto:" + mailto);
            }
            catch
            {
                DXMessageBox.Show(OPEN_EMAIL_CLIENT_ERROR, "Неизвестная ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                DialogResult = false;
            }

            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void grdContacts_CustomUnboundColumnData(object sender, GridColumnDataEventArgs e)
        {
            if (e.Column.FieldName != "Selected") return;

            var sId = e.GetListSourceFieldValue("ContactID").ToString();
            long id;
            long.TryParse(sId, out id);

            if (e.IsGetData)
            {
                e.Value = GetIsSelected(id);
            }
            if (e.IsSetData)
            {
                SetIsSelected(id, (bool)e.Value);
            }

            OKButton.IsEnabled = SelectedValues.Count > 0;
        }

        private bool GetIsSelected(long id)
        {
            return SelectedValues.Contains(id);
        }

        private void SetIsSelected(long id, bool value)
        {
            if (value == false)
                SelectedValues.Remove(id);
            else
                if (SelectedValues.Contains(id) == false)
                    SelectedValues.Add(id);
        }

        private void UpdateSelectionForAll(bool newIsSelected)
        {
            var list = GetDataRowHandles();
            for (int i = 0; i < list.Count; i++)
            {
                int rowHandle = grdContacts.GetRowHandleByListIndex(i);
                grdContacts.SetCellValue(rowHandle, "Selected", newIsSelected);
                SetIsSelected(Convert.ToInt32(grdContacts.GetCellValue(rowHandle, "ContractID")), newIsSelected);
            }
        }

        private bool _mGridRowCheckBoxClicked;
        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            _mGridRowCheckBoxClicked = true;
            if (SelectedValues.Count == 0)
                _headerCheckEdit.IsChecked = false;
            else if (SelectedValues.Count != Model.Contacts.Count)
                _headerCheckEdit.IsChecked = null;
            else
                _headerCheckEdit.IsChecked = true;
        }

        private void CheckEdit_Checked(object sender, RoutedEventArgs e)
        {
            if (!_mGridRowCheckBoxClicked)
                UpdateSelectionForAll(true);
            _mGridRowCheckBoxClicked = false;
        }

        private void CheckEdit_Indeterminate(object sender, RoutedEventArgs e)
        {
            if (!_mGridRowCheckBoxClicked)
                _headerCheckEdit.IsChecked = false;
            _mGridRowCheckBoxClicked = false;
        }

        private void CheckEdit_Unchecked(object sender, RoutedEventArgs e)
        {
            UpdateSelectionForAll(false);
            _mGridRowCheckBoxClicked = false;
        }

        private List<int> GetDataRowHandles()
        {
            var rowHandles = new List<int>();
            for (int i = 0; i < grdContacts.VisibleRowCount; i++)
            {
                int rowHandle = grdContacts.GetRowHandleByVisibleIndex(i);
                if (grdContacts.IsGroupRowHandle(rowHandle))
                {
                    if (!grdContacts.IsGroupRowExpanded(rowHandle))
                    {
                        rowHandles.AddRange(GetDataRowHandlesInGroup(rowHandle));
                    }
                }
                else
                    rowHandles.Add(rowHandle);
            }
            return rowHandles;
        }

        private IEnumerable<int> GetDataRowHandlesInGroup(int groupRowHandle)
        {
            var rowHandles = new List<int>();
            for (int i = 0; i < grdContacts.GetChildRowCount(groupRowHandle); i++)
            {
                int rowHandle = grdContacts.GetChildRowHandle(groupRowHandle, i);
                if (grdContacts.IsGroupRowHandle(rowHandle))
                {
                    rowHandles.AddRange(GetDataRowHandlesInGroup(rowHandle));
                }
                else
                    rowHandles.Add(rowHandle);
            }
            return rowHandles;
        }

        private CheckBox _headerCheckEdit;
        private void CheckEdit_Loaded(object sender, RoutedEventArgs e)
        {
            _headerCheckEdit = sender as CheckBox;
        }
    }
}
