﻿using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for SelectCBReportParamsDlg.xaml
    /// </summary>
    public partial class SelectCBReportParamsDlg
    {
        public SelectCBReportParamsDlg()
        {
            InitializeComponent();
            ModelInteractionHelper.SignUpForCloseRequest(this);
        }
    }
}
