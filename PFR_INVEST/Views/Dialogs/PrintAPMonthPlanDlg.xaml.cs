﻿using System.Windows;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for FromUKEnterGetRequirementDateDlg.xaml
    /// </summary>
    public partial class PrintAPMonthPlanDlg
    {
        public const string INVALID_YEAR_MSG = "Для выделенного года/месяца планы перечислений не создавались ранее!";

        public PrintAPMonthPlanDlgViewModel Model => DataContext as PrintAPMonthPlanDlgViewModel;

        public PrintAPMonthPlanDlg()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (CheckIfSelectedYearMonthIsValid())
                DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private bool CheckIfSelectedYearMonthIsValid()
        {
            if (Model.IsSelectedYearValid())
                return true;
            
            ViewModelBase.DialogHelper.ShowAlert(INVALID_YEAR_MSG);
            return false;
        }
    }
}
