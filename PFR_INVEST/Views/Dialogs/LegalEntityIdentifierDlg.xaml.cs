﻿using System.Windows;
using DevExpress.Xpf.Core;

namespace PFR_INVEST.Views.Dialogs
{
    public partial class LegalEntityIdentifierDlg
    {

        public LegalEntityIdentifierDlg()
        {
            InitializeComponent();
        }

        private void cmdSave_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
           
        private void DXWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (DialogResult == true)
                return;

            if (cmdSave.Command != null && cmdSave.Command.CanExecute(null))
            {
                var res = DXMessageBox.Show("Вы хотите сохранить изменения?", "Подтверждение", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
                switch (res)
                {
                    case MessageBoxResult.Yes:
                        cmdSave.Command.Execute(null);
                        DialogResult = true;
                        break;
                    case MessageBoxResult.Cancel:
                        e.Cancel = true;
                        break;
                }
            }
        }
    }
}
