﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
//using System.Windows.Forms;
using System.Windows.Media.Imaging;
using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.BusinessLogic.ViewModelImportExport;
using PFR_INVEST.BusinessLogic.ViewModelsCard;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;
using PFR_INVEST.BusinessLogic.ViewModelsReport;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.DataObjects.Reports;
using PFR_INVEST.Tools;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;
using System.Reflection;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Forms;
using PFR_INVEST.BusinessLogic.ViewModelsPrint;
using PFR_INVEST.BusinessLogic.ViewModelsWizards.CBReport;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core.ListExtensions;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.Views.Dialogs.ReportPromt;
using PFR_INVEST.Views.Wizards.SpnDeposit;
using CBReportWizardWindow = PFR_INVEST.Views.Wizards.CBReportWizard.CBReportWizardWindow;
using Document = PFR_INVEST.DataObjects.Document;
using HorizontalAlignment = System.Windows.HorizontalAlignment;
using SaveFileDialog = Microsoft.Win32.SaveFileDialog;

namespace PFR_INVEST.Views.Dialogs
{
    internal class DialogHelper : IDialogHelper
    {
        #region Helpers
        /// <summary>
        /// Подготавливает диалог, устанавливает тему, датасоурс и другие повторяющиеся действия.
        /// </summary>
        /// <typeparam name="W">Диалог</typeparam>
        /// <typeparam name="M">Модель</typeparam>
        /// <param name="view">Диалог</param>
        /// <param name="model">Модель</param>
        /// <returns>Диалог</returns>
        public static W PrepareDialog<W, M>(W view, M model)
            where W : DXWindow
            where M : class
        {
            if (view == null) throw new NullReferenceException("view");
            if (model == null) throw new NullReferenceException("model");

            view.DataContext = model;
            view.Owner = App.mainWindow;
            if (view.Icon == null && view.ShowIcon)
                view.Icon = new BitmapImage(new Uri("pack://application:,,,/PFR_INVEST;component/Images/icon.ico", UriKind.RelativeOrAbsolute)) { CacheOption = BitmapCacheOption.OnLoad };
            ThemesTool.SetCurrentTheme(view);

            return view;
        }

        #endregion Helpers

        #region Диалоги DXMessageBox
        const string CONFIRMATION_CAPTION = "Подтверждение";

        const string MESSAGE_DUE_NOT_FOUND_FORMAT = "Основной документ {0} в выбранном периоде для указанного портфеля не обнаружен. Подтвердить запись Документа казначейства?";
        const string MESSAGE_MORE_THAN_ONE_DUE_FORMAT = "Обнаружено несколько документов {0} в выбранном периоде для указанного портфеля. Документ казначейства будет уточнять первый найденный документ. Продолжить?";

        public void RefreshUndeleteLists(string[] updateViewModels)
        {
            var ass = Assembly.GetAssembly(typeof(ViewModelBase));
            var list = new List<Type>();
            updateViewModels.ForEach(typeName =>
            {
                var type = ass.GetType(typeName, false);
                if (type == null) return;
                var model = DashboardManager.Instance.FindViewModel(type);
                if (model != null && model is IPagedLoadListModel)
                    ((IPagedLoadListModel)model).RefreshListExternal();
                else list.Add(type);
            });
            DashboardManager.Instance.RefreshListViewModels(list);
        }


        public bool CheckTreasurityBaseDocument(int count, bool isDSV)
        {
            if (count == 0)
                return DXMessageBox.Show(string.Format(MESSAGE_DUE_NOT_FOUND_FORMAT, isDSV ? "ДСВ" : "СВ"),
                                         CONFIRMATION_CAPTION,
                                         MessageBoxButton.YesNo,
                                         MessageBoxImage.Question) == MessageBoxResult.Yes;
            if (count > 1)
                return DXMessageBox.Show(string.Format(MESSAGE_MORE_THAN_ONE_DUE_FORMAT, isDSV ? "ДСВ" : "СВ"),
                    CONFIRMATION_CAPTION,
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Question) == MessageBoxResult.Yes;
            return true;
        }

        public bool ConfirmNPFNameChanged()
        {
            return DXMessageBox.Show("Вы хотите изменить наименование?",
                                     CONFIRMATION_CAPTION,
                                    MessageBoxButton.YesNo,
                                    MessageBoxImage.Question) == MessageBoxResult.Yes;
        }

        public bool ConfirmHistoryChanged()
        {
            return DXMessageBox.Show("Сохранить изменение в истории?",
                         CONFIRMATION_CAPTION,
                        MessageBoxButton.YesNo,
                        MessageBoxImage.Question) == MessageBoxResult.Yes;
        }

        public bool ConfirmSIRegisterDeleting()
        {
            return DXMessageBox.Show("Удаление Регистра перечислений повлечет за собой удаление всех вложенных списков перечислений! Продолжить?",
                                     CONFIRMATION_CAPTION,
                                     MessageBoxButton.YesNo,
                                     MessageBoxImage.Question) == MessageBoxResult.Yes;
        }

        public bool ConfirmSITransferListDeleting()
        {
            return DXMessageBox.Show("Удаление Списка перечислений повлечет за собой удаление всех вложеных перечислений! Продолжить?",
                                     CONFIRMATION_CAPTION,
                                     MessageBoxButton.YesNo,
                                     MessageBoxImage.Question) == MessageBoxResult.Yes;
        }

        public bool ConfirmDeletingTemplates()
        {
            return DXMessageBox.Show("Удалить выбранные шаблоны?",
                                     CONFIRMATION_CAPTION,
                                     MessageBoxButton.YesNo,
                                     MessageBoxImage.Question) == MessageBoxResult.Yes;
        }

        public bool ConfirmUpdateSIName()
        {
            return DXMessageBox.Show("Наименование СИ было изменено. Вы уверены что хотите изменить наименование СИ, без выполнения процедуры 'Переименование СИ'?",
                                     CONFIRMATION_CAPTION,
                                     MessageBoxButton.YesNo,
                                     MessageBoxImage.Question) == MessageBoxResult.Yes;
        }

        public bool ConfirmImportDepClaim2Rewrite()
        {
            return DXMessageBox.Show("Существующие заявки для аукциона будут заменены импортированными. Продолжить импорт?",
                                        CONFIRMATION_CAPTION,
                                        MessageBoxButton.YesNo,
                                        MessageBoxImage.Asterisk) == MessageBoxResult.Yes;
        }

        public bool ConfirmImportKORewrite()
        {
            return DXMessageBox.Show("Cписок кредитных организаций уже существует для выбранного числа." + Environment.NewLine + "Существующие организации будут заменены импортированными. Продолжить импорт?",
                                        CONFIRMATION_CAPTION,
                                        MessageBoxButton.YesNo,
                                        MessageBoxImage.Asterisk) == MessageBoxResult.Yes;
        }

        public bool ConfirmImportOwnCapitalRewrite(DateTime ReportDate)
        {
            string[] MONTHES = new string[] { "январь", "февраль", "март", "апрель", "май", "июнь", "июль", "август", "сентябрь", "октябрь", "ноябрь", "декабрь" };

            return DXMessageBox.Show(
                string.Format("Информация о собственных средствах КО на {0} {1} г. уже содержится в ПТК ДОКиП." +
                    Environment.NewLine + "Существующие данные будут заменены импортированными. Продолжить импорт?",
                    MONTHES[ReportDate.Month - 1], ReportDate.Year),
                                        CONFIRMATION_CAPTION,
                                        MessageBoxButton.YesNo,
                                        MessageBoxImage.Asterisk) == MessageBoxResult.Yes;
        }

        public bool ConfirmImportDepClaimMaxRewrite(string message)
        {
            return DXMessageBox.Show(message,
                                        CONFIRMATION_CAPTION,
                                        MessageBoxButton.YesNo,
                                        MessageBoxImage.Asterisk) == MessageBoxResult.Yes;
        }


        /// <summary>
        /// Показать диалоговое окно с восклицательным знаком
        /// </summary>
        /// <param name="msg">Сообщение</param>
        /// <param name="caption">Заголовок</param>
        /// <param name="yesNo">Использовать кнопки Да/Нет</param>
        public bool ShowExclamation(string msg, string caption, bool yesNo = false)
        {
            var result = DXMessageBox.Show(msg, caption, yesNo ? MessageBoxButton.YesNo : MessageBoxButton.OK, MessageBoxImage.Exclamation);
            return !yesNo || result == MessageBoxResult.Yes;
        }

        /// <summary>
        /// Показать обычное диалоговое окно
        /// </summary>
        /// <param name="msg">Сообщение</param>
        /// <param name="p">Параметры форматирования сообщения</param>
        public void ShowAlert(string msg, params object[] p)
        {
            DXMessageBox.Show(string.Format(msg, p));
        }

        /// <summary>
        /// Показать диалоговое окно с ошибкой
        /// </summary>
        /// <param name="msg">Сообщение</param>
        /// <param name="p">Параметры форматирования сообщения</param>
        public void ShowError(string msg, params object[] p)
        {
            ShowError(string.Format(msg, p));
        }

        /// <summary>
        /// Показать диалоговое окно с ошибкой
        /// </summary>
        /// <param name="msg">Сообщение</param>
        /// <param name="caption">Заголовок окна</param>
        public void ShowError(string msg, string caption = null)
        {
            DXMessageBox.Show(msg, string.IsNullOrEmpty(caption) ? "Ошибка" : caption, MessageBoxButton.OK, MessageBoxImage.Error);
        }

        public bool? ShowYesNoCancelMessage(string message, string caption = null)
        {
            var res = DXMessageBox.Show(message, string.IsNullOrEmpty(caption) ? "Внимание" : caption, MessageBoxButton.YesNoCancel, MessageBoxImage.Error);
            if (res == MessageBoxResult.Cancel) return null;
            return res == MessageBoxResult.Yes;
        }

        /// <summary>
        /// Показать диалоговое окно с предупреждением
        /// </summary>
        /// <param name="msg">Сообщение</param>
        /// <param name="caption">Заголовок окна</param>
        /// <param name="yesNo">Использовать кнопки Да/Нет</param>
        /// <param name="okCancel">использовать кнопки Ок/Отмена, если не заданы Да/нет</param>
        public bool ShowWarning(string msg, string caption = null, bool yesNo = false, bool okCancel = false)
        {
            var result = DXMessageBox.Show(msg, string.IsNullOrEmpty(caption) ? "Внимание" : caption, yesNo ? MessageBoxButton.YesNo : (okCancel ? MessageBoxButton.OKCancel : MessageBoxButton.OK), MessageBoxImage.Warning);
            return result == MessageBoxResult.Yes || result == MessageBoxResult.OK;
        }

        /// <summary>
        /// Диалог с информацией
        /// </summary>
        /// <param name="msg">Сообщение</param>
        /// <param name="caption">Заголовок</param>
        public void ShowInformation(string msg, string caption = null)
        {
            DXMessageBox.Show(msg, caption);
        }

        /// <summary>
        /// Диалог, вопрос да/нет
        /// </summary>
        /// <param name="msg">Сообщение</param>
        /// <param name="caption">Заголовок</param>
        /// <returns></returns>
        public bool ShowQuestion(string msg, string caption)
        {

            return DXMessageBox.Show(msg, caption,
                                        MessageBoxButton.YesNo,
                                        MessageBoxImage.Question) == MessageBoxResult.Yes;

        }

        /// <summary>
        /// Диалог подтверждения
        /// </summary>
        /// <param name="msg">Сообщение</param>
        /// <param name="caption">Заголовок</param>
        public bool ShowConfirmation(string msg, string caption = null)
        {

            return DXMessageBox.Show(msg, string.IsNullOrEmpty(caption) ?
                CONFIRMATION_CAPTION : caption,
                                        MessageBoxButton.YesNo,
                                        MessageBoxImage.Asterisk) == MessageBoxResult.Yes;

        }

        #endregion

        #region Диалоги приложения


        public ReportFile ShowAggregateSPNReportDialog()
        {
            var dlg = new ReportAggregateSPNPromptDlg();
            ThemesTool.SetCurrentTheme(dlg);
            if(dlg.ShowDialog() != true) return null;

            return WCFClient.Client.CreateReportAgrregateSPN(dlg.ResultNpfIds, dlg.ResultOpList, dlg.ResultAddNpf, dlg.ResultDirection, dlg.DateFrom.Value, dlg.DateTo.Value, dlg.IsExtended, dlg.IsGarant);

        }


        public void OpenChfrReportDialog()
        {
            var v = new SelectChfrReportYearQuartalDlgView();
            var vm = new SelectChfrReportYearQuartalDlgViewModel();
            PrepareDialog(v, vm);
            v.ShowDialog();
        }

        public bool AddNpfForFinregister(long registerId, long limitZl, decimal limitSpn, long parentId)
        {
            var vM = new FinRegisterViewModel(registerId, ViewModelState.Create);
            vM.PrepareAddNotTransferredNpfMode(limitZl, limitSpn, parentId);
            var dlg = new AddNpfDlg { DataContext = vM, Owner = App.mainWindow };
            ThemesTool.SetCurrentTheme(dlg);
            vM.PostOpen(dlg.Title, true);
            return dlg.ShowDialog().Value;
        }

        public bool AddPayment(long SecurityID)
        {
            var sec = DataContainerFacade.GetByID<Security>(SecurityID);
            var dlg = new AddPaymentDlg();
            var dlgVM = new PaymentViewModel(sec, ViewModelState.Create);
            dlg.DataContext = dlgVM;
            dlg.Owner = App.mainWindow;
            ThemesTool.SetCurrentTheme(dlg);

            dlgVM.PostOpen(dlg.Title, true);
            dlgVM.State = ViewModelState.Read;
            if (!dlg.ShowDialog().Value) return false;
            var p = dlgVM.Payment;
            p.SecurityId = sec.ID;
            p.ID = DataContainerFacade.Save<Payment, long>(p);
            JournalLogger.LogModelEvent(dlgVM, JournalEventType.INSERT);
            return true;
        }

        public DlgSecurityListItem SelectSecurity()
        {
            var dlg = new SelectSecurityDlg { Owner = App.mainWindow };
            ThemesTool.SetCurrentTheme(dlg);
            return dlg.ShowDialog().Value ? dlg.Security : null;
        }

        public bool UploadTemplate()
        {
            var depList = DataContainerFacade.GetList<Department>();
            var dlg = new UploadTemplateDlg(depList);
            ThemesTool.SetCurrentTheme(dlg);
            dlg.Owner = App.mainWindow;
            return dlg.ShowDialog().Value;
        }

        public List<NPFforERZLNotifyListItem> AddNPFToOther(List<NPFforERZLNotifyListItem> lst, long SelectedNPFCAID)
        {
            var dlg = new NPF2NPFDlg();
            var dlgVM = new NPF2NPFViewModel();
            dlg.DataContext = dlgVM;

            dlgVM.ContragentsList = lst;
            if (SelectedNPFCAID > 0)
                dlgVM.ContragentsList.RemoveAll(item => item.Contragent.ID == SelectedNPFCAID);

            dlg.Owner = App.mainWindow;
            dlg.Title = "Передача из НПФ";

            ThemesTool.SetCurrentTheme(dlg);
            dlgVM.PostOpen(dlg.Title, true);
            return dlg.ShowDialog().Value ? dlgVM.ContragentsList : null;
        }

        public List<NPFforERZLNotifyListItem> AddOther2NPF(List<NPFforERZLNotifyListItem> lst, long SelectedNPFCAID)
        {
            var dlg = new NPF2NPFDlg();
            var dlgVM = new NPF2NPFViewModel();
            dlg.DataContext = dlgVM;

            dlgVM.ContragentsList = lst;
            if (SelectedNPFCAID > 0)
                dlgVM.ContragentsList.RemoveAll(item => item.Contragent.ID == SelectedNPFCAID || item.Contragent.StatusID == -1);

            dlg.Title = "Передача в НПФ";
            dlg.Owner = App.mainWindow;
            ThemesTool.SetCurrentTheme(dlg);

            dlgVM.PostOpen(dlg.Title, true);
            return dlg.ShowDialog().Value ? dlgVM.ContragentsList : null;
        }

        public void AddBranchSI(long leID)
        {
            var parent = App.DashboardManager.FindViewModelsList<SIViewModel>()
                .FirstOrDefault(m => m.ID == leID) ?? new SIViewModel(leID, ViewModelState.Edit);

            var dlg = new AddBranch();
            var dlgVM = new BranchViewModel(parent) { State = ViewModelState.Create };
            dlg.DataContext = dlgVM;

            dlg.Owner = App.mainWindow;
            ThemesTool.SetCurrentTheme(dlg);

            dlgVM.PostOpen(dlg.Title, true);
            dlg.ShowDialog();
        }

        public void AddBranchNPF(long leID)
        {
            var parent = App.DashboardManager.FindViewModelsList<NPFViewModel>()
                .FirstOrDefault(m => m.ID == leID) ?? new NPFViewModel(ViewModelState.Edit, leID);

            var dlg = new AddBranch { Title = "Филиал" };
            var dlgVM = new BranchViewModel(parent) { State = ViewModelState.Create };
            dlg.DataContext = dlgVM;

            ThemesTool.SetCurrentTheme(dlg);

            dlg.SizeToContent = SizeToContent.WidthAndHeight;

            dlgVM.PostOpen(dlg.Title, true);
            if (dlg.ShowDialog().Value)
            {
                parent.RefreshBranchesList();
            }
        }

        public void EditBranchNPF(long leID)
        {
            var parent = App.DashboardManager.FindViewModelsList<NPFViewModel>()
                .FirstOrDefault(m => m.ID == leID) ?? new NPFViewModel(ViewModelState.Edit, leID);

            var dlg = new AddBranch();
            if (parent.SelectedBranch == null) return;
            dlg.Title = "Филиал";

            var dlgVM = new BranchViewModel(parent, parent.SelectedBranch.ID) { State = ViewModelState.Edit };
            dlg.DataContext = dlgVM;

            ThemesTool.SetCurrentTheme(dlg);

            dlg.SizeToContent = SizeToContent.WidthAndHeight;

            dlgVM.PostOpen(dlg.Title);
            if (dlg.ShowDialog().Value)
            {
                parent.RefreshBranchesList();
            }
        }

        public bool AddContact(long leID)
        {
            var dialog = new AddSIContactDlg();
            var dialogVM = new SIContactDlgViewModel(leID) { State = ViewModelState.Create };
            dialog.DataContext = dialogVM;
            ThemesTool.SetCurrentTheme(dialog);
            //dialog.Owner = App.mainWindow;

            dialogVM.PostOpen(dialog.Title, true);
            return dialog.ShowDialog().Value;
        }

        public bool EditContact(long leID, long cId)
        {
            var dialog = new AddSIContactDlg { Title = "Редактировать контакт" };
            var dialogVM = new SIContactDlgViewModel(cId, leID) { State = ViewModelState.Edit };
            dialog.DataContext = dialogVM;
            ThemesTool.SetCurrentTheme(dialog);
            //dialog.Owner = App.mainWindow;

            dialogVM.PostOpen(dialog.Title);
            return dialog.ShowDialog().Value;
        }

        public void AddDeposit()
        {
            var dlg = new AddDopDueDeadDlg();
            var dlgVM = new DepositViewModel(0, ViewModelState.Create) { State = ViewModelState.Create };
            dlg.DataContext = dlgVM;

            dlg.Owner = App.mainWindow;
            ThemesTool.SetCurrentTheme(dlg);

            dlg.ShowDialog();
            dlgVM.PostOpen(dlg.Title, true);
        }

        public bool AddNotify(long erzlID, List<ERZLNotifiesListItem> lst)
        {
            var dlg = new AddNotifyDlg();
            var erzlNotifyVM = new ERZLNotifyViewModel();

            // фильтруем список НПФ, чтобы нельзя было добавить уведомление на одного и того же НПФ дважды
            if (lst != null && erzlNotifyVM.NPFList != null && erzlNotifyVM.NPFList.Count > 0)
                foreach (var id in lst.Select(notifyLi => notifyLi.ContragentID))
                {
                    erzlNotifyVM.NPFList = erzlNotifyVM.NPFList
                        .Where(item => item.ContragentID != id)
                        .ToList();
                }

            using (Loading.StartNew("Загрузка данных"))
                erzlNotifyVM.Init(erzlID, ViewModelState.Create);

            dlg.DataContext = erzlNotifyVM;

            ThemesTool.SetCurrentTheme(dlg);

            dlg.SizeToContent = SizeToContent.WidthAndHeight;

            dlg.Title = "Добавить уведомление";

            erzlNotifyVM.PostOpen(dlg.Title, true);
            if (!dlg.ShowDialog().Value) return false;
            erzlNotifyVM.SaveCard.Execute(null);
            return true;
        }

        public List<SIImportListItem> ShowImportSIWizardDialog(SIRegister.Directions dir)
        {
            var dlg = new ImportSIDlg();
            var vm = new ImportSIDlgViewModel(dir);
            PrepareDialog(dlg, vm);
            if (dlg.ShowDialog() != true) return new List<SIImportListItem>();
            return vm.Items;
        }


        public bool EditNotify(long erzlID, List<ERZLNotifiesListItem> lst, ERZLNotifiesListItem focused)
        {
            var dlg = new AddNotifyDlg();
            var erzlNotifyVM = new ERZLNotifyViewModel();

            // фильтруем список НПФ, чтобы нельзя было добавить уведомление на одного и того же НПФ дважды
            if (erzlNotifyVM.NPFList != null && erzlNotifyVM.NPFList.Count > 0)
                foreach (var notifyLi in lst)
                {
                    if (notifyLi.ContragentID == focused.ContragentID) continue;
                    var id = notifyLi.ContragentID;
                    erzlNotifyVM.NPFList = erzlNotifyVM.NPFList.Where(item => item.ContragentID != id).ToList();//.NPFList.RemoveAll(item => item.ID == id);
                }

            if (erzlNotifyVM.NPFList != null)
            {
                erzlNotifyVM.SelectedNPF = erzlNotifyVM.NPFList.FirstOrDefault(ca => ca.ContragentID == focused.ContragentID);
                if (erzlNotifyVM.SelectedNPF == null)
                {
                    // если по каким то причинам НПФ не попал в список (был реарганизован), то тащим напрямик из базы
                    erzlNotifyVM.SelectedNPF = DataContainerFacade.GetListByProperty<LegalEntity>("ContragentID", focused.ContragentID).FirstOrDefault();
                    erzlNotifyVM.NPFList = new List<LegalEntity>(erzlNotifyVM.NPFList) { erzlNotifyVM.SelectedNPF };
                }
            }

            erzlNotifyVM.NotifyNum = focused.NotifyNum;
            erzlNotifyVM.Date = focused.Date;
            erzlNotifyVM.Init(erzlID, ViewModelState.Edit);

            dlg.DataContext = erzlNotifyVM;
            ThemesTool.SetCurrentTheme(dlg);
            dlg.SizeToContent = SizeToContent.WidthAndHeight;
            dlg.Title = "Редактировать уведомление";

            erzlNotifyVM.PostOpen(dlg.Title);
            if (!dlg.ShowDialog().Value || focused.ContragentID <= 0) return false;
            erzlNotifyVM.SaveCard.Execute(null);
            return true;
        }

        public PortfolioFullListItem SelectPortfolio(params Portfolio.Types[] types)
        {
            return SelectPortfolio(false, types);
        }

        public PortfolioFullListItem SelectPortfolio(bool rOnly, params Portfolio.Types[] types)
        {
            return SelectPortfolio(rOnly, false, types);
        }

        public PortfolioFullListItem SelectPortfolio(bool rOnly, bool excludeTypes, params Portfolio.Types[] types)
        {
            var dlg = new SelectPFRAccountByPortfolioDlg();
            var vm = new PortfoliosListViewModel(new PortfoliosListViewModel.PortfolioTypesFilter() { Exclude = excludeTypes, Types = types, OnlyRubles = rOnly });

            PrepareDialog(dlg, vm);


            vm.SelectedItem = null;
            return dlg.ShowDialog() == true ? vm.SelectedItem : null;
        }

        public PortfolioFullListItem SelectPortfolio(PortfolioIdentifier.PortfolioPBAParams p)
        {
            var dlg = new SelectPFRAccountByPortfolioDlg();
            //Этим диалогом пользуются редко, записей там мало, пусть будет красиво:)
            //dlg.SizeToContent = SizeToContent.WidthAndHeight;
            var vm = new PortfoliosListViewModel(p);

            PrepareDialog(dlg, vm);


            //vm.SelectedItem = null;
            return dlg.ShowDialog() == true ? vm.SelectedItem : null;
        }

        public decimal? GetCourceManuallyOrFromCBRSite(long p_CurrencyID, DateTime p_CourceDate)
        {
            var vm = new GetCourceManuallyOrFromCBRSiteDialogViewModel(p_CurrencyID, p_CourceDate);
            var dlg = new GetCourceManuallyOrFromCBRSiteDialog { Owner = App.mainWindow, DataContext = vm };
            ThemesTool.SetCurrentTheme(dlg);

            bool result = dlg.ShowDialog().Value;
            return result ? vm.CourceRate : (decimal?)null;
        }

        public bool RenameSI(long ID)
        {
            var legalEntity = DataContainerFacade.GetByID<LegalEntity>(ID);
            var model = new RenameSIDialogViewModel(legalEntity);
            var renameSI = new RenameSIDlg();
            ThemesTool.SetCurrentTheme(renameSI);

            renameSI.Owner = App.mainWindow;
            renameSI.DataContext = model;



            if (!renameSI.ShowDialog().GetValueOrDefault() || !renameSI.IsNameChanged) return false;

            var dateChanged = renameSI.date.EditValue == null ? null : (DateTime?)Convert.ToDateTime(renameSI.date.EditValue);
            var newFormalizedName = renameSI.formalizedName.Text;

            //var oldSIName = new OldSIName
            //{
            //    LegalEntityID = legalEntity.ID,
            //    OldFullName = legalEntity.FullName,
            //    OldShortName = legalEntity.ShortName,
            //    OldFormalizedName = legalEntity.FormalizedName,
            //    OldINN = legalEntity.INN,
            //    Date = dateChanged
            //};
            //DataContainerFacade.Save<OldSIName, long>(oldSIName);

            var contragent = legalEntity.GetContragent();
            contragent.Name = $"{newFormalizedName} ({legalEntity.FormalizedName})";
            DataContainerFacade.Save<Contragent, long>(contragent);

            legalEntity.FullName = renameSI.fullName.Text;
            legalEntity.ShortName = renameSI.shortName.Text;
            legalEntity.FormalizedName = newFormalizedName;

            var contracts = new List<Contract>(legalEntity.GetContracts());
            foreach (var sa in contracts.Select(contract => new SupAgreement
            {
                ContractID = contract.ID,
                Kind = "Смена наименования",
                RegDate = dateChanged,
                Number = model.Num
            }))
            {
                DataContainerFacade.Save<SupAgreement, long>(sa);
            }
            return true;
        }

        public Contract ShowSelectContract()
        {
            var dlg = new SelectContractDlgView();
            var vm = new SelectContractDlgViewModel();
            PrepareDialog(dlg, vm);
            if (dlg.ShowDialog() != true) return null;
            return vm.SelectedItem;
        }

        public void ViewOldSIName(long ID)
        {
            var model = new RenameSIDialogViewModel(ID) { IsEditable = false };
            var renameSi = new RenameSIDlg
            {
                Owner = App.mainWindow,
                DataContext = model,
            };
            ThemesTool.SetCurrentTheme(renameSi);

            renameSi.ShowDialog();
        }

        public bool SuspendActivity(long caID)
        {
            var dlg = new SuspendActivityDlg { Owner = App.mainWindow };
            ThemesTool.SetCurrentTheme(dlg);
            if (!dlg.ShowDialog().Value) return false;

            //объект приостановления деятельности
            var newNPFPause = new NPFPause
            {
                StartDate = dlg.SuspendDateStart.DateTime,
                EndDate = dlg.SuspendDateEnd.DateTime,
                RegNum = dlg.RegNum.Text,
                DocumentDate = dlg.DocDate.DateTime,
                ContragentID = caID
            };
            DataContainerFacade.Save(newNPFPause);
            JournalLogger.LogEvent("Приостановление деятельности", JournalEventType.CHANGE_STATE, null, "Приостановить ЗЛ",
                $"Контрагент ID:{newNPFPause.ContragentID} Номер документа:{newNPFPause.RegNum}");
            return true;
        }

        public DateTime? AnnulLicence()
        {
            var dlg = new AnnulLicenceDlg
            {
                AnnulDate = { EditValue = DateTime.Now.Date },
                SizeToContent = SizeToContent.WidthAndHeight,
                Owner = App.mainWindow
            };
            ThemesTool.SetCurrentTheme(dlg);
            if (dlg.ShowDialog().Value)
                return (DateTime?)dlg.AnnulDate.EditValue;
            return null;
        }

        public bool ViewPayment(long ID)
        {
            var dlg = new AddPaymentDlg
            {
                Title = "Выплата"
            };
            var dlgVM = new PaymentViewModel(ID, ViewModelState.Edit);
            dlg.DataContext = dlgVM;

            ThemesTool.SetCurrentTheme(dlg);
            dlgVM.PostOpen(dlg.Title);
            if (!dlg.ShowDialog().Value) return false;
            var p = dlgVM.Payment;
            p.ID = DataContainerFacade.Save<Payment, long>(p);
            return true;
        }

        public bool CreatePFRContact(long BranchID)
        {
            var dlg = new AddPFRContactDlg();
            var dlgVM = new PFRContactViewModel(0, ViewModelState.Create);
            dlg.DataContext = dlgVM;
            dlg.Owner = App.mainWindow;
            ThemesTool.SetCurrentTheme(dlg);
            dlgVM.PostOpen(dlg.Title, true);
            if (!dlg.ShowDialog().Value) return false;
            DataContainerFacade.Save(dlgVM.Contact);
            return true;
        }

        public bool ShowLoginMessages(bool hideIgnoreBox, params LoginMessageItem[] messages)
        {
            bool res = false;

            if (messages.Count(m => !m.Ignore) <= 0) return false;

            var dlg = PrepareDialog(new LoginMessagesDlg(), new LoginMessagesDlgViewModel(messages, hideIgnoreBox));
            res = dlg.ShowDialog().Value;

            //Сохраняем список для игнора
            if (res == false && messages != null)
            {
                try
                {
                    DataContainerFacade.SetIgnoredLoginMessages(messages.Where(m => !m.Ignore).Select(m => m.Key).ToList());
                }
                catch (Exception ex)
                {
                    App.log.WriteException(ex, "Ошибка при сохранении настроек игнорирования сообщений!");
                }
            }

            return res;
        }

        public bool ShowLoginMessages(params LoginMessageItem[] messages)
        {
            return ShowLoginMessages(false, messages);
        }


        public bool GenerateDeposit()
        {
            var dlg = new GenerateDepositDlg();
            var vm = new GenerateDepositDlgViewModel();
            dlg.DataContext = vm;
            dlg.Owner = App.mainWindow;
            ThemesTool.SetCurrentTheme(dlg);

            var result = dlg.ShowDialog();
            vm.PostOpen(dlg.Title, true);

            return result.HasValue && result.Value;
        }

        public void ReturnDeposit()
        {
            var dlg = new DepositReturnDlg();
            var m = new DepositReturnDlgViewModel();
            dlg.DataContext = m;
            dlg.Owner = App.mainWindow;
            ThemesTool.SetCurrentTheme(dlg);


            if (!dlg.ShowDialog().Value) return;
            m.PostOpen(dlg.Title, true);
            var dL = m.DepositList;

            if (dL.Count == 0)
            {
                ShowAlert("На указанную дату нет депозитов к возврату");
                return;
            }

            ShowDepositReturnList(dL, m.Date.Value);
        }

        public bool CreatePerson(long divisionId)
        {
            var dlg = new AddPFRContactDlg();
            var dlgVM = new PersonViewModel(0, ViewModelState.Create);
            dlg.DataContext = dlgVM;
            dlg.Owner = App.mainWindow;
            ThemesTool.SetCurrentTheme(dlg);


            if (!dlg.ShowDialog().Value) return false;
            dlgVM.PostOpen(dlg.Title, true);
            DataContainerFacade.Save(dlgVM.Person);
            return true;
        }

        public string OpenFile(string filter)
        {
            var dialog = new OpenFileDialog { CheckFileExists = true, Multiselect = false, Filter = filter };
            string result = null;
            bool? dlgRes = dialog.ShowDialog();
            if (dlgRes.HasValue && dlgRes.Value)
                result = dialog.FileName;
            return result;
        }

        public string SaveFile(string filter)
        {
            var dialog = new SaveFileDialog { CheckFileExists = false, Filter = filter };
            return dialog.ShowDialog() != true ? null : dialog.FileName;
        }

        public Person ShowPersonLookup()
        {
            var dlg = new LookupPersonGridDlg();
            var dlgVM = new LookupPersonGridDlgViewModel();
            PrepareDialog(dlg, dlgVM);

            var res = dlg.ShowDialog();
            dlgVM.PostOpen(dlg.Title, true);
            return res == true ? dlgVM.SelectedPerson : null;
        }

        public bool ShowAddSumForPortfolio(long offerId, decimal sum, out OfferPfrBankAccountSum offerPfrBankAccountSum, int valMaxDigits = 18, bool onlyInteger = false)
        {
            var dlg = new AddSumForPortfolio(onlyInteger);
            var dlgVM = new AddSumForPortfolioViewModel(offerId, sum);
            dlgVM.ValidationMaxDigits = valMaxDigits;
            dlg.DataContext = dlgVM;
            dlg.Title = "Добавить сумму по портфелю";
            dlg.Owner = App.mainWindow;
            ThemesTool.SetCurrentTheme(dlg);

            dlgVM.PostOpen(dlg.Title, true);
            var result = dlg.ShowDialog();

            offerPfrBankAccountSum = dlgVM.Result;
            return result.HasValue && result.Value;
        }

        public bool ShowEditSumForPortfolio(OfferPfrBankAccountSum sum)
        {
            var dlg = new AddSumForPortfolio();
            var dlgVM = new AddSumForPortfolioViewModel(sum);
            dlg.DataContext = dlgVM;
            dlg.Title = "Изменить сумму по портфелю";
            dlg.Owner = App.mainWindow;
            ThemesTool.SetCurrentTheme(dlg);

            dlgVM.PostOpen(dlg.Title);
            var result = dlg.ShowDialog();

            return result.HasValue && result.Value;
        }


        public void ShowDepClaim2OfferAcceptedList(IEnumerable<DepClaimOffer> listOffer, DateTime date)
        {
            App.DashboardManager.OpenNewTab(typeof(DepClaim2OfferAcceptedList), ViewModelState.Edit, listOffer, $"Перечень акцептованных оферт на {date.ToString("dd.MM.yyyy")}");
        }

        public bool ShowDepositReturnList(IEnumerable<DepositReturnListItem> itemList, DateTime date)
        {
            App.DashboardManager.OpenNewTab(typeof(DepositReturnListView), ViewModelState.Edit, itemList, $"Перечень депозитов к возврату на {date.ToString("dd.MM.yyyy")}");

            return true;
        }

        public string[] OpenFiles(string filter)
        {
            var dialog = new OpenFileDialog { CheckFileExists = true, Multiselect = true, Filter = filter };
            bool? dlgRes = dialog.ShowDialog();
            if (dlgRes.HasValue && dlgRes.Value)
                return dialog.FileNames;
            return null;
        }

        /// <summary>
        /// Показывает диалог выбора папки
        /// </summary>
        /// <param name="description">Текст подсказки при выборе каталога</param>
        /// <returns>null  в случае отмены</returns>
        public string OpenCatalog(string description = null, string selectedPatch = null, System.Environment.SpecialFolder rootFolder = System.Environment.SpecialFolder.MyComputer)
        {
            var dlg = new FolderBrowserDialog { Description = description, SelectedPath = selectedPatch, RootFolder = rootFolder };
            var result = dlg.ShowDialog();

            return result == DialogResult.OK ? dlg.SelectedPath : null;
        }

        public bool AddUser()
        {
            var dlg = new AddUserDlg(false);
            dlg.usersList.Refresh(AP.Provider.GetAuthUser());

            dlg.Owner = App.mainWindow;
            ThemesTool.SetCurrentTheme(dlg);

            if (!dlg.ShowDialog().Value) return false;
            dlg.usersList.AddUsers();
            return true;
        }

        public bool DeleteUser()
        {
            var dlg = new AddUserDlg(true);
            dlg.usersList.Refresh(AP.Provider.GetAuthUsers());

            dlg.Owner = App.mainWindow;
            ThemesTool.SetCurrentTheme(dlg);

            if (!dlg.ShowDialog().Value) return false;
            dlg.usersList.DeleteUsers();
            return true;
        }

        public PortfolioPFRBankAccountListItem AddAccount(long[] excludeIDs)
        {
            var dlg = new SelectAccountDlg();
            var dlgVM = new SelectAccountDialogViewModel(excludeIDs);
            ThemesTool.SetCurrentTheme(dlg);
            dlg.Owner = App.mainWindow;
            dlg.DataContext = dlgVM;


            if (!dlg.ShowDialog().Value) return null;
            dlgVM.PostOpen(dlg.Title, true);
            return new PortfolioPFRBankAccountListItem
            {
                ID = dlgVM.SelectedAccountID,
                AssignKindID = dlgVM.SelectedAssign.ID,
                AssignKindName = dlgVM.SelectedAssign.Name,
            };
        }

        public void AddMP(object model)
        {
            var vMP = new MarketPriceView();
            var vmMP = new MarketPriceViewModel((SecurityViewModel)model, 0);
            App.DashboardManager.OpenNewTab("Рыночная цена", vMP, ViewModelState.Create, vmMP);
        }

        public bool IgnorZLCountOwerflow(DateTime date, long zlCount)
        {
            //            string msg = string.Format("На момент {0:dd.MM.yyyy}, выбранный портфель содержит {1} ЗЛ, что меньше чем указанная сумма. Продолжить?", date, zlCount);
            var msg = $"На момент {date:dd.MM.yyyy} выбранный портфель содержит {zlCount} ЗЛ, что меньше, чем указанная сумма. Продолжить?";
            return DXMessageBox.Show(msg, "Внимание", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes;
        }

        public bool ConfirmOwnCapitalRewrite()
        {
            var res = DXMessageBox.Show(
                $"В истории собственных капиталов уже существует данные на выбранную отчетную дату.{Environment.NewLine}Вы хотите перезаписать существующие данные?",
                                     CONFIRMATION_CAPTION,
                                    MessageBoxButton.YesNo,
                                    MessageBoxImage.Question);

            return res == MessageBoxResult.Yes;
        }

        public bool EditOwnCapital(OwnCapitalHistory x, List<OwnCapitalHistory> items, bool isNew)
        {
            var dlg = new EditOwnCapitalDlg();
            var dlgVM = new EditOwnCapitalDlgViewModel(new OwnCapitalHistory
            {
                ID = x.ID,
                LegalEntityID = x.LegalEntityID,
                LegalEntityName = x.LegalEntityName,
                Key = x.Key,
                LicenseNumber = x.LicenseNumber,
                Summ = x.Summ,
                ReportDate = x.ReportDate,
                ImportDate = x.ImportDate,
                IsImported = isNew ? (byte)0 : x.IsImported
            }, items);
            ThemesTool.SetCurrentTheme(dlg);
            dlg.Owner = App.mainWindow;
            dlg.DataContext = dlgVM;

            dlg.Title = isNew ? "Добавление собственного капитала" : "Редактирование собственного капитала";

            dlgVM.PostOpen(dlg.Title, isNew);
            if (!dlg.ShowDialog().Value) return false;
            x.ID = dlgVM.OwnCapital.ID;
            x.LegalEntityID = dlgVM.OwnCapital.LegalEntityID;
            x.LegalEntityName = dlgVM.OwnCapital.LegalEntityName;
            x.Key = dlgVM.OwnCapital.Key;
            x.Summ = dlgVM.OwnCapital.Summ;
            x.ReportDate = dlgVM.OwnCapital.ReportDate;
            x.ImportDate = dlgVM.OwnCapital.ImportDate;
            x.IsImported = 0;

            return true;
        }

        public bool ViewOwnCapitalHistory(long bankId)
        {
            var bank = DataContainerFacade.GetByID<LegalEntity>(bankId);

            var dlg = new OwnCapitalHistoryDlg();
            var dlgVM = new OwnCapitalDlgModel(bank);
            ThemesTool.SetCurrentTheme(dlg);
            dlg.Owner = App.mainWindow;
            dlg.DataContext = dlgVM;


            dlg.Title = "История обновления собственных средств";
            dlgVM.PostOpen(dlg.Title);
            if (!dlg.ShowDialog().Value)
            {
                return false;
            }
            return true;
        }

        public bool EditLegalEntityHead(LegalEntityHead head, bool isNew)
        {
            var dlg = new EditLegalEntityHeadDlg();
            var dlgVM = new LegalEntityHeadDlgViewModel(new LegalEntityHead
            {
                Position = head.Position,
                FullName = head.FullName,
                Phone = head.Phone,
                Email = head.Email
            });
            ThemesTool.SetCurrentTheme(dlg);
            dlg.Owner = App.mainWindow;
            dlg.DataContext = dlgVM;

            dlg.Title = isNew ? "Добавление контакта" : "Редактирование контакта";

            dlgVM.PostOpen(dlg.Title, isNew);
            if (!dlg.ShowDialog().Value) return false;
            head.Position = dlgVM.Head.Position;
            head.FullName = dlgVM.Head.FullName;
            head.Phone = dlgVM.Head.Phone;
            head.Email = dlgVM.Head.Email;
            return true;
        }

        public bool EditLegalEntityRating(LegalEntityRating rating, bool isNew, IList<long> excludedAgenceis)
        {
            excludedAgenceis = excludedAgenceis ?? new List<long>();
            var dlg = new EditLegalEntityRatingDlg();
            var dlgVM = new LegalEntityRatingDlgViewModel(new LegalEntityRating
            {
                //AgencyID = rating.AgencyID, 
                //RatingID = rating.RatingID, 
                Date = rating.Date,
                //Multiplier = rating.Multiplier,
                MultiplierRatingID = rating.MultiplierRatingID,
            },
                                                            !isNew,
                                                            excludedAgenceis);
            ThemesTool.SetCurrentTheme(dlg);
            dlg.Owner = App.mainWindow;
            dlg.DataContext = dlgVM;

            dlg.Title = isNew ? "Добавление рейтинга" : "Редактирование рейтинга";

            dlgVM.PostOpen(dlg.Title, isNew);
            if (!dlg.ShowDialog().Value) return false;
            //rating.AgencyID = dlgVM.Rating.AgencyID;
            //rating.RatingID = dlgVM.Rating.RatingID;
            rating.Date = dlgVM.Rating.Date;
            //rating.Multiplier = dlgVM.Rating.Multiplier;
            rating.MultiplierRatingID = dlgVM.Rating.MultiplierRatingID;

            if (rating.MultiplierRatingID != 0) return true;
            ShowAlert("Коэффициент рейтинга рейтингового агентства не найден!");
            return false;
        }

        public bool EditRating(Rating r, bool isNew)
        {
            var dlg = new RatingDlg();
            var dlgVM = new RatingDlgModel(r);
            ThemesTool.SetCurrentTheme(dlg);
            dlg.Owner = App.mainWindow;
            dlg.DataContext = dlgVM;

            dlg.Title = isNew ? "Добавление рейтинга" : "Редактирование рейтинга";

            dlgVM.PostOpen(dlg.Title, isNew);
            return dlg.ShowDialog().Value;
        }

        public bool EditBankStockCode(BankStockCode value, bool isNew, IList<long> excludedStoks)
        {
            excludedStoks = excludedStoks ?? new List<long>();
            var dlg = new EditBankStockCodeDlg();
            var dlgVM = new BankStockCodeDlgViewModel(value.Copy(), !isNew, excludedStoks);

            ThemesTool.SetCurrentTheme(dlg);
            dlg.Owner = App.mainWindow;
            dlg.DataContext = dlgVM;

            dlg.Title = isNew ? "Добавление биржевого кода" : "Редактирование биржевого кода";


            dlgVM.PostOpen(dlg.Title);
            if (!dlg.ShowDialog().Value) return false;
            dlgVM.Value.CopyTo(value);
            return true;
        }

        public DateTime? RetireChief(DateTime? inaugurationDate)
        {
            var dlg = new RetireChiefDlg { InaugurationDate = inaugurationDate, retireDate = { EditValue = DateTime.Now.Date }, SizeToContent = SizeToContent.WidthAndHeight, Owner = App.mainWindow };
            ThemesTool.SetCurrentTheme(dlg);
            if (dlg.ShowDialog().Value)
            {
                return (DateTime?)dlg.retireDate.EditValue;
            }
            return null;
        }

        #endregion

        #region NPF Courier

        public bool EditNpfCourier(LegalEntityCourier x, bool isNew)
        {
            return OpenNpfCourier(x, isNew, false);
        }

        private bool OpenNpfCourier(LegalEntityCourier x, bool isNew, bool isReadOnly)
        {
            var dlg = new LegalEntityCourierDlg();
            var dlgVM = new LegalEntityCourierDlgViewModel(new LegalEntityCourier(x));
            ThemesTool.SetCurrentTheme(dlg);
            dlg.Owner = App.mainWindow;
            dlg.DataContext = dlgVM;


            if (isReadOnly)
            {
                dlgVM.IsReadOnly = true;
                dlg.Title = "Просмотр контакта";
            }
            else
            {
                dlg.Title = isNew ? "Добавление контакта" : "Редактирование контакта";
            }


            dlgVM.PostOpen(dlg.Title, isNew);
            if (!dlg.ShowDialog().Value) return false;
            x.ApplyValues(dlgVM.Item);
            return true;
        }


        public void ShowNpfCourierDeletedList(List<LegalEntityCourier> courierListDeleted)
        {
            var dlg = new NpfCourierDeletedListDlg();
            var dlgVM = new NPFCourierDeletedListViewModel(courierListDeleted);
            ThemesTool.SetCurrentTheme(dlg);
            dlg.Owner = App.mainWindow;
            dlg.DataContext = dlgVM;

            dlg.ShowDialog();
            dlgVM.PostOpen(dlg.Title, true);
        }


        public void ViewNpfCourier(LegalEntityCourier x)
        {
            OpenNpfCourier(x, false, true);
        }

        #endregion

        #region NPF Identifier

        /*class LEIntityInfo 
        {
            public long id { private set; get; }
            public string iden { private set; get; }
            public Guid addition { private set; get; }
        }*/

        public bool EditNpfIdentifier(LegalEntityIdentifier identifier, bool isNew, List<LegalEntityIdentifier> existingLEIdentifiers)
        {
            if (identifier == null)
                throw new ArgumentNullException("identifier");
            var copy = new LegalEntityIdentifier();
            identifier.CopyTo(copy);
            if (isNew)
            {
                copy.SetDate = DateTime.Now;
                copy.StatusID = 1;
                if (copy.AdditionInfo != Guid.Empty)
                    copy.AdditionInfo = Guid.NewGuid();
            }

            var dlg = new LegalEntityIdentifierDlg();
            var dlgVM = new LegalEntityIdentifierDlgViewModel(copy, existingLEIdentifiers);
            ThemesTool.SetCurrentTheme(dlg);
            dlg.Owner = App.mainWindow;
            dlg.DataContext = dlgVM;

            dlg.Title = isNew ? "Создание идентификатора НПФ" : "Редактирование идентификатора НПФ";

            dlgVM.PostOpen(dlg.Title, isNew);
            if (dlg.ShowDialog() != true) return false;
            copy.CopyTo(identifier);
            return true;
        }

        public bool EditUKIdentifier(LegalEntityIdentifier identifier, bool isNew, List<LegalEntityIdentifier> existingLEIdentifiers)
        {
            if (identifier == null)
                throw new ArgumentNullException(nameof(identifier));
            var copy = new LegalEntityIdentifier();
            identifier.CopyTo(copy);
            if (isNew)
            {
                copy.SetDate = DateTime.Now;
                copy.StatusID = 1;
                if (copy.AdditionInfo != Guid.Empty)
                    copy.AdditionInfo = Guid.NewGuid();
            }

            var dlg = new LegalEntityIdentifierDlg();
            var dlgVM = new LegalEntityIdentifierDlgViewModel(copy, existingLEIdentifiers);
            ThemesTool.SetCurrentTheme(dlg);
            dlg.Owner = App.mainWindow;
            dlg.DataContext = dlgVM;

            dlg.Title = isNew ? "Создание идентификатора УК" : "Редактирование идентификатора УК";

            dlgVM.PostOpen(dlg.Title, isNew);
            if (dlg.ShowDialog() == true)
            {
                copy.CopyTo(identifier);
                return true;
            }

            return false;
        }

        #endregion

        #region NPF Contact

        public bool EditNpfContact(LegalEntityCourier contact, bool isNew)
        {
            if (contact == null)
                throw new ArgumentNullException("contact");
            var copy = new LegalEntityCourier();
            contact.CopyTo(copy);

            var dlg = new LegalEntityContactDlg();
            var dlgVM = new LegalEntityContactDlgViewModel(copy);
            ThemesTool.SetCurrentTheme(dlg);
            dlg.Owner = App.mainWindow;
            dlg.DataContext = dlgVM;

            dlg.Title = isNew ? "Создание контакта НПФ" : "Редактирование контакта НПФ";

            dlgVM.PostOpen(dlg.Title, isNew);
            if (dlg.ShowDialog() != true) return false;
            copy.CopyTo(contact);
            return true;
        }

        #endregion NPF Contact

        #region Bank Letter Of Attorney
        public bool EditCourierLetterOfAttorney(LegalEntityCourierLetterOfAttorney x, bool isNew)
        {
            return EditCourierLetterOfAttorney(x, isNew, false);
        }

        private bool EditCourierLetterOfAttorney(LegalEntityCourierLetterOfAttorney x, bool isNew, bool isReadOnly)
        {
            var dlg = new EditLegalEntityLetterOfAttorneyDlg();
            var dlgVM = new LegalEntityLetterOfAttorneyDlgViewModel(new LegalEntityCourierLetterOfAttorney
            {
                ID = x.ID,
                LegalEntityCourierID = x.LegalEntityCourierID,
                Number = x.Number,
                IssueDate = x.IssueDate,
                ExpireDate = x.ExpireDate,
                StockID = x.StockID,
                StockName = x.StockName,
                RegisterDate = x.RegisterDate,
                StatusID = 1

            });
            ThemesTool.SetCurrentTheme(dlg);
            dlg.Owner = App.mainWindow;
            dlg.DataContext = dlgVM;

            dlg.Title = isNew ? "Добавление доверенности" : "Редактирование доверенности";

            dlgVM.PostOpen(dlg.Title, isNew);
            if (!dlg.ShowDialog().Value) return false;
            x.ID = dlgVM.Letter.ID;
            x.LegalEntityCourierID = dlgVM.Letter.LegalEntityCourierID;
            x.Number = dlgVM.Letter.Number;
            x.IssueDate = dlgVM.Letter.IssueDate;
            x.ExpireDate = dlgVM.Letter.ExpireDate;
            x.StockID = dlgVM.Letter.StockID;
            x.StockName = dlg.SelectedStockName;
            x.RegisterDate = dlgVM.Letter.RegisterDate;
            return true;
        }

        #endregion

        #region Bank Certificate
        public bool EditCourierCertificate(LegalEntityCourierCertificate x, bool isNew)
        {
            return EditCourierCertificate(x, isNew, false);
        }

        private bool EditCourierCertificate(LegalEntityCourierCertificate x, bool isNew, bool isReadOnly)
        {
            var dlg = new EditLegalEntityCertificateDlg();
            var dlgVM = new LegalEntityCertificateDlgViewModel(new LegalEntityCourierCertificate
            {
                ID = x.ID,
                LegalEntityCourierID = x.LegalEntityCourierID,
                Signature = x.Signature,
                ExpireDate = x.ExpireDate,
                StockID = x.StockID,
                StockName = x.StockName,
                RegisterDate = x.RegisterDate,
                StatusID = 1

            });
            ThemesTool.SetCurrentTheme(dlg);
            dlg.Owner = App.mainWindow;
            dlg.DataContext = dlgVM;

            dlg.Title = isNew ? "Добавление сертификата" : "Редактирование сертификата";

            dlgVM.PostOpen(dlg.Title, isNew);
            if (!dlg.ShowDialog().Value) return false;
            x.ID = dlgVM.Certificate.ID;
            x.LegalEntityCourierID = dlgVM.Certificate.LegalEntityCourierID;
            x.Signature = dlgVM.Certificate.Signature;
            x.ExpireDate = dlgVM.Certificate.ExpireDate;
            x.StockID = dlgVM.Certificate.StockID;
            x.StockName = dlg.SelectedStockName;
            x.RegisterDate = dlgVM.Certificate.RegisterDate;
            return true;
        }

        #endregion

        #region Bank Person

        public bool EditBankPerson(LegalEntityCourier x, bool isNew)
        {
            return OpenBankPerson(x, isNew, false);
        }

        private bool OpenBankPerson(LegalEntityCourier x, bool isNew, bool isReadOnly)
        {
            var dlg = new LegalEntityPersonDlg();
            var orig = new LegalEntityCourier(x).Copy();
            var dlgVM = new BankCourierDlgViewModel(new LegalEntityCourier(x));

            PrepareDialog(dlg, dlgVM);



            if (isReadOnly)
            {
                dlgVM.IsReadOnly = true;
                dlg.Title = "Просмотр контакта";
            }
            else
            {
                if (isNew)
                {
                    dlg.Title = "Добавление контакта";
                    dlgVM.Item.LetterOfAttorneyExpireAlert = 0;
                    dlgVM.Item.SignatureExpireAlert = 0;
                }
                else
                    dlg.Title = "Редактирование контакта";
            }


            dlgVM.PostOpen(dlg.Title, isNew);
            if (!dlg.ShowDialog().Value)
            {
                x = orig;
                return false;
            }
            x.ApplyValues(dlgVM.Item);
            return true;
        }


        //public void ShowBankPersonDeletedList(List<LegalEntityCourier> personListDeleted)
        //{
        //    var dlg = new NpfCourierDeletedListDlg();
        //    var dlgVM = new NPFCourierDeletedListViewModel(personListDeleted);
        //    ThemesTool.SetCurrentTheme(dlg);
        //    dlg.Owner = App.mainWindow;
        //    dlg.DataContext = dlgVM;

        //    dlg.ShowDialog();
        //    dlgVM.PostOpen(dlg.Title);
        //}


        //public void ViewBankPerson(LegalEntityCourier x)
        //{
        //    OpenBankPerson(x, false, true);
        //}

        #endregion


        public void CreateCBMonthlyReport()
        {
            var vm = new SelectCBReportYearQuartalDlgViewModel(false, true, true);
            if (vm.YearsList.Count == 0 || vm.MonthsList == null || vm.MonthsList.Count == 0)
            {
                ShowExclamation("Отсутствуют допустимые года или месяцы для создания новой информации!", "Внимание");
                return;
            }
            var v = new SelectCBReportYearQuartalDlg(false, true);
            PrepareDialog(v, vm);
            if (v.ShowDialog() != true) return;

            var year = vm.SelectedYear ?? 0;
            var month = vm.SelectedMonth.Number + 1;
            var id = DataContainerFacade.Save(new BOReportForm1
            {
                ReportType = 1,
                CreateDate = DateTime.Today,
                UpdateDate = DateTime.Today,
                Year = year,
                Quartal = month,
                OKUD = null,
                Status = 1,
                Date = new DateTime(year, month, DateTime.DaysInMonth(year, month)),
            });
            ViewModelBase.ViewModelManager.UpdateDataInAllModels(typeof(BOReportForm1), id);
        }

        public void CBMonthlyReportExport(long reportId)
        {
            var report = DataContainerFacade.GetByID<BOReportForm1>(reportId);
            var dlg = new SelectCBReportParamsDlg();
            var vm = new SelectCBReportParamsDlgViewModel(report, true);
            PrepareDialog(dlg, vm);
            if (dlg.ShowDialog() != true) return;
            using (Loading.StartNew("Генерация отчета..."))
            {
                SaveCBReportDialogData(report, new[] { vm.INN, vm.OGRN, vm.OKATO, vm.FullName, vm.ShortName, vm.RegNum }, vm.SelectedPerson.ID, vm.RegDate);
                new CBMonthlyReportExportToXMLViewModel().GenerateXML(report, vm.SelectedPerson, vm.FileName);
            }
            ViewModelBase.ViewModelManager.UpdateDataInAllModels(typeof(BOReportForm1), reportId);
        }

        public void OpenCBReportApprovementStatus(long? id, bool isApproveMode = false)
        {
            if (!id.HasValue) return;

            App.mainWindow.Dispatcher.BeginInvoke(new Action(() =>
            {
                var dlg = new CBReportApprovementStatusDlg();
                var vm = new CBReportApprovementStatusDlgViewModel(id.Value, isApproveMode);
                PrepareDialog(dlg, vm);

                dlg.ShowDialog();
            }), System.Windows.Threading.DispatcherPriority.ApplicationIdle, null);
        }

        public List<long> ShowOnesSelectExportDataDlg(OnesSettingsDef.IntGroup group)
        {
            var dlg = new OnesSelectExportDataDlg(group);
            var dlgVM = new OnesSelectExportDataViewModel(group);
            PrepareDialog(dlg, dlgVM);
            return dlg.ShowDialog() != true ? null : dlgVM.List.Where(a => a.Selected).Select(a => a.ID).ToList();
        }

        #region BOReportForm1 - Отчет для БО #1

        public bool BOReportForm1CheckForAvailability(int quarter, int year)
        {
            var vm = WCFClient.Client.GetBOReportForm1FreeYearsAndQaurtersList(BOReportForm1.ReportTypeEnum.One);
            if (!vm.ContainsKey(year) || !vm[year].Contains(quarter))
            {
                ViewModelBase.DialogHelper.ShowAlert("Отчет на выбранные год и квартал уже заведен в системе! Создание нового отчета по этим же данным невозможно!");
                return false;
            }
            return true;
        }

        /// <summary>
        /// Создание отчетов 1,2,3 для ЦБ
        /// </summary>
        /// <param name="forceOne">Форсировать выбор только 1 очтета</param>
	    public bool CreateReportForCB(bool forceOne)
        {
            var list = forceOne ? new List<BOReportForm1.ReportTypeEnum> { BOReportForm1.ReportTypeEnum.One } : new List<BOReportForm1.ReportTypeEnum> { BOReportForm1.ReportTypeEnum.Two, BOReportForm1.ReportTypeEnum.Three };
            var res = WCFClient.Client.HasCBReportFreeYearsAndQaurters(list);
            if (!res)
            {
                ShowAlert("Все отчеты на доступные Года/Кварталы уже созданы!");
                return false;
            }
            var model = new SelectCBReportYearQuartalDlgViewModel(forceOne);
            var view = new SelectCBReportYearQuartalDlg(forceOne);
            //контекст спецом отдельно тут,чтобы евент подхватился
            view.DataContext = model;
            ThemesTool.SetCurrentTheme(view);
            if (view.ShowDialog() == false) return false;

            if (model.SelectedReportType == BOReportForm1.ReportTypeEnum.One)
            {
                //Отчет в Банк России (форма №1) за .... квартал .... года
                App.DashboardManager.OpenNewTab(typeof(BOReportForm1View),
                    ViewModelState.Create,
                    new[] { model.SelectedQuarter.Number, model.SelectedYear.Value },
                    $@"Отчет в Банк России (форма №1) за {model.SelectedQuarter.Text} {model.SelectedYear.Value} года");
            }
            else
            {
                var id = WCFClient.Client.CBReportCreate(model.SelectedYear.Value, model.SelectedQuarter.Number, model.SelectedReportType);
                RefreshCBReportManagerList(id);
            }

            return true;
        }

        private void SaveCBReportDialogData(BOReportForm1 report, string[] strings, long personId, DateTime? date)
        {
            report.INN = strings[0];
            report.OGRN = strings[1];
            report.OKATO = strings[2];
            report.FullName = strings[3];
            report.ShortName = strings[4];
            report.RegNum = strings[5];
            report.PersonID = personId;
            report.RegDate = date;
            report.UpdateDate = DateTime.Today;
            DataContainerFacade.Save(report);
        }

        public void CBReportExportToWord(long reportId, bool skipGeneration = false)
        {
            var report = WCFClient.Client.GetCBReport(reportId);
            if (!skipGeneration)
            {
                var dlg = new SelectCBReportParamsDlg();
                var vm = new SelectCBReportParamsDlgViewModel(report);
                PrepareDialog(dlg, vm);
                if (dlg.ShowDialog() != true) return;
                using (Loading.StartNew("Генерация отчета..."))
                {
                    SaveCBReportDialogData(report, new[] { vm.INN, vm.OGRN, vm.OKATO, vm.FullName, vm.ShortName, vm.RegNum }, vm.SelectedPerson.ID, vm.RegDate);
                    new CBReportWordExportViewModel(report, vm.SelectedPerson).GenerateReport();
                }
            }
            else
            {
                var p = DataContainerFacade.GetByID<Person>(report.PersonID);
                new CBReportWordExportViewModel(report, p).GenerateReport();
            }
        }

        public void CBReportExportToXml(long reportId)
        {
            var report = WCFClient.Client.GetCBReport(reportId);
            var dlg = new SelectCBReportParamsDlg();
            var vm = new SelectCBReportParamsDlgViewModel(report, true);
            PrepareDialog(dlg, vm);
            if (dlg.ShowDialog() != true) return;
            using (Loading.StartNew("Генерация XML..."))
            {
                SaveCBReportDialogData(report, new[] { vm.INN, vm.OGRN, vm.OKATO, vm.FullName, vm.ShortName, vm.RegNum }, vm.SelectedPerson.ID, vm.RegDate);
                if (!new CBReportExportToXMLViewModel().GenerateXML(report, vm.SelectedPerson, vm.FileName))
                {
                    ShowError("При генерации или валидации файла произошли ошибки! Доп. информация доступна в логе приложения.");
                }
                RefreshCBReportManagerList(reportId);
            }
        }

        public void RefreshBOReportForm1(BOReportForm1 report, bool showAfterUpdate = true)
        {
            if (!ShowConfirmation("При обновлении отчета данные ранее сформированного отчета удалятся!\r\nВы действительно хотите обновить отчет?")) return;
            // var vm = new BOReportForm1ViewModel(report.ID, true);
            //vm.SaveCard.Execute(null);
            if (showAfterUpdate)
            {
                App.DashboardManager.OpenNewTab(typeof(BOReportForm1View),
                    ViewModelState.Edit,
                    report.ID,
                    true,
                    $@"Отчет в Банк России (форма №1) за {DateTools.GetQuarterRomeName(report.Quartal)} {report.Year} года");
            }

            RefreshCBReportManagerList(report.ID);
        }

        public BOReportForm1Sum BOReportForm1AddSum(long reportId)
        {
            OfferPfrBankAccountSum result;
            return !ShowAddSumForPortfolio(0, 0, out result, 15, true) ? null : new BOReportForm1Sum { ReportID = reportId, Sum = result.Sum, PortfolioID = result.PortfolioId, PortfolioName = result.Name };
        }

        public void RefreshCBReportManagerList(long id = 0)
        {
            var model = (CBReportManagerViewModel)App.DashboardManager.FindViewModel(typeof(CBReportManagerViewModel));
            model?.RefreshGrid(id);
        }

        #endregion

        public void ShowErrorReport(ErrorReportType type)
        {
            ViewModelBase vm;
            string title;
            switch (type)
            {
                case ErrorReportType.Tipology:
                    vm = new ReportErrorTypesViewModel();
                    title = "Выберите период отчёта";
                    break;
                case ErrorReportType.Npf:
                    vm = new ReportErrorNpfSpreadViewModel();
                    title = "Выберите период отчёта";
                    break;
                case ErrorReportType.Region:
                    vm = new ReportErrorRegionSpreadViewModel();
                    title = "Выберите период отчёта";
                    break;
                case ErrorReportType.Month:
                    vm = new ReportErrorMonthSpreadViewModel();
                    title = "Выберите период отчёта";
                    break;
                default:
                    throw new Exception("Неизвестный тип отчета об ошибках!");
            }
            var dlg = new ReportErrorTypesDlg(type) { DataContext = vm, Owner = App.mainWindow, Title = title };
            vm.PostOpen(dlg.Title, true);
            ThemesTool.SetCurrentTheme(dlg);
            dlg.ShowDialog();
        }

        public void SelectReqTransferFromAssPay(long? ukPlanId)
        {
            if (!ukPlanId.HasValue) return;
            var model = new SelectReqTransferForAssPayDlgViewModel(ukPlanId.Value);
            long id;
            switch (model.List.Count)
            {
                case 0:
                    return;
                case 1:
                    id = model.List.First().ReqTransfer.ID;
                    break;
                default:
                    var dlg = PrepareDialog(new SelectReqTransferForAssPayDlgView(), model);
                    if (dlg.ShowDialog() != true) return;
                    id = model.SelectedItem.ReqTransfer.ID;
                    break;
            }

            App.DashboardManager.OpenNewTab(typeof(ReqTransferView), ViewModelState.Edit, id, "Перечисление");
        }

        public bool? ConfirmLegalEntityIdentifierSaveOnClose()
        {
            var res = DXMessageBox.Show("Вы хотите сохранить изменения?",
                                     CONFIRMATION_CAPTION,
                                    MessageBoxButton.YesNoCancel,
                                    MessageBoxImage.Question);

            if (res == MessageBoxResult.Cancel)
                return null;

            return res == MessageBoxResult.Yes;
        }

        public bool? ConfirmLegalEntityCourierSaveOnClose()
        {
            var res = DXMessageBox.Show("Вы хотите сохранить изменения?",
                                     CONFIRMATION_CAPTION,
                                    MessageBoxButton.YesNoCancel,
                                    MessageBoxImage.Question);

            if (res == MessageBoxResult.Cancel)
                return null;

            return res == MessageBoxResult.Yes;
        }

        public bool? ConfirmSaveOnClose()
        {
            var res = DXMessageBox.Show("Вы хотите сохранить изменения?",
                                     CONFIRMATION_CAPTION,
                                    MessageBoxButton.YesNoCancel,
                                    MessageBoxImage.Question);

            if (res == MessageBoxResult.Cancel)
                return null;

            return res == MessageBoxResult.Yes;
        }

        public void AddViolationCategory()
        {
            var model = App.DashboardManager.FindViewModel(typeof(ViolationCategoryViewModel));
            if (model == null)
                App.DashboardManager.OpenNewTab(typeof(ViolationCategoryView), ViewModelState.Create, "Создать категорию нарушения", "Категория нарушения");
        }

        public void ShowEditDepositSetPercent(List<DepositReturnListItem> list)
        {
            var copy = list.Select(d => d.Copy()).ToList();
            var vm = new EditDepositSetPercentDlgViewModel(copy);
            var dlg = PrepareDialog(new EditDepositSetPercentDlg(), vm);
            if (dlg.ShowDialog() != true) return;
            foreach (var d in list)
            {
                var item = copy.FirstOrDefault(p => p.SumID == d.SumID);
                if (item != null)
                {
                    var amnt = (item.Amount - d.Amount);
                    d.AmountSumPercent += amnt;
                    d.Amount = item.Amount;
                    //запоминаем плановую сумму процентов, если она null присваиваем новую сумму напрямую, если нет - инкрементируем разницу
                    d.AmountPercPlanned = d.AmountPercPlanned == null ? item.Amount : (d.AmountPercPlanned + amnt);
                }
            }
        }

        public bool ShowEditDepositReturnSum(ref DateTime paymentDate, ref decimal amountActual, decimal amountMax)
        {
            var dlg = new EditDepositReturnSumDlg();
            var m = new EditDepositReturnSumDlgViewModel(paymentDate, amountActual, amountMax);
            dlg.DataContext = m;
            dlg.Owner = App.mainWindow;
            ThemesTool.SetCurrentTheme(dlg);

            var result = dlg.ShowDialog();
            m.PostOpen(dlg.Title);

            bool res = result ?? false;

            if (!res) return false;
            paymentDate = m.PaymentDate.Value;
            amountActual = m.AmountActual;

            return true;
        }


        public ReportIPUKPrompt ShowReportIPUKPrompt(bool isTotal = false)
        {
            var model = new ReportIPUKPromtViewModel(isTotal);
            var dlg = PrepareDialog(new ReportIPUKPromtDlg(isTotal), model);

            var result = dlg.ShowDialog();
            return result == true ? model.GetReportPrompt() : null;
        }

        public ReportDepositsPrompt ShowReportDepositsPrompt()
        {
            var model = new ReportDepositsPromtViewModel();
            var dlg = PrepareDialog(new ReportDepositsPromtDlg(), model);

            var result = dlg.ShowDialog();
            return result == true ? model.GetReportPrompt() : null;
        }

        public ReportRSASCAPrompt ShowReportRSASCAPrompt(Document.Types type)
        {
            var model = new ReportRSASCAPromtViewModel(type);
            var dlg = PrepareDialog(new ReportRSASCAPromtDlg(), model);

            var result = dlg.ShowDialog();
            return result == true ? model.GetReportPrompt() : null;
        }

        public ReportInvestRatioPrompt ShowReportInvestRatioPrompt(Document.Types type)
        {
            var model = new ReportInvestRatioPromtViewModel(type);
            var dlg = PrepareDialog(new ReportInvestRatioPromtDlg(), model);

            var result = dlg.ShowDialog();
            return result == true ? model.GetReportPrompt() : null;
        }

        public ReportF060Prompt ShowReportF060Prompt(Document.Types type)
        {
            var model = new ReportF060PromtViewModel(type);
            var dlg = PrepareDialog(new ReportF060PromtDlg(), model);

            var result = dlg.ShowDialog();
            return result == true ? model.GetReportPrompt() : null;
        }

        public ReportF070Prompt ShowReportF070Prompt(Document.Types type)
        {
            var model = new ReportF070PromtViewModel(type);
            var dlg = PrepareDialog(new ReportF070PromtDlg(), model);

            var result = dlg.ShowDialog();
            return result == true ? model.GetReportPrompt() : null;
        }

        public ReportFile ShowReportOpsFromNpfPrompt()
        {
            var dlg = new ReportOpsFromNpfPromptDlg();
            ThemesTool.SetCurrentTheme(dlg);
            if (dlg.ShowDialog() != true) return null;

            using (Loading.StartNew("Генерация отчёта"))
            {
                return WCFClient.Client.CreateReportForNpfOpsReport((int)dlg.YearID+2000, (int)dlg.QuarterID, dlg.IsToNpfOperation, dlg.ResultNpfIds.ToList(), dlg.LoadUnlinkedPP, dlg.IsFormalizedName);
            }
        }

        public ReportProfitabilityPrompt ShowReportProfitabilityPrompt(Document.Types type)
        {
            var model = new ReportProfitabilityPromtViewModel(type);
            var dlg = PrepareDialog(new ReportProfitabilityPromtDlg(), model);

            var result = dlg.ShowDialog();
            return result == true ? model.GetReportPrompt() : null;
        }
        public ReportOwnedFoundsPrompt ShowReportOwnedFoundsPrompt(Document.Types type)
        {
            var model = new ReportOwnedFoundsPromtViewModel(type);
            var dlg = PrepareDialog(new ReportOwnedFoundsPromtDlg(), model);

            var result = dlg.ShowDialog();
            return result == true ? model.GetReportPrompt() : null;
        }


        public ReportBalanceUKPrompt ShowReportBalanceUKPrompt()
        {
            var model = new ReportBalanceUKPromtViewModel();
            var dlg = PrepareDialog(new ReportBalanceUKPromtDlg(), model);

            var result = dlg.ShowDialog();
            return result == true ? model.GetReportPrompt() : null;
        }

        public CommonRegisterListItem ShowSelectRegister(AsgFinTr.Sections section, AsgFinTr.Directions direction, string content = null)
        {
            var model = new SelectCommonRegisterDlgViewModel(section, (long)direction, content);
            var dlg = PrepareDialog(new SelectCommonRegisterDlgView(), model);

            var result = dlg.ShowDialog();
            return result == true ? model.SelectedItem : null;
        }


        public CommonRegisterListItem ShowSelectRegisterForPP(AsgFinTr.Sections section, long direction)
        {
            var model = new SelectCommonRegisterDlgViewModel(section, (long)direction, null, true);
            var dlg = PrepareDialog(new SelectCommonRegisterDlgView(), model);

            var result = dlg.ShowDialog();
            return result == true ? model.SelectedItem : null;
        }
        public FinregisterListItem SelectFinregister(long registerID)
        {
            var model = new SelectFinregisterDlgViewModel(registerID);
            var dlg = PrepareDialog(new SelectFinregisterDlgView(), model);

            var result = dlg.ShowDialog();
            return result == true ? model.SelectedItem : null;
        }

        /// <summary>
        /// Выбор перечисления для СИ
        /// Исп: при добавлении п/п в БО
        /// </summary>
        /// <param name="registerID">ID реестра</param>
        public ReqTransferListItem SelectReqTransferSI(long registerID)
        {
            var model = new SelectReqTransferDlgViewModel(registerID, Document.Types.SI);
            var dlg = PrepareDialog(new SelectReqTransferDlgView(false), model);

            var result = dlg.ShowDialog();
            return result == true ? model.SelectedItem : null;
        }

        /// <summary>
        /// Выбор перечисления для ВР
        /// Исп: при добавлении п/п в БО
        /// </summary>
        /// <param name="registerID">ID реестра</param>
        public ReqTransferListItem SelectReqTransferVR(long registerID)
        {

            var model = new SelectReqTransferDlgViewModel(registerID, Document.Types.VR);
            var dlg = PrepareDialog(new SelectReqTransferDlgView(false), model);

            var result = dlg.ShowDialog();
            return result == true ? model.SelectedItem : null;
        }

        public ReqTransferListItem SelectReqTransferOPFR(long registerID)
        {

            var model = new SelectReqTransferDlgViewModel(registerID, Document.Types.OPFR);
            var dlg = PrepareDialog(new SelectReqTransferDlgView(true), model);

            var result = dlg.ShowDialog();
            return result == true ? model.SelectedItem : null;
        }


        public AsgFinTr SelectUnlinkedCommonPP(long? trID, AsgFinTr.Sections section, AsgFinTr.Directions direction)
        {
            var model = new SelectCommonPPDlgViewModel(trID, section, direction);
            var dlg = PrepareDialog(new SelectCommonPPDlgView(), model);

            var result = dlg.ShowDialog();
            return result == true ? model.SelectedItem : null;
        }



        public void ShowOnesPkipPathSettings(int setting)
        {
            var model = new OnesPkipPathSettingsDlgViewModel(setting);
            var dlg = PrepareDialog(new OnesPkipPathSettingsDlg(setting), model);

            dlg.ShowDialog();
        }

        public void ShowOnesDefSettings()
        {
            var model = new OnesDefSettingsDlgViewModel();
            var dlg = PrepareDialog(new OnesDefSettingsDlg(), model);
            dlg.WindowStyle = WindowStyle.SingleBorderWindow;

            dlg.ShowDialog();
        }

        public bool EditCBPaymentDetail(CBPaymentDetail item, bool isNew)
        {
            if (item == null)
                throw new ArgumentNullException("item");
            var copy = new CBPaymentDetail();
            item.CopyTo(copy);

            var dlg = new CBPaymentDetailDlg();
            var dlgVM = new CBPaymentDetailDlgViewModel(copy);
            ThemesTool.SetCurrentTheme(dlg);
            dlg.Owner = App.mainWindow;
            dlg.DataContext = dlgVM;

            dlg.Title = isNew ? "Создание назначения платежа для Отчета в ЦБ" : "Редактирование назначения платежа для Отчета в ЦБ";

            dlgVM.PostOpen(dlg.Title, isNew);
            if (dlg.ShowDialog() != true) return false;
            copy.CopyTo(item);

            return true;
        }

        public void ImportOpfr(long id)
        {
            var dlg = new ImportOpfrTransfersDlg();
            var dlgVM = new ImportOpfrTransfersDlgViewModel(id);
            ThemesTool.SetCurrentTheme(dlg);
            dlg.Owner = App.mainWindow;
            dlg.DataContext = dlgVM;
            dlgVM.PostOpen(dlg.Title);
            dlg.ShowDialog();
        }

        /// <summary>
        /// Показать индикатор загрузки через модель
        /// </summary>
        /// <param name="message">Сообщение</param>
	    public void ShowLoadingIndicator(string message = null)
        {
            Loading.StartNew(message);
        }

        /// <summary>
        /// Убрать индикатор загрузки через модель
        /// </summary>
	    public void HideLoadingIndicator()
        {
            Loading.CloseWindow();
        }

        public void CreateCBReportWizard()
        {
            var vm = new CBReportWizardWindowViewModel();
            vm.InitializeSelectCB();
            var dlg = new CBReportWizardWindow { Owner = App.mainWindow, DataContext = vm };

            if (vm.YearsList==null || vm.YearsList.Count == 0 || vm.QuartersList == null || vm.QuartersList.Count==0
                || vm.OkudList == null || vm.OkudList.Count==0)
            {
                ShowExclamation("Отсутствуют данные для согласования!", "Внимание");
                return;
            }

            ThemesTool.SetCurrentTheme(dlg);
            dlg.ShowDialog();
        }

        public void OpenSpnDepositWizard(long id, int auctionStatusID)
        {
            if (auctionStatusID >= (int)DepClaimSelectParams.Statuses.OfferCreated) return;
            App.DashboardManager.OpenNewTab(typeof(SpnDepositWizardView), ViewModelState.Create, id, "Мастер размещения СПН на депозиты");
        }

        /// <summary>
        /// Метод отображает любой UserControl в виде модального окна, с добавлением стандартных кнопок, заголовком, произвольным текстом кнопки Ок
        /// </summary>
        /// <param name="vm">VievModel для контрола</param>
        /// <param name="view">View контрола</param>
        /// <param name="header">Заголовок</param>
        /// <param name="buttons">Стандартные кнопки</param>
        /// <param name="okButtonText">Текст кнопки Ок (по умолчанию: Выполнить)</param>
        /// <returns></returns>
        public static MessageBoxResult ShowControlAsDialogBox(object vm, System.Windows.Controls.UserControl view, string header,
            DialogButtons buttons = DialogButtons.OkCancel, string okButtonText = "Выполнить")
        {
            var bt = buttons == DialogButtons.YesNoCancel || buttons == DialogButtons.YesNo ? (MessageBoxButton)(buttons + 1) : (MessageBoxButton)buttons;
            view.DataContext = vm;
            var d = new DXDialog(header, buttons)
            {
                Owner = App.mainWindow,
                ResizeMode = ResizeMode.NoResize,
                Content = view,
                SizeToContent = SizeToContent.WidthAndHeight,
                WindowStartupLocation = WindowStartupLocation.CenterScreen
            };
            d.Loaded += (sender, args) =>
            {
                //Выравнивание кнопок по центру {
                var sPan = (d.OkButton.Parent as StackPanel);
                if (sPan != null)
                {
                    sPan.HorizontalAlignment = HorizontalAlignment.Center;
                }
                d.OkButton.HorizontalAlignment = d.CancelButton.HorizontalAlignment = HorizontalAlignment.Center;
                //}
                d.OkButton.Content = okButtonText;
                var width = okButtonText.Length * 8;
                d.OkButton.Width = width < 65 ? 65 : width;
                d.CancelButton.Width = width < 65 ? 65 : width;
            };
            ThemesTool.SetCurrentTheme(d);
            return d.ShowDialog(bt);
        }
        /// <summary>
        /// Метод отображает любой UserControl в виде модального окна, с добавлением стандартных кнопок, заголовком, произвольным текстом кнопки Ок
        /// </summary>
        /// <param name="vm">VievModel для контрола</param>
        /// <param name="view">View контрола</param>
        /// <param name="header">Заголовок</param>
        /// <param name="buttons">Стандартные кнопки</param>
        /// <param name="okButtonText">Текст кнопки Ок (по умолчанию: Выполнить)</param>
        /// <param name="okAllowPropName">Имя bool свойства в VM для проверки валидации (по умолчанию IsValid)</param>
        /// <returns></returns>
        public static MessageBoxResult ShowControlValidateAsDialogBox(object vm, System.Windows.Controls.UserControl view, string header,
           DialogButtons buttons = DialogButtons.OkCancel, string okButtonText = "Выполнить", string okAllowPropName = "IsValid")
        {
            var bt = buttons == DialogButtons.YesNoCancel || buttons == DialogButtons.YesNo ? (MessageBoxButton)(buttons + 1) : (MessageBoxButton)buttons;
            view.DataContext = vm;
            var d = new DXDialog(header, buttons)
            {
                Owner = App.mainWindow,
                ResizeMode = ResizeMode.NoResize,
                Content = view,
                SizeToContent = SizeToContent.WidthAndHeight,
                WindowStartupLocation = WindowStartupLocation.CenterScreen
            };

            d.Loaded += (sender, args) =>
            {
                //Выравнивание кнопок по центру {
                var sPan = (d.OkButton.Parent as StackPanel);
                if (sPan != null)
                {
                    sPan.HorizontalAlignment = HorizontalAlignment.Center;
                }
                d.OkButton.HorizontalAlignment = d.CancelButton.HorizontalAlignment = HorizontalAlignment.Center;
                //}
                d.OkButton.Content = okButtonText;
                var width = okButtonText.Length * 10;
                d.OkButton.Width = width < 65 ? 65 : width;
                d.CancelButton.Width = width < 65 ? 65 : width;
                var bind = new System.Windows.Data.Binding
                {
                    Source = vm,
                    Path = new PropertyPath(okAllowPropName),
                    Mode = BindingMode.OneWay,
                    FallbackValue = true,
                    UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
                };
                d.OkButton.SetBinding(UIElement.IsEnabledProperty, bind);
            };
            ThemesTool.SetCurrentTheme(d);
            return d.ShowDialog(bt);
        }

        public long[] ShowAddTransferDialog(long direction)
        {
            var vm = new AddOpfrTransferViewModel(direction);
            if (ShowControlValidateAsDialogBox(vm, new AddOpfrTransfer(), "Добавление перечисления ОПФР", DialogButtons.OkCancel,
                    "Добавить") == MessageBoxResult.OK)
            {
                return new[] {vm.LinkedRegisterID.Value, vm.LinkedTransferID.Value};
            }
            return null;
        }
    }
}
