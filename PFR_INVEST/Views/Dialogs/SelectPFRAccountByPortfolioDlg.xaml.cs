﻿using System.Windows;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for SelectPFRAccountByPortfolioDlg.xaml
    /// </summary>
    public partial class SelectPFRAccountByPortfolioDlg
    {
        public SelectPFRAccountByPortfolioDlg()
        {
            InitializeComponent();
            SelectPFRAccountView.OnGridRowDoubleClickedDelegate += SelectPFRAccountView_OnGridRowDoubleClickedDelegate;
        }

        private void CloseOnSelect()
        {
            DialogResult = true;
        }

        private void SelectPFRAccountView_OnGridRowDoubleClickedDelegate()
        {
            CloseOnSelect();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            CloseOnSelect();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
