﻿using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for ERZLNotifyDlg.xaml
    /// </summary>
    public partial class ERZLNotifyDlg
    {
        public ERZLNotifyDlg()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }
    }
}
