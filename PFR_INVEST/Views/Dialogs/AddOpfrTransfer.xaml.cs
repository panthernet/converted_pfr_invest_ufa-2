﻿using System.Windows;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for AddBranch.xaml
    /// </summary>
    public partial class AddOpfrTransfer
    {
        public AddOpfrTransferViewModel ViewModel => DataContext as AddOpfrTransferViewModel;

        public AddOpfrTransfer()
        {
            InitializeComponent();
        }
    }
}
