﻿using DevExpress.Xpf.Grid;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views.RegionsReport
{
    /// <summary>
    /// Interaction logic for RegionReportImportXmlDlg.xaml
    /// </summary>
    public partial class RegionReportImportXmlDlg
    {
		public RegionReportImportXmlDlg()
        {
            InitializeComponent();
            ModelInteractionHelper.SignUpForCloseRequest(this);
        }

        private void GridControl_OnCustomColumnSort(object sender, CustomColumnSortEventArgs e)
        {
            e.Result = ComponentHelper.OnColumnCustomSort(sender, e);
            e.Handled = true;
        }

        private void OnShowFilterPopup(object sender, FilterPopupEventArgs e)
        {
            ComponentHelper.OnColumnFilterOptionsSort(sender, e);
        }
    }
}
