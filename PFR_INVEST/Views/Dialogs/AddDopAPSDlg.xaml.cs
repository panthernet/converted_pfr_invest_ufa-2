﻿using System.Windows;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for AddDopAPSDlg.xaml
    /// </summary>
    public partial class AddDopAPSDlg
    {
        public AddDopAPSDlg()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            var vm = DataContext as PrepaymentAccurateViewModel;
            if (vm != null)
            {
                //vm.SaveCard.Execute(null);
                DialogResult = true;
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
