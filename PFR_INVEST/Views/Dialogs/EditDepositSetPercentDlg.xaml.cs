﻿using System;
using System.Windows;
using System.Windows.Input;
using DevExpress.Xpf.Grid;
using DevExpress.Xpf.Editors;
using System.Globalization;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views.Dialogs
{
	public partial class EditDepositSetPercentDlg
	{
		public EditDepositSetPercentDlg()
		{
			InitializeComponent();
		    ModelInteractionHelper.SignUpForCloseRequest(this);
		}

		private void CancelButton_Click(object sender, RoutedEventArgs e)
		{
			DialogResult = false;
		}

		private void tableView_CellValueChanging(object sender, CellValueChangedEventArgs e)
		{
			var val = (sender as TableView).ActiveEditor.EditValue;
			if (val is string)
			{
				decimal num;
				if (decimal.TryParse(val.ToString().Replace(",", "."), NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out num))
					(sender as TableView).ActiveEditor.EditValue = Math.Abs(num);
			}
            if(val is decimal)
                (sender as TableView).ActiveEditor.EditValue = Math.Abs((decimal)val);
			(sender as TableView).PostEditor();
			Grid.UpdateTotalSummary();
		}

		private void tableView_ShownEditor(object sender, EditorEventArgs e)
		{
			CommandManager.AddPreviewExecutedHandler((BaseEdit)e.Editor, Editor_PreviewExecuted);
		}

		private void Editor_PreviewExecuted(object sender, ExecutedRoutedEventArgs e)
		{
			//var commandName = ((System.Windows.Input.RoutedUICommand)(e.Command)).Text;
			//if (commandName == "Paste" || commandName == "Cut" || commandName == "Undo" || commandName == "Redo")
			//    e.Handled = true;
		}



	}
}
