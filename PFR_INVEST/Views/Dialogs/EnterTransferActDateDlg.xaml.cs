﻿using System;
using System.Windows;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for EnterTransferActDateDlg.xaml
    /// </summary>
    public partial class EnterTransferActDateDlg
    {
        private ReqTransferViewModel Model => DataContext as ReqTransferViewModel;

        public EnterTransferActDateDlg()
        {
            InitializeComponent();
            dtActDate.EditValueChanged += dtActDate_EditValueChanged;
            dtActDate.EditValue = DateTime.Now;
        }

        void dtActDate_EditValueChanged(object sender, DevExpress.Xpf.Editors.EditValueChangedEventArgs e)
        {
            okButton.IsEnabled = dtActDate.EditValue != null;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (Model != null)
            {
                Model.TransferActDate = dtActDate.EditValue == null ? null : (DateTime?)Convert.ToDateTime(dtActDate.EditValue);
                Model.TransferActNumber = GenerateActNumber(Model.TransferActDate, Model.TransferRegister.GetOperation().Name);
                Model.TransferStatus = TransferStatusIdentifier.sActSigned;
            }
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        public void SetTitle(string ukFormalName, string dogNum)
        {
            var title = $"Акт передачи для {ukFormalName}, договор № {dogNum}";
            Title = title;
        }

        public void SetSPNDebitDate(DateTime dt)
        {
            dtActDate.EditValue = dt;
            dtActDate.IsEnabled = false;
        }

        public static string GenerateActNumber(DateTime? date, string operationName)
        {
            if (date == null)
                return null;

            string prefix = date.Value.Year.ToString().Substring(2, 2);
            string operationType = null;
            if (TransferOperationIdentifier.IsAPSIOpertion(operationName))
                operationType = "У";
            else if (TransferOperationIdentifier.IsDSVOperation(operationName))
                operationType = "ДСВ";
            else if (TransferOperationIdentifier.IsMSKOperation(operationName))
                operationType = "МСК";
            else
                operationType = "ДУ";

            //год(2 символа)/номер квартала + кв/тип
            string quarter = $"{(date.Value.Month/4) + 1}кв";
            return $"{prefix}/{quarter}/{operationType}";
        }
    }
}
