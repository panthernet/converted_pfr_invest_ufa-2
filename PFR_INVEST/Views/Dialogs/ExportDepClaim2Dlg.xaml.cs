﻿namespace PFR_INVEST.Views.Dialogs
{
    using DevExpress.Xpf.Core;
    using System.Linq;
    using PFR_INVEST.BusinessLogic.ViewModelsList;
    using PFR_INVEST.Constants.Identifiers;

    /// <summary>
    /// Interaction logic for ExportDepClaimMaxListDlg.xaml
    /// </summary>
    public partial class ExportDepClaim2Dlg : DXWindow
    {
        public ExportDepClaim2Dlg()
        {
            InitializeComponent();           
        }

        private void OKButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
