﻿using System;
using System.Windows;
using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Tools;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for BanksVerificationDlg.xaml
    /// </summary>
    public partial class ExportDepClaimAllocationDlg : DXWindow
    {
        public ExportDepClaimAllocationDlg()
        {
            InitializeComponent();
        }

        private ExportDepClaimAllocationViewModel Model
        {
            get { return DataContext as ExportDepClaimAllocationViewModel; }

        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

    }
}
