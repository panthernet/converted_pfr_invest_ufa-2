﻿using System;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for F402PFRActions.xaml
    /// </summary>
    public partial class F402PFRActions
    {
        private F401402UKViewModel Model => DataContext as F401402UKViewModel;

        public F402PFRActions()
        {
            InitializeComponent();
            Loaded += F402PFRActions_Loaded;
        }

        private bool _mDateWasNull;

        private void F402PFRActions_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            if (Model.OutgoingDocDate != null) return;
            Model.OutgoingDocDate = DateTime.Today;
            _mDateWasNull = true;
        }

        private void CancelButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (_mDateWasNull)
            {
                Model.OutgoingDocDate = null;
                Model.IsDataChanged = false;
            }
            DialogResult = false;
        }

        private void OKButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
