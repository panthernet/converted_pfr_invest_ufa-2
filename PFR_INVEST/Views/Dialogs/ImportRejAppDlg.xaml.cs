﻿using System.Windows;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for RegionReportImportView.xaml
    /// </summary>
    public partial class ImportRejAppDlg
    {
        public ImportRejAppDlg()
        {
            InitializeComponent();            
            DataContextChanged += OnDataContextChanged;
            ModelInteractionHelper.SignUpForCloseRequest(this);
        }

        private void OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var model = DataContext as ImportRejAppDlgViewModel;
            if(model != null)
                model.PropertyChanged += ImportRejAppDlg_PropertyChanged;
        }

        private void ImportRejAppDlg_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if(e.PropertyName == "Items")
                mainGrid.RefreshData();
        }
    }
}
