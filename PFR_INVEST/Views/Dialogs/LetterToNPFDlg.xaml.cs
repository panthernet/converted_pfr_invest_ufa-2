﻿using System;
using System.Windows;
using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for LetterToNPFDlg.xaml
    /// </summary>
    public partial class LetterToNPFDlg
    {
        private const string WARNING = "Произошла ошибка при печати. Повторить?";
        public LetterToNPFViewModel Model => DataContext as LetterToNPFViewModel;

        public LetterToNPFDlg()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            var retry = true;
            while (retry)
            {
                try
                {
                    using (Loading.StartNew())
                    {
                        Model.Print();
                        retry = false;
                    }
                }
                catch (Exception ex)
                {
                    Loading.CloseWindow();
                    App.log.WriteException(ex);
                    if (DXMessageBox.Show(Properties.Resources.ApplicationName, WARNING, MessageBoxButton.YesNo) == MessageBoxResult.No)
                        retry = false;
                }
            }
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
