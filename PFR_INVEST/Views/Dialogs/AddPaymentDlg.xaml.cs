﻿using System.Windows;
using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for AddPaymentDlg.xaml
    /// </summary>
    public partial class AddPaymentDlg
    {
        public AddPaymentDlg()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            var vm = DataContext as PaymentViewModel;
            if (vm == null) return;
            if (vm.CanExecuteSave())
                DialogResult = true;
            else
                DXMessageBox.Show("Не заполнены все обязательные поля");
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
