﻿using System;
using System.Windows;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Editors;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for RenameSI.xaml
    /// </summary>
    public partial class ReportDepositsPromtDlg : DXWindow
    {
        public bool IsNameChanged { get; private set; }
		public bool IsReadOnly { get; private set; }


		public ReportDepositsPromtDlg()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
       
    }
}
