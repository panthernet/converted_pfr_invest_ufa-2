﻿using System.Windows;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for AddDopDueExcessDlg.xaml
    /// </summary>
    public partial class AddDopDueExcessDlg
    {
        public AddDopDueExcessDlg()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            var vm = DataContext as DueExcessAccurateViewModel;
            if (vm != null)
            {
                //vm.SaveCard.Execute(null);
                DialogResult = true;
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
