﻿using System;
using System.Windows;
using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic;
using DevExpress.Xpf.Editors;
using PFR_INVEST.Extensions;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for AddCourceManuallyOrFromCBRSiteDialog.xaml
    /// </summary>
    public partial class GetCourceManuallyOrFromCBRSiteDialog
    {
        private const string SAVE_COURCE_WARNING = "Операцию отменить нельзя. Вы уверены, что хотите сохранить курс?";
        private GetCourceManuallyOrFromCBRSiteDialogViewModel Model => DataContext as GetCourceManuallyOrFromCBRSiteDialogViewModel;

        private void CloseDialog(bool result)
        {
            DialogResult = result;
        }

        public GetCourceManuallyOrFromCBRSiteDialog()
        {
            InitializeComponent();
        }

        private void DXWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if (Model != null)
            {
                txtHint.Text =
                    $"Введите курс или нажмите кнопку в текстовом поле, чтобы загрузить его с сайта ЦБРФ. Валюта: {Model.Currency.Name.Trim()}, дата: {Model.CourceDate:dd.MM.yyy}.";
                Title = $"Добавление курса. {Model.Currency.Name.Trim()}, {Model.CourceDate:dd.MM.yyy}.";

                Model.OnCourceEntered += Model_OnCourceEntered;
                Model.OnCantGetCourseFromCBRSite += Model_OnCantGetCourseFromCBRSite;
                Model.OnStartedGettingCourseFromCBRSite += Model_OnStartedGettingCourseFromCBRSite;
                Model.OnFinishedGettingCourseFromCBRSite += Model_OnFinishedGettingCourseFromCBRSite;
            }
        }

        private void Model_OnCourceEntered(object sender, EventArgs e)
        {
            var result = DXMessageBox.Show(SAVE_COURCE_WARNING, Properties.Resources.ApplicationName, MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
                CloseDialog(true);
        }

        private static void Model_OnStartedGettingCourseFromCBRSite(object sender, EventArgs e)
        {
            Loading.ShowWindow();
        }

        private static void Model_OnFinishedGettingCourseFromCBRSite(object sender, EventArgs e)
        {
            Loading.CloseWindow();
        }

        private static void Model_OnCantGetCourseFromCBRSite(object sender, CantGetCourseFromCBRSiteEventArgs e)
        {
            DXMessageBox.Show(e.Message, PFR_INVEST.Properties.Resources.ApplicationName, MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            CloseDialog(false);
        }

		#region Numeric field edit fix
		private bool _isManualChange;

		private void OnGotFocus(object sender, EventArgs e)
        {
            _isManualChange = true;
        }

        private void OnLostFocus(object sender, EventArgs e)
        {
            _isManualChange = false;
        }

		private void OnEditValueChanged(object sender, EditValueChangedEventArgs e)
		{
			if (_isManualChange)
			{
				Dispatcher.RemoveTextLeadingZero(sender, e);
			}
		}
		#endregion
	}
}
