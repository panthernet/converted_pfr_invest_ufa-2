﻿using System.Windows;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for CBReportApprovementStatusDlg.xaml
    /// </summary>
    public partial class CBReportApprovementStatusDlg
    {
        private CBReportApprovementStatusDlgViewModel Model => (CBReportApprovementStatusDlgViewModel) DataContext;

        public CBReportApprovementStatusDlg()
        {
            InitializeComponent();
            DataContextChanged += (sender, args) =>
            {
                if (DataContext != null) Refresh_OnClick(null, null);
            };
        }

        private void Refresh_OnClick(object sender, RoutedEventArgs e)
        {
            Model.Refresh();
            labelDate1.Visibility = labelName1.Visibility = labelStatus1.Visibility = string.IsNullOrEmpty(Model.Name1) ? Visibility.Collapsed : Visibility.Visible;
            labelDate2.Visibility = labelName2.Visibility = labelStatus2.Visibility = string.IsNullOrEmpty(Model.Name2) ? Visibility.Collapsed : Visibility.Visible;
        }

        private void Close_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
