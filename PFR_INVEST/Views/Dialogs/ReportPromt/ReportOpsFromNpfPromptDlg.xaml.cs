﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Editors.Helpers;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Views.Dialogs.ReportPromt
{
    /// <summary>
    /// Interaction logic for RenameSI.xaml
    /// </summary>
    public partial class ReportOpsFromNpfPromptDlg: INotifyPropertyChanged
    {
        public string ReportName => "Операции по передаче/получению д/с от НПФ";

        public List<SimpleObjectValue> QuarterList { get; private set; }
        public List<Year> YearList { get; private set; }

        private long? _yearID;
        public long? YearID { get { return _yearID; } set { _yearID = value; } }

        private long? _quarterID;
        public long? QuarterID { get { return _quarterID; } set { _quarterID = value; } }

        private long? _operationID;
        private bool _isFormalizedName;
        public long? OperationID { get { return _operationID; } set { _operationID = value; } }
        public bool IsToNpfOperation => _operationID == (long) AsgFinTr.Directions.FromPFR;

        public bool LoadUnlinkedPP { get; set; } = true;

        public bool IsFormalizedName
        {
            get => _isFormalizedName;
            set { _isFormalizedName = value; RaisePropertyChanged(nameof(IsFormalizedName)); }
        }

        public List<string> NameTypeList { get; set; } = new List<string>
        {
            "Краткое наименование",
            "Формализованное наименование"
        };

        public ICommand OkCommand { get; set; }
        public ObservableCollection<LegalEntity> NpfList { get; set; } = new ObservableCollection<LegalEntity>();

        public ObservableCollection<SimpleObjectValue> OpsList { get; set; } = new ObservableCollection<SimpleObjectValue>
        {
            new SimpleObjectValue {ID = (long) AsgFinTr.Directions.FromPFR, Name = "Передача"},
            new SimpleObjectValue {ID = (long) AsgFinTr.Directions.ToPFR, Name = "Получение"}
        };
        public List<string> GarantList { get; set; } = new List<string> { "Все", "Входят" };    
        public bool IsGarant => cboxGarant.SelectedIndex == 1;

        //result
        public long[] ResultNpfIds => listNpf.SelectedItems.Cast<LegalEntity>().Select(a => a.ID).ToArray();


        public ReportOpsFromNpfPromptDlg()
        {
            InitializeComponent();
            DataContext = this;

            OperationID = OpsList.FirstOrDefault().ID;
            QuarterList = new List<SimpleObjectValue>
            {
                new SimpleObjectValue {ID =1, Name = "I"},
                new SimpleObjectValue {ID =2, Name = "II"},
                new SimpleObjectValue {ID =3, Name = "III"},
                new SimpleObjectValue {ID =4, Name = "IV"}
            };
            YearList = DataContainerFacade.GetList<Year>();
            YearID = DateTime.Now.Year - 2000;
            QuarterID = ((DateTime.Now.Month - 1) / 3) + 1;
            UpdateNpfList();

            butSelectAllnpf.Click += (sender, e) =>
            {
                if (listNpf.SelectedItems.Count == NpfList.Count)
                    listNpf.SelectedItems.Clear();
                else listNpf.SelectAll();
            };
            OkCommand = new DelegateCommand(a=> listNpf.SelectedItems.Any() && OperationID.HasValue && YearID.HasValue && QuarterID.HasValue, a=> DialogResult = true);

        }
     
        private void UpdateNpfList()
        {
            NpfList.Clear();
            WCFClient.Client.GetNPFListLEHib(null)
                .Where(a=> !IsGarant || a.GarantACBID == (long)LegalEntity.GarantACB.Included)
                .ForEach(a=> NpfList.Add(a));
            listNpf.SelectedItems.Clear();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

    }
}
