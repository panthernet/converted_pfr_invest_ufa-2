﻿using System.Windows;
using PFR_INVEST.BusinessLogic.ViewModelsDialog.Common;

namespace PFR_INVEST.Views.Dialogs.ReportPromt
{
    /// <summary>
    /// Interaction logic for SelectUKView.xaml
    /// </summary>
    public partial class SelectUKForReportView
    {
        public SelectUKForReportViewModel VM => DataContext as SelectUKForReportViewModel;
        public SelectUKForReportView()
        {
            InitializeComponent();
            Loaded += SelectUKForReportView_Loaded;
        }

        private void SelectUKForReportView_Loaded(object sender, RoutedEventArgs e)
        {
            VM.PropertyChanged += (s, ev) =>
            {
                if (ev.PropertyName == "UKList")
                    itemsGrid.RefreshData();
            };

        }
    }
}
