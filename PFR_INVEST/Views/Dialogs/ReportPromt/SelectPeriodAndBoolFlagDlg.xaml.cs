﻿using PFR_INVEST.BusinessLogic.ViewModelsDialog.Common;

namespace PFR_INVEST.Views.Dialogs.ReportPromt
{
    /// <summary>
    /// Interaction logic for SelectDateAndBoolFlagDlg.xaml
    /// </summary>
    public partial class SelectPeriodAndBoolFlagDlg
    {
        public SelectPeriodAndBoolFlagViewModel VM => DataContext as SelectPeriodAndBoolFlagViewModel;
        public SelectPeriodAndBoolFlagDlg()
        {
            InitializeComponent();
        }
    }
}
