﻿using System.Windows;
using System.Windows.Input;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for RenameSI.xaml
    /// </summary>
    public partial class SelectReqTransferForAssPayDlgView
    {
        public SelectReqTransferForAssPayDlgView()
        {
            InitializeComponent();
            tableView.Loaded += tableView_Loaded;
        }

        private static void tableView_Loaded(object sender, RoutedEventArgs e)
        {
            //if (tableView.SearchControl != null)
                //tableView.SearchControl.Focus();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {            
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

		private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			if(Grid.CurrentItem != null)
				DialogResult = true;
		}
    }
}
