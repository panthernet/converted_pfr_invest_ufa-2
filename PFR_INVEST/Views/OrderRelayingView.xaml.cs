﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    public partial class OrderRelayingView : UserControl
    {
        private OrderRelayingViewModel Model => DataContext as OrderRelayingViewModel;

        public OrderRelayingView()
        {
            InitializeComponent();
            Loaded += OrderRelayingView_Loaded;
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }
        private void OrderRelayingView_Loaded(object sender, RoutedEventArgs e)
        {
            if (Model != null && !Model.IsEventsRegisteredOnLoad)
            {
                Model.IsEventsRegisteredOnLoad = true;
                Model.OnDestinationPortfolioCountChanged += Model_OnDestinationPortfolioCountChanged;
                Model.CloseBindingWindows += (s, ee) =>
                            {
                                var lv = new List<ViewModelBase>();
                                lv.AddRange(App.DashboardManager.FindViewModelsList(typeof(SaleReportViewModel)));
                                lv.AddRange(App.DashboardManager.FindViewModelsList(typeof(BuyReportViewModel)));
                                lv.AddRange(App.DashboardManager.FindViewModelsList(typeof(RelayingReportViewModel)));
                                foreach (var viewModelBase in lv.OfType<OrderReportViewModelBase>())
                                {
                                    viewModelBase.CloseWindow();
                                }
                            };
            }
            SetDataSourceGrid();
        }


        protected void Model_OnDestinationPortfolioCountChanged(object Sender, EventArgs e)
        {
            SetDataSourceGrid();
        }


        private void SetDataSourceGrid()
        {
            if (!Model.PropertyExpandoItemsSource.Any())
            {
                return;
            }

            var properties = (IDictionary<string, object>)Model.PropertyExpandoItemsSource.First().Properties;

            var names = properties.Where(pr => pr.Key.Contains(Model.ColumnPrefix)).Select(pr => pr.Key).ToList();
            List<string> repeats = new List<string>();

            names.ForEach(
                n =>
                {
                    var name = n.Remove(n.IndexOf('*')).Replace(Model.ColumnPrefix, string.Empty);
                    if (!repeats.Contains(name))
                    {
                        repeats.Add(name);
                    }
                });


            var dgColumns = new ObservableList<GridColumn>();
            foreach (var property in properties)
            {

                var gridColumn = new GridColumn
                {
                    FieldName = $"Properties.{property.Key}",
                    Header = GetColumnName(property.Key, repeats),
                    EditTemplate = (ControlTemplate) gridControl.FindResource("EditTemplate"),
                    DisplayTemplate = (ControlTemplate) gridControl.FindResource("DisplayTemplate")
                };
                dgColumns.Add(gridColumn);
            }
            gridControl.ColumnsSource = dgColumns;
        }


        private string GetColumnName(string key, List<string> repeats)
        {
            if (key.Contains(Model.ColumnPrefix))
            {
                string name = key.Replace(Model.ColumnPrefix, string.Empty);
                string[] temp = name.Split('*');
                if (repeats.Contains(temp[0]) && temp.Count() > 1)
                {
                    if (temp[1].Length < 4)
                    {
                        return $"В {temp[0]}({temp[1]})".Replace("@",".");
                    }
                    return $"В {temp[0]}({temp[1].Remove(0, temp[1].Length - 4)})".Replace("@",".");
                }
                return $"В {temp[0]}".Replace("@",".");
            }

            switch (key)
            {
                case "SecurityName":
                    return "Выпуск ЦБ";

                case "CurrencyName":
                    return "Валюта";

                case "Count":
                    return "Всего";

                case "PriceSell":
                    return "Цена %";
                default:
                    return key;

            }
            
        }


        private void GridViewBase_OnShowingEditor(object sender, ShowingEditorEventArgs e)
        {
            e.Cancel = true;
            var pr = e.Row as PropertyExpandoObject;

            if (e.Column.FieldName.Contains(Model.ColumnPrefix))
                e.Cancel = pr.IsReadOnly;
        }

    }

}






