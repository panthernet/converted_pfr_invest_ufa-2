﻿using System.Windows;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
	/// <summary>
	/// Interaction logic for PaymentOrderListView.xaml
	/// </summary>
	public partial class PaymentOrderListView
	{
		public PaymentOrderListView()
		{
			InitializeComponent();
			Loaded += ViewLoaded;
            ModelInteractionHelper.SubscribeForGridRefreshData(this, Grid, "GridList");
            ModelInteractionHelper.SubscribeForPageLoadModel(this);
        }

	    private void ViewLoaded(object sender, RoutedEventArgs e)
		{
			Grid.CustomColumnDisplayText += Grid_CustomColumnDisplayText;
		}

	    private void Grid_CustomColumnDisplayText(object sender, DevExpress.Xpf.Grid.CustomColumnDisplayTextEventArgs e)
		{
			if (!Grid.IsValidRowHandle(e.RowHandle))
				return;
		}
       

		void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			if (Grid.View.GetRowElementByMouseEventArgs(e) == null) return;

			var item = Grid.CurrentItem as PPListItem;
			if (item != null)
				DashboardManager.Instance.OpenNewTab(typeof(CommonPPView), ViewModelState.Edit, item.ID, "Платежное поручение");
		}
	}
}
