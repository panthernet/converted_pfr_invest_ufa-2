﻿using System.Windows.Controls;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for BudgetCorrectionView.xaml
    /// </summary>
    public partial class BudgetCorrectionView : UserControl
    {
        public BudgetCorrectionView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }
    }
}
