﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using DevExpress.Data;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
	/// <summary>
	/// Interaction logic for FinRegistersListView.xaml
	/// </summary>
	public partial class NPFTempAllocationListView
	{
		public NPFTempAllocationListView()
		{
			InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, registersListGrid);
            ModelInteractionHelper.SubscribeForGridRefreshData(this, registersListGrid, "List");
            ModelInteractionHelper.SubscribeForPageLoadModel(this);
        }

        public NPFTempAllocationListViewModel Model => DataContext as NPFTempAllocationListViewModel;

		private void grid_CustomSummaryExists(object sender, CustomSummaryExistEventArgs e)
		{
			e.Exists = e.GroupLevel >= 1;
		}

		private void TableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			if (registersListGrid.View.GetRowElementByMouseEventArgs(e) == null) return;
			if (IsRegisterSelected())
			{
				var lID = GetSelectedTempAllocationID();
				if (lID > 0)
					App.DashboardManager.OpenNewTab(IsTempAllocationSelected() ? typeof(SPNAllocationView) : typeof(SPNReturnView), ViewModelState.Edit, lID, "Реестр");
			}
			else
			{
			    var item = registersListGrid.CurrentItem as RegistersListItem;
			    if(item?.ID > 0)
			        App.DashboardManager.OpenNewTab(typeof(AllocationNPFView), ViewModelState.Edit, item.ID, "Финреестр");
			}
		}

		public long GetSelectedTempAllocationID()
		{
            return Convert.ToInt64(registersListGrid.GetCellValue(registersListGrid.View.FocusedRowHandle, "RegisterID"));
		}

		public string GetSelectedTempAllocationType()
		{
			return registersListGrid.GetCellValue(registersListGrid.View.FocusedRowHandle, "RegisterKind") as string;
		}

		public bool IsRegisterSelected()
		{
			var lvl = registersListGrid.View.FocusedRowData.Level;
			if (lvl < registersListGrid.GetGroupedColumns().Count)
				return (registersListGrid.GetGroupedColumns()[lvl].FieldName == "RegisterID");
			return false;
		}

		public bool IsTempAllocationSelected()
		{
			return RegisterIdentifier.IsTempAllocation(GetSelectedTempAllocationType());
		}

		public RegistersListItem SelectedItem => registersListGrid.CurrentItem as RegistersListItem;


	    private void registersListGrid_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
		{
			const string dispText = "{0} \tдокумент: {1}     \r\n\t№ {2} от: {3}";
			if (!registersListGrid.IsValidRowHandle(e.RowHandle))
				return;

			if (e.Column.FieldName != "RegisterID")
				return;

			//string dispText = "";

			var currRow = (e.Row as RegistersListItem);

			if (currRow != null)
			{
				e.DisplayText = string.Format(dispText,
					currRow.RegisterID > 0 ? currRow.RegisterID.ToString() : string.Empty,
					!string.IsNullOrEmpty(currRow.ApproveDocName) ? currRow.ApproveDocName : string.Empty,
					!string.IsNullOrEmpty(currRow.RegNum?.Trim()) ? currRow.RegNum.ToString().Trim() : string.Empty,
					currRow.RegDate != null && currRow.RegDate != DateTime.MinValue ? currRow.RegDate.Value.ToShortDateString() : string.Empty
					);
			}
			else
				e.DisplayText = string.Empty;
		}

		#region RegLoad


		private void registersListGrid_Loaded(object sender, RoutedEventArgs e)
		{
		    if (Model != null)
			{
				Model.OnSelectGridRowDelegate += Model_OnSelectGridRowDelegate;
			}
		}

		protected virtual void Model_OnSelectGridRowDelegate(long rid, int type)
		{
			if (rid == 0 || registersListGrid.VisibleRowCount == 0)
				return;

			try
			{
				//int rowHandle = Grid.View.FocusedRowHandle + 1;
				var rowHandle = registersListGrid.FindRowByValue(type == 0 ? "ID" : "RegisterID", rid); // by finregister

				var handles = new List<int>(7);
				while (registersListGrid.IsValidRowHandle(rowHandle))
				{
					handles.Add(rowHandle);
					rowHandle = registersListGrid.GetParentRowHandle(rowHandle);
				}
				//4 уровень вложения по групповым вкладкам для реестра или 5 для финреестра
				if (handles.Any())
					registersListGrid.View.FocusedRowHandle = type == 0 ? handles[0] : (handles.Count > 4 ? handles[handles.Count - 4] : rowHandle);

			}
			catch (Exception ex)
			{
				App.log.WriteException(ex);
			}
		}

		#endregion
	}
}
