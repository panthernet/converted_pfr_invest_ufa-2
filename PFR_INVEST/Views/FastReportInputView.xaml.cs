﻿using System.Windows;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataObjects.Helpers;
using PFR_INVEST.Views.Dialogs;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
	public partial class FastReportInputView
	{
		private const string NOT_ALL_ITEMS_HAVE_COURSES_MSG = "Не для всех добавленных отчетов удалось найти соответствующий курс валют. Пожалуйста укажите курс вручную перед сохранением. Даты, для которых не указаны курсы: {0}.";

		private bool _loaded;

	    private FastReportInputViewModel Model => DataContext as FastReportInputViewModel;

	    public FastReportInputView()
		{
			InitializeComponent();
			Loaded += ViewLoaded;
			Unloaded += FastReportInputView_Unloaded;
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        private void FastReportInputView_Unloaded(object sender, RoutedEventArgs e)
		{
			Model.OnNotAllItemsHaveCourses -= Model_OnNotAllItemsHaveCourses;
			Model.OnSellAbort -= Model_OnSellAbort;
		}

	    private void ViewLoaded(object sender, RoutedEventArgs e)
		{
		    if (Model == null || _loaded) return;
		    ReportsGrid.Columns["Contragent"].Header = Model.IsBuyOrder ? "Продавец" : "Покупатель";
		    Model.OnNotAllItemsHaveCourses += Model_OnNotAllItemsHaveCourses;
		    Model.OnSellAbort += Model_OnSellAbort;
		    _loaded = true;
		}

		private static void Model_OnSellAbort(object sender, StringEventArgs e)
		{
            ViewModelBase.DialogHelper.ShowError(e.Text);			
		}

		private static void Model_OnNotAllItemsHaveCourses(object sender, NotAllItemsHaveCoursesEventArgs e)
		{
			var message = string.Format(NOT_ALL_ITEMS_HAVE_COURSES_MSG, e.GetNotPresentCoursesDatesString());
            ViewModelBase.DialogHelper.ShowError(message);
		}

		private void AddReportButton_Click(object sender, RoutedEventArgs e)
		{
			Model.AddNewRow();
		}

		private void ReportsTableView_CellValueChanging(object sender, CellValueChangedEventArgs e)
		{
			(sender as TableView)?.PostEditor();
		}

		private void EditReportButton_Click(object sender, RoutedEventArgs e)
		{
			if (Model.IsBuyOrder)
				ToBuyReport(ReportsGrid.GetFocusedRow() as FastReportItem);
			else
				ToSellReport(ReportsGrid.GetFocusedRow() as FastReportItem);

			ReportsGrid.RefreshData();
		}

		protected void ToSellReport(FastReportItem item)
		{
			var dlg = new EditOrderReport();
			var dlgVM = new SaleReportViewModel(Model.Order.ID);
			dlg.DataContext = dlgVM;

			dlgVM.DateOrder = item.Date;
			dlgVM.DateDEPO = item.DEPODate;
			dlgVM.Count = item.Count;
			dlgVM.Price = item.Price;
			dlgVM.NKDPerOneObl = item.OneNKD ?? 0;
			dlgVM.SelectedContragent = item.Contragent;
			dlgVM.SelectedSecurity = dlgVM.SecuritiesList.Find(
				sec => sec.ID == item.Issue.SecurityID
			);

			Tools.ThemesTool.SetCurrentTheme(dlg);
			dlg.Owner = App.mainWindow;
			dlg.Title = OrderReportsHelper.SELLING_REPORT;

			if (dlg.ShowDialog().Value)
			{
				item.Date = dlgVM.DateOrder.Value;
				item.DEPODate = dlgVM.DateDEPO.Value;
				item.Count = dlgVM.Count;
				item.Price = dlgVM.Price;
				item.OneNKD = dlgVM.NKDPerOneObl;
				item.Contragent = dlgVM.SelectedContragent;
				item.Issue = FastReportInputViewModel.Issues.Find(
					issue => issue.SecurityID == dlgVM.SelectedSecurity.ID
				);
			}
		}

		protected void ToBuyReport(FastReportItem item)
		{
			var dlg = new EditOrderReport();
			var dlgVM = new BuyReportViewModel(Model.Order.ID);
			dlg.DataContext = dlgVM;

			dlgVM.DateOrder = item.Date;
			dlgVM.DateDEPO = item.DEPODate;
			dlgVM.Count = item.Count;
			dlgVM.Price = item.Price;
			dlgVM.NKDPerOneObl = item.OneNKD ?? 0;
			dlgVM.Yield = item.Yield;
			dlgVM.SelectedContragent = item.Contragent;
			dlgVM.SelectedSecurity = dlgVM.SecuritiesList.Find(
				sec => sec.ID == item.Issue.SecurityID
			);

			Tools.ThemesTool.SetCurrentTheme(dlg);
			dlg.Owner = App.mainWindow;
			dlg.Title = OrderReportsHelper.BUYING_REPORT;

			if (dlg.ShowDialog().Value)
			{
				item.Date = dlgVM.DateOrder.Value;
				item.DEPODate = dlgVM.DateDEPO.Value;
				item.Count = dlgVM.Count;
				item.Price = dlgVM.Price;
				item.OneNKD = dlgVM.NKDPerOneObl;
				item.Yield = dlgVM.Yield;
				item.Contragent = dlgVM.SelectedContragent;
				item.Issue = FastReportInputViewModel.Issues.Find(
					issue => issue.SecurityID == dlgVM.SelectedSecurity.ID
				);
			}
		}


		private void ReposrtsTableView_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
		{
			if (e.Key == System.Windows.Input.Key.OemMinus || e.Key == System.Windows.Input.Key.Subtract)
				e.Handled = true;
		}
	}
}
