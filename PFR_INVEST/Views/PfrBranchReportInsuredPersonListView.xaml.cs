﻿using DevExpress.Xpf.Grid;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for PfrBranchReportInsuredPersonListView.xaml
    /// </summary>
    public partial class PfrBranchReportInsuredPersonListView
    {
        public PfrBranchReportInsuredPersonListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, ReportGrid);
        }

        private void ReportGrid_CustomColumnSort(object sender, CustomColumnSortEventArgs e)
        {
            e.Result = ComponentHelper.OnColumnCustomSort(sender, e);
            e.Handled = true;
        }
        
        private void ReportGrid_OnCustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (e.Value == null)
                e.DisplayText = "Нет данных";
            else if (e.Value is long?)
            {
                var longValue = (long) e.Value;
                e.DisplayText = longValue.ToString("N0");
            }
        }

        private void OnShowFilterPopup(object sender, FilterPopupEventArgs e)
        {
            ComponentHelper.OnColumnFilterOptionsSort(sender, e);
        }
    }
}
