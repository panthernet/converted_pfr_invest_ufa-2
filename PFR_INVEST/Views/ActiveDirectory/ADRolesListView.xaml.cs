﻿using System.Windows.Input;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.Commands;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for RolesView.xaml
    /// </summary>
    public partial class ADRolesListView
    {
        public ADRolesListView()
        {
            InitializeComponent();
        }

        private DelegateCommand _showRoleCommand;
        /// <summary>
        /// Команда на отображение выбранной роли
        /// </summary>
        public ICommand ShowRoleCommand
        {
            get { return _showRoleCommand ?? (_showRoleCommand = new DelegateCommand(null, o => ShowRole())); }
        }

        /// <summary>
        /// Отобразить свойства выбранной роли
        /// </summary>
        private void ShowRole()
        {
            var selectedItem = listGrid.SelectedItems == null || listGrid.SelectedItems.Count < 1 ? null : listGrid.SelectedItems[0];
            if (!(selectedItem is ADRoleViewModel)) return;
            //  Перед отображением роли загрузим список входящих в неё пользователей
            //  На данный момент списки пользователей формируются в логике представления роли, эта загрузка не несет смысла
            var roleForm = new ADRoleView();
            var rm = selectedItem as ADRoleViewModel;
            roleForm.DataContext = rm;
            App.DashboardManager.OpenNewTab("Роль: " + rm.ShowName, roleForm, ViewModelState.Create, rm);
        }

        private void listView1_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (listView1.GetRowHandleByMouseEventArgs(e) == DataControlBase.InvalidRowHandle) return;
            ShowRole();
        }

    }
}
