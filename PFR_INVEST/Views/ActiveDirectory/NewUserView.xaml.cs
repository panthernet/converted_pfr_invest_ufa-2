﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using DevExpress.Xpf.Core;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.DataObjects.Journal;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for NewUserView.xaml
    /// </summary>
    public partial class NewUserView
    {
        public ObservableCollection<User> Users { get; set; }

        public NewUserView()
        {
            InitializeComponent();
        }

        public void Refresh(List<User> users)
        {
            Users = new ObservableCollection<User>();
            foreach (var user in users)
                Users.Add(user);
            listGrid.DataContext = Users;
        }

        public void AddUsers()
        {
            foreach (var user in listGrid.SelectedItems.Cast<User>()) 
            {
                string message;
                if (AP.Provider.AddAuthUser(user, out message))
                    JournalLogger.LogEvent("Добавление пользователя", JournalEventType.USERS_ADDED, null, GetType().Name, $"Добавлен пользователь: {user.Login}");
                else 
                {
                    JournalLogger.LogEvent("Добавление пользователя", JournalEventType.USERS_ADDED, null, GetType().Name,
                        $"Ошибка добавление пользователя \"{user.Login}\"  : {message}");
                    DXMessageBox.Show($@"При добавлении пользователя ""{user.Login}"" произошла ошибка : {message}", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        public void DeleteUsers()
        {
            foreach (var user in listGrid.SelectedItems.Cast<User>()) 
            {
                string message;
                if (AP.Provider.RemoveAuthUser(user, out message))
                    JournalLogger.LogEvent("Удаление пользователя", JournalEventType.USERS_DELETED, null, GetType().Name, $"Удален пользователь: {user.Login}");
                else
                {
                    JournalLogger.LogEvent("Удаление пользователя", JournalEventType.USERS_DELETED, null, GetType().Name,
                        $"Ошибка удаления пользователя \"{user.Login}\"  : {message}");
                    DXMessageBox.Show($@"При удалении пользователя ""{user.Login}"" произошла ошибка : {message}", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);                
                }
            }
        }
    }
}