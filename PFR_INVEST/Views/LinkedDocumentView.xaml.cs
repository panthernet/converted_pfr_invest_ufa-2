﻿using System;
using System.Windows;
using System.Windows.Controls;
using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for LinkedDocumentView.xaml
    /// </summary>
    public partial class LinkedDocumentView : UserControl
    {
        private const string ATTACHMENT_OPEN_FAILED = "Вложение открыть не удалось";

        private LinkedDocumentBaseViewModel Model => DataContext as LinkedDocumentBaseViewModel;

        public LinkedDocumentView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (Model != null)
                Model.OnAttachmentOpenFailed += Model_OnAttachmentOpenFailed;
        }

        protected void Model_OnAttachmentOpenFailed(object sender, EventArgs e)
        {
            DXMessageBox.Show(ATTACHMENT_OPEN_FAILED, Properties.Resources.ApplicationName, MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}
