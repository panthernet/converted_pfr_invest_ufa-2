﻿using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for RatingsList.xaml
    /// </summary>
    public partial class RatingsListView
    {
        public RatingsListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, ratingsListGrid);
        }

        public RatingsListViewModel Model => DataContext as RatingsListViewModel;

        private void TableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ratingsListGrid.View.GetRowElementByMouseEventArgs(e) == null) return;
            if (ratingsListGrid.View.FocusedRowHandle >= 0)
                Model.EditSelectedRating((long)ratingsListGrid.GetCellValue(ratingsListGrid.View.FocusedRowHandle, "ID"));
        }
    }
}
