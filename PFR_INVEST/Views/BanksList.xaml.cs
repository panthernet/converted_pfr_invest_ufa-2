﻿using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for ContragentsList.xaml
    /// </summary>
    public partial class BanksList
    {
        public BanksList()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, npfListGrid);
        }

        private void tableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (npfListGrid.View.GetRowElementByMouseEventArgs(e) == null)
                return;
            if (npfListGrid.View.FocusedRowHandle < 0)
                return;
            object value = npfListGrid.GetCellValue(npfListGrid.View.FocusedRowHandle, "ID");

            App.DashboardManager.OpenNewTab(typeof(BankView),
                ViewModelState.Edit,
                (long)
                    value, "Банк");
        }
    }
}
