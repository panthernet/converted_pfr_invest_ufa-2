﻿using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    public partial class SIContractsListView : IContractsListView
    {
        public SIContractsListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, ContractsGrid.ItemsGrid);
        }

        public ContractsGridView GridControl => ContractsGrid;
    }
}
