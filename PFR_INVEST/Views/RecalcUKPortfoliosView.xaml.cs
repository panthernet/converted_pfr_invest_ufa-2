﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for RecalcUKPortfoliosView.xaml
    /// </summary>
    public partial class RecalcUKPortfoliosView : UserControl
    {
        //cached for threads
        private RecalcUKPortfoliosViewModel m_model;
        public RecalcUKPortfoliosViewModel Model => m_model ?? (m_model = DataContext as RecalcUKPortfoliosViewModel);

        public RecalcUKPortfoliosView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        protected void btnStartRecalc_Click(object sender, EventArgs e)
        {
            Model.LoadContracts();
            btnStartRecalc.IsEnabled = false;
            btnStopRecalc.IsEnabled = true;

            progress.Maximum = Model.ContractsCount + 1;
            progress.Minimum = 0;
            progress.Value = 0;
            ProcessNextContrtact();
            //Model.Recalc();
        }

        private void ProcessNextContrtact()
        {
            ThreadPool.QueueUserWorkItem(ProcessNextContrtactAsync);
        }

        private void ProcessNextContrtactAsync(object state)
        {
            try
            {
                Model.RecalcNextContract();
            }
            catch
            {

            }

            UpdateProgressStatus();

            if (Model.Canceled)
            {
                Dispatcher.Invoke(new Action(RecalcualtionCancelled));
                return;
            }

            if (Model.IsRecalcuationFinished())
            {

                Dispatcher.Invoke(new Action(RecalcualtionFinished));
            }
            else
            {

                ProcessNextContrtact();
            }
        }

        private void UpdateProgressStatus()
        {
            Dispatcher.Invoke(new Action(delegate()
            {
                progress.Value = Model.CurrentContractIndex;
            }));

        }

        private void RecalcualtionFinished()
        {
            progress.Value = progress.Maximum;
            DXMessageBox.Show($"Пересчет завершен. Всего {Model.ContractsCount} Ошибок {Model.ErrorsCount}");
            btnStartRecalc.IsEnabled = true;
            btnStopRecalc.IsEnabled = false;
            progress.Value = progress.Minimum;
        }

        private void RecalcualtionCancelled()
        {
            DXMessageBox.Show("Пересчет отменен");
            btnStartRecalc.IsEnabled = true;
            btnStopRecalc.IsEnabled = false;
        }

        protected void btnStopRecalc_Click(object sender, EventArgs e)
        {
            Model.Canceled = true;
        }

        private void btnStopRecalc_Click(object sender, RoutedEventArgs e)
        {
            Model.Canceled = true;
        }
    }
}
