﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for AssignPaymentsArchiveListView.xaml
    /// </summary>
    public partial class AssignPaymentsArchiveListView
    {
        private bool _mGridRowMouseDoubleClicked;
        public long InitialSelectedRID;

        private AssignPaymentsArchiveListViewModel Model => DataContext as AssignPaymentsArchiveListViewModel;

        public AssignPaymentsArchiveListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, Grid);
        }

        private void Grid_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            const string dispText = "{0} \tдокумент: {1}     \r\n\t№ {2} от: {3}";

            if (!Grid.IsValidRowHandle(e.RowHandle))
                return;

            if (e.Column.FieldName == "Operation" && string.IsNullOrEmpty(e.DisplayText))
                e.DisplayText = "(Содержание операции отсутствует)";

            if (e.Column.FieldName == "OperationType" && string.IsNullOrEmpty(e.DisplayText))
                e.DisplayText = "(Тип операции отсутствует)";

            if (e.Column.FieldName != "Company")
                return;

            var currRow = (e.Row as TransferListItem);

            if (currRow != null)
            {
                e.DisplayText = string.Format(dispText,
                    currRow.Company,
                    currRow.Kind,
                    currRow.RegisterNumber,
                    currRow.RegisterDate?.ToShortDateString() ?? string.Empty
                );
            }
            else
                e.DisplayText = string.Empty;
        }

        private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (Grid.View.GetRowElementByMouseEventArgs(e) == null)
                return;

            if (IsRegisterSelected())
            {
                var id = GetSelectedRegID();
                _mGridRowMouseDoubleClicked = true;
                if (id > 0)
                {
                    App.DashboardManager.OpenNewTab(typeof(YearPlanView), ViewModelState.Edit, id, "Годовой план");
                }
            }
            else if (IsTransferSelected())
            {
                var id = GetSelectedTrID();
                _mGridRowMouseDoubleClicked = true;
                if (id > 0)
                {
                    App.DashboardManager.OpenNewTab(typeof(UKPaymentPlanView), ViewModelState.Edit, id, "План выплат по УК");
                }
            }
            else if (IsReqTransferSelected())
            {
                _mGridRowMouseDoubleClicked = true;
                var id = GetSelectedUkPlanID();
                if (id > 0)
                {
                    ViewModelBase.DialogHelper.SelectReqTransferFromAssPay(id);
                }
            }
        }

        private long GetSelectedUkPlanID()
        {
            return Convert.ToInt64(Grid.GetCellValue(Grid.View.FocusedRowHandle, "UkPlanID"));
        }

        public bool IsRegisterSelected()
        {
            int lvl = Grid.View.FocusedRowData.Level;
            if (lvl < Grid.GetGroupedColumns().Count)
                return (Grid.GetGroupedColumns()[lvl].FieldName == "Company");
            return false;
        }

        public bool IsTransferSelected()
        {
            int lvl = Grid.View.FocusedRowData.Level;
            if (lvl < Grid.GetGroupedColumns().Count)
                return (Grid.GetGroupedColumns()[lvl].FieldName == "ContractNumber");
            return false;
        }

        public bool IsReqTransferSelected()
        {
            return Grid.View.FocusedRowData.Level >= Grid.GetGroupedColumns().Count && Grid.SelectedItem != null;
        }

        public long GetSelectedTrID()
        {
            return Convert.ToInt64(Grid.GetCellValue(Grid.View.FocusedRowHandle, "TransferID"));
        }

        public long GetSelectedReqID()
        {
            return Convert.ToInt64(Grid.GetCellValue(Grid.View.FocusedRowHandle, "ReqTransferID"));
        }

        public long GetSelectedRegID()
        {
            return Convert.ToInt64(Grid.GetCellValue(Grid.View.FocusedRowHandle, "RegisterID"));
        }

        public string GetSelectedStatus()
        {
            return Convert.ToString(Grid.GetCellValue(Grid.View.FocusedRowHandle, "Status"));
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            if(Model != null)
                Model.OnSelectGridRowDelegate += Model_OnSelectGridRowDelegate;
            Model_OnSelectGridRowDelegate(InitialSelectedRID);
        }

        protected virtual void Model_OnSelectGridRowDelegate(long rid)
        {
            if (rid == 0 || Grid.VisibleRowCount == 0)
                return;

            try
            {
                //int rowHandle = Grid.View.FocusedRowHandle + 1;
                var rowHandle = Grid.FindRowByValue("RegisterID", rid);

                var handles = new List<int>(7);
                while (Grid.IsValidRowHandle(rowHandle))
                {
                    handles.Add(rowHandle);
                    rowHandle = Grid.GetParentRowHandle(rowHandle);
                }
                //2 уровень вложения по групповым вкладкам
                Grid.View.FocusedRowHandle = handles.Count > 2 ? handles[handles.Count - 2] : rowHandle;
            }
            catch (Exception ex)
            {
                App.log.WriteException(ex);
            }
        }

        private void Grid_CustomColumnSort(object sender, CustomColumnSortEventArgs e)
        {
            e.Result = ComponentHelper.OnColumnCustomSort(sender, e);
            e.Handled = true;
        }

        private void ProcessCollapsingOrExpanding(object sender, RowAllowEventArgs e)
        {
            e.Allow = !_mGridRowMouseDoubleClicked;
            _mGridRowMouseDoubleClicked = false;
        }
		
		private void OnShowFilterPopup(object sender, FilterPopupEventArgs e)
		{
			ComponentHelper.OnColumnFilterOptionsSort(sender, e);
		}
	}
}
