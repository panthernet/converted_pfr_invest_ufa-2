﻿namespace PFR_INVEST.Views
{
    using BusinessLogic;
    public class InsuranceSIListView : InsuranceListView
    {
        protected override void OpenItem(long id)
        {
            App.DashboardManager.OpenNewTab(typeof(InsuranceSIView),ViewModelState.Edit, id,  "Договор страхования СИ");
        }
    }
}
