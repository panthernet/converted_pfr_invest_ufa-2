﻿using System;
using System.Windows.Input;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for ContragentsList.xaml
    /// </summary>
    public partial class RatesListView
    {
        public RatesListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, Grid);
        }

        public long GetSelectedRateID()
        {
            return Convert.ToInt64(Grid.GetCellValue(Grid.View.FocusedRowHandle, "ID"));
        }

        public bool IsRateSelected()
        {
            return Grid.View.FocusedRowData.Level >= Grid.GetGroupedColumns().Count && Grid.SelectedItem != null;
        }

        private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (!IsRateSelected())
                return;
            long lID = GetSelectedRateID();
            if (lID > 0)
                App.DashboardManager.OpenNewTab(typeof(RateView), ViewModelState.Edit, lID, "Курс");
        }

        private void Grid_CustomColumnSort(object sender, CustomColumnSortEventArgs e)
        {
            e.Result = ComponentHelper.OnColumnCustomSort(sender, e);
            e.Handled = true;
        }

		private void OnShowFilterPopup(object sender, FilterPopupEventArgs e)
		{
			ComponentHelper.OnColumnFilterOptionsSort(sender, e);
		}
	}
}
