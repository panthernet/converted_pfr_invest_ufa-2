﻿using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for ZLRedistListView.xaml
    /// </summary>
    public partial class ZLRedistListView
    {
        public ZLRedistListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, zlRedistListGrid);
        }

        public long GetZLRedistID()
        {
            return (long)(zlRedistListGrid.GetFocusedRowCellValue("ID") ?? 0L);
        }

        private void tableView_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var vm = DataContext as ZLRedistListViewModel;
            if (vm == null)
                return;
            var val = zlRedistListGrid.GetFocusedRowCellValue("ID");
            if (val != null)
            {
                vm.ID = (long)val;
            }
        }

        public bool IsZLRedistSelected()
        {
            int lvl = zlRedistListGrid.View.FocusedRowData.Level;
            return lvl == zlRedistListGrid.GetGroupedColumns().Count;
        }

        private void tableView_KeyDown(object sender, KeyEventArgs e)
        {
            tableView_MouseDown(sender, null);
        }

        private void tableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (zlRedistListGrid.View.GetRowElementByMouseEventArgs(e) != null)
                App.DashboardManager.OpenNewTab(typeof(ZLRedistView), ViewModelState.Edit,
                        (long)zlRedistListGrid.GetCellValue(zlRedistListGrid.View.FocusedRowHandle, "ID"), "Перераспределение ЗЛ");
        }
    }
}
