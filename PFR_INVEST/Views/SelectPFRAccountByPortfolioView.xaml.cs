﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects.ListItems;
using System.Collections.Generic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for SelectPFRAccountByPortfolioView.xaml
    /// </summary>
    public partial class SelectPFRAccountByPortfolioView : UserControl
    {
        private Dictionary<long, string> _currencies = null;
        public delegate void GridRowDoubleClickedDelegate();
        public event GridRowDoubleClickedDelegate OnGridRowDoubleClickedDelegate;
        public void GridRowDoubleClicked()
        {
            OnGridRowDoubleClickedDelegate?.Invoke();
        }

        public SelectPFRAccountByPortfolioView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        public PortfoliosListViewModel Model => DataContext as PortfoliosListViewModel;

        private void tableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (PortfoliosGrid.View.GetRowElementByMouseEventArgs(e) != null)
            {
                if (PortfoliosGrid.View.FocusedRowHandle >= 0)
                {

                    if (Model != null)
                    {
                        Model.SelectedItem = PortfoliosGrid.GetFocusedRow() as PortfolioFullListItem;
                        GridRowDoubleClicked();
                    }
                }
            }
        }

        private void tableView_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var vm = Model;
            if (vm != null)
            {
                vm.FocusedRowHandle = PortfoliosGrid.View.FocusedRowHandle;
                //vm.SelectedItem = this.PortfoliosGrid.GetFocusedRow() as PortfolioFullListItem;
            }
        }

        //2) и сделать список попроще - раз там итак все текущие, то убрать колонку тип счета и убрать группировку по банку - Нескин
        //грязный хак для определения надо ли отображать группировку и колонку (для ввода п/п по НПФ в перечислениях БО)
        private bool CheckModelRefreshParam(PortfolioIdentifier.PortfolioPBAParams p)
        {
            return p.CurrencyName != null && p.CurrencyName.Length > 0 &&
                   p.AccountType != null && p.AccountType.Length > 0 &&
                   p.LegalEntityFormalizedNameContains != null && p.LegalEntityFormalizedNameContains.Length > 0;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            var vm = Model;
            if (vm != null)
            {
                if (Model.RefreshParameter is PortfolioIdentifier.PortfolioPBAParams)
                {
                    PortfolioIdentifier.PortfolioPBAParams param = (PortfolioIdentifier.PortfolioPBAParams)Model.RefreshParameter;
                    if (CheckModelRefreshParam(param))
                    {
                        PortfoliosGrid.ExpandAllGroups();
                        PortfoliosGrid.Width = 600;
                        PortfoliosGrid.Columns["AccountType.Name"].GroupIndex = -1;
                        PortfoliosGrid.Columns["AccountType.Name"].Visible = false;
                        PortfoliosGrid.Columns["BankLE.FormalizedNameFull"].GroupIndex = -1;
                    }
                }

                //vm.SelectedItem = this.PortfoliosGrid.GetFocusedRow() as PortfolioFullListItem;
            }
        }

        
        private void PortfoliosGrid_CustomColumnDisplayText(object sender, DevExpress.Xpf.Grid.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.Name == "c7"){
                if (_currencies == null){
                    _currencies = new Dictionary<long,string>();
                    foreach(Currency c in DataContainerFacade.GetList<Currency>())
                        _currencies.Add(c.ID, c.Name);
                }
                e.DisplayText = _currencies[(long)e.Value];
            }
        }

    }
}
