﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using DevExpress.Data;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataObjects.Helpers;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
	/// <summary>
	/// Interaction logic for BalanceView.xaml
	/// </summary>
	public partial class BalanceView
	{
		public BalanceView()
		{
			InitializeComponent();
            tableView.ShowFilterPopup += ComponentHelper.OnColumnFilterOptionsSort;
            ModelInteractionHelper.SubscribeForGridRefreshData(this, Grid, "GridList");
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
            ModelInteractionHelper.SubscribeForPageLoadModel(this);
		}

	    private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
	    {
	        tableView.ShowPrintPreviewDialog(MainWindow.Instance);
	    }

		public static void OpenForm<T>(BalanceListItem item)
		{
			var operation = item.KIND.Split('/')[0].Trim();
			App.DashboardManager.OpenNewTab(typeof(T),
											ViewModelState.Read, item.ID, operation);
		}

		public static void OpenForm<T>(BalanceListItem item, ViewModelState action)
		{
			var operation = item.KIND.Split('/')[0].Trim();

			App.DashboardManager.OpenNewTab(typeof(T),
											action, item.ID, operation);
		}

		public static void OpenForm<T>(BalanceListItem item, ViewModelState action, object param)
		{
			var operation = item.KIND.Split('/')[0].Trim();

			App.DashboardManager.OpenNewTab(typeof(T),
											action, item.ID, param, operation);
		}

		bool IsDocSelected()
		{
            return Grid.View.FocusedRowData.Level >= Grid.GetGroupedColumns().Count && Grid.SelectedItem != null;
		}

		public BalanceListItem GetSelectedItem()
		{
			if (Grid.View.FocusedRowHandle >= 0 && Grid.View.FocusedRowHandle > -1000)
			{
				return Grid.CurrentItem as BalanceListItem;
			}

			return null;
		}

		public static void OpenFormForItem(BalanceListItem item) 
		{
			var part = (SaldoParts)item.Ord;
			var isHandled = true;

			// Новая система привязки по типу записи, а не по тексту
			switch (part)
			{
				case SaldoParts.Req_Transfer:
				case SaldoParts.Asg_Fin_Tr:
					OpenForm<CommonPPView>(item, ViewModelState.Read);
					break;
				case SaldoParts.Cost:
					OpenForm<CostsView>(item, ViewModelState.Read);
					break;
				default:
					isHandled = false;
					break;
			}


			// старая система привязки формы по тексту содержания
			if (!isHandled)
			{
				var operation = item.KIND.Split('/')[0].Trim();
				switch (operation)
				{
					#region accurate quarter
					case DueDocKindIdentifier.AccurateQuarter:
					case DueDocKindIdentifier.DueDSVAccurateYear:
						App.DashboardManager.OpenNewTab(typeof(QuarterAccurateView), ViewModelState.Read,
															item.ID, (object) operation, operation);
						break;

					case DueDocKindIdentifier.PenaltyAccurateQuarter:
						App.DashboardManager.OpenNewTab(typeof(QuarterAccurateView), ViewModelState.Read,
															item.ID, operation);
						break;

					#endregion

					#region due
					case DueDocKindIdentifier.Due:
						App.DashboardManager.OpenNewTab(typeof(DueView), ViewModelState.Read,
															item.ID, PortfolioIdentifier.PortfolioTypes.SPN, operation);
						break;
					case DueDocKindIdentifier.DueDSV:
						App.DashboardManager.OpenNewTab(typeof(DueView), ViewModelState.Read,
															item.ID, PortfolioIdentifier.PortfolioTypes.DSV, operation);
						break;
					case DueDocKindIdentifier.Penalty:
						OpenForm<PenaltyView>(item, ViewModelState.Read);
						break;

					case DueDocKindIdentifier.Prepayment:
						//OpenForm<PrepaymentView>(item, ViewModelState.Edit);
						App.DashboardManager.OpenNewTab(typeof(PrepaymentView), ViewModelState.Read,
															item.ID, PortfolioIdentifier.PortfolioTypes.SPN, operation);
						break;

					case DueDocKindIdentifier.PrepaymentDSV:
						App.DashboardManager.OpenNewTab(typeof(PrepaymentView), ViewModelState.Read,
															item.ID, PortfolioIdentifier.PortfolioTypes.DSV, operation);
						break;

					case DueDocKindIdentifier.DueDead:
						App.DashboardManager.OpenNewTab(typeof(DueDeadView), ViewModelState.Read,
															item.ID, PortfolioIdentifier.PortfolioTypes.SPN, operation);
						//                    OpenForm<DueDeadView>(item);
						break;

					case DueDocKindIdentifier.DueDeadDSV:
						App.DashboardManager.OpenNewTab(typeof(DueDeadView), ViewModelState.Read,
															item.ID, PortfolioIdentifier.PortfolioTypes.DSV, operation);
						break;

					case DueDocKindIdentifier.DueExcess:
						App.DashboardManager.OpenNewTab(typeof(DueExcessView), ViewModelState.Read,
														  item.ID, PortfolioIdentifier.PortfolioTypes.SPN, operation);
						//                  OpenForm<DueExcessView>(item);
						break;

					case DueDocKindIdentifier.DueExcessDSV:
						App.DashboardManager.OpenNewTab(typeof(DueExcessView), ViewModelState.Read,
														  item.ID, PortfolioIdentifier.PortfolioTypes.DSV, operation);
						//                  OpenForm<DueExcessView>(item);
						break;

					case DueDocKindIdentifier.DueUndistributed:
						App.DashboardManager.OpenNewTab(typeof(DueUndistributedView), ViewModelState.Read,
															item.ID, PortfolioIdentifier.PortfolioTypes.SPN, operation);
						//OpenForm<DueUndistributedView>(item);
						break;

					case DueDocKindIdentifier.DueUndistributedDSV:
						App.DashboardManager.OpenNewTab(typeof(DueUndistributedView), ViewModelState.Read,
															item.ID, PortfolioIdentifier.PortfolioTypes.DSV, operation);
						//OpenForm<DueUndistributedView>(item);
						break;

					case DueDocKindIdentifier.PrepaymentAccurate:
					case DueDocKindIdentifier.PrepaymentAccurateDSV:
						OpenForm<PrepaymentAccurate>(item, ViewModelState.Read);
						break;

					case DueDocKindIdentifier.PenaltyAccurate:
						OpenForm<PrecisePenaltyView>(item, ViewModelState.Read);
						break;

					case DueDocKindIdentifier.DueAccurate:
					case DueDocKindIdentifier.DueDSVAccurate:
						OpenForm<MonthAccurateView>(item, ViewModelState.Read, operation);
						break;

					case DueDocKindIdentifier.DueDeadAccurate:
					case DueDocKindIdentifier.DueDeadAccurateDSV:
						OpenForm<DueDeadAccurateView>(item, ViewModelState.Read);
						break;

					case DueDocKindIdentifier.DueExcessAccurate:
					case DueDocKindIdentifier.DueExcessAccurateDSV:
						OpenForm<DueExcessAccurateView>(item, ViewModelState.Read);
						break;

					case DueDocKindIdentifier.DueUndistributedAccurate:
					case DueDocKindIdentifier.DueUndistributedAccurateDSV:
						OpenForm<DueUndistributedAccurateView>(item, ViewModelState.Read);
						break;

					case DueDocKindIdentifier.Treasurity:
					case DueDocKindIdentifier.PenaltyTreasurity:
						OpenForm<TreasurityView>(item, ViewModelState.Read);
						break;
					#endregion

					#region accoperations;
					case AccDocKindIdentifier.EnrollmentPercents:
						OpenForm<EnrollmentPercents>(item, ViewModelState.Read);
						break;
					case AccDocKindIdentifier.EnrollmentOther:
						OpenForm<EnrollmentOtherView>(item, ViewModelState.Read);
						break;
					case AccDocKindIdentifier.Transfer:
						OpenForm<TransferToAccount>(item, ViewModelState.Read);
						break;
					case AccDocKindIdentifier.Withdrawal:
						OpenForm<WithdrawalView>(item, ViewModelState.Read);
						break;
					#endregion

					#region CB & commissions
					case BalanceIdentifier.s_BuyingCB:
						App.DashboardManager.OpenNewTab(typeof(OrderReportView), ViewModelState.Read, item.ID, OrderReportsHelper.BUYING_REPORT);
						break;
					case BalanceIdentifier.s_SellingCB:
						App.DashboardManager.OpenNewTab(typeof(OrderReportView), ViewModelState.Read, item.ID, OrderReportsHelper.SELLING_REPORT);
						break;
					case CostIdentifier.s_CBFee:
					case CostIdentifier.s_TradingCommission:
					case CostIdentifier.s_DepositServicesCommission:
					case CostIdentifier.s_CurrencyBuySellCommission:
					case CostIdentifier.s_CurrencyBuySellCommission_ForSaldoBugfix:
					case CostIdentifier.s_DirectDebiting:
						OpenForm<CostsView>(item, ViewModelState.Read);
						break;
					case IncomeSecIdentifier.s_CouponYield:
					case IncomeSecIdentifier.s_SecurityRedemption:
						OpenForm<IncomeSecurityView>(item, ViewModelState.Read);
						break;
					case IncomeSecIdentifier.s_DepositsPercents:
						if (item.ForeignKey == 0)//PaymentHistory
							OpenForm<PaymentHistoryView>(item, ViewModelState.Read);
						else
							OpenForm<IncomeSecurityView>(item, ViewModelState.Read);
						break;
					#endregion CB

					#region UK transfer

					case BalanceIdentifier.s_TransferFromPFRToUK:
					case BalanceIdentifier.s_ReturnFromUKToPFR:
					case BalanceIdentifier.s_TransferFromPFRToUK_VR:
					case BalanceIdentifier.s_ReturnFromUKToPFR_VR:
						OpenForm<CommonPPView>(item, ViewModelState.Read);
						break;

					#endregion

					#region Schils

					case BalanceIdentifier.s_TransferCostsILS:
						OpenForm<TransferSchilsView>(item, ViewModelState.Read);
						break;
					case BalanceIdentifier.s_ReturnCostsILS:
						OpenForm<ReturnView>(item, ViewModelState.Read);
						break;

					#endregion

					#region deposit

					case BalanceIdentifier.s_DepositPlacement:
					case BalanceIdentifier.s_DepositReturn:
						//case BalanceIdentifier.s_DepositPercent:
						if (item.ForeignKey > 0)
							OpenForm<DepositView>(item, ViewModelState.Read);
						else
							MessageBox.Show("Данные мигрированы. Форма не подлежит открытию");
						break;

					#endregion

					default:
						var lowerKind = operation.ToLower();

						#region Register

						if (lowerKind.Contains(RegisterIdentifier.NPFtoPFR.ToLower()) || lowerKind.Contains(RegisterIdentifier.PFRtoNPF.ToLower()))
						{
							OpenForm<CommonPPView>(item, ViewModelState.Read);
						}


						#endregion

						break;
				}
			}
		}

		void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			if (Grid.View.GetRowElementByMouseEventArgs(e) == null) return;

			if (!IsDocSelected())
				return;

			var item = GetSelectedItem();

			if (item == null)
				return;

			if (item.ForeignKey < 0)
			{
				MessageBox.Show("Данные мигрированы. Форма не подлежит открытию");
				return;
			}

			OpenFormForItem(item);
			
			var model = App.DashboardManager.GetActiveViewModel() as ViewModelCard;
			if (model != null)
			{
				model.IsDataChanged = true; // маскируем для CanExecuteSave чтобы узнать результат Validate
				model.IsDataChanged = !model.CanExecuteSave(); // если есть невалидные поля при закрытии отображаем диалог
			}
		}

		private void Grid_CustomColumnSort(object sender, CustomColumnSortEventArgs e)
		{
			if (e.Value1 == null)
			{
				if (e.Value2 == null)
					//оба null
					e.Result = 0;
				else
					//Value1 null Value2 не null - Value2 > Value1
					e.Result = -1;
			}
			else
			{
				if (e.Value2 == null)
					//Value1 не null Value2 - Value1 > Value2
					e.Result = 1;
				else
				{
					// оба не null - сравниваем как месяцы
					var months = new List<string>(DateTools.Months);
					var idx1 = months.IndexOf(e.Value1.ToString().Trim());
					var idx2 = months.IndexOf(e.Value2.ToString().Trim());
					e.Result = Comparer<int>.Default.Compare(idx1, idx2);
				}
			}
			e.Handled = true;
		}

	    readonly Dictionary<int, decimal> _sumgroup = new Dictionary<int, decimal>();

		private readonly List<BalanceListItem> _listSummary = new List<BalanceListItem>();

		private void Grid_CustomSummary(object sender, CustomSummaryEventArgs e)
		{
			if (e.GroupLevel == 0 && !_sumgroup.ContainsKey(e.GroupLevel))
				_sumgroup.Add(e.GroupLevel, 0);

			var row = Grid.GetRow(e.RowHandle) as BalanceListItem;

			if (row == null)
				return;

			//var item = e.Item as GridSummaryItem;

			switch (e.SummaryProcess.ToString().ToLower())
			{
				case "start":
					_listSummary.Clear();
					break;
				case "calculate":
					_listSummary.Add(row);
					break;
				case "finalize":
					var item = e.Item as GridSummaryItem;
					//var groups = Grid.GetGroupedColumns().Select(g => g.FieldName).ToList();
					//До портфеля/счёта берём последнёё по дате - после сумму.
					//var index = Math.Max(groups.IndexOf("PORTFOLIO"), groups.IndexOf("ACCOUNT")) + 1;
					//var isSum = e.GroupLevel <= index;

					if (_listSummary.Count == 0)
						return;

					switch (item.FieldName)
					{
						case "Start":

							e.TotalValue = _listSummary.GroupBy(b => new { Portfolio = b.PORTFOLIO, Account = b.ACCOUNT }).Select(b => b.OrderBy(bb => bb.Date).ThenBy(bb => bb.ID).First())
											.Sum(b => b.Start);

							break;
						case "Finish":

							e.TotalValue = _listSummary.GroupBy(b => new { Portfolio = b.PORTFOLIO, Account = b.ACCOUNT }).Select(b => b.OrderBy(bb => bb.Date).ThenBy(bb => bb.ID).Last())
												.Sum(b => b.Finish);
							break;

					}
					break;
			}
		}
	}
}
