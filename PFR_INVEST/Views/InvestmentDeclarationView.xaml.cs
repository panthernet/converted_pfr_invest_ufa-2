﻿using System.Windows.Controls;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for InvestmentDeclarationView.xaml
    /// </summary>
    public partial class InvestmentDeclarationView : UserControl
    {
        public InvestmentDeclarationView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }
    }
}
