﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DevExpress.Xpf.Editors;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
	/// <summary>
	/// Interaction logic for RegexEdit.xaml
	/// </summary>
	public partial class RegexEdit : UserControl
	{
		public RegexEdit()
		{
			InitializeComponent();
			var mask = DependencyPropertyDescriptor.FromProperty(MaskProperty, typeof(RegexEdit));
			mask.AddValueChanged(this, delegate { UpdateSource(); });
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        public static readonly DependencyProperty TextProperty =
			DependencyProperty.Register("Text", typeof(string), typeof(RegexEdit));

		public string Text
		{
			get { return (string) GetValue(TextProperty); }
			set { SetValue(TextProperty, value); }
		}

		public static readonly DependencyProperty MaskProperty =
			DependencyProperty.Register("Mask", typeof (string), typeof(RegexEdit));

		public string Mask
		{
			get { return (string) GetValue(MaskProperty); }
			set { SetValue(MaskProperty, value); }
		}

		private void UpdateSource()
		{
			var expr = textEdit.GetBindingExpression(TextEditBase.TextProperty);
		    expr?.UpdateSource();
		}

		private void OnTextEditPreviewKeyDown(object sender, KeyEventArgs e)
		{
			UpdateSource();
		}
	}
}
