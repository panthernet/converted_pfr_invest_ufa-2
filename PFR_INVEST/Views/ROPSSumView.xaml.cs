﻿using System.Windows.Controls;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for DivisionView.xaml
    /// </summary>
    public partial class ROPSSumView : UserControl
    {
		public ROPSSumView()
        {
            InitializeComponent();          
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }
    }
}
