﻿using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.ViewModelsCard;
using System.Windows.Controls;
using System.Windows.Input;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for OpfrRegisterView.xaml
    /// </summary>
    public partial class OpfrRegisterView : UserControl
    {
        public OpfrRegisterView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
            ppColumn.Visible = App.ServerSettings.IsOpfrTransfersEnabled;
        }

        private OpfrRegisterViewModel Model => (OpfrRegisterViewModel)DataContext;

        private void TableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (grid.View.GetRowElementByMouseEventArgs(e) == null) return;
            if (grid.View.FocusedRowHandle >= 0)
                App.DashboardManager.OpenNewTab(typeof(OpfrTransferView), ViewModelState.Edit,
                    (long)grid.GetCellValue(grid.View.FocusedRowHandle, "ID"), 
                    (object)Model.ID, "Перечисление");

        }
    }
}
