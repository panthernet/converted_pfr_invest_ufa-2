﻿using System;
using System.Collections.Generic;
using System.Windows;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Views.Wizards;

namespace PFR_INVEST.Views.LetterToNPFWizard
{
    /// <summary>
    /// Interaction logic for LetterToNPFWizardView.xaml
    /// </summary>
    public partial class LetterToNPFWizardView
    {
        private LetterToNPFWizardViewModel Model => DataContext as LetterToNPFWizardViewModel;

        private readonly bool _isUk;
        public LetterToNPFWizardView(bool isuk)
        {
            _isUk = isuk;
            InitializeComponent();
            InitWizardSteps();
        }

        public void InitWizardSteps()
        {
            ctlWizard.WizardSteps.Add(new KeyValuePair<string, Type>("SelectParameters", typeof(SelectParametersView)));
            ctlWizard.WizardSteps.Add(!_isUk ? new KeyValuePair<string, Type>("SelectNPF", typeof (SelectNPFView)) : new KeyValuePair<string, Type>("SelectNPF", typeof (SelectUKView)));
            ctlWizard.WizardSteps.Add(new KeyValuePair<string, Type>("FinishScreen", typeof(FinishScreenView)));

            Loaded += PlanCorrectionWizardView_Loaded;
            Wizard.Cancel += Wizard_Cancel;
            Wizard.Complete += Wizard_Complete;
        }

        private void Wizard_Complete(object sender, RoutedEventArgs e)
        {
            Model.OpenCreatedFolder();
            DashboardManager.CloseActiveView();
            Wizard.Cancel -= Wizard_Cancel;
            Wizard.Complete -= Wizard_Complete;
            ctlWizard.Dispose();
        }

        private void Wizard_Cancel(object sender, RoutedEventArgs e)
        {
            DashboardManager.CloseActiveView();
            Wizard.Cancel -= Wizard_Cancel;
            Wizard.Complete -= Wizard_Complete;
            ctlWizard.Dispose();
        }

        private void PlanCorrectionWizardView_Loaded(object sender, RoutedEventArgs e)
        {
            ctlWizard.ShowStep(0);
            ctlWizard.btnFinish.Content = "Готово";
        }

        public IWizard Wizard => ctlWizard;
    }
}
