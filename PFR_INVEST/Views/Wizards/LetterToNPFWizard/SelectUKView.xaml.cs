﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Views.Wizards;

namespace PFR_INVEST.Views.LetterToNPFWizard
{
    /// <summary>
    /// Interaction logic for SelectNPFView.xaml
    /// </summary>
    public partial class SelectUKView : IWizardStep
    {
        private const string WARNING = "Не выбрано ни одного НПФ!";

        public SelectUKView()
        {
            InitializeComponent();
        }

        public LetterToNPFWizardViewModel Model => DataContext as LetterToNPFWizardViewModel;

        public IWizard Wizard
        {
            get;
            set;
        }

        public bool OnMoveNext()
        {
            if (Model.NPFList.Count(item => item.Selected) != 0)
                return true;
            DXMessageBox.Show(WARNING, Properties.Resources.ApplicationName, MessageBoxButton.OK, MessageBoxImage.Error);
            return false;
        }

        public bool OnMovePrev()
        {
            return true;
        }

        public void OnShow()
        {
            var shouldRequest = true;

            while (shouldRequest)
            {
                try
                {
                    Model.EnsureAvailableList();
                    grdCC.RefreshData();
                    shouldRequest = false;
                }
                catch (Exception ex)
                {
                    App.log.WriteException(ex);
                    var res = DXMessageBox.Show("Произошла неизвестная ошибка.\nПовторить операцию?", "Неизвестная ошибка", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                    shouldRequest = res == MessageBoxResult.Yes;
                    if (!shouldRequest)
                        Wizard.DoCancel();
                }
            }
        }

        public WizardNavigationButtonState NextButtonState => WizardNavigationButtonState.Visible;

        public WizardNavigationButtonState PrevButtonState => WizardNavigationButtonState.Visible;

        public WizardNavigationButtonState CancelButtonState => WizardNavigationButtonState.Visible;

        private void SetIsSelected(long id, bool value)
        {
            Model.NPFList.First(item => item.ID == id).Selected = value;
        }

        private void UpdateSelectionForAll(bool newIsSelected)
        {
            var list = GetDataRowHandles();
            for (var i = 0; i < list.Count; i++)
            {
                var rowHandle = grdCC.GetRowHandleByListIndex(i);
                grdCC.SetCellValue(rowHandle, "Selected", newIsSelected);
                SetIsSelected(Convert.ToInt32(grdCC.GetCellValue(rowHandle, "ID")), newIsSelected);
            }
        }

        private bool _mGridRowCheckBoxClicked;
        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            _mGridRowCheckBoxClicked = true;
            if (Model.NPFList.Count(item => item.Selected) == 0)
                _headerCheckEdit.IsChecked = false;
            else if (Model.NPFList.Count(item => item.Selected) != Model.NPFList.Count)
                _headerCheckEdit.IsChecked = null;
            else
                _headerCheckEdit.IsChecked = true;
        }

        private void CheckEdit_Checked(object sender, RoutedEventArgs e)
        {
            if (!_mGridRowCheckBoxClicked)
                UpdateSelectionForAll(true);
            _mGridRowCheckBoxClicked = false;
        }

        private void CheckEdit_Indeterminate(object sender, RoutedEventArgs e)
        {
            if (!_mGridRowCheckBoxClicked)
                _headerCheckEdit.IsChecked = false;
            _mGridRowCheckBoxClicked = false;
        }

        private void CheckEdit_Unchecked(object sender, RoutedEventArgs e)
        {
            UpdateSelectionForAll(false);
            _mGridRowCheckBoxClicked = false;
        }

        private List<int> GetDataRowHandles()
        {
            var rowHandles = new List<int>();
            for (var i = 0; i < grdCC.VisibleRowCount; i++)
            {
                var rowHandle = grdCC.GetRowHandleByVisibleIndex(i);
                if (grdCC.IsGroupRowHandle(rowHandle))
                {
                    if (!grdCC.IsGroupRowExpanded(rowHandle))
                    {
                        rowHandles.AddRange(GetDataRowHandlesInGroup(rowHandle));
                    }
                }
                else
                    rowHandles.Add(rowHandle);
            }
            return rowHandles;
        }

        private List<int> GetDataRowHandlesInGroup(int groupRowHandle)
        {
            var rowHandles = new List<int>();
            for (var i = 0; i < grdCC.GetChildRowCount(groupRowHandle); i++)
            {
                var rowHandle = grdCC.GetChildRowHandle(groupRowHandle, i);
                if (grdCC.IsGroupRowHandle(rowHandle))
                {
                    rowHandles.AddRange(GetDataRowHandlesInGroup(rowHandle));
                }
                else
                    rowHandles.Add(rowHandle);
            }
            return rowHandles;
        }

        private CheckBox _headerCheckEdit;
        private void CheckEdit_Loaded(object sender, RoutedEventArgs e)
        {
            _headerCheckEdit = sender as CheckBox;
        }
    }
}
