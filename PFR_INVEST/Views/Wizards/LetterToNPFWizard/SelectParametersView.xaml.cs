﻿using System.Windows;
using System.Windows.Forms;
using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Views.Wizards;

namespace PFR_INVEST.Views.LetterToNPFWizard
{
    /// <summary>
    /// Interaction logic for SelectParametersView.xaml
    /// </summary>
    public partial class SelectParametersView : IWizardStep
    {
        private const string SELECT_FOLDER_DIALOG_DESC = "Выберите папку для сохранения писем.";
        private const string INVALID_SELECTED_FOLDER = "Неверно указана папка для сохранения писем!";
        private const string WARNING = "Внимание! Если выбранная папка не существует, она будет создана. В случае, если выбранная папка существует, все существующие письма в ней будут перезаписаны!";
        private const string INITIAL_SELECTED_FOLDER = "C:\\";

        public SelectParametersView()
        {
            InitializeComponent();
            txtSelectedFolder.EditValue = INITIAL_SELECTED_FOLDER;
            txtWarnig.Text = WARNING;
        }

        public IWizard Wizard
        {
            get;
            set;
        }

        public LetterToNPFWizardViewModel Model => DataContext as LetterToNPFWizardViewModel;

        public bool OnMoveNext()
        {
            var folder = txtSelectedFolder.Text;
            if (!MonthTransferCreationWizardModel.CheckDirectoryName(folder))
            {
                DXMessageBox.Show(INVALID_SELECTED_FOLDER, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            Model.SelectedFolder = folder;
            return true;
        }

        public bool OnMovePrev()
        {
            return false;
        }

        public void OnShow()
        {
            Wizard.UpdateNavigationControlsState();
        }

        public WizardNavigationButtonState NextButtonState => WizardNavigationButtonState.Visible;

        public WizardNavigationButtonState PrevButtonState => WizardNavigationButtonState.Hidden;

        public WizardNavigationButtonState CancelButtonState => WizardNavigationButtonState.Visible;

        private void SelecFolderButton_Click(object sender, RoutedEventArgs e)
        {
            using (var dlg = new FolderBrowserDialog() { Description = SELECT_FOLDER_DIALOG_DESC })
            {

                if (MonthTransferCreationWizardModel.DirectoryExists(txtSelectedFolder.Text))
                    dlg.SelectedPath = txtSelectedFolder.Text;

                if (dlg.ShowDialog() == DialogResult.OK)
                    txtSelectedFolder.EditValue = dlg.SelectedPath;
            }
        }
    }
}
