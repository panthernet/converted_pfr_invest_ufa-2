﻿using DevExpress.Data;
using PFR_INVEST.BusinessLogic.ViewModelsWizards;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views.Wizards.DepositCountWizard
{
    public class ShowDepositsReturnStepViewUserControl : GenericWizardStepEx<DepositCountWizardViewModel> { }
    /// <summary>
    /// Interaction logic for ShowDepositsReturnStepView.xaml
    /// </summary>
    public partial class ShowDepositsReturnStepView : IWizardStepEx
    {
        public ShowDepositsReturnStepView()
        {
            InitializeComponent();
            Grid.View.ShowGridMenu += (sender, e) => DevExpressHelper.HideGroupingFromColumnMenu(e);
            ModelInteractionHelper.SignUpForCustomAction(this, o => Grid.UpdateGroupSummary());
            FinishButtonText = "Вернуть депозиты";
        }

        private void grid_CustomSummary(object sender, CustomSummaryEventArgs e)
        {
            if (!e.IsGroupSummary) return;
            if (e.SummaryProcess == CustomSummaryProcess.Calculate)
                e.TotalValue = "Итого: ";
        }
    }
}
