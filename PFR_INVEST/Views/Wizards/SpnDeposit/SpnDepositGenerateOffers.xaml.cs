﻿using PFR_INVEST.BusinessLogic.ViewModelsWizards;

namespace PFR_INVEST.Views.Wizards.SpnDeposit
{
    public class SpnDepositGenerateOffersUserControl : GenericWizardStepEx<SpnDepositWizardViewModel> { }

    /// <summary>
    /// Interaction logic for SpnDepositExportCutoffRate.xaml
    /// </summary>
    public partial class SpnDepositGenerateOffers
    { 
        public SpnDepositGenerateOffers()
        {
            InitializeComponent();
            FinishButtonText = "Сгенерировать оферты";
        }
    }
}
