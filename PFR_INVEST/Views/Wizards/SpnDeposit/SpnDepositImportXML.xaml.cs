﻿using PFR_INVEST.BusinessLogic.ViewModelsDialog;
using PFR_INVEST.BusinessLogic.ViewModelsWizards;

namespace PFR_INVEST.Views.Wizards.SpnDeposit
{
    public class SpnDepositImportXMLUserControl : GenericWizardStepEx<SpnDepositWizardViewModel> { }

    /// <summary>
    /// Interaction logic for SpnDepositImportXML.xaml
    /// </summary>
    public partial class SpnDepositImportXML
    {
        private SpnDepositWizardViewModel _model;
        private ImportDepClaimBatchDlgViewModel _dcModel;

        protected override SpnDepositWizardViewModel Model => _model;

        public SpnDepositImportXML()
        {
            InitializeComponent();
            DataContextChanged += (sender, e) =>
            {
                if (e.NewValue is SpnDepositWizardViewModel)
                    _model = e.NewValue as SpnDepositWizardViewModel;
            };
            NextButtonText = "Импортировать";
        }

        /// <summary>
        /// Вызывается при успешном переходе на следующий шаг, до самого перехода
        /// </summary>
        public override bool OnMoveNext()
        {
            _dcModel.ImportCommand.Execute(null);
            return !_dcModel.IsAttemptFailed && _model.OnMoveNext();
        }

        /// <summary>
        /// Вызывается один раз при создании шага
        /// </summary>
        public override void OnInitialization()
        {
            DataContext = _dcModel = new ImportDepClaimBatchDlgViewModel(_model.Auction.ID) {IsVerbose = false};
            _model.OnInitialization();
        }

        /// <summary>
        /// Вызывается для проверки доступности кнопки Далее
        /// </summary>
        public override bool CanExecuteNext()
        {
            return _dcModel.ImportCommand.CanExecute(null);
        }

        public override void OnShow()
        {
            _dcModel.UpdateAuction(_model.Auction.ID);
        }

        public override bool CanExecutePrev()
        {
            return false;
        }
    }
}
