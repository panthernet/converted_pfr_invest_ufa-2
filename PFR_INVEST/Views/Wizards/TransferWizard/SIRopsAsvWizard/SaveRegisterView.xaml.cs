﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.ViewModelsWizards.SI.RopsAsvWizard;

namespace PFR_INVEST.Views.Wizards.TransferWizard.SIRopsAsvWizard
{
    /// <summary>
    /// Interaction logic for SaveRegisterView.xaml
    /// </summary>
    public partial class SaveRegisterView : UserControl, IWizardStep
    {
        public SaveRegisterView()
        {
            InitializeComponent();
        }
        public IWizard Wizard
        {
            get;
            set;
        }

        public TransferSIRopsAsvWizardViewModel Model => DataContext as TransferSIRopsAsvWizardViewModel;

        public bool OnMoveNext()
        {
            return true;
        }

        public bool OnMovePrev()
        {
            return true;
        }


        public void OnShow()
        {
            CreateObject();
            _created = true;
            Wizard.UpdateNavigationControlsState();
            Wizard.SetDimensions(550, 120);
        }

        public WizardNavigationButtonState NextButtonState => _created ? WizardNavigationButtonState.Visible : WizardNavigationButtonState.Disabled;

        public WizardNavigationButtonState PrevButtonState => WizardNavigationButtonState.Hidden;

        public WizardNavigationButtonState CancelButtonState => _created ? WizardNavigationButtonState.Hidden : WizardNavigationButtonState.Visible;

        private bool _created;

        private void CreateObject()
        {
            bool shouldRequest = true;

            while (shouldRequest)
            {
                try
                {
                    if (!Model.CreateRegisterEx())
                    {
                        ViewModelBase.DialogHelper.ShowError("Внутренняя ошибка при сохранении данных на сервере!\nПроизведен откат, данные не сохранены.");
                        const string exText = "EXCEPTION: На стороне сервиса при сохранении данных мастера перечислений! CreateRegisterEx()";
                        App.log.WriteLine(exText);
                        throw new Exception(exText);
                    }
                    shouldRequest = false;
                }
                catch (Exception ex)
                {
                    App.log.WriteException(ex);
                    var res = DXMessageBox.Show("Произошла неизвестная ошибка.\nПовторить операцию?", "Неизвестная ошибка", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                    shouldRequest = res == MessageBoxResult.Yes;
                    if (!shouldRequest)
                        Wizard.DoCancel();
                }
            }
            if (Model.CreateSITransfers)
                txtNumber.EditValue = Model.RegisterID;
            else
                txtNumber.EditValue = Model.SelectedYear.Name;
        }
    }
}
