﻿using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Views.Wizards;

namespace PFR_INVEST.Views.PlanCorrectionWizard
{
    /// <summary>
    /// Interaction logic for YearMonthSelectView.xaml
    /// </summary>
    public partial class YearMonthSelectView : IWizardStep
    {
        public YearMonthSelectView()
        {
            InitializeComponent();
            //if(Model.YearsList == null)
            //    Model.InitializeDates();
        }

        public PlanCorrectionWizardViewModel<PlanContractRecord> Model => DataContext as PlanCorrectionWizardViewModel<PlanContractRecord>;

        public IWizard Wizard { get; set; }

        public bool OnMoveNext()
        {
            return true;
        }

        public bool OnMovePrev()
        {
            return true;
        }

        public void OnShow()
        {
            //Wizard.SetDimensions(450, 310);
        }

        public WizardNavigationButtonState NextButtonState => WizardNavigationButtonState.Visible;

        public WizardNavigationButtonState PrevButtonState => WizardNavigationButtonState.Visible;

        public WizardNavigationButtonState CancelButtonState => WizardNavigationButtonState.Visible;
    }
}
