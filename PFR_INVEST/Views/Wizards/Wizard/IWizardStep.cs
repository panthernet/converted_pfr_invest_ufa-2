﻿namespace PFR_INVEST.Views.Wizards
{
	public interface IWizardStep
	{
		IWizard Wizard { get; set; }		
		bool OnMoveNext();
		bool OnMovePrev();
        void OnShow();

        WizardNavigationButtonState NextButtonState { get; }
        WizardNavigationButtonState PrevButtonState { get; }
        WizardNavigationButtonState CancelButtonState { get; }        
	}

    public enum WizardNavigationButtonState
    { 
        Visible,
        Hidden,
        Disabled
    }

}
