﻿using System.Windows.Controls;
using PFR_INVEST.BusinessLogic.Interfaces;

namespace PFR_INVEST.Views.Wizards
{
    public abstract class GenericWizardStepEx<T>: UserControl, IWizardStepEx
        where T: IWizardExModel
    {
        public string NextButtonText { get; protected set; } = "Далее >>";
        public string FinishButtonText { get; protected set; } = "Завершить";
        public string PrevButtonText { get; protected set; } = "<< Назад";
        public string CancelButtonText { get; protected set; } = "Отмена";

        protected virtual T Model => (T)DataContext;

        public IWizard Wizard { get; set; }

        /// <summary>
        /// Вызывается при успешном переходе на следующий шаг, до самого перехода
        /// </summary>
        public virtual bool OnMoveNext()
        {
            return Model.OnMoveNext();
        }

        /// <summary>
        /// Вызывается при успешном возврате на предыдущий шаг, до самого перехода
        /// </summary>
        public virtual bool OnMovePrev()
        {
            return Model.OnMovePrev();
        }

        /// <summary>
        /// Вызывается каждый раз при отображении шага
        /// </summary>
        public virtual void OnShow()
        {
            Model.OnShow();
        }

        /// <summary>
        /// Вызывается один раз при создании шага
        /// </summary>
        public virtual void OnInitialization()
        {
            Model.OnInitialization();
        }

        /// <summary>
        /// Вызывается для проверки доступности кнопки Отмена
        /// </summary>
        public virtual bool CanExecuteCancel()
        {
            return true;
        }

        /// <summary>
        /// Вызывается для проверки доступности кнопки Далее
        /// </summary>
        public virtual bool CanExecuteNext()
        {
            return Model.CanExecuteNext();
        }

        /// <summary>
        /// Вызывается для проверки доступности кнопки Назад
        /// </summary>
        public virtual bool CanExecutePrev()
        {
            return Model.CanExecutePrev();
        }

        public WizardNavigationButtonState NextButtonState { get; protected set; } = WizardNavigationButtonState.Visible;
        public WizardNavigationButtonState PrevButtonState { get; protected set; } = WizardNavigationButtonState.Visible;
        public WizardNavigationButtonState CancelButtonState { get; protected set; } = WizardNavigationButtonState.Visible;
    }
}
