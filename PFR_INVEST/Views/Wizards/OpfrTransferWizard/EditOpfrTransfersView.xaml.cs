﻿using PFR_INVEST.BusinessLogic.ViewModelsWizards.OpfrTransfer;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views.Wizards.OpfrTransferWizard
{
    public class EditOpfrTransfersViewUserControl : GenericWizardStepEx<OpfrTransferWizardViewModel> { }
    /// <summary>
    /// Interaction logic for EditOpfrTransfersView.xaml
    /// </summary>
    public partial class EditOpfrTransfersView
    {
        public EditOpfrTransfersView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshData(this, grid, "ImportHandler");
        }
        /// <summary>
        /// Вызывается каждый раз при отображении шага
        /// </summary>
        public override void OnShow()
        {
            Model.OnShow();
            grid.RefreshData();
        }
    }
}
