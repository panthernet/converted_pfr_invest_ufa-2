﻿using PFR_INVEST.BusinessLogic.ViewModelsWizards.OpfrTransfer;

namespace PFR_INVEST.Views.Wizards.OpfrTransferWizard
{
    public class CreateOpfrRegisterViewUserControl : GenericWizardStepEx<OpfrTransferWizardViewModel> { }

    /// <summary>
    /// Interaction logic for CreateOpfrRegisterView.xaml
    /// </summary>
    public partial class CreateOpfrRegisterView
    {
        public CreateOpfrRegisterView()
        {
            InitializeComponent();
        }
    }
}
