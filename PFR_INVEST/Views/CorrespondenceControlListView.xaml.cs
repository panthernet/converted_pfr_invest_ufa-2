﻿using System.Windows.Input;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    using DataObjects.ListItems;

    public abstract partial class CorrespondenceControlListView
    {
        protected CorrespondenceControlListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, Grid);
        }

        protected abstract void OpenItem(CorrespondenceListItemNew item);

        private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var item = Grid.CurrentItem as CorrespondenceListItemNew;
            if (item != null)
                OpenItem(item);
        }
    }
}
