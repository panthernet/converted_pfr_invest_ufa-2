﻿using PFR_INVEST.BusinessLogic;
using System.Windows;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for CostsView.xaml
    /// </summary>
    public partial class CostsView
    {
        public CostsView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        private CostsViewModel Model => DataContext as CostsViewModel;

        private void ButtonInfo_Click(object sender, RoutedEventArgs e)
        {
            Model?.GetCourceManuallyOrFromCBRSite();
        }
    }
}
