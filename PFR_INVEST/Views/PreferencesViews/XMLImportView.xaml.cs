﻿using System.Windows;
using System.Windows.Forms;
using PFR_INVEST.BusinessLogic.ViewModelsCard;
using UserControl = System.Windows.Controls.UserControl;

namespace PFR_INVEST.Views.PreferencesViews
{
    /// <summary>
    /// Interaction logic for XMLImportView.xaml
    /// </summary>
    public partial class XMLImportView : UserControl
    {
        private XMLImportViewModel VM => DataContext as XMLImportViewModel;
        public XMLImportView()
        {
            InitializeComponent();
          //  Loaded += XMLImportView_Loaded;
           
        }

        //private void XMLImportView_Loaded(object sender, RoutedEventArgs e)
        //{
        //    VM.RequestClose += VM_RequestClose;
        //}

        //private void VM_RequestClose(object sender, System.EventArgs e)
        //{
        //    DashboardManager.CloseActiveView();
        //}

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            FileDialog fd = new OpenFileDialog();
            fd.Filter = "Файлы итогов торгов (*.xml)|*.xml";
            if (fd.ShowDialog() == DialogResult.OK)
            {
                VM.FilePath = fd.FileName;
            }
        }

        //private void Close_OnClick(object sender, RoutedEventArgs e)
        //{
        //    DashboardManager.CloseActiveView();
        //}
    }
}
