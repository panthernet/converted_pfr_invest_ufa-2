﻿using System.Windows.Controls;
using System;
using DevExpress.Xpf.Editors;
using PFR_INVEST.Extensions;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for MonthAccurateView.xaml
    /// </summary>
    public partial class QuarterAccurateView : UserControl
    {
        public QuarterAccurateView()
        {
            InitializeComponent();
            ModelInteractionHelper.SignUpForCloseRequest(this, true);
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

		#region Numeric field edit fix
		private bool IsManualChange = false;

		private void OnGotFocus(object sender, EventArgs e)
		{
			IsManualChange = true;
		}

		private void OnLostFocus(object sender, EventArgs e)
		{
			IsManualChange = false;
		}

		private void OnEditValueChanged(object sender, EditValueChangedEventArgs e)
		{
			if (IsManualChange)
			{
				this.Dispatcher.RemoveTextLeadingZero(sender, e);
			}
		}
		#endregion 
	}
}
