﻿namespace PFR_INVEST.Views
{
    using BusinessLogic;

    internal class InsuranceArchiveSIListView:InsuranceArchiveListView
    {
        protected override void OpenItem(long id)
        {
            App.DashboardManager.OpenNewTab(typeof(InsuranceSIView), ViewModelState.Edit, id, "Договор страхования СИ");
        }
    }
}
