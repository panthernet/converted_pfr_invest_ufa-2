﻿using System;
using System.Configuration;
using System.ServiceModel;
using System.ServiceModel.Configuration;
using System.ServiceModel.Security;
using System.Windows;
using db2connector.Contract;
using DevExpress.Xpf.Core;
using PFR_INVEST.Auth.SharedData;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.Views;
using PFR_INVEST.Views.Login;

namespace PFR_INVEST
{
    public partial class MainWindow
    {
        private bool OpenConnection(bool isRetry)
        {
            WCFClient.UseAutoRepair = false;
            WCFClient.ClientCredentials = new System.ServiceModel.Description.ClientCredentials();

            string session = null;
            bool lResult;
            if (WCFClient.AuthType == ClientAuthType.None)
            {
                var loginWindow = new LoginView();
                Tools.ThemesTool.SetCurrentTheme(loginWindow);
                lResult = loginWindow.ShowDialog().GetValueOrDefault();
                if (lResult)
                {
                    WCFClient.ClientCredentials.Windows.ClientCredential.UserName = loginWindow.txtLogin.Text;
                    WCFClient.ClientCredentials.Windows.ClientCredential.Password = loginWindow.txtPassword.Password;
                }
            }
            else
            {
                if (isRetry)
                    lResult = false;
                else
                {
                    string userName;
                    lResult = AuthInEcasa(out userName, out session);
                    if (lResult)
                        WCFClient.ClientCredentials.Windows.ClientCredential.UserName = userName;
                }
            }

            if (!lResult)
            {
                _mRetryToConnect = false;
                Application.Current.Shutdown();
                return false;
            }

            try
            {
                //авторизация
                WCFClient.CreateClientByCredentials(false, session);
                WCFClient.UseAutoRepair = true;
                //логирование авторизации
                JournalLogger.LogAuthorizationEvent();
            }
            catch (AuthInitException e)
            {
                if (AppSettings.IsAuthDebugEnabled) App.log.WriteLine("AUTH: general fault");

                Loading.CloseWindow();
                if (e.Ex is InvalidLoginOrPasswordException)
                {
                    DXMessageBox.Show(Properties.Resources.InvalidLoginOrPasswordErrorString,
                                      Properties.Resources.ApplicationName, MessageBoxButton.OK,
                                      MessageBoxImage.Error);
                    App.log.WriteException(e.Ex);
                    //логирование неудачной авторизации находится в работе с Auth коннектором
                    return false;
                }

                if (e.Ex is UserMustChangePasswordException)
                {
                    Tools.ThemesTool.ApplyGlobalTheme(Tools.ThemesTool.GetActualTheme());

                    App.log.WriteException(e.Ex);
                    if (DXMessageBox.Show("Необходимо сменить пароль пользователя! Продолжить?",
                                      "Внимание", MessageBoxButton.OKCancel, MessageBoxImage.Warning) == MessageBoxResult.OK)
                    {
                        var pResult = AuthMsg.Denied;
                        string tmpPwd = null;
                        do
                        {
                            var pwdWindow = new ChangePasswordWindow(true) {txtOldPassword = {Password = tmpPwd}};
                            Tools.ThemesTool.SetCurrentTheme(pwdWindow);

                            if (pwdWindow.ShowDialog().GetValueOrDefault())
                            {
                                try
                                {
                                    //App.log.WriteLine(string.Format("pwd: {0}", pwdWindow.txtPassword.Password));
                                    using (Loading.StartNew("Смена пароля пользователя..."))
                                    {
                                        var qResult = WCFClient.SetUserPassword(pwdWindow.txtPassword.Password, pwdWindow.txtOldPassword.Password);
                                        pResult = qResult.Response;
                                        if (pResult != AuthMsg.Denied)
                                        {
                                            Loading.CloseWindow();
                                            if (pResult == AuthMsg.FailPwdPolicy)
                                            {
                                                DXMessageBox.Show("Введенный пароль не соответствует политике безопасности АД!",
                                                    "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                                                tmpPwd = pwdWindow.txtOldPassword.Password;
                                                continue;
                                            }

                                            if (pResult == AuthMsg.FailPwdWrong)
                                            {
                                                DXMessageBox.Show("Введен неверный прежний пароль учетной записи!",
                                                    "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                                                continue;
                                            }

                                            DXMessageBox.Show("Пароль успешно изменен!",
                                                "Внимание", MessageBoxButton.OK, MessageBoxImage.Information);
                                            WCFClient.ClientCredentials.Windows.ClientCredential.Password = pwdWindow.txtPassword.Password;
                                            try
                                            {
                                                WCFClient.CreateClientByCredentials(false, session);
                                                return pResult == AuthMsg.Success;
                                            }
                                            catch (Exception ex)
                                            {
                                                App.log.WriteException(ex);
                                                return false;
                                            }
                                        }
                                    }

                                    DXMessageBox.Show("Не удалось установить новый пароль пользователя!",
                                        "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                                    return false;
                                }
                                catch (Exception ex)
                                {
                                    App.log.WriteException(ex);
                                    DXMessageBox.Show("Не удалось установить новый пароль пользователя!",
                                        "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                                }
                                finally
                                {
                                    Loading.CloseWindow();
                                }
                            }
                            else break;
                        } while (pResult == AuthMsg.FailPwdPolicy || pResult == AuthMsg.FailPwdWrong);
                    }
                    return false;
                }
                DXMessageBox.Show(Properties.Resources.ADInitErrorString,
                    Properties.Resources.ApplicationName, MessageBoxButton.OK,
                    MessageBoxImage.Error);

                App.log.WriteException(e.Ex);
                Application.Current.Shutdown();
                _bShutdown = true;
            }
            catch (NotUserException)
            {
                App.log.WriteLine("AUTH: CheckUserInRole() failed");
                Loading.CloseWindow();

                DXMessageBox.Show(Properties.Resources.NotUserErrorString,
                                  Properties.Resources.ApplicationName, MessageBoxButton.OK,
                                  MessageBoxImage.Error);

                Application.Current.Shutdown();
                _bShutdown = true;
                JournalLogger.LogAuthorizationEvent(false);
                return false;
            }
            catch (EndpointNotFoundException e)
            {
                Loading.CloseWindow();
                string message = Properties.Resources.InvalidEndPointErrorString;
                DXMessageBox.Show(message, Properties.Resources.ApplicationName, MessageBoxButton.OK,
                                  MessageBoxImage.Error);

                _mRetryToConnect = false;
                App.log.WriteException(e);
                Application.Current.Shutdown();
                _bShutdown = true;

                return false;
            }
            catch (SecurityNegotiationException e)
            {
                Loading.CloseWindow();
                DXMessageBox.Show(Properties.Resources.InvalidClientCredentialErrorString, Properties.Resources.ApplicationName, MessageBoxButton.OK,
                                  MessageBoxImage.Error);

                _mRetryToConnect = true;
                App.log.WriteException(e);
                try { JournalLogger.LogAuthorizationEvent(false); }
                catch
                {
                    // ignored
                }
                return false;
            }
            catch (Exception e)
            {
                Loading.CloseWindow();
                string message = Properties.Resources.ConnectionErrorString;
                DXMessageBox.Show(message, Properties.Resources.ApplicationName, MessageBoxButton.OK,
                                  MessageBoxImage.Error);

                _mRetryToConnect = true;
                App.log.WriteException(e);
                try { JournalLogger.LogAuthorizationEvent(false); }
                catch
                {
                    // ignored
                }
                return false;
            }
            return true;
        }

        private static bool AuthInEcasa(out string userName, out string session)
        {
          /*  var url = "";
            var clientSection = (ClientSection)ConfigurationManager.GetSection("system.serviceModel/client");
            for (var i = 0; i < clientSection.Endpoints.Count; i++)
            {
                if (clientSection.Endpoints[i].Name != "WSHttpsBinding_IDB2Connector")
                    continue;
                url = clientSection.Endpoints[i].Address.ToString();
                break;
            }
            */
            userName = "";
            session = "";
            var url = AppSettings.EcasaLoginUrl;
            if (string.IsNullOrEmpty(url))
            {
                ViewModelBase.DialogHelper.ShowError("Не указан параметр EcasaLoginUrl конфига клиента, необходимый для авторизации через ЕЦАСА!");
                return false;
            }

            #region Autologin - DO NOT DELETE
            /*  var result = false;

              var req =
                  (HttpWebRequest)WebRequest.Create(new Uri("https://sitepfr.fed.pfr.local:7893/dokip/db2connector.IISService.svc"));
              req.ImpersonationLevel = TokenImpersonationLevel.None;
              req.Method = "GET";
              session = "";
              userName = "ksp_admin";
              var password = "Qwer1234";
              var req_id = "";
              var res = req.GetResponse();
              if (res.ResponseUri.ToString().Contains("obrareq"))
              {
                  //var cContainer = new CookieContainer();
                  //cStrings.ForEach(a=> cContainer.Add(new Cookie(a.Key, a.Value, "/", res.ResponseUri.Host) { HttpOnly = true }));
                  //https://sitepfr.fed.pfr.local:7893/oam/server/obrareq.cgi?wh%3DIAMSuiteAgent%20wu%3D%2Fdokip%2Fdb2connector.IISService.svc%20wo%3D1%20rh%3Dhttps%3A%2F%2Fsitepfr.fed.pfr.local%3A7893%20ru%3D%252Fdokip%252Fdb2connector.IISService.svc
                  req = (HttpWebRequest)WebRequest.Create(res.ResponseUri);
                  req.Headers.Add(HttpRequestHeader.Cookie, @"ObSSOCookie=loggedoutcontinue");
                  req.Method = "GET";
                  var res2 = req.GetResponse();
                  using (var reader = new System.IO.StreamReader(res2.GetResponseStream(), Encoding.UTF8))
                  {
                      string str = reader.ReadToEnd();
                      req_id = str.Split('\n').FirstOrDefault(a => a.Contains("request_id"))?.Split('=')[3].Replace("\"","").Replace(">","");
                  }

                  var cStrings = new List<string>();
                  res2.Headers["Set-Cookie"].Split(',').Where(a => a.StartsWith("OAM")).ForEach(a =>
                  {
                      cStrings.Add(a.Split(';')[0]);
                  });
                  var reqCountCookie = cStrings.FirstOrDefault(a => a.StartsWith("OAM_REQ_COUNT"));
                  var cstr = "ObSSOCookie=loggedoutcontinue; " + string.Join("; ", cStrings.Where(a => a.StartsWith("OAM_REQ") || a.StartsWith("ObSSO")));

                  var host = res.ResponseUri.Authority;
                 // var prm =
                 //     "username=ksp_admin&password=Qwer1234&request_id=-1950869576331908287&displayLangSelection=false&Languages=";
                  //build prm string
                  var sb = new StringBuilder();
                  sb.AppendUrlEncoded("username", userName, true);
                  sb.AppendUrlEncoded("password", password, true);
                  sb.AppendUrlEncoded("request_id", req_id, true);
                  sb.AppendUrlEncoded("displayLangSelection", "false", true);
                  sb.AppendUrlEncoded("Languages", "", false);

                  req = (HttpWebRequest)WebRequest.Create(new Uri(res.ResponseUri.Scheme+ "://"+host + "/oam/server/auth_cred_submit"));
                  req.Accept = "text/html,application/xhtml+xml,application/xml";
                  req.Headers.Add(HttpRequestHeader.Cookie, cstr);
                  req.KeepAlive = true;
                  req.ContentLength = sb.Length;
                  req.Host = host;
                  req.Referer = res.ResponseUri.OriginalString;
                  req.ContentType = "application/x-www-form-urlencoded";
                  req.Headers.Add("Accept-Encoding", "gzip, deflate, br");
                  req.Headers.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
                  req.Headers.Add("Cache-Control", "max-age=0");
                  req.Headers.Add("DNT", "1");
                  req.Headers.Add("Origin", res.ResponseUri.Scheme + "://" + host);
                  req.Headers.Add("Upgrade-Insecure-Requests", "1");
                  req.Method = "POST";
                  using (var stream = req.GetRequestStream())
                  {
                      var bytes = Encoding.ASCII.GetBytes(sb.ToString());
                      stream.Write(bytes, 0, bytes.Length);
                  }
                  var res3 = req.GetResponse();
                  var urlHeader = res3.Headers["Location"];
                  if (string.IsNullOrEmpty(urlHeader)) return false;

                  cStrings.Clear();
                  res3.Headers["Set-Cookie"].Split(',').Where(a => a.StartsWith("OAM")).ForEach(a =>
                  {
                      cStrings.Add(a.Split(';')[0]);
                  });
                  cstr = "ObSSOCookie=loggedoutcontinue; " + reqCountCookie + "; " + string.Join(";", cStrings.Where(a => a.StartsWith("OAM_REQ_") || a.StartsWith("OAM_ID")));


                  //https://sitepfr.fed.pfr.local:7893/obrar.cgi?cookie=...
                  req = (HttpWebRequest)WebRequest.Create(urlHeader);
                  req.UserAgent = "PTK DOKIP";
                  req.KeepAlive = true;
                  req.Headers.Add("Accept-Encoding", "gzip, deflate, br");
                  req.Headers.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
                  req.Headers.Add("Cache-Control", "max-age=0");
                  req.Headers.Add(HttpRequestHeader.Cookie, cstr);
                  req.Headers.Add("DNT", "1");
                  req.Headers.Add("Host", host);
                  req.Headers.Add("Referer", res.ResponseUri.ToString());
                  req.Headers.Add("Upgrade-Insecure-Requests", "1");
                  req.Method = "GET";
                  var res4 = req.GetResponse();

                  req.Headers.Add("", "");

              }

              return true;
              */
            #endregion

            var wnd = new EcasaLoginView(url);
            var result = wnd.ShowDialog();
            userName = wnd.UserName;
            session = wnd.SessionId;
            return result == true;  
        }
    }
}
