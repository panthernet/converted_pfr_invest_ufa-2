﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using DevExpress.Xpf.Core;
using DevExpress.XtraPrinting.Native;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.Common.MessageCompressor;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Ribbon;
using PFR_INVEST.Tools;
using PFR_INVEST.Views;
using PFR_INVEST.Views.Dialogs;

namespace PFR_INVEST
{
    public partial class MainWindow
    {
        public void NPFConstructor()
        {
            RibbonCommands.ShowAggregSPN.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowAggregSPN.Executed +=ShowAggregSPNOnExecuted;
            RibbonCommands.OpsFromNpfReport.CanExecute += CanExecuteIsTrue;
            RibbonCommands.OpsFromNpfReport.Executed += OpsFromNpfReportOnExecuted;

        }

        private void OpsFromNpfReportOnExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var file =  ViewModelBase.DialogHelper.ShowReportOpsFromNpfPrompt();
            SaveReport(file);
        }

        private void ShowAggregSPNOnExecuted(object sender, ExecutedRoutedEventArgs executedRoutedEventArgs)
        {
            try
            {
                var file = ViewModelBase.DialogHelper.ShowAggregateSPNReportDialog();
                if(file != null)
                    SaveReport(file);
            }
            finally
            {
                Loading.CloseWindow();
            }
        }

        #region Передача СПН

        #region Передача СПН - Создать заявку
        private void CreateRequest_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var regListUC = App.DashboardManager.GetActiveView() as RegistersListView;
            long regID = 0;

            if (regListUC != null)
            {
                if (regListUC.registersListGrid.GetFocusedRowCellValue("RegisterID") != null)
                {
                    if (regListUC.IsOnlyNotTransferred())
                    {
                        ViewModelBase.DialogHelper.ShowError("У выбранного реестра нет финреестров для создания заявки!");
                        return;
                    }
                    if (regListUC.CanCreateRequest())
                        regID = (long)regListUC.registersListGrid.GetFocusedRowCellValue("RegisterID");
                    else
                    {
                        ViewModelBase.DialogHelper.ShowError("У одного или нескольких финреестров не заполнена сумма!");
                        return;
                    }
                }
            }
            else
            {
                var rVM = App.DashboardManager.GetActiveViewModel() as RegisterViewModel;

                if (rVM?.ID > 0)
                {
                    var list = rVM.FinregWithNPFList;
                    if (list != null && list.Count > 0)
                    {
                        if (list.All(a => RegisterIdentifier.FinregisterStatuses.IsNotTransferred(a.Finregister.Status)))
                        {
                            ViewModelBase.DialogHelper.ShowError("У выбранного реестра нет финреестров для создания заявки!");
                            return;
                        }

                        if (list.Any(item => item?.Count == null || item.Count.Value == 0))
                        {
                            ViewModelBase.DialogHelper.ShowError("У одного или нескольких финреестров не заполнена сумма!");
                            return;
                        }
                        regID = rVM.ID;
                    }
                }
            }

            if (regID > 0)
                App.DashboardManager.OpenNewTab(typeof(TransferRequestView),
                                                ViewModelState.Create, regID, "Создать заявку на перечисление", "Заявка на перечисление");
        }

        private void CreateRequest_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var regListUC = App.DashboardManager.GetActiveView() as RegistersListView;

            if (regListUC != null)
            {
                if (regListUC.IsRegisterSelected() &&
                    regListUC.registersListGrid.GetFocusedRowCellValue("RegisterKind") != null &&
                    RegisterIdentifier.IsToNPF(
                        regListUC.registersListGrid.GetFocusedRowCellValue("RegisterKind").ToString().ToLower()))
                {
                    e.CanExecute = true;
                    return;
                }
            }
            else
            {
                var rVM = App.DashboardManager.GetActiveViewModel() as RegisterViewModel;

                if (rVM != null &&
                    rVM.ID > 0 &&
                    RegisterIdentifier.IsToNPF(rVM.Kind) &&
                    rVM.FinregWithNPFList.Count > 0)
                {
                    e.CanExecute = true;
                    return;
                }
            }

            e.CanExecute = false;
        }
        #endregion

        #region Common
        /// <summary>
        /// Общий код для кнопок работы с финреестром
        /// </summary>		
        private FinRegisterViewModel ExtractFinRegisterViewModel()
        {
            var model = App.DashboardManager.GetActiveViewModel() as FinRegisterViewModel;
            if (model == null && App.DashboardManager.GetActiveView() is RegistersListView)
            {
                var rlV = App.DashboardManager.GetActiveView() as RegistersListView;
                model = new FinRegisterViewModel(rlV.GetCurrFinRegID(), ViewModelState.Edit);
            }
            else if (model == null && App.DashboardManager.GetActiveView() is RegistersArchiveListView)
            {
                var rlV = App.DashboardManager.GetActiveView() as RegistersArchiveListView;
                model = new FinRegisterViewModel(rlV.GetCurrFinRegID(), ViewModelState.Edit);
            }
            else if (model == null && App.DashboardManager.GetActiveView() is RegisterView)
            {
                var view = App.DashboardManager.GetActiveView() as RegisterView;
                if (view?.SelectedFinregister != null)
                    model = new FinRegisterViewModel(view.SelectedFinregister.ID, ViewModelState.Edit);
            }
            return model;
        }

        private List<RegistersListItem> ExtractRegisterData()
        {
            var view = App.DashboardManager.GetActiveView();
            var rlv = view as RegistersListView;
            var reg = rlv?.SelectedRegister;
            return reg == null ? null : rlv.Model.List.Where(a => a.RegisterID == reg.RegisterID).ToList();
        }

        /// <summary>
        /// Общий код для кнопок работы с финреестром
        /// </summary>	
        private RegistersListItem ExtractFinregister()
        {
            RegistersListItem item = null;//ArchiveRegistersListView
            if (App.DashboardManager.GetActiveView() is RegistersListView)
                item = ((RegistersListView) App.DashboardManager.GetActiveView()).SelectedFinRegister;
            else if (App.DashboardManager.GetActiveView() is RegistersArchiveListView)
                item = ((RegistersArchiveListView) App.DashboardManager.GetActiveView()).SelectedFinRegister;
            else if (App.DashboardManager.GetActiveViewModel() is FinRegisterViewModel)
            {
                var model = App.DashboardManager.GetActiveViewModel() as FinRegisterViewModel;
                item = new RegistersListItem()
                {
                    RegisterKind = model?.RegisterKind,
                    Status = model?.Status,
                    Count = model.Count ?? 0,
                    DraftSum = model.DraftSum,
                    TrancheDate = model.TrancheDate ?? DateTime.MinValue,
                    IsActiveNPF = model.IsActiveNPFStatusBool,
                    ID = model.ID,
                };
            }
            else if (App.DashboardManager.GetActiveView() is RegisterView)
            {
                var rV = App.DashboardManager.GetActiveView() as RegisterView;
                var rVM = rV.DataContext as RegisterViewModel;
                var finreg = rV.SelectedFinregister;
                if (finreg == null) return null;

                //Register reg = finreg.GetRegister() ?? new Register();
                var drafts = finreg.GetDraftsList();
                var asgList = drafts == null ? new List<AsgFinTr>() : new List<AsgFinTr>(drafts.Cast<AsgFinTr>());
                item = new RegistersListItem()
                {
                    RegisterKind = rVM == null ? string.Empty : rVM.Kind,
                    Status = finreg.Status,
                    Count = finreg.Count ?? 0,
                    DraftSum = asgList.Sum(a => a.DraftAmount ?? 0),
                    TrancheDate = rVM?.TrancheDate,
                    IsActiveNPF = StatusIdentifier.IsNPFStatusActivityCarries(rV.SelectedFinregisterWithCorrAndNPF.NPFStatus),
                    ID = rVM.ID,
                };
            }

            return item;
        }
        #endregion

        #region Правка - Откат статуса

        void RollbackFinRegStatus_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
            var rAlV = App.DashboardManager.GetActiveView() as RegistersArchiveListView;
            var rlV = App.DashboardManager.GetActiveView() as RegistersListView;

            if (rAlV != null || rlV != null)
            {
                RegistersListItem rLI;
                bool ifrs;
                if (rAlV != null)
                {
                    rLI = rAlV.registersListGrid.GetFocusedRow() as RegistersListItem;
                    ifrs = rAlV.IsFinRegisterSelected();
                }
                else
                {
                    rLI = rlV.registersListGrid.GetFocusedRow() as RegistersListItem;
                    ifrs = rlV.IsFinRegisterSelected();
                }

                if (rLI != null && ifrs && rLI.Status != null && rLI.Status != RegisterIdentifier.FinregisterStatuses.NotTransferred && (!RegisterIdentifier.IsToNPF(rLI.RegisterKind) || rLI.TrancheDate != null))
                {
                    if (rLI.DraftSum != null || rLI.Status != RegisterIdentifier.FinregisterStatuses.Created || !RegisterIdentifier.IsToNPF(rLI.RegisterKind))
                    {
                        var isPureViewer = AP.Provider.CurrentUserSecurity.GetRoles()
                            .Where(a => a != DOKIP_ROLE_TYPE.None && a != DOKIP_ROLE_TYPE.User).All(a => a.ToString().ToLower().EndsWith("viewer"));
                        if (RegisterIdentifier.IsToNPF(rLI.RegisterKind) || rLI.Status != RegisterIdentifier.FinregisterStatuses.Created && !isPureViewer)
                        {
                            e.CanExecute = true;
                            return;
                        }
                    }
                }
            }
            else
            {
                var rv = App.DashboardManager.GetActiveView() as RegisterView;
                if (rv != null)
                {
                    var frNPFLi = rv.grid.GetFocusedRow() as FinregisterWithCorrAndNPFListItem;
                    var rvm = rv.DataContext as RegisterViewModel;
                    if (frNPFLi?.Finregister?.Status != null && frNPFLi.Finregister.Status != RegisterIdentifier.FinregisterStatuses.NotTransferred)
                    {
                        //пропускаем откат заявки для Возврата
                        if (RegisterIdentifier.IsToNPF(rvm.Kind) || rvm.TrancheDate != null || frNPFLi.Finregister.Status != RegisterIdentifier.FinregisterStatuses.Created)
                        {
                            //пропускаем откат заявки для Прихода
                            if (frNPFLi.Finregister.GetDraftsListCount() != 0 || frNPFLi.Finregister.Status != RegisterIdentifier.FinregisterStatuses.Created || !RegisterIdentifier.IsToNPF(rvm.Kind))
                            {
                                e.CanExecute = true;
                                return;
                            }
                        }
                    }
                }
                else
                {
                    var frVM = App.DashboardManager.GetActiveViewModel() as FinRegisterViewModel;
                    if (frVM?.FinregisterID > 0)
                    {
                        if (frVM.Status != null && frVM.Status != RegisterIdentifier.FinregisterStatuses.NotTransferred)
                        {
                            //Пропускаем откат даты форм. заявки для Возврата
                            if (RegisterIdentifier.IsToNPF(frVM.RegisterKind) || frVM.TrancheDate != null || frVM.Status != RegisterIdentifier.FinregisterStatuses.Created)
                            {
                                //Пропускаем откат даты форм. заявки для Прихода
                                if (frVM.DraftsList.Count != 0 || frVM.Status != RegisterIdentifier.FinregisterStatuses.Created || !RegisterIdentifier.IsToNPF(frVM.RegisterKind))
                                {
                                    e.CanExecute = true;
                                    return;
                                }
                            }
                        }
                    }
                }
            }
            e.CanExecute = false;
        }

        void RollbackFinRegStatus_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (DXMessageBox.Show("Произвести откат статуса документа? Все данные текущего статуса будут удалены!", "Внимание!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                return;
            var id = GetSelectedFinregisterId();
            if (!id.HasValue) return;

            var fr = DataContainerFacade.GetByID<Finregister>(id.Value);
            if (fr == null)
                throw new Exception("Непредвиденная ошибка при попытке отката статуса финреестра!"); 
            var register = fr.GetRegister();

            if (register != null && register.IsCustomArchive == 1)
            {
                ViewModelBase.DialogHelper.ShowError("Невозможно откатить статус финреестра, принадлежащего реестру, принудительно помещенному в архив!");
                return;
            }

            var oldstatus = fr.Status;
            var ppList = fr.GetDraftsList().Cast<AsgFinTr>().ToList();

            if (RegisterIdentifier.IsToNPF(register?.Kind))
            {
                if (RegisterIdentifier.FinregisterStatuses.IsCreated(fr.Status))
                {
                    //начальный статус - не должны были сюда попасть
                    if (register?.TrancheDate == null) return;
                    //если есть пп - сбрасываем их связи (возврат в Работу с НПФ)
                    if (ppList.Count > 0)
                    {
                        ppList.ForEach(a => a.ClearLink());
                        DataContainerFacade.BatchSave<AsgFinTr, long>(ppList);
                    }
                    else return;
                }
                //если статус Оформлен -> сбрасываем на Создан
                else if (RegisterIdentifier.FinregisterStatuses.IsIssued(fr.Status))
                {
                    fr.Status = RegisterIdentifier.FinregisterStatuses.Created;
                    DataContainerFacade.Save(fr);
                }
                    //передан -> оформлен
                else if (RegisterIdentifier.FinregisterStatuses.IsTransferred(fr.Status))
                {
                    fr.Status = RegisterIdentifier.FinregisterStatuses.Issued;
                    fr.FinNum = null;
                    fr.FinMoveType = null;
                    fr.FinDate = null;
                    fr.IsTransfered = 0;
                    DataContainerFacade.Save(fr);
                } 
            }
            else
            {
                //начальный статус - не должны были сюда попасть
                if (RegisterIdentifier.FinregisterStatuses.IsCreated(fr.Status)) return;

                //утчонение -> передан
                if (RegisterIdentifier.FinregisterStatuses.IsRrefine(fr.Status))
                {
                    fr.Status = RegisterIdentifier.FinregisterStatuses.Transferred;
                    fr.IsTransfered = 1;
                    DataContainerFacade.Save(fr);
                }
                    //финреестр оформлен
                else if (RegisterIdentifier.FinregisterStatuses.IsIssued(fr.Status))
                {
                    //возврат в самое начало
                    if (ppList.Count == 0)
                    {
                        fr.Status = RegisterIdentifier.FinregisterStatuses.Created;
                        DataContainerFacade.Save(fr);
                    }
                    else
                    {  
                        //очищаем п/п (возврат в Работу с НПФ)
                        ppList.ForEach(a=> a.ClearLink());
                        DataContainerFacade.BatchSave<AsgFinTr, long>(ppList);
                    }
                }
                    //передан -> оформлен
                else if (RegisterIdentifier.FinregisterStatuses.IsTransferred(fr.Status))
                {
                    fr.Status = RegisterIdentifier.FinregisterStatuses.Issued;
                    fr.FinNum = null;
                    fr.FinMoveType = null;
                    fr.FinDate = null;
                    fr.IsTransfered = 0;
                    DataContainerFacade.Save(fr);
                }
            }
            var vmNewStatus = fr.Status;
            //App.DashboardManager.RefreshListViewModels(viewModelsToRefresh);
            ViewModelBase.ViewModelManager.UpdateDataInAllModels(new List<KeyValuePair<Type, long>> { new KeyValuePair<Type, long>(typeof(Finregister), fr.ID) });
            var rAlV = App.DashboardManager.GetActiveView() as RegistersArchiveListView;
            var rlV = App.DashboardManager.GetActiveView() as RegistersListView;
            //если открыта модель карточки, закрываем
            if(rAlV == null && rlV == null)
                DashboardManager.Instance.DockManager.DockController.Close(DashboardManager.Instance.GetActivePanel());
            JournalLogger.LogEvent(oldstatus == vmNewStatus ? "Сброс даты формирования заявки" : string.Format("{1} <- {0}", oldstatus, vmNewStatus), JournalEventType.ROLLBACK, id.Value, "FinregisterViewModel", "", typeof(Finregister), "Финреестр");
        }

        #endregion

        void InputFinregisterProps_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var rData = ExtractRegisterData();
            //обработка выбора реестра
            var data = rData?.Where(a =>
            {
                if (RegisterIdentifier.IsToNPF(a.RegisterKind))
                {
                    if (RegisterIdentifier.FinregisterStatuses.IsCreated(a.Status)
                        && a.DraftSum == a.Count && a.TrancheDate != null)
                        return true;
                }
                else if (RegisterIdentifier.FinregisterStatuses.IsCreated(a.Status))
                    return true;
                return false;
            }).ToList();
            if (data?.Count > 0)
            {
                var fromNpf = RegisterIdentifier.IsFromNPF(data.First().RegisterKind);
                if (!ViewModelBase.DialogHelper.ShowConfirmation("Создать реквизиты для группы финреестров?", "Внимание"))
                    return;
       
                WCFClient.Client.SetFinregisterStateIssued(data.Select(a => a.ID).ToList(), fromNpf);
                var list = new List<KeyValuePair<Type, long>>();
                data.Select(a=> a.ID).ForEach(a=> list.Add(new KeyValuePair<Type, long>(typeof(Finregister), a)));
                ViewModelBase.ViewModelManager.UpdateDataInAllModels(list);
                return;
            }

            var model = ExtractFinRegisterViewModel();
            if (model == null) return;
            model.State = ViewModelState.Edit;
            try
            {
                model.IsInputPropsCountBlocked = true;
                var dlg = new FinregisterPropsInputDlg {DataContext = model};
                ThemesTool.SetCurrentTheme(dlg);
                if (!dlg.ShowDialog() == true)
                    return;
                model.SaveCard.Execute(null);
                if (model.IsBeforeSaveCheckFailed)
                    return;
                model.RefreshProps();
                model.RefreshStatus();
                var frVM = App.DashboardManager.FindViewModel(typeof (FinRegisterViewModel)) as FinRegisterViewModel;
                if (frVM == null)
                    return;
                frVM.Count = model.Count;
                frVM.RegNum = model.RegNum;
                frVM.Date = model.Date;
                frVM.Status = model.Status;
            }
            finally
            {
                model.IsInputPropsCountBlocked = false;
            }
        }

        void InputFinregisterProps_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var vm = App.DashboardManager.FindViewModel<FinRegisterViewModel>();
            if (vm != null && vm.State == ViewModelState.Create)
            {
                e.CanExecute = false;
                return;
            }
            e.CanExecute = false;
            var registers = ExtractRegisterData();
            if (registers?.FirstOrDefault(a =>
            {
                if (RegisterIdentifier.IsToNPF(a.RegisterKind))
                {
                    if (RegisterIdentifier.FinregisterStatuses.IsCreated(a.Status)
                        && a.DraftSum == a.Count && a.TrancheDate != null)
                        return true;
                }
                else if (RegisterIdentifier.FinregisterStatuses.IsCreated(a.Status))
                    return true;
                return false;
            }) != null)
                e.CanExecute = true;
            //TODO оптимизировать генерацию реестра, точечная выборка?
            var item = ExtractFinregister();
            if (item == null)
                return;
            if (RegisterIdentifier.IsToNPF(item.RegisterKind))
            {
                if (RegisterIdentifier.FinregisterStatuses.IsCreated(item.Status)
                    && item.DraftSum == item.Count && item.TrancheDate != null)
                    e.CanExecute = true;
            }
            else if (RegisterIdentifier.FinregisterStatuses.IsCreated(item.Status))
                e.CanExecute = true;
        }

        void GiveFinRegister_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var rData = ExtractRegisterData();
            if (rData != null)
            {
                var isNpfToPfr = RegisterIdentifier.IsFromNPF(rData.First().RegisterKind);

                var frIdList = rData.Where(a => RegisterIdentifier.FinregisterStatuses.IsIssued(a.Status) && a.DraftSum == a.Count).Select(a => a.ID).ToList();
                var model = new FinRegStateViewModel();
                if (!isNpfToPfr)
                {
                    var dlg = new FinregisterGiveDlg(true) {DataContext = model, SizeToContent = SizeToContent.WidthAndHeight};
                    ThemesTool.SetCurrentTheme(dlg);
                    if (dlg.ShowDialog() != true) return;
                    model = dlg.DataContext as FinRegStateViewModel;
                }
                else
                {
                    if (!ViewModelBase.DialogHelper.ShowConfirmation("Перевести группу финреестров в статус \"Финреестр передан\"?"))
                        return;
                }
                WCFClient.Client.SetFinregisterStateTransfered(model.FinDate, model.FinMoveType, frIdList, isNpfToPfr);
                model.RefreshConnectedCards();
                var list = new List<KeyValuePair<Type, long>>();
                frIdList.ForEach(a => list.Add(new KeyValuePair<Type, long>(typeof(Finregister), a)));
                ViewModelBase.ViewModelManager.UpdateDataInAllModels(list);
            }
            else
            {
                var model = ExtractFinRegisterViewModel();
                if (model == null)
                    return;
                model.State = ViewModelState.Edit;
                var dlg = new FinregisterGiveDlg {DataContext = model, SizeToContent = SizeToContent.WidthAndHeight};
                ThemesTool.SetCurrentTheme(dlg);
                if (dlg.ShowDialog() != true)
                    return;
                model.SaveCard.Execute(null);
                model.RefreshGiveFields();
                model.RefreshStatus();
                model.PostOpen("Финреестр");
                JournalLogger.LogArchiveEvent(model);
            }
        }

        void GiveFinRegister_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
            var registers = ExtractRegisterData();
            if (registers?.FirstOrDefault(a=> RegisterIdentifier.FinregisterStatuses.IsIssued(a.Status) && a.DraftSum == a.Count) != null)
                e.CanExecute = true;
            else
            {
                var item = ExtractFinregister();
                if (item == null) return;

                //Теперь суммы всегда должны быть равны
                if (RegisterIdentifier.FinregisterStatuses.IsIssued(item.Status) && item.DraftSum == item.Count)
                    e.CanExecute = true;
            }
        }

        void RefineFinregister_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
            var item = ExtractFinregister();
            if (item != null && RegisterIdentifier.IsFromNPF(item.RegisterKind) 
                && RegisterIdentifier.FinregisterStatuses.IsTransferred(item.Status))
                e.CanExecute = true;
        }

        void RefineFinregister_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var model = ExtractFinRegisterViewModel();
            if (model == null) return;
            if (model.IsForcedArchiveRegister)
            {
                ViewModelBase.DialogHelper.ShowError("Невозможно уточнить финреестр, принадлежащий реестру, принудительно помещенному в архив!");
                return;
            }
            model.State = ViewModelState.Edit;
            model.Status = RegisterIdentifier.FinregisterStatuses.Refine;//"Финреестр на уточнении";
            model.SetTransferedFlag(0);
            model.SaveCard.Execute(null);
            model.RefreshStatus();
        }

        void FixFinregister_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
            var item = ExtractFinregister();
            if (item != null && RegisterIdentifier.IsFromNPF(item.RegisterKind)
                && RegisterIdentifier.FinregisterStatuses.IsRrefine(item.Status))
                e.CanExecute = true;
        }

        void FixFinregister_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var model = ExtractFinRegisterViewModel();
            if (model == null) return;

            model.State = ViewModelState.Edit;
            model.Status = RegisterIdentifier.FinregisterStatuses.Transferred;// "Финреестр передан";
            model.SetTransferedFlag(1);
            model.SaveCard.Execute(null);
            model.RefreshStatus();
        }

        void NotTransferredFinRegister_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
            var item = ExtractFinregister();
            if (item == null) return;
            //http://jira.dob.datateh.ru/browse/DOKIPIV-484?focusedCommentId=238860&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-238860
            //запрещаем перевод с 0 суммой
            if (!RegisterIdentifier.IsToNPF(item.RegisterKind) || item.Count == 0) return;
            if ((RegisterIdentifier.FinregisterStatuses.IsCreated(item.Status) || RegisterIdentifier.FinregisterStatuses.IsIssued(item.Status))
                && item.ID != 0)
                e.CanExecute = true;
        }

        void NotTransferredFinRegister_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (!ViewModelBase.DialogHelper.ShowConfirmation("Выбранный финреестр будет переведен в статус \"Финреестр не передан\".\nПродолжить?")) return;
            var model = ExtractFinRegisterViewModel();
            if (model == null)
                return;
            if (model.IsChildFinregister)
            {
                ViewModelBase.DialogHelper.ShowError("Текущий финреестр нельзя перевести в статус 'Финреестр не передан', \nт.к. он является дочерним по отношению к финреестру с таким же статусом.", "Невозможно выполнить операцию");
                return;
            }
            model.State = ViewModelState.Edit;
            model.Status = RegisterIdentifier.FinregisterStatuses.NotTransferred;// "Финреестр не передан";
            model.SaveCard.Execute(null);
            model.RefreshStatus();
        }

        #endregion Finregister

        #region Отказные заявления
        private void OpenRejAppFileExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var id = e.Parameter as long? ?? 0;
            if (id > 0)
            {
                var doc = DataContainerFacade.GetByID<RepositoryImpExpFile, long>(id);
                if (doc != null)
                {
                    string file;
                    do
                    {
                        file = Path.Combine(Path.GetTempPath(), $"RejAppRepository_{id}_{Guid.NewGuid()}.csv");
                    } while (File.Exists(file));

                    if (doc.Repository != null && doc.Repository.Length > 0)
                        File.WriteAllBytes(file, GZipCompressor.Decompress(doc.Repository));
                    else
                    {
                        DXMessageBox.Show("Неизвестная ошибка при открытии документа!", "Ошибка", MessageBoxButton.OK,
                                          MessageBoxImage.Error);
                        return;
                    }
                    try
                    {
                        Process.Start(file);
                    }
                    catch
                    {
                        DXMessageBox.Show("Неизвестная ошибка при открытии документа!", "Ошибка", MessageBoxButton.OK,
                                          MessageBoxImage.Error);
                    }
                }
                else
                    DXMessageBox.Show($"Документ отсутствует в базе. (Id:{e.Parameter})");
            }
            e.Handled = true;
        }
        #endregion
    }
}
