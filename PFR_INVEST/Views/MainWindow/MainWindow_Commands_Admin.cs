﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using DevExpress.Xpf.Core;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Helpers;
using PFR_INVEST.Views;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.BusinessLogic.ViewModelsCard;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.Common.MessageCompressor;

namespace PFR_INVEST
{
	public partial class MainWindow
	{
		//UNCOMMENT true
		public static readonly bool Enable1C = true;

		#region Интеграция с ПКИП

		private void PKIP_Settings_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			ViewModelBase.DialogHelper.ShowOnesPkipPathSettings(1);
		}

		private void PKIP_DataExchange_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			//Настройки импорта/экспорта
			var importSettings = DataContainerFacade.GetByID<SettingsPathFile>((long)SettingsPathFile.IdDefinition.PkipImport);
			var pathImport = importSettings.Path;
			var serverImport = importSettings.ServClientValue == SettingsPathFile.FileLocation.Server;
			if (!serverImport && !Directory.Exists(pathImport))
			{
				ViewModelBase.DialogHelper.ShowError($"Не найдена папка для импорта '{pathImport}'");
				return;
			}

			var exportSettings = DataContainerFacade.GetByID<SettingsPathFile>((long)SettingsPathFile.IdDefinition.PkipExport);
			var pathExport = exportSettings.Path;
			var serverExport = exportSettings.ServClientValue == SettingsPathFile.FileLocation.Server;
			if (!serverExport && !Directory.Exists(pathImport))
			{
				ViewModelBase.DialogHelper.ShowError($"Не найдена папка для экспорта '{pathExport}'");
				return;
			}

			Task.Factory.StartNew(() =>
			{
				//Импорт файлов
				var importTotal = 0;
				var importError = 0;

				var files = new string[0];

				if (serverImport)
				{
					var res = DataContainerFacade.GetClient().GetKIPFilesForImport();
					if (res.IsSuccess)
						files = res.Data;
					else
					{
						Dispatcher.Invoke(new Action(() => { ViewModelBase.DialogHelper.ShowError(res.ErrorMessage); }));
						return;
					}
				}
				else
				{
					files = Directory.GetFiles(pathImport, "*.xml");
				}


				importTotal = files.Length;
				for (var i = 0; i < importTotal; i++)
				{
					var file = files[i];
					App.StatusbarManager.SetProgress($"Импорт КИП {i + 1}/{importTotal}", i, importTotal);

				    var kipSettings = DataContainerFacade.GetClient().GetKIPSettings();
                    var res = serverImport
						? DataContainerFacade.GetClient().LoadKIPFileFromServer(file)
						: DataContainerFacade.GetClient().LoadKIPFile(kipSettings, GZipCompressor.Compress(File.ReadAllBytes(Path.Combine(pathImport, file))), true);
					if (!res.IsSuccess) importError++;
				}

				//Экспорт файлов
				var exportTotal = 0;
				var exportReal = 0;
				App.StatusbarManager.SetProgress("Экспорт КИП", 0);
				var exports = DataContainerFacade.GetClient().GetKIPExportReadyList();
				exportTotal = exports.Count();

				for (var i = 0; i < exportTotal; i++)
				{
					var num = exports[i];
					App.StatusbarManager.SetProgress($"Экспорт КИП {i + 1}/{exportTotal}", i, exportTotal);

					if (serverExport)
					{
						var doc = DataContainerFacade.GetClient().GetKIPExportFile(num, true);
						if (doc.IsSuccesss)						
							exportReal++;						
						else 						
							ViewModelBase.DialogHelper.ShowError(doc.Error);
					}
					else
					{
						var doc = DataContainerFacade.GetClient().GetKIPExportFile(num);
					    if (!doc.IsSuccesss) continue;
					    SaveXML(doc, pathExport, false);
					    DataContainerFacade.GetClient().SetKIPRegisterExportStatus(num, KipRegister.ExportStatuses.Exported);
					    exportReal++;
					}
				}

				App.StatusbarManager.SetProgress("Обмен завершён", 100);
				//Финальное сообщение
				Dispatcher.Invoke(new Action(() =>
				{
				    var messages = new List<string> {"Обмен данными с ПТК КИП завершён."};
				    if (importTotal > 0)
						messages.Add($"Импортировано {importTotal - importError} из {importTotal} документов.");
					if (importError > 0)
					{
						messages.Add($"{importError} документов не импортировано из за ошибок.");
						messages.Add("Подробности в журнале ошибок обмена документами.");
					}
					if (exportTotal > 0)
						messages.Add($"Экспортировано {exportReal} документов.");
					var text = string.Join(Environment.NewLine, messages.ToArray());
					MessageBox.Show(text);
					//Логируем событие обмена данными
					JournalLogger.LogEvent("Обмен данными КИП", JournalEventType.IMPORT_DATA, null, string.Empty, text);
				}));
			});
		}

		private void PKIP_ViewErrorList_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			App.DashboardManager.OpenNewTab(typeof(KIPErrorLogListView), ViewModelState.Read, "Журнал ошибок импорта/экспорта КИП");
		}

		private void PKIP_ViewJournal_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			App.DashboardManager.OpenNewTab(typeof(KIPRegisterListView), ViewModelState.Read, "Журнал документов КИП");
		}

		#endregion

		#region Интеграция с 1С

		private void OneS_Settings_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			ViewModelBase.DialogHelper.ShowOnesPkipPathSettings(0);
		}

		private void OneS_OpSettings_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			ViewModelBase.DialogHelper.ShowOnesDefSettings();
		}

		private void OneS_DataExchange_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			if (!Enable1C)
			{
				ViewModelBase.DialogHelper.ShowError("Функция в данный момент недоступна");
				return;
			}
			OnesExecuteAllImport();
		}

		/// <summary>
		/// Запуск общего импорта 1C
		/// </summary>
		private void OnesExecuteAllImport()
		{
			var data = DataContainerFacade.GetByID<SettingsPathFile>((long)SettingsPathFile.IdDefinition.OnesImport);
			var isClient = data.ServClient == (int)SettingsPathFile.FileLocation.Client;
			if (string.IsNullOrEmpty(data.Path))
			{
				ViewModelBase.DialogHelper.ShowError("Не задан путь хранения файлов импорта 1С! Задайте путь в настройках и повторите попытку.");
				return;
			}

			//ИМПОРТ ДАННЫХ
			if (isClient && !Directory.Exists(data.Path))
			{
				ViewModelBase.DialogHelper.ShowError("Папка для импорта на клиенте, указанная в настройках, не существует!");
				return;
			}

			if (data.ServClient == (int)SettingsPathFile.FileLocation.Server)
			{
			    var result = WCFClient.Client.StartOnesImport(data.Path);
			    switch (result)
			    {
			        case WebServiceDataError.SessionInProgress:
			            ViewModelBase.DialogHelper.ShowExclamation("На сервере уже запущена сессия импорта 1С! Дождитесь окончания текущей сессии и повторите попытку.", "Внимание");
			            break;
			        case WebServiceDataError.DirectoryNotFound:
			            ViewModelBase.DialogHelper.ShowExclamation("На сервере не найдена папка импорта файлов 1С! Проверьте правильность настройки путей импорта/экспорта.", "Внимание");
			            break;
			    }
			}
			else
			{
                App.StatusbarManager.SetProgress("Импорт 1С - Загрузка...", 0D);
                var model = new ImportIntegrationFilesDlgViewModel(ImportIntegrationFilesDlgViewModel.IntegrationImportType.OneS, data.Path);
                model.UploadFinished += OnesImportClientModel_UploadFinished;
                model.UploadFiles();
            }
		}

        void OnesImportClientModel_UploadFinished(object sender, EventArgs e)
        {
            Task.Factory.StartNew(() =>
            {
                int[] status;
                do
                {
                    status = WCFClient.Client.GetOnesImportProgress();
                    if (status == null) continue;
                    App.StatusbarManager.SetProgress($"Импорт 1С - Обработано {status[0]}/{(status[1] == 0 ? "?" : status[1].ToString())}", 0D);
                    TaskHelper.Delay(2000).Wait();
                } while (status != null);
                App.StatusbarManager.SetProgress("Импорт 1С завершен" + status, 100D);
            });
        }

		private void OneS_ViewImportJournal_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			if (!Enable1C)
			{
				ViewModelBase.DialogHelper.ShowError("Функция в данный момент недоступна");
				return;
			}
			App.DashboardManager.OpenNewTab(typeof(OnesImportJournalListView), ViewModelState.Read, "Журнал регистрации импорта");
		}

		private void OneS_ViewExportJournal_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			if (!Enable1C)
			{
				ViewModelBase.DialogHelper.ShowError("Функция в данный момент недоступна");
				return;
			}
            App.DashboardManager.OpenNewTab(typeof(OnesExportJournalListView), ViewModelState.Read, "Журнал экспорта документов");
		}

		private void OneS_ViewErrorList_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			if (!Enable1C)
			{
				ViewModelBase.DialogHelper.ShowError("Функция в данный момент недоступна");
				return;
			}
			App.DashboardManager.OpenNewTab(typeof(OnesImportErrorsListView), ViewModelState.Read, "Список ошибок при импорте");
		}

        private void OnesDataSI_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = AP.Provider.CurrentUserSecurity.CheckUserInAnyRole(DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.Administrator);
        }

        private void OnesDataSIOnExecuted(object sender, ExecutedRoutedEventArgs e)
		{
			if (!Enable1C)
			{
				ViewModelBase.DialogHelper.ShowError("Функция в данный момент недоступна");
				return;
			}
			var result = ViewModelBase.DialogHelper.ShowOnesSelectExportDataDlg(OnesSettingsDef.IntGroup.Si);
			if (result == null || result.Count == 0) return;
			OnesExecuteExportWrapper(OnesSettingsDef.IntGroup.Si, result);
		}
        
        private void OnesDataVR_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = AP.Provider.CurrentUserSecurity.CheckUserInAnyRole(DOKIP_ROLE_TYPE.OUFV_manager, DOKIP_ROLE_TYPE.Administrator);
        }

        void OnesDataVR_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			if (!Enable1C)
			{
				ViewModelBase.DialogHelper.ShowError("Функция в данный момент недоступна");
				return;
			}
			var result = ViewModelBase.DialogHelper.ShowOnesSelectExportDataDlg(OnesSettingsDef.IntGroup.Vr);
			if (result == null || result.Count == 0) return;
			OnesExecuteExportWrapper(OnesSettingsDef.IntGroup.Vr, result);
		}

        private void OnesDataDEPO_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = AP.Provider.CurrentUserSecurity.CheckUserInAnyRole(DOKIP_ROLE_TYPE.OARRS_manager, DOKIP_ROLE_TYPE.Administrator);
        }

        void OnesDataDEPO_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			if (!Enable1C)
			{
				ViewModelBase.DialogHelper.ShowError("Функция в данный момент недоступна");
				return;
			}
			var result = ViewModelBase.DialogHelper.ShowOnesSelectExportDataDlg(OnesSettingsDef.IntGroup.Depo);
			if (result == null || result.Count == 0) return;
			OnesExecuteExportWrapper(OnesSettingsDef.IntGroup.Depo, result);
		}

        void OnesDataNPF_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = AP.Provider.CurrentUserSecurity.CheckUserInAnyRole(DOKIP_ROLE_TYPE.OKIP_manager, DOKIP_ROLE_TYPE.Administrator);
        }

        void OnesDataNPF_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			if (!Enable1C)
			{
				ViewModelBase.DialogHelper.ShowError("Функция в данный момент недоступна");
				return;
			}
			var result = ViewModelBase.DialogHelper.ShowOnesSelectExportDataDlg(OnesSettingsDef.IntGroup.Npf);
			if (result == null || result.Count == 0) return;
			OnesExecuteExportWrapper(OnesSettingsDef.IntGroup.Npf, result);
		}

		public void OnesExecuteExportWrapper(OnesSettingsDef.IntGroup type, List<long> list)
		{
			if (!Enable1C)
			{
				ViewModelBase.DialogHelper.ShowError("Функция в данный момент недоступна");
				return;
			}
			var data = DataContainerFacade.GetByID<SettingsPathFile>((long)SettingsPathFile.IdDefinition.OnesExport);
			bool isClient = data.ServClient == (int)SettingsPathFile.FileLocation.Client;
			//ЭКСПОРТ ДАННЫХ
			if (isClient && !Directory.Exists(data.Path))
				try
				{
					Directory.CreateDirectory(data.Path);
				}
				catch
				{
					ViewModelBase.DialogHelper.ShowError("Папка для экспорта на клиенте, указанная в настройках, не существует и не удается создать её автоматически!");
					return;
				}
			var resultItems = WCFClient.Client.StartOnesExport(data.Path, isClient, type, list);

			var errors = resultItems.Where(a => a is WebServiceDataError).Cast<WebServiceDataError>().ToList();
			if (errors.Any())
			{
				errors.ForEach(a =>
				{
					switch (a)
					{
						case WebServiceDataError.DirectoryNotFound:
							ViewModelBase.DialogHelper.ShowError("Папка для экспорта на сервере, указанная в настройках, не существует и не удается создать её автоматически!");
							break;
						case WebServiceDataError.GeneralException:
							ViewModelBase.DialogHelper.ShowError("Во время обработки операции экспорта возникла непредвиденная ошибка!");
							break;
						case WebServiceDataError.SessionInProgress:
							ViewModelBase.DialogHelper.ShowError("На сервере уже запущена сессия экспорта данных для 1С! \nПопробуйте повторить операцию позднее.");
							break;
					}
				});
				return;
			}

			var name = "";
			switch (type)
			{
				case OnesSettingsDef.IntGroup.Depo:
					name = "Депозиты";
					break;
				case OnesSettingsDef.IntGroup.Npf:
					name = "НПФ";
					break;
				case OnesSettingsDef.IntGroup.Si:
					name = "СИ";
					break;
                case OnesSettingsDef.IntGroup.Vr:
                    name = "ВР";
                    break;
                case OnesSettingsDef.IntGroup.Opfr:
                    name = "Распоряжения Правления";
                    break;
            }

            Task.Factory.StartNew(() =>
			{
				OnesExportStatus status;
				do
				{
					status = WCFClient.Client.GetOnesExportStatus(type);
					App.StatusbarManager.SetProgress("Экспорт 1С - " + name, status.Count, status.MaxCount, 0D);
					TaskHelper.Delay(2000).Wait();
				}
				while (!status.Completed);
			})
			.ContinueWith(param =>
			{
				if (!isClient) return;

				try
				{
					var count = 0;
					//вычисляем порядковый номер в рамках дня
					//АНАЛОГИЧНАЯ ЛОГИКА В OnesGenerateExport
					Directory.EnumerateFiles(data.Path, "docip_*.xml").ToList().ForEach(a =>
					{
						var array = Path.GetFileNameWithoutExtension(a)?.Split('_');
						var src = array?[2];
					    if (DateTime.ParseExact(src, "yyMMdd", new DateTimeFormatInfo()).Date != DateTime.Now.Date) return;
					    var nCount = Convert.ToInt32(array?[4]);
					    count = nCount > count ? nCount : count;
					});

					WCFClient.Client.OnesGetGeneratedExportFiles().ForEach(f =>
					{
						try
						{
							if (f.Repository.Length == 0) return;
							var file = Path.Combine(data.Path, $"docip_RequestForTransfer_{DateTime.Now.ToString("yyMMdd_hhmmss")}_{++count}.xml");
							using (var writer = File.CreateText(file))
							{
								writer.Write(Encoding.Default.GetChars(GZipCompressor.Decompress(f.Repository)));
							}
						}
						catch (Exception ex)
						{
							App.log.WriteException(ex);
						}
					});
				}
				catch (Exception ex)
				{
					App.log.WriteException(ex);
				}
			});
			//результат
			ViewModelBase.DialogHelper.ShowAlert("Запущен экспорт данных для 1С. За прогрессом операции вы можете наблюдать в статусбаре приложения!");
		}

		private void OpenOnesImportXmlBodyExecuted(object sender, ExecutedRoutedEventArgs e)
		{
			var id = (e.Parameter as long?) ?? 0;
			OpenXMLFile(id, true);
			e.Handled = true;
		}

	    private void RemoveOnesExportEntryExecuted(object sender, ExecutedRoutedEventArgs e)
	    {
            var id = (e.Parameter as long?) ?? 0;
	        if (id == 0)
	        {
	            ViewModelBase.DialogHelper.ShowAlert("Для выбранной записи не найден идентификатор экспорта! Удаление невозможно.");
                return;
	        }
	        if (!ViewModelBase.DialogHelper.ShowQuestion("Выбранный файл экспорта и все связанные записи экспорта будут удалены! Продолжить?", "Внимание"))
	            return;
	        WCFClient.Client.RemoveOnesExportByExportId(id);
            RefreshViewModelList<OnesExportJournalListViewModel>();
	    }

		private void OpenOnesExportXmlBodyExecuted(object sender, ExecutedRoutedEventArgs e)
		{
			var id = (e.Parameter as long?) ?? 0;
			OpenXMLFile(id, false);
			e.Handled = true;
		}

        private void OpenCBReportExportXmlBodyExecuted(object sender, ExecutedRoutedEventArgs e)
	    {
            var id = (e.Parameter as long?) ?? 0;
            OpenXMLFile(id, false);
            e.Handled = true;
	    }

		private void OpenXMLFile(long id, bool utf8)
		{
		    if (id <= 0) return;
		    var doc = WCFClient.Client.GetOnesXmlStorageItem(id); //DataContainerFacade.GetByID<OnesXmlStorage, long>(id);
		    if (doc != null)
		    {
		        string file;
		        do
		        {
		            file = Path.Combine(Path.GetTempPath(), $"OnesXmlStorage_{id}_{Guid.NewGuid()}.xml");
		        } while (File.Exists(file));

		        if (doc.Repository != null && doc.Repository.Length > 0)
		        {
		            using (var writer = File.CreateText(file))
		            {
		                writer.Write(utf8 ? Encoding.UTF8.GetChars(GZipCompressor.Decompress(doc.Repository)) : Encoding.Default.GetChars(GZipCompressor.Decompress(doc.Repository)));
		            }
		        }
		        else
		        {
		            DXMessageBox.Show("Неизвестная ошибка при открытии документа!", "Ошибка", MessageBoxButton.OK,
		                MessageBoxImage.Error);
		            return;
		        }
		        try
		        {
		            Process.Start(file);
		        }
		        catch
		        {
		            DXMessageBox.Show("Неизвестная ошибка при открытии документа!", "Ошибка", MessageBoxButton.OK,
		                MessageBoxImage.Error);
		        }
		    }
		    else
		        DXMessageBox.Show($"Документ отсутствует в базе. (Id:{id})");
		}
		#endregion

        #region Отчеты в ЦБ

        void CBReportList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (App.DashboardManager.FindViewModelsList(typeof(CBReportManagerViewModel)).Cast<CBReportManagerViewModel>().Count(a => a.IsCentralized) > 0) return;
            App.DashboardManager.OpenNewTab(typeof (CBReportManagerView),
                    ViewModelState.Read, true, "Отчеты в Банк России");
        }
        void CBReportCreate_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ViewModelBase.DialogHelper.CreateReportForCB(false);
        }

        void CBReportExport_Executed(object sender, ExecutedRoutedEventArgs e)
        {
             var vm = App.DashboardManager.GetActiveViewModel() as CBReportManagerViewModel;
             if (vm?.SelectedReport == null) return;
             ViewModelBase.DialogHelper.CBReportExportToXml(vm.SelectedReport.ID);
        }

        void CBReportExport_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var vm = App.DashboardManager.GetActiveViewModel() as CBReportManagerViewModel;
            e.CanExecute = vm?.SelectedReport != null;
        }

	    void CBReportSettingsPaymentDetail_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(CBPaymentDetailListView), ViewModelState.Read, "Настройка назначений платежей");
        }

        void CBReportSettingsPaymentDetail_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        #endregion

        private void RecalcUKReqTransfers_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var c = ViewModelBase.DialogHelper.ShowSelectContract();
            if (c == null) return;
            if (ViewModelBase.DialogHelper.ShowConfirmation($"Произвести пересчет сумм перечислений по договору {c.ContractNumber}?") != true) return;
            using (Loading.StartNew("Пересчет сумм..."))
                WCFClient.Client.UpdateReqTrAndContractByActSigned(c.ID, false);
        }
    }
}
