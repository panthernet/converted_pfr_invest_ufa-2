﻿using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    using DevExpress.Xpf.Grid;
    using DataObjects.ListItems;

    /// <summary>
    /// Interaction logic for DepclaimMaxListView.xaml
    /// </summary>
    public partial class DepClaimListView
    {
        public DepClaimListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, Grid);
            tableView.FocusedRowHandleChanged += tableView_FocusedRowHandleChanged;
        }

        private void tableView_FocusedRowHandleChanged(object sender, FocusedRowHandleChangedEventArgs e)
        {
            var item = tableView.FocusedRowData.Row as DepClaimMaxListItem;
            SelectedDepClaimMaxHelper.Value = item;
        }
    }
}
