﻿using System.Windows.Controls;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for DueExcessAccurateView.xaml
    /// </summary>
    public partial class DueExcessAccurateView : UserControl
    {
        public DueExcessAccurateView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
            ModelInteractionHelper.SignUpForCloseRequest(this, true);
        }
    }
}
