﻿using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for ContragentsList.xaml
    /// </summary>
    public partial class BankConclusionList
    {
        public BankConclusionList()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, bankConclusionListGrid);
        }

        private void tableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (bankConclusionListGrid.View.GetRowElementByMouseEventArgs(e) == null)
                return;
            if (bankConclusionListGrid.View.FocusedRowHandle < 0)
                return;
            var value = bankConclusionListGrid.GetCellValue(bankConclusionListGrid.View.FocusedRowHandle, "ID");

            App.DashboardManager.OpenNewTab(typeof(BankConclusionView),
                ViewModelState.Read,
                (long)
                    value, "Статус кредитной организацииции");
        }
    }
}
