﻿using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using DevExpress.Xpf.Grid;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for InsuranceListView.xaml
    /// </summary>
    public partial class InsuranceArchiveListView
    {
        public InsuranceArchiveListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, grdInsuranceList);
        }

        protected void tvInsuranceList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (grdInsuranceList.View.GetRowElementByMouseEventArgs(e) == null)
                return;
            if (grdInsuranceList.View.FocusedRowHandle >= 0)
                OpenItem((long)grdInsuranceList.GetCellValue(grdInsuranceList.View.FocusedRowHandle, "ID"));
        }

        private void InsuranceArchiveListViewShowFilterPopup(object sender, FilterPopupEventArgs e)
        {
            if (e.Column.FieldName != "Status") return;

            var items = (List<object>)e.ComboBoxEdit.ItemsSource;
            var filterItems = items.OfType<CustomComboBoxItem>().Where(item => !(item.DisplayValue.Equals("(Пустое)") || item.DisplayValue.Equals("(Не пустое)"))).ToList();

            e.ComboBoxEdit.ItemsSource = filterItems;
        }

        protected virtual void OpenItem(long id)
        {
            App.DashboardManager.OpenNewTab(typeof(InsuranceView), ViewModelState.Edit, id, "Договор страхования");
        }
    }
}
