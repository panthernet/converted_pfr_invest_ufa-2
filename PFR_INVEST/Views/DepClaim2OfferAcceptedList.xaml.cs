﻿using System.Windows;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for DepClaim2OfferAcceptedList.xaml
    /// </summary>
    public partial class DepClaim2OfferAcceptedList
    {
        public DepClaim2OfferAcceptedList()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            App.DashboardManager.DockManager.ActiveDockItem.Closed = true;
        }
    }
}
