﻿using System;
using System.Windows;
using System.Windows.Controls;
using PFR_INVEST.BusinessLogic;
using DevExpress.Xpf.Docking;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class KBKView : UserControl
    {
        protected KBKViewModel Model => DataContext as KBKViewModel;

        public KBKView()
        {
            InitializeComponent();
            Loaded += KBKView_Loaded;
            Loaded += KBKView_UpdateCaption;
            Loaded += KBKView_UpdateSize;
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        private void KBKView_Loaded(object sender, EventArgs e)
        {
            if (Model != null)
            {
                Model.OnKBKCreated += KBKView_UpdateCaption;
                Model.OnKBKCreated += KBKView_UpdateSize;
                Model.OnKBKUpdated += KBKView_UpdateCaption;
            }
        }

        private void KBKView_UpdateCaption(object sender, EventArgs e)
        {
            var tabPanel = Parent as LayoutPanel;
            if (tabPanel != null)
            {
                if (Model.State == ViewModelState.Edit && Model?.KBK != null)
                {
                    tabPanel.Caption = Model.KBK.Name;
                }
            }
        }

        private void KBKView_UpdateSize(object sender, EventArgs e)
        {
            var tabPanel = Parent as LayoutPanel;
            if (tabPanel != null)
            {
                if (Model.State == ViewModelState.Edit)
                {
                    var size = new Size(550, 315);
                    var maxSize = new Size(550, 1000);
                    DashboardManager.ResizeView(size, maxSize, tabPanel);
                }
                else
                {
                    var size = new Size(550, 140);
                    DashboardManager.ResizeView(size, size, tabPanel);
                }
            }
        }
    }
}
