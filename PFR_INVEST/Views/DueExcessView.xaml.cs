﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for DueExcessView.xaml
    /// </summary>
    public partial class DueExcessView : UserControl
    {
        public DueExcessView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        public DueExcessViewModel Model => DataContext as DueExcessViewModel;

        public string GetSelectedDop()
        {
            if (Dops.View.FocusedRowHandle >= 0 && Dops.View.FocusedRowHandle > -1000)
                return Dops.GetCellValue(Dops.View.FocusedRowHandle, "ID").ToString();

            return string.Empty;
        }

        private void TableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (Dops.View.GetRowElementByMouseEventArgs(e) == null) return;
            long dopID = Convert.ToInt64(GetSelectedDop());
            if (dopID > 0)
                App.DashboardManager.OpenNewTab(typeof(DueExcessAccurateView), Model.State == ViewModelState.Read ? ViewModelState.Read : ViewModelState.Edit, dopID, Model, DueDocKindIdentifier.DueExcessAccurate);
        }
    }
}
