﻿using System.Windows;
using System.Windows.Controls;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for EditLegalEntityHeadDlg.xaml
    /// </summary>
    public partial class AgencyRatingView : UserControl
    {
        public AgencyRatingView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }
        
        private void EditRatings_Click(object sender, RoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(RatingsListView), ViewModelState.Read, "Рейтинги");
        }
    }
}
