﻿using System.Windows.Controls;
using System.Windows.Input;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;
using PFR_INVEST.BusinessLogic.ViewModelsList;

namespace PFR_INVEST.Views.Analyze
{
    /// <summary>
    /// Interaction logic for AnalyzePensionPlacementReportListView.xaml
    /// </summary>
    public partial class AnalyzePensionfundtonpfListView : UserControl
    {
        public AnalyzePensionfundtonpfListViewModel VM => (AnalyzePensionfundtonpfListViewModel)DataContext;
        private RibbonStateBL _ribbonState;//для корректного отображения полей ввода параметров, если позже был выбран другой раздел
        public DelegateCommand OpenEditorCommand { get; private set; }
        public AnalyzePensionfundtonpfListView()
        {
            InitializeComponent();
            _ribbonState = App.RibbonManager.GetCurrentPageRibbonState();
            OpenEditorCommand = new DelegateCommand(x => VM != null && VM.Current != null, x => OpenEditorAction());
        }

        void OpenEditorAction()
        {
            if (VM.Current == null)
                return;
            var state = !VM.IsArchiveMode ? ViewModelState.Edit : ViewModelState.Read;
            var vm = new AnalyzePensionfundtonpfEditViewModel(VM.Current, _ribbonState, state, VM.WithSubtract, false, VM.IsArchiveMode);
            var view = new AnalyzePensionfundtonpfEditView(vm);

            App.DashboardManager.OpenNewTab("Детализация отчета", view, state, vm);
        }

        private void grid_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (tableView.GetRowHandleByMouseEventArgs(e) == DataControlBase.InvalidRowHandle) return;
            OpenEditorCommand.Execute(null);
        }
    }
}
