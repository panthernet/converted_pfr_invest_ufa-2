﻿using System.Windows.Controls;
using System.Windows.Input;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;
using PFR_INVEST.BusinessLogic.ViewModelsList;

namespace PFR_INVEST.Views.Analyze
{
    /// <summary>
    /// Interaction logic for AnalyzePensionPlacementReportListView.xaml
    /// </summary>
    public partial class AnalyzeIncomingstatementsListView : UserControl
    {
        public AnalyzeIncomingstatementsListViewModel VM => (AnalyzeIncomingstatementsListViewModel)DataContext;
        private RibbonStateBL _ribbonState;
        public DelegateCommand OpenEditorCommand { get; private set; }
        public AnalyzeIncomingstatementsListView()
        {
            InitializeComponent();
            _ribbonState = App.RibbonManager.GetCurrentPageRibbonState();
            OpenEditorCommand = new DelegateCommand(x => VM != null && VM.Current != null, x => OpenEditorAction());
        }

        void OpenEditorAction()
        {
            if (VM.Current == null)
                return;
            var state = !VM.IsArchiveMode ? ViewModelState.Edit : ViewModelState.Read;
            var vm = new AnalyzeIncomingstatementsEditViewModel(VM.Current, _ribbonState, state,  false, VM.IsArchiveMode);
            var view = new AnalyzeIncomingstatementsEditView(vm);
            App.DashboardManager.OpenNewTab("Детализация отчета", view, state, vm);
        }

        private void grid_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (tableView.GetRowHandleByMouseEventArgs(e) == DataControlBase.InvalidRowHandle) return;
            OpenEditorCommand.Execute(null);
        }
    }
}
