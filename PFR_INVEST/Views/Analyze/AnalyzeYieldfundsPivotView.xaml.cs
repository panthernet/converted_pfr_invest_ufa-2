﻿using System;
using System.Windows;
using PFR_INVEST.BusinessLogic.ViewModelsList;
//using UserControl = System.Windows.Controls.UserControl;

namespace PFR_INVEST.Views.Analyze
{
    /// <summary>
    /// Interaction logic for AnalyzeYieldfundsPivotView.xaml
    /// </summary>
    public partial class AnalyzeYieldfundsPivotView
    {
        AnalyzeYieldfundsPivotViewModel VM { get { return DataContext as AnalyzeYieldfundsPivotViewModel; } }
        public AnalyzeYieldfundsPivotView(AnalyzeYieldfundsPivotViewModel vm)
        {
            InitializeComponent();
            DataContext = vm;
            Loaded += AnalyzeInsuredpersonPivotView_Loaded;
        }

        private void AnalyzeInsuredpersonPivotView_Loaded(object sender, RoutedEventArgs e)
        {
            if (VM != null)
                VM.OnDataRefreshing += VM_OnDataRefreshing;
        }

        private void VM_OnDataRefreshing(object sender, EventArgs e)
        {
            pivot61.ReloadData();
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
           
            AnalyzeReportHelper.SaveReport(pivot61, VM, (DataTemplate)Resources["PageHeader"], VM.FormTitle, 9);
        }





    }
}
