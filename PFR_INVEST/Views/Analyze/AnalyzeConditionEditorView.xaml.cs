﻿using System.Windows;
using System.Windows.Controls;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic.ViewModelsDialog.Analyze.AnalyzeConditionChecker;

namespace PFR_INVEST.Views.Analyze
{
    /// <summary>
    /// Interaction logic for AnalyzeConditionEditorView.xaml
    /// </summary>
    public partial class AnalyzeConditionEditorView : UserControl
    {
        public AnalyzeConditionEditorViewModel VM => (AnalyzeConditionEditorViewModel)DataContext;
        public AnalyzeConditionEditorView(AnalyzeConditionEditorViewModel vm)
        {
            DataContext = vm;
            InitializeComponent();
            Unloaded += AnalyzeConditionEditorView_Unloaded;
        }

        private void AnalyzeConditionEditorView_Unloaded(object sender, RoutedEventArgs e)
        {
            VM.SaveCurrent();
        }

        private void DataControlBase_OnSelectedItemChanged(object sender, SelectedItemChangedEventArgs e)
        {
            grid.UpdateLayout();
        }
    }
}
