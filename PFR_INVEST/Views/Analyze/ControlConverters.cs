﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace PFR_INVEST.Views.Analyze
{
    public class IntervalIdConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return string.Empty;
            switch (value.ToString())
            {
                case "Q":
                    return "Ежеквартально";
                case "Y":
                    return "Ежегодно";
                default:
                    return value.ToString();

            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return string.Empty;
            switch (value.ToString())
            {
                case "Ежеквартально":
                    return "Q";
                case "Ежегодно":
                    return "Y";
                default:
                    return value.ToString();
            }
        }

    }
    public class IntervalInvertIdConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return string.Empty;
            switch (value.ToString())
            {
                case "Ежеквартально":
                    return "Q";
                case "Ежегодно":
                    return "Y";

                default:
                    return value.ToString();
            }
            
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return string.Empty;
            switch (value.ToString())
            {
                case "Q":
                    return "Ежеквартально";
                case "Y":
                    return "Ежегодно";
                default:
                    return value.ToString();
            }
            
        }

    }

    public class NumToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //short val = -1;
            //short.TryParse(value.ToString(), out val);

            if (value == null)
                return false;

            if ((short)value == 1)
                return true;

            return false;


        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return 0;

            if ((bool)value)
                return 1;

            return 0;
        }
    }

}
