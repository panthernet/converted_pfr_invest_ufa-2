﻿using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.ViewModelsCard;

namespace PFR_INVEST.Views
{
    using DataObjects.ListItems;

    public class CorrespondenceVRControlListView : CorrespondenceControlListView
    {
        protected override void OpenItem(CorrespondenceListItemNew item)
        {
            if (item.IsAttach)
                App.DashboardManager.OpenNewTab<LinkedDocumentView, LinkedDocumentVRViewModel>(CorrespondenceListItem.s_AttachDoc, ViewModelState.Edit, item.ID, ViewModelState.Edit);
            else
                App.DashboardManager.OpenNewTab<DocumentVRView, DocumentVRViewModel>(CorrespondenceListItem.s_Doc, ViewModelState.Edit, item.ID, ViewModelState.Edit);
        }
    }
}
