﻿using System.Windows;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for WithdrawalView.xaml
    /// </summary>
    public partial class WithdrawalView
    {
        public WithdrawalView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        private WithdrawalViewModel Model => DataContext as WithdrawalViewModel;

        private void ButtonInfo_Click(object sender, RoutedEventArgs e)
        {
            Model?.GetCourceManuallyOrFromCBRSite();
        }
    }
}
