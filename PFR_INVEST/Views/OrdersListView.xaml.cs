﻿using System.Windows.Input;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    using System.ComponentModel;
    using System.Windows;
    using DevExpress.Xpf.Core;
    using DataAccess.Client;
    using DataObjects;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Interaction logic for OrdersListView.xaml
    /// </summary>
    public partial class OrdersListView
    {
        private bool _mGridRowMouseDoubleClicked;

        public OrdersListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshData(this, Grid, "List");
            ModelInteractionHelper.SubscribeForPageLoadModel(this);
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, Grid);
        }

        private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (Grid.View.GetRowElementByMouseEventArgs(e) == null)
                return;
            if (!IsOrderSelected())
                return;
            _mGridRowMouseDoubleClicked = true;
            var lID = GetSelectedOrderID();
            var id = GetSelectedRelayingOrderID(lID);
            if (id == null)
            {
                App.DashboardManager.OpenNewTab(typeof(OrderView), ViewModelState.Edit, lID, "Поручение");
            }
            else
            {
                App.DashboardManager.OpenNewTab(
                    typeof(OrderRelayingView), ViewModelState.Edit, id.Value, "Поручение на перекладку");
            }
        }

        private void ProcessCollapsingOrExpanding(object sender, RowAllowEventArgs e)
        {
            e.Allow = !_mGridRowMouseDoubleClicked;
            _mGridRowMouseDoubleClicked = false;
        }

        public bool IsOrderSelected()
        {
            var lvl = Grid.View.FocusedRowData.Level;
            if (lvl < Grid.GetGroupedColumns().Count)
                return (Grid.GetGroupedColumns()[lvl].FieldName == "RegNum");
            return true;//All rows are expanded
        }


        public long GetSelectedOrderID()
        {
            return Convert.ToInt64(Grid.GetCellValue(Grid.View.FocusedRowHandle, "OrderID"));
        }

        public long? GetSelectedRelayingOrderID(long lID)
        {
            var order = DataContainerFacade.GetByID<CbOrder, long>(lID);
            if (order == null)
                return null;
            if (order.ParentId.HasValue && order.ParentId.Value > 0)
                return order.ParentId.Value;
            if (order.GetChildrenList().Count > 0)
                return lID;
            return null;
        }


        public string GetSelectedOrderStatus()
        {
            return Convert.ToString(Grid.GetCellValue(Grid.View.FocusedRowHandle, "Status"));
        }

        public OrdersListItem GetSelectedItem()
        {
            var x = Grid.View.FocusedRowData.Row;
            return (OrdersListItem)x;
        }

        public string GetSelectedOrderType()
        {
            return Convert.ToString(Grid.GetCellValue(Grid.View.FocusedRowHandle, "Type"));
        }

        public decimal GetSelectedOrderNonCompetitiveRequest()
        {
            var res = Convert.ToDecimal(Grid.GetCellValue(Grid.View.FocusedRowHandle, "NoncompReq"));
            if (res == 0)
                return -1;
            return res;
        }

        public bool IsSelectedOrderAuction()
        {
            return
                Convert.ToString(Grid.GetCellValue(Grid.View.FocusedRowHandle, "Type"))
                    .Contains(OrderTypeIdentifier.BUYING) &&
                Convert.ToString(Grid.GetCellValue(Grid.View.FocusedRowHandle, "Place"))
                    .Contains(OrderPlacesIdentifier.AUCTION);
        }

        private void OnShowFilterPopup(object sender, FilterPopupEventArgs e)
        {
            if (e.Column.FieldName != "MonthYear") return;

            foreach (var o in e.ComboBoxEdit.ItemsSource as List<object>)
            {
                var item = o as CustomComboBoxItem;
                var editvalue = item?.EditValue as DateTime?;
                if (editvalue != null)
                    item.DisplayValue = $"{DateTools.GetMonthInWordsForDate(editvalue)} {DateTools.GetYearInWordsForDate(editvalue)}";
            }
        }

        private void Grid_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName != "MonthYear")
                return;
            var item = e.Row as OrdersListItem;
            if (item != null)
                e.DisplayText = $"{DateTools.GetMonthInWordsForDate(item.MonthYear)} {DateTools.GetYearInWordsForDate(item.MonthYear)}";
        }
    }
}
