﻿using System;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using DevExpress.Xpf.Grid;
using PFR_INVEST.DataObjects.ListItems;
using System.Collections.Generic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for F50ListView.xaml
    /// </summary>
    public partial class F50ListView
    {
        public F50ListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshData(this, F050Grid, "List");
            ModelInteractionHelper.SubscribeForPageLoadModel(this);
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, F050Grid);
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        private F50ListViewModel Model => DataContext as F50ListViewModel;

        private void tableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (F050Grid.View.GetRowElementByMouseEventArgs(e) == null) return;

            if (F050Grid.View.FocusedRowHandle > 0) return;
            
            var handle = F050Grid.View.FocusedRowHandle;
            var level = F050Grid.GetRowLevelByRowHandle(handle);

            if (level >= F050Grid.GetGroupedColumns().Count || F050Grid.GetGroupedColumns()[level].Name != "c5")
                return;
            object value = F050Grid.GetCellValue(F050Grid.View.FocusedRowHandle, "ID");

            App.DashboardManager.OpenNewTab(typeof(F50View),
                ViewModelState.Edit,
                (long)
                    value, "Отчет о видах и стоимости ценных бумаг");
            e.Handled = true;
        }

        [Obsolete]
        public long GetSelectedID()
        {
            if (F050Grid.View.FocusedRowHandle > -10000)
            {
                if (F050Grid.View.FocusedRowHandle >= 0)
                {
                    return (long)F050Grid.GetCellValue(F050Grid.View.FocusedRowHandle, "EdoID");
                }
            }
            return 0;
        }

        private void F50ListViewShowFilterPopup(object sender, FilterPopupEventArgs e)
        {
            if (e.Column.FieldName != "ReportDate") return;

            var source = (F050Grid.ItemsSource as List<F050ReportListItem>);

            foreach (var o in (List<object>) e.ComboBoxEdit.ItemsSource)
            {
                var item = o as CustomComboBoxItem;
                var editvalue = item?.EditValue as DateTime?;
                if (editvalue == null)
                    continue;
                var si = source?.Find(x => x.ReportDate.HasValue && x.ReportDate.Value.Equals(editvalue.Value));
                item.DisplayValue = si?.RDDate;
            }
        }

        private void F50ListViewDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName != "ReportDate")
                return;
            var item = e.Row as F050ReportListItem;
            if (item != null)
                e.DisplayText = item.RDDate;
        }
    }
}
