﻿using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using DevExpress.Xpf.Grid;
using PFR_INVEST.DataObjects.ListItems;
using System.Collections.Generic;
using System;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
	using System.ComponentModel;
	using System.Windows;
	using DevExpress.Xpf.Core;

	/// <summary>
	/// Interaction logic for MarketCostListView.xaml
	/// </summary>
	public partial class MarketCostListView
	{
		public MarketCostListView()
		{
			InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshData(this, grMarketCostList, "List");
            ModelInteractionHelper.SubscribeForPageLoadModel(this);
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, grMarketCostList);
        }

	    private MarketCostListViewModel Model => DataContext as MarketCostListViewModel;

	    #region events

		private void tableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			if (tableView.GetRowElementByMouseEventArgs(e) == null) return;

			var item = grMarketCostList.SelectedItem as MarketCostListItem;
			if (item == null)
				return;

			long selectedID = item.ID;
			
				switch (item.FormType)
				{
					case Constants.Identifiers.MarketCostIdentifier.Forms.F20:
						App.DashboardManager.OpenNewTab(typeof(F20View), ViewModelState.Read, selectedID, "Форма 20");
						break;
					case Constants.Identifiers.MarketCostIdentifier.Forms.F25:
						App.DashboardManager.OpenNewTab(typeof(F25View), ViewModelState.Read, selectedID, "Расчет рыночной стоимости активов, в которые инвестированы средства пенсионных накоплений (по портфелю УК)");
						break;

				}
		   
		}

		#endregion	

		private void MarketCostListViewShowFilterPopup(object sender, FilterPopupEventArgs e)
		{
			if (e.Column.FieldName != "ReportOnDate") return;

			var source = (grMarketCostList.ItemsSource as List<MarketCostListItem>);

			foreach (var o in (List<object>) e.ComboBoxEdit.ItemsSource)
			{
				var item = o as CustomComboBoxItem;
			    var editvalue = item?.EditValue as DateTime?;
			    if (editvalue == null)
                    continue;
			    var si = source?.Find(x => x.ReportOnDate.HasValue && x.ReportOnDate.Value.Equals(editvalue.Value));
			    item.DisplayValue = si?.ReportOnDateDate;
			}
		}

		private void MarketCostListViewDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
		{
		    if (e.Column.FieldName != "ReportOnDate")
                return;
		    var item = e.Row as MarketCostListItem;
		    if (item != null)
		        e.DisplayText = item.ReportOnDateDate;
		}
	}
}
