﻿using System.Windows.Controls;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for ReorganizationView.xaml
    /// </summary>
    public partial class ReorganizationView : UserControl
    {
        public ReorganizationView()
            : base()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }
    }
}
