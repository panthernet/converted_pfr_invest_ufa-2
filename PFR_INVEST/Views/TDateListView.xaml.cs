﻿using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for INDateListView.xaml
    /// </summary>
    public partial class TDateListView
    {
        public TDateListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, TDateGrid);
        }
    }
}
