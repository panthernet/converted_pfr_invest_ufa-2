﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PFR_INVEST.Resources.Controls
{
	/// <summary>
	/// Interaction logic for ClearableDateEdit.xaml
	/// </summary>
	public partial class ClearableDateEdit : UserControl
	{
		public ClearableDateEdit()
		{
			InitializeComponent();
		}

		public static readonly DependencyProperty EditValueProperty =
			DependencyProperty.Register("EditValue", typeof (object), typeof (ClearableDateEdit), new PropertyMetadata(default(object)));

		public object EditValue
		{
			get { return (object) GetValue(EditValueProperty); }
			set { SetValue(EditValueProperty, value); }
		}

		private void OnClearClick(object sender, RoutedEventArgs e)
		{
			EditValue = null;
		}
	}
}
