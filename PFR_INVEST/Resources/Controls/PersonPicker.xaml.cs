﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Views.Dialogs;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Tools;

namespace PFR_INVEST.Resources.Controls
{
	/// <summary>
	/// Interaction logic for PersonPicker.xaml
	/// </summary>
	public partial class PersonPicker : UserControl
	{
		public PersonPicker()
		{
			InitializeComponent();
		}



		public Person SelectedPerson
		{
			get { return (Person)GetValue(SelectedPersonProperty); }
			set { SetValue(SelectedPersonProperty, value); }
		}

		public static readonly DependencyProperty SelectedPersonProperty =
			DependencyProperty.Register("SelectedPerson", typeof(Person), typeof(PersonPicker),
			new FrameworkPropertyMetadata
			{
				BindsTwoWayByDefault = true,
				DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged,
				PropertyChangedCallback = new PropertyChangedCallback((sender, e) =>
												{
													var ctl = sender as PersonPicker;
													if (ctl != null)
													{
														var person = e.NewValue as Person;
														if (person == null)
															ctl.beText.EditValue = string.Empty;
														else
															ctl.beText.EditValue = person.FIOShort;
													}
												})
			});



		public bool IsReadOnly
		{
			get { return (bool)GetValue(IsReadOnlyProperty); }
			set { SetValue(IsReadOnlyProperty, value); }
		}

		public static readonly DependencyProperty IsReadOnlyProperty =
			DependencyProperty.Register("IsReadOnly", typeof(bool), typeof(PersonPicker), new UIPropertyMetadata(false, new PropertyChangedCallback((sender, e) =>
			{
				((PersonPicker)sender).beText.IsReadOnly = (bool)e.NewValue;
			})));






		private void SelectPerson()
		{
			if (IsReadOnly)
				return;
			var dlg = new LookupPersonGridDlg();
			var dlgVM = new LookupPersonGridDlgViewModel();
			dlg.DataContext = dlgVM;
			dlg.Owner = App.mainWindow;
			ThemesTool.SetCurrentTheme(dlg);
			if (dlg.ShowDialog() == true)
				this.SelectedPerson = dlgVM.SelectedPerson;
		}

		private void beText_DefaultButtonClick(object sender, RoutedEventArgs e)
		{
			SelectPerson();
		}

		private void cmdClear_Click(object sender, RoutedEventArgs e)
		{
			if (IsReadOnly)
				return;
			this.SelectedPerson = null;
		}

		private void beText_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			SelectPerson();
		}


	}
}
