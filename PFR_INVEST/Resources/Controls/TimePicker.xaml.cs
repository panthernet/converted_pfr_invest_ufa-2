﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace PFR_INVEST.Resources.Controls
{
    /// <summary>
    /// Interaction logic for TimePicker.xaml
    /// </summary>
    public partial class TimePicker : UserControl
    {
        public static readonly DependencyProperty ShowSecondsProperty;
      
        public bool ShowSeconds
        {
            set
            {
                SetValue(ShowSecondsProperty, value);
            }
            get { return (bool)GetValue(ShowSecondsProperty); }
        }

        private static void SetSecondsVisibility(TimePicker picker, bool isVisible)
        {
            if (!isVisible)
            {
                picker.editSeconds.Visibility = Visibility.Hidden;
                picker.lblSeconds.Visibility = Visibility.Hidden;
                picker.LayoutRoot.ColumnDefinitions[3].MaxWidth = 0;
                picker.LayoutRoot.ColumnDefinitions[4].MaxWidth = 0;
            }
            else
            {
                picker.editSeconds.Visibility = Visibility.Visible;
                picker.lblSeconds.Visibility = Visibility.Visible;
                picker.LayoutRoot.ColumnDefinitions[3].MaxWidth = double.PositiveInfinity;
                picker.LayoutRoot.ColumnDefinitions[4].MaxWidth = double.PositiveInfinity;
            }
        }


       
        public static readonly DependencyProperty IsReadOnlyProperty = DependencyProperty.Register("IsReadOnly", typeof(bool), typeof(TimePicker), new UIPropertyMetadata(false));
        public bool IsReadOnly
        {
            get { return (bool)GetValue(IsReadOnlyProperty); }
            set { SetValue(IsReadOnlyProperty, value); }
        }
        

        public static readonly DependencyProperty ValueProperty;
        public TimeSpan Value
        {
            get
            {
                return (TimeSpan)GetValue(ValueProperty);
            }
            set
            {
                SetValue(ValueProperty, value);
            }
        }

        public static readonly DependencyProperty HoursProperty;
        public int Hours
        {
            get
            {
                return (int)GetValue(HoursProperty);
            }
            set
            {
                SetValue(HoursProperty, value);
            }
        }

        public static readonly DependencyProperty MinutesProperty;
        public int Minutes
        {
            get
            {
                return (int)GetValue(MinutesProperty);
            }
            set
            {
                SetValue(MinutesProperty, value);
            }
        }

        public static readonly DependencyProperty SecondsProperty;
        public int Seconds
        {
            get
            {
                return (int)GetValue(SecondsProperty);
            }
            set
            {
                SetValue(SecondsProperty, value);
            }
        }

        protected bool ValueChangedInternally;
        private static void OnTimeChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var picker = obj as TimePicker;
            picker.Value = new TimeSpan(picker.Hours, picker.Minutes, picker.Seconds);
            picker.ValueChangedInternally = true;
        }

        private static void OnValueChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var picker = obj as TimePicker;
            if (!picker.ValueChangedInternally)
            {
                picker.Hours = ((TimeSpan)e.NewValue).Hours;
                picker.Minutes = ((TimeSpan)e.NewValue).Minutes;
                picker.Seconds = ((TimeSpan)e.NewValue).Seconds;
                picker.ValueChangedInternally = false;
            }
        }

        private static void OnShowSecondsChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var picker = obj as TimePicker;
            SetSecondsVisibility(picker,(bool) e.NewValue);
        }

        static TimePicker()
        {
            var ValuePropertyMetadata = new FrameworkPropertyMetadata
            {
                PropertyChangedCallback = OnValueChanged,
                DefaultValue = DateTime.Now.TimeOfDay,
                DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged,
                BindsTwoWayByDefault = true
            };
            ValueProperty = DependencyProperty.Register("Value", typeof(TimeSpan), typeof(TimePicker), ValuePropertyMetadata);

            var HoursPropertyMetadata = new FrameworkPropertyMetadata
            {
                PropertyChangedCallback = OnTimeChanged,
                DefaultValue = 0,
                DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged,
                BindsTwoWayByDefault = true
            };
            HoursProperty = DependencyProperty.Register("Hours", typeof(int), typeof(TimePicker), HoursPropertyMetadata);

            var MinutesPropertyMetadata = new FrameworkPropertyMetadata
            {
                PropertyChangedCallback = OnTimeChanged,
                DefaultValue = 0,
                DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged,
                BindsTwoWayByDefault = true
            };
            MinutesProperty = DependencyProperty.Register("Minutes", typeof(int), typeof(TimePicker), MinutesPropertyMetadata);

            var SecondsPropertyMetadata = new FrameworkPropertyMetadata
            {
                PropertyChangedCallback = OnTimeChanged,
                DefaultValue = 0,
                DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged,
                BindsTwoWayByDefault = true
            };
            SecondsProperty = DependencyProperty.Register("Seconds", typeof(int), typeof(TimePicker), SecondsPropertyMetadata);

            var ShowSecondsPropertyMetadata = new FrameworkPropertyMetadata
            {
                PropertyChangedCallback = OnShowSecondsChanged,
                DefaultValue = true,
                DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged,
                BindsTwoWayByDefault = true
            };
            ShowSecondsProperty = DependencyProperty.Register("ShowSeconds", typeof(bool), typeof(TimePicker), ShowSecondsPropertyMetadata);
        }

        public TimePicker()
        {
            InitializeComponent();
        }
    }
}
