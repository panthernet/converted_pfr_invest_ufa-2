﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;

namespace PFR_INVEST.ValidationRules
{
	public class PositiveNumberValidationRule : ValidationRule
	{
		private static readonly string DefaultMessage = "Значение не может быть отрицательным";
		public string CustomMessage { get; set; }

		public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
		{
			bool isValid = (value as decimal?) >= 0
				|| (value as int?) >= 0
				|| (value as long?) >= 0;

			return new ValidationResult(isValid, CustomMessage ?? DefaultMessage);
		}
	}
}
