﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Windows.Controls;

namespace PFR_INVEST.ValidationRules
{
    public class IntValueMaxValidationRule : ValidationRule
    {
        public int Max { get; set; } = int.MaxValue;

        public bool AllowNull { get; set; } = true;

        


        public override ValidationResult Validate(object value, System.Globalization.CultureInfo ci)
        {
            if (value == null && !AllowNull)
                return new ValidationResult(false, "Ввод значения обязателен");

            double total = 0;
            
            try
            {
                total = double.Parse(value.ToString());
            }
            catch
            {
                return new ValidationResult(false, "Недопустимое число");
            }

            if (total > Max)
            {
                return new ValidationResult(false,
                  "Значение больше максимально допустимого (2 147 483 647). \rЗначение не будет сохранено!");
            }
            else
            {
                return new ValidationResult(true, null);
            }
        }
    }


}
