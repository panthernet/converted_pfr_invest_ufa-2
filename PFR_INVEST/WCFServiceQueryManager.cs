﻿namespace PFR_INVEST
{
    /// <summary>
    /// Класс-хелпер для периодических запросов на сервис по заданному интервалу (дефолт, раз в 30сек)
    /// Запросы должны быть макисмально просты и не тормозить
    /// </summary>
    public static class WCFServiceQueryManager
    {
        /// <summary>
        /// Начать процедуру отправки запросов на сервис
        /// </summary>
        public static void Query()
        {
            //простой пинг для поддержания сессии путем запроса сервисных данных
            Ping();

            QueryROPS();
            QueryCBReportApprovement();
        }

        private static void Ping()
        {
            WCFClient.Client.GetServiceVersion();
        }

        private static void QueryCBReportApprovement()
        {
            var message = WCFClient.Client.GetCBReportForApproveMessage();
            if (App.StatusbarManager != null)
                App.StatusbarManager.SetCBReportAppMessage(message);
        }

        private static void QueryROPS()
        {
            //Регулярно так-же проверяем нарушения лимита РОПС
            var message = WCFClient.Client.GetROPSLimitMessage(true, true);
            if (App.StatusbarManager != null)
                App.StatusbarManager.SetErrorMessage(message);
        }
    }
}
