﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using PFR_INVEST.MetricService.BL;

namespace PFR_INVEST.MetricService.Controllers
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MetricService" in code, svc and config file together.
	public class MetricService : IMetricService
	{
		public IEnumerable<MetricItem> GetMetric ()
		{
			return MetricFactory.GetMetric();
		}
	}
}
