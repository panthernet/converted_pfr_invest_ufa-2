﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.DataObjects.ServiceItems;
using PFR_INVEST.Tests.Shared;

namespace PFR_INVEST.Tests.List.BusinessLogic
{
    [TestClass]
    public class TestListViewModelsCreation : BaseTest
    {
        #region Депозиты

        [TestMethod]
        public void TestDepositsListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new DepositsListViewModel((long)1);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestDepositsArchiveListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new DepositsArchiveListViewModel((long)2);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestDepositsListByPortfolioViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new DepositsListByPortfolioViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        #endregion

        #region F40 - F401402

        [TestMethod]
        public void TestSIF402ListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new SIF402ListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestF401402ListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new F401402UKListViewModel(Document.Types.SI);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }
        [TestMethod]
        public void TestF40SIListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new F40SIListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }
        [TestMethod]
        public void TestF40VRListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new F40SIListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestF50SIListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new F50SIListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestF50VRListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new F50SIListViewModel();
            TestPublicProperty(model);
        }

        [TestMethod]
        public void TestF60SIListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new F60SIListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestF60VRListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new F60VRListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestF70SIListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new F70SIListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestF70VRListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new F70VRListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestF80SIListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new F80SIListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestF80VRListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new F80VRListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }



        #endregion

        #region Страхование
        [TestMethod]
        public void TestInsuranceArchiveVRListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new InsuranceArchiveVRListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestInsuranceVRListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new InsuranceVRListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestInsuranceArchiveSIListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new InsuranceArchiveSIListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestInsuranceSIListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new InsuranceSIListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        #endregion

        #region Настройки
        [TestMethod]
        public void TestTemplatesListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new TemplatesListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }
        #endregion

        #region Рейтинги и агенства
        [TestMethod]
        public void TestRatingAgenciesListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new RatingAgenciesListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestRatingsListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new RatingsListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestAgencyRatingsListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new AgencyRatingsListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        #endregion

        #region Заявки
        [TestMethod]
        public void TestDepClaimSelectParamsListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new DepClaimSelectParamsListViewModel();
            TestPublicProperty(model, "IsMoscowStockSelected");
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestDepclaimMaxListViewModel()
        {
            var chk = new PerformanceChecker();
            var modelDepclaimMax = new DepClaimMaxListViewModel(DepClaimMaxListItem.Types.Max);
            var modelDepclaim = new DepClaimMaxListViewModel(DepClaimMaxListItem.Types.Common);
            TestPublicProperty(modelDepclaimMax);
            TestPublicProperty(modelDepclaim);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestDepClaim2ListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new DepClaim2ListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestDepClaim2OfferListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new DepClaim2OfferListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestDepClaim2ConfirmListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new DepClaim2ConfirmListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }


        #endregion

        #region Банки
        [TestMethod]
        public void TestBanksListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new BanksListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestBankAccountsListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new BankAccountsListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestBankAgentsListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new BankAgentsListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestBankConclusionListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new BankConclusionListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }
        #endregion

        #region Контракт (сделка)

        [TestMethod]
        public void TestSIContractsListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new SIContractsListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestVRContractsListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new VRContractsListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        #endregion

        #region Корреспонденция

        [TestMethod]
        public void TestCorrespondenceArchiveListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new CorrespondenceSIArchiveListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestCorrespondenceNpfArchiveListViewModell()
        {
            var chk = new PerformanceChecker();
            var model = new CorrespondenceNpfArchiveListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestCorrespondenceNpfListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new CorrespondenceNpfListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestCorrespondenceNpfArchiveListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new CorrespondenceNpfArchiveListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestCorrespondenceNpfControlListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new CorrespondenceNpfControlListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestCorrespondenceSIListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new CorrespondenceSIListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestCorrespondenceSIArchiveListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new CorrespondenceSIArchiveListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestCorrespondenceSIControlListViewModell()
        {
            var chk = new PerformanceChecker();
            var model = new CorrespondenceSIControlListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestCorrespondenceVRListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new CorrespondenceVRListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestCorrespondenceVRArchiveListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new CorrespondenceVRArchiveListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestCorrespondenceVRControlListViewModell()
        {
            var chk = new PerformanceChecker();
            var model = new CorrespondenceSIControlListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        #endregion

        #region Итоги торгов и индексы
        [TestMethod]
        public void TestBDateListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new BDateListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestINDateListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new INDateListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestTDateListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new TDateListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        #endregion

        #region Журнал регистрации документов ЭДО
        [TestMethod]
        public void TestLoadedODKErrorListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new LoadedODKLogListViewModel(false);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestLoadedODKSuccessListSIViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new LoadedODKLogListSIViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();

        }

        [TestMethod]
        public void TestLoadedODKSuccessListVRViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new LoadedODKLogListVRViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }
        #endregion

        #region Активы
        [TestMethod]
        public void TestMarketCostSIListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new MarketCostSIListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestMarketCostVRListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new MarketCostVRListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }
        [TestMethod]
        public void TestMarketCostScopeListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new MarketCostScopeListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestMarketPricesListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new MarketPricesListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestNetWealthsSIListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new NetWealthsSIListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestNetWealthsVRListViewModel()
        {
            var chk = new PerformanceChecker();
            NetWealthsListViewModel model = new NetWealthsVRListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestNetWealthsSumListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new NetWealthsSumListViewModel(null);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        #endregion

        #region Поручения

        [TestMethod]
        public void TestOrdersListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new OrdersListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }


        #endregion

        #region Реестры
        [TestMethod]
        public void TestRegistersListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new RegistersListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestRegistersArchiveListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new RegistersArchiveListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        #endregion

        #region Отчеты

        [TestMethod]
        public void TestBuyReportsListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new BuyReportsListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestSellReportsListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new SaleReportsListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }


        #endregion

        #region Передача средств

        [TestMethod]
        public void TestTransfersSIListViewModel()
        {
            var chk = new PerformanceChecker();
            TransfersListViewModel model = new TransfersSIListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }


		[TestMethod]
		public void TestTransfersVRListViewModel()
		{
			var chk = new PerformanceChecker();
			TransfersListViewModel model = new TransfersVRListViewModel();
			TestPublicProperty(model);
			chk.CheckPerformance();
		}

        [TestMethod]
        public void TestTransfersArchListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new TransfersArchListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestTransferToNPFListArchiveViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new TransferFromOrToNPFArchiveListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestTransferToNPFListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new TransferFromOrToNPFListViewModel(true);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestTransferFromNPFListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new TransferFromOrToNPFListViewModel(false);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestUKTransfersListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new UKTransfersListViewModel(false);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestUKTransfersListArchiveViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new UKTransfersListArchiveViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        #endregion

        #region Нарушения

        [TestMethod]
        public void TestViolationCategoriesListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new ViolationCategoriesListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestViolationsSIListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new ViolationsSIListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestViolationsVRListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new ViolationsVRListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        #endregion

        #region Выплаты правопреемникам

        [TestMethod]
        public void TestSIAssignPaymentsArchiveListViewModel()
        {
            var chk = new PerformanceChecker();
            AssignPaymentsArchiveListViewModel model = new SIAssignPaymentsArchiveListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestSIAssignPaymentsListViewModel()
        {
            var chk = new PerformanceChecker();
            AssignPaymentsListViewModel model = new SIAssignPaymentsListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();

        }

        [TestMethod]
        public void TestVRAssignPaymentsArchiveListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new VRAssignPaymentsArchiveListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestVRAssignPaymentsListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new VRAssignPaymentsListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        #endregion

        #region Доходность

        [TestMethod]
        public void TestYieldsSIListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new YieldsSIListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestYieldsVRListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new YieldsVRListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }
        #endregion

        #region Журнал движения ЗЛ и СПН

        [TestMethod]
        public void TestSPNMovementsSIListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new SPNMovementsSIListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestSPNMovementsVRListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new SPNMovementsVRListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }


        [TestMethod]
        public void TestZLMovementsSIListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new ZLMovementsSIListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestZLMovementsVRListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new ZLMovementsVRListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        #endregion

        #region Отказные заявления

        [TestMethod]
        
        public void TestRejAppListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new RejAppListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        #endregion

        [TestMethod]
        public void TestPortfoliosListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new PortfoliosListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestPortfolioStatusListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new PortfolioStatusListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestRatesListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new RatesListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestMissedRatesListViewModel()
        {
            using (PerformanceChecker.New())
            {
                var model = new MissedRatesListViewModel();
                TestPublicProperty(model);
            }
        }

        [TestMethod]
        public void TestTempAllocationListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new TempAllocationListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestSIListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new SIListViewModel(false);
            TestPublicProperty(model);
            chk.CheckPerformance();
            model = new SIListViewModel(true);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestSecuritiesListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new SecuritiesListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestSchilsCostsListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new SchilsCostsListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestBalanceListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new BalanceListViewModel(new List<string> { "Сальдо", "Сальдо" });
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestAccountBalanceListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new AccountBalanceListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestPFRBranchesListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new PFRBranchesListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestPFRAccountsListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new PFRAccountsListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestOwnFundsListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new OwnFundsListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestNPFListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new NPFListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestKBKListViewModel()
        {
            using (PerformanceChecker.New())
            {
                var model = new KBKListViewModel();
                TestPublicProperty(model);
            }
        }

        [TestMethod]
        public void TestDivisionListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new DivisionListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestCostsListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new CostsListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestDeadZLListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new DeadZLListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestDelayedPaymentClaimListViewModel()
        {
            using (PerformanceChecker.New())
            {
                var model = new DelayedPaymentClaimListViewModel();
                TestPublicProperty(model);
            }
        }

        [TestMethod]
        public void TestDeleteDocumentsListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new DeleteDocumentsListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestDueListViewModel()
        {
            var chk = new PerformanceChecker();
			var model = new DueListViewModel(new PortfolioTypeFilter { Types = new[] { Portfolio.Types.DSV }, Exclude = true });
            TestPublicProperty(model);

			model = new DueListViewModel(new PortfolioTypeFilter { Types = new[] { Portfolio.Types.DSV } });
			TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestIncomeSecurityListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new IncomeSecurityListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestZLRedistListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new ZLRedistListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

      

        [TestMethod]
        public void TestADUsersListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new ADUsersListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestADRolesListViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new ADRolesListViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }


    }
}
