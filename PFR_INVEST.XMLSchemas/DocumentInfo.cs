﻿using System;
using System.Globalization;
using System.Xml.Serialization;

namespace PFR_INVEST.XmlSchemas
{
    [Serializable]
    public class DocumentInfo
    {
        [XmlAttribute("DOC_DATE")]
        public string Date
        {
            get { return DateValue.ToString("yyyy-MM-dd"); }
            set { DateValue = DateTime.ParseExact(value, @"yyyy-MM-dd", CultureInfo.InvariantCulture); }
        }

        [XmlIgnore]
        public DateTime DateValue { get; set; }

        [XmlAttribute("DOC_TIME")]
        public string Time
        {
            get
            {
                var dt = DateTime.Today.AddSeconds(TimeValue.TotalSeconds);

                return dt.ToString("hh:mm:ss");
            }
            set
            {
                var dt = DateTime.ParseExact($"{DateTime.Today.ToString("yyyy-MM-dd")} {value}", @"yyyy-MM-dd hh:mm:ss", CultureInfo.InvariantCulture);
                TimeValue = dt.TimeOfDay;
            }
        }

        [XmlIgnore]
        public TimeSpan TimeValue { get; set; }

        [XmlAttribute("DOC_NO")]
        public string Number { get; set; }

        [XmlAttribute("DOC_TYPE_ID")]
        public string TypeID { get; set; }

        [XmlAttribute("SENDER_ID")]
        public string SenderID { get; set; }

        [XmlAttribute("RECEIVER_ID")]
        public string RecieverID { get; set; }

        [XmlAttribute("REMARKS")]
        public string Remarks { get; set; }
    }
}
