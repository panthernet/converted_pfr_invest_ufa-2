// ------------------------------------------------------------------------------
//  <auto-generated>
//    Generated by Xsd2Code. Version 3.4.0.37595
//    <NameSpace>PFR_INVEST.BusinessLogic.XMLModels</NameSpace><Collection>List</Collection><codeType>CSharp</codeType><EnableDataBinding>False</EnableDataBinding><EnableLazyLoading>False</EnableLazyLoading><TrackingChangesEnable>False</TrackingChangesEnable><GenTrackingClasses>False</GenTrackingClasses><HidePrivateFieldInIDE>False</HidePrivateFieldInIDE><EnableSummaryComment>False</EnableSummaryComment><VirtualProp>False</VirtualProp><IncludeSerializeMethod>False</IncludeSerializeMethod><UseBaseClass>False</UseBaseClass><GenBaseClass>False</GenBaseClass><GenerateCloneMethod>False</GenerateCloneMethod><GenerateDataContracts>False</GenerateDataContracts><CodeBaseTag>Net40</CodeBaseTag><SerializeMethodName>Serialize</SerializeMethodName><DeserializeMethodName>Deserialize</DeserializeMethodName><SaveToFileMethodName>SaveToFile</SaveToFileMethodName><LoadFromFileMethodName>LoadFromFile</LoadFromFileMethodName><GenerateXMLAttributes>True</GenerateXMLAttributes><EnableEncoding>False</EnableEncoding><AutomaticProperties>False</AutomaticProperties><GenerateShouldSerialize>False</GenerateShouldSerialize><DisableDebug>False</DisableDebug><PropNameSpecified>Default</PropNameSpecified><Encoder>UTF8</Encoder><CustomUsings></CustomUsings><ExcludeIncludedTypes>False</ExcludeIncludedTypes><EnableInitializeFields>True</EnableInitializeFields>
//  </auto-generated>
// ------------------------------------------------------------------------------
namespace PFR_INVEST.XmlSchemas
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Xsd2Code", "3.4.0.37595")]
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class EDO_ODKF140
    {

        private string specDepTreatField;

        private string stopDateField;

        private string realStopDateField;

        private string dateOfReportField;

        private string operatorPostField;

        private string operatorFIOField;

        private string stopDateDayField;

        private string contentReportField;

        private string dogovorNumberField;

        private string dogovorDateField;

        private uint iDReportTypeField;

        private string licenseNFNPFField;

        private string codeInFirm1Field;

        private string codeInFirm2Field;

        private string uKNameField;

        private string uKINNField;

        private string uKFilialField;

        private string theDateField;

        private string docNumberField;

        private string baseDocNumberField;

        private string baseDocDateField;

        private string investcaseNameField;

        private string violationDateField;

        private string violationTypeField;

        private string violationNormaField;

        private string violationFactField;

        private string regNumberOutField;

        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string SpecDepTreat
        {
            get
            {
                return this.specDepTreatField;
            }
            set
            {
                this.specDepTreatField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string StopDate
        {
            get
            {
                return this.stopDateField;
            }
            set
            {
                this.stopDateField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string RealStopDate
        {
            get
            {
                return this.realStopDateField;
            }
            set
            {
                this.realStopDateField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string DateOfReport
        {
            get
            {
                return this.dateOfReportField;
            }
            set
            {
                this.dateOfReportField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string OperatorPost
        {
            get
            {
                return this.operatorPostField;
            }
            set
            {
                this.operatorPostField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string OperatorFIO
        {
            get
            {
                return this.operatorFIOField;
            }
            set
            {
                this.operatorFIOField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string StopDateDay
        {
            get
            {
                return this.stopDateDayField;
            }
            set
            {
                this.stopDateDayField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string ContentReport
        {
            get
            {
                return this.contentReportField;
            }
            set
            {
                this.contentReportField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string DogovorNumber
        {
            get
            {
                return this.dogovorNumberField;
            }
            set
            {
                this.dogovorNumberField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string DogovorDate
        {
            get
            {
                return this.dogovorDateField;
            }
            set
            {
                this.dogovorDateField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public uint IDReportType
        {
            get
            {
                return this.iDReportTypeField;
            }
            set
            {
                this.iDReportTypeField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string LicenseNFNPF
        {
            get
            {
                return this.licenseNFNPFField;
            }
            set
            {
                this.licenseNFNPFField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string CodeInFirm1
        {
            get
            {
                return this.codeInFirm1Field;
            }
            set
            {
                this.codeInFirm1Field = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string CodeInFirm2
        {
            get
            {
                return this.codeInFirm2Field;
            }
            set
            {
                this.codeInFirm2Field = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string UKName
        {
            get
            {
                return this.uKNameField;
            }
            set
            {
                this.uKNameField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string UKINN
        {
            get
            {
                return this.uKINNField;
            }
            set
            {
                this.uKINNField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public string UKFilial
        {
            get
            {
                return this.uKFilialField;
            }
            set
            {
                this.uKFilialField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 17)]
        public string TheDate
        {
            get
            {
                return this.theDateField;
            }
            set
            {
                this.theDateField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 18)]
        public string DocNumber
        {
            get
            {
                return this.docNumberField;
            }
            set
            {
                this.docNumberField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 19)]
        public string BaseDocNumber
        {
            get
            {
                return this.baseDocNumberField;
            }
            set
            {
                this.baseDocNumberField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 20)]
        public string BaseDocDate
        {
            get
            {
                return this.baseDocDateField;
            }
            set
            {
                this.baseDocDateField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 21)]
        public string InvestcaseName
        {
            get
            {
                return this.investcaseNameField;
            }
            set
            {
                this.investcaseNameField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 22)]
        public string ViolationDate
        {
            get
            {
                return this.violationDateField;
            }
            set
            {
                this.violationDateField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 23)]
        public string ViolationType
        {
            get
            {
                return this.violationTypeField;
            }
            set
            {
                this.violationTypeField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 24)]
        public string ViolationNorma
        {
            get
            {
                return this.violationNormaField;
            }
            set
            {
                this.violationNormaField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 25)]
        public string ViolationFact
        {
            get
            {
                return this.violationFactField;
            }
            set
            {
                this.violationFactField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 26)]
        public string RegNumberOut
        {
            get
            {
                return this.regNumberOutField;
            }
            set
            {
                this.regNumberOutField = value;
            }
        }
        
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string VERSION
        { get; set; }
    }
}
