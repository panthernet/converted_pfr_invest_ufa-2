﻿using System;
using System.Xml.Serialization;

namespace PFR_INVEST.XmlSchemas
{
    [Serializable]
        [XmlRoot("MICEX_DOC")]
        public class DocumentPDX09 : DocumentBase
        {

            [XmlElement("PDX09")]
            public PDX09 Body { get; set; }
        }

        [Serializable]
        public class PDX09
        {
            [XmlAttribute("VER")]
            public string Version { get; set; }

            [XmlElement("PDX09_TAB")]
            public PDX09Tab Tab { get; set; }
        }

        [Serializable]
        public class PDX09Tab
        {

            [XmlAttribute("TRADE_DATE")]
            public DateTime TradeDate { get; set; }

            [XmlAttribute("AUCTNO")]
            public int Auctno { get; set; }

            [XmlElement("BOARD")]
            public BOARD09 Board { get; set; }
        }

        [Serializable]
        public class BOARD09
        {
            [XmlAttribute("BOARDID")]
            public string BoardId { get; set; }

            [XmlElement("PDX09_REC")]
            public PDX09Record[] Records { get; set; }
        }

        [Serializable]
        public class PDX09Record
        {
            [XmlAttribute("SECURITYID")]
            public string SECURITYID { get; set; }

            [XmlAttribute("RATE")]
            public decimal RATE { get; set; }

            [XmlAttribute("TOTVAL1")]
            public decimal TOTVAL1 { get; set; }

            [XmlAttribute("TOTVAL1N")]
            public decimal TOTVAL1N { get; set; }

            [XmlAttribute("TOTVAL2")]
            public decimal TOTVAL2 { get; set; }

            [XmlAttribute("WARATE")]
            public decimal WARATE { get; set; }

            [XmlAttribute("SETTLEDATE")]
            public DateTime SETTLEDATE { get; set; }

            [XmlAttribute("SETTLEDATE2")]
            public DateTime SETTLEDATE2 { get; set; }

            [XmlAttribute("QUANT_PART")]
            public long QUANT_PART { get; set; }

            [XmlAttribute("QUANT_REQ")]
            public long QUANT_REQ { get; set; }

        }


    }

