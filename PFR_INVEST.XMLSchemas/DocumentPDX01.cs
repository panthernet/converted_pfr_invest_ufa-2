﻿using System;
using System.Xml.Serialization;

namespace PFR_INVEST.XmlSchemas
{
    [Serializable]
    [XmlRoot("MICEX_DOC")]
    public class DocumentPDX01 : DocumentBase
    {
        [XmlElement("PDX01")]
        public PDX01Body Body { get; set; }
    }

    [Serializable]
    public class PDX01Body
    {
        [XmlAttribute("VER")]
        public string Version { get; set; }

        [XmlElement("PDX01_TAB")]
        public PDX01Tab Tab { get; set; }
    }

    [Serializable]
    public class PDX01Tab
    {
        [XmlAttribute("LIMITDATE")]
        public string LimitDate
        {
            get { return LimitDateValue.ToString("yyyy-MM-dd"); }
            set { LimitDateValue = DateTime.ParseExact(value, @"yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture); }
        }

        [XmlIgnore]
        public DateTime LimitDateValue { get; set; }

        [XmlAttribute("SECURITYID")]
        public string SecurityId { get; set; }

        [XmlElement("PDX01_REC")]
        public PDX01Record[] Records { get; set; }
    }

    [Serializable]
    public class PDX01Record
    {
        [XmlAttribute("FIRMID")]
        public string FirmId { get; set; }

        [XmlAttribute("SHORTNAME")]
        public string ShortName { get; set; }

        [XmlAttribute("LIMIT")]
        public long Limit { get; set; }

        [XmlAttribute("LIMITINFO")]
        public long LimitInfo { get; set; }

        [XmlAttribute("STATUSR")]
        public string StatusUser { get; set; }
    }
}
