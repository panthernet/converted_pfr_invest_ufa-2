﻿using System.Runtime.Serialization;
using System.ServiceModel;

namespace PFR_INVEST.SettingsService.Contract
{
    [ServiceContract(SessionMode = SessionMode.Required)]
    public interface ISettingsService
    {
        [OperationContract]
        UserSettings GetUserSettings(string domain, string userName);

        [OperationContract]
        bool SetUserSettings(UserSettings userSettings);

        [OperationContract]
        string GetLastError();

        [OperationContract]
        string Ping();
    }

    [DataContract]
    public class UserSettings
    {
        [DataMember]
        public string Domain { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string Settings { get; set; }
    }
}
