﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace PFR_INVEST.FilesImportToDBUtil.Converters
{
    public class TrueByValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null && parameter == null)
                return true;
            else if (value == null || parameter == null)
                return false;
            else
                return value.ToString().Equals(parameter.ToString(), StringComparison.CurrentCultureIgnoreCase);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (object.Equals(value, true))
            {
                if (parameter == null)
                    return null;
                else if (parameter.GetType() == targetType)
                    return parameter;
                else if (targetType.IsEnum)
                    return Enum.Parse(targetType, parameter.ToString(), true);
                else if (targetType == typeof(Nullable<bool>))
                    return bool.Parse(parameter.ToString());
                else
                    return null;
            }
            else
            {
                if (targetType != typeof(Guid))
                    return Guid.Empty;
                else
                    return null;
            }
        }
    }
}
