﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PFR_INVEST.FilesImportToDBUtil.Models
{
    public class ImportFileEntry
    {
        public string FilePath { get; set; }
        public string TableName { get; set; }
        public string ColumnName { get; set; }
        public string Id { get; set; }
    }
}
