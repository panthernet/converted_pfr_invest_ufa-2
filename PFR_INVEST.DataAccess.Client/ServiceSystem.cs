﻿using db2connector;

namespace PFR_INVEST.DataAccess.Client
{
    public class ServiceSystem
    {
        public static string EndPointConfigName = "WSHttpBinding_IDB2Connector";
        public static string HttpsEndPointConfigName = "WSHttpsBinding_IDB2Connector";

        public static DataServiceClient CreateNewDataClient(ClientAuthType authType)
        {
            string name;
            switch (authType)
            {
                case ClientAuthType.ECASAHTTPS:
                    name = HttpsEndPointConfigName;
                    break;
                default:
                    name = EndPointConfigName;
                    break;
            }

            return new DataServiceClient(name);
        }

        public static GetDataService GetDataServiceDelegate { get; set; }

        internal static IDB2Connector Client => GetDataServiceDelegate();

        public delegate IDB2Connector GetDataService();
    }
}
