﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataAccess.Client;

namespace PFR_INVEST.DataObjects
{
    public static class FinregisterCorrExtension
    {
        public static Finregister GetFinregister(this FinregisterCorr p_finregisterCorr)
        {
            return DataContainerFacade.GetExtensionData<Finregister>(p_finregisterCorr, "Finregister", delegate()
            {
                return DataContainerFacade.GetByID<Finregister, long>(p_finregisterCorr.FinregisterID);
            });
        }
    }
}
