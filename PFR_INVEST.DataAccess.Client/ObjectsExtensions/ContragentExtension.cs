﻿using System.Collections;
using System.Linq;
using PFR_INVEST.DataAccess.Client;

namespace PFR_INVEST.DataObjects
{
    public static class ContragentExtension
    {
        public static IList GetAccounts(this Contragent p_contragent)
        {
            return DataContainerFacade.GetExtensionData<IList>(p_contragent, "Accounts", delegate()
            {
                return DataContainerFacade.GetObjectProperty<Contragent, long, IList>(p_contragent.ID, "Accounts");
            });
        }

        public static IList GetReorganizations(this Contragent p_contragent)
        {
            return DataContainerFacade.GetExtensionData<IList>(p_contragent, "Reorganizations", delegate()
            {
                return DataContainerFacade.GetObjectProperty<Contragent, long, IList>(p_contragent.ID, "Reorganizations");
            });
        }

        public static Status GetStatus(this Contragent p_contragent)
        {
            return DataContainerFacade.GetExtensionData<Status>(p_contragent, "Status", delegate()
            {
                return DataContainerFacade.GetByID<Status, long>(p_contragent.StatusID);
            });
        }

        public static LegalEntity GetLegalEntity(this Contragent p_contragent)
        {
            if (p_contragent != null)
                return DataContainerFacade.GetExtensionData<LegalEntity>(p_contragent, "LegalEntity", delegate()
                {
                    return DataContainerFacade.GetListByProperty<LegalEntity>("ContragentID", p_contragent.ID).FirstOrDefault();
                });
            else return null;
        }
    }
}
