﻿using PFR_INVEST.DataAccess.Client;
using System.Collections;
using System.Linq;

namespace PFR_INVEST.DataObjects
{
    public static class DocumentExtension
    {
        public static LegalEntity GetLegalEntity(this Document p_doc)
        {
            return DataContainerFacade.GetExtensionData<LegalEntity>(p_doc, "LegalEntity", delegate()
            {
                return DataContainerFacade.GetByID<LegalEntity, long>(p_doc.LegalEntityID);
            });
        }

		public static Person GetExecutor(this Document p_doc)
        {
			return DataContainerFacade.GetExtensionData<Person>(p_doc, "Person", delegate()
            {
				return DataContainerFacade.GetByID<Person, long>(p_doc.ExecutorID);
            });
        }

        public static DocumentClass GetDocumentClass(this Document p_doc)
        {
            return DataContainerFacade.GetExtensionData<DocumentClass>(p_doc, "DocumentClass", delegate()
            {
                return DataContainerFacade.GetByID<DocumentClass, long>(p_doc.DocumentClassID);
            });
        }

        public static AttachClassific GetAttachClass(this Document p_doc)
        {
            return DataContainerFacade.GetExtensionData<AttachClassific>(p_doc, "AttachClassific", delegate()
            {
                return DataContainerFacade.GetByID<AttachClassific, long>(p_doc.DocumentClassID);
            });
        }

        public static AdditionalDocumentInfo GetAdditionalDocumentInfo(this Document p_doc)
        {
            return DataContainerFacade.GetExtensionData<AdditionalDocumentInfo>(p_doc, "AdditionalDocumentInfo", delegate()
            {
                return DataContainerFacade.GetByID<AdditionalDocumentInfo, long>(p_doc.AdditionalInfoID);
            });
        }

        public static DocFileBody GetDocFileBody(this Document p_doc)
        {
            if ( !p_doc.ExtensionData.ContainsKey("DocFileBody")  ||  p_doc.ExtensionData["DocFileBody"] == null)
                p_doc.ExtensionData.Remove("DocFileBody");
            return DataContainerFacade.GetExtensionData<DocFileBody>(p_doc, "DocFileBody", delegate()
            {
                return ServiceSystem.Client.GetDocFileBodyForDocument(p_doc.ID);
            });
        }

        public static IList GetAttaches(this Document p_doc)
        {
            return DataContainerFacade.GetExtensionData<IList>(p_doc, "Attaches", delegate()
            {
                return DataContainerFacade.GetObjectProperty<Document, long, IList>(p_doc.ID, "Attaches")
                                            .Cast<Attach>().Where(a => a.StatusID != -1).ToList();
            });
        }
    }
}
