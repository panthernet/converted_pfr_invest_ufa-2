﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataObjects.BranchReport;
using PFR_INVEST.DataObjects;
//using db2connector;
using System.ServiceModel.Channels;

namespace PFR_INVEST.DataAccess.Client.ObjectsExtensions.PfrBranchReport
{
    /// <summary>
    /// Not used any more. 
    /// </summary>
    [Obsolete]
    public class ImportContentExtensionClient
    {
        public static void Save(PfrBranchReportImportContentBase x)
        {
            SaveBase(x);

            if (x is PfrBranchReportImportContent_CashExpenditures)
                SaveInternal(x as PfrBranchReportImportContent_CashExpenditures);
            else
                if (x is PfrBranchReportImportContent_Application741)
                    SaveInternal(x as PfrBranchReportImportContent_Application741);
                else
                    if (x is PfrBranchReportImportContent_InsuredPerson)
                        SaveInternal(x as PfrBranchReportImportContent_InsuredPerson);
                    else
                        if (x is PfrBranchReportImportContent_AssigneePayment)
                            SaveInternal(x as PfrBranchReportImportContent_AssigneePayment);

                        else
                            if (x is PfrBranchReportImportContent_SignatureContract)
                                SaveInternal(x as PfrBranchReportImportContent_SignatureContract);

        }

        public static void SaveBase(PfrBranchReportImportContentBase x)
        {
            x.PfrBranchReport.ID = DataContainerFacade.Save(x.PfrBranchReport);
        }


        public static void SaveInternal(PfrBranchReportImportContent_CashExpenditures x)
        {
            x.CashExpenditures.BranchReportID = x.PfrBranchReport.ID;
            DataContainerFacade.Save<PfrBranchReportCashExpenditures>(x.CashExpenditures);
        }

        public static void SaveInternal(PfrBranchReportImportContent_Application741 x)
        {
            x.Application741.ReportId = x.PfrBranchReport.ID;
            DataContainerFacade.Save<PfrBranchReportApplication741>(x.Application741);
        }

        public static void SaveInternal(PfrBranchReportImportContent_AssigneePayment x)
        {
            x.AssigneePayment.ReportId = x.PfrBranchReport.ID;
            DataContainerFacade.Save<PfrBranchReportAssigneePayment>(x.AssigneePayment);
        }

        public static void SaveInternal(PfrBranchReportImportContent_InsuredPerson x)
        {
            foreach (PfrBranchReportInsuredPerson insuredPerson in x.Report)
            {
                insuredPerson.ReportId = x.PfrBranchReport.ID;
                DataContainerFacade.Save(insuredPerson);
            }
        }

        public static void SaveInternal(PfrBranchReportImportContent_SignatureContract x)
        {
            foreach (PfrBranchReportSignatureContract y in x.ItemsCredit)
            {
                y.ReportID = x.PfrBranchReport.ID;
                DataContainerFacade.Save(y);
            }

            foreach (PfrBranchReportSignatureContract y in x.ItemsNpf)
            {
                y.ReportID = x.PfrBranchReport.ID;
                DataContainerFacade.Save(y);
            }
        }
    }
}
