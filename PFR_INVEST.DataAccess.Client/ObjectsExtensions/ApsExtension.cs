﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataAccess.Client;

namespace PFR_INVEST.DataObjects
{
	public static class ApsExtension
	{
		public static Portfolio GetPortfolio(this Aps p_aps)
		{
			if (p_aps != null)
				return DataContainerFacade.GetExtensionData<Portfolio>(p_aps, "Portfolio", delegate()
				{
					return DataContainerFacade.GetByID<Portfolio, long>(p_aps.PortfolioID);
				});
			else
				return null;
		}

		public static Portfolio GetSPortfolio(this Aps p_aps)
		{
			if (p_aps != null)
				return DataContainerFacade.GetExtensionData<Portfolio>(p_aps, "SPortfolio", delegate()
				{
					return DataContainerFacade.GetByID<Portfolio, long>(p_aps.SPortfolioID);
				});
			else
				return null;
		}

		public static PfrBankAccount GetPFRBankAccount(this Aps p_aps)
		{
			if (p_aps != null)
				return DataContainerFacade.GetExtensionData<PfrBankAccount>(p_aps, "PFRBankAccount", delegate()
				{
					return DataContainerFacade.GetByID<PfrBankAccount, long>(p_aps.PFRBankAccountID);
				});
			else
				return null;
		}
		public static PfrBankAccount GetSPFRBankAccount(this Aps p_aps)
		{
			if (p_aps != null)
				return DataContainerFacade.GetExtensionData<PfrBankAccount>(p_aps, "SPFRBankAccount", delegate()
				{
					return DataContainerFacade.GetByID<PfrBankAccount, long>(p_aps.SPFRBankAccountID);
				});
			else
				return null;
		}
		public static ApsKind GetKind(this Aps p_aps)
		{
			if (p_aps != null)
				return DataContainerFacade.GetExtensionData<ApsKind>(p_aps, "Kind", delegate()
				{
					return DataContainerFacade.GetByID<ApsKind, long>((long)p_aps.KindID);
				});
			else
				return null;
		}
	}
}
