﻿using System.Collections;
using PFR_INVEST.DataAccess.Client;

namespace PFR_INVEST.DataObjects
{
    public static class SupAgreementExtension
    {
        public static Contract GetContract(this SupAgreement p_SupAgreement)
        {
            return DataContainerFacade.GetExtensionData<Contract>(p_SupAgreement, "Contract", delegate()
            {
                return DataContainerFacade.GetByID<Contract, long>(p_SupAgreement.ContractID);
            });
        }
    }
}
