﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.DataAccess.Client;
using System.Collections;

namespace PFR_INVEST.DataObjects
{
	public static class ContractExtension
	{
		public static LegalEntity GetLegalEntity(this Contract pContract)
		{
		    if (pContract?.LegalEntityID == null)
				return null;
		    return DataContainerFacade.GetExtensionData(pContract, "LegalEntity", () => DataContainerFacade.GetByID<LegalEntity, long>(pContract.LegalEntityID));
		}

	    public static BankAccount GetBankAccount(this Contract pContract)
	    {
	        if (pContract?.BankAccountID == null)
                return null;
	        return DataContainerFacade.GetExtensionData(pContract, "BankAccount", () => DataContainerFacade.GetByID<BankAccount, long>(pContract.BankAccountID));
	    }

	    public static IList GetTransferLists(this Contract pContract)
		{
			return DataContainerFacade.GetExtensionData(pContract, "TransferLists", () => DataContainerFacade.GetObjectProperty<Contract, long, IList>(pContract.ID, "TransferLists"));
		}

		public static IList GetContrInvests(this Contract pContract)
		{
			return DataContainerFacade.GetExtensionData(pContract, "ContrInvests", () => DataContainerFacade.GetObjectProperty<Contract, long, IList>(pContract.ID, "ContrInvests"));
		}

		public static IList GetRewardCosts(this Contract pContract)
		{
			return DataContainerFacade.GetExtensionData(pContract, "RewardCosts", () => DataContainerFacade.GetObjectProperty<Contract, long, IList>(pContract.ID, "RewardCosts"));
		}

		public static List<SupAgreement> GetSupAgreements(this Contract pContract)
		{
		    if (pContract == null) return new List<SupAgreement>();
		    var list = DataContainerFacade.GetExtensionData(pContract, "SupAgreements", () => DataContainerFacade.GetObjectProperty<Contract, long, IList>(pContract.ID, "SupAgreements"));
		    if (list != null && list.Count > 0)
		        return list.Cast<SupAgreement>().ToList();
		    return new List<SupAgreement>();
		}
	}
}
