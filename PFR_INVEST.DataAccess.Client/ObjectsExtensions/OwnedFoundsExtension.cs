﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Client
{
    public static class OwnedFoundsExtension
    {
        public static Year GetYear(this OwnedFounds p_obj)
        {
            return DataContainerFacade.GetExtensionData<Year>(p_obj, "Year", delegate()
            {
                return DataContainerFacade.GetByID<Year, long>(p_obj.YearID);
            });
        }

        public static LegalEntity GetLegalEntity(this OwnedFounds p_obj)
        {
            return DataContainerFacade.GetExtensionData<LegalEntity>(p_obj, "LegalEntity", delegate()
            {
                return DataContainerFacade.GetByID<LegalEntity, long>(p_obj.LegalEntityID);
            });
        }
    }
}
