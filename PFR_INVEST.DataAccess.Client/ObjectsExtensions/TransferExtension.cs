﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataAccess.Client;

namespace PFR_INVEST.DataObjects
{
    public static class TransferExtension
    {
        public static Portfolio GetPortfolio(this Transfer pb_t)
        {
            return DataContainerFacade.GetExtensionData<Portfolio>(pb_t, "Portfolio", delegate()
            {
                return DataContainerFacade.GetByID<Portfolio, long>(pb_t.PortfolioID);
            });
        }

        public static PfrBankAccount GetPfrBankAccount(this Transfer pb_t)
        {
            return DataContainerFacade.GetExtensionData<PfrBankAccount>(pb_t, "PfrBankAccount", delegate()
            {
                return DataContainerFacade.GetByID<PfrBankAccount, long>(pb_t.PFRBankAccountID);
            });
        }
    }
}
