﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.DataAccess.Client;

namespace PFR_INVEST.DataObjects
{
    public static class SIUKPlanExtension
    {
        public static SITransfer GetTransferList(this SIUKPlan p_Plan)
        {
            return DataContainerFacade.GetExtensionData(p_Plan, "TransferList",
                () => DataContainerFacade.GetByID<SITransfer, long>(p_Plan.TransferListID));
        }

        public static ReqTransfer GetReqTransfer(this SIUKPlan p_Plan)
        {
            return DataContainerFacade.GetExtensionData(p_Plan, "ReqTransfer",
                () => DataContainerFacade.GetByID<ReqTransfer, long>(p_Plan.TransferID));
        }

        public static ReqTransfer GetReqTransfer2(this SIUKPlan p_Plan)
        {
            return DataContainerFacade.GetExtensionData(p_Plan, "ReqTransfer2",
                () => DataContainerFacade.GetByID<ReqTransfer, long>(p_Plan.Transfer2ID));
        }

        public static List<ReqTransfer> GetAllReqTransfers(this SIUKPlan p_Plan)
        {
            return new List<ReqTransfer>()
            {
                DataContainerFacade.GetExtensionData(p_Plan, "ReqTransfer2",
                    () => DataContainerFacade.GetByID<ReqTransfer, long>(p_Plan.Transfer2ID)),
                DataContainerFacade.GetExtensionData(p_Plan, "ReqTransfer",
                    () => DataContainerFacade.GetByID<ReqTransfer, long>(p_Plan.TransferID))
            }.Where(a=> a != null).ToList();
        }

        /// <summary>
        /// Возвращает сводное (фейковое) перечисление по суммам всех перечислений указанного Плана УК
        /// </summary>
        /// <param name="p_Plan">План УК</param>
        public static ReqTransfer GetTotalSummReqTransfer(this SIUKPlan p_Plan)
        {
            var one = DataContainerFacade.GetExtensionData(p_Plan, "ReqTransfer",
                () => DataContainerFacade.GetByID<ReqTransfer, long>(p_Plan.TransferID));
            var two = DataContainerFacade.GetExtensionData(p_Plan, "ReqTransfer2",
                () => DataContainerFacade.GetByID<ReqTransfer, long>(p_Plan.Transfer2ID));
            if (one == null && two != null) return two;
            if (one != null && two == null) return one;
            if (one == null && two == null) return null;
            
            return new ReqTransfer()
            {
                InvestmentIncome = one.InvestmentIncome + two.InvestmentIncome,
                Sum = one.Sum + two.Sum
            };
        }

    }
}
