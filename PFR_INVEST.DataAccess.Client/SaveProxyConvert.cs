﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PFR_INVEST.DataAccess.Client
{
    public static class SaveProxyConvert
    {
        public static T[] ConvertList<T>(List<T> p_list)
        {
            if (p_list == null)
                return null;
            return p_list.ToArray();
        }
    }

}
