﻿using System.Windows;
using DevExpress.Xpf.Bars;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Grid;
using System.Windows.Data;
using System.Data;
using System;
using System.Text;
using System.Diagnostics;
using PFR_INVEST.LoadTesting.Scenarios;

namespace PFR_INVEST.LoadTesting.Util
{
    public partial class MainWindow : DXWindow
    {
        LoadTestingController TestingController
        {
            get
            {
                return DataContext as LoadTestingController;
            }
        }

        public delegate void RefreshDelegate();

        public MainWindow()
        {
            InitializeComponent();
            ThemeManager.SetThemeName(this, "Office2007Blue");

			
			
			
			
			DataContext = App.TestingController;
            TestingController.OnRefreshTestItemsList += TestingController_OnRefreshTestItemsList;
            TestingController.OnRefreshMeasure += TestingController_OnRefreshMeasure;

            TestItemsView.RowCellMenuCustomizations.Add(new BarButtonItem
            {
                IsEnabled = false,
                Content = "Выберите сценарий:"
            });
            TestingController.Scenarios.Sort(new ScenarioComparer());
            TestingController.Scenarios.ForEach(
                sc =>
                    {
                        BarButtonItem item = new BarButtonItem 
                        { 
                            Tag = sc.Type.Name, 
                            Content = sc.Name
                        };
                        item.ItemClick += BarButtonItem_ItemClick;
                        Binding binding = new Binding("TestingIsNotInProgress")
                        {
                            Source = TestingController,
                            UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged,
                            Mode = BindingMode.OneWay
                        };
                        item.SetBinding(BarButtonItem.IsEnabledProperty, binding);
                        TestItemsView.RowCellMenuCustomizations.Add(item);
                    }
            );
        }

        void TestingController_OnRefreshMeasure(LoadTestingListItem item)
        {
            Dispatcher.Invoke( new System.Action(() => {
            if ((TestItemsGrid.GetFocusedRow() as LoadTestingListItem) == item)
                mGrid.RefreshData();
            }));
        }

        void BarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            foreach (int handle in TestItemsView.GetSelectedRowHandles())
            {
                (TestItemsGrid.GetRow(handle) as LoadTestingListItem).Scenario = TestingController.Scenarios.Find(sc => sc.Type.Name == e.Item.Tag.ToString()).Type;
            }
            RefreshGrid();
        }

        void RefreshGrid()
        {
            TestItemsGrid.GetBindingExpression(GridControl.DataSourceProperty).UpdateTarget();
            TestItemsGrid.RefreshData();
        }

        void TestingController_OnRefreshTestItemsList()
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.SystemIdle, new RefreshDelegate(RefreshGrid));
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            TestingController.Run();
        }

        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            TestingController.Stop();
        }

        private void TestItemsGrid_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (TestingController.TestingIsInProgress)
                return;

            string message = (TestItemsGrid.GetFocusedRow() as LoadTestingListItem).ScenarioReport;
            if (!string.IsNullOrWhiteSpace(message))
                TestingController.ShowScenarioReport(message);
        }

        private void DXWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            TestingController.Stop();
        }

        private void btnMeasures_Click(object sender, RoutedEventArgs e)
        {
            MeasureTable window = null;
            foreach(var win in Application.Current.Windows)
                if (win is MeasureTable && ((MeasureTable)win).Title.Contains("Сводная"))
                { window = win as MeasureTable; break; }
            if (window == null)
                new MeasureTable(TestingController.TestItems).Show();
            else
            {
                window.UpdateData(TestingController.TestItems);
                window.Focus();
            }
            
        }

        private void TestItemsView_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            var item = (TestItemsGrid.GetFocusedRow() as LoadTestingListItem);
            if (item == null) { mGrid.DataSource = null; return; }
            mGrid.DataSource = item.Measures;
        }

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            if (DXMessageBox.Show("Экспортировать данные?", "Экспорт", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No) return;

            RKLib.ExportData.Export exp = new RKLib.ExportData.Export("Win");

            var tlitem = (TestItemsGrid.GetFocusedRow() as LoadTestingListItem);

            var dt = new DataTable();
            dt.Columns.Add(new DataColumn("text", typeof(string)));
            dt.Columns.Add(new DataColumn("extime", typeof(double)));
            foreach (var item in tlitem.Measures)
            {
                var row = dt.NewRow();
                row.ItemArray = new object[] { item.Text, item.Value};
                dt.Rows.Add(row);
            }

            var filename = string.Format("CSV/{4}_{0}_{1}.{2}.{3}.csv", DateTime.Now.ToShortDateString(), DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, tlitem.ScenarioName);

            if (!System.IO.Directory.Exists("CSV"))
                System.IO.Directory.CreateDirectory("CSV");
            string sTempFile = System.IO.Path.GetTempFileName();

            exp.ExportDetails(dt, RKLib.ExportData.Export.ExportFormat.CSV, sTempFile);
            string sText = System.IO.File.ReadAllText(sTempFile, Encoding.UTF8);
            sText = sText.Replace("\",\"", "\";\"");
            System.IO.File.WriteAllText(filename, sText, Encoding.UTF8);
        }

        private void ComboBoxEditSettings_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            
        }

        private void ComboBoxEditSettings_ContextMenuClosing(object sender, System.Windows.Controls.ContextMenuEventArgs e)
        {
            TestingController.ItemsCount = TestingController.ItemsCount;
        }
    }
}
