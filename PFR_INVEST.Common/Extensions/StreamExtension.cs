﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;

namespace PFR_INVEST.Common.Extensions
{
	public static class StreamExtension
	{
        public static string AttachUniqueTimeStamp(this string str, bool isFileName = false)
	    {
	        if (isFileName)
	        {
	            var path = Path.GetDirectoryName(str);
	            var file = Path.GetFileNameWithoutExtension(str);
	            path = Path.Combine(path, file);
	            var ext = Path.GetExtension(str);
                return $@"{path}_{DateTime.Now:dd.MM.yyyy_HHmmss}{ext}";
            }
	        return $@"{str}_{Guid.NewGuid():N}";
	    }

        public static string AttachUniqueGuid(this string str, bool isFileName = false)
        {
            if (isFileName)
            {
                var file = Path.GetFileNameWithoutExtension(str);
                var ext = Path.GetExtension(str);
                return $@"{file}_{Guid.NewGuid():N}{ext}";
            }
            return $@"{str}_{Guid.NewGuid():N}";
        }

        public static DateTime? ParseTime(this string time, CultureInfo culture = null)
	    {
	        culture = culture ?? CultureInfo.InvariantCulture;

            DateTime result;
	        if (DateTime.TryParse(time, culture, DateTimeStyles.None, out result))
	            return result;
            if (DateTime.TryParseExact(time, new[] { "HH.mm.ss", "HH:mm:ss.f", "HH:mm:ss" }, culture, DateTimeStyles.None, out result))
                return result;

            return null;
	    }

        /// <summary>
        /// Word 2007+ использует другой симол для перехода на новую строку. Данная функция осуществляет замену стандартного символа перевода строки C# на вордовый
        /// </summary>
	    public static string ConvertToWordLineBreaks(this string value)
	    {
	        return value?.Replace("\r\n", "\v") ?? value;
	    }

		// Only useful before .NET 4
		public static void CopyTo(this Stream input, Stream output)
		{
			byte[] buffer = new byte[16 * 1024]; // Fairly arbitrary size
			int bytesRead;

			while ((bytesRead = input.Read(buffer, 0, buffer.Length)) > 0)
			{
				output.Write(buffer, 0, bytesRead);
			}
		}

	    public static long ToLong<T>(this T obj) where T: struct
	    {
	        return (long)(object)obj;
	    }


        /// <summary>
        /// Trims all string properties & fields from object
        /// </summary>
        /// <param name="obj">Object</param>
        /// <param name="trimProps">Do properties trim</param>
        /// <param name="trimFields">Do fields trim</param>
	    public static void TrimAllPropsAndFields(this object obj, bool trimProps = true, bool trimFields = false)
	    {
            if (trimProps)
            {
                var stringProperties = obj.GetType().GetProperties()
                    .Where(p => p.PropertyType == typeof (string));
                foreach (var stringProperty in stringProperties)
                {
                    //skip if we can't write to the property
                    if (!stringProperty.CanWrite) continue;
                    string currentValue = (string) stringProperty.GetValue(obj, null);
                    //skip if value is null
                    if (currentValue != null)
                        stringProperty.SetValue(obj, currentValue.Trim(), null);
                }
            }
            if (trimFields)
            {
                var stringFields = obj.GetType().GetFields()
                    .Where(p => p.FieldType == typeof (string));
                foreach (var field in stringFields)
                {
                    string currentValue = (string)field.GetValue(obj);
                    //skip if value is null
                    if (currentValue != null)
                        field.SetValue(obj, currentValue.Trim());
                }
            }
	    }
	}
}
