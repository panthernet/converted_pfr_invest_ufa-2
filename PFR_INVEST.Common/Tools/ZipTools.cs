﻿using System;
using System.Collections.Generic;
using System.IO;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using PFR_INVEST.Common.Extensions;

namespace PFR_INVEST.Common.Tools
{
	public class ZipTools
	{
		public static List<KeyValuePair<string, byte[]>> UnzipFile(byte[] zippedFile)
		{
			var retVal = new List<KeyValuePair<string, byte[]>>();
			using (var zip = new ZipFile(new MemoryStream(zippedFile)))
			{
				foreach (ZipEntry zipEntry in zip)
				{
					if (!(zipEntry).IsFile)
						continue;

					using (var ms = new MemoryStream())
					{
						zip.GetInputStream(zipEntry).CopyTo(ms);
						var item = new KeyValuePair<string, byte[]>(zipEntry.Name, ms.ToArray());
						retVal.Add(item);
					}
				}
			}
			return retVal;
		}

		public static byte[] ZipFiles(string fileName, byte[] file)
		{
			using (var msOut = new MemoryStream())
			{
				var zipStream = new ZipOutputStream(msOut);
				zipStream.SetLevel(3); //0-9, 9 being the highest level of compression

				var entryName = ZipEntry.CleanName(fileName);
			    var newEntry = new ZipEntry(entryName)
			    {
			        DateTime = DateTime.Now,
			        Size = file.Length
			    };
			    // Note the zip format stores 2 second granularity

			    zipStream.PutNextEntry(newEntry);

				// Zip the file in buffered chunks
				// the "using" will close the stream even if an exception occurs
				var buffer = new byte[4096];
				using (var ms = new MemoryStream(file))
				{
					StreamUtils.Copy(ms, zipStream, buffer);
				}
				zipStream.CloseEntry();


				zipStream.IsStreamOwner = true; // Makes the Close also Close the underlying stream
				zipStream.Close();

				return msOut.ToArray();
			}
		}
	}
}
