﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop.Word;
using Application = Microsoft.Office.Interop.Word.Application;
using Point = Microsoft.Office.Interop.Word.Point;
using Series = Microsoft.Office.Interop.Word.Series;
using _Application = Microsoft.Office.Interop.Word._Application;

namespace PFR_INVEST.Common.Tools
{
    public class WordReportInterop : IWordReport
    {
        private Document _document;
        private readonly object _missing = Missing.Value;
        private _Application _application;
        private readonly string _filePath;

        /// <summary>
        /// Currently opened Word document
        /// </summary>
        public Document Document
        {
            get { return _document; }
        }

        /// <summary>
        /// Get opened Word document 
        /// </summary>
        /// <typeparam name="T">Document class type</typeparam>
        public T GetDocument<T>() where T: class
        {
            return (T)_document;
        }

        /// <summary>
        ///  Constructor
        /// </summary>
        /// <param name="filePath">Template file path</param>
        /// <param name="silent">Supress all warnings and dialogs</param>
        /// <param name="visible">Word is visible to end user</param>
        public WordReportInterop(string filePath, bool silent = true, bool visible = false)
        {
            if(string.IsNullOrEmpty(filePath))
                throw new ArgumentNullException("filePath", "Не задан путь к файлу шаблона!");
            _filePath = filePath;
            try
            {
                _application = new Application {Visible = visible, DisplayAlerts = silent ? WdAlertLevel.wdAlertsNone : WdAlertLevel.wdAlertsAll};
                var filePathInternal = (object) filePath;
                _document = _application.Documents.Add(ref filePathInternal, ref _missing, ref _missing);
            }
            catch
            {
                InternalDispose();
            }
        }

        /// <summary>
        /// Populate pie chart in the template with the data
        /// </summary>
        /// <param name="data">Data</param>
        /// <param name="chartIndex">Chart index inside Word document (starts from 1)</param>
        /// <param name="visibleLabelsCount">Labels count to display starting from 1st data element</param>
        public void PopulatePieChart(Dictionary<string, object> data, int chartIndex, int visibleLabelsCount, int dataStartColumn = 1)
        {
            //data = data.OrderBy(a => a.Key).ToDictionary(a=> a.Key, a => a.Value);
            Workbook wb = null;
            Worksheet sheet = null;
            InlineShape shape = null;
            Series serie = null;
            if (data.Count == 0)
                throw new ArgumentException("Пустые данные для чарта!");
            if (visibleLabelsCount >= data.Count)
                visibleLabelsCount = data.Count;
            if(chartIndex < 1)
                throw new ArgumentException("Индекс чарта должен быть больше нуля!");
            

            try
            {
                shape = _document.InlineShapes[chartIndex];
                if (shape.HasChart != MsoTriState.msoTrue) return;
                shape.Chart.ChartData.Activate();
                wb = (Workbook)shape.Chart.ChartData.Workbook;
                wb.Application.Visible = false;
                sheet = (Worksheet)wb.Sheets[1];

                int counter = 2;
                foreach (var item in data)
                {
                    sheet.Range["A" + counter].Value2 = item.Key;
                    sheet.Range["B" + counter].Value2 = item.Value;
                    counter++;
                }
                //var chartRange = sheet.Range["A2", "B"+counter];

                //sheet.Range["A1", string.Format(@"A{0}", counter - 1)].NumberFormat = "@";
                //обход бага? екселя, при котором неверно строятся зоны значений, если последнее название в списке содержит только цифры
                var savedValue = sheet.Range["A" + (counter - 1)].Value2;
                var savedValueString = string.Empty;
                if (savedValue != null)
                    savedValueString = savedValue.ToString();
                if (!savedValueString.All(char.IsDigit))
                    savedValueString = null;
                else sheet.Range["A" + (counter - 1)].Value2 = "xxx";

                shape.Chart.SetSourceData(string.Format(@"={0}!{2}1:B{1}", sheet.Name, counter - 1, ExtensionClass.GetColumnLetter(sheet, dataStartColumn)), Microsoft.Office.Interop.Excel.XlRowCol.xlColumns);
                if(!string.IsNullOrEmpty(savedValueString))
                    sheet.Range["A" + (counter - 1)].Value2 = savedValue;
                serie = (Series)shape.Chart.SeriesCollection(1);
                for (var i = visibleLabelsCount + 1; i < data.Count+1; i++)
                {
                    var point = (Point)serie.Points(i);
                    if (point.HasDataLabel)
                        point.DataLabel.Delete();
                }
            }
            finally
            {
                if (serie != null)
                    Marshal.ReleaseComObject(serie);

                if (shape != null)
                    Marshal.ReleaseComObject(shape);

                if (wb != null)
                {
                    wb.Close();
                    Marshal.FinalReleaseComObject(wb);
                }

                if (sheet != null)
                    Marshal.FinalReleaseComObject(sheet);
            }
        }

        /// <summary>
        /// Populate bar charts
        /// </summary>
        /// <param name="data">Chart data rows collection</param>
        /// <param name="columns">List of chart column names (Excel view point)</param>
        /// <param name="chartIndex">Chart index in Word document (starts from 1)</param>
        /// <param name="dataStartColumn"> Default 1. Set to 2 if row names are NUMBERS.</param>
        public void PopulateBarChart(Dictionary<string, List<object>> data, List<string> columns, int chartIndex, int dataStartColumn = 1)
        {
            Workbook wb = null;
            Worksheet sheet = null;
            InlineShape shape = null;
            if (data == null || data.Count == 0)
                throw new ArgumentException("Пустые данные для чарта!");
            if (columns == null || columns.Count == 0)
                throw new ArgumentException("Пустые данные для колонок чарта!");
            if (chartIndex < 1)
                throw new ArgumentException("Индекс чарта должен быть больше нуля!");

            try
            {
                shape = _document.InlineShapes[chartIndex];
                if (shape.HasChart != MsoTriState.msoTrue) return;
                shape.Chart.ChartData.Activate();
                wb = (Workbook)shape.Chart.ChartData.Workbook;
                wb.Application.Visible = false;
                sheet = (Worksheet)wb.Sheets[1];

                int counter = 2;
                columns.ForEach(a =>
                {
                    ExtensionClass.GetCell(sheet, 1, counter).Value2 = a;
                    counter++;
                });
                
                counter = 2;
                foreach (var item in data)
                {
                    sheet.Range["A" + counter].Value2 = item.Key;
                    for(int i = 2; i < item.Value.Count + 2; i++)
                    {
                        ExtensionClass.GetCell(sheet, counter, i).Value2 = item.Value[i-2];
                    }
                    counter++;
                }
                shape.Chart.SetSourceData(string.Format(@"={0}!{3}1:{1}{2}", sheet.Name, ExtensionClass.GetColumnLetter(sheet, columns.Count + 1), counter - 1, ExtensionClass.GetColumnLetter(sheet, dataStartColumn)));

            }
            finally
            {
                if (shape != null)
                    Marshal.ReleaseComObject(shape);

                if (wb != null)
                {
                    wb.Close();
                    Marshal.FinalReleaseComObject(wb);
                }

                if (sheet != null)
                    Marshal.FinalReleaseComObject(sheet);
            }
        }

        private void SaveDocument()
        {
            _document.SaveAs(_filePath);
            _document.Saved = true;
        }

        /// <summary>
        /// Save all changes to the document and close app
        /// </summary>
        public void SaveAndClose()
        {
            SaveDocument();
            InternalDispose();
        }

        #region Find and replace
        /// <summary>
        /// Find and replace text in the document
        /// </summary>
        /// <param name="findText">Search text</param>
        /// <param name="replaceWithText">Replace text</param>
        public void FindAndReplace(object findText, object replaceWithText)
        {
            object matchCase = true;
            object matchWholeWord = true;
            object matchWildCards = false;
            object matchSoundsLike = false;
            object matchAllWordForms = false;
            object forward = true;
            object format = false;
            object matchKashida = false;
            object matchDiacritics = false;
            object matchAlefHamza = false;
            object matchControl = false;
            object replace = 2;
            object wrap = 1;


            if (_application != null)
                _application.Selection.Find.Execute(ref findText, ref matchCase, ref matchWholeWord, ref matchWildCards,
                                               ref matchSoundsLike, ref matchAllWordForms, ref forward,
                                               ref wrap, ref format, ref replaceWithText, ref replace, ref matchKashida,
                                               ref matchDiacritics, ref matchAlefHamza, ref matchControl);
        }
        #endregion

        /// <summary>
        /// Save document and show word app
        /// </summary>
        public void Show()
        {
            if(_document != null)
                SaveDocument();

            if(_application != null)
                _application.Visible = true;
        }

        /// <summary>
        /// Hide word app
        /// </summary>
        public void Hide()
        {
            if (_application != null)
                _application.Visible = false;
        }

        /// <summary>
        /// Populate word table
        /// </summary>
        /// <param name="columns">Table columns data</param>
        /// <param name="rows">Table rows data</param>
        /// <param name="options">Table params</param>
        public void PopulateTable(List<WordReportDataColumn> columns, Dictionary<string, List<object>> rows, PopulateTableOptions options)
        {
            if(_document == null)
                throw new Exception("Документ не инициализирован!");
            if (options == null)
                throw new ArgumentException("Настройки генерации таблицы не заданы!", "options");
            if (columns == null || !columns.Any())
                throw new ArgumentException("Пустой список колонок!", "columns");
            if (rows == null || !rows.Any())
                throw new ArgumentException("Пустой список строк!", "rows");
            if(options.TableIndex < 1)
                throw new ArgumentException("Неверный индекс таблицы!");
            Table table = null;
            var defaultRowAppearance = new RowAppearance();
            var defaultColumnAppearance = new ColumnAppearance();
            try
            {
                table = _document.Tables[options.TableIndex];
                //table.Columns[1].Shading.BackgroundPatternColor = (WdColor)15853276;
                foreach (var t in columns)
                {
                    Column lastColumn = null;
                    try
                    {
                        lastColumn = table.Columns.Add();
                        var cIndex = lastColumn.Index;
                        table.Cell(1, cIndex).Range.Text = t.Name;

                        var columnAppearance = options.ColumnAppearances.ContainsKey(cIndex) ? options.ColumnAppearances[cIndex] : defaultColumnAppearance;
                        lastColumn.PreferredWidth = columnAppearance.PreferredWidth ?? options.DefaultColumnWidth;
                        lastColumn.PreferredWidthType = columnAppearance.PreferredWidthType;
                    }
                    finally
                    {
                        if (lastColumn != null)
                            Marshal.ReleaseComObject(lastColumn);
                    }
                }

                foreach (var row in rows)
                {
                    Row lastRow = null;
                    try
                    {
                        lastRow = options.InsertBeforeLastRow ? table.Rows.Add(table.Rows.Last) : table.Rows.Add();

                        var index = lastRow.Index;
                        table.Cell(index, 1).Range.Text = row.Key;

                        for (int i = 2; i < (row.Value.Count + 2); i++)
                        {
                            table.Cell(index, i).Range.Text = (int) row.Value[i - 2] == 0 ? null : row.Value[i - 2].ToString();
                        }

                        var rowAppearance = options.RowAppearances.ContainsKey(index) ? options.RowAppearances[index] : defaultRowAppearance;
                        lastRow.Shading.BackgroundPatternColor = rowAppearance.BackgroundColor;
                        lastRow.Range.Font.Bold = rowAppearance.TextBold ? 1 : 0;
                        lastRow.Cells.Height = rowAppearance.Height ?? options.DefaultRowHeight;
                    }
                    finally
                    {
                        if (lastRow != null)
                            Marshal.ReleaseComObject(lastRow);
                    }
                }
                if(options.AutoFitTableWidth)
                    table.AutoFitBehavior(WdAutoFitBehavior.wdAutoFitWindow);
            }
            finally
            {
                if (table != null)
                    Marshal.FinalReleaseComObject(table);
            }
        }

        #region Table helper functions
        #endregion

        #region Dispose
        private void InternalDispose()
        {
            if (_document != null)
            {
                _document.Close();
                Marshal.FinalReleaseComObject(_document);
                _document = null;
            }

            if (_application != null)
            {
                _application.Quit();
                Marshal.FinalReleaseComObject(_application);
                _application = null;
            }
        }

        public void Dispose()
        {
            InternalDispose();
        }
        #endregion


    }

    public class PopulateTableOptions
    {
        public int TableIndex { get; set; }
        public bool InsertBeforeLastRow { get; set; }
        public int DefaultColumnWidth { get; set; }
        public int DefaultRowHeight { get; set; }
        /// <summary>
        /// Gets or sets if table should fit the page width after all columns has been generated
        /// </summary>
        public bool AutoFitTableWidth { get; set; }


        public Dictionary<int, RowAppearance> RowAppearances { get; set; }
        public Dictionary<int, ColumnAppearance> ColumnAppearances { get; set; }

        public PopulateTableOptions()
        {
            DefaultRowHeight = 25;
            DefaultColumnWidth = 25;
            TableIndex = 1;
            RowAppearances = new Dictionary<int, RowAppearance>();  
            ColumnAppearances = new Dictionary<int, ColumnAppearance>();
        }
    }

    /// <summary>
    /// Table columns appearance settings
    /// </summary>
    public class ColumnAppearance
    {
        /// <summary>
        /// Column preferred width
        /// </summary>
        public int? PreferredWidth { get; set; }
        /// <summary>
        /// Column preferred width type
        /// </summary>
        public WdPreferredWidthType PreferredWidthType { get; set; }

        public ColumnAppearance()
        {
            PreferredWidthType = WdPreferredWidthType.wdPreferredWidthPoints;
        }
    }

    /// <summary>
    /// Table row appearance settings
    /// </summary>
    public class RowAppearance
    {
        private WdColor _backColor;
        /// <summary>
        /// Row cells background color
        /// </summary>
        public WdColor BackgroundColor { get { return CustomBackgroundColor != null ? (WdColor)CustomBackgroundColor : _backColor; } set { _backColor = value; } }

        /// <summary>
        /// Custom RGB color, auto used in BackgroundColor property if set
        /// </summary>
        public int? CustomBackgroundColor { get; set; }

        /// <summary>
        /// Row text is bold
        /// </summary>
        public bool TextBold { get; set; }
        /// <summary>
        /// Row height
        /// </summary>
        public int? Height { get; set; }

        public RowAppearance()
        {
            BackgroundColor = WdColor.wdColorAutomatic;
        }
    }

    public class WordReportDataColumn
    {
        public string Name { get; set; }
        public string Value { get; set; }

        public WordReportDataColumn(string name, string value)
        {
            Name = name;
            Value = value;
        }
    }

    public static class ExtensionClass
    {
        public static Microsoft.Office.Interop.Excel.Range GetCell(this Worksheet sheet, long row, long col)
        {
            return sheet.Range[sheet.Cells[row, col], sheet.Cells[row, col]];
        }

        public static string GetColumnLetter(this Worksheet sheet, int columnNumber)
        {
            var dividend = columnNumber;
            var columnName = String.Empty;

            while (dividend > 0)
            {
                var modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo) + columnName;
                dividend = (dividend - modulo) / 26;
            }

            return columnName;
        }
    }
}
