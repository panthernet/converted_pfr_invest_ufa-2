﻿using System;

namespace PFR_INVEST.Common.Tools
{
	/// <summary>
	/// Класс расширений для безопасного преобразования типов
	/// </summary>
	public static class ConvertTools
	{
		/// <summary>
		/// Приводит <paramref name="obj"/> значение к типу <typeparamref name="T"/>,
		/// если не может привести. возвращает дефолтное значение (<typeparamref name="T"/>)
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="obj"></param>
		/// <returns></returns>
		public static T To<T>(this object obj)
		{
			return To(obj, default(T));
		}

		/// <summary>
		/// Приводит <paramref name="obj"/> значение к типу <typeparamref name="T"/>,
		/// если не может привести. возвращает дефолтное значение (<typeparamref name="T"/>)
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="obj"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public static T To<T>(this object obj, T defaultValue)
		{
			if (obj == null)
				return defaultValue;

			if (obj is T)
				return (T)obj;

			Type type = typeof(T);

			// Место преобразования в ссылочные типы
			if (type == typeof(string))
			{
				return (T)(object)obj.ToString();
			}

			Type underlyingType = Nullable.GetUnderlyingType(type);
			if (underlyingType != null)
			{
				return To(obj, defaultValue, underlyingType);
			}

			return To(obj, defaultValue, type);
		}

		private static T To<T>(object obj, T defaultValue, Type type)
		{
			if (type.IsEnum)
			{
				if (Enum.IsDefined(type, obj))
					return (T)Enum.Parse(type, obj.ToString(), true);
				return defaultValue;
			}

			try
			{
				return (T)Convert.ChangeType(obj, type, null);
			}
			catch (Exception e)
			{
				return defaultValue;
			}
		}
	}
}
