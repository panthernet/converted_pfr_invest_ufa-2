﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Word;

namespace PFR_INVEST.Common.Tools
{

    public class MyWord: IDisposable
    {
        private object _internalWord;
        private object _objCurDoc;
        private bool _isSuccesLast;

        public bool IsSucces
        {
            get { var old = _isSuccesLast; _isSuccesLast = false; return old; }
        }

        public MyWord()
        {
            try
            {
                //var objClassType = Type.GetTypeFromProgID("Word.Application");
                _internalWord = new Application(); //Activator.CreateInstance(objClassType);
                _isSuccesLast = true;
            }
            catch
            {
                _isSuccesLast = false;
            }

        }

        public void Dispose()
        {
            if (_objCurDoc != null)
                CloseDoc();
            if (_internalWord != null)
            {
                GetInternal().Quit();
                Marshal.FinalReleaseComObject(_internalWord);
                _internalWord = null;
            }
        }

        public Application GetInternal()
        {            
            return _internalWord as Application;
        }

        public bool CloseDoc()
        {
            try
            {
                //Close Active Document
                _objCurDoc.GetType().InvokeMember("Close", BindingFlags.InvokeMethod,
                    null, _objCurDoc, null);
                Marshal.FinalReleaseComObject(_objCurDoc);
                _objCurDoc = null;
                return true;
            }
            catch
            {
                return false;
            }
        }
        /*public bool Quit()
        {
            try
            {
                var parameters = new object[3];
                parameters[0] = 0;
                parameters[1] = 0;
                parameters[2] = false;
                _internalWord.GetType().InvokeMember("Quit", BindingFlags.SetProperty,
                    null, _internalWord, parameters);
                //var obj = Type.Missing;
                //((Microsoft.Office.Interop.Word.ApplicationClass)_internalWord).Quit();
                return true;
            }
            catch
            {
                return false;
            }
        }*/
        public bool ShowWord(bool isVisible)
        {
            try
            {
                var parameters = new object[1];
                parameters[0] = isVisible;
                _internalWord.GetType().InvokeMember("Visible", BindingFlags.SetProperty,
                    null, _internalWord, parameters);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool OpenFile(string fileName)
        {
            try
            {
                var objDocsLate = _internalWord.GetType().InvokeMember(
                    "Documents", BindingFlags.GetProperty, null, _internalWord, null);

                var parameters = new object[4];
                parameters[0] = fileName;
                parameters[1] = Missing.Value;
                parameters[2] = Missing.Value;
                parameters[3] = Missing.Value;
                _objCurDoc = objDocsLate.GetType().InvokeMember("Add",
                    BindingFlags.InvokeMethod, null, objDocsLate, parameters);
                
               /* object missing = Missing.Value;
                object filePath = fileName;
                _objCurDoc = _internalWord.Documents.Add(ref filePath, ref missing, ref missing, ref missing);*/
                return true;
            }
            catch
            {
                return false;
            }

        }
        public bool SaveDocument()
        {
            if (_objCurDoc == null) return false;
            try
            {
                _objCurDoc.GetType().InvokeMember("Save",
                BindingFlags.InvokeMethod, null, _objCurDoc, null);
                return true;
            }
            catch
            {
                return false;
            }

        }

        public bool FindAndReplace(object findText, string replaceWithText)
        {
            try
            {
                //http://jira.dob.datateh.ru/browse/DOKIPIV-541
                var rt = replaceWithText == null ? null : replaceWithText.Replace("^", "");

                var objSelection = _internalWord.GetType().InvokeMember(
                    "Selection", BindingFlags.GetProperty, null, _internalWord, null);

                var parameters = new object[2];
                parameters[0] = 0;
                parameters[1] = 0;

                objSelection.GetType().InvokeMember(
                    "SetRange", BindingFlags.InvokeMethod, null, objSelection, parameters);

                var objFind = objSelection.GetType().InvokeMember(
                    "Find", BindingFlags.GetProperty, null, objSelection, null);

                parameters = new object[11];
                parameters[0] = findText; //find text
                parameters[1] = false; //match case
                parameters[2] = true; //match whole word
                parameters[3] = false; //match wild card
                parameters[4] = false; //match sounds like
                parameters[5] = false; //match all word forms
                parameters[6] = true; //forward
                parameters[7] = false; //wrap
                parameters[8] = false; //format
                parameters[9] = rt; //replace text
                parameters[10] = 2; //replace?
                objFind.GetType().InvokeMember("Execute",
                    BindingFlags.InvokeMethod, null, objFind, parameters);
                return true;
            }
            catch
            {
                return false;
            }

        }
        public bool SaveAs(string filename)
        {
            if (_objCurDoc == null) return false;
            var parameters = new object[1];
            parameters[0] = filename; //find text
            try
            {
                _objCurDoc.GetType().InvokeMember("SaveAs",
                    BindingFlags.InvokeMethod, null, _objCurDoc, parameters);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool PrintPreview()
        {
            if (_objCurDoc == null) return false;
            try
            {
                _objCurDoc.GetType().InvokeMember("PrintPreview",
                    BindingFlags.InvokeMethod, null, _objCurDoc, null);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Print()
        {
            if (_objCurDoc == null) return false;
            try
            {
                var parameters = new object[14];
                parameters[0] = false; //in bacground?
                parameters[0] = Missing.Value; //append
                parameters[0] = 0; //all document wdPrintAllDocument
                parameters[0] = Missing.Value; //Output file name
                parameters[0] = Missing.Value; //From
                parameters[0] = Missing.Value; //To page
                parameters[0] = Missing.Value; //Item?
                parameters[0] = 1; //Copies
                parameters[0] = Missing.Value; //Pages
                parameters[0] = Missing.Value; //Page type?
                parameters[0] = false; //print to file
                parameters[0] = false; //collate
                parameters[0] = Missing.Value; //only for MAC
                parameters[0] = Missing.Value; //Manual duplex
                parameters[0] = Missing.Value; //collate


                _objCurDoc.GetType().InvokeMember("PrintOut",
                    BindingFlags.InvokeMethod, null, _objCurDoc, null);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static string GetUniqueFile(string filename)
        {
            return GetUniqueFile(filename, false, string.Empty);
        }

        public static string GetUniqueFile(string filename, bool move)
        {
            return GetUniqueFile(filename, move, string.Empty);
        }

        public static string GetUniqueFile(string filename, bool move, string newfilename)
        {
            if (string.IsNullOrEmpty(filename)) return newfilename;

            var path = Path.GetDirectoryName(filename);
            if (string.IsNullOrEmpty(path)) return newfilename;// "GetUniqueFile -> Путь к исходному файлу не указан!";

            var name = Path.GetFileNameWithoutExtension(!string.IsNullOrEmpty(filename) ? newfilename : filename);
            if (string.IsNullOrEmpty(name)) return newfilename;//throw new Exception("GetUniqueFile -> Исходное имя файла не указано!");
            var extension = !string.IsNullOrEmpty(newfilename) ? Path.GetExtension(newfilename) : Path.GetExtension(filename);
            if (string.IsNullOrEmpty(extension)) return newfilename;//throw new Exception("GetUniqueFile -> Расширение исходного файла не указано!");
            var result = string.Empty;

            for (var i = 0; i < 999999; i++)
            {
                var tmpfile = $"{name}{(i == 0 ? "" : i.ToString())}{extension}";
                var tmppath = Path.Combine(path, tmpfile);
                if (!File.Exists(tmppath))
                {
                    result = tmppath; break;
                }
                //try { File.Delete(tmppath); result = tmppath; break; }
                //catch { continue; }
            }

            if (string.IsNullOrEmpty(result))
            {
                return Path.Combine(Path.GetTempPath(), Path.GetTempFileName());
                //throw new Exception("GetUniqueFile -> Ошибка генерации имени файла!");
            }
            if (move)
                File.Move(filename, result);
            return result;
        }
    }
}
