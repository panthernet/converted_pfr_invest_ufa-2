﻿using System;
using System.Collections.Generic;

namespace PFR_INVEST.Common.Tools
{
    public static class DateTools
    {
        public static readonly string[] Months = "Январь.Февраль.Март.Апрель.Май.Июнь.Июль.Август.Сентябрь.Октябрь.Ноябрь.Декабрь".Split('.');

        public static readonly string[] MonthesGenitive = { "января", "февраля", "марта", "апреля", "мая", "июня",
            "июля", "августа", "сентября", "октября", "ноября", "декабря"};

        public static readonly string[] MonthesPrepositional = { "январе", "феврале", "марте", "апреле", "мае", "июне",
            "июле", "августе", "сентябре", "октябре", "ноябре", "декабре"};


        public static int GetLastMonthOfQuarterForBO(int quarter)
        {
            switch (quarter)
            {
                case 1:
                    return 4;
                case 2:
                    return 7;
                case 3:
                    return 10;
                default:
                    return 1;
            }
        }

        public static int GetFirstMonthOfQuarterForBO(int quarter)
        {
            switch (quarter)
            {
                case 1:
                    return 1;
                case 2:
                    return 4;
                case 3:
                    return 7;
                default:
                    return 10;
            }
        }

        public static string GetRomeQuarterFromMonth(int month)
        {
            switch (month)
            {
                case 1:
                case 2:
                case 3:
                    return "I";
                case 4:
                case 5:
                case 6:
                    return "II";
                case 7:
                case 8:
                case 9:
                    return "III";
                case 10:
                case 11:
                case 12:
                    return "IV";
                default:
                    return "";
            }
        }

        public static bool IsPeriodIntersect(DateTime? firstStart, DateTime? firstEnd, DateTime? secondStart, DateTime? secondEnd) 
        {
            firstStart = firstStart ?? DateTime.MinValue;
            firstEnd = firstEnd ?? DateTime.MaxValue;
            secondStart = secondStart ?? DateTime.MinValue;
            secondEnd = secondEnd ?? DateTime.MaxValue;

            if (firstStart > firstEnd)
                throw new ArgumentException("First period");
            if (secondStart > secondEnd)
                throw new ArgumentException("Second period");

            if (firstEnd < secondStart) return false;
            if (secondEnd < firstStart) return false;

            return true;
        }

        public static string GetYearInWordsForDate(DateTime? value)
        {
            if (!value.HasValue)
                return null;

            return GetYearInWordsForDate(value.Value);
        }

        public static string GetYearInWordsForDate(DateTime value)
        {
            return value.Year.ToString();
        }

        public static int GetCurrentQuarter()
        {
            switch (DateTime.Today.Month)
            {
                case 1:
                case 2:
                case 3:
                    return 1;
                case 4:
                case 5:
                case 6:
                    return 2;
                case 7:
                case 8:
                case 9:
                    return 3;
                case 10:
                case 11:
                case 12:
                    return 4;
            }
            return 0;
        }

        public static int GetQuarter(DateTime date)
        {
            switch (date.Month)
            {
                case 1:
                case 2:
                case 3:
                    return 1;
                case 4:
                case 5:
                case 6:
                    return 2;
                case 7:
                case 8:
                case 9:
                    return 3;
                case 10:
                case 11:
                case 12:
                    return 4;
            }
            return 0;
        }

        public static string GetQarterYearInWordsForDate(DateTime? value)
        {
            if (!value.HasValue)
                return null;
            return GetQarterYearInWordsForDate(value.Value.Month);
        }

        public static string GetQuarterRomeName(int quarter, bool onlyNumber = false)
        {
            switch (quarter)
            {
                case 1:
                    return $"I{(onlyNumber? "" : " квартал")}";
                case 2:
                    return $"II{(onlyNumber ? "" : " квартал")}";
                case 3:
                    return $"III {(onlyNumber ? "" : " квартал")}";
                case 4:
                    return $"IV{(onlyNumber ? "" : " квартал")}";
                default: return null;
            }
        }

        public static string GetQarterYearInWordsForDate(int? monthIndex)
        {
            const string quarter = " квартал";
            string result = String.Empty;

            switch (monthIndex)
            {
                case 1:
                case 2:
                case 3:
                    result = "I" + quarter;
                    break;
                case 4:
                case 5:
                case 6:
                    result = "II" + quarter;
                    break;
                case 7:
                case 8:
                case 9:
                    result = "III" + quarter;
                    break;
                case 10:
                case 11:
                case 12:
                    result = "IV" + quarter;
                    break;
                default:
                    result = null;
                    break;
            }
            return result;
        }

        public static string GetHalfYearInWordsForDate(DateTime? value)
        {
            if (!value.HasValue)
                return null;

            const string halfYear = " полугодие";
            string result = (value.Value.Month <= 6 ? "I" : "II") + halfYear;

            return result;
        }

        public static string GetHalfYearInWordsForDate(DateTime value)
        {
            return GetHalfYearInWordsForDate(value);
        }

        public static string GetMonthInWordsForDate(DateTime? value)
        {
            if (!value.HasValue)
                return null;
            if (value == DateTime.MinValue)
                return null;
            return GetMonthInWordsForDate(value.Value.Month);
        }

        public static string GetMonthInWordsForDate(int? monthIndex)
        {
            if (monthIndex.HasValue && monthIndex <= 12)
            {
                string result = Months[monthIndex.Value - 1];
                return result;
            }
            else
                return null;
        }

        public static DateTime? Max(DateTime? lh, DateTime? rh)
        {
            if (!lh.HasValue)
            {
                if (!rh.HasValue)
                    return null;
                else
                    return rh;
            }
            else
            {
                if (!rh.HasValue)
                    return lh;
                else
                    return DateTime.Compare(lh.Value, rh.Value) < 0 ? rh : lh;
            }
        }

        public static string GetProperMonthName(long monthId, string monthName)
        {
            string month = monthName.Trim().ToLower();
            switch (monthId)
            {
                case 3:
                case 8:
                    return month + "е";
                default:
                    return month.Substring(0, month.Length - 1) + "е";
            }
        }

        public static Dictionary<string, int> GetYearsDictionaryPlusTwoYears()
        {
            int firstYear = 2000;
            int lastYear = DateTime.Now.Year + 2;
            int allperiodValue = 0;
            string allperiodKey = "Весь период";

            var OptionYearsList = new Dictionary<string, int> { { allperiodKey, allperiodValue } };

            for (int c = lastYear; c >= firstYear; c--)
            {
                OptionYearsList.Add(c.ToString(), c);
            }

            return OptionYearsList;
        }

        public static Dictionary<string, int> GetYearsDictionaryPlus(int plusYearCount, bool addAllPeriod = false)
        {
            const int firstYear = 2000;
            const int allperiodValue = 0;
            var lastYear = DateTime.Now.Year + plusYearCount;

            var optionYearsList = new Dictionary<string, int>();
            if(addAllPeriod)
                optionYearsList.Add("Весь период", allperiodValue);

            for (var c = lastYear; c >= firstYear; c--)
                optionYearsList.Add(c.ToString(), c);

            return optionYearsList;
        }


    }
}
