using System;
using System.Collections.ObjectModel;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Xml;

namespace PFR_INVEST.Common.MessageCompressor
{
    /// <summary>
    /// ����������� ������
    /// </summary>
    [Flags]
    public enum Compress
    {
        /// <summary>
        /// ��� ������
        /// </summary>
        None = 0,
        /// <summary>
        /// ������ ��������
        /// </summary>
        Request = 1,
        /// <summary>
        /// ������ �������
        /// </summary>
        Reply = 2,
    }

    /// <summary>
    /// ���������� ��������� ��� ������ ��������� �������
    /// </summary>
    public class MessageCompressionAttribute : Attribute, IEndpointBehavior, IServiceBehavior
    {
        /// <summary>
        /// ���������� ���������
        /// </summary>
        public class MessageInspector : IDispatchMessageInspector, IClientMessageInspector
        {
            /// <summary>
            /// ����������� �� ���������
            /// </summary>
            public MessageInspector()
                : this(Compress.None)
            { }

            /// <summary>
            /// �����������
            /// </summary>
            /// <param name="compress">����������� ������</param>
            public MessageInspector(Compress compress)
            {
                _compress = compress;
            }

            /// <summary>
            /// ����������� ������
            /// </summary>
            private readonly Compress _compress;
            /// <summary>
            /// ����������� ������
            /// </summary>
            public Compress Compress
            {
                get { return _compress; }
            }

            /// <summary>
            /// �������� ���������� ���������
            /// </summary>
            private static byte[] GetBodyContents(Message message)
            {
                byte[] result;
                using (var ms = new MemoryStream())
                {
                    var bodyWriter = XmlDictionaryWriter.CreateBinaryWriter(ms);
                    message.WriteBodyContents(bodyWriter);
                    bodyWriter.Flush();
                    ms.Position = 0;
                    result = ms.ToArray();
                    bodyWriter.Close();
                }
                return result;
            }

            /// <summary>
            /// ������� ��������� �� ������ �������
            /// </summary>
            private static Message CreateMessage(Message prototype, XmlReader body)
            {
                var msg = Message.CreateMessage(prototype.Version, null, body);
                msg.Headers.CopyHeadersFrom(prototype);
                msg.Properties.CopyProperties(prototype.Properties);

                return msg;
            }

            /// <summary>
            /// ����� ���������
            /// </summary>
            private static Message CompressMessage(Message message)
            {
                // ������ ������ ��������� ���������
                byte[] data = GZipCompressor.Compress(GetBodyContents(message));

                // ������������ ������ ����������� ���������
                var ms = new MemoryStream();
                
                var bodyWriter = XmlDictionaryWriter.CreateBinaryWriter(ms);
                bodyWriter.WriteStartElement("CompressedData");
                bodyWriter.WriteAttributeString("Algorithm", "gzip");
                bodyWriter.WriteBase64(data, 0, data.Length);
                bodyWriter.WriteEndElement();
                bodyWriter.Flush();
                ms.Position = 0;
                return CreateMessage(message, XmlDictionaryReader.CreateBinaryReader(ms, XmlDictionaryReaderQuotas.Max));
                
            }

            /// <summary>
            /// ����������� ���������
            /// </summary>
            private static Message DecompressMessage(Message message)
            {
                // ���������� ������ ��������� ���������
                var bodyReader = XmlDictionaryReader.CreateBinaryReader(GetBodyContents(message), XmlDictionaryReaderQuotas.Max);
                bodyReader.MoveToStartElement();
                byte[] data = GZipCompressor.Decompress(bodyReader.ReadElementContentAsBase64());

                return CreateMessage(message, XmlDictionaryReader.CreateBinaryReader(data, XmlDictionaryReaderQuotas.Max));
            }


            #region IDispatchMessageInspector Members

            /// <summary>
            /// ����� ��������� �������
            /// </summary>
            public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
            {
                if ((_compress & Compress.Request) == Compress.Request)
                {
                    request = DecompressMessage(request);
                }

                return null;
            }

            /// <summary>
            /// ����� ��������� ������
            /// </summary>
            public void BeforeSendReply(ref Message reply, object correlationState)
            {
                if ((_compress & Compress.Reply) == Compress.Reply)
                {
                    reply = CompressMessage(reply);
                }
            }

            #endregion

            #region IClientMessageInspector Members

            /// <summary>
            /// ����� ��������� ������
            /// </summary>
            public void AfterReceiveReply(ref Message reply, object correlationState)
            {
                if ((_compress & Compress.Reply) == Compress.Reply)
                {
                    reply = DecompressMessage(reply);
                }
            }

            /// <summary>
            /// ����� ��������� �������
            /// </summary>
            public object BeforeSendRequest(ref Message request, IClientChannel channel)
            {
                if ((_compress & Compress.Request) == Compress.Request)
                {
                    request = CompressMessage(request);
                }

                return null;
            }

            #endregion
        }


        /// <summary>
        /// ����������� �� ���������
        /// </summary>
        public MessageCompressionAttribute()
            : this(Compress.None)
        { }

        /// <summary>
        /// �����������
        /// </summary>
        /// <param name="compress">����������� ������</param>
        public MessageCompressionAttribute(Compress compress)
        {
            _compress = compress;
        }

        /// <summary>
        /// ����������� ������
        /// </summary>
        private readonly Compress _compress;
        /// <summary>
        /// ����������� ������
        /// </summary>
        public Compress Compress
        {
            get { return _compress; }
        }


        #region IEndpointBehavior Members

        void IEndpointBehavior.AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        { }

        void IEndpointBehavior.ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.MessageInspectors.Add(new MessageInspector(Compress));
        }

        void IEndpointBehavior.ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
            endpointDispatcher.DispatchRuntime.MessageInspectors.Add(new MessageInspector(Compress));
        }

        void IEndpointBehavior.Validate(ServiceEndpoint endpoint)
        { }

        #endregion

        #region IServiceBehavior Members

        void IServiceBehavior.AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
        { }

        void IServiceBehavior.ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            foreach (var currentDispatcher in serviceHostBase.ChannelDispatchers)
            {
                foreach (var endpoint in ((ChannelDispatcher)currentDispatcher).Endpoints)
                {
                    endpoint.DispatchRuntime.MessageInspectors.Add(new MessageInspector(Compress));
                }
            }
        }

        void IServiceBehavior.Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        { }

        #endregion
    }
}