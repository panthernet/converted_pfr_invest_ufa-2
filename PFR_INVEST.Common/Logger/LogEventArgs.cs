﻿using System;

namespace PFR_INVEST.Common.Logger
{
    public class LogEventArgs : EventArgs
    {
        /// <summary>
        /// Создает экземпляр класса типа <see cref="LogEventArgs"/>
        /// </summary>
        /// <param name="severity"></param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        /// <param name="date"></param>
		public LogEventArgs(LogSeverity severity, string message,
                            Exception exception, DateTime date):
			this(severity, message, exception, date, null)
		{
		}

        public LogEventArgs(LogSeverity severity, string message,
                            Exception exception, DateTime date, object sender)
        {
            Severity = severity;
            Message = message;
            Exception = exception;
            Date = date;
			Sender = sender;
        }

        /// <summary>
        /// Уровень (тип) ошибки.
        /// </summary>        
        public LogSeverity Severity { get; set; }

        /// <summary>
        /// Сообщение об ошибке.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Информация о вложенной ошибке (inner exception).
        /// </summary>
        public Exception Exception { get; set; }

        /// <summary>
        /// Дата и время записи лога.
        /// </summary>
        public DateTime Date { get; set; }

        public String SeverityString
        {
            get { return Severity.ToString("G"); }
        }

        /// <summary>
        /// Строковое представление объекта типа <see cref="LogEventArgs"/>.
        /// </summary>
        /// <returns>Строковое представление объекта типа <see cref="LogEventArgs"/>.</returns>
        public override String ToString()
        {
            return String.Format("{0} - {1} - {2} - {3}", Date, SeverityString, Message, Exception);
        }

		public object Sender { get; set; }
	}
}
