﻿using System.IO;

namespace PFR_INVEST.Common.Logger
{
    /// <summary>
    /// Класс для записи информации об ошибках в файл.
    /// </summary>
    /// <remarks>
    /// Паттерн: Наблюдатель (Observer).
    /// </remarks>
    public class ObserverLogToFile : ILog
    {
        private readonly string _fileName;

        /// <summary>
        /// Создает экземпляр класса типа <see cref="ObserverLogToFile"/>.
        /// </summary>
        /// <param name="fileName">Имя файла для записи лога.</param>
        public ObserverLogToFile(string fileName)
        {
            _fileName = fileName;
        }

        /// <summary>
        /// Осуществляет запись информации об ошибке в файл.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Log(object sender, LogEventArgs e)
        {
            string message = string.Format("[{0}] {1}: {2}", e.Date, e.SeverityString, e.Message);
            if (e.Exception != null)
                message = System.Environment.NewLine + string.Format("[{0}] {1}: Details: {2}", e.Date, e.SeverityString, e);

            FileStream fileStream;

            try
            {
                fileStream = new FileStream(_fileName, FileMode.Append);
            }
            catch (DirectoryNotFoundException)
            {
                Directory.CreateDirectory((new FileInfo(_fileName)).DirectoryName);
                fileStream = new FileStream(_fileName, FileMode.Append);
            }

            var writer = new StreamWriter(fileStream);
            try
            {
                writer.WriteLine(message);
            }
            catch
            {
                // тут ничего не делаем
            }
            finally
            {
                try
                {
                    writer.Close();
                }
                catch
                {
                    // тут ничего не делаем
                }
            }
        }
    }
}