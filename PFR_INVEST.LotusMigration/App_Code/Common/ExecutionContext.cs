﻿namespace PFR_INVEST.DBUtils
{

    public class ExecutionContext
    {
        public string ConnectionString { get; private set; }

        public string SchemaName { get; private set; }

        public ExecutionContext(string connectionString, string schemaName)
        {
            this.ConnectionString = connectionString;
            this.SchemaName = schemaName;
        }
    }
}
