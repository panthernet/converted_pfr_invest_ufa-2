﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DBUtils;
using System.Data.Odbc;
using System.IO;
using PFR_INVEST.Core.Logger;
using PFR_INVEST.LotusMigration.App_Code.Common;
using PFR_INVEST.LotusMigration.App_Code.DataItems;

namespace PFR_INVEST.LotusMigration.App_Code
{
    public abstract class ScriptHandlerBase : IActionHandler
    {
        public string ConnectionString;
        public OdbcConnection OdbcConnection;

        public abstract string ActionName { get; }

        public bool Execute()
        {
            bool result = true;
            using (OdbcConnection odbcConnection = new OdbcConnection(ConnectionString))
            {
                try
                {
                    odbcConnection.Open();
                }
                catch (Exception ex)
                {
                    Logger.Instance.Error("Error. " + ex.Message);
                    return false;
                }

                try
                {
                    OdbcConnection = odbcConnection;
                    ConnectionManager.Connection = odbcConnection;

                    Process();
                }
                finally
                {
                    OdbcConnection = null;
                    ConnectionManager.Connection = null;
                }

            }
            return result;
        }


        public virtual void Process()
        {
            throw new NotImplementedException();
        }

        public virtual void SaveToFile(string FilePath, string value)
        {
            //System.IO.File.WriteAllText(FilePath,value);

            using (StreamWriter writer = new StreamWriter(FilePath, false, Encoding.UTF8))
            {
                writer.Write(value);
            }
        }


        public virtual T GetTableInfo<T>(string SchemaName, string TableName) where T : TableInfoBase, new()
        {
            T res = new T() { Name = TableName, Schema = SchemaName };


            string commandText =
    string.Format(@"
select 
ColNo, ColName, Remarks, TypeName, Length, Scale, Identity, Nulls

from syscat.columns 
where tabschema = '{0}' and tabname ='{1}'
order by colno
", SchemaName, TableName);

            FillTableInfo<T>(commandText, res);
            return res;
        }

        public virtual void FillTableInfo<T>(string commandText, T res) where T : TableInfoBase
        {
            DataItemBase.ReadAllRecords(commandText, delegate(OdbcDataReader reader)
            {
                res.AddCol(reader);
            });
        }

    }
}
