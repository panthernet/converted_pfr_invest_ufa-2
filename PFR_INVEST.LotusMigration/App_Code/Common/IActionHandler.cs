﻿// -----------------------------------------------------------------------
// <copyright file="IDBUtil.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace PFR_INVEST.DBUtils
{
    using PFR_INVEST.Core.Logger;

    public interface IActionHandler
    {
        bool Execute();
        string ActionName {get;}

    }
}
