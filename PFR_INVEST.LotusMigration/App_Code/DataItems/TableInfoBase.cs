﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Odbc;
using PFR_INVEST.LotusMigration.App_Code.DataItems;
using System.Xml;

namespace PFR_INVEST.LotusMigration.App_Code.Common
{
    public class TableInfoBase
    {
        public enum enStatus
        {
            Empty = -1,
            NotMapped = 0,
            PartiallyMapped = 50,
            Completed = 100
        }


        public string Name;
        public string Schema;

        public List<ColInfoBase> Cols;

        public ColInfoBase IdentityCol;


        public enStatus Status;
        public string Comment;
        public string Comment2;
        public bool IsIgnore;

        public bool IsPrintMissingScript;

        public string PrintMissingIdScript_TableName;

        public string Remarks;


        public int RowCountLotusID = 0;
        public int RowCountLotusTable = 0;
        public int RowCountLotusGUID = 0;


        public TableInfoBase()
        {
            Cols = new List<ColInfoBase>();
        }

        internal void AddCol(OdbcDataReader reader)
        {
            ColInfoBase r = new ColInfoBase();

            r.ColNo = (short)reader["ColNo"];
            r.ColName = (string)reader["COLNAME"];
            r.Remarks = (string)IsDbNull(reader["REMARKS"], null);

            r.TypeName = (string)reader["TypeName"];
            r.Length = (int)reader["Length"];
            r.Scale = (short)reader["Scale"];

            r.IsIdentity = (string)reader["Identity"] == "Y";
            r.IsNullable = (string)reader["NULLS"] == "Y";

            Cols.Add(r);

            if (r.IsIdentity)
                IdentityCol = r;
        }

        public static object IsDbNull(object value, object defaultValue)
        {
            if (value == DBNull.Value)
                return defaultValue;

            return value;
        }

        public bool HasRowType(ColInfoBase.enType dataType)
        {
            return Cols.Exists(p => p.DataType == dataType && !p.IsServiceRowLotus && !p.IsServiceRowPfr);
        }


        public virtual string GetInsertRows(ColInfoBase.enType? dataType, string typeName)
        {
            StringBuilder sb = new StringBuilder();

            if (dataType != null)
            {
                if (!HasRowType(dataType.Value))
                    return "";

                sb.AppendLine();
                sb.AppendFormat("     -- {0}\r\n", typeName);
            }

            foreach (var r in Cols)
            {
                if (r.IsServiceRowPfr)
                    continue;

                if (dataType != null && dataType.Value != r.DataType)
                    continue;

                if (r.IsNullable)
                    sb.AppendFormat("--, {0}      -- {1}", r.ColName, r.Remarks);
                else
                    sb.AppendFormat(", {0}   -- NOT NULL -- {1}", r.ColName, r.Remarks);

                sb.AppendLine();
            }

            return sb.ToString();
        }



        public string GetSelectRows(ColInfoBase.enType? dataType, string typeName)
        {
            StringBuilder sb = new StringBuilder();

            if (dataType != null)
            {
                if (!HasRowType(dataType.Value))
                    return "";

                sb.AppendLine();
                sb.AppendFormat("     -- {0}\r\n", typeName);
            }

            foreach (var r in Cols)
            {
                if (r.IsServiceRowLotus)
                    continue;

                if (dataType != null && dataType.Value != r.DataType)
                    continue;

                sb.AppendFormat("--, {0}      -- {1}", r.ColName, r.Remarks);

                sb.AppendLine();
            }

            return sb.ToString();
        }

        public virtual string GetMatchText(OdbcConnection OdbcConnection, string searchString)
        {
            foreach (var c in Cols)
                c.MatchCount = 0;

            FindMatch(OdbcConnection, searchString);

            if (!Cols.Exists(p => p.MatchCount > 0))//No matches found
                return null;

            StringBuilder sb = new StringBuilder();

            sb.AppendLine();
            sb.AppendLine();
            sb.AppendFormat("Table: {0}", Name);
            sb.AppendLine();
            sb.AppendLine();
            foreach (ColInfoBase r in Cols.Where(p => p.MatchCount > 0))
            {
                sb.AppendFormat("{0} : {1}   -- {2}", r.ColName, r.MatchCount, r.Remarks);
                sb.AppendLine();
            }
            sb.AppendLine();

            return sb.ToString();
        }

        public virtual void FindMatch(OdbcConnection OdbcConnection, string searchString)
        {
            List<String> sL = new List<string>();


            foreach (var c in Cols)
            {
                if (c.IsIdentity)
                    continue;

                //if (c.DataType == ColInfoBase.enType.date || 
                //    c.DataType == ColInfoBase.enType.time || 
                //    c.DataType== ColInfoBase.enType.integer || 
                //    c.DataType== ColInfoBase.enType.bigint || 
                //    c.DataType== ColInfoBase.enType.decimalType )
                //    continue;

                if (c.DataType != ColInfoBase.enType.varchar)
                    continue;

                string s = string.Format(@"
select count(*) as matchCount, cast('{2}' as varchar(100)) as colName
from ""{0}"".""{1}""
where ""{2}""='{3}'
having count(*)>0
", this.Schema, this.Name, c.ColName, searchString);

                sL.Add(s);
            }


            string commandText = string.Join("\r\n UNION \r\n", sL); ;

            DataItemBase.ReadAllRecords(commandText, AddMatch);
        }

        private void AddMatch(OdbcDataReader reader)
        {
            string colName = ((string)reader["COLNAME"]).ToUpper().Trim();
            int matchCount = (int)IsDbNull(reader["matchCount"], null);

            ColInfoBase r = Cols.First(p => p.ColName.ToUpper().Trim() == colName);

            r.MatchCount = matchCount;
        }

        internal string GetMatchTextLike(OdbcConnection OdbcConnection, string searchString)
        {
            foreach (var c in Cols)
                c.MatchCount = 0;

            FindMatchLike(OdbcConnection, searchString);

            if (!Cols.Exists(p => p.MatchCount > 0))//No matches found
                return null;

            StringBuilder sb = new StringBuilder();

            sb.AppendLine();
            sb.AppendLine();
            sb.AppendFormat("Table: {0}", Name);
            sb.AppendLine();
            sb.AppendLine();
            foreach (ColInfoBase r in Cols.Where(p => p.MatchCount > 0))
            {
                sb.AppendFormat("{0} : {1}   -- {2}", r.ColName, r.MatchCount, r.Remarks);
                sb.AppendLine();
            }
            sb.AppendLine();

            return sb.ToString();
        }

        private void FindMatchLike(OdbcConnection OdbcConnection, string searchString)
        {
            List<String> sL = new List<string>();

            foreach (var c in Cols)
            {
                if (c.IsIdentity)
                    continue;

                if (c.DataType != ColInfoBase.enType.varchar)
                    continue;

                string s = string.Format(@"
select count(*) as matchCount, cast('{2}' as varchar(100)) as colName
from ""{0}"".""{1}""
where lower(""{2}"") like '{3}'
having count(*)>0
", this.Schema, this.Name, c.ColName, searchString.ToLower());

                sL.Add(s);
            }

            string commandText = string.Join("\r\n UNION \r\n", sL); ;

            DataItemBase.ReadAllRecords(commandText, AddMatch);
        }



        internal string GetMatchTextIn(OdbcConnection OdbcConnection, List<string> searchStringList)
        {
            foreach (var c in Cols)
                c.MatchCount = 0;

            FindMatchIn(OdbcConnection, searchStringList);

            if (!Cols.Exists(p => p.MatchCount > 0))//No matches found
                return null;

            StringBuilder sb = new StringBuilder();

            sb.AppendLine();
            sb.AppendLine();
            sb.AppendFormat("Table: {0}", Name);
            sb.AppendLine();
            sb.AppendLine();
            foreach (ColInfoBase r in Cols.Where(p => p.MatchCount > 0))
            {
                sb.AppendFormat("{0} : {1}   -- {2}", r.ColName, r.MatchCount, r.Remarks);
                sb.AppendLine();
            }
            sb.AppendLine();

            return sb.ToString();
        }

        private void FindMatchIn(OdbcConnection OdbcConnection, List<string> searchStringList)
        {
            List<String> sL = new List<string>();

            string searchString = string.Join(", ", searchStringList.Select(p => string.Format("'{0}'", p.Replace("'", "''"))));

            searchString = searchString.ToLower();

            foreach (var c in Cols)
            {
                if (c.IsIdentity)
                    continue;

                if (c.DataType != ColInfoBase.enType.varchar)
                    continue;

                string s = string.Format(@"
select count(*) as matchCount, cast('{2}' as varchar(100)) as colName
from ""{0}"".""{1}""
where lower(""{2}"") in ({3})
having count(*)>0
", this.Schema, this.Name, c.ColName, searchString);

                sL.Add(s);
            }

            string commandText = string.Join("\r\n UNION \r\n", sL); ;

            DataItemBase.ReadAllRecords(commandText, AddMatch);
        }


        internal string GetMatchTextDecimal(OdbcConnection OdbcConnection, string searchString)
        {
            foreach (var c in Cols)
                c.MatchCount = 0;

            FindMatchDecimal(OdbcConnection, searchString);

            if (!Cols.Exists(p => p.MatchCount > 0))//No matches found
                return null;

            StringBuilder sb = new StringBuilder();

            sb.AppendLine();
            sb.AppendLine();
            sb.AppendFormat("Table: {0}", Name);
            sb.AppendLine();
            sb.AppendLine();
            foreach (ColInfoBase r in Cols.Where(p => p.MatchCount > 0))
            {
                sb.AppendFormat("{0} : {1}   -- {2}", r.ColName, r.MatchCount, r.Remarks);
                sb.AppendLine();
            }
            sb.AppendLine();

            return sb.ToString();
        }


        private void FindMatchDecimal(OdbcConnection OdbcConnection, string searchString)
        {
            List<String> sL = new List<string>();


            foreach (var c in Cols)
            {
                if (c.IsIdentity)
                    continue;

                //if (c.DataType == ColInfoBase.enType.date || 
                //    c.DataType == ColInfoBase.enType.time || 
                //    c.DataType== ColInfoBase.enType.integer || 
                //    c.DataType== ColInfoBase.enType.bigint || 
                //    c.DataType== ColInfoBase.enType.decimalType )
                //    continue;

                if (c.DataType != ColInfoBase.enType.decimalType)
                    continue;

                string s = string.Format(@"
select count(*) as matchCount, cast('{2}' as varchar(100)) as colName
from ""{0}"".""{1}""
where ""{2}""= '{3}'
having count(*)>0
", this.Schema, this.Name, c.ColName, searchString);

                sL.Add(s);
            }


            string commandText = string.Join("\r\n UNION \r\n", sL); ;

            DataItemBase.ReadAllRecords(commandText, AddMatch);
        }


        internal string GetMatchTextDate(OdbcConnection OdbcConnection, string searchString)
        {
            foreach (var c in Cols)
                c.MatchCount = 0;

            FindMatchDate(OdbcConnection, searchString);

            if (!Cols.Exists(p => p.MatchCount > 0))//No matches found
                return null;

            StringBuilder sb = new StringBuilder();

            sb.AppendLine();
            sb.AppendLine();
            sb.AppendFormat("Table: {0}", Name);
            sb.AppendLine();
            sb.AppendLine();
            foreach (ColInfoBase r in Cols.Where(p => p.MatchCount > 0))
            {
                sb.AppendFormat("{0} : {1}   -- {2}", r.ColName, r.MatchCount, r.Remarks);
                sb.AppendLine();
            }
            sb.AppendLine();

            return sb.ToString();
        }


        private void FindMatchDate(OdbcConnection OdbcConnection, string searchString)
        {
            List<String> sL = new List<string>();


            foreach (var c in Cols)
            {
                if (c.IsIdentity)
                    continue;

                //if (c.DataType == ColInfoBase.enType.date || 
                //    c.DataType == ColInfoBase.enType.time || 
                //    c.DataType== ColInfoBase.enType.integer || 
                //    c.DataType== ColInfoBase.enType.bigint || 
                //    c.DataType== ColInfoBase.enType.decimalType )
                //    continue;

                if (c.DataType != ColInfoBase.enType.date)
                    continue;

                string s = string.Format(@"
select count(*) as matchCount, cast('{2}' as varchar(100)) as colName
from ""{0}"".""{1}""
where ""{2}""= '{3}'
having count(*)>0
", this.Schema, this.Name, c.ColName, searchString);

                sL.Add(s);
            }


            string commandText = string.Join("\r\n UNION \r\n", sL); ;

            DataItemBase.ReadAllRecords(commandText, AddMatch);
        }


        public virtual void GetRowCount()
        {
            RowCount = FindRowCount(ConnectionManager.Connection);
        }

        public virtual void GetRowCount(OdbcConnection OdbcConnection)
        {
            RowCount = FindRowCount(OdbcConnection);
        }

        public virtual int FindRowCount(OdbcConnection OdbcConnection)
        {
            List<String> sL = new List<string>();

            string s = string.Format(@"
select count(*) as matchCount
from ""{0}"".""{1}""
", this.Schema, this.Name);

            sL.Add(s);

            string commandText = string.Join("\r\n UNION \r\n", sL); ;

            DataItemBase.ReadAllRecords(commandText, SetRowCount);

            return _RowCount;
        }


        public virtual void LoadNullLotusRowCount()
        {
            LoadNullLotusRowCount(ConnectionManager.Connection);
        }

        public virtual void LoadNullLotusRowCount(OdbcConnection OdbcConnection)
        {

            List<String> sL = new List<string>();

            string s = string.Format(@"
select count(*) as COUNT_ID,

(select count(*) as matchCount
from ""{0}"".""{1}""
where Lotus_TABLE is null) as COUNT_TABLE,

(select count(*) as matchCount
from ""{0}"".""{1}""
where Lotus_GUID is null) as COUNT_GUID

from ""{0}"".""{1}""
where Lotus_ID is null
", this.Schema, this.Name);

            sL.Add(s);

            string commandText = string.Join("\r\n UNION \r\n", sL); ;

            DataItemBase.ReadAllRecords(commandText,
                 delegate(OdbcDataReader reader)
                 {
                     RowCountLotusID = (int)reader["COUNT_ID"];
                     RowCountLotusTable = (int)reader["COUNT_TABLE"];
                     RowCountLotusGUID = (int)reader["COUNT_GUID"];
                 });
        }

        private void SetRowCount(OdbcDataReader reader)
        {
            _RowCount = (int)IsDbNull(reader["matchCount"], null);
        }

        private int _RowCount;

        public int RowCount;


        public bool IsMigratedTable()
        {
            return (!IsIgnore) && Cols.Exists(p => p.ColName == "LOTUS_ID") && Cols.Exists(p => p.ColName == "LOTUS_TABLE");
        }

        public bool IsMigratedTableGuid()
        {
            return (!IsIgnore) && Cols.Exists(p => p.ColName == "LOTUS_ID") && Cols.Exists(p => p.ColName == "LOTUS_TABLE") && Cols.Exists(p => p.ColName == "LOTUS_GUID");
        }

        public bool IsMigratedTableNone()
        {
            return (!IsIgnore) &&
            (!Cols.Exists(p => p.ColName == "LOTUS_ID") && !Cols.Exists(p => p.ColName == "LOTUS_TABLE") && !Cols.Exists(p => p.ColName == "LOTUS_GUID"));
        }

        public bool IsNullLotus()
        {
            return (!IsIgnore) && (RowCountLotusID > 0 || RowCountLotusTable > 0 || RowCountLotusGUID > 0);
        }

        internal void SelectMapData()
        {
            MapIndex = new Dictionary<string, int>();

            string cmd = string.Format(@"
select count(*) as count, ""LOTUS_TABLE""
from ""{0}"".""{1}""
group by ""LOTUS_TABLE""
"
               , Schema, Name);

            DataItemBase.ReadAllRecords(cmd, delegate(OdbcDataReader reader)
            {
                int n = (int)reader["COUNT"];
                string s = (string)IsDbNull(reader["LOTUS_TABLE"], "[NULL]");
                AddMapIndex(s, n);
            });
        }

        public Dictionary<string, int> MapIndex;

        public void AddMapIndex(string tableName, int count)
        {
            MapIndex.Add(tableName, count);
        }

        internal bool IsFullMapped()
        {
            return MapIndex.Count == 1 && MapIndex.First().Value == RowCount;
        }

        internal void SetSettings(XmlElement x)
        {
            if (x == null)
                return;

            SetStatus(x.GetAttribute("status"));
            SetComment(x.GetAttribute("comment"));
            SetComment2(x.GetAttribute("comment2"));
            SetIsIgnore(x.GetAttribute("ignore"));

            SetIsPrintMissingScript(x.GetAttribute("printMissingScript"));
            SetIsPrintMissingIdScript(x.GetAttribute("printMissingIdScript"));
        }

        private void SetStatus(string s)
        {
            switch (s)
            {
                case "50": Status = enStatus.PartiallyMapped; break;
                case "100": Status = enStatus.Completed; break;

                case null:
                case "":
                case "0":
                default:
                    Status = enStatus.NotMapped;
                    break;
            }
        }

        private void SetComment(string s)
        {
            Comment = s;
        }

        private void SetComment2(string s)
        {
            Comment2 = s;
        }

        private void SetIsIgnore(string s)
        {
            bool b = s == "1";

            IsIgnore = b;
        }

        private void SetIsPrintMissingScript(string s)
        {
            bool b = s == "1";

            IsPrintMissingScript = b;
        }

        private void SetIsPrintMissingIdScript(string s)
        {
            PrintMissingIdScript_TableName = s.Trim();
        }

        internal List<long> GetNotMappedIdList()
        {
            List<long> res = new List<long>();


            if (MapIndex.Count == 0)
                return null;

            string cmd = string.Format(@"
select ID
from ""{0}"".""{1}""
where ID not in 
(
select Lotus_ID from PFR_BASIC.""{2}""
where lotus_table = '{1}'
)
"
               , Schema, Name, PrintMissingIdScript_TableName);

            DataItemBase.ReadAllRecords(cmd, delegate(OdbcDataReader reader)
            {
                long id = (long)reader["ID"];
                res.Add(id);
            });

            return res;
        }


        internal void AppendGuidScript(StringBuilder sb, string SchemaFromName)
        {
            List<string> sL = new List<string>();

            string cmd = string.Format(@"
SELECT distinct
      LOTUS_TABLE
from ""{0}"".""{1}""
		WHERE 
      LOTUS_TABLE is not null
"
   , Schema.ToUpper(), Name.ToUpper());

            DataItemBase.ReadAllRecords(cmd, delegate(OdbcDataReader reader)
            {
                string s = (string)IsDbNull(reader["LOTUS_TABLE"], null);
                if (!string.IsNullOrEmpty(s))
                    sL.Add(s);
            });



            if (sL.Count == 0)
                return;


            sb.AppendLine();

            foreach (var s in sL)
            {
                sb.AppendFormat("Call PFR_LOTUS.Set_Lotus_Guid('{0}', '{1}');", s.ToUpper(), Name.ToUpper());
                sb.AppendLine();
            }

            sb.AppendLine();
        }
    
    
    
    
    }
}

