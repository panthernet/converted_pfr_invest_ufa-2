﻿
namespace PFR_INVEST.DBUtils
{
    using System;
    using System.Collections.Generic;
    using System.Data.Odbc;
    using System.Text;
    using PFR_INVEST.Core.Logger;
    using System.Configuration;
    using System.IO;
    using PFR_INVEST.LotusMigration.App_Code;
    using PFR_INVEST.LotusMigration.App_Code.Common;

    using System.Linq;
    using System.Xml;

    public class Audit2AllTablesHandler : ScriptHandlerBase
    {
        public override string ActionName
        {
            get { return "AuditAllTables"; }
        }


        private string[] Args;

        public string SchemaNameLotus;
        public string SchemaNameBasic;

        public string XmlFileName;
        public string XmlFilePath;

        public string FileName;
        public string FilePath;

        public Audit2AllTablesHandler(string connectionString, string[] args)
        {
            ConnectionString = connectionString;
            Args = args;

            SchemaNameLotus = ConfigurationManager.AppSettings.Get("LotusSchemeName");
            SchemaNameBasic = ConfigurationManager.AppSettings.Get("PfrSchemeName");


            if (args.Length < 3)
                throw new ArgumentNullException();


            XmlFileName = args[1];
            XmlFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, XmlFileName);

            FileName = args[2];
            FilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, FileName);
        }

        public override void Process()
        {
            Console.WriteLine("Загрузка таблиц");

            Logger.Instance.Info("Выборка данных о таблицах Pfr_Lotus");
            SchemaInfoBase sLotus = SchemaInfoBase.GetSchemaAllTables(SchemaNameLotus);

            Logger.Instance.Info("Выборка данных о таблицах Pfr_Basic");
            SchemaInfoBase sBasic = SchemaInfoBase.GetSchemaAllTables(SchemaNameBasic);

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Tables found");
            sb.AppendFormat("PFR_BASIC - {0} Tables", sBasic.Tables.Count);
            sb.AppendLine();
            sb.AppendFormat("PFR_LOTUS - {0} Tables", sLotus.Tables.Count);
            sb.AppendLine();

            Console.WriteLine("Загрузка комментариев");

            sLotus.LoadTableRemarks();
            sBasic.LoadTableRemarks();

            Console.WriteLine("Загрузка настроек");
            LoadTableSettings(sLotus, sBasic);


            Logger.Instance.Info("Аудит таблиц");



            Console.WriteLine("Загрузка числа строк в таблицах");
            sBasic.LoadRowCount();
            sLotus.LoadRowCount();


            sb.AppendLine();
            sb.AppendLine("Таблицы PFR_BASIC, пропущенные таблицы:");
            sb.AppendLine();
            PrintTables(sb, sBasic, IsIgnore);
            sBasic.FilterTables(IsIgnore);// Don't need this tables any more


            sb.AppendLine();
            sb.AppendLine("Таблицы PFR_LOTUS, пропущенные таблицы:");
            sb.AppendLine();
            PrintTables(sb, sLotus, IsIgnore);
            sLotus.FilterTables(IsIgnore);// Don't need this tables any more


            sb.AppendLine();
            sb.AppendLine("======  PFR_BASIC  Completed ======================================");
            sb.AppendLine();
            PrintTables(sb, sBasic, IsCompleted);

            sb.AppendLine();
            sb.AppendLine("======  PFR_LOTUS  Completed ======================================");
            sb.AppendLine();
            PrintTables(sb, sLotus, IsCompleted);
            sLotus.FilterTables(IsCompleted);


            sb.AppendLine();
            sb.AppendLine("Таблицы PFR_BASIC, в которых отсутствуют колонки Lotus_ID, Lotus_Table:");
            sb.AppendLine();
            //PrintNoImportTables(sb, sBasic);
            PrintTables(sb, sBasic, delegate(TableInfoBase t)
            {
                if (t.Status == TableInfoBase.enStatus.Completed) // Завершенные таблицы тут не показываем
                    return false;
                return IsNotMigratedTable(t);
            });

            sBasic.FilterTables(IsNotMigratedTable);// Don't need this tables any more

            Console.WriteLine("Загрузка Маппинга");
            LoadMapping(sBasic, sLotus);

            sBasic.FilterTables(IsCompleted); // Отфильтровываем завершенные таблицы. Раньше нельзя

            sb.AppendLine();
            sb.AppendLine("-----  PFR_BASIC  ---------------------------------------");
            sb.AppendLine();
            PrintMapped(sb, sBasic);


            


            sb.AppendLine("======  PFR_LOTUS  empty (пустые таблицы) ======================================");
            sb.AppendLine();
            PrintMapped(sb, sLotus, delegate(TableInfoBase t)
            {
                if (t.RowCount== 0)
                    return true;
                return false;
            });
            sLotus.FilterTables(delegate(TableInfoBase t)
            {
                if (t.RowCount == 0)
                    return true;
                return false;
            });


            sb.AppendLine("======  PFR_LOTUS  almost empty (таблицы с записями < 10) ======================================");
            sb.AppendLine();
            PrintMapped(sb, sLotus, delegate(TableInfoBase t)
            {
                if (t.RowCount < 10)
                    return true;
                return false;
            });
            sLotus.FilterTables(delegate(TableInfoBase t)
            {
                if (t.RowCount < 10)
                    return true;
                return false;
            });


            sb.AppendLine("======  PFR_LOTUS  no mapping ======================================");
            sb.AppendLine();
            PrintMapped(sb, sLotus, delegate(TableInfoBase t)
            {
                if (t.MapIndex.Count == 0)
                    return true;
                return false;
            });

            sb.AppendLine();
            sb.AppendLine("======  PFR_LOTUS  full  ======================================");
            sb.AppendLine();
            PrintMapped(sb, sLotus, delegate(TableInfoBase t)
            {
                if (t.IsFullMapped())
                    return true;

                return false;
            });
            

            sb.AppendLine();
            sb.AppendLine("======  PFR_LOTUS  partial  ======================================");
            sb.AppendLine();
            PrintMapped(sb, sLotus, delegate(TableInfoBase t)
            {
                if (t.MapIndex.Count == 0) //Empty 
                    return false;

                if (t.IsFullMapped())
                    return false;
                return true;
            });

            SaveToFile(FilePath, sb.ToString());

            Logger.Instance.Info("Готово");
            Console.WriteLine("Готово");
        }

        private void LoadMapping(SchemaInfoBase sBasic, SchemaInfoBase sLotus)
        {
            foreach (var t in sBasic.Tables)
            {
                t.SelectMapData();
            }

            sLotus.ApplyMapData(sBasic);
        }

        private bool IsCompleted(TableInfoBase t)
        {
            if (t.Status == TableInfoBase.enStatus.Completed)
                return true;
            return false;
        }

        private bool IsNotMigratedTable(TableInfoBase t)
        {
            if (!t.IsMigratedTable())
                return true;

            return false;
        }

        private bool IsIgnore(TableInfoBase t)
        {
            if (t.IsIgnore)
                return true;

            return false;
        }

        private void LoadTableSettings(SchemaInfoBase sLotus, SchemaInfoBase sBasic)
        {
            Logger.Instance.Info("Загрузка шаблона XML");


            XmlDocument doc = new XmlDocument();
            doc.Load(XmlFilePath);

            XmlElement settings = doc["settings"];

            foreach (XmlElement s in settings.SelectNodes("scheme"))
            {
                string sName = s.GetAttribute("name");
                if (sName == "PFR_LOTUS")
                {
                    sLotus.SetSettings(s);
                }

                if (sName == "PFR_BASIC")
                {
                    sBasic.SetSettings(s);
                }
            }
        }


        public delegate bool ConditionHandler(TableInfoBase t);

        private void PrintTables(StringBuilder sb, SchemaInfoBase s, ConditionHandler selectCondition)
        {
            int n = 0;

            foreach (var t in s.Tables)
            {
                if (!selectCondition(t))
                    continue;

                sb.AppendFormat("{0} - {1}", t.Name, t.Remarks);
                sb.AppendLine();

                if (!string.IsNullOrEmpty(t.Comment))
                    sb.AppendLine(t.Comment);
                if (!string.IsNullOrEmpty(t.Comment2))
                    sb.AppendLine(t.Comment2);


                sb.AppendFormat("  {0} Rows", t.RowCount);
                sb.AppendLine();

                n++;
            }
            sb.AppendLine();
            sb.AppendFormat("###############  Count : {0}", n);
            sb.AppendLine();
        }


        private void PrintMapped(StringBuilder sb, SchemaInfoBase s)
        {
            PrintMapped(sb, s, delegate(TableInfoBase t)
            {
                return true;
            });
        }

        private void PrintMapped(StringBuilder sb, SchemaInfoBase s, ConditionHandler selectCondition)
        {
            int n = 0;

            foreach (var t in s.Tables)
            {
                if (!selectCondition(t))
                    continue;

                sb.AppendFormat("{0} - {1}", t.Name, t.Remarks);
                sb.AppendLine();

                if (!string.IsNullOrEmpty(t.Comment))
                    sb.AppendLine(t.Comment);
                if (!string.IsNullOrEmpty(t.Comment2))
                    sb.AppendLine(t.Comment2);


                sb.AppendFormat("{0} Records", t.RowCount);
                sb.AppendLine();

                if (t.MapIndex.Count > 0)
                {
                    sb.AppendLine("Mapping");
                    foreach (var x in t.MapIndex)
                    {
                        sb.AppendFormat("{0} - {1}", x.Key ?? "[NULL]", x.Value);
                        sb.AppendLine();
                    }
                }

                if (t.IsPrintMissingScript)//For lotus tables only
                {
                    foreach (var x in t.MapIndex)
                    {
                        sb.AppendFormat(
    @"
select ts.* from 
PFR_LOTUS.""{0}""   ts
left join PFR_BASIC.""{1}""  td
on td.LOTUS_ID = ts.ID
and td.LOTUS_TABLE = '{0}'
where td.id is null
", t.Name, x.Key);
                    }
                }

                if (!string.IsNullOrEmpty(t.PrintMissingIdScript_TableName))//For lotus tables only 
                {
                    List<long> iL = t.GetNotMappedIdList();

                    if (iL == null)//No mapping
                    {
                        sb.AppendLine("No Migrated Rows");
                    }
                    else
                    {
                        sb.AppendFormat(
    @"
select * from PFR_LOTUS.""{0}"" ts where ts.id in ({1})
", t.Name, string.Join(", ", iL));
                    }
                }

                sb.AppendLine();
                n++;
            }

            sb.AppendLine();
            sb.AppendFormat("###############  Count : {0}", n);
            sb.AppendLine();
        }

    }
}

