﻿
namespace PFR_INVEST.DBUtils
{
    using System;
    using System.Collections.Generic;
    using System.Data.Odbc;
    using System.Text;
    using PFR_INVEST.Core.Logger;
    using System.Configuration;
    using System.IO;
    using PFR_INVEST.LotusMigration.App_Code;
    using PFR_INVEST.LotusMigration.App_Code.Common;
    using PFR_INVEST.LotusMigration.App_Code.DataItems;
    using System.Text.RegularExpressions;

    public class F7ScriptHandler : ScriptHandlerBase
    {
        public override string ActionName
        {
            get { return "F7ScriptHandler"; }
        }


        private string[] Args;

        public string SchemaName;

        public string FileName;
        public string FilePath;

        List<string> IgnoreTables;

        public F7ScriptHandler(string connectionString, string[] args)
        {
            ConnectionString = connectionString;
            Args = args;

            SchemaName = ConfigurationManager.AppSettings.Get("LotusSchemeName");

            if (args.Length < 2)
                throw new ArgumentNullException();

            FileName = args[1];
            FilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, FileName);
        }

        public override void Process()
        {
            Logger.Instance.Info("в таблицах");

            StringBuilder sb = new StringBuilder();


            GetUpdateScript2(sb);

            SaveToFile(FilePath, sb.ToString());

            Logger.Instance.Info("Готово");
        }

        private void GetUpdateScript(StringBuilder sb)
        {
            string cmd = "select * from PFR_LOTUS.F7FORM___ACCOUNTS where type=2";

            sb.AppendLine("-- Set F7FORM___ACCOUNTS columns CONTRAGENT, RETURN_DATE");
            sb.AppendLine();

            DataItemBase.ReadAllRecords(cmd, delegate(OdbcDataReader reader)
            {
                string id_lotus = (string)reader["ID_LOTUS"];
                string notes = (string)reader["NOTES"];

                string entrydate = string.Format("{0:dd}.{0:MM}.{0:yyyy}", reader["ENTRYDATE"]);

                string contragent = GetContragent(notes);
                string returnDate = GetReturnDate(notes);


                if ((entrydate == "25.04.2013") && (returnDate == "25.04.2013"))
                    returnDate = "27.05.2013";

                if ((entrydate == "12.09.2013") && (returnDate == "12.09.2013"))
                    returnDate = "24.09.2013";


                if(returnDate.Contains("по реестру №735"))
                    returnDate = "05.09.2013";

                if (returnDate.Contains("по реестру №736"))
                    returnDate = "05.09.2013";

                if (returnDate == "05.09.13")
                    returnDate = "05.09.2013";

                if (returnDate == "12.03.14")
                    returnDate = "12.03.2014";

                if (returnDate.EndsWith(".14"))
                    returnDate = returnDate.Replace(".14", ".2014");

                sb.AppendFormat(@"update pfr_lotus.F7FORM___ACCOUNTS set CONTRAGENT='{0}', RETURN_DATE='{1}'
                                    where ID_LOTUS = '{2}';--{3}", contragent, returnDate, id_lotus, notes);
                sb.AppendLine();
                sb.AppendLine();
            });
        }


        private void GetUpdateScript2(StringBuilder sb)
        {
            string cmd = "select * from PFR_LOTUS.F7FORM___ACCOUNTS where type=2";

            sb.AppendLine("-- Set BALANCE_EXTRA column REGNUMBER");
            sb.AppendLine();

            DataItemBase.ReadAllRecords(cmd, delegate(OdbcDataReader reader)
            {
                string id_lotus = (string)reader["ID_LOTUS"];
                string notes = (string)reader["NOTES"];

                string entrydate = string.Format("{0:dd}.{0:MM}.{0:yyyy}", reader["ENTRYDATE"]);


                string regNum = GetRegNum(notes);


                sb.AppendFormat(@"update pfr_basic.BALANCE_EXTRA set REGNUMBER='{0}'
where LOTUS_GUID = '{1}';--{2}", regNum,  id_lotus, notes);
                sb.AppendLine();
                sb.AppendLine();
            });
        }

        private string GetRegNum(string notes)
        {
            Regex r = new Regex(@"№\s*(?<N>\d*)");

            Match m = r.Match(notes);

            string s = m.Groups["N"].Captures[0].Value;

            return s;
        }

        private string GetContragent(string notes)
        {
            string s = GetContragent2(notes);

            if (s == "Газпромбанк") s = "ОАО \"Газпромбанк\"";
            if (s == "Банк ВТБ 24(закрытое акционерное общество)") s = "ВТБ 24 (ЗАО)";
            if (s.Contains("ОАО \"Альфа-Банк\"")) s = "ОАО \"Альфа-Банк\"";
            if (s == "Открытое акционерное общество \"ТрансКредитБанк\"") s = "ОАО \"ТрансКредитБанк\"";
            if (s == "\"Газпромбанк\" (Открытое акционерное общество)") s = "ОАО \"Газпромбанк\"";

            if (s.Contains("ОАО \"Сбербанк России\"")) s = "ОАО \"Сбербанк России\"";
            if (s == "Закрытое акционерное общество \"ЮниКредит Банк\"") s = "ЗАО \"ЮниКредит Банк\"";
            if (s.Contains("ОАО \"Россельхозбанк\"")) s = "ОАО \"Россельхозбанк\"";
            if (s.Contains("банк \"Глобэкс\"")) s = "ЗАО КБ \"ГЛОБЭКС\"";
            if (s.Contains("ОАО \"Банк Москвы\"")) s = "ОАО \"Банк Москвы\"";

            if (s.Contains("Номос-Банк")) s = "Номос-Банк";
            if (s.Contains("ОАО \"Газпромбанк\"")) s = "ОАО \"Газпромбанк\"";
            if (s.Contains("Альфа-Банк")) s = "ОАО \"Альфа-Банк\"";
            if (s.Contains("Россельхозбанк")) s = "ОАО \"Россельхозбанк\"";
            if (s == "") s = "";
            if (s == "") s = "";
            if (s == "") s = "";
            if (s == "") s = "";
            if (s == "") s = "";




            return s;
        }

        private string GetContragent2(string notes)
        {
            string[] a = notes.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

            if (a.Length == 0)
                return notes.Trim();

            return a[0].Trim();
        }

        private string GetReturnDate(string notes)
        {
            notes = notes.Replace("дата возврата", ", дата возврата");
            notes = notes.Replace("дата возврата", ", дата возврата");
            notes = notes.Replace("дата возвраа", ", дата возврата");

            string[] a = notes.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

            if (a.Length == 0)
                return notes;

            foreach (var ss in a)
            {
                string s = ss.ToLower();
                if (s.Contains("дата возврата"))
                {
                    string x = s.Replace("дата возврата", "").Trim();

                    if (x.Contains("06 июня 2013"))
                        x = "06.06.2013";

                    return x;
                }
            }

            return notes;
        }

    }
}
