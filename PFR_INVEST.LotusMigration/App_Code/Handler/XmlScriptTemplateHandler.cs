﻿
namespace PFR_INVEST.DBUtils
{
    using System;
    using System.Collections.Generic;
    using System.Data.Odbc;
    using System.Text;
    using PFR_INVEST.Core.Logger;
    using System.Configuration;
    using System.IO;
    using PFR_INVEST.LotusMigration.App_Code;
    using PFR_INVEST.LotusMigration.App_Code.Common;
    using System.Xml;

    public class XmlScriptTemplateHandler : ScriptHandlerBase
    {
        public override string ActionName
        {
            get { return "XmlScriptTemplate"; }
        }

        private string[] Args;

        public string LotusSchemaName;
        public string PfrSchemaName;

        public string LotusTableName;
        public string PfrTableName;


        public string XmlFileName;
        public string XmlFilePath;

        public string FileName;
        public string FilePath;


        public XmlScriptTemplateHandler(string connectionString, string[] args)
        {
            ConnectionString = connectionString;
            Args = args;

            LotusSchemaName = ConfigurationManager.AppSettings.Get("LotusSchemeName");
            PfrSchemaName = ConfigurationManager.AppSettings.Get("PfrSchemeName");


            if (args.Length < 3)
                throw new ArgumentNullException();

            XmlFileName = args[1];
            XmlFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, XmlFileName);

            FileName = args[2];
            FilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, FileName);
        }

        public override void Process()
        {
            Logger.Instance.Info("Загрузка шаблона XML");


            XmlDocument doc = new XmlDocument();
            doc.Load(XmlFilePath);

            string template = doc["Template"]["Template"].InnerText;

            XmlElement xPL = doc["Template"]["TemplateParameters"];

            List<string> Parameters = new List<string>();

            foreach (XmlElement x in xPL.SelectNodes("Parameter"))
            {
                string s = x.InnerText;
                Parameters.Add(s);
            }

            XmlElement xVL = doc["Template"]["ValueList"];

            List<ValueList> Values = new List<ValueList>();

            foreach (XmlElement x in xVL.SelectNodes("Value"))
            {
                ValueList vL = new ValueList();

                foreach (string p in Parameters)
                {
                    string pv = x.GetAttribute(p);
                    vL.Add(pv);
                }


                Values.Add(vL);
            }


            Logger.Instance.Info("Генерация скрипта");

            StringBuilder sb = new StringBuilder();

            foreach (var v in Values)
            {
                string s = template;

                for (int i = 0; i < Parameters.Count; i++)
                {
                    string p = "$" + Parameters[i];
                    string pv = v[i];

                    s = s.Replace(p, pv);
                }

                sb.Append(s);
            }


            SaveToFile(FilePath, sb.ToString());

            Logger.Instance.Info("Скрипт создан");
        }

        public class ValueList : List<string>
        {
        }
    }
}
