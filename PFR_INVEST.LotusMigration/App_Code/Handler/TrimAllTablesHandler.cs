﻿
namespace PFR_INVEST.DBUtils
{
    using System;
    using System.Collections.Generic;
    using System.Data.Odbc;
    using System.Text;
    using PFR_INVEST.Core.Logger;
    using System.Configuration;
    using System.IO;
    using PFR_INVEST.LotusMigration.App_Code;
    using PFR_INVEST.LotusMigration.App_Code.Common;
    using PFR_INVEST.LotusMigration.App_Code.DataItems;

    public class TrimAllTablesHandler : ScriptHandlerBase
    {
        public override string ActionName
        {
            get { return "TrimAllTables"; }
        }


        private string[] Args;

        public string SchemaName;

        public string FileName;
        public string FilePath;

        List<string> IgnoreTables;

        public TrimAllTablesHandler(string connectionString, string[] args)
        {
            ConnectionString = connectionString;
            Args = args;

            if (args[1].ToLower().Contains("lotus"))
                SchemaName = ConfigurationManager.AppSettings.Get("LotusSchemeName");
            else
                SchemaName = ConfigurationManager.AppSettings.Get("PfrSchemeName");


            if (args.Length < 3)
                throw new ArgumentNullException();

            FileName = args[2];
            FilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, FileName);

            string s = ConfigurationManager.AppSettings.Get("IgnoreTrimTableList");

            IgnoreTables = new List<string>();

            if (!string.IsNullOrEmpty(s))
            {
                foreach (string t in s.ToUpper().Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                    IgnoreTables.Add(t.Trim());
            }
        }

        public override void Process()
        {
            Logger.Instance.Info("Выборка данных о таблицах");

            SchemaInfoBase s = SchemaInfoBase.GetSchemaAllTables(SchemaName);

            Logger.Instance.Info("Trim в таблицах");

            StringBuilder sb = new StringBuilder();

            sb.AppendLine("Tables found");

            foreach (var t in s.Tables)
            {
                if (IgnoreTables.Contains(t.Name.ToUpper()))
                    continue;

                Console.WriteLine(t.Name);
                sb.Append(GetTrimText(OdbcConnection, t));
            }


            SaveToFile(FilePath, sb.ToString());

            Logger.Instance.Info("Готово");
        }

        //!!TODO
        //Если Trim произвел замену, возвращает имена столбцов с кол-вом замен
        private string GetTrimText(System.Data.Odbc.OdbcConnection OdbcConnection, TableInfoBase t)
        {
            StringBuilder sb = new StringBuilder();

            foreach (var c in t.Cols)
            {
                try
                {
                    TrimColumn(sb, t, c);
                }
                catch (Exception e)
                {
                    sb.AppendLine();
                    sb.AppendFormat("Error occured on {0}.{1}", t.Name, c.ColName);
                    sb.AppendLine();
                    sb.AppendLine(e.Message);
                    sb.AppendLine();
                    sb.AppendLine(e.StackTrace);
                    sb.AppendLine();
                }
            }

            return sb.ToString();
        }

        private void TrimColumn(StringBuilder sb, TableInfoBase t, ColInfoBase c)
        {
            switch (c.DataType)
            {
                case ColInfoBase.enType.varchar:
                    break;
                default:
                    return;
            }

            string cmd = string.Format(@"
SELECT ""{2}"" as VALUE FROM OLD TABLE
(
update ""{0}"".""{1}"" a
set ""{2}"" = trim(""{2}"")
where  
a.""{2}"" is not null
and length(trim(a.""{2}"")) <> length(a.""{2}"")
)"
                , t.Schema, t.Name, c.ColName);

            List<string> sL = new List<string>();

            DataItemBase.ReadAllRecords(cmd, delegate(OdbcDataReader reader)
            {
                string s = (string)reader["VALUE"];
                sL.Add(string.Format("[{0}]", s));
            });

            c.MatchCount = sL.Count;

            if (c.MatchCount > 0)
            {
                sb.AppendLine();
                sb.AppendFormat("{3}.{0} : {1}   -- {2}", c.ColName, c.MatchCount, c.Remarks, t.Name);
                sb.AppendLine();
                sb.AppendLine(string.Join(", ", sL));
                sb.AppendLine();
            }
        }
    }
}
