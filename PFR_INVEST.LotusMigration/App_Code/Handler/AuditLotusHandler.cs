﻿
namespace PFR_INVEST.DBUtils
{
    using System;
    using System.Collections.Generic;
    using System.Data.Odbc;
    using System.Text;
    using PFR_INVEST.Core.Logger;
    using System.Configuration;
    using System.IO;
    using PFR_INVEST.LotusMigration.App_Code;
    using PFR_INVEST.LotusMigration.App_Code.Common;

    using System.Linq;
    using System.Xml;

    public class AuditLotusHandler : ScriptHandlerBase
    {
        public override string ActionName
        {
            get { return "AuditLotus"; }
        }


        private string[] Args;

        public string SchemaNameLotus;
        public string SchemaNameBasic;


        public string FileName;
        public string FilePath;

        public AuditLotusHandler(string connectionString, string[] args)
        {
            ConnectionString = connectionString;
            Args = args;

            SchemaNameLotus = ConfigurationManager.AppSettings.Get("LotusSchemeName");
            SchemaNameBasic = ConfigurationManager.AppSettings.Get("PfrSchemeName");


            if (args.Length < 2)
                throw new ArgumentNullException();


            FileName = args[1];
            FilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, FileName);
        }

        public override void Process()
        {
            Console.WriteLine("Загрузка таблиц");

            Logger.Instance.Info("Выборка данных о таблицах Pfr_Basic");
            SchemaInfoBase sBasic = SchemaInfoBase.GetSchemaAllTables(SchemaNameBasic);

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Tables found");
            sb.AppendFormat("PFR_BASIC - {0} Tables", sBasic.Tables.Count);
            sb.AppendLine();

            Console.WriteLine("Загрузка комментариев");

            sBasic.LoadTableRemarks();


            Logger.Instance.Info("Аудит таблиц");


            Console.WriteLine("Загрузка числа строк в таблицах");
            sBasic.LoadRowCount();


            sb.AppendLine();
            sb.AppendLine("*************** Таблицы PFR_BASIC, Отсутствуют все поля LOTUS_ID, LOTUS_TABLE, LOTUS_GUID: *******************");
            sb.AppendLine();
            PrintTables(sb, sBasic, IsMissAllLotus);
            sBasic.FilterTables(IsMissAllLotus);// Don't need this tables any more

            sb.AppendLine();
            sb.AppendLine("************* Таблицы PFR_BASIC, Отсутствуют НЕКОТОРЫЕ поля LOTUS_ID, LOTUS_TABLE, LOTUS_GUID: *****************");
            sb.AppendLine();
            PrintTables(sb, sBasic, IsMissSomeLotus);
            sBasic.FilterTables(IsMissSomeLotus);// Don't need this tables any more

            Console.WriteLine("Поиск пустых значений");
            sBasic.LoadNullLotusRowCount();

            sb.AppendLine();
            sb.AppendLine("======  PFR_BASIC  Содержат Null в полях LOTUS_ID, LOTUS_TABLE, LOTUS_GUID ======================================");
            sb.AppendLine();
            PrintTables(sb, sBasic, IsNullLotus);
            sBasic.FilterTables(IsNullLotus);// Don't need this tables any more

            sb.AppendLine();
            sb.AppendLine("///////  PFR_BASIC  все заполнено в LOTUS_ID, LOTUS_TABLE, LOTUS_GUID //////////////////////////////////////");
            sb.AppendLine();
            PrintTables(sb, sBasic, IsTrue);


            SaveToFile(FilePath, sb.ToString());

            Logger.Instance.Info("Готово");
            Console.WriteLine("Готово");
        }

        private void LoadMapping(SchemaInfoBase sBasic, SchemaInfoBase sLotus)
        {
            foreach (var t in sBasic.Tables)
            {
                t.SelectMapData();
            }

            sLotus.ApplyMapData(sBasic);
        }



        private bool IsCompleted(TableInfoBase t)
        {
            if (t.Status == TableInfoBase.enStatus.Completed)
                return true;
            return false;
        }

        private bool IsNotMigratedTable(TableInfoBase t)
        {
            if (!t.IsMigratedTable())
                return true;

            return false;
        }

        private bool IsIgnore(TableInfoBase t)
        {
            if (t.IsIgnore)
                return true;

            return false;
        }


        public delegate bool ConditionHandler(TableInfoBase t);

        private void PrintTables(StringBuilder sb, SchemaInfoBase s, ConditionHandler selectCondition)
        {
            int n = 0;

            foreach (var t in s.Tables)
            {
                if (!selectCondition(t))
                    continue;

                sb.AppendFormat("{0} - {1}", t.Name, t.Remarks);
                sb.AppendLine();

                if (!string.IsNullOrEmpty(t.Comment))
                    sb.AppendLine(t.Comment);
                if (!string.IsNullOrEmpty(t.Comment2))
                    sb.AppendLine(t.Comment2);

                if (t.RowCountLotusTable > 0)
                {
                    sb.AppendFormat("  {0} Rows LOTUS_TABLE missing", t.RowCountLotusTable);
                    sb.AppendLine();
                }

                if (t.RowCountLotusID > 0)
                {
                    sb.AppendFormat("  {0} Rows LOTUS_ID missing", t.RowCountLotusID);
                    sb.AppendLine();
                }

                if (t.RowCountLotusGUID > 0)
                {
                    sb.AppendFormat("  {0} Rows LOTUS_GUID missing", t.RowCountLotusGUID);
                    sb.AppendLine();
                }


                sb.AppendFormat("  {0} Rows", t.RowCount);
                sb.AppendLine();

                n++;
            }
            sb.AppendLine();
            sb.AppendFormat("###############  Count : {0}", n);
            sb.AppendLine();
        }


        private void PrintMapped(StringBuilder sb, SchemaInfoBase s)
        {
            PrintMapped(sb, s, delegate(TableInfoBase t)
            {
                return true;
            });
        }

        private void PrintMapped(StringBuilder sb, SchemaInfoBase s, ConditionHandler selectCondition)
        {
            int n = 0;

            foreach (var t in s.Tables)
            {
                if (!selectCondition(t))
                    continue;

                sb.AppendFormat("{0} - {1}", t.Name, t.Remarks);
                sb.AppendLine();

                if (!string.IsNullOrEmpty(t.Comment))
                    sb.AppendLine(t.Comment);
                if (!string.IsNullOrEmpty(t.Comment2))
                    sb.AppendLine(t.Comment2);


                sb.AppendFormat("{0} Records", t.RowCount);
                sb.AppendLine();

                if (t.MapIndex.Count > 0)
                {
                    sb.AppendLine("Mapping");
                    foreach (var x in t.MapIndex)
                    {
                        sb.AppendFormat("{0} - {1}", x.Key ?? "[NULL]", x.Value);
                        sb.AppendLine();
                    }
                }

                if (t.IsPrintMissingScript)//For lotus tables only
                {
                    foreach (var x in t.MapIndex)
                    {
                        sb.AppendFormat(
    @"
select ts.* from 
PFR_LOTUS.""{0}""   ts
left join PFR_BASIC.""{1}""  td
on td.LOTUS_ID = ts.ID
and td.LOTUS_TABLE = '{0}'
where td.id is null
", t.Name, x.Key);
                    }
                }

                if (!string.IsNullOrEmpty(t.PrintMissingIdScript_TableName))//For lotus tables only 
                {
                    List<long> iL = t.GetNotMappedIdList();

                    if (iL == null)//No mapping
                    {
                        sb.AppendLine("No Migrated Rows");
                    }
                    else
                    {
                        sb.AppendFormat(
    @"
select * from PFR_LOTUS.""{0}"" ts where ts.id in ({1})
", t.Name, string.Join(", ", iL));
                    }
                }

                sb.AppendLine();
                n++;
            }

            sb.AppendLine();
            sb.AppendFormat("###############  Count : {0}", n);
            sb.AppendLine();
        }




        private bool IsMissAllLotus(TableInfoBase t)
        {
            if (t.IsMigratedTableNone())
                return true;

            return false;
        }

        private bool IsMissSomeLotus(TableInfoBase t)
        {
            if (!t.IsMigratedTableGuid())
                return true;

            return false;
        }

        private bool IsNullLotus(TableInfoBase t)
        {
            if (t.IsNullLotus())
                return true;

            return false;
        }

        private bool IsTrue(TableInfoBase t)
        {
            return true;
        }
    }
}

