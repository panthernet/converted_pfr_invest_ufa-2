﻿
namespace PFR_INVEST.DBUtils
{
    using System;
    using System.Collections.Generic;
    using System.Data.Odbc;
    using System.Text;
    using PFR_INVEST.Core.Logger;
    using System.Configuration;
    using System.IO;
    using PFR_INVEST.LotusMigration.App_Code;
    using PFR_INVEST.LotusMigration.App_Code.Common;
    using System.Xml;

    public class SearchXmlAllTablesHandler : ScriptHandlerBase
    {
        public class SearchParams
        {
            public string SchemaName { get; set; }
            public string OutputFileName { get; set; }

            public List<string> ValueList { get; set; }

            public SearchParams()
            {
                ValueList = new List<string>();
            }

            public SearchParams(string xmlFilePath)
                : this()
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(xmlFilePath);


                XmlElement q = doc["Request"];

                SchemaName = q.GetAttribute("Schema");
                OutputFileName = q.GetAttribute("Output");

                foreach (XmlElement x in q["Values"].SelectNodes("Value"))
                {
                    string s = x.InnerText;
                    ValueList.Add(s);
                }

            }


            internal string GetSearchString()
            {
                return string.Join(", ", ValueList);
            }

            public string OutputFilePath
            {
                get
                {
                    return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, OutputFileName);
                }
            }
        }


        public override string ActionName
        {
            get { return "SearchXmlAllTables"; }
        }


        private string[] Args;

        //public string SchemaName;

        public string InFileName;
        public string InFilePath;


        List<string> IgnoreTables;

        public SearchParams Parameters;

        public SearchXmlAllTablesHandler(string connectionString, string[] args)
        {
            ConnectionString = connectionString;
            Args = args;



            if (args.Length < 1)
                throw new ArgumentNullException();

            InFileName = args[1];
            InFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, InFileName);

            Parameters = new SearchParams(InFilePath);


            string s = ConfigurationManager.AppSettings.Get("IgnoreSearchTableList");

            IgnoreTables = new List<string>();

            if (!string.IsNullOrEmpty(s))
            {
                foreach (string t in s.ToUpper().Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                    IgnoreTables.Add(t.Trim());
            }
        }

        public override void Process()
        {
            Logger.Instance.Info("Выборка данных о таблицах");

            SchemaInfoBase s = SchemaInfoBase.GetSchemaAllTables(Parameters.SchemaName);

            Logger.Instance.Info("Поиск в таблицах");

            Console.WriteLine(Parameters.GetSearchString());

            StringBuilder sb = new StringBuilder();

            sb.AppendLine("Искомая строка");
            sb.AppendFormat("[{0}]\r\n", Parameters.GetSearchString());

            sb.AppendLine("Tables found");

            foreach (var t in s.Tables)
            {
                if (IgnoreTables.Contains(t.Name.ToUpper()))
                    continue;

                Console.WriteLine(t.Name);
                sb.Append(t.GetMatchTextIn(OdbcConnection, Parameters.ValueList));
            }


            SaveToFile(Parameters.OutputFilePath, sb.ToString());


            Logger.Instance.Info("Готово");
        }

    }
}
