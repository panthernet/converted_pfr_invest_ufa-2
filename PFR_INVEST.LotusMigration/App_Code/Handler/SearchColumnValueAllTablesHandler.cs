﻿
namespace PFR_INVEST.DBUtils
{
    using System;
    using System.Collections.Generic;
    using System.Data.Odbc;
    using System.Text;
    using PFR_INVEST.Core.Logger;
    using System.Configuration;
    using System.IO;
    using PFR_INVEST.LotusMigration.App_Code;
    using PFR_INVEST.LotusMigration.App_Code.Common;
    using PFR_INVEST.LotusMigration.App_Code.DataItems;


    /// <summary>
    /// Ищет столбец во всех таблицах схемы и выбирает все его значения
    /// </summary>
    public class SearchColumnValueAllTablesHandler : ScriptHandlerBase
    {
        public override string ActionName
        {
            get { return "SearchColumnValueAllTables"; }
        }


        private string[] Args;

        public string SchemaName;

        public string FileName;
        public string FilePath;

        public string SearchString;

        List<string> IgnoreTables;


        public SearchColumnValueAllTablesHandler(string connectionString, string[] args)
        {
            ConnectionString = connectionString;
            Args = args;

            if (args[1].ToLower().Contains("lotus"))
                SchemaName = ConfigurationManager.AppSettings.Get("LotusSchemeName");
            else
                SchemaName = ConfigurationManager.AppSettings.Get("PfrSchemeName");


            if (args.Length < 4)
                throw new ArgumentNullException();

            SearchString = args[2];

            FileName = args[3];
            FilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, FileName);

            string s = ConfigurationManager.AppSettings.Get("IgnoreSearchTableList");

            IgnoreTables = new List<string>();

            if (!string.IsNullOrEmpty(s))
            {
                foreach (string t in s.ToUpper().Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                    IgnoreTables.Add(t.Trim());
            }
        }

        public override void Process()
        {
            Logger.Instance.Info("Выборка данных о таблицах");

            SchemaInfoBase s = SchemaInfoBase.GetSchemaAllTables(SchemaName);

            Logger.Instance.Info("Поиск в таблицах");

            Console.WriteLine(SearchString);

            StringBuilder sb = new StringBuilder();

            sb.AppendLine("Искомая строка");
            sb.AppendFormat("[{0}]\r\n",SearchString);

            sb.AppendLine("Tables found");

            foreach (var t in s.Tables)
            {
                TableInfoColumnData tt = new TableInfoColumnData(t);

                if (IgnoreTables.Contains(t.Name.ToUpper()))
                    continue;

                Console.WriteLine(t.Name);
                sb.Append(tt.GetMatchText(OdbcConnection, SearchString));
            }


            SaveToFile(FilePath, sb.ToString());


            Logger.Instance.Info("Готово");
        }

    }
}
