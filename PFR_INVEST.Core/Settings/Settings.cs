﻿using PFR_INVEST.Core.Settings;
using System;
namespace PFR_INVEST.Core.Properties {
    
    
    // This class allows you to handle specific events on the settings class:
    //  The SettingChanging event is raised before a setting's value is changed.
    //  The PropertyChanged event is raised after a setting's value is changed.
    //  The SettingsLoaded event is raised after the setting values are loaded.
    //  The SettingsSaving event is raised before the setting values are saved.
    public sealed partial class Settings {
        
        public Settings() 
		{
			////Загрузка настроек из предидущих версий приложения 
			//if (UpgradeSettings)
			//{
			//    Upgrade();
			//    UpgradeSettings = false;
			//    Save();
			//}
			
			
			// // To add event handlers for saving and changing settings, uncomment the lines below:
            //
            // this.SettingChanging += this.SettingChangingEventHandler;
            //
            // this.SettingsSaving += this.SettingsSavingEventHandler;
            //
        }
        
        /*private void SettingChangingEventHandler(object sender, System.Configuration.SettingChangingEventArgs e) {
            // Add code to handle the SettingChangingEvent event here.
        }
        
        private void SettingsSavingEventHandler(object sender, System.ComponentModel.CancelEventArgs e) {
            // Add code to handle the SettingsSaving event here.
        }*/

		[global::System.Configuration.SettingsProvider(typeof(AppUserSettingsProvider))]
		[global::System.Configuration.UserScopedSettingAttribute()]
		[global::System.Configuration.DefaultSettingValueAttribute("")]
		public long? StoredPPPortfolioID
		{
			get { return ((long?)(this["StoredPPPortfolioID"])); }
			set { this["StoredPPPortfolioID"] = value; }
		}

		[global::System.Configuration.SettingsProvider(typeof(AppUserSettingsProvider))]
		[global::System.Configuration.UserScopedSettingAttribute()]
		[global::System.Configuration.DefaultSettingValueAttribute("")]
		public long? StoredPPPfrBankAccountID
		{
			get { return ((long?)(this["StoredPPPfrBankAccountID"])); }
			set { this["StoredPPPfrBankAccountID"] = value; }
		}

		[global::System.Configuration.SettingsProvider(typeof(AppUserSettingsProvider))]
		[global::System.Configuration.UserScopedSettingAttribute()]
		[global::System.Configuration.DefaultSettingValueAttribute("true")]
		public bool StoredPPActive
		{
			get { return ((bool)(this["StoredPPActive"])); }
			set { this["StoredPPActive"] = value; }
		}

		[global::System.Configuration.SettingsProvider(typeof(AppUserSettingsProvider))]
		[global::System.Configuration.UserScopedSettingAttribute()]
		[global::System.Configuration.DefaultSettingValueAttribute("true")]
		public bool StoredPPDatesActive
		{
			get { return ((bool)(this["StoredPPDatesActive"])); }
			set { this["StoredPPDatesActive"] = value; }
		}

		[global::System.Configuration.SettingsProvider(typeof(AppUserSettingsProvider))]
		[global::System.Configuration.UserScopedSettingAttribute()]
		[global::System.Configuration.DefaultSettingValueAttribute("")]
		public DateTime? StoredPPPayDate
		{
			get { return ((DateTime?)(this["StoredPPPayDate"])); }
			set { this["StoredPPPayDate"] = value; }
		}

		[global::System.Configuration.SettingsProvider(typeof(AppUserSettingsProvider))]
		[global::System.Configuration.UserScopedSettingAttribute()]
		[global::System.Configuration.DefaultSettingValueAttribute("")]
		public DateTime? StoredPPSPNDate
		{
			get { return ((DateTime?)(this["StoredPPSPNDate"])); }
			set { this["StoredPPSPNDate"] = value; }
		}

        [global::System.Configuration.SettingsProvider(typeof(AppUserSettingsProvider))]
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public bool StoredPPDirection2
        {
            get { return ((bool)(this["StoredPPDirection2"])); }
            set { this["StoredPPDirection2"] = value; }
        }

        [global::System.Configuration.SettingsProvider(typeof(AppUserSettingsProvider))]
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public long? StoredPPDirectionElId
        {
            get { return ((long?)(this["StoredPPDirectionElId"])); }
            set { this["StoredPPDirectionElId"] = value; }
        }



		[global::System.Configuration.SettingsProvider(typeof(AppUserSettingsProvider))]
		[global::System.Configuration.UserScopedSettingAttribute()]
		[global::System.Configuration.DefaultSettingValueAttribute("")]
		public long? StoredPPDirection
		{
			get { return ((long?)(this["StoredPPDirection"])); }
			set { this["StoredPPDirection"] = value; }
		}

		[global::System.Configuration.SettingsProvider(typeof(AppUserSettingsProvider))]
		[global::System.Configuration.UserScopedSettingAttribute()]
		[global::System.Configuration.DefaultSettingValueAttribute("")]
		public long? StoredPPSection
		{
			get { return ((long?)(this["StoredPPSection"])); }
			set { this["StoredPPSection"] = value; }
		}

        [global::System.Configuration.SettingsProvider(typeof(AppUserSettingsProvider))]
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public long? StoredPPSection2
        {
            get { return ((long?)(this["StoredPPSection2"])); }
            set { this["StoredPPSection2"] = value; }
        }

        [global::System.Configuration.SettingsProvider(typeof(AppUserSettingsProvider))]
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public long? StoredPPPayDetails
        {
            get { return ((long?)(this["StoredPPPayDetails"])); }
            set { this["StoredPPPayDetails"] = value; }
        }
        

		[global::System.Configuration.SettingsProvider(typeof(AppUserSettingsProvider))]
		[global::System.Configuration.UserScopedSettingAttribute()]
		[global::System.Configuration.DefaultSettingValueAttribute("")]
		public long? StoredPPLinkedRegister
		{
			get { return ((long?)(this["StoredPPLinkedRegister"])); }
			set { this["StoredPPLinkedRegister"] = value; }
		}

		[global::System.Configuration.SettingsProvider(typeof(AppUserSettingsProvider))]
		[global::System.Configuration.UserScopedSettingAttribute()]
		[global::System.Configuration.DefaultSettingValueAttribute("")]
		public bool StoredPPLinkedRegisterActive
		{
			get { return ((bool)(this["StoredPPLinkedRegisterActive"])); }
			set { this["StoredPPLinkedRegisterActive"] = value; }
		}

		[global::System.Configuration.SettingsProvider(typeof(AppUserSettingsProvider))]
		[global::System.Configuration.UserScopedSettingAttribute()]
		[global::System.Configuration.DefaultSettingValueAttribute("")]
		public decimal? StoredReportInvestMultiplier
		{
			get { return ((decimal?)(this["StoredReportInvestMultiplier"])); }
			set { this["StoredReportInvestMultiplier"] = value; }
		}

        //////////////////////////////////////////////////////////////////////
        //Временные настройки, реализовать по мере надобности в нужном месте
        //////////////////////////////////////////////////////////////////////

        //[global::System.Configuration.SettingsProvider(typeof(AppUserSettingsProvider))]
        //[global::System.Configuration.UserScopedSettingAttribute()]
        //[global::System.Configuration.DefaultSettingValueAttribute("false")]
        /// <summary>
        /// Показывать сальдо в валюте или в рублевом эквиваленте
        /// </summary>
        public static bool ShowSaldoInCurrency
        {
            get { return true; }
        }

    }
}
