﻿using System;
using IBM.WMQ;
using System.Threading.Tasks;
using System.Collections;
using System.Configuration;
using System.Linq;
using System.Text;
using PFR_INVEST.Common.Extensions;
using PFR_INVEST.Core.BusinessLogic;

namespace PFR_INVEST.Core.MQConnector
{
	public class MQCoreConnector : IDisposable
	{
		private readonly MQQueueManager _mqQueueManager;
		private MQTopic _mqTopic;
	    private readonly string _topicPrefix;

		public delegate void LogExceptionDelegate(Exception ex);
		public LogExceptionDelegate LogException;
		public delegate void LogMessageDelegate(string message);
		public LogMessageDelegate LogMessage;
        public const int MQ_REASONCODE_NOMESSAGE = 2033;

	    private readonly bool _isEnabled = true;
        /// <summary>
        /// Указывает, что MQ коннектор подписан на очередь или топик и обрабатывет сообщения
        /// </summary>
        public bool IsOperating { get; protected set; }

	    public MQCoreConnector(string managerName, string topicPrefix, Hashtable props = null, LogMessageDelegate logMessage = null, LogExceptionDelegate logException = null )
        {
            if (ConfigurationManager.AppSettings.AllKeys.Contains("MQEnabled"))
                _isEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["MQEnabled"]);

            if (!_isEnabled) return;
		    LogMessage = logMessage;
            LogException = logException;
			props = props ?? new Hashtable();
            _topicPrefix = topicPrefix;

            _mqQueueManager = new MQQueueManager(managerName, props);
			if (!_mqQueueManager.OpenStatus) //ConnectionStatus
			{
				_mqQueueManager = null;
				throw new Exception("MQCoreConnector can't connect to queue manager");
			}
            LogMessage?.Invoke("Created MQCoreConnector to " + managerName);
		}

		public void Close() 
		{
			_mqQueueManager.Close();
		}

		public bool QueueMessage(string mes, string queuename, int pEncoding = 0, int pCharset = 0)
		{
            if (!_isEnabled) return false;
			try
			{
				//MQMessage message = MQSession.AccessMessage() as MQMessage;
				var message = new MQMessage { Format = MQC.MQFMT_STRING };
				if (pCharset > 0)
					message.CharacterSet = pCharset;
				if (pEncoding > 0)
					message.Encoding = pEncoding;
				message.WriteString(mes);

				if (_mqQueueManager.IsConnected)
				{
					const int openOptions = MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_OUTPUT;
					var queue = _mqQueueManager.AccessQueue(queuename, openOptions, "", "", "");

					queue.ClearErrorCodes();
					var messageOptions = new MQPutMessageOptions();
					queue.Put(message, messageOptions);

				    if (queue.CompletionCode == 0) return true;
				    LogMessage?.Invoke($"MQQueue.Put Failed. ReasonName: {queue.ReasonName}, ReasonCode: {queue.ReasonCode}");
				    return false;
				}
				return true;
			}
			catch (Exception ex)
			{
			    LogException?.Invoke(ex);
			    return false;
			}
		}

		public string GetMessage(string queuename)
		{
            if (!_isEnabled) return "";
			try
			{
				var queue = _mqQueueManager.AccessQueue(queuename, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
				var queueMessage = new MQMessage { Format = MQC.MQFMT_STRING };
				var queueGetMessageOptions = new MQGetMessageOptions();
				queue.Get(queueMessage, queueGetMessageOptions);
                return queueMessage.ReadString(queueMessage.MessageLength);
            }
			catch (MQException ex)
			{
				//Нет сообщения в очереди
				if (ex.ReasonCode == MQ_REASONCODE_NOMESSAGE)
					return null;
				LogMessage("Exception : " + ex.Message);
			}
			catch (Exception ex)
			{
				LogMessage("Exception : " + ex.Message);
			}
			return string.Empty;

            #region Old
            //try
            //{


            //    MQMessage message = MQSession.AccessMessage() as MQMessage;
            //    if (p_Charset > 0)
            //        message.CharacterSet = p_Charset;
            //    if (p_Encoding > 0)
            //        message.Encoding = p_Encoding;
            //    message.WriteString(mes);

            //    if (MQQueueManager.IsConnected)
            //    {
            //        int openOptions = (int)IBM.WMQ.MQC.MQOO_INPUT_AS_Q_DEF + (int)IBM.WMQ.MQC.MQOO_OUTPUT;
            //        MQQueue queue = MQQueueManager.AccessQueue(queuename, openOptions, "", "", "") as MQQueue;

            //        queue.ClearErrorCodes();
            //        MQPutMessageOptions messageOptions = MQSession.AccessPutMessageOptions() as MQPutMessageOptions;
            //        queue.Put(message, messageOptions);

            //        if (queue.CompletionCode != 0)
            //        {
            //            if (LogMessage != null)
            //                LogMessage(string.Format("MQQueue.Put Failed. ReasonName: {0}, ReasonCode: {1}", queue.ReasonName, queue.ReasonCode));
            //            return "";
            //        }
            //    }
            //    return "t";


            //}
            //catch (Exception ex)
            //{
            //    if (LogException != null)
            //        LogException(ex);
            //    return "";
            //}
            #endregion
        }

		public string SubscribeTopic(string topicName, Action<string> callBack, int interval = 1000, Action<Exception> exceptionAction = null, bool logQueries = false)
		{
            if (!_isEnabled) return "";
			var topic = _mqQueueManager.AccessTopic(topicName, null, MQC.MQSO_CREATE | MQC.MQSO_FAIL_IF_QUIESCING | MQC.MQSO_DURABLE | MQC.MQSO_RESUME, null, _topicPrefix + topicName);
			MQMessage message;

			_mqTopic = topic;

			Task.Factory.StartNew(() =>
			{
			    try
			    {
			        if (logQueries)
			            LogMessage("IsOpen: " + topic.IsOpen);
			        IsOperating = true;
			        while (topic.IsOpen)
			        {
			            if (logQueries)
			                LogMessage("Querying message...");

			            // creating a message object
			            message = new MQMessage();
			            message.ClearMessage();
			            try
			            {
			                topic.Get(message);
			                var msg = message.ReadString(message.MessageLength);
			                message.ClearMessage();
			                callBack?.Invoke(msg);
			            }
			            catch (MQException mqe)
			            {
			                if (mqe.ReasonCode == MQ_REASONCODE_NOMESSAGE)
			                {
			                    if (logQueries)
			                        LogMessage("No message found!");
			                    Console.WriteLine("No message available");
			                    TaskHelper.Delay(interval).Wait();
			                }
			                else throw;
			            }
			        }
			    }
			    catch (Exception ex)
			    {
			        exceptionAction?.Invoke(ex);
			    }
			    finally
			    {
			        IsOperating = false;
                    LogMessage("WARNING! Subscription ended! Need to resubscribe.");
                }
            });

			return null;
		}




        public void PublishInTopic(string topicName, string messageString)
		{
            if (!_isEnabled) return;
			var topic = _mqQueueManager.AccessTopic(topicName, "", MQC.MQTOPIC_OPEN_AS_PUBLICATION, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);

		    try
		    {
		        var message = new MQMessage();
		        message.WriteString(messageString);
		        topic.Put(message);
		    }
		    finally
		    {
		        topic.Close();
		    }
		}

		void IDisposable.Dispose()
		{
			if (_mqTopic != null && _mqTopic.IsOpen)
				_mqTopic.Close();

			if (_mqQueueManager != null && _mqQueueManager.IsOpen)
				_mqQueueManager.Close();
		}

	    public void SubscribeToQueue(string queueName, Action<string> callBack, int interval, Action<Exception> exceptionAction, bool logQueries)
	    {
	        LogMessage($"SubscribeToQueue 1 isEnabled= {_isEnabled} logQueries={logQueries} logMessage={LogMessage != null}");
            if (!_isEnabled) return;
            LogMessage("SubscribeToQueue 2");

            Task.Factory.StartNew(() =>
            {
                LogMessage("SubscribeToQueue NewTask");
                try
                {
                    IsOperating = true;
                    LogMessage($"SubscribeToQueue IsOperating={IsOperating}");
                    while (true)
                    {
                        if (logQueries)
                            LogMessage($"Querying message from queue {queueName}...");
                        try
                        {
                            var msg = GetMessage(queueName);
                            if (!string.IsNullOrEmpty(msg))
                            {
                                if (logQueries)
                                    LogMessage("GOT message from queue! Sending for parse...");
                                callBack?.Invoke(msg);
                            }
                            else
                            {
                                if (logQueries)
                                    LogMessage("No message available!.");
                                TaskHelper.Delay(interval).Wait();
                            }
                        }
                        catch (MQException mqe)
                        {
                            if (mqe.ReasonCode == MQ_REASONCODE_NOMESSAGE)
                            {
                                if (logQueries)
                                    LogMessage("No message available!");
                                TaskHelper.Delay(interval).Wait();
                            }
                            else throw;
                        }
                    }
                }
                catch (Exception ex)
                {
                    exceptionAction?.Invoke(ex);
                }
                finally
                {
                    IsOperating = false;
                    LogMessage("WARNING! Subscription ended! Need to resubscribe.");
                }
            });
        }
	}
}
