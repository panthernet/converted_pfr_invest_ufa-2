﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PFR_INVEST.Core.BusinessLogic
{
    public interface IBusyHelper
    {

        void Start();
        void Start(string message);

        void Stop();
    }
}
