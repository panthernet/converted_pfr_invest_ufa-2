﻿namespace PFR_INVEST.Core.BusinessLogic
{
    public enum ErrorReportType
    {
        Tipology = 0,
        Npf,
        Region,
        Month
    }
}
