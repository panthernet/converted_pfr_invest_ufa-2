﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.Core.BusinessLogic
{
	[DataContract]
	public class ActionResult<T>
	{
		[IgnoreDataMember]
		public bool IsSuccess { get { return string.IsNullOrEmpty(ErrorMessage); } }

		[DataMember]
		public string ErrorMessage { get; set; }

		[DataMember]
		public int ErrorCode { get; set; }

		[DataMember]
		public T Data { get; set; }

		public ActionResult() { }

		public ActionResult(T data)
			: this()
		{
			this.Data = data;
		}

		public static ActionResult<T> Error(string errorMessage, int errorCode = 0)
		{
			return new ActionResult<T>() { ErrorMessage = errorMessage, ErrorCode = errorCode };
		}
	}

	[DataContract]
	public class ActionResult
	{
		[DataMember]
		public bool IsSuccess { get; set; }

		[DataMember]
		public string ErrorMessage { get; set; }

		[DataMember]
		public bool IsTotalReport { get; set; }

		public static ActionResult Success()
		{
			return new ActionResult() { IsSuccess = true, ErrorMessage = null, IsTotalReport = false };
		}


		public static ActionResult Error(string errorMessage)
		{
			return new ActionResult() { IsSuccess = false, ErrorMessage = errorMessage, IsTotalReport = false };
		}

		public static ActionResult Error(string errorMessage, params object[] par)
		{
			return new ActionResult() { IsSuccess = false, ErrorMessage = string.Format(errorMessage, par), IsTotalReport = false };
		}

		public static ActionResult TotalReport()
		{
			return new ActionResult() { IsSuccess = true, ErrorMessage = null, IsTotalReport = true };
		}
	}
}
