﻿namespace PFR_INVEST.Web2.ClassLibrary.Models
{
    /// <summary>
    /// Интерфейс, указыыающий, что модель содержит универсальный метод вызова валидации данных
    /// </summary>
    public interface IValidatableViewModel
    {
        string Validate();
    }
}
