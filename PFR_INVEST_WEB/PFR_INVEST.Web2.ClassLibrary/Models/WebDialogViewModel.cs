﻿namespace PFR_INVEST.BusinessLogic.Web
{
    /// <summary>
    /// Единая базовая модель для всех веб-моделей данных
    /// </summary>
    public class WebDialogViewModel : ViewModelCardDialog
    {

#if WEBCLIENT
        public override void WebExecuteDelete(long id) { }

        
        /// <summary>
        /// Возвращает 
        /// </summary>
        /// <returns></returns>
        public override long WebGetEntryId()
        {
            return 0;
        }



        public override bool WebExecuteSaveWithChecks()
        {
            if (!WebBeforeExecuteSaveCheck()) return false;      
            return true;
        }
        
        public bool EditAccessDenied => (this as ViewModelBase)?.EditAccessDenied ?? false;
        /// <summary>
        /// Указывает были ли изменены данные в модели
        /// </summary>
        public bool IsDataChanged
        {
            get { return (this as ViewModelBase)?.IsDataChanged ?? true; }
            set { if(this is ViewModelBase)
                    (this as ViewModelBase).IsDataChanged = value;
            }
        }
#else
        public bool EditAccessDenied => true;
        public bool IsDataChanged => false;
#endif

        
    }
}