﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PFR_INVEST.Web2.ClassLibrary
{
    public class JournalLogger
    {
        public static void LogChangeStateEvent(IWebView model, string msg)
        {
            if (model == null || model.State == ViewModelState.Create)
                return;

            var header = GetHeader(model);
            var fullMsg = string.IsNullOrEmpty(header) ? msg : header + ", " + msg;
            LogEvent(fullMsg, JournalEventType.CHANGE_STATE, model.ID, model.GetType().Name, null, model.GetModelEntityType());
        }
    }
}
