﻿using System;
using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;

namespace PFR_INVEST.Web2.ClassLibrary.Attributes
{
    public abstract class ViewModelAccessAttribute : Attribute
    {
        protected List<DOKIP_ROLE_TYPE> roles = new List<DOKIP_ROLE_TYPE>();
        public List<DOKIP_ROLE_TYPE> Roles
        {
            get
            {
                return roles;
            }

            set
            {
                roles = value;
            }
        }

        protected ViewModelAccessAttribute(params DOKIP_ROLE_TYPE[] roles)
        {
            this.roles.Clear();
            this.roles.AddRange(roles);
        }
    }

    public class ReadAccessAttribute : ViewModelAccessAttribute
    {
        public ReadAccessAttribute(params DOKIP_ROLE_TYPE[] roles)
            : base(roles)
        {
        }
    }

    public class EditAccessAttribute : ViewModelAccessAttribute
    {
        public EditAccessAttribute(params DOKIP_ROLE_TYPE[] roles)
            : base(roles)
        {
        }
    }

    public class DeleteAccessAttribute : ViewModelAccessAttribute
    {
        public DeleteAccessAttribute(params DOKIP_ROLE_TYPE[] roles)
            : base(roles)
        {
        }
    }

}