﻿using System;
using System.Collections.Generic;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web
{
    public class RegisterWeb: IIdentifiable, IStatusEntity
    {
        public ICollection<FinregisterWeb> Finregisters { get; set; }

        public virtual ApproveDocWeb ApproveDoc { get; set; }

        public virtual PortfolioWeb Portfolio { get; set; }

        public virtual RegisterWeb Return { get; set; }


        public long ID { get; set; }

        public string PayAssignment { get; set; }

        public long? FZ_ID { get; set; }

        public string Kind { get; set; }

        public string RegNum { get; set; }

        public long? PortfolioID { get; set; }

        public long? ApproveDocID { get; set; }

        public DateTime? RegDate { get; set; }

        public string Comment { get; set; }

        public string Company { get; set; }

        public long? ERZL_ID { get; set; }

        public string Content { get; set; }

        public string TrancheNumber { get; set; }

        public DateTime? TrancheDate { get; set; }

        public long? ReturnID { get; set; }

        public long StatusID { get; set; }

        public int? IsCustomArchive { get; set; }
    }
}