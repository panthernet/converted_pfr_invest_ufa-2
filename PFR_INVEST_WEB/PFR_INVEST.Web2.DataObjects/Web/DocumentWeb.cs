﻿using System;
using System.Collections.Generic;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web
{
    public class DocumentWeb: IIdentifiable, IStatusEntity
    {
        public List<AttachWeb> Attaches { get; set; }
        public virtual LegalEntityWeb LegalEntity { get; set; }
        public virtual DocumentClassWeb DocumentClass { get; set; }
        public virtual PersonWeb Executor { get; set; }
        public virtual AdditionalDocumentInfo AdditionalInfo { get; set; }
        public virtual DocFileBodyWeb DocFileBody { get; set; }

        /// <summary>
        /// Псевдо статусы. Используется для фильтрации выборки.
        /// </summary>
        public enum Statuses 
        {       
            /// <summary>
            /// Все
            /// </summary>
            All = 1,
            /// <summary>
            /// Активные(без даты выполнения)
            /// </summary>
            Active = 2,
            /// <summary>
            /// Выполненые(с датой выполнения)
            /// </summary>
            Executed = 3,
            /// <summary>
            /// Контроль(с датой контроля)
            /// </summary>
            Control = 4
        }

        /// <summary>
        /// Типы документации
        /// </summary>
        public enum Types
        {
            All = 0,
            /// <summary>
            /// SI = 1
            /// </summary>
			SI = 1,
			/// <summary>
			/// NPF = 2
			/// </summary>
            Npf = 2,
			/// <summary>
			/// VR = 3
			/// </summary>
            VR = 3,
            OPFR = 4,
            Other =  10
        }

        public long ID { get; set; }

        public string IncomingNumber { get; set; }

        public DateTime? IncomingDate { get; set; }

        public long? LegalEntityID { get; set; }

        public string OutgoingNumber { get; set; }

        public DateTime? OutgoingDate { get; set; }

        public long? DocumentClassID { get; set; }

        public long? AdditionalInfoID { get; set; }

        public long? ExecutorID { get; set; }

        public string ExecutorName { get; set; }

        public DateTime? ExecutionDate { get; set; }

        public string AdditionalExecutionInfo { get; set; }

        public string AdditionalInfoText { get; set; }
                  
        public DateTime? ControlDate { get; set; }

        public string OriginalStoragePlace { get; set; }

        public string Comment { get; set; }

        public int TypeID { get; set; }

        public Types Type
        {
            get { return (Types)this.TypeID; }
            set { this.TypeID = (int)value; }
        }

        public long StatusID { get; set; } = 1;

        //TODO public string AdditionalInfo { get; set; }

    }
}
