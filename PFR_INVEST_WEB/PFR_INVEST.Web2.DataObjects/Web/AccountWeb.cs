﻿using System.Collections.Generic;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web
{
    public class AccountWeb: IIdentifiable
    {
        public IList<FinregisterWeb> Finregisters { get; set; }
        public IList<FinregisterWeb> FinregistersD { get; set; }
        public virtual ContragentWeb Contragent { get; set; }

        public long ID { get; set; }
        
        public string Name { get; set; }
        
        public long? ContragentID { get; set; }
        
        public long? AccountTypeID { get; set; }
    }
}
