﻿using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web
{
    public class AdditionalDocumentInfoWeb: IIdentifiable
    {
        public virtual long ID { get; set; }

        public virtual string Name { get; set; }
    }
}
