﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web
{
    public class BankAccountWeb: IIdentifiable
    {
        public virtual LegalEntityWeb LegalEntity {get; set; }

        public virtual long ID { get; set; }

        public virtual long? PfrBranchID { get; set; }

        public virtual string LegalEntityName { get; set; }

        public virtual string AccountNumber { get; set; }

        public virtual string BankName { get; set; }

        public virtual string BankLocation { get; set; }

        public virtual string CorrespondentAccountNumber { get; set; }

        public virtual string CorrespondentAccountNote { get; set; }

        public virtual string BIK { get; set; }

        public virtual string INN { get; set; }

        public virtual string KPP { get; set; }

        public virtual long? LegalEntityID { get; set; }

        public virtual DateTime? CloseDate { get; set; }

        public virtual string FormalizedName => LegalEntity?.FormalizedName;
       
        public virtual string CorrespondentAccountFull => string.Join(" ", new [] {CorrespondentAccountNumber, CorrespondentAccountNote}).Trim();

        public virtual bool IsEqual(BankAccountWeb to)
        {
            return ID == to.ID && PfrBranchID == to.PfrBranchID && LegalEntityName == to.LegalEntityName && AccountNumber == to.AccountNumber && BankName == to.BankName
                   && BankLocation == to.BankLocation && CorrespondentAccountNumber == to.CorrespondentAccountNumber && CorrespondentAccountNote == to.CorrespondentAccountNote
                   && BIK == to.BIK && INN == to.INN && KPP == to.KPP && LegalEntityID == to.LegalEntityID && CloseDate == to.CloseDate && FormalizedName == to.FormalizedName;

        }
    }
}
