﻿using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web.ListItems
{
    public class FinregisterWithCorrAndNPFWebListItem
    {
        /// <summary>
        /// ID финреестра
        /// </summary>
        public long? ID { get; set; }

        /// <summary>
        /// сущность финреестра
        /// </summary>
        public FinregisterWeb Finregister { get; set; }

        /// <summary>
        /// наименование НПФ
        /// </summary>
        public string NPFName { get; set; }

        /// <summary>
        /// статус НПФ
        /// </summary>
        public string NPFStatus { get; set; }

        /// <summary>
        /// количество ЗЛ
        /// </summary>
        public long? ZLCount { get; set; }

        /// <summary>
        /// сумма финреестра
        /// </summary>
        public decimal? Count { get; set; }

        /// <summary>
        /// сумма с учетом корректировки
        /// </summary>
        public decimal? CorrCount { get; set; }

        public decimal? CFRCount { get; set; }

        public bool IsItemChanged { get; set; }

        public bool IsNewItem { get; set; }
    }
}
