﻿using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web.ListItems
{
    public class NameValueListItem: IIdentifiable
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string INN { get; set; }
    }
}
