﻿using System;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web
{
    public class AttachWeb: IIdentifiable, IStatusEntity
    {
        public virtual DocumentWeb Document { get; set; }

        public virtual PersonWeb Executor { get; set; }

        public virtual AttachClassificWeb AttachClassific { get; set; }

        public long ID { get; set; }

        public long? DocumentID { get; set; }

        public long? AttachClassificID { get; set; }

        public string Additional { get; set; }

        public long? ExecutorID { get; set; }

        public string ExecutorName { get; set; }

        public DateTime? ControlDate { get; set; }

        public DateTime? ExecutionDate { get; set; }

        public string AdditionalToExecution { get; set; }

        public long? OriginalPlaceID { get; set; }

        public string Comment { get; set; }

        public long? FileContentId { get; set; }

        public int TypeID { get; set; }

        public long StatusID { get; set; }

        /// <summary>
        /// Поле только для вычитки из базы
        /// </summary>
        public string OriginalPlace { get; set; }

        public DocumentWeb.Types Type
        {
            get { return (DocumentWeb.Types)this.TypeID; }
            set { this.TypeID = (int)value; }
        }

    }
}
