﻿using System.Collections.Generic;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web
{
    public class FileExport1CWeb : IIdentifiable
    {
        public virtual long ID { get; set; }

        public virtual long SessionID { get; set; }

        public virtual int EntityType { get; set; }

        public virtual long EntityID { get; set; }

        public virtual long ExportID { get; set; }

        public enum OnesExportType
        {
            Finregister = 0,
            ReqTransfer = 1,
            Deposit = 2,
            ReqTransferVr = 3,
            Opfr = 4
        }

        public virtual bool IsOfType(OnesExportType type)
        {
            return EntityType == (int) type;
        }
    }
}
