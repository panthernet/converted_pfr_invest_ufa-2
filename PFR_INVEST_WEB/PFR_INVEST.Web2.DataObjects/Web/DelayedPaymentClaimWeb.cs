﻿using System;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web
{
    public class DelayedPaymentClaimWeb : IIdentifiable, IStatusEntity
    {
        public virtual LegalEntityWeb Npf { get; set; }
        public virtual ElementWeb FZ { get; set; }

        public long ID { get; set; }

        public string UFONumber { get; set; }

        public DateTime UFODate { get; set; }

        public string LetterNumber { get; set; }

        public DateTime? LetterDate { get; set; }

        public long? FZID { get; set; }

        public string Name { get; set; }

        public long? NpfID { get; set; }

        public decimal? Ammount { get; set; }

        public long? ZLCount { get; set; }

        public string PRFNumber { get; set; }

        public string Comment { get; set; }

        public long StatusID { get; set; }

        public DateTime? RestrictStartDate { get; set; }

        public DateTime? RestrictEndDate { get; set; }

        public DateTime? PfrDate { get; set; }

        public string NpfName => Npf?.FormalizedName;
        public string FZName => FZ?.Name;
        public string Inn => Npf?.INN;
    }
}
