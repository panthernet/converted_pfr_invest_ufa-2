﻿using System;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web
{
    public class ReturnWeb : IIdentifiable
    {
       // public virtual List<RegisterWeb> Registers { get; set; }

        public long ID { get; set; }

        public long? BudgetID { get; set; }

        public long? PFRBankAccountID { get; set; }

        public DateTime? Date { get; set; }

        public DateTime? DIDate { get; set; }

        public decimal? Sum { get; set; }

        public string Recipient { get; set; }

        public string Number { get; set; }

        public string Comment { get; set; }

        public long? PortfolioID { get; set; }
    }
}
