﻿using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web
{
    public class ElementWeb: IIdentifiable
    {
        public long ID { get; set; }

        public long Key { get; set; }

        public Types Type => (Types)Key;

        public int? Order { get; set; }

        public string Name { get; set; }

        public short Visible { get; set; }

        public bool IsVisible => Visible == 1;

        /// <summary>
        /// Преобразует направление передачи реестра НПФ в идентификатор направления Element
        /// </summary>
        /// <param name="kind">Направление</param>
        public static long KindToCommonDirection(string kind){
            return kind == RegisterIdentifier.PFRtoNPF ? (long)SpecialDictionaryItems.CommonTransferDirectionFromPFR : (long)SpecialDictionaryItems.CommonTransferDirectionToPFR;
        }

        /// <summary>
        /// Преобразует направление передачи реестра СИ/ВР в идентификатор направления Element
        /// </summary>
        /// <param name="direction">Направление</param>
        public static long SiDirectionToCommonDirection(SPNDirection direction)
        {
            return direction.isFromUKToPFR == true ? (long) SpecialDictionaryItems.CommonTransferDirectionToPFR : (long) SpecialDictionaryItems.CommonTransferDirectionFromPFR;
        }

        public enum Types
        {
            SIDocumentStoragePlaces = 1,
            SIDocumentAdditionalExecutionInfo = 2,

            NpfDocumentStoragePlaces = 10,
            NpfDocumentAdditionalExecutionInfo = 11,

            /// <summary>
            /// Поле "ФЗ" Заявки по задержанным выплатам
            /// </summary>
            FZ = 20,
            /// <summary>
            /// Комментарии к возвращаемым депозитам
            /// </summary>
            DepositComment=30,

            VRDocumentStoragePlaces = 40,
            VRDocumentAdditionalExecutionInfo = 41,
            StatusF140 = 50,

            RegisterNPFtoPFR = 60,
            RegisterPFRtoNPF = 61,

			TempAllocation = 62,
			///// <summary>
			///// Биржи для аукционов
			///// </summary>
			//AuctionStock = 70

			KDocPeriod = 80,

            /// <summary>
            /// Значимость счета в привязке к портфелю (основной - не основной)
            /// </summary>
            AccountAssignKind = 90,

			/// <summary>
			/// Связь для п/п
			/// </summary>
			PPLink = 100,
			/// <summary>
			/// Направление для п/п
			/// </summary>
			PPDirection = 101,
			
			/// <summary>
			/// Разделы приложения для  п/п
			/// </summary>
			PPSection = 102,

            /// <summary>
            /// Факт внесения НПФ в систему гарантирования АСВ
            /// </summary>
            GarantACB = 103,

            /// <summary>
            /// Тип уведомления о назначении НЧТП
            /// </summary>
            PensionNotificationType = 104,

            /// <summary>
            /// Тип биржи
            /// </summary>
            StockType = 106,

            /// <summary>
            /// Тип операции для реестра СИ/ВР раздела Отзыв средств (Годовой план и т.д.)
            /// </summary>
            RegisterOperationType = 107,

			/// <summary>
			/// Тип портфеля
			/// </summary>
			PortfolioType = 108,

            /// <summary>
            /// Раздел приложения (СИ, НПФ, ВР, Депозиты...)
            /// </summary>
            AppPart = 109,

            /// <summary>
            /// Общее направление перевода (из ПФР, в ПФР)
            /// </summary>
            CommonTransferDirection = 110,

			/// <summary>
            /// Тип документа (п/п по депозитам, импортированные из 1С)
			/// http://jira.dob.datateh.ru/browse/DOKIPIV-965
            /// </summary>
            PODocumentType = 111,
            OpfrRegType = 113,
            OpfrDirection = 112,
            OpfrTransferStatus = 114,
            BankConclusionNames = 115
        }

        public enum SpecialDictionaryItems: long
        {
            /// <summary>
            /// Выплаты правопреемникам
            /// </summary>
            PaymentZLType = 107001L,
            /// <summary>
            /// Выплатной резерв (в т.ч. План передачи средств для формирования выплатного резерва)
            /// </summary>
            PaymentReserve = 107002L,

            PPDirIncome = 101001L,
            PPDirOutcome = 101002L,

            /// <summary>
            /// Раздел приложения - Работа с СИ
            /// </summary>
            AppPartSI = 109001L,
            /// <summary>
            /// Раздел приложения - Работа с ВР
            /// </summary>
            AppPartVR = 109002L,
            /// <summary>
            /// Раздел приложения - Работа с НПФ
            /// </summary>
            AppPartNPF = 109003L,
            /// <summary>
            /// Раздел приложения - Работа с ЦБ и Депозитами
            /// </summary>
            AppPartDEPO = 109004L,

            /// <summary>
            /// Общее направление перевода из ПФР в НПФ/УК/ГУК
            /// </summary>
            CommonTransferDirectionFromPFR = 110001L,
            /// <summary>
            /// Общее направление перевода в ПФР из НПФ/УК/ГУК
            /// </summary>
            CommonTransferDirectionToPFR = 110002L,
            /// <summary>
            /// Система гарантирования, элемент списка 'Все'
            /// </summary>
            ACBAll = 103003L,
            OpfrDirectionToPfr = 112002L,
            OpfrDirectionFromPfr = 112001L,
            OpfrRegTypeRasp = 113001L,
            OpfrTransferStatusInitial = 114001L,
            OpfrTransferStatusIssued = 114002L,
        }
    }
}
