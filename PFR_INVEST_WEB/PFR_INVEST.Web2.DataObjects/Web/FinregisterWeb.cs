﻿using System;
using System.Collections.Generic;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web
{
    public class FinregisterWeb: IIdentifiable, IStatusEntity
    {
        public virtual RegisterWeb Register { get; set; }
        public virtual AccountWeb AccountCredit { get; set; }
        public virtual AccountWeb AccountDebit { get; set; }
        public List<AsgFinTrWeb> AsgFinTrs { get; set; }
        public List<AsgFinTrExportWeb> AsgFinTrExports { get; set; }

        public int IsTransfered { get; set; }

        public long ID { get; set; }

        public long? RegisterID { get; set; }

        public DateTime? Date { get; set; }

        public decimal? Count { get; set; } = 0;

        public decimal? CFRCount { get; set; }

        public long CrAccID { get; set; }

        public long DbtAccID { get; set; }

        public string Status { get; set; }

        public DateTime? LetterDate { get; set; }

        public string LetterNum { get; set; }

        public long? ZLCount { get; set; }

        public DateTime? FinDate { get; set; }

        public string FinNum { get; set; }

        public string FinMoveType { get; set; }

        public string RegNum { get; set; }

        public long? ParentFinregisterID { get; set; }

        public long? SelectedContentID { get; set; }
        
        public long? SelectedRegisterID { get; set; }

        public string Comment { get; set; }

        public long StatusID { get; set; }

    }
}