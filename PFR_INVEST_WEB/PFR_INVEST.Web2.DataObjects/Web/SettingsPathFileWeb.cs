﻿using System;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web
{
    public class SettingsPathFileWeb: IIdentifiable
    {
        public enum IdDefinition
        {
            OnesImport = 2,
            OnesExport = 1,
            PkipImport = 4,
            PkipExport = 3
        }

        public enum FileLocation
        {
            Client = 1,
            Server = 2
        }

        public virtual long ID { get; set; }

        public virtual DateTime Date { get; set; }

        public virtual int ServClient { get; set; }

        public virtual string Path { get; set; }

        public virtual string Comment { get; set; }

        public virtual FileLocation ServClientValue
        {
            get => (FileLocation)ServClient;
            set => ServClient = (int)value;
        }

    }

}
