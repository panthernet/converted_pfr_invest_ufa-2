﻿namespace PFR_INVEST.Web2.DataObjects.Web.Enums
{
    public enum WebServiceDataErrorWeb
    {
        /// <summary>
        /// Без ошибки
        /// </summary>
        None,
        /// <summary>
        /// Финреестр в статусе Не передан и имеет дочерние финреестры
        /// </summary>
        FinregisterHasChildren,
        /// <summary>
        /// Финреестр находится в статусе, отличном от Создан
        /// </summary>
        FinregisterNotInCreatedStatus,
        /// <summary>
        /// Для финреестра заданы п/п
        /// </summary>
        FinregisterHasPP,
        /// <summary>
        /// У реестре присутствуют финреестры в статусе, отличном от начального
        /// </summary>
        RegisterHasFinregWithInvStatus,
        /// <summary>
        /// Ошибка удаления документа
        /// </summary>
        DocumentDeleteError,
        /// <summary>
        /// Неизвестный тип данных для операции восстановления записей из архива
        /// </summary>
        UnknownUndeleteType,
        /// <summary>
        /// Неизвестное исключение
        /// </summary>
        GeneralException,
        /// <summary>
        /// Папка не найдена
        /// </summary>
        DirectoryNotFound,
        SessionInProgress
    }

}
