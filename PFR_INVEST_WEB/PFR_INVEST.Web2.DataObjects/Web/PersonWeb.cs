﻿using System.Globalization;
using System.Linq;
using System.Text;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web
{
    public class PersonWeb: IIdentifiable, IStatusEntity
    {
        public long ID { get; set; }

        public string LastName { get; set; }

		public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string Position { get; set; }

        public string Phone { get; set; }

		public string Email { get; set; }

        public string Fax { get; set; }

		public long? DivisionId { get; set; }

		public int? IsDisabledFlag { get; set; }

        public long StatusID { get; set; }

        public bool IsDisabled
        {
            get { return IsDisabledFlag == 1; }
            set
            {
                if (IsDisabled != value)
                {
                    IsDisabledFlag = value ? 1 : 0;
                }
            }
        }

        public string FIOShort
        {
            get
            {
                var fio = (!string.IsNullOrEmpty(LastName.Trim()) ? LastName.Trim() + " " : "") + (!string.IsNullOrEmpty(FirstName) ? (FirstName.ToUpper().First() + ".") : "");

                if (!string.IsNullOrEmpty(MiddleName))
                    fio += (" " + MiddleName.ToUpper().First() + ".").Trim();
                return fio;
            }
        }

        public string FullName
        {
            get
            {
                var sb = new StringBuilder();

                if (!string.IsNullOrEmpty(LastName))
                {
                    sb.Append(LastName);
                    sb.Append(" ");
                }
                if(!string.IsNullOrEmpty(FirstName))
                    sb.Append(FirstName);

                if (!string.IsNullOrEmpty(MiddleName))
                {
                    sb.Append(" ");
                    sb.Append(MiddleName);
                }

                return sb.ToString();
            }
        }

        public string FormattedFullName
        {
            get
            {                
                StringBuilder sb = new StringBuilder();

                sb.Append(string.IsNullOrEmpty(LastName) ? string.Empty : CultureInfo.CurrentCulture.TextInfo.ToTitleCase(LastName).Trim() + " ");

                sb.Append(string.IsNullOrEmpty(FirstName) ? string.Empty : CultureInfo.CurrentCulture.TextInfo.ToTitleCase(FirstName.Trim()).First() + ". ");

                sb.Append(string.IsNullOrEmpty(MiddleName) ? string.Empty : CultureInfo.CurrentCulture.TextInfo.ToTitleCase(MiddleName.Trim()).First() + ".");

                
                return sb.ToString();
            }
        }
    }

}
