﻿using System;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web
{
    public class PensionNotificationWeb : IIdentifiable, IStatusEntity
    {
        public virtual LegalEntityWeb Sender { get; set; }

        public virtual ElementWeb NotifType { get; set; }

        public long ID { get; set; }

        public long SenderID { get; set; }

        public DateTime? DeliveryDate { get; set; }

        /// <summary>
        /// Ссылка на тип уведомления (ELEMENT) 
        /// </summary>
        public long? NotifTypeID { get; set; }

        public string NotifNum { get; set; }

        public DateTime? NotifDate { get; set; }

        public string RegisterNum { get; set; }

        public DateTime? RegisterDate { get; set; }

        public long StatusID { get; set; }

        public string LegalEntityName => Sender?.FormalizedName;

        public string NotifTypeName => NotifType?.Name;

        public bool IsDeleted => StatusID == -1;
    }
}
