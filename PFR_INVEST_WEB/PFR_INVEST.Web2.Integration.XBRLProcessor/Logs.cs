﻿using log4net;

namespace PFR_INVEST.Integration.XBRLProcessor
{
    public static class Logs
    {
        internal static readonly ILog Logger = LogManager.GetLogger("db2connector");

    }
}
