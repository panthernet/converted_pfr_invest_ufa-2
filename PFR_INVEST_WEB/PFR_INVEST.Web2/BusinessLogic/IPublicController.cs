using System.Web.Mvc;

namespace PFR_INVEST.Web2.BusinessLogic
{
    public interface IPublicController
    {
        RedirectToRouteResult RedirectToAction(string action, string controller);
        ActionResult ViewPublic(string action, string controller);
    }
}