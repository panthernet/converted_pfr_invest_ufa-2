﻿using System;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.Web2.BusinessLogic.Models;

namespace PFR_INVEST.Web2.BusinessLogic
{
    public static class JournalLogger
    {
        public static void LogModelEvent(PureWebViewModel model, JournalEventType type)
        {
            LogModelEvent(model, type, string.Empty, string.Empty);
        }

        public static void LogArchiveEvent(PureWebViewModel model)
        {
            if (model == null) return;
            if (model.State != ViewModelState.Create)
                LogModelEvent(model, JournalEventType.ARCHIVE);
        }

        public static void LogArchiveEvent(PureWebViewModel model, string modelName)
        {
            if (model == null) return;
            if (model.State != ViewModelState.Create)
                LogModelEvent(model, JournalEventType.ARCHIVE, modelName);
        }

        public static void LogModelEvent(PureWebViewModel model, JournalEventType type, string message, string additionalInfo = null)
        {
            if (model == null) return;
            var documentId = (model as PureWebFormViewModel)?.ID ?? 0;
            var entityType = model.GetModelEntityType();
            var entityDoc = model.GetModelEntityJComment();
            PFR_INVEST.BusinessLogic.Journal.JournalLogger.LogEvent(message, type, documentId, model.GetType().Name, additionalInfo, entityType, entityDoc);           
        }

        public static void LogStatusChangeEvent(PureWebViewModel model, long id, string msg)
        {
            if (model == null || model.State == ViewModelState.Create)
                return;
            PFR_INVEST.BusinessLogic.Journal.JournalLogger.LogEvent(msg, JournalEventType.CHANGE_STATE, id, model.GetType().Name, null, model.GetModelEntityType());
        }

        public static void LogEvent(string msg, JournalEventType state, long id, string modelType, string empty, Type type, string addinfo)
        {
            PFR_INVEST.BusinessLogic.Journal.JournalLogger.LogEvent(msg, state, id, modelType, null, type, addinfo);
        }
    }
}