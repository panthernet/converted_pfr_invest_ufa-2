﻿using System;
using System.IO;
using System.Web;
using System.Web.UI;
using DevExpress.Web;
using DevExpress.Web.Mvc;

namespace PFR_INVEST.Web2.BusinessLogic.Binders
{
    public class CorrespFileUploadBinder: DevExpressEditorsBinder 
    {
        public const string UPLOAD_DIRECTORY = "~/Content/Upload/";

        public static readonly UploadControlValidationSettings UploadValidationSettings = new UploadControlValidationSettings {
            MaxFileCount = 1, MaxFileSize =  1024 * 1024 * 4
        };

        public CorrespFileUploadBinder() {
         //   UploadControlBinderSettings.ValidationSettings.Assign(UploadValidationSettings);
            UploadControlBinderSettings.FileUploadCompleteHandler += ucMultiSelection_FileUploadComplete;
        }

        private void ucMultiSelection_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if(!e.UploadedFile.IsValid) return;

            var fname = $"{Path.GetFileNameWithoutExtension(e.UploadedFile.FileName)}_{Guid.NewGuid():N}{Path.GetExtension(e.UploadedFile.FileName)}";
            var path = Path.Combine(UPLOAD_DIRECTORY, fname);
            var filePath = HttpContext.Current.Request.MapPath(path);

            if (!Directory.Exists(Path.GetDirectoryName(filePath)))
                Directory.CreateDirectory(Path.GetDirectoryName(filePath));

            if(File.Exists(filePath))
                File.Delete(filePath);

            using (var file = File.Open(filePath, FileMode.Create))
            {
                e.UploadedFile.FileContent.Seek(0, SeekOrigin.Begin);
                e.UploadedFile.FileContent.CopyTo(file);
            }

            var urlResolver = sender as IUrlResolutionService;
            if (urlResolver != null)
            {
                e.CallbackData = $"{urlResolver.ResolveClientUrl(path)}|{e.UploadedFile.FileName}";
            }
        }
    }
}