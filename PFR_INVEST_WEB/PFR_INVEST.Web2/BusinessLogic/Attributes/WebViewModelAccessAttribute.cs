﻿using System;
using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;

namespace PFR_INVEST.Web2.BusinessLogic.Attributes
{
    public abstract class WebViewModelAccessAttribute : Attribute
    {
        protected List<DOKIP_ROLE_TYPE> roles = new List<DOKIP_ROLE_TYPE>();
        public List<DOKIP_ROLE_TYPE> Roles
        {
            get
            {
                return roles;
            }

            set
            {
                roles = value;
            }
        }

        protected WebViewModelAccessAttribute(params DOKIP_ROLE_TYPE[] roles)
        {
            this.roles.Clear();
            this.roles.AddRange(roles);
        }
    }

    public class WebReadAccessAttribute : WebViewModelAccessAttribute
    {
        public WebReadAccessAttribute(params DOKIP_ROLE_TYPE[] roles)
            : base(roles)
        {
        }
    }

    public class WebEditAccessAttribute : WebViewModelAccessAttribute
    {
        public WebEditAccessAttribute(params DOKIP_ROLE_TYPE[] roles)
            : base(roles)
        {
        }
    }

    public class WebDeleteAccessAttribute : WebViewModelAccessAttribute
    {
        public WebDeleteAccessAttribute(params DOKIP_ROLE_TYPE[] roles)
            : base(roles)
        {
        }
    }

}