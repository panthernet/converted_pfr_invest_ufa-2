﻿using System.Collections.Generic;

namespace PFR_INVEST.Web2.BusinessLogic.Models
{
    public interface IWebViewModel: IWebBaseViewModel
    {
        bool EditAccessDenied { get; }

        /// <summary>
        /// Доп. параметр для AJAX запроса перезагрузки формы по запросу
        /// Нужен для передачи данных, например при обновлении формы при выборе элемента комбо-бокса
        /// </summary>
        int AjaxRequestParamValue { get; set; }

        long WebGetEntryId();

        /// <summary>
        /// Указывает, что модель была успешно сохранена
        /// </summary>
        bool IsSaveCompleted { get; }
        /// <summary>
        /// Указывает, что модель была успешно удалена
        /// </summary>
        bool IsDeleteCompleted { get; }

        string WebLastModelError { get; set; }

        bool IsDataChanged { get; }
        string WebSave();
        /// <summary>
        /// Выполняется после создания шаблона формы
        /// </summary>
        void WebPostModelCreateAction();

        void WebPostModelUpdateAction();

        string WebTitle { get; set; }
        /// <summary>
        /// Набор данных для форм, ассоциированных со списком
        /// </summary>
        IList<NameEntity> FormData { get; }

        /// <summary>
        /// Ссылка на экшн контроллера
        /// </summary>
        string AjaxAction { get; }

        /// <summary>
        /// Идентификатор формы
        /// </summary>
        string FormId { get; set; }

        int FormWidth { get; }
        int FormHeight { get; }
        bool IsModalEx { get; set; }

        /// <summary>
        /// Видимость кнопки удаления 
        /// </summary>
        bool IsDeleteButtonVisible { get; set; }

        void WebDelete(long delID);
    }
}