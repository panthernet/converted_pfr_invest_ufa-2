﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.UI;
using DevExpress.Utils;
using DevExpress.Web;
using DevExpress.Web.Mvc;
using PFR_INVEST.Web2.BusinessLogic.Models;

namespace PFR_INVEST.Web2.BusinessLogic
{
    public static class FrontEndHelper
    {
        public const string DECIMAL_MASK = "# ### ### ### ### ### ##0.00"; //19
        public const string LONG_MASK = "# ### ### ### ### ### ###"; //19

        #region Ribbon elements enabled

        public enum RibbonArea
        {
            Dictionary = 0
        }

        /// <summary>
        /// not used
        /// </summary>
        /// <param name="column"></param>
        /// <param name="context"></param>
        /// <param name="mask"></param>
        /// <param name="zerovalue"></param>
        public static void SetGridColumnMask(MVCxGridViewColumn column, ViewContext context, string mask, string zerovalue = "0.00")
        {
            column.SetDataItemTemplateContent(c =>
            {
                var text = c.Text == "&nbsp;" ? "" : c.Text;
                context.Writer.Write(string.IsNullOrEmpty(text) ? zerovalue : text.Replace("&nbsp;", " ").FormatWithMask(mask).Replace(" ", "&nbsp;"));
            });
        }

        /// <summary>
        /// Генерация строки скрипта для двойному клику по гриду с открытием формы
        /// </summary>
        /// <param name="prms">Наименование свойств данных элемента списка для выборки</param>
        /// <param name="childFormId">Строковый идентификатор открываемой формы</param>
        /// <param name="controller">Наименование контроллера</param>
        public static string GetGridRowDblClickString(string prms, string childFormId, string controller)
        {
            return $"function(s,e) {{ OnGlobalGridViewEntryDblClick(s, '{prms}', '{childFormId}', '{controller}'); }}";
        }

        /// <summary>
        /// Генерация строки скрипта для начала отправки данных грида
        /// </summary>
        /// <param name="useFunc">Использовать обертку JS функции</param>
        public static string GetGridBeginCallbackString(bool useFunc = true)
        {
            return $"{(useFunc ? "function(s,e) {{" : "")} e.customArgs['DeleteID'] = deleteEntryId; deleteEntryId=null; {(useFunc ? "}}" : "")}";
        }

        /// <summary>
        /// Генерация строки скрипта для смены строки фокуса в гриде
        /// </summary>
        /// <param name="formId">Строковый идентификатор формы</param>
        /// <param name="incSelect">Включить обработку выбора элемента</param>
        /// <param name="incDelete">Включить обработку удаления элемента</param>
        public static string GetGridFocusedRowString(string formId, bool incSelect = true, bool incDelete = false, string delButtonName =null)
        {
            var text = "";
            if (incSelect)
                text = $" listFocusedRowChanged(s,e); ";
            if (incDelete)
            {
                delButtonName = !string.IsNullOrEmpty(delButtonName) ? delButtonName : $"btnDelete{formId}";
                text += $" OnGlobalGridViewSelectionChanged(s,'{delButtonName}','listParentEntryName', '{formId}'); ";
            }

            return $"function(s,e) {{ {text} }}";
        }

        public static string GetGridFocusedRowString2(string formId, string controller, bool incSelect = true, bool incDelete = false, string delButtonName =null, string customAction = null)
        {
            var text = "";
            if (incSelect)
                text = $"debugger;listParentEntryId = s.GetRowKey(s.GetFocusedRowIndex());listParentEntryName = s.name;var result = listParentEntryId != null;globalUpdateButtonsVisibility(result, '{controller}'); {customAction} ";
            if (incDelete)
            {
                delButtonName = !string.IsNullOrEmpty(delButtonName) ? delButtonName : $"btnDelete{formId}";
                text += $" OnGlobalGridViewSelectionChanged(s,'{delButtonName}','listParentEntryName', '{formId}'); ";
            }

            return $"function(s,e) {{ {text} }}";
        }



        public static string GetGridDefaultDblClickRowString(IList<NameEntity> data)
        {
            return $"function(s,e) {{ listRowDoubleClick(s,e,'{(data.Count == 0 ? "" : data[0].Id)}'); }}";
        }

        public static string GetReqParamsCallbackJSString(ViewDataDictionary vd)
        {
            var result = new StringBuilder();
                result.Append("function(s,e){ ");
//            if (!string.IsNullOrEmpty((string)vd["ID"]))
                //result.Append($" e.requestParams['ID']='{vd["ID"]}'; ");

            if (!string.IsNullOrEmpty((string)vd["FormID"]))
                result.Append($" e.requestParams['FormID']='{vd["FormID"]}'; ");

            if (!string.IsNullOrEmpty(vd["EntryID"]?.ToString()))
                result.Append($" e.requestParams['EntryID']='{vd["EntryID"]}'; ");

            if (!string.IsNullOrEmpty((string)vd["ParentID"]))
                result.Append($" e.requestParams['ParentID']='{vd["ParentID"]}'; ");

            if (!string.IsNullOrEmpty(vd["ParentEntryID"]?.ToString()))
                result.Append($" e.requestParams['ParentEntryID']='{vd["ParentEntryID"]}'; ");

            result.Append(" }");
            return result.ToString();
        }



        //private const string DIC_ELEMENTS = "DicNpfCreateAccount|DicNpfCreateReorg||DicNpfStopZL|DicNpfBranch|dlgNpfIdentifier|DicNpfCreateNpfContact|DicNpfCreateNpfCourier";
        public static bool IsRibbonElementEnabled(string name)
        {
             return FormDataHelper.Data.ContainsKey(name) && FormDataHelper.Data[name].IsEnabled;
        }
        #endregion

        public static string GetGridViewBeginCallBack(string id, bool includeDelete = false)
        {
            var delete = includeDelete ? "  e.customArgs[\"DeleteID\"] = deleteEntryId; deleteEntryId=null; " : null;
            return $@"function (s,e) {{ e.customArgs[""IsRefresh""] = true; e.customArgs[""FormID""] = '{id}'; {delete}}}";
        }

        public static string GetGridViewBeginCallBack(string id, string entryId, bool includeDelete)
        {
            var delete = includeDelete ? "  e.customArgs[\"DeleteID\"] = deleteEntryId; deleteEntryId=null; " : null;
            var entry = !string.IsNullOrEmpty(entryId) ? $"  e.customArgs[\"EntryID\"] = {entryId}; " : null;
            return $@"function (s,e) {{ e.customArgs[""IsRefresh""] = true; e.customArgs[""FormID""] = '{id}'; {delete} {entry}}}";
        }

        public static void GenerateModalClickNoSaveScript(ButtonSettings settings, string id, string field, int layer)
        {      
            settings.ClientSideEvents.Click = $@"function (s,e) {{ eval(onPopupClosed{id})({field},'{id}'); backNotifyPopupClose{layer}(); }}";
        }
        public static void GenerateModalClickSaveScript(ButtonSettings settings, string id, string field)
        {
            settings.ClientSideEvents.Click = $@"function (s,e) {{ eval(onPopupClosed{id})({field},'{id}'); eval('btnSubmit{id}').click(); }}";
        }
        //$('#hiddenSubmit{id}').click(function (e) {{ e.preventDefault(); }});

        public static void SetTextBoxMask(TextBoxSettings settings, string mask)
        {
            settings.Properties.MaxLength = 0;
            settings.Properties.MaskSettings.Mask = mask;
        }

        public static void GenerateSimpleSubmitButtonSettings(ButtonSettings settings, IWebViewModel model, string valueCommand, int layer)
        {
            settings.Name = "btnSubmit" + model.FormId;
            settings.Text = $"Сохранить {(model.IsModalEx ? null : " форму")}";
            settings.ControlStyle.CssClass = "padding4";
            //settings.UseSubmitBehavior = true;
            if (model.EditAccessDenied)
                settings.ClientEnabled = false;
            settings.Width = 200;
            FrontEndHelper.GenerateModalClickNoSaveScript(settings, model.FormId, valueCommand, layer);
        }

        public static void GenerateSimpleCancelButtonSettings(ButtonSettings settings, IWebViewModel model, int layer)
        {
            GenerateCloseButtonSettings(settings, model, layer);
            settings.ClientSideEvents.Click = $"function (s,e) {{ backNotifyPopupClose{layer}(); }}";
        }


        public static void SetupInboundGridViewSettings(GridViewSettings settings, ViewContext viewContext, int height = 150, string keyId = "ID")
        {
            SetupDefaultGridViewSettings(settings, viewContext, keyId);
            settings.Settings.ShowGroupPanel = false;
            settings.Settings.ShowFilterRow = false;
            settings.Settings.ShowHeaderFilterButton = true;
            settings.SettingsPopup.HeaderFilter.Height = 200;
            settings.Settings.VerticalScrollableHeight = height;

            settings.Settings.VerticalScrollBarMode = ScrollBarMode.Auto;
            //remove autoadjust for inbound grids
            settings.ClientSideEvents.Init = "";
            settings.ClientSideEvents.EndCallback = "";
            settings.Styles.Header.Wrap = DefaultBoolean.False; 
            // settings.ControlStyle.CssClass = string.IsNullOrEmpty(cssclass) ? "gridviewmaxheight200" : cssclass;
        }


        public static void SetupInboundAutoGridViewSettings(GridViewSettings settings, ViewContext viewContext, string keyId = "ID")
        {
            SetupDefaultGridViewSettings(settings, viewContext, keyId);
            settings.Settings.ShowGroupPanel = false;
            settings.Settings.ShowFilterRow = false;
            settings.Settings.ShowHeaderFilterButton = true;
            settings.SettingsPopup.HeaderFilter.Height = 200;
            settings.Settings.VerticalScrollBarMode = ScrollBarMode.Auto;
            settings.Settings.VerticalScrollableHeight = 400;
            //remove autoadjust for inbound grids
            settings.ClientSideEvents.Init = "";
            settings.ClientSideEvents.EndCallback = "";
            settings.Styles.Header.Wrap = DefaultBoolean.False;            

            // settings.ControlStyle.CssClass = string.IsNullOrEmpty(cssclass) ? "gridviewmaxheight200" : cssclass;
        }

        public static void SetupGridViewSummaries(GridViewSettings settings)
        {
            settings.Settings.ShowFooter = true;
        }


        public static void SetupDefaultGridViewSettings(GridViewSettings settings, ViewContext viewContext, string keyName = "ID")
        {
            settings.Width = System.Web.UI.WebControls.Unit.Percentage(100);
            //settings.Height = System.Web.UI.WebControls.Unit.Pixel(400);
            //settings.Settings.VerticalScrollBarMode = ScrollBarMode.Auto;
           // settings.Settings.HorizontalScrollBarMode = ScrollBarMode.Visible;
            settings.ClientSideEvents.Init = "OnGridViewInit";
            settings.ClientSideEvents.EndCallback = "OnGridViewEndCallback";

            settings.SettingsResizing.ColumnResizeMode = ColumnResizeMode.NextColumn;
            settings.SettingsResizing.Visualization = ResizingMode.Live;

            settings.SettingsBehavior.AllowEllipsisInText = true;

            settings.KeyFieldName = keyName;
            settings.SettingsPager.Visible = true;
            settings.SettingsPager.Mode = GridViewPagerMode.ShowPager;
            settings.SettingsPager.PageSize = 100;
            settings.Settings.ShowGroupPanel = true;
            settings.Settings.ShowFilterRow = true;
            settings.Settings.ShowFilterRowMenu = true;
            settings.Settings.ShowFilterBar = GridViewStatusBarMode.Auto;
            settings.CommandColumn.Visible = false;
            settings.SettingsBehavior.AllowSelectByRowClick = true;
            settings.SettingsBehavior.AllowFocusedRow = true;

            settings.Styles.Header.Wrap = DefaultBoolean.True;            
            settings.SettingsPager.EnableAdaptivity = true;
            settings.Settings.VerticalScrollBarMode = ScrollBarMode.Visible;
            settings.Settings.VerticalScrollableHeight = 0;

            settings.SetGroupRowContentTemplateContent(c =>
            {
                var gText = c.GroupText;
                viewContext.Writer.Write(gText + " " + c.SummaryText);
            });

        }

        public static bool GetIsDataElementVisible(object item, ViewDataDictionary viewData)
        {
            var xmlElement = (item as IHierarchyData)?.Item as System.Xml.XmlElement;
            if (xmlElement != null)
            {
                var roles = xmlElement.Attributes.GetNamedItem("Roles") == null ? new string[0] : xmlElement.Attributes["Roles"].Value.Split('|');
                var bRoles = viewData.ContainsKey("Roles") ? ((string)viewData["Roles"]).Split('|') : null;
                return !roles.Any() || (bRoles != null && bRoles.Any() && (bRoles.Intersect(roles).Any() || bRoles.Contains("PTK_DOKIP_ADMINISTRATORS")));
            }
            return false;
        }

        public static string GetDisplayName<T>(Expression<Func<T, string>> propertyExpression)
        {
            return GetPropertyAttributeValue<T, string, DisplayAttribute, string>(propertyExpression, attr => attr.Name);
        }

        //Optional Extension method
        public static string GetDisplayName<T>(this T instance, Expression<Func<T, string>> propertyExpression)
        {
            return GetDisplayName<T>(propertyExpression);
        }


        //Required generic method to get any property attribute from any class
        public static TValue GetPropertyAttributeValue<T, TOut, TAttribute, TValue>(Expression<Func<T, TOut>> propertyExpression, Func<TAttribute, TValue> valueSelector) where TAttribute : Attribute
        {
            var expression = (MemberExpression)propertyExpression.Body;
            var propertyInfo = (PropertyInfo)expression.Member;
            var attr = propertyInfo.GetCustomAttributes(typeof(TAttribute), true).FirstOrDefault() as TAttribute;

            if (attr == null)
            {
                throw new MissingMemberException(typeof(T).Name + "." + propertyInfo.Name, typeof(TAttribute).Name);
            }

            return valueSelector(attr);
        }

        public static AjaxOptions GenerateDefaultAjaxOptions(IWebViewModel model)
        {
            return new AjaxOptions
            {
                HttpMethod = "POST",
                OnBegin = $"loadingPanel.Show(); ",
                OnComplete = $"loadingPanel.Hide();",
                OnFailure = "loadingPanel.Hide(); ",
                UpdateTargetId = "contentBody"+model.FormId,
                InsertionMode = InsertionMode.Replace
            };
        }

        public static object GenerateDefaultAjaxStyle(IWebViewModel model, string customId = null)
        {
            if (model.FormHeight == 0 && model.FormWidth == 0)
                return new
                {
                    id = "validationForm"+(customId ?? model.FormId),
                    @class = "edit_form",
                };

            var w = model.FormWidth > 0 ?  $"width: {model.FormWidth}px;" : null;
            var h = model.FormHeight > 0 ? $"height: {model.FormHeight}px;" : null;
            return new
            {
                id = "validationForm"+ (customId ?? model.FormId),
                @class = "edit_form",
                style = $"{w} {h}"
            };
        }

        public static LoadingPanelSettings GenerateLoadingPanelSettings()
        {
            var x =  new LoadingPanelSettings
            {
                    Name = "loadingPanel",
                    Modal = true,
                    ContainerElementID = "validationForm"
            };
            x.Styles.LoadingDiv.BackColor = Color.Transparent;
            return x;
        }

        public static LoadingPanelSettings GenerateExtLoadingPanelSettings()
        {
            var x = new LoadingPanelSettings
            {
                Name = "loadingPanelExt",
                Modal = true,
                ContainerElementID = "page-content-wrapper",
                Text = "Обработка запроса..."
            };
            x.Styles.LoadingDiv.BackColor = Color.Transparent;
            return x;
        }

        public static void GenerateLabelSettings(LabelSettings settings, int width, IWebViewModel model = null, string propname = null)
        {
            settings.Width = width;
            settings.ControlStyle.CssClass = "label";
            if( model != null && model.IsRequired(settings.AssociatedControlName))
                settings.Text=$"{settings.Text}*";
        }

        public static void GenerateLabelSettings(LabelSettings settings, string text, int width)
        {
            settings.Text = text;
            settings.Width = width;
            settings.ControlStyle.CssClass = "label";
        }

        public static Action<TextBoxSettings> GenerateHiddenTextBoxSettings(string name = null)
        {
            return settings=>
            {
                settings.ClientVisible = false;
                if(!string.IsNullOrEmpty(name))
                    settings.Name = name;
            };
        }

        public static void GenerateHiddenTextBoxSettings(TextBoxSettings settings, string name = null)
        {
                settings.ClientVisible = false;
                if (!string.IsNullOrEmpty(name))
                    settings.Name = name;
        }


        public static void GenerateTextSpineditSettings(SpinEditSettings settings, int width, IWebViewModel model, string css = null)
        {
            if (!string.IsNullOrEmpty(css))
                settings.ControlStyle.CssClass = "css";
            settings.Properties.ClientSideEvents.KeyDown = "function(s, e) { if (e.htmlEvent.keyCode == 13) ASPxClientUtils.PreventEventAndBubble(e.htmlEvent); }";
            settings.Width = width;
            settings.ShowModelErrors = true;
            settings.Properties.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
            settings.Properties.SpinButtons.Visible = false;
            settings.Properties.ClearButton.ClientVisible = false;
            if(settings.ClientEnabled)
                settings.ClientEnabled = !model.EditAccessDenied;
        }

        public static void GenerateTextBoxSettings(TextBoxSettings settings, int width, IWebViewModel model, string css = null)
        {
            if (!string.IsNullOrEmpty(css))
                settings.ControlStyle.CssClass = "css";
            settings.Properties.ClientSideEvents.KeyDown = "function(s, e) { if (e.htmlEvent.keyCode == 13) ASPxClientUtils.PreventEventAndBubble(e.htmlEvent); }";
            settings.Width = width;
            settings.ShowModelErrors = true;
            settings.Properties.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
            if(settings.ClientEnabled)
                settings.ClientEnabled = !model.EditAccessDenied;
            if(string.IsNullOrEmpty(settings.Properties.MaskSettings.Mask))
                settings.Properties.MaxLength = model.GetMaxLengthFromAttr(settings.Name);
        }

        public static void GenerateComboBoxSettings(ComboBoxSettings settings, int width, IWebViewModel model, string textField, string valueField)
        {
           // var t = model.GetValueType(settings.Properties.DataMember)
            //return GenerateComboBoxSettings(settings, width, model, textField, valueField, t);
        }

        public static void GenerateComboBoxSettings(ComboBoxSettings settings, int width, IWebViewModel model, string textField, string valueField, Type valueType)
        {
            settings.Properties.ClientSideEvents.KeyDown = "function(s, e) { if (e.htmlEvent.keyCode == 13) ASPxClientUtils.PreventEventAndBubble(e.htmlEvent); }";
            settings.Width = width;
            settings.ShowModelErrors = true;
            settings.Properties.AllowUserInput = false;
            settings.Properties.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
            if(settings.ClientEnabled)
                settings.ClientEnabled = !model.EditAccessDenied;
            settings.Properties.ClientSideEvents.ValueChanged = "function(s,e) { $('#'+s.GetInputElement().id).keyup(); }";

            settings.Properties.TextField = textField;
            settings.Properties.ValueField = valueField;
            settings.Properties.ValueType = valueType;
            //settings.SelectedIndex = 0;
        }

        public static void GenerateComboBoxSettings(ComboBoxSettings settings, int width, IWebViewModel model, Type valueType)
        {
            GenerateComboBoxSettings(settings, width, model, null, null, valueType);
        }

        public static void GenerateReadonlyTextBoxSettings(TextBoxSettings settings, int width, IWebViewModel model, bool isEnabled = true, string css = null)
        {
            GenerateTextBoxSettings(settings, width, model, css);
            //settings.ReadOnly = true;
            settings.ClientEnabled = false;// isEnabled;
        }

        public static void GenerateCheckBoxSettings(CheckBoxSettings settings, string text, IWebViewModel model)
        {
            if(!string.IsNullOrEmpty(text))
                settings.Text = text;
            settings.ShowModelErrors = true;
            settings.Properties.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
            if(settings.ClientEnabled)
                settings.ClientEnabled = !model.EditAccessDenied;
        }


        public static void GenerateDateEditSettings(DateEditSettings settings, int width, IWebViewModel model, string css = null)
        {
            if (!string.IsNullOrEmpty(css))
                settings.ControlStyle.CssClass = css;
            settings.Properties.ClientSideEvents.KeyDown = "function(s, e) { if (e.htmlEvent.keyCode == 13) ASPxClientUtils.PreventEventAndBubble(e.htmlEvent); }";
            settings.Properties.ClientSideEvents.ValueChanged = "function(s,e) { $('#'+s.GetInputElement().id).keyup(); }";
            settings.Properties.EnableClientSideAPI = true;
            settings.Width = width;
            settings.ShowModelErrors = true;
            settings.Properties.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
            if(settings.ClientEnabled)
                settings.ClientEnabled = !model.EditAccessDenied;
        }

        public static void GenerateReadonlyDateEditSettings(DateEditSettings settings, int width, IWebViewModel model, string css = null)
        {
            GenerateDateEditSettings(settings, width, model);
            //settings.ReadOnly = true;
            settings.ClientEnabled = false;
        }

        public static void GenerateDisabledDateEditSettings(DateEditSettings settings, int width, IWebViewModel model, string css = null)
        {
            GenerateDateEditSettings(settings, width, model);
            settings.ClientEnabled = false;
        }

        public static void GenerateMemoSettings(MemoSettings settings, int width, int height, IWebViewModel model, string css = null)
        {
            if(!string.IsNullOrEmpty(css))
                settings.ControlStyle.CssClass = "css";
            settings.Width = width;
            settings.Height = height;
            settings.ShowModelErrors = true;
            settings.Properties.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
            if(settings.ClientEnabled)
                settings.ClientEnabled = !model.EditAccessDenied;
        }

        public static ButtonSettings GenerateSubmitButtonSettings(IWebViewModel model, Action<ButtonSettings> action = null)
        {
            var settings = new ButtonSettings
            {
                Name = "btnSubmit"+model.FormId,
                ClientVisible = false,
                UseSubmitBehavior = true
            };
            settings.Width = 100;
            settings.ControlStyle.CssClass = "padding4";
            if(settings.ClientEnabled)
                settings.ClientEnabled = !model.EditAccessDenied;
            action?.Invoke(settings);
            return settings;
        }

        public static ButtonSettings GenerateVisibleSubmitButtonSettings(IWebViewModel model, Action<ButtonSettings> action = null)
        {
            var settings = GenerateSubmitButtonSettings(model, action);
            settings.ClientVisible = true;
            return settings;
        }

        public static void GenerateButtonSettings(ButtonSettings settings, int width, string text, string name, IWebViewModel model)
        {
            settings.Text = text;
            settings.Name = name;
            settings.Width = width;
            settings.ControlStyle.CssClass = "padding4";
            if(settings.ClientEnabled)
                settings.ClientEnabled = !model?.EditAccessDenied ?? false;
        }

        public static void GenerateCloseButtonSettings(ButtonSettings settings, IWebViewModel model, int layer = 0)
        {
            settings.Text = "Отмена";
            settings.Name = "btnCancel" + model.FormId;
            settings.Width = 100;
            settings.ControlStyle.CssClass = "padding4";
            settings.ClientSideEvents.Click = $"function (s,e) {{ backNotifyPopupClose{(layer == 0 ? "" : layer.ToString())}(); }}";
        }

        public static void GenerateDeleteButtonSettings(ButtonSettings settings, IWebViewModel model, int layer = 0)
        {
            settings.Text = "Удалить";
            settings.Name = "btnDelete" + model.FormId;
            settings.Width = 100;
            settings.ControlStyle.CssClass = "padding4";
            
        }
    }
}