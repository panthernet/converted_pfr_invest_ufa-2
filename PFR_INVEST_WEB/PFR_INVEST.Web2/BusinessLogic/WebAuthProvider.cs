﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using PFR_INVEST.Auth.ClientData;

namespace PFR_INVEST.Web2.BusinessLogic
{
    public class WebAuthProvider: AuthProvider
    {
        public WebAuthProvider() : base(HttpContext.Current.User.Identity.GetUserName())
        {

        }
    }
}