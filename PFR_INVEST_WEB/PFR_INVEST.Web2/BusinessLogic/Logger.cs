﻿using System;
using System.IO;
using PFR_INVEST.Common.Logger;

namespace PFR_INVEST.Web2.BusinessLogic
{
    public static class Logger
    {
        private static Log _logger;
        private static Log SystemLogger => _logger ?? (_logger = new Log(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Logs","system.log")));
        private static Log DebugLogger => _logger ?? (_logger = new Log(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Logs","debug.log")));

        public static void Log(string message)
        {
            SystemLogger.WriteLine(message);
        }

        public static void LogEx(Exception ex, string message = null)
        {
            SystemLogger.WriteException(ex, message);
        }

        public static void LogDebug(string message)
        {
            DebugLogger.WriteLine(message);
        }

        public static Log GetSystemLoggerInstance()
        {
            return SystemLogger;
        }
    }
}