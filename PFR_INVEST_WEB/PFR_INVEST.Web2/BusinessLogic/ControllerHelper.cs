﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using DevExpress.XtraPrinting.Native;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using PFR_INVEST.Web2.BusinessLogic.Models;
using WebGrease.Css.Extensions;

namespace PFR_INVEST.Web2.BusinessLogic
{
    public static class ControllerHelper
    {
        public static ViewEngineCollection ViewEngineCollection { get; set; }

        #region GetPartialView by ID

        /// <summary>
        /// Возвращает частичное представление по идентификатору
        /// </summary>
        /// <typeparam name="T">Тип возвращаемых данных</typeparam>
        /// <param name="c">Контроллер</param>
        /// <param name="id">Идентификатор</param>
        /// <param name="entryId">Идентификатор записи</param>
        /// <param name="isRefresh"></param>
        /// <param name="model"></param>
        /// <param name="parentId">Идентификатор родителя</param>
        private static T GetPartialView<T>(Controller c, string id, long entryId, bool isRefresh, out object model, long parentId = 0)
        {
            model = null;

            //автоматическая привязка и создание модели согласнно данным в XML
            //формат записи:  МОДЕЛЬ|параметр1,параметр2,...
            //где параметры могут быть: id, parentId,  
            var data = FormDataHelper.Data[id];
            if (!string.IsNullOrEmpty(data.Model))
            {
                var modelName = data.Model;
                var fullModelName = "PFR_INVEST.Web2.Models." + modelName;
                var prms = data.ModelParams;
                var realParams = new List<object>();
                prms.ForEach(p =>
                {
                    switch (p.ToLower())
                    {
                        case "id":
                            realParams.Add(entryId);
                            break;
                        case "parentid":
                            realParams.Add(parentId);
                            break;
                        default:
                            throw new Exception($"Параметр {p} типа {fullModelName} не может быть создан! Проверьте маппинг XML файла!");
                    }
                });
                var modelObject = (IWebBaseViewModel)EntityCreator.CreateWebViewModel(fullModelName, realParams.Count == 0 ? null : realParams.ToArray());
                model = modelObject;
                return GetPartialViewInternal<T>(c, modelObject is IWebListViewModel && isRefresh ? modelObject.AjaxPartial : modelObject.AjaxMain, modelObject);
            }

            switch (id)
            {
                default:
                    return (T)(object)null;
            }
        }
        #endregion

        public static void SetupMenuAndRoles(ViewDataDictionary viewData)
        {
            var user = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByName(HttpContext.Current.User.Identity.GetUserName());
            viewData["XPath"] = "/items/*";
            viewData["Roles"] = user?.DokipRolesString;
        }

        public static void UpdateCulture()
        {
           /* if (Thread.CurrentThread.CurrentUICulture.Name != "ru")
            {
                var ci = CultureInfo.GetCultureInfo("ru");
                Thread.CurrentThread.CurrentUICulture = ci;
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(ci.Name);
            }*/
        }

        public static ActionResult LoadViewPartialBind(Controller c, string username)
        {
            var entryIda = !string.IsNullOrEmpty(c.Request.Params["EntryID"]) ? c.Request.Params["EntryID"] : "0";
            var parentEntryId = !string.IsNullOrEmpty(c.Request.Params["ParentEntryID"]) ? long.Parse(c.Request.Params["ParentEntryID"]) : 0;
            var parentId = c.Request.Params["ParentID"];
            var category = c.Request.Params["Category"];
            var id = !string.IsNullOrEmpty(c.Request.Params["FormID"]) ? c.Request.Params["FormID"] : null;
            var isModalEx = !string.IsNullOrEmpty(c.Request.Params["IsModalEx"]) && Convert.ToBoolean(c.Request.Params["IsModalEx"]);

            var entryId = string.IsNullOrEmpty(entryIda) ? 0 : long.Parse(entryIda);
            var isRefresh = !string.IsNullOrEmpty(c.Request.Params["IsRefresh"]) ? (bool?)bool.Parse(c.Request.Params["IsRefresh"]) : null;
            var layer = string.IsNullOrEmpty(c.Request.Params["Layer"]) ? 0 : int.Parse(c.Request.Params["Layer"]);


            ControllerHelper.UpdateCulture();

            //populate history
            if (id.ToLower().Contains("list"))
                CookieStore.UpdateHistoryCookie(username, id, category);

            object model;
            ActionResult result;
            var prms = c.Request.Params["InputParams"];
            if (string.IsNullOrEmpty(prms))
                result = GetPartialView<ActionResult>(c, id, entryId, isRefresh ?? false, out model, parentEntryId);
            else result = GetPartialView<ActionResult>(c, id, prms.Split('|').Cast<object>().ToArray(), isRefresh ?? false, out model);

            var viewModel = model as IWebViewModel;
            if (viewModel != null) viewModel.IsModalEx = isModalEx;

            Logger.LogDebug($"{nameof(LoadViewPartialBind)}: Result {result != null}");

            c.ViewData["FormID"] = id;
            c.ViewData["Layer"] = layer;
            c.ViewData["Title"] = (model as IWebViewModel)?.WebTitle ?? (model as IWebListViewModel)?.Title;
            c.ViewData["EntryID"] = entryId;
            c.ViewData["ParentEntryID"] = parentEntryId;
            c.ViewData["ParentID"] = parentId;
            c.ViewData["IsDeleteButtonVisible"] = viewModel != null && viewModel.IsDeleteButtonVisible;
            c.ViewData["Category"] = category;
            return result;
        }

        private static T GetPartialView<T>(Controller c, string id, object[] prms, bool isRefresh, out object model)
        {
            model = null;

            //автоматическая привязка и создание модели согласнно данным в XML
            //формат записи:  МОДЕЛЬ|параметр1,параметр2,...
            //где параметры могут быть: id, parentId,  
            var data = FormDataHelper.Data[id];
            if (!string.IsNullOrEmpty(data.Model))
            {
                var modelName = data.Model;
                var fullModelName = "PFR_INVEST.Web2.Models." + modelName;
                var modelObject = (IWebBaseViewModel)EntityCreator.CreateWebViewModel(fullModelName, prms);
                model = modelObject;
                return GetPartialViewInternal<T>(c, modelObject is IWebListViewModel && isRefresh ? modelObject.AjaxPartial : modelObject.AjaxMain, modelObject);
            }

            switch (id)
            {
                default:
                    return (T)(object)null;
            }
        }

        public static ActionResult LoadViewPartialBindDialogPopup(Controller c, string username, string id, string parentIdForSubmit, string layerstr, string category, string inputParams = null)
        {
            UpdateCulture();
            var layer = string.IsNullOrEmpty(layerstr) ? 0 : int.Parse(layerstr);
            var grids = new StringBuilder();
            if (!FormDataHelper.Data.ContainsKey(id))
            {
                Logger.Log("Unknown name id: " + id);
                throw new Exception("Unknown name id: " + id);
            }
            
            if (FormDataHelper.Data[id].HasGrids)
                FormDataHelper.Data[id].Grids.ForEach(g =>
                {
                    grids.Append($"if($('#{g}GridView').length>0) eval(\"{g}GridView\").Refresh(); ");
                });

            var divname = layer == 0 ? "popupdiv" : $"popupdiv{layer}";
            var divscrname = layer == 0 ? "popupscriptdiv" : $"popupscriptdiv{layer}";
            var addon = layer == 0 ? "" : layerstr;

            var prms = new PopupControlParams
            {
                Name = "PopupFormName" + addon,
                BeginCallback = "PopupFormBeginCallback" + addon,
                EndCallback = "PopupFormEndCallback" + addon,
                ShowMethod = "showPopup" + addon,
                ClosedMethod = "onPopupClosed" + id,
                FormId = id,
                FormTitle = FormDataHelper.Data[id].DisplayName,
                IsModal = true,
                IsResizable = true,
                Layer = layer,
                InputParams = inputParams
            };

            if (FormDataHelper.Data[id].ModalWidth > 0)
                prms.Width = FormDataHelper.Data[id].ModalWidth;

            c.ViewData["Layer"] = layer;

            string onCloseUpScript = null;
            if (parentIdForSubmit != null)
            {
                onCloseUpScript = $" debugger; SubmitAjaxForm(\"{parentIdForSubmit}\");";
            }

            var popupVariation = "_PopupControlPartialView";
            switch (FormDataHelper.Data[id].DialogMode)
            {
                case "DialogNoValidation":
                    popupVariation = "_PopupControlNoValidationPartialView";
                    break;
            }


            var res = new JsonResult
            {
                Data = new
                {
                    Html = RenderRazorViewToString(c, popupVariation, prms),
                    IsSaveEnabled = true,
                    Script = $@"<script>
                                function backNotifyPopupClose{addon}() {{ {prms.Name}.Hide(); }}
                                function backNotifyPopupSaveCompleted{addon}(id) {{ debugger; if (id === '{id}') {{ {prms.Name}.Hide(); {grids} }} }}
                                function backNotifyPopupClosed{addon}(id) {{ $(""#{divname}"").html(''); $(""#{divscrname}"").html(''); {onCloseUpScript} }}
                            </script>",
                    PopupShowMethod = prms.ShowMethod
                }
            };
            return res;
        }

        public static ActionResult LoadViewPartialBindPopup(Controller c, string username, string id, string entryId, string layerstr = null, string parentEntryId = null, string parentId = null, string category = "Dictionary")
        {
            //var entryIdlong = string.IsNullOrEmpty(entryIdString) ? 0 : long.Parse(entryIdString);
            Logger.LogDebug($"{nameof(LoadViewPartialBindPopup)}: Enter {id} {entryId}");
            ControllerHelper.UpdateCulture();

            var layer = string.IsNullOrEmpty(layerstr) ? 0 : int.Parse(layerstr);

            bool isList = id.ToLower().Contains("list");

            //populate history
            if (isList)
                CookieStore.UpdateHistoryCookie(username, id, category);
            var grids = new StringBuilder();

            if (!FormDataHelper.Data.ContainsKey(id))
            {
                Logger.Log("Unknown name id: " + id);
                throw new Exception("Unknown name id: " + id);
            }

            if (FormDataHelper.Data[id].HasGrids)
                FormDataHelper.Data[id].Grids.ForEach(g =>
                {
                    grids.Append($"if($('#{g}GridView').length>0) eval(\"{g}GridView\").Refresh(); ");
                });

            //if (_diverseCounter > int.MaxValue - 10) _diverseCounter = 1;
            //var addon = !string.IsNullOrEmpty(divname) ? (++_diverseCounter).ToString() : null;
            var divname = layer == 0 ? "popupdiv" : $"popupdiv{layer}";
            var divscrname = layer == 0 ? "popupscriptdiv" : $"popupscriptdiv{layer}";
            var addon = layer == 0 ? "" : layerstr;

            var prms = new PopupControlParams
            {
                Name = "PopupFormName" + addon,
                BeginCallback = "PopupFormBeginCallback" + addon,
                EndCallback = "PopupFormEndCallback" + addon,
                ShowMethod = "showPopup" + addon,
                ClosedMethod = "onPopupClosed" + id,
                FormId = id,
                ParentEntryId = long.Parse(parentEntryId ?? "0"), //parent form ID if any
                ParentId = parentId,
                FormTitle = isList || long.Parse(entryId ?? "0") != 0 ? FormDataHelper.Data[id].DisplayName : (FormDataHelper.Data[id].DisplayCreateName ?? FormDataHelper.Data[id].DisplayName),
                IsModal = true,
                IsResizable = true,
                Layer = layer
            };

            if (FormDataHelper.Data[id].ModalWidth > 0)
                prms.Width = FormDataHelper.Data[id].ModalWidth;

            c.ViewData["Layer"] = layer;

            string onCloseUpScript = null;
            if (parentId != null)
            {
                onCloseUpScript = $" debugger; SubmitAjaxForm(\"{parentId}\");";
            }

            var popupVariation = "_PopupControlPartialView";
            switch (FormDataHelper.Data[id].DialogMode)
            {
                case "DialogNoValidation":
                    popupVariation = "_PopupControlNoValidationPartialView";
                    break;
            }


            var res = new JsonResult
            {
                Data = new
                {
                    Html = RenderRazorViewToString(c, popupVariation, prms),
                    IsSaveEnabled = true,
                    Script = $@"<script>
                                function backNotifyPopupClose{addon}() {{ {prms.Name}.Hide(); }}
                                function backNotifyPopupSaveCompleted{addon}(id) {{ debugger; if (id === '{id}') {{ {prms.Name}.Hide(); {grids} }} }}
                                function backNotifyPopupClosed{addon}(id) {{ $(""#{divname}"").html(''); $(""#{divscrname}"").html(''); {onCloseUpScript} }}
                            </script>",
                    EntryId = entryId,
                    PopupShowMethod = prms.ShowMethod
                }
            };
            return res;
        }

        public static JsonResult LoadViewPartial(Controller c, string username, string id, string entryId = null, string category = "Dictionary")
        {
            Logger.LogDebug($"{nameof(LoadViewPartial)}: Enter {id} {entryId}");
            ControllerHelper.UpdateCulture();

            //populate history
            if (id.ToLower().Contains("list"))
                CookieStore.UpdateHistoryCookie(username, id, category);

            // ViewData["Layer"] = string.IsNullOrEmpty(Request.Params["Layer"]) ? 0 : int.Parse(Request.Params["Layer"]);


            object model;
            var id2 = string.IsNullOrEmpty(entryId) ? 0 : long.Parse(entryId);
            var result = GetPartialView<string>(c, id, id2, false, out model);
            Logger.LogDebug($"{nameof(LoadViewPartial)}: Result {result != null}");

            var viewModel = model as IWebViewModel;
            c.ViewData["FormID"] = id;
            c.ViewData["EntryID"] = entryId;
            c.TempData["Title"] = c.ViewData["Title"] = (model as IWebViewModel)?.WebTitle ?? (model as IWebListViewModel)?.Title;
            c.ViewData["IsDeleteButtonVisible"] = viewModel != null && viewModel.IsDeleteButtonVisible;

            return new JsonResult { Data = new
            {
                Html = result,
                Title = (model as IWebViewModel)?.WebTitle ?? (model as IWebListViewModel)?.Title,
                Success = result != null,
                IsAjax = true,
                IsSaveEnabled = model is IWebViewModel
            }};
        }

        private static PartialViewResult PartialView(Controller c, string viewName, object model)
        {
            if (model != null)
                c.ViewData.Model = model;
            var partialViewResult = new PartialViewResult
            {
                ViewName = viewName,
                ViewData = c.ViewData,
                TempData = c.TempData,
                ViewEngineCollection = ViewEngineCollection
            };
            return partialViewResult;
        }

        private static ViewResult View(Controller c, string viewName, string masterName, object model)
        {
            if (model != null)
                c.ViewData.Model = model;
            var viewResult = new ViewResult
            {
                ViewName = viewName,
                MasterName = masterName,
                ViewData = c.ViewData,
                TempData = c.TempData,
                ViewEngineCollection = ViewEngineCollection
            };
            return viewResult;
        }


        private static T GetPartialViewInternal<T>(Controller c, string path, object model)
        {
            if (typeof(T) == typeof(ActionResult))
                return (T)(object)PartialView(c, path.Split('/').Last().Replace(".cshtml", ""), model);
            return (T)(object)RenderRazorViewToString(c, path, model);
        }

        private static T GetPartialView<T>(Controller c, string id)
        {
            object model;
            return GetPartialView<T>(c, id, 0, false, out model);
        }

        private static string RenderRazorViewToString(Controller c, string viewName, object model)
        {
            c.ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(c.ControllerContext,
                    viewName);
                var viewContext = new ViewContext(c.ControllerContext, viewResult.View,
                    c.ViewData, c.TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(c.ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        public static void UpdateViewDataOnCallback(Controller c)
        {
            c.ViewData["Layer"] = !string.IsNullOrEmpty(c.Request.Params["Layer"]) ? long.Parse(c.Request.Params["Layer"]) : 0L;

            if(!string.IsNullOrEmpty(c.Request.Params["FormID"]))
                c.ViewData["FormID"] = c.Request.Params["FormID"];

            if(!string.IsNullOrEmpty(c.Request.Params["EntryID"]))
                c.ViewData["EntryID"] = c.Request.Params["EntryID"];

        }

        public static ActionResult GetPostView<T>(Controller c, string username, string view, T model, string partialView = null, bool save = true)
            where T : class, IWebViewModel
        {
            var delID = string.IsNullOrEmpty(c.Request.Params["deleteEntryIdHolder"]) ? 0L : long.Parse(c.Request.Params["deleteEntryIdHolder"]);
            //передаем данные о слое
            UpdateViewDataOnCallback(c);

            if (delID > 0)
            {
                model.WebDelete(delID);
                return PartialView(c, partialView ?? view, model);
            }

            if (model.EditAccessDenied)
            {
                Logger.Log($"Model save attempt with insufficient rights: {username} - {model.GetType().FullName}");
                return PartialView(c,  partialView ?? view, model);
            }

            var skipSave = !model.IsModalEx && !c.Request.Params.AllKeys.Contains("btnSubmit" + model.FormId);
            if (skipSave)
                return PartialView(c, partialView ?? view, model);

            if (!c.Request.IsAjaxRequest())
            { // Theme changing
                c.ModelState.Clear();
                return View(c, view, null, model);
            }

            if (c.ModelState.IsValid && save)
            {
                model.WebSave();
                if (!string.IsNullOrEmpty(model.WebLastModelError))
                    c.ModelState.AddModelError("InternalValidationError", new Exception(model.WebLastModelError));
                return PartialView(c, partialView ?? view, model);
            }

            if (!c.ModelState.IsValid && string.IsNullOrEmpty(model.WebLastModelError))
            {
                model.WebLastModelError= c.ModelState.Values.FirstOrDefault(a => a.Errors.Count > 0)?.Errors.FirstOrDefault()?.ErrorMessage;
                c.ModelState.AddModelError("InternalValidationError", new Exception(model.WebLastModelError));
            }

            return PartialView(c, partialView ?? view, model);
        }

    }
}