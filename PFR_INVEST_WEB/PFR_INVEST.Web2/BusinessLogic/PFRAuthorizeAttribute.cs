﻿using System.Linq;
using System.Web.Mvc;
using PFR_INVEST.Auth.SharedData.Auth;

namespace PFR_INVEST.Web2.BusinessLogic
{
	public class PFRAuthorizeAttribute : AuthorizeAttribute
	{
		public PFRAuthorizeAttribute(params DOKIP_ROLE_TYPE[] roles)
		{
			var names = roles.Select(UserSecurity.GetRoleADName).ToArray();
			Roles = string.Join(", ", names);
		}
	}
}