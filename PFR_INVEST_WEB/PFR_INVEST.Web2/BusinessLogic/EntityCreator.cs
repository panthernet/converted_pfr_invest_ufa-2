﻿using System;
using System.Linq;
using System.Linq.Expressions;
using PFR_INVEST.Web2.Models;

namespace PFR_INVEST.Web2.BusinessLogic
{
    public class EntityCreator
    {
        public delegate object ConstructorDelegate(params object[] args);

        public static ConstructorDelegate CreateConstructor(Type type, Type[] parameters = null)
        {
            // Get the constructor info for these parameters
            var constructorInfo = type.GetConstructor(parameters ?? Type.EmptyTypes);

            var paramExpr = Expression.Parameter(typeof(object[]));
            if (parameters != null)
            {
                // define a object[] parameter

                // To feed the constructor with the right parameters, we need to generate an array 
                // of parameters that will be read from the initialize object array argument.
                var constructorParameters = parameters.Select((paramType, index) =>
                    // convert the object[index] to the right constructor parameter type.
                        Expression.Convert(
                            // read a value from the object[index]
                            Expression.ArrayAccess(
                                paramExpr,
                                Expression.Constant(index)),
                            paramType)).ToArray();

                // just call the constructor.
                var body = Expression.New(constructorInfo, constructorParameters);
                return Expression.Lambda<ConstructorDelegate>(body, paramExpr).Compile();
            }
            else
            {
                var body = Expression.New(constructorInfo);
                return Expression.Lambda<ConstructorDelegate>(body, paramExpr).Compile();
            }
        }

        public static T CreateEntity<T>(params object[] prms)
            where T: class
        {
            var c = CreateConstructor(typeof(T), prms.Select(x => x.GetType()).ToArray());
            return c(prms) as T;
        }

        public static T CreateWebViewModel<T>(Type type,  params object[] prms)
            where T: class
        {
            var c = CreateConstructor(type, prms.Select(x => x.GetType()).ToArray());
            return c(prms) as T;
        }


        public static object CreateWebViewModel(string typename, object[] prms = null)
        {
            var asm = typeof(WebNpfViewModel).Assembly;
            var type = asm.GetType(typename);

            return prms == null ? CreateConstructor(type)() : CreateConstructor(type, prms.Select(x => x.GetType()).ToArray())(prms);
        }
    }
}