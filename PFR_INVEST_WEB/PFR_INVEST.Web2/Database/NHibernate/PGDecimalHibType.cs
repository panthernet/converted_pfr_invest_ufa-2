﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using NHibernate;
using NHibernate.Engine;
using NHibernate.SqlTypes;
using NHibernate.UserTypes;

namespace PFR_INVEST.Web2.Database.NHibernate
{
	public class PGDecimal4HibType : PGDecimalHibType
    {
		public PGDecimal4HibType() : base(4) { }
	}

	public class PGDecimalHibType : IUserType, IParameterizedType
	{
		public int Scale;

		public PGDecimalHibType()
			: this(0)
		{

		}

		public PGDecimalHibType(int pIScale)
		{
			Scale = pIScale;
		}

		object IUserType.Assemble(object cached, object owner)
		{
			return cached;
		}

	    public void NullSafeSet(DbCommand cmd, object value, int index, ISessionImplementor session)
	    {
	        if (value == null)
	        {
	            ((IDataParameter)cmd.Parameters[index]).Value = DBNull.Value;
	        }
	        else
	        {
	            decimal decimalValue = (decimal)value;
	            ((IDataParameter)cmd.Parameters[index]).Value = decimalValue;
	        }
	    }

	    object IUserType.DeepCopy(object value)
		{
			return value;
		}

		object IUserType.Disassemble(object value)
		{
			return value;
		}

		bool IUserType.Equals(object x, object y)
		{
			if (x == null)
			{
				if (y == null)
					return true;
				return false;
			}

			return x.Equals(y);
		}

		int IUserType.GetHashCode(object x)
		{
		    return x?.GetHashCode() ?? 0;
		}

	    public object NullSafeGet(DbDataReader rs, string[] names, ISessionImplementor session, object owner)
	    {
	        var obj = NHibernateUtil.Decimal.NullSafeGet(rs, names[0], session);

	        if (obj == null || obj == DBNull.Value)
	            return null;

	        return (decimal)obj;
	    }

	    bool IUserType.IsMutable => false;

	    public static object Locker = new object();


		object IUserType.Replace(object original, object target, object owner)
		{
			return original;
		}

		Type IUserType.ReturnedType => typeof(decimal);

	    SqlType[] IUserType.SqlTypes => new[] { SqlTypeFactory.Decimal };


	    void IParameterizedType.SetParameterValues(IDictionary<string, string> parameters)
		{
			if (parameters == null)
				return;

			if (parameters.ContainsKey("scale") == false)
				return;

			int iScale = Convert.ToInt32(parameters["scale"]);
			Scale = iScale;
		}

	}
}
