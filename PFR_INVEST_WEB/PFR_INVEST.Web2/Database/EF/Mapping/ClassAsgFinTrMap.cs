﻿using System.Data.Entity.ModelConfiguration;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Web2.DataObjects.Web;

namespace PFR_INVEST.Web2.Database.EF.Mapping
{
    public class ClassAsgFinTrMap : EntityTypeConfiguration<AsgFinTrWeb>
    {
        public ClassAsgFinTrMap()
        {
            // Primary Key
            HasKey(t => t.ID);

            // Table & Column Mappings
            ToTable("asg_fin_tr");
            Property(t => t.ID).HasColumnName("id");
            Property(t => t.FinregisterID).HasColumnName("fr_id");
            Property(t => t.DraftAmount).HasColumnName("draft_amount");
            Property(t => t.DraftDate).HasColumnName("draft_date");
            Property(t => t.DIDate).HasColumnName("didate");
            Property(t => t.DraftRegNum).HasColumnName("draft_regnum");
            Property(t => t.DraftPayDate).HasColumnName("draft_paydate");
            Property(t => t.PFRBankAccountID).HasColumnName("pba_id");
           // Property(t => t.PortfolioID).HasColumnName("pf_id");
            Property(t => t.Comment).HasColumnName("comment");
            Property(t => t.PayerIssueDate).HasColumnName("issue_date");
            Property(t => t.DirectionElID).HasColumnName("direction_el_id");
            Property(t => t.SectionElID).HasColumnName("section_el_id");
            Property(t => t.LinkElID).HasColumnName("link_el_id");
            Property(t => t.PaymentDetailsID).HasColumnName("paymentdetail_id");
            Property(t => t.LinkedRegisterID).HasColumnName("linkedregister_id");
            Property(t => t.ReqTransferID).HasColumnName("req_tr_id");
            Property(t => t.KBKID).HasColumnName("kbk_id");
            Property(t => t.Unlinked).HasColumnName("unlinked");
            Property(t => t.LotusTable).HasColumnName("lotus_table");
            Property(t => t.StatusID).HasColumnName("status_id");
            Property(t => t.InnContractID).HasColumnName("inn_contract_id");
            Property(t => t.InnLegalEntityID).HasColumnName("inn_legalentity_id");
            Property(t => t.ContragentTypeID).HasColumnName("contragent_type");

            Ignore(t => t.PayDate);
            Ignore(t => t.PayNumber);
            Ignore(t => t.PaySum);
            Ignore(t => t.SPNDate);
            Ignore(t => t.IsMigrated);
            Ignore(t => t.IsMigratedFromSaldo);
            Ignore(t => t.IsSelected);
            Ignore(t => t.IsSelectable);
            Ignore(t => t.ContragentName);

            //TODO
            Ignore(t => t.CBPaymentDetail);

            //  HasRequired(t => t.AsgFinTrExport).WithRequiredDependent(t => t.AsgFinTr);
            HasOptional(t => t.Portfolio).WithOptionalDependent().Map(t=> t.MapKey("pf_id"));
        }
    }

    public class ClassAsgFinTrExportMap : EntityTypeConfiguration<AsgFinTrExportWeb>
    {
        public ClassAsgFinTrExportMap()
        {
            // Primary Key
            HasKey(t => t.ID);

            // Table & Column Mappings
            ToTable("asg_fin_tr");
            Property(t => t.ID).HasColumnName("id");
            Property(t => t.FinregisterID).HasColumnName("fr_id");
            Property(t => t.DraftAmount).HasColumnName("draft_amount");
            Property(t => t.PortfolioID).HasColumnName("pf_id");
            Property(t => t.DirectionElID).HasColumnName("direction_el_id");
            Property(t => t.KBKID).HasColumnName("kbk_id");
            Property(t => t.StatusID).HasColumnName("status_id");
        }
    }
}