﻿using System.Data.Entity.ModelConfiguration;
using PFR_INVEST.Web2.DataObjects.Web;

namespace PFR_INVEST.Web2.Database.EF.Mapping
{
    public class ClassPersonMap : EntityTypeConfiguration<PersonWeb>
    {
        public ClassPersonMap()
        {
            // Primary Key
            HasKey(t => t.ID);

            // Table & Column Mappings
            ToTable("person");
            Property(t => t.ID).HasColumnName("id");
            Property(t => t.LastName).HasColumnName("last_name");
            Property(t => t.FirstName).HasColumnName("first_name");
            Property(t => t.MiddleName).HasColumnName("middle_name");
            Property(t => t.Position).HasColumnName("position");
            Property(t => t.Phone).HasColumnName("phone");
            Property(t => t.Email).HasColumnName("email");
            Property(t => t.Fax).HasColumnName("fax");
            Property(t => t.DivisionId).HasColumnName("division");
            Property(t => t.IsDisabledFlag).HasColumnName("is_disabled");
            Property(t => t.StatusID).HasColumnName("status");

            Ignore(t => t.IsDisabled);
            Ignore(t => t.FIOShort);
            Ignore(t => t.FullName);
            Ignore(t => t.FormattedFullName);
        }
    }
}