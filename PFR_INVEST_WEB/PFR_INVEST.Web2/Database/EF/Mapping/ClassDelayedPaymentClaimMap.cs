﻿using System.Data.Entity.ModelConfiguration;
using PFR_INVEST.Web2.DataObjects.Web;

namespace PFR_INVEST.Web2.Database.EF.Mapping
{
    public class ClassDelayedPaymentClaimtMap : EntityTypeConfiguration<DelayedPaymentClaimWeb>
    {
        public ClassDelayedPaymentClaimtMap()
        {
            // Primary Key
            HasKey(t => t.ID);

            // Table & Column Mappings
            ToTable("delayed_payment_claim");
            Property(t => t.ID).HasColumnName("id");
            Property(t => t.UFONumber).HasColumnName("ufonumber").HasMaxLength(50);
            Property(t => t.UFODate).HasColumnName("ufodate");
            Property(t => t.LetterNumber).HasColumnName("letternumber").HasMaxLength(50);
            Property(t => t.LetterDate).HasColumnName("letterdate");
            Property(t => t.FZID).HasColumnName("fz_id").IsRequired();
            Property(t => t.Name).HasColumnName("name").HasMaxLength(50).IsRequired();
            Property(t => t.NpfID).HasColumnName("npf_id").IsRequired();
            Property(t => t.Ammount).HasColumnName("ammount").IsRequired();
            Property(t => t.ZLCount).HasColumnName("zlcount").IsRequired();
            Property(t => t.PRFNumber).HasColumnName("pfrnumber").HasMaxLength(50);
            Property(t => t.Comment).HasColumnName("comment").HasMaxLength(50);
            Property(t => t.RestrictStartDate).HasColumnName("restrictstartdate");
            Property(t => t.RestrictEndDate).HasColumnName("restrictenddate");
            Property(t => t.PfrDate).HasColumnName("pfrdate");
            Property(t => t.StatusID).HasColumnName("status");

            Ignore(t => t.NpfName);
            Ignore(t => t.FZName);
            Ignore(t => t.Inn);

            HasRequired(t => t.Npf);
            HasRequired(t => t.FZ);
        }
    }
}