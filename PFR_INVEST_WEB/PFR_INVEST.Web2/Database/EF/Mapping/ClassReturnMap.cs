﻿using System.Data.Entity.ModelConfiguration;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Web2.DataObjects.Web;

namespace PFR_INVEST.Web2.Database.EF.Mapping
{
    public class ClassReturnMap : EntityTypeConfiguration<ReturnWeb>
    {
        public ClassReturnMap()
        {
            // Primary Key
            HasKey(t => t.ID);

            // Table & Column Mappings
            ToTable("return");
            Property(t => t.ID).HasColumnName("id");
            Property(t => t.BudgetID).HasColumnName("id_budg");
            Property(t => t.PFRBankAccountID).HasColumnName("id_pfr_ba");
            Property(t => t.Number).HasColumnName("num");
            Property(t => t.Sum).HasColumnName("sum");
            Property(t => t.Date).HasColumnName("date");
            Property(t => t.DIDate).HasColumnName("didate");
            Property(t => t.Comment).HasColumnName("comment");
            Property(t => t.Recipient).HasColumnName("recipient");
            Property(t => t.PortfolioID).HasColumnName("pf_id");

           // HasMany(t => t.Registers).WithOptional(t => t.Return).HasForeignKey(t => t.ReturnID);
        }
    }
}