﻿using System.Data.Entity.ModelConfiguration;
using PFR_INVEST.Web2.DataObjects.Web;

namespace PFR_INVEST.Web2.Database.EF.Mapping
{
    public class ClassLegalEntityAccountMap : EntityTypeConfiguration<LegalEntityWeb>
    {
        public ClassLegalEntityAccountMap()
        {
            // Primary Key
            HasKey(t => t.ID);

            // Table & Column Mappings
            ToTable("legalentity");
            Property(t => t.ID).HasColumnName("id");
            Property(t => t.FullName).HasColumnName("fullname");
            Property(t => t.PDReason).HasColumnName("pdreason");
            Property(t => t.INN).HasColumnName("inn");
            Property(t => t.HeadPosition).HasColumnName("headposition");
            Property(t => t.HeadFullName).HasColumnName("headfullname");
            Property(t => t.LegalAddress).HasColumnName("legaladdress");
            Property(t => t.StreetAddress).HasColumnName("streetaddress");
            Property(t => t.PostAddress).HasColumnName("postaddress");
            Property(t => t.Phone).HasColumnName("phone");
            Property(t => t.Fax).HasColumnName("fax");
            Property(t => t.Comment).HasColumnName("comment");
            Property(t => t.EAddress).HasColumnName("eaddress");
            //Property(t => t.ContragentID).HasColumnName("contragentid");
            Property(t => t.RegistrationDate).HasColumnName("registrationdate");
            Property(t => t.EndDate).HasColumnName("enddate");
            Property(t => t.CloseDate).HasColumnName("closedate");
            Property(t => t.ShortName).HasColumnName("shortname");
            Property(t => t.RegistrationNum).HasColumnName("registrationnum");
            Property(t => t.FormalizedName).HasColumnName("formalizedname");
            Property(t => t.Site).HasColumnName("site");
            Property(t => t.Subsidiaries).HasColumnName("subsidiaries");
            Property(t => t.Registrator).HasColumnName("registrator");
            Property(t => t.OKPP).HasColumnName("okpp");
            Property(t => t.DeclName).HasColumnName("declname");
            Property(t => t.LetterWho).HasColumnName("letterwho");
            Property(t => t.TransfDocKind).HasColumnName("transfdockind");
            Property(t => t.LegalStatusID).HasColumnName("ls_id");
            Property(t => t.PFBA_ID).HasColumnName("pfba_id");
            Property(t => t.Info).HasColumnName("info");
            Property(t => t.PFRAGR).HasColumnName("pfragr");
            Property(t => t.PFRAGRDATE).HasColumnName("pfragrdate");
            Property(t => t.PFRAGRSTAT).HasColumnName("pfragrstat");
            Property(t => t.Money).HasColumnName("money");
            Property(t => t.MoneyDate).HasColumnName("moneydate");
            Property(t => t.Fitch).HasColumnName("fitch");
            Property(t => t.FitchDate).HasColumnName("fitchdate");
            Property(t => t.Standard).HasColumnName("standard");
            Property(t => t.StandardDate).HasColumnName("standarddate");
            Property(t => t.Moody).HasColumnName("moody");
            Property(t => t.MoodyDate).HasColumnName("moodydate");
            Property(t => t.CorrAcc).HasColumnName("corracc");
            Property(t => t.CorrNote).HasColumnName("corraccountnote");
            Property(t => t.BIK).HasColumnName("bik");
            Property(t => t.ForLetter).HasColumnName("forletter");
            Property(t => t.Sex).HasColumnName("sex");
            Property(t => t.StockCode).HasColumnName("stockcode");
            Property(t => t.GarantACBID).HasColumnName("garant_el_id");
            Property(t => t.FundNum).HasColumnName("fund_num");
            Property(t => t.FundDate).HasColumnName("fund_date");
            Property(t => t.NotifNum).HasColumnName("notif_num");
            Property(t => t.NotifDate).HasColumnName("notif_date");
            Property(t => t.FundDate).HasColumnName("fund_date");
            Property(t => t.FundDate).HasColumnName("fund_date");
            Property(t => t.NumberForDocuments).HasColumnName("number_for_docs");

            HasMany(t => t.Documents).WithRequired(t => t.LegalEntity).HasForeignKey(t => t.LegalEntityID);

            Ignore(t => t.Limit4Money);
            Ignore(t => t.FormalizedNameFull);
            Ignore(t => t.GarantName);
            Ignore(t => t.OldName);
            Ignore(t => t.ContragentID);
            Ignore(t => t.StatusID);
        }
    }
}