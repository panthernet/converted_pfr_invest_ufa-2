﻿using System;
using System.Linq;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Server;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Proxy;

namespace db2connector
{
    public partial class IISServiceHib
    {
        public IQueryable<RegisterListItem2> WebLazyGetSpnRegistersArchiveList()
        {
           // return SessionWrapper(s =>
           // {
            var s = DataAccessSystem.OpenSession();
            var q = 
                    from r in s.Query<Register>()
                    //from fr in s.Query<Finregister>().Where(a=> a.RegisterID == r.ID)
                    //       from ca1 in s.Query<Account>().Where(a=> a.ID == fr.CrAccID)
                    // from ca2 in s.Query<Account>().Where(a=> a.ID == fr.DbtAccID)
                    //      from c in s.Query<Contragent>().Where(a=> a.ID == ca1.ContragentID)//(ca1 == null ? ca2.ContragentID : ca1.ContragentID))
                    //      from le in s.Query<LegalEntity>().Where(a=> a.ContragentID == c.ID)
                    // where 
                    //        (r.Kind.StartsWith(RegisterIdentifier.NPFtoPFR, StringComparison.OrdinalIgnoreCase) || r.Kind.StartsWith(RegisterIdentifier.PFRtoNPF, StringComparison.OrdinalIgnoreCase)) && r.StatusID != -1 
                    //       && (fr.Status.StartsWith(RegisterIdentifier.FinregisterStatuses.Transferred, StringComparison.OrdinalIgnoreCase) || fr.Status.StartsWith(RegisterIdentifier.FinregisterStatuses.NotTransferred, StringComparison.OrdinalIgnoreCase)) && fr.StatusID != -1
                    //        && (c.TypeName==null || c.TypeName.StartsWith("нпф", StringComparison.OrdinalIgnoreCase))
                    select new RegisterListItem2
                    {
                        RegisterID = r.ID,
                        RegisterKind = r.Kind,
                        //ID = fr.ID,
                        Content = r.Content,
                        // ContragentName = le.FormalizedName,
                       // Count = fr.Count ?? 0,
                      //  ZLCount = fr.ZLCount,
                       // FinregRegNum = fr.RegNum,
                       // FinregDate = fr.Date,
                        RegNum = r.RegNum,
                        RegDate = r.RegDate,
                      //  Status = fr.Status
                    };

            //var xxx = q.Provider.Execute(q.Expression);

            return q;
            // });
        }
    }
}

/*var query = 
from r in s.Query<Register>()
from fr in s.Query<Finregister>().Where(a=> a.RegisterID == r.ID)
from ca1 in s.Query<Account>().Where(a=> a.ID == fr.CrAccID)
from ca2 in s.Query<Account>().Where(a=> a.ID == fr.DbtAccID)
from c in s.Query<Contragent>().Where(a=> a.ID == (ca1 == null ? ca2.ContragentID : ca1.ContragentID))
from le in s.Query<LegalEntity>().Where(a=> a.ContragentID == c.ID)
where 
(r.Kind.StartsWith(RegisterIdentifier.NPFtoPFR, StringComparison.OrdinalIgnoreCase) || r.Kind.StartsWith(RegisterIdentifier.PFRtoNPF, StringComparison.OrdinalIgnoreCase)) && r.StatusID != -1 
&& (fr.Status.StartsWith(RegisterIdentifier.FinregisterStatuses.Transferred, StringComparison.OrdinalIgnoreCase) || fr.Status.StartsWith(RegisterIdentifier.FinregisterStatuses.NotTransferred, StringComparison.OrdinalIgnoreCase)) && fr.StatusID != -1
&& (c.TypeName==null || c.TypeName.StartsWith("нпф", StringComparison.OrdinalIgnoreCase))
select new RegisterListItem2
{
RegisterID = r.ID,
RegisterKind = r.Kind,
ID = fr.ID,
Content = r.Content,
ContragentName = le.FormalizedName,
Count = fr.Count ?? 0,
ZLCount = fr.ZLCount,
FinregRegNum = fr.RegNum,
FinregDate = fr.Date,
RegNum = r.RegNum,
RegDate = r.RegDate,
Status = fr.Status
};
return query.AsQueryable();*/