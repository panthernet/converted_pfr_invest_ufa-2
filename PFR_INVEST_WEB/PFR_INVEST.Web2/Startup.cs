﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PFR_INVEST.Web2.Startup))]
namespace PFR_INVEST.Web2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
