﻿using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.Web2.Models
{
    public class WebSIContactListViewModel : WebListViewModel<SIListItem>
    {
        public WebSIContactListViewModel() : base(ListProvider.GetSiContactList())
        {
            AjaxMain = "~/Views/Menu/Dictionary/SI/_SiContactListViewPartial.cshtml";
            AjaxPartial = "~/Views/Menu/Dictionary/SI/_SiContactListContainerViewPartial.cshtml";
            AjaxController = "Dictionary";
        }
    }
}