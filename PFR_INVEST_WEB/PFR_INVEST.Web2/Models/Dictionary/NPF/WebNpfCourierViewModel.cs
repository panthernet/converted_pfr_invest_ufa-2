﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Newtonsoft.Json;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.Annotations;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Attributes;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic.Attributes;
using PFR_INVEST.Web2.BusinessLogic.Models;

namespace PFR_INVEST.Web2.Models
{
    [ModelEntity(Type = typeof(LegalEntityCourier), JournalDescription = "Курьер")]
    [WebReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    [WebEditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_directory_editor, DOKIP_ROLE_TYPE.OARRS_directory_editor)]
    [WebDeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OARRS_manager)]
    public class WebNpfCourierViewModel : PureWebFormViewModel, ILoaListProvider
    {
        public override long ID
        {
            get { return Item.ID; }
            protected set { Item.ID = value; }
        }

        public long LegalEntityID
        {
            get { return Item.LegalEntityID; }
            set { Item.LegalEntityID = value; }
        }

        public int TypeID
        {
            get { return Item.TypeID; }
            set { Item.TypeID = value; }
        }


        [Display(Name = "Имя")]
        [Required]
        [StringLength(100, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string FirstName
        {
            get { return Item.FirstName; }
            set { Item.FirstName = value; }
        }

        [Display(Name = "Фамилия")]
        [Required]
        [StringLength(100, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string LastName
        {
            get { return Item.LastName; }
            set { Item.LastName = value; }
        }

        [Display(Name = "Отчество")]
        [StringLength(100, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string MiddleName
        {
            get { return Item.PatronymicName; }
            set { Item.PatronymicName = value; }
        }

        [Display(Name = "Серия паспорта")]
        [StringLength(50, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string PassportSerie
        {
            get { return Item.PassportSer; }
            set { Item.PassportSer = value; }
        }

        [Display(Name = "Номер паспорта")]
        [StringLength(50, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string PassportNumber
        {
            get { return Item.PassportNum; }
            set { Item.PassportNum = value; }
        }

        [Display(Name = "Кем выдан")]
        [StringLength(200, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string PassportBy
        {
            get { return Item.PassportIssuedBy; }
            set { Item.PassportIssuedBy = value; }
        }

        [Display(Name = "Когда выдан")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime? PassportWhen
        {
            get { return Item.PassportIssueDate; }
            set { Item.PassportIssueDate = value; }
        }

        [Display(Name = "Адрес")]
        [StringLength(200, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string Address
        {
            get { return Item.Address; }
            set { Item.Address = value; }
        }


        [Display(Name = "Телефон рабочий")]
        [StringLength(50, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string WorkPhone
        {
            get { return Item.WorkPhone; }
            set { Item.WorkPhone = value; }
        }

        [Display(Name = "Добавочный")]
        [StringLength(50, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string WorkPhoneExtension
        {
            get { return Item.WorkPhoneExtension; }
            set { Item.WorkPhoneExtension = value; }
        }

        [Display(Name = "Телефон мобильный")]
        [StringLength(50, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string MobilePhone
        {
            get { return Item.MobilePhone; }
            set { Item.MobilePhone = value; }
        }

        [Display(Name = "Электронная почта")]
        [StringLength(50, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        [EmailAnnotation]
        public string Email
        {
            get { return Item.Email; }
            set { Item.Email = value; }
        }

        [Display(Name = "№ доверенности")]
        [StringLength(50, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string LoaNum
        {
            get { return Item.LetterOfAttorneyNumber; }
            set { Item.LetterOfAttorneyNumber = value; }
        }

        [Display(Name = "Дата выдачи доверенности")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime? LoaDate
        {
            get { return Item.LetterOfAttorneyIssueDate; }
            set { Item.LetterOfAttorneyIssueDate = value; }
        }

        [Display(Name = "Дата прекращения действия доверенности")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime? LoaDateEnd
        {
            get { return Item.LetterOfAttorneyExpireDate; }
            set { Item.LetterOfAttorneyExpireDate = value; }
        }

        [Display(Name = "Дата регистрации в ПФР")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime? LoaDateReg
        {
            get { return Item.LetterOfAttorneyRegisterDate; }
            set { Item.LetterOfAttorneyRegisterDate = value; }
        }


        public WebNpfCourierViewModel() : this(0,0)
        {
        }

        public WebNpfCourierViewModel(long id = 0, long leId = 0) : 
            base(id)
        {
            //связь со вьюхой
            WebModelHelperEx.InitializeDefault(this,id);
            WebModelHelperEx.UpdateModelState(this);
            AjaxController = "Dictionary";
            AjaxPartial = "_NpfCourierViewPartial";
            var item = id == 0
                ? (new LegalEntityCourier {LegalEntityID = leId, TypeID = (int) LegalEntityCourier.Types.Courier})
                : DataContainerFacade.GetByID<LegalEntityCourier>(id);

            OriginalItem = item;
            Item = new LegalEntityCourier();
            Item.ApplyValues(OriginalItem);
            Item.LoaListProvider = this;

            LecItem = GetItemLECLA(item);
            _oldNumber = Item.LetterOfAttorneyNumber;
            _oldIssueDate = Item.LetterOfAttorneyIssueDate;
            _oldExpireDate = Item.LetterOfAttorneyExpireDate;
            _oldRegisterDate = Item.LetterOfAttorneyRegisterDate;
        }

        public static List<LegalEntityCourierLetterOfAttorney> GetCourierLoaList(long id)
        {
            return DataContainerFacade.GetListByProperty<LegalEntityCourierLetterOfAttorney>("LegalEntityCourierID", id).Where(a => a.StatusID != -1).Reverse().ToList();
        }

        public override bool WebBeforeExecuteSaveCheck()
        {
            return true;
        }

        public static void WebDeleteCourierLoa(long id)
        {
            DataContainerFacade.Delete<LegalEntityCourierLetterOfAttorney>(id);
        }

        public override bool WebExecuteSave()
        {
            if (ID > 0)
            {
                var old = DataContainerFacade.GetByID<LegalEntityCourier>(ID);
                if (Item.IsLetterOfAttorneyNeedSave(old.LetterOfAttorneyNumber, old.LetterOfAttorneyIssueDate, old.LetterOfAttorneyExpireDate, old.LetterOfAttorneyRegisterDate))
                {
                    if (!IsValidLetterOfAttorney()) return false;
                    var x = new LegalEntityCourierLetterOfAttorney
                    {
                        Number = old.LetterOfAttorneyNumber,
                        IssueDate = old.LetterOfAttorneyIssueDate,
                        ExpireDate = old.LetterOfAttorneyExpireDate,
                        RegisterDate = old.LetterOfAttorneyRegisterDate,
                        LegalEntityCourierID = ID
                    };
                    DataContainerFacade.Save(x);
                }
            }
            ID = DataContainerFacade.Save(Item);
            return true;
        }

        [JsonIgnore]
        public LegalEntityCourier Item { get;}
        [JsonIgnore]
        public LegalEntityCourier OriginalItem { get; }
        [JsonIgnore]
        private LegalEntityCourierLetterOfAttorney LecItem { get; set; }

        [JsonIgnore]
        public LegalEntityCourierLetterOfAttorney FocusedLOA { get; set; }

        [JsonIgnore]
        private readonly string _oldNumber;
        [JsonIgnore]
        private readonly DateTime? _oldIssueDate;
        [JsonIgnore]
        private readonly DateTime? _oldExpireDate;
        [JsonIgnore]
        private readonly DateTime? _oldRegisterDate;

        private bool IsValid()
        {
            return Item.IsValid();
        }

        [JsonIgnore]
        public bool IsReadOnly = false;

        [JsonIgnore]
        public bool IsEnabled => !EditAccessDenied;

        [JsonIgnore]
        public override bool EditAccessDenied => IsReadOnly || base.EditAccessDenied;

        [JsonIgnore]
        public virtual List<LegalEntityCourierLetterOfAttorney> LetterOfAttorneyList
        {
            get
            {
                if (Item.LetterOfAttorneyList == null || Item.LetterOfAttorneyList.Count == 0)//Load List
                {
                    Item.LetterOfAttorneyList = DataContainerFacade.GetListByProperty<LegalEntityCourierLetterOfAttorney>("LegalEntityCourierID", Item.ID).ToList();
                }
                return Item.LetterOfAttorneyList;
            }
            set
            {
                Item.LetterOfAttorneyList = value;
            }
        }

        [JsonIgnore]
        public virtual List<LegalEntityCourierLetterOfAttorney> LetterOfAttorneyReverseList => LetterOfAttorneyList.AsEnumerable().Reverse().ToList();

        [JsonIgnore]
        public virtual List<LegalEntityCourierLetterOfAttorney> LetterOfAttorneyListChanged
        {
            get { return Item.LetterOfAttorneyListChanged ?? (Item.LetterOfAttorneyListChanged = new List<LegalEntityCourierLetterOfAttorney>()); }
            set
            {
                Item.LetterOfAttorneyListChanged = value;
            }
        }

        [JsonIgnore]
        public virtual List<LegalEntityCourierLetterOfAttorney> LetterOfAttorneyListDeleted
        {
            get { return Item.LetterOfAttorneyListDeleted ?? (Item.LetterOfAttorneyListDeleted = new List<LegalEntityCourierLetterOfAttorney>()); }
            set
            {
                Item.LetterOfAttorneyListDeleted = value;
            }
        }

        public virtual void DeleteFromDatabase()
        {
            DataContainerFacade.Delete<LegalEntityCourier>(Item.ID);
        }

        /// <summary>
        /// No messages inside
        /// </summary>
        /// <returns></returns>
        public bool IsValidLetterOfAttorney()
        {
            return string.IsNullOrEmpty(ValidateNewLoa(Item));
        }

        private static LegalEntityCourierLetterOfAttorney GetItemLECLA(LegalEntityCourier item)
        {
            return item.GetItemLECLA();
        }

        private bool IsCurrentLOANeedSave(LegalEntityCourier x)
        {
            return x.IsLetterOfAttorneyNeedSave(_oldNumber, _oldIssueDate, _oldExpireDate, _oldRegisterDate);
        }

        public string ValidateNewLoa(LegalEntityCourier x)
        {
            if (!IsCurrentLOANeedSave(x))
                return null;

            var temp = LetterOfAttorneyReverseList;
            temp.Add(LecItem);

            var verDate = Item.IsValidLetterOfAttorneyDate(temp);

            var verNumber = Item.IsValidLetterOfAttorneyNumber(temp);

            return verNumber && verDate ? null : "Введены некорректные данные доверенности";
        }

        public string ValidateNewLoaDate(LegalEntityCourier x)
        {
            if (!IsCurrentLOANeedSave(x))
                return null;

            var temp = LetterOfAttorneyReverseList;
            //temp.Add(_LecItem);

            if (!x.IsValidLetterOfAttorneyDate(temp))
            {
                return "Даты действия доверенности не могут пересекаться с предыдущими";
            }

            if (!x.IsValidLetterOfAttorneyDate(new List<LegalEntityCourierLetterOfAttorney> { LecItem }))
            {
                return "Даты действия доверенности не могут пересекаться с текущими значениями"
                    + (LecItem.IssueDate.HasValue && LecItem.ExpireDate.HasValue ? $": {LecItem.IssueDate.Value.ToShortDateString()} - {LecItem.ExpireDate.Value.ToShortDateString()}"
                           : string.Empty);
            }

            return null;
        }

        public string ValidateNewLoaNumber(LegalEntityCourier x)
        {
            if (!IsCurrentLOANeedSave(x))
                return null;

            var temp = LetterOfAttorneyReverseList;
            //temp.Add(_LecItem);

            if (!x.IsValidLetterOfAttorneyNumber(temp))
            {
                return "Номер доверенности не может пересекаться с предыдущими";
            }

            if (!x.IsValidLetterOfAttorneyDate(new List<LegalEntityCourierLetterOfAttorney> { LecItem }))
            {
                return "Номер доверенности не может совпадать с текущим при измененных датах доверенности";
            }

            return null;
        }

        public bool CanExecuteDeleteLOA()
        {
            return FocusedLOA != null;
        }

        public void ExecuteDeleteLOA()
        {
            if (FocusedLOA.ID > 0)
            {
                LetterOfAttorneyListDeleted.Add(FocusedLOA);
            }

            LetterOfAttorneyList.Remove(FocusedLOA);
            FocusedLOA = null;
        }
    }
}