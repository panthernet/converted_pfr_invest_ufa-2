﻿using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.Web2.Models
{
    public class WebNpfListViewModel: WebListViewModel<NPFListItem>
    {
        public WebNpfListViewModel() : base(ListProvider.GetNPFList())
        {
            AjaxMain = "_NpfListNpfViewPartial";
            AjaxPartial = "_NpfListNpfContainerViewPartial";
            AjaxController = "Dictionary";
           // var x = ListProvider.Test();

        }
    }
}