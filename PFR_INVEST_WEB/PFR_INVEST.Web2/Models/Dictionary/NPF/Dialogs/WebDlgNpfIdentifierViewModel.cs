﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Newtonsoft.Json;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Web2.BusinessLogic;
using WebViewModel = PFR_INVEST.Web2.BusinessLogic.Models.WebViewModel;

namespace PFR_INVEST.Web2.Models
{
    public class WebDlgNpfIdentifierViewModel: WebViewModel
    {
        [JsonIgnore]
        private readonly LegalEntityIdentifier _item;
        [JsonIgnore]
        private readonly List<LegalEntityIdentifier> _usedIdentifiers;

        public WebDlgNpfIdentifierViewModel()
            : this(0,0)
        {
            
        }

        public override bool WebExecuteDelete(long id)
        {
            DataContainerFacade.Delete(_item);
            return true;
        }

        public WebDlgNpfIdentifierViewModel(long id = 0, long leId = 0)
        {
            AjaxController = "Dictionary";
            AjaxAction = "DlgEditIdentifier";
            AjaxPartial = "_DlgIdentifierViewPartial";
            FormId = "dlgNpfIdentifier";
            WebTitle = FormDataHelper.Data[FormId].DisplayName;
            _item = id > 0 ? DataContainerFacade.GetByID<LegalEntityIdentifier>(id) : new LegalEntityIdentifier();
            if (_item.LegalEntityID == 0)
                _item.LegalEntityID = leId;
            var conds = new List<ListPropertyCondition>
            {
                ListPropertyCondition.Equal("StatusID", 1L)
            };
            if(id > 0)
                conds.Add(ListPropertyCondition.NotEqual("ID", id));

            _usedIdentifiers = DataContainerFacade.GetListByPropertyConditions<LegalEntityIdentifier>(conds).ToList();
        }

        public override long WebGetEntryId()
        {
            return _item.ID;
        }


        public long ID
        {
            get { return _item.ID; }
            set { _item.ID = value; }
        }

        [Required]
        public virtual long LegalEntityID
        {
            get { return _item.LegalEntityID; }
            set { _item.LegalEntityID = value; }
        }

        [Required]
        [DisplayName("Идентификатор")]
        [StringLength(4, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public virtual string Identifier
        {
            get { return _item.Identifier; }
            set { _item.Identifier = value;  }
        }

        [DisplayName("Причина установки")]
        [StringLength(256, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public virtual string Reason
        {
            get { return _item.Reason; }
            set { _item.Reason = value; }
        }

        [DisplayName("Дата установки")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        [Required]
        public virtual DateTime? SetDate
        {
            get { return _item.SetDate; }
            set { _item.SetDate = value; }
        }

        [DisplayName("Комментарий")]
        [StringLength(1000, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public virtual string Comment
        {
            get { return _item.Comment; }
            set { _item.Comment = value; }
        }

        public override bool WebBeforeExecuteSaveCheck()
        {
            if (ID == 0 || DataContainerFacade.GetByID<LegalEntityIdentifier>(ID).Identifier != Identifier)
            {
                if (_usedIdentifiers.FirstOrDefault(a => a.Identifier == _item.Identifier) != null)
                {
                    WebLastModelError = "Идентификатор уже существует!";
                    return false;
                }
            }
            return true;
        }

        public override bool WebExecuteSave()
        {
            ID = DataContainerFacade.Save(_item);
            return true;
        }


    }
}