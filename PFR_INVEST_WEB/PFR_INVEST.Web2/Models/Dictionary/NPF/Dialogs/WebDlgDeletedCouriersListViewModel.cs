﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Web2.BusinessLogic;
using WebViewModel = PFR_INVEST.Web2.BusinessLogic.Models.WebViewModel;

namespace PFR_INVEST.Web2.Models
{
    public class WebDlgDeletedCouriersListViewModel : WebViewModel
    {
        [JsonIgnore]
        public List<LegalEntityCourier> CouriersList { get; set; } = new List<LegalEntityCourier>();

        public WebDlgDeletedCouriersListViewModel() : this(0)
        {
            
        }

        public WebDlgDeletedCouriersListViewModel(long id)
        {
            CouriersList = id == 0 ? CouriersList : DataContainerFacade.GetListByProperty<LegalEntityCourier>("LegalEntityID", id).Where(a => a.StatusID == -1).ToList();

            //связь со вьюхой
            AjaxMain = "_SharedAjaxFormSimplePartialView";
            AjaxController = "Dictionary";
            AjaxAction = "DlgDeletedCouriersList";
            AjaxPartial = "_DlgDeletedCouriersViewPartial";
            FormId = "dlgDeletedCouriersList";
            WebTitle = FormDataHelper.Data[FormId].DisplayName;
        }

        public override long WebGetEntryId()
        {
            return 0;
        }
    }
}