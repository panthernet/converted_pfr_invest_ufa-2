﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.Annotations;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Attributes;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic.Attributes;
using PFR_INVEST.Web2.BusinessLogic.Models;

namespace PFR_INVEST.Web2.Models
{
    [ModelEntity(Type = typeof(Contact), JournalDescription = "Контакт НПФ")]
    [WebReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OKIP_worker)]
    [WebEditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_directory_editor, DOKIP_ROLE_TYPE.OARRS_directory_editor, DOKIP_ROLE_TYPE.OKIP_worker)]
    [WebDeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OARRS_manager, DOKIP_ROLE_TYPE.OKIP_manager)]
    public class WebNpfContactViewModel : PureWebFormViewModel
    {
        public override long ID
        {
            get { return Item.ID; }
            protected set { Item.ID = value; }
        }

        public long LegalEntityID
        {
            get { return Item.LegalEntityID; }
            set { Item.LegalEntityID = value; }
        }

        public int TypeID
        {
            get { return Item.TypeID; }
            set { Item.TypeID = value; }
        }


        [Display(Name = "Имя")]
        [Required]
        [StringLength(100, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string FirstName
        {
            get { return Item.FirstName; }
            set { Item.FirstName = value; }
        }

        [Display(Name = "Фамилия")]
        [Required]
        [StringLength(100, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string LastName
        {
            get { return Item.LastName; }
            set { Item.LastName = value; }
        }

        [Display(Name = "Отчество")]
        [StringLength(100, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string MiddleName
        {
            get { return Item.PatronymicName; }
            set { Item.PatronymicName = value; }
        }

        [Display(Name = "Должность")]
        [Required]
        [StringLength(100, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string Position
        {
            get { return Item.Position; }
            set { Item.Position = value; }
        }

        [Display(Name = "Телефон рабочий")]
        [StringLength(50, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string WorkPhone
        {
            get { return Item.WorkPhone; }
            set { Item.WorkPhone = value; }
        }

        [Display(Name = "Добавочный")]
        [StringLength(50, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string WorkPhoneExtension
        {
            get { return Item.WorkPhoneExtension; }
            set { Item.WorkPhoneExtension = value; }
        }

        [Display(Name = "Телефон мобильный")]
        [StringLength(50, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string MobilePhone
        {
            get { return Item.MobilePhone; }
            set { Item.MobilePhone = value; }
        }

        [Display(Name = "Электронная почта")]
        [StringLength(50, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        [EmailAnnotation]
        public string Email
        {
            get { return Item.Email; }
            set { Item.Email = value; }
        }

        [Display(Name = "Комментарий")]
        [StringLength(100, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string Comment
        {
            get { return Item.Comment; }
            set { Item.Comment = value; }
        }


        public WebNpfContactViewModel() : this(0,0)
        {            
        }

        public WebNpfContactViewModel(long id = 0, long leId = 0) : base(id)
        {
            //связь со вьюхой
            WebModelHelperEx.InitializeDefault(this,id);
            WebModelHelperEx.UpdateModelState(this);
            AjaxController = "Dictionary";
            AjaxPartial = "_NpfContactViewPartial";

            var item = id == 0 ? (new LegalEntityCourier {LegalEntityID = leId, TypeID = 2}) : DataContainerFacade.GetByID<LegalEntityCourier>(id);

            Item = item;
            Item.PropertyChanged += (sender, e) => { IsDataChanged = true; };
        }

        public override bool WebBeforeExecuteSaveCheck()
        {
            return IsValid();
        }

        public override bool WebExecuteSave()
        {
            ID = DataContainerFacade.Save(Item);
            return true;
        }

        [JsonIgnore]
        public LegalEntityCourier Item { get;  }


        protected bool IsValid()
        {
            return IsDataChanged && Item.Validate();
        }
    }
}