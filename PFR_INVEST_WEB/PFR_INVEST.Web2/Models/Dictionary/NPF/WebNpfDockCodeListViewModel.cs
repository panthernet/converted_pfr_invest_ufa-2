﻿using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.Models
{
    public class WebNpfDockCodeListViewModel : WebListViewModel<NPFErrorCode>
    {
        public WebNpfDockCodeListViewModel() : base(ListProvider.GetDockCodesList())
        {
            AjaxMain = "_NpfListDockCodesViewPartial";
            AjaxPartial = "_NpfListDockCodesContainerViewPartial";
            AjaxController = "Dictionary";
        }
    }
}