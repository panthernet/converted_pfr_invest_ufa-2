﻿using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.Models
{
    public class WebNpfErrorCodeListViewModel : WebListViewModel<NPFErrorCode>
    {
        public WebNpfErrorCodeListViewModel() : base(ListProvider.GetErrorCodesList())
        {
            AjaxMain = "_NpfListErrorCodesViewPartial";
            AjaxPartial = "_NpfListErrorCodesContainerViewPartial";
            AjaxController = "Dictionary";
        }
    }
}