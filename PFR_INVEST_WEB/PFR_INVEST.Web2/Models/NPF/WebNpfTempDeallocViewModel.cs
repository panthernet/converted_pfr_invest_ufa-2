﻿using System.Collections.Generic;
using System.Linq;
using DevExpress.Xpo;
using Newtonsoft.Json;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Attributes;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.Database.Classes;
using PFR_INVEST.Web2.DataObjects.Web;
using PFR_INVEST.Web2.DataObjects.Web.ListItems;
using ViewModelState = PFR_INVEST.Web2.BusinessLogic.Models.ViewModelState;

namespace PFR_INVEST.Web2.Models
{
    [ModelEntity(Type = typeof(RegisterWeb), JournalDescription = "Временное размещение")]
    public class WebNpfTempDeallocViewModel: WebNpfSpnRegisterViewModel
    {
        [DisplayName("Портфель")]
        public override long? PortfolioID { get { return Register.PortfolioID; }
            set
            {
                Register.PortfolioID = value;
                SelectedPortfolio = value.HasValue ? PortfoliosList.FirstOrDefault(a => a.ID == value) : null;
            } }

        [DisplayName("Суммы СПН утвержд. документом")]
        public override long? ApproveDocID
        {
            get { return Register.ApproveDocID; }
            set
            {
                Register.ApproveDoc = ApproveDocsList.FirstOrDefault(a => a.ID == value);
            }
        }

        public WebNpfTempDeallocViewModel() : this(0, ViewModelState.Create) { }

        public WebNpfTempDeallocViewModel(long entryId) : this(entryId, ViewModelState.Edit) { }

        #region Options
        [JsonIgnore]
        public override bool IsPortfolioVisible => true;

        [JsonIgnore]
        public override bool IsSumCFRVisible => true;
        [JsonIgnore]
        public override bool IsSumZLVisible => true;
        [JsonIgnore]
        public override bool IsCompanyVisible => false;

        [JsonIgnore]
        public override bool IsCompanyRequired => false;
        [JsonIgnore]
        public override bool IsERZLVisible => false;
        [JsonIgnore]
        public override bool IsPayAssVisible => false;
        #endregion

        #region GridColumnSettings
        public override bool IsCFRColumnVisible => true;
        public override bool IsZLColumnEditable => true;
        public override bool IsCountColumnEditable => true;
        public override bool IsCFRCountColumnEditable => true;

        #endregion

		#region Kind
        [JsonIgnore]
        public override List<string> Kinds { get; } = new List<string>
		{
			RegisterIdentifier.TempReturn
		};

        public override bool IsKindEditable => false;

        #endregion

        [JsonIgnore]
        protected List<FinregisterWithCorrAndNPFWebListItem> finregWithNPFWebList { get; set; }

        public WebNpfTempDeallocViewModel(long recordID, ViewModelState action)
            : base(recordID)
        {
            WebModelHelperEx.InitializeDefault(this, "Npf", "_TempDeallocationRegisterViewPartial");
            WebModelHelperEx.UpdateModelState(this);
            AttachDefaultGridScripts = true;

			RefreshPortfoliosList();
			Contents = PFRToNPFContents;

			if (action == ViewModelState.Create)
			{
				if (PortfoliosList.Any())
				{
					SelectedPortfolio = PortfoliosList.First();
				}
			}
			else
			{
				//var mR = this.register.GetMainRegister();
				
				RefreshFinregWithNPFList();
				if (finregWithNPFList.Any(x => !RegisterIdentifier.FinregisterStatuses.IsCreated(x.Finregister.Status)))
				{
					State = ViewModelState.Read;
				}

				SelectedPortfolio = PortfoliosList.FirstOrDefault(x => x.ID == Register.PortfolioID);
			}
        }

		protected void RefreshPortfoliosList()
		{
			PortfoliosList = WCFClient.GetDataServiceFunc().GetSPNAllocatedPortfolios();
			if (State == ViewModelState.Edit && Register?.PortfolioID != null && PortfoliosList.All(x => x.ID != Register.PortfolioID.Value))
			{
				var p = DataContainerFacade.GetByID<Portfolio>(Register.PortfolioID);
				PortfoliosList.Add(p);
			}
		}

		#region Portfolio
        [JsonIgnore]
		public override bool IsPortfolioEnabled => true;

        public override Portfolio SelectedPortfolio
		{
			get
			{
				return base.SelectedPortfolio;
			}

			set
			{
				base.SelectedPortfolio = value;
				if (base.SelectedPortfolio != null)
					Register.PortfolioID = base.SelectedPortfolio.ID;
			    AfterPropertyChanged();
			}
		}

		protected override void AfterPropertyChanged()
		{
			if (State == ViewModelState.Create)
			{
				LoadAvailableSPN();
			}
		}

		private void LoadAvailableSPN()
		{
			if (State != ViewModelState.Create) return;

			if (SelectedPortfolio != null && RegisterContent != null)
			{
				var avList = GetAvailableNPFList(Register);
				FinregWithNPFList = avList.Select(x => new FinregisterWithCorrAndNPFWebListItem
				{
					CFRCount = x.CFRCount,
					CorrCount = null,
					Count = x.Count,
					Finregister = WCFClient.GetEFServiceFunc().SpnFinregisters.FirstOrDefault(a=> a.ID == x.ID),
					ID = 0,
					IsItemChanged = true,
                    IsNewItem = true,
					NPFName = x.NPFName,
					NPFStatus = x.NPFStatus,
					ZLCount = x.ZLCount
				}).ToList();
				RecalcSums();
			}
		}
		#endregion

        public static RegisterWeb GetMainRegister(long id)
        {
            return DbHelper.GetEF().SpnRegisters.FirstOrDefault(a => a.ReturnID == id);
        }

		protected override void LoadFinregFromDB()
		{
			if (Register != null)
			{
				var mRec = GetMainRegister(Register.ID); //DataContainerFacade.GetListByProperty<Register>("ReturnID", this.register.ID).FirstOrDefault();
			    finregWithNPFList = WCFClient.GetEFServiceFunc().GetFinregisterWithCorrAndNPFList(mRec.ID);// BLServiceSystem.Client.GetFinregisterWithCorrAndNPFList(mRec.ID);
			}
		}

		protected override void AddMainRecord()
		{
			if (Register != null)
			{
				// создание основного реестра для извлечения
				var mRec = GetMainRegister(Register.ID); //DataContainerFacade.GetListByProperty<Register>("ReturnID", this.register.ID).FirstOrDefault();

				if (mRec == null)
				{
					mRec = new RegisterWeb();
					Register.CopyTo(mRec);
					mRec.ID = 0;
				}
				else
				{
					var id = mRec.ID;
					Register.CopyTo(mRec);
					mRec.ID = id;
				}
				mRec.Kind = RegisterIdentifier.PFRtoNPF;
				mRec.ReturnID = Register.ID;
				mRec.ID = DbHelper.SaveEntity(mRec);
				finregWithNPFList.ForEach(x => x.Finregister.RegisterID = mRec.ID);
                DbHelper.SaveChanges();
			}
		}

		protected override void SaveReturnFinregisters()
		{
			if (finregWithNPFList != null && finregWithNPFList.Any())
                WCFClient.GetEFServiceFunc().SaveReturnFinregisters(finregWithNPFList);
		}

		public void RaiseFinregisterChanged()
		{
			RecalcSums();
		}

		public override void ExecuteDeleteFR(long frID)
		{
			if (State == ViewModelState.Create)
			{
			    base.ExecuteDeleteFR(frID);
			}
		}

		public override bool WebBeforeExecuteSaveCheck()
		{
			if (ID > 0)
			{
				var mRec = GetMainRegister(Register.ID); //DataContainerFacade.GetListByProperty<Register>("ReturnID", this.register.ID).FirstOrDefault();
				var frNPFList = WCFClient.GetEFServiceFunc().GetFinregisterWithCorrAndNPFList(mRec.ID);
				if (frNPFList.Any(x => !RegisterIdentifier.FinregisterStatuses.IsCreated(x.Finregister.Status)))
				{
					State = ViewModelState.Read;
					WebLastModelError = "Не все финреестры находятся в начальном статусе";
					return false;
				}
			}

		    if (SelectedPortfolio == null)
		    {
                WebLastModelError = "Сохранение реестра с типом операции 'Изъятие СПН из временного размещение' невозможно с пустым значением в поле 'Портфель'. \nВыберите портфель и повторите сохранение.";
		        return false;
		    }

			var avNPFList = GetAvailableNPFList(Register, false);

			if ((avNPFList == null || avNPFList.Count == 0) && finregWithNPFList != null && finregWithNPFList.Count > 0)
			{
				WebLastModelError = "Нет средств, доступных для изъятия в выбранном портфеле";
				return false;
			}

			foreach (var fr in finregWithNPFList)
			{
				var gr = avNPFList.FirstOrDefault(x => x.CrAccID == fr.Finregister.CrAccID && x.DbtAccID == fr.Finregister.DbtAccID);
				if (gr == null)
				{
					WebLastModelError = $"Не удается добавить или отредактировать НПФ \"{fr.NPFName}\". Возможно, для изъятия недостаточно средств.";
					return false;
				}

				if (gr.Count < fr.Count)
				{
					WebLastModelError = $"Недостаточно средств находится в размещении НПФ \"{gr.NPFName}\" по выбранному портфелю.";
					return false;
				}

				if (gr.ZLCount < fr.ZLCount)
				{
				    WebLastModelError = $"Недостаточное количество ЗЛ находится в размещении НПФ \"{gr.NPFName}\" по выбранному портфелю.";
					return false;
				}
			}

			return base.WebBeforeExecuteSaveCheck();
		}
    }
}