﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Newtonsoft.Json;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Attributes;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic.Attributes;
using PFR_INVEST.Web2.Database.Classes;
using PFR_INVEST.Web2.DataObjects.Web;
using PFR_INVEST.Web2.DataObjects.Web.ListItems;
using PureWebDialogViewModel = PFR_INVEST.Web2.BusinessLogic.Models.PureWebDialogViewModel;

namespace PFR_INVEST.Web2.Models
{
    [WebReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [WebEditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [WebDeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager)]
    [ModelEntity(Type = typeof(DelayedPaymentClaimWeb), JournalDescription = "Задержанная выплата (Корреспонденция)")]
    public class WebNpfCorrespDelayedPaymentViewModel : PureWebDialogViewModel
    {
        [JsonIgnore]
        public DelayedPaymentClaimWeb Claim { get; }

        [JsonIgnore]
        public IList<ElementWeb> FZList { get; }
        [JsonIgnore]
        public IList<NameValueListItem> NpfList { get; }

        public long ID
        {
            get { return Claim.ID; }
            set { Claim.ID = value; }
        }

        [DisplayName("№ предписания УФО")]
        public string UFONumber
        {
            get { return Claim.UFONumber; }
            set { Claim.UFONumber = value; }
        }

        [DisplayName("Дата предписания УФО")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime UFODate
        {
            get { return Claim.UFODate; }
            set { Claim.UFODate = value; }
        }

        [DisplayName("№ письма")]
        public string LetterNumber
        {
            get { return Claim.LetterNumber; }
            set { Claim.LetterNumber = value; }
        }

        [DisplayName("Дата письма")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime? LetterDate
        {
            get { return Claim.LetterDate; }
            set { Claim.LetterDate = value; }
        }

        [DisplayName("ФЗ")]
        [Required]
        public long? FZID
        {
            get { return Claim.FZID; }
            set { Claim.FZID = value; }
        }

        [DisplayName("Название передачи")]
        [Required]
        public string Name
        {
            get { return Claim.Name; }
            set { Claim.Name = value; }
        }

        [DisplayName("Краткое наименование фонда")]
        [Required]
         public long? NpfID
         {
             get { return Claim.NpfID; }
             set { Claim.NpfID = value; }
         }

        [DisplayName("ИНН")]
        public string NpfInn => Claim?.Npf?.INN;


        [DisplayName("Сумма")]
        [Required]
        public decimal? Ammount
        {
            get { return Claim.Ammount; }
            set { Claim.Ammount = value; }
        }

        [DisplayName("Количество ЗЛ")]
        [Required]
        public long? ZLCount
        {
            get { return Claim.ZLCount; }
            set { Claim.ZLCount = value; }
        }

        [DisplayName("№ распоряжения правления ПФР")]
        public string PRFNumber
        {
            get { return Claim.PRFNumber; }
            set { Claim.PRFNumber = value; }
        }

        [DisplayName("Дата распоряжения")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime? PfrDate
        {
            get { return Claim.PfrDate; }
            set { Claim.PfrDate = value; }
        }

        [DisplayName("Дата введения запрета")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime? RestrictStartDate
        {
            get { return Claim.RestrictStartDate; }
            set { Claim.RestrictStartDate = value; }
        }

        [DisplayName("Дата окончания действия запрета")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime? RestrictEndDate
        {
            get { return Claim.RestrictEndDate; }
            set { Claim.RestrictEndDate = value; }
        }

        [DisplayName("Примечание(статус)")]
        public string Comment
        {
            get { return Claim.Comment; }
            set { Claim.Comment = value; }
        }

        public WebNpfCorrespDelayedPaymentViewModel()
            : this(0)
        {
        }

        public WebNpfCorrespDelayedPaymentViewModel(long id)
        {
            NpfList = WCFClient.GetEFServiceFunc().GetActiveLegalEntityByType(ContragentWeb.Types.Npf);
            Claim = id == 0 ? new DelayedPaymentClaimWeb {UFODate = DateTime.Now} : WCFClient.GetEFServiceFunc().DelayedPaymentClaims.FirstOrDefault(a => a.ID == id);
            FZList = WCFClient.GetEFServiceFunc().Elements.Where(a => a.Key == (long) Element.Types.FZ && a.Visible == 1 && a.ID == Claim.FZID).ToList();
            ValidatableFields = $"{nameof(ZLCount)}|{nameof(Ammount)}|{nameof(Name)}|{nameof(FZID)}";

            WebModelHelperEx.InitializeDefault(this, "Npf", "_CorrespondenceDelayedPaymentViewPartial");
            WebModelHelperEx.UpdateModelState(this);
            AttachDefaultGridScripts = false;
        }

        public override bool WebExecuteDelete(long id)
        {
            DbHelper.DeleteEntity(Claim);
            return true;
        }

        public override bool WebExecuteSave()
        {
            DbHelper.SaveEntity(Claim);
            return true;
        }

        public override long WebGetEntryId()
        {
            return Claim?.ID ?? 0;
        }

        private const string WRONG_VALUE = "Не указано обязательное поле!";

        [JsonIgnore]
        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case nameof(ZLCount):
                        return !ZLCount.HasValue ? WRONG_VALUE : null;
                    case nameof(Ammount):
                        return !Ammount.HasValue ? WRONG_VALUE : null;
                    case nameof(Name):
                        return string.IsNullOrEmpty(Name) ? WRONG_VALUE : null;
                    case nameof(FZID):
                        return !FZID.HasValue ? WRONG_VALUE : null;
                    default:
                        return null;
                }
            }
        }
    }
}