﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using Newtonsoft.Json;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic.Attributes;
using PFR_INVEST.Web2.BusinessLogic.Models;
using PFR_INVEST.Web2.Database.Classes;
using PFR_INVEST.Web2.DataObjects.Web;

namespace PFR_INVEST.Web2.Models
{
    [WebReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OFPR_worker)]
    [WebEditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    public class WebNpfSpnStatusReqViewModel: PureWebFormViewModel
    {
        public WebNpfSpnStatusReqViewModel() : this(0,0) { }

        [JsonIgnore]
        private readonly long _parentId;
        [JsonIgnore]
        private readonly FinregisterWeb _finregister;

        [DisplayName("НПФ")]
        public string NpfName { get; }

        [DisplayName("Номер документа")]
        [Required]
        public string RegNum
        {
            get { return _finregister?.RegNum ?? (_finregister == null ? "reg1" : null);}
            set { if(_finregister != null) _finregister.RegNum = value; }
        }

        [DisplayName("Дата документа")]
        [Required]
        public DateTime? WebDate
        {
            get { return _finregister?.Date ?? (_finregister == null ? DateTime.MinValue : (DateTime?)null);}
            set
            {
                if (_finregister != null) _finregister.Date = value;
            }
        }

        [DisplayName("Сумма документа")]
        public decimal? WebCount => _finregister?.Count;

        [JsonIgnore]
        public bool IsMassTransfer { get; }

        public WebNpfSpnStatusReqViewModel(long id, long parentId): base(id)
        {
            _parentId = parentId;
            WebModelHelperEx.InitializeDefault(this, "Npf", "_SpnStatusReqViewPartial");
            WebModelHelperEx.UpdateModelState(this);
            IsDeleteButtonVisible = false;

            if (id > 0 && parentId == 0)
            {
                _finregister = DbHelper.GetUncached<FinregisterWeb>(id);
                var creditId = _finregister?.AccountCredit?.ContragentID ?? 0;
                var debitId = _finregister?.AccountDebit?.ContragentID ?? 0;
               // var reg = WCFClient.GetEFServiceFunc().SpnRegisters.FirstOrDefault(a => a.ID == _finregister.RegisterID);
               // var kind = reg.Kind;


                var len = WCFClient.GetEFServiceFunc().LegalEntities.FirstOrDefault(le =>
                    le.Contragent.ID == (_finregister.Register.Kind == RegisterIdentifier.NPFtoPFR ? creditId : debitId));
                NpfName = len?.FormalizedNameFull;
            }
            else IsMassTransfer = true;

            if (WebDate == null ||
                WebDate != null && WebDate.Value == DateTime.MinValue)
                WebDate = DateTime.Now;
        }

        [JsonIgnore]
        public override string this[string columnName]
        {
            get
            {
                const string errorMessage = "Неверный формат данных";
                switch (columnName)
                {
                    case nameof(WebDate): return WebDate == null ? errorMessage : null;
                   // case nameof(WebCount): return WebCount == null || WebCount == 0 ? errorMessage : null;
                    case nameof(RegNum): return string.IsNullOrEmpty(RegNum) ? errorMessage : null;
                    default: return null;
                }
            }
        }


        public override bool WebExecuteSave()
        {
            if (_parentId > 0 && ID == 0)
            {
                //mass 
                WCFClient.GetEFServiceFunc().SpnUpdateFinregisterStatusIssued(_parentId);
                return true;
            }
            if (ID > 0)
            {
                _finregister.Status = RegisterIdentifier.FinregisterStatuses.Issued;
                var c = WCFClient.GetEFServiceFunc();
                c.SpnFinregisters.Attach(_finregister);
                c.Entry(_finregister).State = EntityState.Modified;
                c.SaveChanges();
            }

            return true;
        }

       /* public override bool CanExecuteSave()
        {
            if (IsMassTransfer) return true;
            var fieldNames = $"{nameof(RegNum)}|{nameof(WebDate)}|{nameof(WebCount)}";
            return fieldNames.Split("|".ToCharArray()).All(fieldName => string.IsNullOrEmpty(this[fieldName]));
        }*/
    }
}