﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataObjects.Attributes;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.Database.Classes;
using PFR_INVEST.Web2.DataObjects.Web;
using System.IO;
using System.Web;
using Newtonsoft.Json;
using PFR_INVEST.Constants.Files;
using PFR_INVEST.Web2.BusinessLogic.Attributes;
using PureWebDialogViewModel = PFR_INVEST.Web2.BusinessLogic.Models.PureWebDialogViewModel;

namespace PFR_INVEST.Web2.Models
{
    [ModelEntity(Type = typeof(AttachWeb), JournalDescription = "Связанный документ (Корреспонденция)")]
    [WebReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [WebEditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [WebDeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager)]
    public class WebNpfCorrespLinkedItemViewModel: PureWebDialogViewModel
    {
        public WebNpfCorrespLinkedItemViewModel() : this(0,0) { }
        [JsonIgnore]
        public bool HasExecutorList => false;
        [JsonIgnore]
        protected DocumentWeb.Types Type => DocumentWeb.Types.Npf;

        [JsonIgnore]
		public AttachWeb Attach { get; private set; }

        public long ID
        {
            get { return Attach.ID; }
            set { Attach.ID = value; }
        }

        [Required]
        [DisplayName("Классификация документа")]
        public long? AttachClassificID
        {
            get { return Attach.AttachClassificID; }
            set { Attach.AttachClassificID = value; }
        }

        [DisplayName("Дополнительно по содержанию документа")]
        public string Additional
        {
            get { return Attach.Additional; }
            set { Attach.Additional = value; }
        }

        [DisplayName("Дополнительная информация")]
        public string Comment
        {
            get { return Attach.Comment; }
            set { Attach.Comment = value; }
        }

        [DisplayName("")]
        public long? ExecutorID
        {
            get { return Attach.ExecutorID; }
            set { Attach.ExecutorID = value; }
        }

        [DisplayName("Контроль")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime? ControlDate
        {
            get { return Attach.ControlDate; }
            set { Attach.ControlDate = value; }
        }

        [DisplayName("Дата исполнения")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime? ExecutionDate
        {
            get { return Attach.ExecutionDate; }
            set { Attach.ExecutionDate = value; }
        }

        [DisplayName("Дополнительно по исполнению")]
        public string AdditionalToExecution
        {
            get { return Attach.AdditionalToExecution; }
            set { Attach.AdditionalToExecution = value; }
        }

        public List<ElementWeb> StoragePlacesList { get; set; }
        public IList<AttachClassificWeb> ClassificationList { get; }

        [DisplayName("Место хранения оригинала")]
        [Required]
        public long? OriginalPlaceID
        {
            get { return Attach.OriginalPlaceID; }
            set { Attach.OriginalPlaceID = value; }
        }

        public bool IsFileSelected => Attach.Comment != null;

        [DisplayName("")]
        public string ExecutorName
        {
            get { return Attach.ExecutorName; }
            set { Attach.ExecutorName = value; }
        }

        public string SelectedFileName2
        {
            get { return _selectedFileName2; }
            set { _selectedFileName2 = value; }
        }

        [DisplayName("Вложение")]
        public string DisplayFileName2
        {
            get
            {
                if (string.IsNullOrEmpty(_displayFileName2) && !string.IsNullOrEmpty(SelectedFileName2))
                {
                    if (SelectedFileName2.StartsWith("."))
                    {
                        _displayFileName2 = GetTrueFileName(SelectedFileName2);
                    }
                    else _displayFileName2 = SelectedFileName2;
                }
                return _displayFileName2;
            }
            set { _displayFileName2 = value; }
        }

        private string GetTrueFileName(string filePath)
        {
            var f = Path.GetFileName(filePath);
            var spl = f.Split('_');
            return $"{spl[0]}{Path.GetExtension(f)}";
        }

        //public string AttachText { get; set; } = "Вложение не выбрано";

        public WebNpfCorrespLinkedItemViewModel(long id, long parentId)
        {
            WebModelHelperEx.InitializeDefault(this, "Npf", "_CorrespondenceLinkedItemViewPartial");
            WebModelHelperEx.UpdateModelState(this);
            AttachDefaultGridScripts = true;
            ClassificationList = GetAttachClassificList();
            if (id == 0)
            {
                Attach = new AttachWeb {DocumentID = parentId};
                ExecutionDate = DateTime.Today;
                ControlDate = DateTime.Today;
                AttachClassificID = ClassificationList.FirstOrDefault()?.ID;
                //SelectedExecutor = ExecutorList.FirstOrDefault();
            }
            else
            {
                Attach = WCFClient.GetEFServiceFunc().Attaches.FirstOrDefault(a => a.ID == id);
                var dfb = DbHelper.Get<DocFileBodyWeb>(Attach?.FileContentId ?? 0);
                DisplayFileName2 = SelectedFileName2 = dfb?.FileName;
                SelectedFileName2 = SelectedFileName2 ?? ""; //hack!!!
            }

			StoragePlacesList = GetStoragePlacesList().Where(x => x.IsVisible || x.ID == OriginalPlaceID).ToList();
        }

        protected IList<ElementWeb> GetStoragePlacesList()
        {
            // фильтрацию по признаку Visible лучше производить позднее, так как иначе потребуется дополнительный запрос к БД в случае выставленного невидимого пункта
            return WCFClient.GetEFServiceFunc().Elements.Where(a=> a.Key == (long)ElementWeb.Types.NpfDocumentStoragePlaces).ToList();
        }

        public override bool WebBeforeExecuteSaveCheck()
        {
            var result = CheckAttachment(SelectedFileName2);
            if (result != null)
            {
                WebLastModelError = result;
                return false;
            }

            return true;
        }

        private static string CheckAttachment(string filePath)
        {
            if (string.IsNullOrWhiteSpace(filePath))
                return null;

            var file = HttpContext.Current.Server.MapPath(filePath);
            var fi = new FileInfo(file);
            if (!fi.Exists)
                return $"Файл '{file}' не найден";
            if (StringExtension.Executables.Contains(fi.Extension))
                return $"Недопустимое расширение файла '{fi.Extension}'";
            if (fi.Length > Sizes.Size4Mb)
                return "Файл имеет слишком большой размер!";
            return null;
        }

        protected IList<AttachClassificWeb> GetAttachClassificList()
        {
            return WCFClient.GetEFServiceFunc().AttachClassifics.ToList();
        }

        private const string CHOOSE_SELECTED_CLASSIFICATION = "Параметр 'Классификация документа' должен быть указан.";
        private const string CHOOSE_SELECTED_EXECUTOR = "Параметр 'Исполнитель' должен быть указан.";
        private const string CHOOSE_SELECTED_STORAGE_PLACE = "Параметр 'Место хранения оригинала' должен быть указан.";
        private const string CHOOSE_SELECTED_FILE_NAME = "Необходимо выбрать вложение.";
        private string _selectedFileName2;
        private string _displayFileName2;

        [JsonIgnore]
        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case nameof(AttachClassificID): return AttachClassificID == null ? CHOOSE_SELECTED_CLASSIFICATION : null;
                    case nameof(ExecutorID): return (ExecutorID == null && HasExecutorList) ? CHOOSE_SELECTED_EXECUTOR : null;
                    case nameof(ExecutorName): return (string.IsNullOrWhiteSpace(ExecutorName) && !HasExecutorList) ? CHOOSE_SELECTED_EXECUTOR : null;
                    case nameof(OriginalPlaceID): return OriginalPlaceID.HasValue ? null : CHOOSE_SELECTED_STORAGE_PLACE;
                  //  case "SelectedFileName": return string.IsNullOrEmpty(SelectedFileName) ? CHOOSE_SELECTED_FILE_NAME : null;
                    case nameof(Comment): return string.IsNullOrEmpty(Comment) ? CHOOSE_SELECTED_FILE_NAME : null;
                    default:
                        return string.Empty;
                }
            }
        }

        public override bool WebExecuteSave()
        {
            Attach.Type = Type;
            if (!UpdateAttachment())
                return false;

            DbHelper.SaveEntity(Attach);
            return true;
        }

        private bool UpdateAttachment()
        {
            try
            {
                if(string.IsNullOrWhiteSpace(SelectedFileName2) || !SelectedFileName2.StartsWith("../") || ID <= 0) return true;

                var fullFilePath = HttpContext.Current.Server.MapPath(SelectedFileName2);

                if (!File.Exists(fullFilePath))
                {
                    WebLastModelError = $"Файл не найден! ({fullFilePath})";
                    return false;
                }
                var fs = new FileStream(fullFilePath, FileMode.Open, FileAccess.Read);
                if (fs.Length > 10048576)
                {
                    WebLastModelError = "Файл имеет слишком большой размер!";
                    return false;
                }

                var blob = new byte[fs.Length];
                fs.Read(blob, 0, blob.Length);
                fs.Close();
                //DocFileBody.Body = blob;

                var newBody = new DocFileBodyWeb();

//                if (Document.DocFileBody == null)
                //                  Document.DocFileBody = new DocFileBodyWeb();
                newBody.FileName = GetTrueFileName(fullFilePath);
                newBody.DocumentID = ID;

                try
                {
                    var id = WCFClient.GetEFServiceFunc().SaveDocFileBodyForDocument(ID, newBody, blob);
                    Attach.FileContentId = id;
                }
                catch(Exception ex)
                {
                    // RaiseAttachmentUploadFailed();
                    Logger.LogEx(ex);
                    return false;
                }
            }
            catch(Exception ex)
            {
                Logger.LogEx(ex);
                //  RaiseAttachmentUploadFailed();
                return false;
            }

            return true;
        }

        public override bool WebExecuteDelete(long id)
        {
            using (var tr = WCFClient.GetEFServiceFunc().Database.BeginTransaction())
            {
                try
                {
                    if (DbHelper.DeleteEntity(Attach))
                    {
                        if (Attach.FileContentId.HasValue)
                            WCFClient.GetEFServiceFunc().Database.ExecuteSqlCommand($"delete from pfr_basic.file_content where id={Attach.FileContentId.Value}");
                    }

                    DbHelper.SaveChanges();
                    tr.Commit();
                }
                catch
                {
                    tr.Rollback();
                    return false;
                }
            }

            return true;
        }

        public static void DeleteLinkedDocument(long id)
        {
            var item = WCFClient.GetEFServiceFunc().Attaches.FirstOrDefault(a => a.ID == id);
            if(item == null)
                Logger.Log($"ERROR: Linked document not found for delete -> {id}");
            else
            {
                using (var tr = WCFClient.GetEFServiceFunc().Database.BeginTransaction())
                {
                    try
                    {
                        if(DbHelper.DeleteEntity(item))
                            if (item.FileContentId.HasValue)
                            {
                                WCFClient.GetEFServiceFunc().Database.ExecuteSqlCommand($"delete from pfr_basic.file_content where id={item.FileContentId.Value}");
                                DbHelper.SaveChanges();
                            }

                        tr.Commit();
                    }
                    catch
                    {
                        tr.Rollback();
                    }
                }
            }
        }
    }
}