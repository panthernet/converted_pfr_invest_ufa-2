﻿using System.Globalization;
using Newtonsoft.Json;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic.Attributes;
using PFR_INVEST.Web2.BusinessLogic.Models;

namespace PFR_INVEST.Web2.Models
{
    [WebReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.User)]
    [WebEditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.User)]
    public class WebNpfRedistGenerateTransferViewModel: PureWebDialogViewModel
    {
        [JsonIgnore]
        public string Message { get; set; } = "Генерация финреестров...";

        [JsonIgnore]
        public bool CanClose { get; protected set; }

        public long EntryID { get; set; }

        public WebNpfRedistGenerateTransferViewModel() : this(0)
        {
        }

        public WebNpfRedistGenerateTransferViewModel(long id)
        {
            WebModelHelperEx.InitializeDefault(this, "Npf", "_RzlGenerateTransfersViewPartial");
            WebModelHelperEx.UpdateModelState(this);
            AjaxMain = "_SharedAjaxFormSimplePartialView";
            EntryID = id;
        }

        public void Execute()
        {
            if (EntryID != 0 && !CanClose)
            {
                var res = WCFClient.GetDataServiceFunc().GenerateRegisterByERZL(EntryID);
                if (res.ContainsKey(true))
                    IsSaveCompleted = true;
                else
                {
                    Message = "Не удалось сгенерировать финреестры!";
                    return;
                }

                var num = res[true].ToString(CultureInfo.InvariantCulture) + ".";
                
                //var num = 100500;
                Message = "Финреестры успешно сгенерированы под номером " + num;
                //JournalLogger.LogEvent("Реестр перечислений", JournalEventType.INSERT, null, "Реестр перечислений сгенерирован", $"Номер :{num} ");

                // DXMessageBox.Show("Финреестры успешно сгенерированы под номером " + num);
            }
            CanClose = true;
        }
    }
}