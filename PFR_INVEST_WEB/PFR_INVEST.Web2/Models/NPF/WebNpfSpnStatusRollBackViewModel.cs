﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic.Attributes;
using WebViewModel = PFR_INVEST.Web2.BusinessLogic.Models.WebViewModel;

namespace PFR_INVEST.Web2.Models
{
    [WebReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OFPR_worker)]
    [WebEditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    public class WebNpfSpnStatusRollBackViewModel: WebViewModel
    {
        private long _id;

        public WebNpfSpnStatusRollBackViewModel(long id)
        {
            WebModelHelperEx.InitializeDefault(this, "Npf", "_SpnStatusRollbackViewPartial");
            _id = id;
        }

        public override long WebGetEntryId()
        {
            return _id;
        }

        public override bool WebExecuteSave()
        {
            ViewModelHelperNpf.RollbackFinregisterStatus(_id);
            return true;
        }
    }
}