﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic.Attributes;
using PFR_INVEST.Web2.Database.Classes;

namespace PFR_INVEST.Web2.Models
{
    [WebReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OFPR_worker)]
    [WebEditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [WebDeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager)]
    public class WebNpfTempFinregisterViewModel: WebNpfSpnFinregisterViewModel2
    {
        [JsonIgnore]
		public override bool IsCFRCountVisible => true;
        [JsonIgnore]
        public override bool TempAllocationVisibilityBool => true;
        [JsonIgnore]
	    public override bool CommonFinregisterVisibilityBool => false;

        public WebNpfTempFinregisterViewModel() : this (0,0) { }

	    // recordID - идентификатор основной записи реестра, поэтому никаких дополнительных запросов при проверке привязанных НПФ не требуется
		public WebNpfTempFinregisterViewModel(long recordId, long parentId)
			: base(recordId, parentId)
		{
			FilterNPFList();
		}

		public override bool WebExecuteSave()
		{
			/*if (State == ViewModelState.Create)
			{
				BindNPFAccounts();
                var finregWithNPFList = WCFClient.GetDataServiceFunc().GetFinregisterWithCorrAndNPFList(Register.ID);

			    var targetFr = finregWithNPFList.FirstOrDefault(x => x.Finregister.CrAccID == Finregister.CrAccID && x.Finregister.DbtAccID == Finregister.DbtAccID);
				if (targetFr == null)
				{
					var caId = GetCurrentFinregisterContragent();
					string npfName = SelectedNPF.FormalizedNameFull;
					if (caId != null)
					{
						npfName = DataContainerFacade.GetObjectProperty<Contragent, long, string>(caId, "Name");
					}

					var newList = finregWithNPFList.ToList();
					//ParentModel.FinregWithNPFList
					newList.Add(new FinregisterWithCorrAndNPFListItem
					{
						CFRCount = Finregister.CFRCount,
						Count = Finregister.Count,
						Finregister = Finregister,
						ID = 0,
						IsItemChanged = true,
						NPFName = npfName,
						//NPFStatus = SelectedNPF.GetLegalStatus().Name,
						ZLCount = Finregister.ZLCount
					});

					_parentModel.FinregWithNPFList = newList;
				}
				else
				{
					targetFr.CFRCount = Finregister.CFRCount;
					targetFr.Count = Finregister.Count;
					targetFr.ZLCount = Finregister.ZLCount;
					targetFr.IsItemChanged = true;
					//targetFR.NPFName = SelectedNPF.FormalizedNameFull;
					//targetFR.NPFStatus = SelectedNPF.GetLegalStatus().Name;
				}

				_parentModel.RaiseFinregisterChanged();
				
				return;
			}
            */

		    return false;
		}

		public override bool WebBeforeExecuteSaveCheck()
		{
			// валидация корректности сумм после сохранения
		    if (Register == null || Register.ID <= 0 || SelectedNPF == null) return base.WebBeforeExecuteSaveCheck();
		    if (RegisterIdentifier.IsToNPF(Register.Kind))
		    {// если редактирование не кешированного списка (добавление\редактирование НПФ в изъятие)
		        var allowedFR = GetFinregWithAllowedSums();
		        if (allowedFR == null) return false;
		        // для изъятия суммы не должны превосходить рассчетные
		        if (FrCount > allowedFR.Count)
		        {
		           WebLastModelError = $"Недостаточно средств в размещении. Максимально допустимая сумма {allowedFR.Count}";
		            return false;
		        }

		        if (ZLCount > allowedFR.ZLCount)
		        {
		            WebLastModelError = $"Неверно указано количество ЗЛ. Максимально допустимое количество {allowedFR.ZLCount}";
		            return false;
		        }
		    }
		    else
		    {// редактирование НПФ размещения
		        // при редактировании исчерпанного размещения будет недостаток средств, который должен быть компенсирован (не даем сохранить размещение, если сумма изъяти окажется в итоге больше суммы размещения)
		        var allowedFR = GetFinregWithAllowedSums();
		        if (allowedFR == null) return true;

		        if (-allowedFR.Count > FrCount)
		        {
		            WebLastModelError = $"Сумма в размещении недостаточна для существующих в системе изъятий. Минимально допустимая сумма {-allowedFR.Count}";
		            return false;
		        }

		        if (-allowedFR.ZLCount > ZLCount)
		        {
		            WebLastModelError = $"Неверно указано количество ЗЛ. Минимально допустимое количество {-allowedFR.ZLCount}";
		            return false;
		        }

		        // также надо проверить остаток суммы при смене выбранного НПФ
		        if (Finregister.ID > 0)
		        {
		            var dbFR = DataContainerFacade.GetByID<Finregister>(Finregister.ID);
		            (new List<Finregister> { dbFR }).PopulateBankAccounts();
		            var accountDebit = dbFR.GetDebitAccount();
		            var accountCredit = dbFR.GetCreditAccount();

		            if (accountDebit != null && accountCredit != null)
		            {
		                var caId = RegisterIdentifier.IsFromNPF(Register.Kind.ToLower()) ? accountCredit.ContragentID : accountDebit.ContragentID;

		                if (caId != SelectedNPF.ContragentID)
		                {
		                    var caName = DataContainerFacade.GetObjectProperty<Contragent, long, string>(caId, "Name");
		                    var portfID = Register.PortfolioID.Value;
		                    //var selName = SelectedNPF.GetContragent().Name;

		                    var allListSPN = WCFClient.GetDataServiceFunc().GetSPNAllocatedFinregisters(portfID).Where(x => x.NPFName == caName && x.ID != Finregister.ID);
		                    var retListSPN = WCFClient.GetDataServiceFunc().GetSPNReturnedFinregisters(portfID).Where(x => x.NPFName == caName && x.ID != Finregister.ID);

		                    if (allListSPN.Sum(x => x.Count) - retListSPN.Sum(x => x.Count) < 0)
		                    {
		                        WebLastModelError = $"Невозможно сменить НПФ, так как сумма изъятия превышает остаток размещения";
		                        return false;
		                    }

		                    if (allListSPN.Sum(x => x.ZLCount) - retListSPN.Sum(x => x.ZLCount) < 0)
		                    {
		                        WebLastModelError = $"Невозможно удалить НПФ, так как количество ЗЛ изъятия превышает количество ЗЛ размещения";
		                        return false;
		                    }
		                }
		            }

		            //return true;
		        }
		    }

		    return base.WebBeforeExecuteSaveCheck();
		}

		protected override bool WebBeforeExecuteDeleteCheck()
		{
		    if (Register == null || Register.ID <= 0 || !RegisterIdentifier.IsTempAllocation(Register.Kind)) return base.WebBeforeExecuteDeleteCheck();
		    var dbFR = DataContainerFacade.GetByID<Finregister>(Finregister.ID);
		    (new List<Finregister> { dbFR }).PopulateBankAccounts();
		    var accountDebit = dbFR.GetDebitAccount();
		    var accountCredit = dbFR.GetCreditAccount();

		    if (accountDebit != null && accountCredit != null)
		    {
		        var caID = RegisterIdentifier.IsFromNPF(Register.Kind.ToLower()) ? accountCredit.ContragentID : accountDebit.ContragentID;

		        var caName = DataContainerFacade.GetObjectProperty<Contragent, long, string>(caID, "Name");
		        var portfID = Register.PortfolioID.Value;
		        //var selName = SelectedNPF.GetContragent().Name;

		        var allListSPN = WCFClient.GetDataServiceFunc().GetSPNAllocatedFinregisters(portfID).Where(x => x.NPFName == caName && x.ID != Finregister.ID);
		        var retListSPN = WCFClient.GetDataServiceFunc().GetSPNReturnedFinregisters(portfID).Where(x => x.NPFName == caName && x.ID != Finregister.ID);

		        if (allListSPN.Sum(x => x.Count) - retListSPN.Sum(x => x.Count) < 0)
		        {
		            WebLastModelError = $"Невозможно удалить НПФ, так как сумма изъятия превышает остаток размещения";
		            return false;
		        }

		        if (allListSPN.Sum(x => x.ZLCount) - retListSPN.Sum(x => x.ZLCount) < 0)
		        {
		            WebLastModelError = $"Невозможно удалить НПФ, так как количество ЗЛ изъятия превышает количество ЗЛ размещения";
		            return false;
		        }
		    }

		    return base.WebBeforeExecuteDeleteCheck();
		}

		protected override void FilterNPFList()
		{
		    var allNPF = DbHelper.GetEF().GetActiveNpfList();// WCFClient.GetDataServiceFunc().GetNPFListLEHib(StatusFilter.NotDeletedFilter).ToList();

			// исключить НПФ, содержащиеся в реестре и НПФ, у которых недостаточно средств в размещении
			// добавить НПФ, если этот нпф текущий при редактировании
			var avList = WebNpfSpnRegisterViewModel.GetAvailableNPFList(Register, true);

			var caID = GetCurrentFinregisterContragent();

			if (caID != null)
			{
				NPFList = allNPF.Where(x => avList.Any(y => y.NPFName == x.Contragent.Name)
					|| x.ContragentID == caID).ToList();
			}
			else
			{
				NPFList = allNPF.Where(x => avList.Any(y => y.NPFName == x.Contragent.Name)).ToList();
			}
		}

		protected long? GetCurrentFinregisterContragent()
		{
		    if (Finregister == null) return null;
		    var accountDebit = Finregister.AccountDebit;
		    var accountCredit = Finregister.AccountCredit;

		    if (accountDebit != null && accountCredit != null)
		    {
		        return (RegisterIdentifier.IsFromNPF(Register.Kind.ToLower()) ? accountCredit.ContragentID : accountDebit.ContragentID);
		    }
		    return null;
		}
    }
}