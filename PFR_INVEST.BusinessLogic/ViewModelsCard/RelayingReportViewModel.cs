﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Helpers;

namespace PFR_INVEST.BusinessLogic
{
    public class RelayingReportViewModel : SaleReportViewModel, IUpdateRaisingModel
    {
        #region property


        private ObservableList<Destination> destinationItems;
        public ObservableList<Destination> DestinationItems
        {
            get
            {
                return destinationItems;
            }
            set
            {
                destinationItems = value;
                OnPropertyChanged("DestinationItems");
            }
        }

        public bool IsReportCanCreate => m_CbInOrderList.Any();

        #endregion


        public RelayingReportViewModel(long lOrderID)
            : this(lOrderID, ViewModelState.Create)
        {

        }

        public RelayingReportViewModel(long lID, ViewModelState action)
            : base(lID, action)
        {
            OrderParentId = Order.ID;
            DestinationItems = new ObservableList<Destination>();
            var list = Order.GetChildrenList();
            foreach (var cbOrder in list)
            {
                var destination = new Destination(cbOrder.ID);
                destination.ChangedProperty += ChangedProperty;
                DestinationItems.Add(destination);
            }
            DestinationItems.ToList().ForEach(i => i.ResetPrperty());
			// модель, которая зачем-то используется для хранения генерируемых отчетов по покупке не выполняет фильтрацию по уже созданным ЦБ и по умолчанию выставляет первую ЦБ из списка неотфильтрованных
			DestinationItems.ToList().ForEach(item => item.BuyReportModel.SelectedSecurity = SelectedSecurity);
            // необходимо сохранить сумму НКД на одну облигацию в БД
            Report.OneNKD = NKDPerOneObl;
        }

        #region Execute implementation
        public override bool CanExecuteSave()
        {
            return Validate() && IsDataChanged;
        }


        protected override void ExecuteSave()
        {
            switch (State)
            {
                case ViewModelState.Read:
                    break;
                case ViewModelState.Edit:
                case ViewModelState.Create:
                    //if (MessageBox.Show(
                    //    CONTINUE_TO_SAVE_QUESTION,
                    //    "Подтверждение",
                    //    MessageBoxButton.YesNo,
                    //    MessageBoxImage.Exclamation) != MessageBoxResult.Yes)
                    //{
                    //    return;
                    //}

                    ID = DataContainerFacade.Save<OrdReport, long>(Report);
                    Report.ID = ID;
                    m_OriginalCount = Count;
                    //RefreshAlreadyAddedReports();
                    //RefreshMaxSaleOrBuySecurityCount();

                    foreach (var destination in DestinationItems)
                    {
                        if (destination.BuyReportModel.Report.Count == 0) continue;
                        ID = DataContainerFacade.Save<OrdReport, long>(destination.BuyReportModel.Report);
                        destination.BuyReportModel.Report.ID = ID;
                    }
                    RefreshOrderRelayingViewModel();
                    break;


            }

        }


        public override bool CanExecuteDelete()
        {
            return State != ViewModelState.Create;
        }

        protected override void ExecuteDelete(int delType)
        {

            DataContainerFacade.Delete(Report);
            foreach (var destination in DestinationItems)
            {
                if (destination.BuyReportModel.Report.ID > 0)
                {
                    DataContainerFacade.Delete(destination.BuyReportModel.Report);
                }
                destination.Yield = 0;
            }

            RefreshOrderRelayingViewModel();


            State = ViewModelState.Read;
        }



        #endregion

        #region Validation
        private const string INVALID_DATE_ORDER = "Не указана дата сделки";
        private const string INVALID_DATE_DEPO = "Не указана дата операции по счету ДЕПО";
        private const string INVALID_SELECTED_CONTRAGENT = "Не указан покупатель";
        private const string INVALID_CURS = "Неверно указан курс";

        public override string this[string columnName]
        {
            get
            {

                switch (columnName)
                {
                    case "Count":
                        return OrderReportsHelper.CheckCount(Count);
                    case "DateOrder": return DateOrder == null ? INVALID_DATE_ORDER : null;
                    default: return null;
                }
            }
        }

        public override bool Validate()
        {
            return DestinationItems.All(d => d.Validate()) && string.IsNullOrEmpty(this["DateOrder"]);
        }
        #endregion

        /// <summary>
        /// список "инф о покупке/продаже" для которых нет отчета
        /// </summary>
        /// <param name="cbInOrderList"></param>
        /// <returns></returns>
        public List<CbInOrder> GetCbInOrderListForCreate(List<CbInOrder> cbInOrderList)
        {
            List<CbInOrder> list = new List<CbInOrder>();
            foreach (var cbInOrder in cbInOrderList)
            {
                if (cbInOrder.Count.HasValue && cbInOrder.Count > 0)
                {
                    var ordReports = DataContainerFacade.GetListByProperty<OrdReport>("CbInOrderID", cbInOrder.ID);
                    if (ordReports == null || ordReports.Count == 0)
                    {
                        list.Add(cbInOrder);
                    }
                }
            } return list;
        }
        /// <summary>
        /// список ЦБ для которых нет отчетов
        /// </summary>
        /// <param name="securities"></param>
        /// <returns></returns>
        public List<Security> GetSecurityListForCreate(List<Security> securities)
        {

            return securities.Where(s => m_CbInOrderList.Any(cbo => cbo.SecurityID == s.ID)).ToList();

        }
        /// <summary>
        /// Изменение значения поля в  таблице "Перекладка бумаг"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ChangedProperty(object sender, EventArgs e)
        {
            IsDataChanged = true;
        }


        public class Destination : INotifyPropertyChanged, IDataErrorInfo
        {

            #region property

            public BuyRelayingReportViewModel BuyReportModel;
            public EventHandler ChangedProperty;

            private string namePortfolio;
            public string NamePortfolio
            {
                get
                {
                    if (BuyReportModel != null)
                    {
                        return BuyReportModel.PortfolioYear;
                    }
                    return string.Empty;
                }
                set
                {
                    namePortfolio = value;
                }
            }

            public decimal countSecurity;
            public decimal CountSecurity
            {
                get
                {
                    return countSecurity;
                }
                set
                {
                    countSecurity = value;
                    OnPropertyChanged("CountSecurity");
                    OnPropertyChanged("IsYieldReadOnly");
                }
            }

            private string accountNumber;
            public string AccountNumber
            {
                get
                {
                    if (BuyReportModel != null)
                    {
                        return BuyReportModel.DEPOAccountNumber;
                    }
                    return string.Empty;
                }
                set
                {
                    accountNumber = value;

                }
            }

            private decimal yild;
            public decimal Yield
            {
                get
                {
                    return yild;
                }
                set
                {
                    yild = value;
                    OnPropertyChanged("Yield");

                    if (BuyReportModel != null && BuyReportModel.Report != null)
                    {
                        BuyReportModel.Report.Yield = value;
                    }

                    if (ChangedProperty != null)
                    {
                        ChangedProperty(this, new EventArgs());
                    }
                }
            }

            #endregion


            public void ResetPrperty()
            {
                if (BuyReportModel != null)
                {

                    CountSecurity = BuyReportModel.Count;
                    Yield = 0;
                    //Yield = BuyReportModel.Yield;
                }
            }


            public Destination(long lOrderID)
            {
                BuyReportModel = new BuyRelayingReportViewModel(lOrderID);
            }


            public bool Validate()
            {
                return string.IsNullOrEmpty(this["Yield"]);
            }

            public string this[string columnName]
            {
                get
                {
                    switch (columnName)
                    {
                        case "Yield":
                            return CountSecurity == 0 ? null : OrderReportsHelper.CheckYield(Yield);
                    }
                    return null;
                }
            }


            public bool IsYieldReadOnly => CountSecurity == 0;

            public string Error { get; private set; }

            public event PropertyChangedEventHandler PropertyChanged;

            protected virtual void OnPropertyChanged(string propertyName)
            {
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        }


        public class BuyRelayingReportViewModel : BuyReportViewModel
        {
            public BuyRelayingReportViewModel(long lOrderID)
                : base(lOrderID)
            {
                // необходимо сохранить сумму НКД на одну облигацию в БД
                Report.OneNKD = NKDPerOneObl;
            }

            public BuyRelayingReportViewModel(long lID, ViewModelState action)
                : base(lID, action)
            {
                Report.OneNKD = NKDPerOneObl;
            }


        }

        public IList<KeyValuePair<Type, long>> GetUpdatedList()
        {
            var list = new List<KeyValuePair<Type, long>>();

            foreach (var d in DestinationItems)
            {
                list.Add(new KeyValuePair<Type, long>(typeof(OrdReport), d.BuyReportModel.Report.ID));
            }

            list.Add(new KeyValuePair<Type, long>(typeof(OrdReport), Report.ID));

            return list;
        }
    }


}
