﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Journal;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_directory_editor)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_manager)]
    public class SecurityViewModel : ViewModelCard
    {
        private Security _CB;
        public Security CB
        {
            get { return _CB; }
            set
            {
                _CB = value;
                OnPropertyChanged("CB");
            }
        }

        public int SelectedPaymentPeriod
        {
            get
            {
                switch (CB.PaymentPeriod)
                {
                    case 1: return 0;
                    case 2: return 1;
                    case 4: return 2;
                    default: return 0;
                }
            }
            set
            {
                switch (value)
                {
                    case 0: CB.PaymentPeriod = 1; break;
                    case 1: CB.PaymentPeriod = 2; break;
                    case 2: CB.PaymentPeriod = 4; break;
                    default: CB.PaymentPeriod = 1; break;
                }
            }
        }


        private IList<Currency> currList;
        public IList<Currency> CurrencyList
        {
            get { return currList; }
            set { currList = value; OnPropertyChanged("CurrencyList"); }
        }

        private IList<SecurityKind> secKindList;
        public string[] SecurityKindList
        {
            get
            {
                return secKindList.Select(i => i.Name).ToArray();
            }
            //set { secKindList = value; OnPropertyChanged("SecurityKindList"); }
        }

        private IList<Payment> paymentsList;
        public IList<Payment> PaymentsList
        {
            get { return paymentsList; }
            set { paymentsList = value; OnPropertyChanged("PaymentsList"); }
        }

        private IList<MarketPrice> marketPricesList = new List<MarketPrice>();
        public IList<MarketPrice> MarketPricesList
        {
            get { return marketPricesList; }
            set { marketPricesList = value; OnPropertyChanged("MarketPricesList"); }
        }

        public MarketPrice SelectedMP { get; set; }

        public SecurityKind SelectedSecKind
        {
            get
            {
                return secKindList.FirstOrDefault(secKind => secKind.ID as long? == CB.KindID);
            }

            set
            {
                //if (value != null)
                //{
                    CB.KindID = value == null ? (long?)null : value.ID;
                    OnPropertyChanged("SelectedSecKind");
                //}
            }
        }

        private string selectedSecKindName;
        public string SelectedSecKindName
        {
            get 
            {
                //return this.selectedSecKindName; 
                return SelectedSecKind == null ? selectedSecKindName : SelectedSecKind.Name;
            }
            set
            {
                if(value!= null)
                    SelectedSecKind = secKindList.FirstOrDefault(sk => sk.Name.Trim().ToLower().Equals(value.Trim().ToLower()));
                selectedSecKindName = value;
                OnPropertyChanged("SelectedSecKindName");
            }
        }

        public Currency SelectedCurrency
        {
            get
            {
                return currList.FirstOrDefault(curr => curr.ID as long? == CB.CurrencyID);
            }
            set
            {
                if (value != null)
                {
                    CB.CurrencyID = value.ID;
                    OnPropertyChanged("SelectedCurrency");
                }
            }
        }

        public ICommand AddPayment { get; private set; }
        public SecurityViewModel(long id, ViewModelState action)
            : base(typeof(SecuritiesListViewModel))
        {
            DataObjectTypeForJournal = typeof(Security);
            try
            {
                AddPayment = new DelegateCommand(p => CanExecuteAddPayment(), p => ExecuteAddPayment());

                CB = action == ViewModelState.Create ? new Security { Status = 0 } : DataContainerFacade.GetByID<Security, long>(id);
                ID = CB.ID;
                CB.PropertyChanged += (sender, e) => { IsDataChanged = true; };
                CB.OnValidate += CB_Validate;
                RefreshLists();

                CB = action == ViewModelState.Create ? new Security { Status = 0, PaymentPeriod = SelectedPaymentPeriod } : DataContainerFacade.GetByID<Security, long>(id);
                ID = CB.ID;
                CB.PropertyChanged += (sender, e) => { IsDataChanged = true; };
                CB.OnValidate += CB_Validate;
                RefreshLists();

                if (action == ViewModelState.Create)
                {
                    SelectedCurrency = CurrencyList.First(a => CurrencyIdentifier.IsRUB(a.Name));
                }

                DeletePayment = new DelegateCommand(o => CanDeletePayment(), o => ExecuteDeletePayment());
                DeleteMP = new DelegateCommand(o => CanDeleteMP(), o => ExecuteDeleteMP());
                AddMP = new DelegateCommand(o => CanAddMP(), o => ExecuteAddMP());
                OnPropertyChanged("DeletePayment");
            }
            catch (Exception e)
            {
                if (DoNotThrowExceptionOnError == false)
                    throw;

                Logger.WriteException(e);
                throw new ViewModelInitializeException();
            }
        }

        public bool CanExecuteAddPayment()
        {
            return State == ViewModelState.Edit && CB.ID > 0;
        }

        public void ExecuteAddPayment()
        {
            if (DialogHelper.AddPayment(CB.ID))
            {                
                JournalLogger.LogModelEvent(this, JournalEventType.INSERT, "Ценная бумага, Добавление выплаты", "SecurityID:" + CB.ID);
                PefreshPayments();
            }
        }

        private void CB_Validate(object sender, BaseDataObject.ValidateEventArgs e)
        {
            var cb = (sender as Security);
            if (cb != null)
            {
                const string error = "Не заполнены все обязательные поля!";
                const string errorLength = "Неверная длина поля";
                switch (e.PropertyName)
                {
                    case "KindID": e.Error = string.IsNullOrEmpty(SelectedSecKindName) ? error : null;//cb.KindID > 0 ? null : error;
                        break;
                    case "CurrencyID": e.Error = cb.CurrencyID > 0 ? null : error;
                        break;
                    case "Name": e.Error = string.IsNullOrWhiteSpace(cb.Name) ? error : 
                        cb.Name.Length > 63 ? errorLength : null;
                        break;
                    case "Issue": e.Error = string.IsNullOrWhiteSpace(cb.Issue) ? error :
                        cb.Issue.Length > 511 ? errorLength : null;
                        break;
                    case "SecurityId": e.Error = string.IsNullOrWhiteSpace(cb.SecurityId) ? error :
                        cb.SecurityId.Length > 49 ? errorLength : null;
                        break;
                    case "RegNum": e.Error = string.IsNullOrWhiteSpace(cb.RegNum) ? error :
                        cb.RegNum.Length > 49 ? errorLength : null;
                        break;
                    case "ISIN": e.Error = string.IsNullOrWhiteSpace(cb.ISIN) ? error :
                        cb.ISIN.Length > 49 ? errorLength : null;
                        break;
                    case "IssueVolume": e.Error = cb.IssueVolume.HasValue ? null : error;
                        break;
                    case "PlacedVolume": e.Error = cb.PlacedVolume.HasValue ? null : error;
                        break;
                    case "NomValue": e.Error = cb.NomValue.HasValue ? null : error;
                        break;
                    case "PaymentPeriod": e.Error = cb.PaymentPeriod.HasValue ? null : error;
                        break;
                    case "RepaymentDate": e.Error = cb.RepaymentDate.HasValue ? null : error;
                        break;
                    case "LocationDate": e.Error = cb.LocationDate.HasValue ? null : error;
                        break;
                    default: break;
                }
            }
        }

        public Payment SelectedPayment { set; get; }


        #region Execute implementation

        public override bool CanExecuteDelete()
        {
            return State != ViewModelState.Create && CB.Status != -1;
        }

        protected override void ExecuteDelete(int delType)
        {
            CB.Status = -1;
            foreach (var marketPrice in MarketPricesList)
            {
#warning // StatusID почему-то не сохраняется в базу
                marketPrice.StatusID = -1;   
                DataContainerFacade.Save<MarketPrice, long>(marketPrice);
            }
            DataContainerFacade.Save<Security, long>(CB);
            base.ExecuteDelete(DocOperation.Archive);

        }

        public bool CanDeletePayment()
        {
            return SelectedPayment != null && CheckDeleteAccess(typeof(PaymentViewModel));
        }

        public void ExecuteDeletePayment()
        {
            if (SelectedPayment != null)
            {
                
				DataContainerFacade.Delete<Payment>(SelectedPayment.ID);
                JournalLogger.LogModelEvent(this, JournalEventType.DELETE, "Ценная бумага, Удаление выплаты", String.Format("Ценная бумага ID:{0}, SecurityName:{1} ", SelectedPayment.ID, SelectedPayment.SecurityName));		
			}
            RefreshLists();
        }

        public bool CanDeleteMP()
        {
            return SelectedMP != null && CheckDeleteAccess(typeof(MarketPriceViewModel));
        }

        public void ExecuteDeleteMP()
        {
			if (SelectedMP != null)
			{
				DataContainerFacade.Delete(SelectedMP);
                JournalLogger.LogEvent("Рыночная цена", JournalEventType.DELETE, SelectedMP.ID, "From: " + GetType().Name, null, typeof(MarketPrice)); 
                ViewModelManager.UpdateDataInAllModels(typeof(MarketPrice), SelectedMP.ID);
			}
            RefreshLists();
        }

        public bool CanAddMP()
        {
            return CB.ID > 0 && EditAccessAllowed;
        }

        public void ExecuteAddMP()
        {
            DialogHelper.AddMP(this);
        }

        public override bool CanExecuteSave()
        {
            return IsDataChanged && String.IsNullOrEmpty(Validate());
        }

        protected override void ExecuteSave()
        {
            if (CanExecuteSave())
            {
                switch (State)
                {
                    case ViewModelState.Read:
                        break;
                    case ViewModelState.Edit:
                    case ViewModelState.Create:
                        //SecurityKind kind = this.SecurityKindList.FirstOrDefault(sk => sk.Name.Trim().ToLower().Equals(this.SelectedSecKindName.Trim().ToLower()));
                        if (SelectedSecKind == null)
                        {
                            var kind = new SecurityKind {Name = SelectedSecKindName};
                            kind.ID = DataContainerFacade.Save<SecurityKind, long>(kind);
                            secKindList.Add(kind);
                            OnPropertyChanged("SecurityKindList");
                            CB.KindID = kind.ID;
                        }
                        ID = CB.ID = DataContainerFacade.Save<Security, long>(CB);
                        break;
                    default:
                        break;
                }
            }
        }
        #endregion

        public void RefreshLists()
        {
            bool alreadyChanged = IsDataChanged;
            secKindList = DataContainerFacade.GetList<SecurityKind>();
            CleanedSecKindList();
            CurrencyList = DataContainerFacade.GetList<Currency>();
            PefreshPayments();
            MarketPricesList = CB.GetMarketPrices();
            IsDataChanged = alreadyChanged;
        }
        private void CleanedSecKindList()
        {
            secKindList = DataContainerFacade.GetList<SecurityKind>();

            IList<Security> securitiesList = BLServiceSystem.Client.GetAllSecuritiesListHib();
            var skl = secKindList;
            foreach (var s in skl)
            {
                var sl = securitiesList.Where(sec => sec.KindID == s.ID);
                if (!sl.Any())
                    secKindList = secKindList.Where(skll => skll.ID != s.ID).ToList();
            }
        }
        private void PefreshPayments()
        {
            if (CB.ExtensionData != null) CB.ExtensionData.Clear();
            PaymentsList = CB.GetPayments();
            IsDataChanged = false;
        }


        private const string notNullFields = "KindID|CurrencyID|Name|Issue|SecurityId|RegNum|ISIN|IssueVolume|PlacedVolume|NomValue|PaymentPeriod|LocationDate|RepaymentDate";

        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "SelectedSecKindName": return string.IsNullOrEmpty(SelectedSecKindName) ? "Не выбрано значение" : null;
                    case "SelectedCurrency": return SelectedCurrency == null ? "Не выбрано значение" : null;
                    default: break;
                }
                return null;
            }
        }


        private string Validate()
        {
            foreach (string key in notNullFields.Split('|'))
            {
                var e = new BaseDataObject.ValidateEventArgs(key);
                CB_Validate(CB, e);
                if (!string.IsNullOrEmpty(e.Error))
                {
                    return "Не заполнены все обязательные поля!";
                }
            }
            return "";
        }

        public void ViewPayment(long paymentID)
        {
            if (DialogHelper.ViewPayment(paymentID))
                PefreshPayments();
        }

        public ICommand DeletePayment { get; private set; }

        public ICommand DeleteMP { get; private set; }

        public ICommand AddMP { get; private set; }
    }
}
