﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
	[DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager)]
	public class SPNAllocationViewModel : RegisterViewModel
	{
		public override bool IsPortfolioVisible => true;
	    public override bool IsPortfolioEnabled => true;

	    public override bool IsSumCFRVisible => true;
	    public override bool IsSumZLVisible => true;
	    public override bool IsCompanyVisible => false;
	    public override bool IsCompanyRequired => false;
		public override bool IsERZLVisible => false;
	    public override bool IsPayAssVisible => false;

	    #region GridColumnSettings
        public override bool IsCFRColumnVisible => true;
	    public override bool IsZLColumnEditable => false;
	    public override bool IsCountColumnEditable => false;
	    public override bool IsCFRCountColumnEditable => false;

	    #endregion

		#region Kind
		public override bool IsKindEditable => false;

	    private static readonly List<string> kinds = new List<string>
	    {
			RegisterIdentifier.TempAllocation
		};

		public override List<string> Kinds => kinds;

	    #endregion

		public SPNAllocationViewModel(long recordID, ViewModelState action)
			: base(recordID, action)
		{
			RefreshPortfoliosList();
			if (action != ViewModelState.Create)
			{
				if (register.PortfolioID != null)
					SelectedPortfolio = PortfoliosList.SingleOrDefault(x => x.ID == register.PortfolioID.Value);
				//Content = Contents.First();
			}
			else
			{
				SelectedPortfolio = PortfoliosList.FirstOrDefault();
			    IsDataChanged = true;
			}						
		}

		public void RefreshPortfoliosList()
		{
			PortfoliosList = new List<Portfolio>(DataContainerFacade.GetList<Portfolio>());
		}

		public override void ExecuteDeleteFR()
		{
			if (SelectedPortfolio != null)
			{
				var allNPFSPN = BLServiceSystem.Client.GetSPNAllocatedFinregisters(SelectedPortfolio.ID).Where(x => x.NPFName == SelectedFR.NPFName);
				var retNPFSPN = BLServiceSystem.Client.GetSPNReturnedFinregisters(SelectedPortfolio.ID).Where(x => x.NPFName == SelectedFR.NPFName);
				if (allNPFSPN.Sum(x => x.Count) - retNPFSPN.Sum(x => x.Count) - SelectedFR.Count < 0)
				{
                    DialogHelper.ShowAlert("Нельзя удалить выбранный НПФ, так как сумма изъятия превысит сумму размещения по текущему портфелю для выбранного НПФ");
                    return;
				}

				if (allNPFSPN.Sum(x => x.ZLCount) - retNPFSPN.Sum(x => x.ZLCount) - SelectedFR.ZLCount < 0)
				{
                    DialogHelper.ShowAlert("Нельзя удалить выбранный НПФ, так как количество ЗЛ изъятия превысит количество ЗЛ размещения по текущему портфелю для выбранного НПФ");
					return;
                }

				base.ExecuteDeleteFR();
			}
		}

		protected override bool BeforeExecuteSaveCheck()
		{
			if (register.ID > 0)
			{
				var dbPF = DataContainerFacade.GetObjectProperty<Register, long, long?>(register.ID, "PortfolioID");
                if (!dbPF.HasValue) return true;
                if (register.PortfolioID != dbPF)
				{// проверка остатка размещения для портфеля, идентификатор которого заменяется
					var sumErr = ValidateAllocationSum((long)dbPF);
					if (!string.IsNullOrEmpty(sumErr))
					{
                        DialogHelper.ShowAlert(sumErr, "изменить портфель размещения");
                        return false;
					}
				}
			}

			return base.BeforeExecuteSaveCheck();
		}

		public override bool BeforeExecuteDeleteCheck()
		{
		    if (register.ID <= 0) return base.BeforeExecuteDeleteCheck();
		    var dbPF = DataContainerFacade.GetObjectProperty<Register, long, long?>(register.ID, "PortfolioID");
		    if (!dbPF.HasValue) return true;
		    var sumErr = ValidateAllocationSum((long)dbPF);
		    if (!string.IsNullOrEmpty(sumErr))
		    {
                DialogHelper.ShowAlert(sumErr, "удалить реестр размещения");
                return false;
		    }

		    return base.BeforeExecuteDeleteCheck();
		}

		private string ValidateAllocationSum(long dbPF)
		{
			if (register.ID == 0) return null;

			var frList = BLServiceSystem.Client.GetSPNAllocatedFinregisters(dbPF).Where(x => x.RegisterID != register.ID).GroupBy(x => x.NPFName);
			var frRetList = BLServiceSystem.Client.GetSPNReturnedFinregisters(dbPF).Where(x => x.RegisterID != register.ID).GroupBy(x => x.NPFName);

			if (finregWithNPFList != null)
			{
				foreach (var finReg in finregWithNPFList)
				{
					var ret = frRetList.FirstOrDefault(x => x.Key == finReg.NPFName);
					if (ret != null)
					{
						var allc = frList.FirstOrDefault(x => x.Key == finReg.NPFName);
						if (allc == null)
						{
							return $"Нельзя {{0}}, так как по НПФ \"{ret.Key}\" выбранного портфеля были выполнены изъятия";
						}

						if (allc.Sum(x => x.Count) < ret.Sum(x => x.Count))
						{
							return $"Нельзя {{0}}, сумма изъятия НПФ \"{ret.Key}\" будет превышать сумму размещения";
						}

						if (allc.Sum(x => x.ZLCount) < ret.Sum(x => x.ZLCount))
						{
							return $"Нельзя {{0}}, количество ЗЛ изъятия для НПФ \"{ret.Key}\" будет превышать количество ЗЛ в размещении";
						}
					}
				}
			}

			return null;
		}

	    public override bool CanExecuteSave()
	    {
            //хак для непонятного бага, когда IsDataChanged false при создании
	        if (State == ViewModelState.Create && !IsDataChanged)
	            IsDataChanged = true;
            return ValidateFields("selectedportfolio") && IsDataChanged;
	    }

	    public override string this[string columnName]
		{
			get
			{
				switch (columnName.ToLower())
				{
					case "selectedportfolio": return SelectedPortfolio.ValidateRequired();
					default: return base[columnName];
				}
			}
		}
	}
}
