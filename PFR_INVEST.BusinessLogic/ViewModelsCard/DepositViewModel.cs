﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OFPR_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_manager)]
    public class DepositViewModel : ViewModelCard, IRefreshableCardViewModel
    {
        public long DepositID;

        public DepositInfo Deposit { get; set; }

        public ICommand DialogOKResult { get; protected set; }

        public override string GetDocumentHeader()
        {
            return "Депозит";
        }

        public bool isReadOnly => true;

        public bool isEnabled => !isReadOnly;

        private void InitCommands()
        {
            DialogOKResult = new DelegateCommand(p => CanExecuteSave(), p => { });
        }

        public override bool CanExecuteDelete()
        {
            return false;
        }

        protected override void ExecuteDelete(int delType)
        {
            DataContainerFacade.Delete<Deposit>(Deposit.DepositID);

            base.ExecuteDelete(DocOperation.Delete);
        }

        public override bool CanExecuteSave()
        {
            return false;
        }

        protected override void ExecuteSave()
        {
        }

        public override string this[string columnName]
        {
            get
            {
                //string error = "Неверное значение";
                //string errToBig = "Значение слишком большое";
                switch (columnName)
                {
                    default: return string.Empty;
                }
            }
        }

        private static string fields = "PortfolioID|.DepositYield|AccNum|RegNun|LocationVolume|LocationDate|Status|ContragentID|TermLocation";

        public string Validate()
        {
            foreach (string key in fields.Split('|'))
            {
                var res = this[key];
                if (!string.IsNullOrEmpty(res))
                    return res;
            }

            return string.Empty;
        }

        public DepositViewModel(long id, ViewModelState action)
            : base(new List<Type> { typeof(DepositsListViewModel), typeof(DepositsArchiveListViewModel) })
        {

            DataObjectTypeForJournal = typeof(Deposit);
            State = action;

            if (action == ViewModelState.Create)
            {
                throw new NotSupportedException();
            }
            DepositID = id;
            LoadData();


            ID = Deposit.DepositID;
            InitCommands();
        }

        private void LoadData()
        {
            Deposit = BLServiceSystem.Client.GetDepositInfoHib(DepositID);

            if (Deposit == null) // for fast deposit input
                Deposit = new DepositInfo();

            OnPropertyChanged("Deposit");
            OnPropertyChanged("Status");
            OnPropertyChanged("StatusText");

            OnPropertyChanged("CommentVisibility");
            OnPropertyChanged("ReturnVisibility");
            OnPropertyChanged("PercentVisibility");
            OnPropertyChanged("IsColActualDateVisibe");
        }

        public DateTime ReturnDate => Deposit.Dc2_ReturnDate;

        public long Status => Deposit.Status ?? -1;

        public string StatusText => Deposit.StatusText;

        public Visibility CommentVisibility => Deposit.StatusValue == DepositInfo.enStatus.PartiallyReturned ? Visibility.Visible : Visibility.Collapsed;

        public Visibility ReturnVisibility => Deposit.StatusValue != DepositInfo.enStatus.Placed ? Visibility.Visible : Visibility.Collapsed;

        public Visibility PercentVisibility => Deposit.StatusValue != DepositInfo.enStatus.Placed ? Visibility.Visible : Visibility.Collapsed;

        public bool IsColActualDateVisibe => Deposit.StatusValue != DepositInfo.enStatus.Placed;


        public long? CardID => DepositID;

        public void RefreshModel()
        {
            LoadData();
        }
    }
}
