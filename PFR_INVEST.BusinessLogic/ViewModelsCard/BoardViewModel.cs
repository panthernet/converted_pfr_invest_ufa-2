﻿using System;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsCard
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_directory_editor, DOKIP_ROLE_TYPE.OARRS_directory_editor)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OARRS_manager)]
    public class BoardViewModel : ViewModelCardDialog, IRefreshableCardViewModel
    {
        private Board _board;

        public string BoardId
        {
            get { return _board.BoardId; }
            set
            {
                if (value == _board.BoardId)
                    return;
                _board.BoardId = value;
                OnPropertyChanged(nameof(BoardId));
            }
        }
        public string BoardName
        {
            get { return _board.BoardName; }
            set
            {
                if (value == _board.BoardName) return;
                _board.BoardName = value;
                OnPropertyChanged(nameof(BoardName));
            }
        }

        public BoardViewModel(long boardId, ViewModelState state)
            : base(typeof(AgencyRatingsListViewModel))
        {
            State = state;
            RefreshConnectedCardsViewModels = RefreshConnectedCards;
            DataObjectTypeForJournal = typeof(Board);
            if (boardId > 0)
            {
                var r = DataContainerFacade.GetByID<Board>(boardId);
                if (r == null)
                {
                    throw new Exception("Не удалось получить режим торгов с идентификатором " + boardId);
                }
                _board = r;
                CardID = ID = boardId;
            }
            else
            {
                _board = new Board();
            }
        }
        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "BoardId": return BoardId.ValidateNonEmpty() ?? BoardId.ValidateMaxLength(32);
                    case "BoardName": return BoardName.ValidateNonEmpty() ?? BoardName.ValidateMaxLength(64);
                    default:
                        return base[columnName];
                }

            }

        }

        protected void RefreshConnectedCards()
        {
            if (GetViewModel == null) return;
            var rVM = GetViewModel(typeof(BoardListViewModel)) as BoardListViewModel;
            if (rVM == null) return;
            rVM.RefreshList.Execute(null);
            rVM.Current = rVM.List.FirstOrDefault(d => d.ID == _board.ID);

        }

        protected override void ExecuteSave()
        {
            if (CanExecuteSave())
            {
                switch (State)
                {
                    case ViewModelState.Read:
                        break;
                    case ViewModelState.Edit:
                    case ViewModelState.Create:
                      
                        ID = _board.ID = DataContainerFacade.Save<Board, long>(_board);
                        CardID = ID;
                        break;
                }
            }
            IsDataChanged = false;
        }
        protected override bool BeforeExecuteSaveCheck()
        {
            //Проверка уникальности режима торгов
            if (!isUniqueTradeMode())
            {
                DialogHelper.ShowError($"Код режима торгов \"{BoardId}\" уже содержится в базе данных, сохранение не может быть выполнено","Ошибка");
                return false;
            }
            return base.BeforeExecuteSaveCheck();
        }

        bool isUniqueTradeMode()
        {
            //var b = DataContainerFacade.GetListByProperty<Board>("BoardId", BoardId);
            //return b.Any(x => x.ID != _board.ID);
            var b = DataContainerFacade.GetList<Board>();
            BoardId = BoardId.TrimStart(' ').TrimEnd(' ');
            return !b.Any(x => x.BoardId.Equals(BoardId, StringComparison.CurrentCultureIgnoreCase) && x.ID != _board.ID);
        }

        public override bool CanExecuteSave()
        {
            return ValidateFields() && IsDataChanged;
        }

        public void RefreshModel()
        {
            if (State != ViewModelState.Create)
            {
                _board = DataContainerFacade.GetByID<Board>(CardID);
                OnPropertyChanged(nameof(BoardId));
                OnPropertyChanged(nameof(BoardName));
            }

        }

        public long? CardID { get; private set; }


    }
}
