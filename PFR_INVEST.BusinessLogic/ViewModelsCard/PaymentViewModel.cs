﻿using System;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataAccess.Client.ObjectsExtensions;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_manager)]
    public class PaymentViewModel : ViewModelCard
    {
        private bool _isCreateMode = true;
        public bool IsCreateMode => _isCreateMode;

        private Payment _Payment;
        public Payment Payment
        {
            get { return _Payment; }
            set
            {
                _Payment = value;
                OnPropertyChanged("Payment");
            }
        }

        public string CouponPeriod
        {
            get {
                if (Payment != null && Payment.CouponPeriod.HasValue)
                    return Math.Round(Payment.CouponPeriod.Value, 0).ToString();
                return string.Empty;
            }
            set
            {
                if (Payment != null)
                {
                    decimal v;
                    if (decimal.TryParse(value, out v))
                        Payment.CouponPeriod = v;
                    else
                        Payment.CouponPeriod = null;
                }
            }
        }

        private Security m_CB;
        public Security CB
        {
            get { return m_CB; }
            set
            {
                if (m_CB != value)
                {
                    m_CB = value;
                    OnPropertyChanged("CB");
                }
            }
        }

        private string m_ErrorMessage;
        public string ErrorMessage
        {
            get { return m_ErrorMessage; }
            set
            {
                if (m_ErrorMessage != value)
                {
                    m_ErrorMessage = value;
                    OnPropertyChanged("ErrorMessage");
                }
            }
        }

        private bool IsCouponSizeEntered { get; set; }


        public decimal? CouponSize
        {
            get { return Payment.CouponSize; }
            set
            {
                if (Payment.CouponSize != value)
                {
                    Payment.CouponSize = value;
                    IsCouponSizeEntered = true;
                    OnPropertyChanged("CouponSize");
                }
            }
        }


        public PaymentViewModel(long id, ViewModelState action)
        {
            DataObjectTypeForJournal = typeof(Payment);
            if (action == ViewModelState.Create)
            {
                CB = DataContainerFacade.GetByID<Security, long>(id);
                Payment = new Payment();
                Payment.SecurityId = id;
               
                InitCreate();
            }
            else
            {
                Payment = DataContainerFacade.GetByID<Payment, long>(id);                
                CB = Payment.GetSecurity();

                _isCreateMode = false;
                OnPropertyChanged("IsCreateMode");
            }
            ID = Payment.ID;
            State = action;
            Register();
        }

        public PaymentViewModel(Security parent, ViewModelState action)
        {
            DataObjectTypeForJournal = typeof(Payment);
            CB = parent;
            if (action == ViewModelState.Create)
            {
                Payment = new Payment() ;
                if (parent != null)
                {
                    Payment.Nominal = parent.NomValue;
                    Payment.SecurityName = parent.Name;
                    Payment.SecurityId = parent.ID;
                }
                InitCreate();
            }
            else
            {
                Payment = DataContainerFacade.GetByID<Payment, long>(InternalID);
            }
            ID = Payment.ID;
            Register();            
        }

        private void InitCreate()
        {
            var payments = CB.GetPayments();
            if (payments.Count == 0) Payment.PaymentSize = 1.ToString();
            else
            {
                var ps =(payments.Max(p => Convert.ToInt32(p.PaymentSize)) + 1);

                Payment.PaymentSize = ps >= 999999999 ? 0.ToString() : ps.ToString();
            }
                 
            Payment.NotRepaymentNom = Payment.Nominal;
        }

        private void Register()
        {
            Payment.PropertyChanged += (sender, e) => { IsDataChanged = true; RecalcFields(e.PropertyName); };
            Payment.OnValidate += Payment_Validate;

            IsCouponSizeEntered = Payment.CouponSize.HasValue;
            RecalcFields(null);
        }

        private void Payment_Validate(object sender, BaseDataObject.ValidateEventArgs e)
        {
            Payment p = (sender as Payment);
            if (p != null)
            {
                string error = "Не заполнены все обязательные поля!";
                string error2 = "Значение должно быть в диапазоне от 0 до 100 включительно!";
                string error3 = "Значение должно быть больше 0!";
                string error4 = "Превышено допустимое значение!";
                string error5 = "Не валидно заполненное поле!";


                switch (e.PropertyName)
                {
                    case "Nominal":
                        if (!p.Nominal.HasValue)
                                e.Error = error;
                        else if (p.Nominal <= 0)
                            e.Error = error3;
                        break;
                    case "NotRepaymentNom":
                        if (!p.NotRepaymentNom.HasValue)
                            e.Error = error;
                        else if (p.NotRepaymentNom <= 0)
                                e.Error = error3;
                        break;
                    case "Coupon":
                        {
                            if (!p.Coupon.HasValue)
                            {
                                e.Error = error;
                            }
                            else
                            {
                                if (p.Coupon.Value < 0 || p.Coupon.Value > 100)
                                {
                                    e.Error = error2;
                                }
                            }
                        }
                        break;
                    case "RepaymentDebt":
                        {
                            if (!p.RepaymentDebt.HasValue)
                            {
                                e.Error = error;
                            }
                            else
                            {
                                if (p.RepaymentDebt.Value < 0 || p.RepaymentDebt.Value > 100)
                                {
                                    e.Error = error2;
                                }
                            }
                        }
                        break;
                    case "PaymentDate": e.Error = p.PaymentDate.HasValue ? null : error;
                        break;
                    case "CouponPeriod": 
                        if (!p.CouponPeriod.HasValue) e.Error = error;
                        else if (p.CouponPeriod == 0) e.Error = error3;
                        break;
                    case "CouponSize":
                        if (!p.CouponSize.HasValue)
                            e.Error = error;
                        else if (p.CouponSize <= 0)
                            e.Error = error3;
                        break;
                        case "PaymentBond":
                        e.Error = p.PaymentBond >= (decimal)Math.Pow(10, 15) ? error4 : null  ;
                        break;
                    case "PaymentSize":
                        int result;
                        e.Error = int.TryParse(p.PaymentSize, out result) ? result > 0 ? null : error5: error5;
                        break;
                    default:
                        e.Error = null;
                        break;
                }
            }
        }

        protected override void ExecuteSave()
        {
            if (CanExecuteSave())
            {
                switch (State)
                {
                    case ViewModelState.Read:
                        break;
                    case ViewModelState.Edit:
                    case ViewModelState.Create:
                        Payment.ID = DataContainerFacade.Save<Payment, long>(Payment);
                        break;
                    default:
                        break;
                }
            }
        }

        public override bool CanExecuteSave() { return EditAccessAllowed && String.IsNullOrEmpty(Validate()); }

        private const string notNullFields = "Nominal|Coupon|CouponPeriod|CouponSize|PaymentDate|RepaymentDebt|PaymentBond|PaymentSize"; //RepaymentNom

        public override string this[string columnName] => Payment[columnName];

        private string Validate()
        {
            foreach (string key in notNullFields.Split('|'))
            {
                BaseDataObject.ValidateEventArgs e = new BaseDataObject.ValidateEventArgs(key);
                Payment_Validate(Payment, e);
                if (!string.IsNullOrEmpty(e.Error))
                {
                    return "Не заполнены все обязательные поля!";
                }
            }
            return string.Empty;
        }

        public void RecalcFields(string fieldName)
        {
            try
            {
                ErrorMessage = string.Empty;
                if (!IsCouponSizeEntered)
                {
                    if (Payment.NotRepaymentNom.HasValue
                        && Payment.Nominal.HasValue
                        && Payment.CouponPeriod.HasValue
                        && Payment.Coupon.HasValue)
                    {
                        if (CurrencyIdentifier.IsRUB(CB.CurrencyName))//Round(((NotRepaymentNom*Coupon / 100) * CouponPeriod)/365),2)
                            Payment.CouponSize = Math.Round((Payment.NotRepaymentNom ?? 0) * (Payment.Coupon ?? 0) * (Payment.CouponPeriod ?? 0M) / 365 / 100, 2);
                        else//Nominal * Coupon / 100 * CouponPeriod/360}
                            Payment.CouponSize = Math.Round((Payment.Nominal ?? 0M) * (Payment.Coupon ?? 0M) * (Payment.CouponPeriod ?? 0M) / 360 / 100, 6);
                    }
                    else
                    {
                        Payment.CouponSize = 0;
                        ErrorMessage = "Не заполнены поля для расчета размера купона";
                    }
                    OnPropertyChanged("CouponSize");
                }
                Payment.PaymentBond = (Payment.CouponSize ?? 0) + (Payment.RepaymentNom ?? 0);
            }
            catch { }
        }

    }
}
