﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_directory_editor)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_manager)]
    public sealed class DepClaimSelectParamsViewModel : ViewModelCard,
                                                        IDepClaimAuctionExportProvider,
                                                        IDepClaimOfferProvider,
                                                        IDepClaimMaxExportProvider,
                                                        IDepClaimMaxImportProvider,
                                                        IDepClaimOfferCreateProvider,
                                                        IDepClaim2ImportProvider,
                                                        IDepClaim2ConfirmImportProvider,
                                                        IDepClaim2ExportProvider,
                                                        IDepClaim2ConfirmExportProvider,
                                                        IDepClaimAuctionSourceProvider,
                                                        IDepClaimStockProvider,
                                                        IRefreshableViewModel
    {
        private DateTime _oldSelectDate = DateTime.MinValue;
		private int _oldLocalNum = 1;
        private long _oldStockID;
        private int _previousSelectYear;		
        private int _previousAuctionNum;
        private static readonly decimal MoneyRestrict = new decimal(9999999999999999.99);
        private bool _hasDepClaims;
        private bool _hasDepClaimMax;

        #region Properties


        public new bool IsReadOnly => DepClaimSelectParams.AuctionStatus != DepClaimSelectParams.Statuses.New || EditAccessDenied;

        private bool _exportButtonEnabled;
        public bool ExportButtonEnabled
        {
            get { return _exportButtonEnabled; }
            set
            {
                _exportButtonEnabled = value;
                bool oldIsDataChanged = IsDataChanged;
                OnPropertyChanged("ExportButtonEnabled");
                if (!oldIsDataChanged)
                    IsDataChanged = false;

            }
        }

        private DepClaimSelectParams _depClaimSelectParams;
        public DepClaimSelectParams DepClaimSelectParams
        {
            get { return _depClaimSelectParams; }
            set
            {
                _depClaimSelectParams = value;
                OnPropertyChanged("DepClaimSelectParams");
            }
        }

        public int AuctionNum
        {
            get { return _depClaimSelectParams.AuctionNum; }
            set
            {
                if (_depClaimSelectParams.AuctionNum != value)
                {
                    _depClaimSelectParams.AuctionNum = value;
                    OnPropertyChanged("AuctionNum");
                }
            }
        }

		public int LocalNum
		{
			get { return _depClaimSelectParams.LocalNum; }
			set
			{
				if (_depClaimSelectParams.LocalNum != value)
				{
					_depClaimSelectParams.LocalNum = value;
					OnPropertyChanged("LocalNum");
				}
			}
		}

        public string AuctionStatusText => DepClaimSelectParams.AuctionStatusText;

        public int MaxDepClaimAmount
        {
            get { return _depClaimSelectParams.MaxDepClaimAmount; }
            set
            {
                if (_depClaimSelectParams.MaxDepClaimAmount != value)
                {
                    _depClaimSelectParams.MaxDepClaimAmount = value;
                    OnPropertyChanged("MaxDepClaimAmount");
                }
            }
        }

        public int Period
        {
            get { return _depClaimSelectParams.Period; }
            set
            {
                if (_depClaimSelectParams.Period != value)
                {
                    _depClaimSelectParams.Period = value;
                    OnPropertyChanged("Period");
                }
            }
        }

        public decimal MaxInsuranceFee
        {
            get { return _depClaimSelectParams.MaxInsuranceFee; }
            set
            {
                if (_depClaimSelectParams.MaxInsuranceFee != value)
                {
                    _depClaimSelectParams.MaxInsuranceFee = value;
                    OnPropertyChanged("MaxInsuranceFee");
                }
            }
        }

        public decimal MinDepClaimVolume
        {
            get { return _depClaimSelectParams.MinDepClaimVolume; }
            set
            {
                if (_depClaimSelectParams.MinDepClaimVolume != value)
                {
                    _depClaimSelectParams.MinDepClaimVolume = value;
                    OnPropertyChanged("MinDepClaimVolume");
                }
            }
        }

        public decimal MinRate
        {
            get { return _depClaimSelectParams.MinRate; }
            set
            {
                if (_depClaimSelectParams.MinRate != value)
                {
                    _depClaimSelectParams.MinRate = value;
                    OnPropertyChanged("MinRate");
                }
            }
        }

        public DateTime PlacementDate
        {
            get { return _depClaimSelectParams.PlacementDate; }
            set
            {
                if (_depClaimSelectParams.PlacementDate != value)
                {
                    _depClaimSelectParams.PlacementDate = value;
                    OnPropertyChanged("PlacementDate");
                }
            }
        }

        public DateTime? ReturnDate
        {
            get { return _depClaimSelectParams.ReturnDate == DateTime.MinValue ? (DateTime?)null : _depClaimSelectParams.ReturnDate; }
            set
            {
                if (_depClaimSelectParams.ReturnDate != value)
                {
                    _depClaimSelectParams.ReturnDate = value ?? DateTime.MinValue;
                    OnPropertyChanged("ReturnDate");
                }
            }
        }

        public DateTime SelectDate
        {
            get { return _depClaimSelectParams.SelectDate; }
            set
            {
                if (_depClaimSelectParams.SelectDate != value)
                {
                    _depClaimSelectParams.SelectDate = value;

                    OnPropertyChanged("SelectDate");
					UpdateLocalNum();
                }
            }
        }

        public TimeSpan DepClaimAcceptEnd
        {
            get { return _depClaimSelectParams.DepClaimAcceptEnd; }
            set
            {
                if (_depClaimSelectParams.DepClaimAcceptEnd != value)
                {
                    _depClaimSelectParams.DepClaimAcceptEnd = value;
                    UpdatePeriodValidation();
                }
            }
        }

        public TimeSpan DepClaimAcceptStart
        {
            get { return _depClaimSelectParams.DepClaimAcceptStart; }
            set
            {
                if (_depClaimSelectParams.DepClaimAcceptStart != value)
                {
                    _depClaimSelectParams.DepClaimAcceptStart = value;
                    UpdatePeriodValidation();
                }
            }
        }

        public TimeSpan DepClaimListEnd
        {
            get { return _depClaimSelectParams.DepClaimListEnd; }
            set
            {
                if (_depClaimSelectParams.DepClaimListEnd != value)
                {
                    _depClaimSelectParams.DepClaimListEnd = value;
                    //OnPropertyChanged("DepClaimListStart");
                    //OnPropertyChanged("DepClaimListEnd");
                    UpdatePeriodValidation();
                }
            }
        }

        public TimeSpan DepClaimListStart
        {
            get { return _depClaimSelectParams.DepClaimListStart; }
            set
            {
                if (_depClaimSelectParams.DepClaimListStart != value)
                {
                    _depClaimSelectParams.DepClaimListStart = value;
                    //OnPropertyChanged("DepClaimListStart");
                    //OnPropertyChanged("DepClaimListEnd");
                    UpdatePeriodValidation();
                }
            }
        }

        public TimeSpan FilterEnd
        {
            get { return _depClaimSelectParams.FilterEnd; }
            set
            {
                if (_depClaimSelectParams.FilterEnd != value)
                {
                    _depClaimSelectParams.FilterEnd = value;
                    //OnPropertyChanged("FilterStart");
                    //OnPropertyChanged("FilterEnd");
                    UpdatePeriodValidation();
                }
            }
        }

        public TimeSpan FilterStart
        {
            get { return _depClaimSelectParams.FilterStart; }
            set
            {
                if (_depClaimSelectParams.FilterStart != value)
                {
                    _depClaimSelectParams.FilterStart = value;
                    //OnPropertyChanged("FilterStart");
                    //OnPropertyChanged("FilterEnd");
                    UpdatePeriodValidation();
                }
            }
        }

        public string DepClaimSelectLocation
        {
            get { return _depClaimSelectParams.DepClaimSelectLocation; }
            set
            {
                if (_depClaimSelectParams.DepClaimSelectLocation != value)
                {
                    _depClaimSelectParams.DepClaimSelectLocation = value;
                    OnPropertyChanged("DepClaimSelectLocation");
                }
            }
        }

        public TimeSpan OfferReceiveEnd
        {
            get { return _depClaimSelectParams.OfferReceiveEnd; }
            set
            {
                if (_depClaimSelectParams.OfferReceiveEnd != value)
                {
                    _depClaimSelectParams.OfferReceiveEnd = value;
                    //OnPropertyChanged("OfferReceiveStart");
                    //OnPropertyChanged("OfferReceiveEnd");
                    UpdatePeriodValidation();
                }
            }
        }

        public TimeSpan OfferReceiveStart
        {
            get { return _depClaimSelectParams.OfferReceiveStart; }
            set
            {
                if (_depClaimSelectParams.OfferReceiveStart != value)
                {
                    _depClaimSelectParams.OfferReceiveStart = value;
                    //OnPropertyChanged("OfferReceiveStart");
                    //OnPropertyChanged("OfferReceiveEnd");
                    UpdatePeriodValidation();
                }
            }
        }

        public TimeSpan OfferSendEnd
        {
            get { return _depClaimSelectParams.OfferSendEnd; }
            set
            {
                if (_depClaimSelectParams.OfferSendEnd != value)
                {
                    _depClaimSelectParams.OfferSendEnd = value;
                    //OnPropertyChanged("OfferSendEnd");
                    //OnPropertyChanged("OfferSendStart");
                    UpdatePeriodValidation();
                }
            }
        }

        public TimeSpan OfferSendStart
        {
            get { return _depClaimSelectParams.OfferSendStart; }
            set
            {
                if (_depClaimSelectParams.OfferSendStart != value)
                {
                    _depClaimSelectParams.OfferSendStart = value;
                    //OnPropertyChanged("OfferSendStart");
                    //OnPropertyChanged("OfferSendEnd");
                    UpdatePeriodValidation();
                }
            }
        }

		#region Stock
		List<Stock> _mStocks;
		public List<Stock> Stocks
		{
			get
			{
				return _mStocks;
			}
			set
			{
				_mStocks = value;
				OnPropertyChanged("Stocks");
			}
		}

		private Stock _mSelectedStock;
		public Stock SelectedStock
		{
			get
			{
				return _mSelectedStock;
			}
			set
			{
				if (_mSelectedStock != value)
				{
					_mSelectedStock = value;
					if (value != null)
					{
						_depClaimSelectParams.StockId = _mSelectedStock.ID;
						DepClaimSelectLocation = _mSelectedStock.Name;
                        if(!_isLoading)
                            LoadDepClaimTimes();
					}
					OnPropertyChanged("SelectedStock");
				}
			}
		}
		#endregion

        public bool IsNotNewStatus => _depClaimSelectParams.AuctionStatus != DepClaimSelectParams.Statuses.New;

        public string SelectedStockName => _depClaimSelectParams.StockName;
        #endregion

        #region Constructors



        public DepClaimSelectParamsViewModel(long id, ViewModelState action)
            : base(typeof(DepClaimSelectParamsListViewModel))
        {
            DataObjectTypeForJournal = typeof(DepClaimSelectParams);
            ID = id;
            State = action;
			Stocks = DataContainerFacade.GetList<Stock>();
            if (id > 0)
            {
                Load();
                if (!string.IsNullOrEmpty(_depClaimSelectParams.StockName) && _depClaimSelectParams.StockName != SelectedStock?.Name)
                    IsDataChanged = true;
            }
            else
            {
                _depClaimSelectParams = new DepClaimSelectParams();
                SelectDate = DateTime.Now;
                PlacementDate = DateTime.Now;
                ReturnDate = DateTime.Now;

				// Получаем название Московской биржи из БД
                SelectedStock = Stocks.FirstOrDefault(x => x.ID == (int) Stock.StockID.MoscowStock);
	            if (SelectedStock != null)
					DepClaimSelectLocation = SelectedStock.Name;
                
                LoadDepClaimTimes();
                AuctionNum = BLServiceSystem.Client.GetMaxAuctionNumDepClaimSelectParams(SelectDate.Year) + 1;
                Period = 0;
                _activeAuctionOfferts = new List<DepClaimOfferListItem>();
                RegisterAuctionEvents();
                _previousAuctionNum = BLServiceSystem.Client.GetMaxAuctionNumDepClaimSelectParams(SelectDate.Year) + 1;
                _previousSelectYear = SelectDate.Year;
				UpdateLocalNum();
            }
            OnPropertyChanged(nameof(SelectedStockName));
        }

        private void LoadDepClaimTimes()
        {
            var lastDepClaim = BLServiceSystem.Client.SelectLastDepClaimSelectParams(SelectedStock.ID);
            if (lastDepClaim == null)
            {
                DepClaimAcceptStart = new TimeSpan(0, 10, 30, 0);
                DepClaimAcceptEnd = new TimeSpan(0, 11, 00, 0);
                DepClaimListStart = DepClaimAcceptEnd;
                DepClaimListEnd = new TimeSpan(0, 11, 30, 0);
                FilterStart = DepClaimListEnd;
                FilterEnd = new TimeSpan(0, 12, 00, 0);
                OfferSendStart = FilterEnd;
                OfferSendEnd = new TimeSpan(0, 13, 30, 0);
                OfferReceiveStart = new TimeSpan(0, 12, 00, 0);
                OfferReceiveEnd = new TimeSpan(0, 14, 30, 0);
            }
            else
            {
                DepClaimAcceptStart = lastDepClaim.DepClaimAcceptStart;
                DepClaimAcceptEnd = lastDepClaim.DepClaimAcceptEnd;
                DepClaimListStart = lastDepClaim.DepClaimListStart;
                DepClaimListEnd = lastDepClaim.DepClaimListEnd;
                FilterStart = lastDepClaim.FilterStart;
                FilterEnd = lastDepClaim.FilterEnd;
                OfferSendStart = lastDepClaim.OfferSendStart;
                OfferSendEnd = lastDepClaim.OfferSendEnd;
                OfferReceiveStart = lastDepClaim.OfferReceiveStart;
                OfferReceiveEnd = lastDepClaim.OfferReceiveEnd;
            }
        }

        private bool _isLoading;

        private void Load()
        {
			_isLoading = true;
            if (ID > 0)
			{
                _depClaimSelectParams = DataContainerFacade.GetByID<DepClaimSelectParams>(InternalID);

                _oldSelectDate = _depClaimSelectParams.SelectDate;
				_oldLocalNum = _depClaimSelectParams.LocalNum;
                _oldStockID = _depClaimSelectParams.StockId;
				_previousSelectYear = SelectDate.Year;

				_previousAuctionNum = AuctionNum;
				_activeAuctionOfferts = BLServiceSystem.Client.GetDepClaimOfferListByAuctionId(ID);
				var loc = _depClaimSelectParams.DepClaimSelectLocation;
				// срабатывает автоматическое выставление наименования биржи в поле проведения аукциона
				SelectedStock = Stocks.FirstOrDefault(x => x.ID == _depClaimSelectParams.StockId);
				_depClaimSelectParams.DepClaimSelectLocation = loc;
				CheckImportedEntities(InternalID);
			}

            IsDataChanged = false;			

            RegisterAuctionEvents();

            OnPropertyChanged("AuctionStatusText");
            OnPropertyChanged("ExportButtonEnabled");
            OnPropertyChanged("DepClaimSelectParams");
            OnPropertyChanged("AuctionNum");
            OnPropertyChanged("MaxDepClaimAmount");
            OnPropertyChanged("Period");
            OnPropertyChanged("MaxInsuranceFee");
            OnPropertyChanged("MinDepClaimVolume");
            OnPropertyChanged("MinRate");
            OnPropertyChanged("PlacementDate");
            OnPropertyChanged("ReturnDate");
            OnPropertyChanged("SelectDate");
            OnPropertyChanged("DepClaimAcceptEnd");
            OnPropertyChanged("DepClaimAcceptStart");
            OnPropertyChanged("DepClaimListEnd");
            OnPropertyChanged("DepClaimListStart");
            OnPropertyChanged("FilterEnd");
            OnPropertyChanged("FilterStart");
            OnPropertyChanged("DepClaimSelectLocation");
            OnPropertyChanged("OfferReceiveEnd");
            OnPropertyChanged("OfferReceiveStart");
            OnPropertyChanged("OfferSendEnd");
            OnPropertyChanged("OfferSendStart");
            OnPropertyChanged("IsReadOnly");
            _isLoading = false;
        }

        void RegisterAuctionEvents()
        {
            _depClaimSelectParams.PropertyChanged += (sender, e) => { IsDataChanged = true; };
            _depClaimSelectParams.PropertyChanged += DepClaimSelectParamsOnPropertyChanged;
        }

        private void DepClaimSelectParamsOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
			if (
				e.PropertyName == "Period" || 
				e.PropertyName == "PlacementDate")
			{
                try
                {
                    ReturnDate = PlacementDate.AddDays(Period);
                }
                catch (ArgumentOutOfRangeException)
                {
                    ReturnDate = null;
                }
			}

            if (e.PropertyName == "SelectDate")
                // если номер аукциона не был изменен вручную, то выбираем новый номер при смене года, иначе контроль номера аукциона на совести пользователя
                if (_previousSelectYear != SelectDate.Year && AuctionNum == _previousAuctionNum)
                {
                    AuctionNum = BLServiceSystem.Client.GetMaxAuctionNumDepClaimSelectParams(SelectDate.Year) + 1;
                    _previousSelectYear = SelectDate.Year;
                    _previousAuctionNum = AuctionNum;
                }
        }

        #endregion

        #region Execute implementation

        protected override void ExecuteSave()
        {
            if (!CanExecuteSave()) return;
            switch (State)
            {
                case ViewModelState.Read:
                    break;
                case ViewModelState.Edit:
                case ViewModelState.Create:					
					UpdateLocalNum();
                    ReturnDate = PlacementDate.AddDays(Period);
                    _depClaimSelectParams.StockName = _mSelectedStock.Name;        
                    ID = DepClaimSelectParams.ID = DataContainerFacade.Save(_depClaimSelectParams);
                    if (ID > 0)
                    {
                        State = ViewModelState.Edit;
                        _oldSelectDate = _depClaimSelectParams.SelectDate;
                        _oldStockID = _depClaimSelectParams.StockId;
                        IsDataChanged = false;
                        _previousAuctionNum = AuctionNum;
                        _previousSelectYear = SelectDate.Year;

                        CheckImportedEntities(ID);
                    }
                    break;
            }
        }

        protected override bool BeforeExecuteSaveCheck()
        {
            if (!base.BeforeExecuteSaveCheck()) return false;
            var existsAuction = DataContainerFacade.GetListByProperty<DepClaimSelectParams>("AuctionNum", AuctionNum).Where(x => x.SelectDate.Year == SelectDate.Year && x.ID != DepClaimSelectParams.ID).ToList();
            if (existsAuction.Any())
            {
                DialogHelper.ShowError(existsAuction.FirstOrDefault(a => a.StatusID > 0) == null
                    ? "№ Аукциона существует среди удаленных Уведомлений! Исправьте № Аукциона на уникальный и сохраните Уведомление заново."
                    : "№ Аукциона существует! Исправьте № Аукциона на уникальный и сохраните Уведомление заново.");
                return false;
            }

			//var isDateNotUnique = DataContainerFacade.GetListByProperty<DepClaimSelectParams>("SelectDate", SelectDate).Where(x => x.ID != Auction.ID).Any();
			//if (_SelectDateInUseLastResult = isDateNotUnique)
			//{
			//    DialogHelper.ShowError("Уведомление на данную дату проведения отбора заявок уже существует");
			//    OnPropertyChanged("SelectDate");
			//    return false;
			//}

            // http://jira.vs.it.ru/browse/PFRPTKIV-2?focusedCommentId=218819&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-218819
            // срок размещения должен быть уникален в пределах одного дня
            var isPeriodNotUnique = DataContainerFacade.GetListByPropertyConditions<DepClaimSelectParams>(new List<ListPropertyCondition>
            {
                new ListPropertyCondition{
                    Name = "SelectDate",
                    Operation = "eq",
                    Value = SelectDate
                },
                new ListPropertyCondition{
                    Name = "Period",
                    Operation = "eq",
                    Value = Period
                },
                new ListPropertyCondition{
                    Name = "ID",
                    Operation = "neq",
                    Value = _depClaimSelectParams.ID
                },
                new ListPropertyCondition{
                    Name = "StatusID",
                    Operation = "neq",
                    Value = -1L
                },
                new ListPropertyCondition{
                    Name = "StockId",
                    Operation = "eq",
                    Value = _depClaimSelectParams.StockId
                }
            });

            if (isPeriodNotUnique.Any())
            {
                DialogHelper.ShowError("Аукцион с датой проведения отбора заявок {0} уже есть со сроком размещения {1}. Необходимо изменить Срок размещения! ", SelectDate.ToShortDateString(), Period);
                return false;
            }

            return true;
        }

        public void RefreshAuction(long auctionID)
        {
            var auction = DataContainerFacade.GetByID<DepClaimSelectParams>(auctionID);
            _depClaimSelectParams.AuctionStatus = auction.AuctionStatus;
            CheckImportedEntities(auctionID);
            OnPropertyChanged("AuctionCommonStatus");
        }

        private void CheckImportedEntities(long auctionID)
        {
            _hasDepClaims = DataContainerFacade.GetListByPropertyConditions<DepClaim2>(
                new List<ListPropertyCondition>(2)
                {
                    new ListPropertyCondition
                        {
                            Name = "StatusID",
                            Value = -1,
                            Operation = "neq"
                        },
                    new ListPropertyCondition
                        {
                            Name = "AuctionID",
                            Value = auctionID
                        }
                }).Count > 0;


            _hasDepClaimMax = DataContainerFacade.GetListByPropertyConditions<DepClaimMaxListItem>(
                new List<ListPropertyCondition>(1)
                {
                    new ListPropertyCondition
                        {
                            Name = "DepclaimselectparamsId",
                            Value = auctionID
                        }
                }).Count > 0;

        }

        public override string this[string columnName]
        {
            get
            {
                string error = "Неверный диапазон времени";
                switch (columnName)
                {
                    case "DepClaimAcceptStart":
                        if (DepClaimAcceptStart > DepClaimAcceptEnd) return error;

                        if (DepClaimAcceptStart > DepClaimListStart) return error;
                        break;
                    case "DepClaimAcceptEnd":
                        if (DepClaimAcceptStart > DepClaimAcceptEnd) return error;
                        break;

                    case "DepClaimListStart":
                        if (DepClaimListStart > DepClaimListEnd) return error;

                        if (DepClaimListStart > FilterStart) return error;
                        if (DepClaimAcceptStart > DepClaimListStart) return error;
                        break;
                    case "DepClaimListEnd":
                        if (DepClaimListStart > DepClaimListEnd) return error;
                        break;


                    case "FilterStart":
                        if (FilterStart > FilterEnd) return error;

                        if (FilterStart > OfferSendStart) return error;
                        if (DepClaimListStart > FilterStart) return error;
                        break;
                    case "FilterEnd":
                        if (FilterStart > FilterEnd) return error;
                        break;


                    case "OfferSendStart":
                        if (OfferSendStart > OfferSendEnd) return error;

                        if (OfferSendStart > OfferReceiveStart) return error;
                        if (FilterStart > OfferSendStart) return error;
                        break;
                    case "OfferSendEnd":
                        if (OfferSendStart > OfferSendEnd) return error;
                        break;


                    case "OfferReceiveStart":
                        if (OfferReceiveStart > OfferReceiveEnd) return error;

                        if (OfferSendStart > OfferReceiveStart) return error;
                        break;
                    case "OfferReceiveEnd":
                        if (OfferReceiveStart > OfferReceiveEnd) return error;
                        break;


                    case "DepClaimSelectLocation":
                        if (string.IsNullOrEmpty(DepClaimSelectLocation)) return "Поле обязательное для заполнения";
                        break;
                    case "Period":
                        if (Period > 99999)
                            return "Недопустимо большое значение";
                        break;
                    case "MinDepClaimVolume":
                        if (MinDepClaimVolume > MoneyRestrict)
                            return "Недопустимо большое значение";
                        break;
                    case "MaxDepClaimAmount":
                        if (MaxDepClaimAmount > 99999999)
                            return "Недопустимо большое значение";
                        break;
                    case "MinRate":
                        if (MinRate > 100)
                            return "Значение  должно быть не больше 100 (%)";
                        break;
                    case "MaxInsuranceFee":
                        if (MaxInsuranceFee > MoneyRestrict)
                            return "Недопустимо большое значение";
                        break;

                    case "SelectDate":
						//if (State == ViewModelState.Create || _oldSelectDate != SelectDate)
						//    if (IsSelectDateInUse())
						//        return "Уведомление на данную дату проведения отбора заявок уже существует";
						return null;
					case "SelectedStock":
						if (SelectedStock == null)
							return "Поле обязательное для заполнения";
						break;
                    case "PlacementDate":
                        if (PlacementDate.Year < 1753)
                            return "Необходимо ввести корректное значение даты";
                        break;
                    case "ReturnDate":
                        if (ReturnDate == null || ReturnDate.Value.Year < 1753)
                            return "Необходимо ввести корректные значения даты и срока размещения";
                        break;
                }
                return null;
            }
        }

        public bool IsValid()
        {
            const string fields = "DepClaimAcceptStart|DepClaimAcceptEnd|DepClaimListStart|DepClaimListEnd|FilterStart|FilterEnd|OfferSendStart|OfferSendEnd|OfferReceiveStart|OfferReceiveEnd|DepClaimSelectLocation|Period|MaxDepClaimAmount|MinDepClaimVolume|MinRate|MaxInsuranceFee|SelectDate|SelectedStock|ReturnDate";
            return fields.Split('|').Select(key => this[key]).All(string.IsNullOrEmpty);
        }

        public override bool CanExecuteSave()
        {
            if (!(!IsReadOnly && (State == ViewModelState.Create || IsDataChanged || (!string.IsNullOrEmpty(_depClaimSelectParams.StockName) && _depClaimSelectParams.StockName != SelectedStock?.Name)) && !_hasDepClaims && !_hasDepClaimMax))
            {
                ExportButtonEnabled = false;
                return false;
            }

            bool isValid = ExportButtonEnabled = IsValid();
            return isValid;
        }

        public override bool CanExecuteDelete()
        {
            return _depClaimSelectParams.AuctionStatus == DepClaimSelectParams.Statuses.New && State != ViewModelState.Create;
            //Создал аукцион? сам виноват. удалять нельзя!
            //Но если очень хочется, то можно: http://jira.dob.datateh.ru/browse/DOKIPIV-460
        }

        protected override void ExecuteDelete(int delType)
        {
            DataContainerFacade.Delete(_depClaimSelectParams);
            base.ExecuteDelete(DocOperation.Archive);

        }
        #endregion

		private void UpdateLocalNum()
		{
		    switch (State)
		    {
		        case ViewModelState.Create:
		            //При создании всегда обновляем номер
		            LocalNum = BLServiceSystem.Client.GetAuctionNextLocalNum(SelectDate);
		            break;
		        case ViewModelState.Edit:
		            //При редактировании возвращаем старый номер, если дата стала старая
		            LocalNum = _oldSelectDate != SelectDate ? BLServiceSystem.Client.GetAuctionNextLocalNum(SelectDate) : _oldLocalNum;
		            break;
		    }
		}

        public void UpdatePeriodValidation()
        {
            OnPropertyChanged("DepClaimAcceptEnd");
            OnPropertyChanged("DepClaimAcceptStart");
            OnPropertyChanged("DepClaimListEnd");
            OnPropertyChanged("DepClaimListStart");
            OnPropertyChanged("FilterEnd");
            OnPropertyChanged("FilterStart");
            OnPropertyChanged("OfferReceiveEnd");
            OnPropertyChanged("OfferReceiveStart");
            OnPropertyChanged("OfferSendEnd");
            OnPropertyChanged("OfferSendStart");
        }

        private long? AuctionID => DepClaimSelectParams.ID > 0 ? (long?)DepClaimSelectParams.ID : null;

        void IRefreshableViewModel.RefreshModel()
        {
            Load();
        }

        private List<DepClaimOfferListItem> _activeAuctionOfferts;
        private List<DepClaimOfferListItem> ActiveAuctionOfferts => _activeAuctionOfferts ?? new List<DepClaimOfferListItem>();

        long? IDepClaimAuctionProvider.AuctionID => AuctionID;

        int? IDepClaimAuctionFullInfoProvider.DepClaimMaxCount => DepClaimSelectParams.DepClaimMaxCount;

        int? IDepClaimAuctionFullInfoProvider.DepClaimCommonCount => DepClaimSelectParams.DepClaimCommonCount;

        DepClaimSelectParams.Statuses? IDepClaimAuctionInfoProvider.AuctionStatus => DepClaimSelectParams.AuctionStatus;

        public bool CanGenerateOfferts
        {
            get
            {
                if (DepClaimSelectParams.AuctionStatus == DepClaimSelectParams.Statuses.DepClaimConfirmImported)
                    return true;

                return DepClaimSelectParams.AuctionStatus == DepClaimSelectParams.Statuses.OfferCreated && ActiveAuctionOfferts.All(item => item.Offer.Status == DepClaimOffer.Statuses.NotSigned);
            }
        }

        bool IDepClaimMaxExportProvider.IsMaxClaim => true;

        bool IDepClaimMaxExportProvider.IsCommonClaim => true;

        long? IDepClaimOfferProvider.OfferID => null;

        bool IDepClaimStockProvider.IsMoscowStockSelected => _oldStockID == (long)Stock.StockID.MoscowStock;

        bool IDepClaimStockProvider.IsSPVBStockSelected => _oldStockID == (long)Stock.StockID.SPVB;
    }
}
