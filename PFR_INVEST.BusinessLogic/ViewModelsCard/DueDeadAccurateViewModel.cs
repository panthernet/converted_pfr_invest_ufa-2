﻿using PFR_INVEST.Auth.SharedData.Auth;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OFPR_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
    public class DueDeadAccurateViewModel : ApsAccurateModelBase
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public DueDeadAccurateViewModel(DueDeadViewModel parentVM)
            : base(parentVM)
        {
            Dopaps.DocKind = 2; // потому что страховые взносы умерших
        }
    }
}
