﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OFPR_manager)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
    public sealed class ReturnViewModel : ViewModelCard, IUpdateRaisingModel
    {
        public Return Return { get; private set; }


        private readonly ICommand _selectAccount;
        public ICommand SelectAccount => _selectAccount;

        private PortfolioFullListItem _mAccount;
        public PortfolioFullListItem Account
        {
            get
            {
                return _mAccount;
            }
            set
            {
                _mAccount = value;
                if (value != null)
                {
                    Return.PFRBankAccountID = value.PfrBankAccount.ID;
                    Return.PortfolioID = value.Portfolio.ID;
                }
                OnPropertyChanged("AccountNumber");
                OnPropertyChanged("AccountPortfolio");
            }
        }

        public string AccountNumber => Account == null ? "" : Account.PfrBankAccount.AccountNumber;

        public string AccountPortfolio => Account == null ? "" : Account.Portfolio.Year;

        private List<Budget> _mBudgetsList;
        public List<Budget> BudgetsList
        {
            get
            {
                return _mBudgetsList;
            }
            set
            {
                _mBudgetsList = value;
                OnPropertyChanged("DirectionsList");
            }
        }

        private Budget _mSelectedBudget;
        public Budget SelectedBudget
        {
            get
            {
                return _mSelectedBudget;
            }
            set
            {
                _mSelectedBudget = value;
                if (value != null)
                {
                    Return.BudgetID = value.ID;
                    DirectionDate = value.Date;
                }
            }
        }

        private readonly List<string> _mRecipientsList = new List<string> { "ОПФР", "ИЦПУ", "Не определен" };
        public List<string> RecipientsList => _mRecipientsList;

        public string SelectedRecipient
        {
            get
            {
                return (Return.Recipient ?? "").Trim();
            }
            set
            {
                Return.Recipient = value;
                OnPropertyChanged("SelectedRecipient");
            }
        }

        public DateTime? Date
        {
            get
            {
                return Return.Date;
            }
            set
            {
                Return.Date = value;
                OnPropertyChanged("Date");
            }
        }

        public DateTime? DIDate
        {
            get
            {
                return Return.DIDate;
            }
            set
            {
                Return.DIDate = value;
                OnPropertyChanged("DIDate");
            }
        }

        private DateTime? _mDirectionDate;
        public DateTime? DirectionDate
        {
            get
            {
                return _mDirectionDate;
            }
            set
            {
                _mDirectionDate = value;
                OnPropertyChanged("DirectionDate");
            }
        }

        public string Number
        {
            get
            {
                return Return.Number;
            }
            set
            {
                Return.Number = value;
                OnPropertyChanged("Number");
            }
        }

        public decimal? Sum
        {
            get
            {
                return Return.Sum.HasValue ? Return.Sum : 0;
            }
            set
            {
                Return.Sum = value;
                OnPropertyChanged("Sum");
            }
        }

        public string Comment
        {
            get
            {
                return Return.Comment;
            }
            set
            {
                Return.Comment = value;
                OnPropertyChanged("Comment");
            }
        }

        private static bool CanExecuteSelectAccount()
        {
            return true;
        }

        private void ExecuteSelectAccount()
        {
            var filter = new PortfolioIdentifier.PortfolioPBAParams
            {
                CurrencyName = new[] {"рубли"}                
            };

            var selected = DialogHelper.SelectPortfolio(filter);

            if (selected != null)
            {
                Account = selected;
            }
        }

        public override bool CanExecuteDelete()
        {
            return State == ViewModelState.Edit;
        }

        protected override void ExecuteDelete(int delType = 1)
        {
            DataContainerFacade.Delete(Return);

            base.ExecuteDelete(DocOperation.Delete);
        }

        protected override void ExecuteSave()
        {
            if (!CanExecuteSave()) return;
            switch (State)
            {
                case ViewModelState.Read:
                    break;
                case ViewModelState.Edit:
                    DataContainerFacade.Save<Return, long>(Return);
                    break;
                case ViewModelState.Create:
                    ID = DataContainerFacade.Save<Return, long>(Return);
                    Return.ID = ID;
                    break;
            }
        }

        public override bool CanExecuteSave()
        {
            return Validate() && IsDataChanged;
        }

        private const string INVALID_DATE = "Не указана дата";
        private const string INVALID_DIDATE = "Не указана дата ДИ";
        private const string INVALID_NUMBER = "Неверно указан номер";
        private const string INVALID_SUM = "Неверно указана сумма";
        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "Date": return Date != null ? null : INVALID_DATE;
                    case "DIDate": return DIDate != null ? null : INVALID_DIDATE;
                    case "Number": return !string.IsNullOrEmpty(Number) ? Number.ValidateMaxLength(128) : INVALID_NUMBER;
                    case "Sum": return Sum.HasValue && Sum != 0 ? null : INVALID_SUM;
                    case "Comment": return Comment.ValidateMaxLength(512);
                    default: return null;
                }
            }
        }

        private bool Validate()
        {
            return ValidateFields() &&
                   Account != null;
        }

        public void LoadBudget(long bID)
        {
            SelectedBudget = BudgetsList.FirstOrDefault(b => b.ID == bID);
        }

        public void Load(long lID)
        {
            try
            {
                ID = lID;
                Return = DataContainerFacade.GetByID<Return, long>(lID);

                SelectedBudget = BudgetsList.FirstOrDefault(b=>b.ID == Return.BudgetID);

                if (Return.PFRBankAccountID.HasValue && Return.PortfolioID.HasValue)
                    Account = BLServiceSystem.Client.GetPortfolioFull(Return.PFRBankAccountID.Value, Return.PortfolioID.Value);
            }
            catch (Exception e)
            {
                if (DoNotThrowExceptionOnError == false)
                    throw;

                Logger.WriteException(e);
                throw new ViewModelInitializeException();
            }
        }

        public ReturnViewModel()
            : base(typeof(SchilsCostsListViewModel))
        {
            DataObjectTypeForJournal = typeof(Return);
            BudgetsList = DataContainerFacade.GetList<Budget>();

            _selectAccount = new DelegateCommand(o => CanExecuteSelectAccount(),
                o => ExecuteSelectAccount());

            Return = new Return();

            SelectedBudget = BudgetsList.FirstOrDefault();
            SelectedRecipient = RecipientsList.FirstOrDefault();

            DIDate = DateTime.Now;
            Date = DateTime.Now;

            RefreshConnectedCardsViewModels = RefreshParentViewModel;
        }

        private static void RefreshParentViewModel()
        {
            if (GetViewModel == null) return;
            var vm = GetViewModel(typeof(DirectionViewModel)) as DirectionViewModel;
            if (vm != null)
                vm.RefreshTransfers();
        }

        public IList<KeyValuePair<Type, long>> GetUpdatedList()
        {
            var list = new List<KeyValuePair<Type, long>> {new KeyValuePair<Type, long>(typeof (Return), Return.ID)};

            return list;
        }
    }
}
