﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_directory_editor, DOKIP_ROLE_TYPE.OKIP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_directory_editor)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager)]
    public class ReorganizationViewModel : ViewModelCard
    {
        protected readonly Reorganization _reorganization;
        protected  LegalEntity _le;

        [DisplayName("Номер приказа ФСФР")]
        [StringLength(50, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        [Required]
        public string RegNum
        {
            get {
                return _reorganization.RegNum?.Trim() ?? _reorganization.RegNum;
            }
            set
            {
                _reorganization.RegNum = value;
                OnPropertyChanged("RegNum");
            }
        }

        [DisplayName("Дата реорганизации")]
        [Required]
        public DateTime ReorganizationDate
        {
            get
            {
                return _reorganization.ReorganizationDate;
            }
            set
            {
                _reorganization.ReorganizationDate = value;
                OnPropertyChanged("ReorganizationDate");
            }
        }

        [DisplayName("Дата приказа ФСФР")]
        [Required]
        public DateTime? OrderDate
        {
            get
            {
                return _reorganization.OrderDate;
            }
            set
            {
                _reorganization.OrderDate = value;
                OnPropertyChanged("ReorganizationDate");
            }
        }

        private List<ReorganizationType> m_ReorganizationTypeList;
        public List<ReorganizationType> ReorganizationTypeList
        {
            get
            {
                return m_ReorganizationTypeList;
            }
            set
            {
                m_ReorganizationTypeList = value;
                OnPropertyChanged("ReorganizationTypeList");
            }
        }

        private ReorganizationType m_SelectedReorganizationType;
        public ReorganizationType SelectedReorganizationType
        {
            get
            {
                return m_SelectedReorganizationType;
            }
            set
            {
                if (value != null)
                {
                    m_SelectedReorganizationType = value;
                    _reorganization.ReorganizationTypeID = value.ID;
                    OnPropertyChanged("SelectedReorganizationType");
                }
            }
        }

        private IList<Contragent> m_ContragentList = new List<Contragent>();
        public IList<Contragent> ContragentList
        {
            get
            {
                return m_ContragentList;
            }
            set
            {
                m_ContragentList = value;
                OnPropertyChanged("ContragentList");
            }
        }

        private Contragent m_SelectedContragent;
        public Contragent SelectedContragent
        {
            get
            {
                return m_SelectedContragent;
            }
            set
            {
                if (value == null) return;
                m_SelectedContragent = value;
                _reorganization.ReceiverContragentID = value.ID;
                OnPropertyChanged("SelectedNPFListItem");
            }
        }
        
        protected override void ExecuteSave()
        {
            switch (State)
            {
                case ViewModelState.Read:
                    break;
                case ViewModelState.Edit:
                case ViewModelState.Create:
                    ID = DataContainerFacade.Save(_reorganization);

					////Снимаем галочу участника Система гарантирования
					//var ca = DataContainerFacade.GetByID<Contragent>(_reorganization.SourceContragentID);
					//var le = ca.GetLegalEntity();
					//le.GarantACBID = (long)LegalEntity.GarantACB.NotIncluded;
					//DataContainerFacade.Save<LegalEntity, long>(le);

                    _reorganization.ID = ID;
                    break;
                default:
                    break;
            }
        }

        public override bool CanExecuteSave()
        {
            return Validate() && IsDataChanged;
        }

        public override bool CanExecuteDelete()
        {
            return State != ViewModelState.Create;
        }

        protected override void ExecuteDelete(int delType)
        {
            DataContainerFacade.Delete(_reorganization);
            base.ExecuteDelete(DocOperation.Delete);
        }

        public void ExecuteDelete()
        {
            ExecuteDelete(DocOperation.Delete);
        }

        public void RefreshNPFViewModel()
        {
            if (GetViewModel != null)
            {
                NPFViewModel vm = GetViewModel(typeof(NPFViewModel)) as NPFViewModel;

                if (vm == null && _le != null)
                    vm = new NPFViewModel(ViewModelState.Edit, _le.ID);

				if (vm != null)
				{
					vm.RefreshReorganizationsList();//Нужен для корректной обработки статуса при реорганизации
					vm.RefreshStatus(); // Status refreshing here
					vm.SaveCard.Execute(null);
				}

                var vml = GetViewModel(typeof(NPFListViewModel)) as NPFListViewModel;
                vml?.RefreshList.Execute(null);
            }
        }

        protected void UpdateContragents(long leID)
        {
			StatusFilter sf = StatusFilter.NotDeletedFilter;
            long[] contragentStatusIdsToIgnore = { 2, 3, 4, 5, 6, -1, 2 };
            ContragentList = BLServiceSystem.Client.GetFilteredNPFContragentListHib(sf, contragentStatusIdsToIgnore, leID);
            _le = DataContainerFacade.GetByID<LegalEntity>(leID);
        }


        public ReorganizationViewModel(long id, long leID, ViewModelState action)
            : base(typeof(NPFListViewModel))
        {
            DataObjectTypeForJournal = typeof(Reorganization);

            if (leID > 0) //web
                UpdateContragents(leID);

            m_ReorganizationTypeList = DataContainerFacade.GetList<ReorganizationType>();

            State = action;
            if (State == ViewModelState.Create)
            {
                _reorganization = new Reorganization { SourceContragentID = id, OrderDate = null, ReorganizationDate = DateTime.Today };
                SelectedContragent = ContragentList.FirstOrDefault();
                SelectedReorganizationType = ReorganizationTypeList.FirstOrDefault();
            }
            else
            {
                ID = id;
                _reorganization = DataContainerFacade.GetByID<Reorganization>(id);
                SelectedContragent = ContragentList.FirstOrDefault(c => c.ID == _reorganization.ReceiverContragentID);
                SelectedReorganizationType = ReorganizationTypeList.FirstOrDefault(rt => rt.ID == _reorganization.ReorganizationTypeID);
            }
#if !WEBCLIENT
            RefreshConnectedCardsViewModels = RefreshNPFViewModel;
#endif
        }

        private const string FieldRequiredErrorMessage = "Заполнены не все обязательные поля";

        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "RegNum":
                        return string.IsNullOrWhiteSpace(RegNum) ? FieldRequiredErrorMessage : null;
                    case "OrderDate":
                        if (OrderDate == null)
                            return FieldRequiredErrorMessage;
                        if (OrderDate >= ReorganizationDate)
                            return "Дата приказа ФСФР должна быть меньше даты реорганизации";
                        return null;
                    case "ReorganizationDate":
                        if (ReorganizationDate.Date > DateTime.Today)
                            return "Дата реорганизации не может быть больше сегодняшнего дня";
                        if (ReorganizationDate.Date <= OrderDate)
                            return "Дата реорганизации должна быть больше даты приказа ФСФР";
                        return null;
                    default:
                        return null;
                }
            }
        }

        private bool Validate()
        {
            return SelectedContragent != null && 
                   OrderDate != null &&
                   SelectedReorganizationType != null &&
                   !string.IsNullOrWhiteSpace(RegNum) &&
                   OrderDate != null &&
                   OrderDate < ReorganizationDate &&
                   ReorganizationDate.Date <= DateTime.Today;
        }
    }
}
