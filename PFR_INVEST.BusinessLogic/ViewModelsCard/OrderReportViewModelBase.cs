﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Helpers;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OFPR_manager)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OFPR_worker)]
	[DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_manager)]
	public abstract class OrderReportViewModelBase : ViewModelCard, IRequestCloseViewModel, IUpdateRaisingModel
	{
		public EventHandler OnAvailableSaleSecurityCountChanged;
		public event EventHandler RequestClose;
		public event EventHandler CloseBindingWindows;

		// событие загрузки может вызваться несколько раз, но обработчики событий должны регистрироваться только единожды
		public bool IsEventsRegisteredOnLoad = false;
		protected void RaiseAvailableSaleSecurityCountChanged()
		{
			if (OnAvailableSaleSecurityCountChanged != null)
				OnAvailableSaleSecurityCountChanged(this, null);
		}

		protected const string CONTINUE_TO_SAVE_QUESTION = "Продолжить сохранение?";
		protected const string CONTINUE_TO_DELETE_QUESTION = "Продолжить удаление?";
		protected const string AVAILABLE_COUNT_STRING_FORMAT = "Количество доступных бумаг для {0}: {1}";
		protected const string AVAILABLE_COUNT_INVALID = "для расчета доступного количества бумаг введите значение в поле \"Цена, % от номинала\"";
		protected long m_MaxSaleOrBuySecurityCount;
		protected abstract void RefreshMaxSaleOrBuySecurityCount();
		private string m_AvailableCountString;
		public string AvailableCountString
		{
			get
			{
				return m_AvailableCountString;
			}
			set
			{
				m_AvailableCountString = value;
				OnPropertyChanged("AvailableCountString");
			}
		}


		public readonly OrdReport Report;
		protected readonly CbOrder Order;
		protected CbInOrder CbInOrder;
		protected List<Payment> Payments;
		protected Security Security;
		private readonly Portfolio Portfolio;
		private readonly PfrBankAccount DepoAccount;
		private readonly LegalEntity LegalEntity;

		public EventHandler OnNeedEnterCourseManuallyOrFromCBRSite;
		protected void RaiseNeedEnterCourseManuallyOrFromCBRSite()
		{
			if (OnNeedEnterCourseManuallyOrFromCBRSite != null)
				OnNeedEnterCourseManuallyOrFromCBRSite(this, null);
		}

		public bool CurrencyIsRubles
		{
			get
			{
			    if (SelectedSecurity != null)
					return CurrencyIdentifier.IsRUB(SelectedSecurity.CurrencyName);
			    return false;
			}
		}

		public bool IsBuyOrder
		{
			get
			{
			    if (Order != null)
					return OrderTypeIdentifier.IsBuyOrder(Order.Type);
			    return false;
			}
		}



		#region ReadOnly Properties

		protected long? OrderParentId;
		public bool IsRelaying { get; private set; }

		private bool GetIsRelaying(CbOrder Order)
		{
			if (this is RelayingReportViewModel)
			{
				return true;

			}
		    if (Order.ParentId.HasValue && Order.ParentId.Value > 0)
		    {
		        OrderParentId = Order.ParentId;
		        return true;
		    }
		    if (Order.GetChildrenList().Any())
		    {
		        OrderParentId = Order.ID;
		        return true;
		    }
		    return false;
		}

		public bool isReadOnly => Report.IsReadOnly || (State == ViewModelState.Read || IsRelaying);
	    public bool isYieldReadOnly => Report.IsReadOnly || State == ViewModelState.Read;

	    public bool isEnabled => !isReadOnly;

	    public string OrderRegNum => Order != null ? Order.RegNum : null;

	    public DateTime? OrderRegDate => Order != null ? Order.RegDate : null;

	    public string BankAgentName => LegalEntity != null ? LegalEntity.FormalizedName : null;

	    public string PortfolioYear => Portfolio != null ? Portfolio.Year : null;

	    public string DEPOAccountNumber => DepoAccount != null ? DepoAccount.AccountNumber : null;

	    public string SecurityKindName => SelectedSecurity != null ? SelectedSecurity.KindName : null;

	    public string SecurityCurrencyName => SelectedSecurity != null ? SelectedSecurity.CurrencyName : null;

	    public decimal? SecurityNomValue => SelectedSecurity != null ? (Report.OneNomValue ?? SelectedSecurity.NomValue) : null;

	    #endregion

		#region SelectedContragent
		public List<string> Contragents => OrderReportsHelper.Contragents;

	    public string SelectedContragent
		{
			get
			{
				return Report != null ? Report.ContragentStatus : null;
			}
			set
			{
				if (Report != null)
				{
					Report.ContragentStatus = value;
					OnPropertyChanged("SelectedContragent");
				}
			}
		}
		#endregion

		#region SelectedSecurity
		protected List<CbInOrder> m_CbInOrderList;

		//Список отчетов, исключая редактируемый
		protected List<BuyOrSaleReportsListItem> m_AlreadyAddedReports;
		private void RefreshAlreadyAddedReports()
		{
			m_AlreadyAddedReports = BLServiceSystem.Client.GetBuyOrSaleReportsListHib(OrderTypeIdentifier.IsSellOrder(Order.Type), Order.ID);
		}

		private List<Security> m_SecuritiesList;
		public List<Security> SecuritiesList
		{
			get
			{
				return m_SecuritiesList;
			}

			set
			{
				m_SecuritiesList = value;
				OnPropertyChanged("SecuritiesList");
			}
		}

		private Security m_SelectedSecurity;
		public Security SelectedSecurity
		{
			get
			{
				return m_SelectedSecurity;
			}
			set
			{
				m_SelectedSecurity = value;
				if (value != null)
				{

					if (this is RelayingReportViewModel)
					{
						m_CbInOrderList = (this as RelayingReportViewModel).GetCbInOrderListForCreate(Order.GetCbInOrderList().Cast<CbInOrder>().ToList());
						SecuritiesList = (this as RelayingReportViewModel).GetSecurityListForCreate(SecuritiesList);
						Report.ID = 0;
					}
					CbInOrder = m_CbInOrderList.First(cbinorder => cbinorder.SecurityID == value.ID);

					Report.CbInOrderID = CbInOrder.ID;
					RecalcTransactionAmount();
					RefreshMaxSaleOrBuySecurityCount();
					RecalcTransactionAmountForPurchasePrice();
					RefreshPaymentsList();
					if (this is RelayingReportViewModel)
					{
						Count = CbInOrder.Count.Value;
						Price = CbInOrder.MinPriceSell.Value;
						var model = this as RelayingReportViewModel;
						if (model.DestinationItems != null)
						{
							model.DestinationItems.ToList().ForEach(item => item.BuyReportModel.SelectedSecurity = m_SelectedSecurity);
							model.DestinationItems.ToList().ForEach(item => item.ResetPrperty());
						}

					}
					if (this is RelayingReportViewModel.BuyRelayingReportViewModel)
					{
						Count = CbInOrder.Count.Value;
						Price = CbInOrder.MaxPriceBay.Value;
						Report.ID = 0;
					}
				}
			}
		}
		#endregion

		#region PaymentsList
		private List<Payment> m_PaymentsList;
		public List<Payment> PaymentsList
		{
			get
			{
				return m_PaymentsList;
			}

			set
			{
				m_PaymentsList = value;
				RecalcPaymentsExist();
				OnPropertyChanged("PaymentsList");
				OnPropertyChanged("NKDPerOneObl");
			}
		}

		private Brush m_PaymentsStringColor;
		public Brush PaymentsStringColor
		{
			get
			{
				return m_PaymentsStringColor;
			}
			set
			{
				m_PaymentsStringColor = value;
				OnPropertyChanged("PaymentsStringColor");
			}
		}

		private string m_PaymentsString;
		public string PaymentsString
		{
			get
			{
				return m_PaymentsString;
			}
			set
			{
				m_PaymentsString = value;
				OnPropertyChanged("PaymentsString");
			}
		}
		#endregion

		#region DateDEPO
		public DateTime? DateDEPO
		{
			get
			{
				return Report != null ? Report.DateDEPO : null;
			}

			set
			{
				if (Report != null && Report.DateDEPO != value)
				{
					Report.DateDEPO = value;
					RecalcPaymentsExist();
					RefreshRate();
					RefreshMaxSaleOrBuySecurityCount();
					RecalcTransactionAmountForPurchasePrice();
					OnPropertyChanged("DateDEPO");
				}
			}
		}
		#endregion

		#region DateOrder
		public DateTime? DateOrder
		{
			get
			{
				return Report != null ? Report.DateOrder : null;
			}
			set
			{
				if (Report != null && Report.DateOrder != value)
				{

					Report.DateOrder = value;

					if (!Report.DateOrder.HasValue) return;

					RecalcPaymentsExist();
					RefreshMaxSaleOrBuySecurityCount();
					RecalcTransactionAmountForPurchasePrice();
					OnPropertyChanged("DateOrder");
					if (this is RelayingReportViewModel)
					{

						var model = this as RelayingReportViewModel;
						if (model.DestinationItems != null)
						{
							model.DestinationItems.ToList().ForEach(item => item.BuyReportModel.Report.DateOrder = value);

						}

					}
				}
			}
		}
		#endregion

		#region ReportDate
		public DateTime? ReportDate
		{
			get { return Report != null ? Report.ReportDate : null; }
			set
			{
				if (Report != null && Report.ReportDate != value)
				{
					Report.ReportDate = value;
					OnPropertyChanged("ReportDate");
				}
			}
		}
		#endregion

		#region Rate
		public bool IsRateRequested { get; set; }

		private bool m_IsSelectCourceButtonEnabled;
		public bool IsSelectCourceButtonEnabled
		{
			get
			{
				return m_IsSelectCourceButtonEnabled && !isReadOnly;
			}
			set
			{
				m_IsSelectCourceButtonEnabled = value;
				OnPropertyChanged("IsSelectCourceButtonEnabled");
			}
		}

		public void GetCourceManuallyOrFromCBRSite()
		{
			decimal? requestedRate = DialogHelper.GetCourceManuallyOrFromCBRSite(SelectedSecurity.CurrencyID.Value, Report.DateDEPO.Value);
			if (requestedRate.HasValue)
			{
				IsRateRequested = true;
				IsSelectCourceButtonEnabled = false;
				Rate = requestedRate.Value;
				DateTime date;
				if (CurrencyIsRubles)
					date = Report.DateOrder.Value;
				else
					date = Report.DateDEPO.Value;
				Curs rate = new Curs
				{
					Value = Rate,
					Date = date,
					CurrencyID = SelectedSecurity.CurrencyID
				};
				DataContainerFacade.Save<Curs, long>(rate);
			}
		}

		private void RefreshRate()
		{
			if (CurrencyIsRubles)
			{
				IsRateRequested = true;
				Rate = 1;
			}
			else
			{
				decimal? requestedRate = null;
				if (SelectedSecurity != null && SelectedSecurity.CurrencyID > 0 && Report != null && Report.DateDEPO != null)
				{
					try
					{
						DateTime date;
						if (CurrencyIsRubles)
							date = Report.DateOrder.Value;
						else
							date = Report.DateDEPO.Value;
						requestedRate = BLServiceSystem.Client.GetRateForDateHib(SelectedSecurity.CurrencyID, date).Value;
					}
					catch
					{
						requestedRate = null;
					}
				}

				if (requestedRate == null)
				{
					IsSelectCourceButtonEnabled = true;
					IsRateRequested = false;
					Rate = 0;
					RaiseNeedEnterCourseManuallyOrFromCBRSite();
				}
				else
				{
					IsRateRequested = true;
					Rate = requestedRate.Value;
				}
			}
		}

		protected virtual void RecalcRubSums()
		{
		}

		private decimal m_Rate;
		public decimal Rate
		{
			get
			{
			    if (CurrencyIsRubles)
					return 1;

			    if ((Report.CustomCurs ?? 0) > 0)
			        return Report.CustomCurs.Value;
			    return m_Rate;
			}
			set
			{
				if (m_Rate != value)
				{
					m_Rate = value;
					RecalcRubSums();
				}
				OnPropertyChanged("Rate");
				OnPropertyChanged("NKDTotalRub");
			}
		}
		#endregion

		#region Price
		public decimal Price
		{
			get
			{
				return Report == null ? 0 : Report.Price ?? 0;
			}
			set
			{
				if (Report != null)
				{
					Report.Price = value;
					RefreshMaxSaleOrBuySecurityCount();
					RecalcSumWithoutNKD();
					OnPropertyChanged("Price");
				}
			}
		}
		#endregion

		#region Count
		protected long m_OriginalCount;
		public long Count
		{
			get
			{
				return Report == null ? 0 : Report.Count ?? 0;
			}
			set
			{
				if (Report != null)
				{
					if (!string.IsNullOrEmpty(OrderReportsHelper.CheckNkdTotal(value, NKDPerOneObl, 16)))
					{
						return;
					}

					Report.Count = value;
					RecalcNKDTotal();
					RecalcSumNom();
					RecalcTransactionAmountForPurchasePrice();
					OnPropertyChanged("Count");
					OnPropertyChanged("NKDTotal");
					OnPropertyChanged("NKDPerOneObl");
				}
			}
		}
		#endregion

		#region Вычисляемые суммы
		private void RecalcSumNom()
		{
			decimal nfn = 0;
			var dt = CurrencyIsRubles ? DateOrder.Value : DateDEPO.Value;

			if (PaymentsList != null)
				nfn = OrderReportsHelper.CalcNFN(PaymentsList, dt, SelectedSecurity.NomValue);

			if (State == ViewModelState.Create)
			{

				//if(nfn == 0) nfn = CbInOrder.GetSecurity().NomValue ?? 0;

				SumNom = SelectedSecurity != null
						? nfn * Count
						: 0;
				Report.OneNomValue = nfn;
			}
			else if (State == ViewModelState.Edit)
			{
				SumNom = (Report.OneNomValue ?? nfn) * Count;
			}
			else if (State == ViewModelState.Read) 
			{
				if(SumNom == 0)
					SumNom = (Report.OneNomValue ?? nfn) * Count;
			} 
		}
		private decimal m_SumNom;
		public decimal SumNom
		{
			get
			{
				return m_SumNom;
			}
			set
			{
				m_SumNom = value;
				RecalcSumWithoutNKD();
				OnPropertyChanged("SumNom");
			}
		}

		private void RecalcSumWithoutNKD()
		{
			SumWithoutNKD = SumNom * Price / 100m;
		}
		private decimal m_SumWithoutNKD;
		public decimal SumWithoutNKD
		{
			get
			{
			    if (Report == null) return 0;
			    return Report.CustomSumWithoutNKD ?? m_SumWithoutNKD;
			}
			set
			{
				m_SumWithoutNKD = Math.Round(value, 2, MidpointRounding.AwayFromZero);//как в лотусе
				SumWithoutNKDRub = m_SumWithoutNKD * Rate;
				RecalcTransactionAmount();
				OnPropertyChanged("SumWithoutNKD");
			}
		}

		private decimal m_SumWithoutNKDRub;
		public decimal SumWithoutNKDRub
		{
			get
			{
			    if (Report == null) return 0;
			    return Report.CustomSumWithoutNKDRUR ?? m_SumWithoutNKDRub;
			}
			set
			{
				m_SumWithoutNKDRub = value;
				OnPropertyChanged("SumWithoutNKDRub");
			}
		}
		#endregion

		#region TransactionAmount
		protected abstract void RecalcTransactionAmountForPurchasePrice();
		protected void RecalcTransactionAmount()
		{
			TransactionAmount = SumWithoutNKD + NKDTotal;
		}

		private decimal m_TransactionAmount;
		public decimal TransactionAmount
		{
			get
			{
			    if (Report == null) return 0;
			    return Report.CustomSumm ?? m_TransactionAmount;
			}
			set
			{
				m_TransactionAmount = value;// Math.Round(value, 2);
				TransactionAmountRub = m_TransactionAmount * Rate;
				OnPropertyChanged("TransactionAmount");
			}
		}

		private decimal m_TransactionAmountRub;
		public decimal TransactionAmountRub
		{
			get
			{
			    if (Report == null) return 0;
			    return Report.CustomSummRUR ?? m_TransactionAmountRub;
			}
			set
			{
				m_TransactionAmountRub = value;
				OnPropertyChanged("TransactionAmountRub");
			}
		}
		#endregion

		#region NKDPerOneObl
		private Payment m_ClosestPayment;
		private void RecalcPaymentsExist()
		{
			DateTime dt;
			if (CurrencyIsRubles)
				dt = DateOrder ?? DateTime.Now;
			else
				dt = DateDEPO ?? DateTime.Now;

			//var dt = DateTime.Now;

			if (PaymentsList != null)
			{
				m_ClosestPayment = OrderReportsHelper.GetClosestPayment(dt, PaymentsList);
			}

			if (m_ClosestPayment != null)
			{
				PaymentsString = string.Format("Выплаты {0} (период {1:N0})", m_ClosestPayment.PaymentDate.Value.ToString("dd.MM.yyyy"), m_ClosestPayment.CouponPeriod.Value);
				PaymentsStringColor = Brushes.Black;
			}
			else
			{
				PaymentsString = "Выплаты отсутствуют!";
				PaymentsStringColor = Brushes.Red;
			}

			//if (this.State == ViewModelState.Create)
			RecalcNKDPerOneObl();
		}

		private void RecalcNKDPerOneObl()
		{
			if (m_ClosestPayment != null)
			{
				decimal total = 0;
				if (CurrencyIsRubles)
				{
					if (DateOrder.HasValue)
						total = OrderReportsHelper.CalcNKDForRubles(DateOrder.Value, m_ClosestPayment);
				}
				else
				{
					if (DateDEPO.HasValue)
						total = OrderReportsHelper.CalcNKDForDollars(DateDEPO.Value, m_ClosestPayment);
				}
				if (total < 0)
					m_NKDPerOneObl = 0;
				else
					m_NKDPerOneObl = Math.Round(total, 16);
			}
			else
				m_NKDPerOneObl = 0;
			OnPropertyChanged("NKDPerOneObl");
		}

		private decimal m_NKDPerOneObl;
		public decimal NKDPerOneObl
		{
			get
			{
				return Report.OneNKD ?? m_NKDPerOneObl;
			}
			set
			{

				if (!string.IsNullOrEmpty(OrderReportsHelper.CheckNkdTotal(Count, value, 16)))
				{
					return;
				}

				if (!Report.IsReadOnly)
				{					
					//НКД на одну аблигацию принудительно округлять до 2х знаков после запятой.
					Report.OneNKD = Math.Round(value, 2, MidpointRounding.AwayFromZero);
					RecalcNKDTotal();
					OnPropertyChanged("NKDPerOneObl");
					OnPropertyChanged("Count");
				}
			}

		}
		#endregion

		#region NKDTotal


		private void RecalcNKDTotal()
		{

			NKDTotal = NKDPerOneObl * Count;//Math.Round(NKDPerOneObl * Count, 2, MidpointRounding.AwayFromZero);   

		}

		public decimal NKDTotal
		{
			get
			{
			    if (Report != null)
					return Report.CustomNKD ?? Report.NKD ?? 0;
			    return 0;
			}
			set
			{
				if (Report != null)
				{
					if (Report.NKD != value)
					{
						Report.NKD = value;
						RecalcTransactionAmount();
						OnPropertyChanged("NKDTotal");
						OnPropertyChanged("Count");
						OnPropertyChanged("NKDPerOneObl");
						OnPropertyChanged("NKDTotalRub");
					}

				}
			}
		}

		//private decimal m_NKDTotalRub;
		public decimal NKDTotalRub
		{
			get
			{
			    if (Report == null)
					return 0;
			    return Report.CustomNKDRUR ?? (Math.Round(NKDTotal, 2, MidpointRounding.AwayFromZero) * Rate);//как в лотусе
			}
			//set
			//{
			//    m_NKDTotalRub = value;
			//    OnPropertyChanged("NKDTotalRub");
			//}
		}
		#endregion

		public OrderReportViewModelBase(long lOrderID, Type[] p_ViewModelListTypes)
			: this(lOrderID, ViewModelState.Create, p_ViewModelListTypes)
		{
		}

		public OrderReportViewModelBase(long lID, ViewModelState action, Type[] p_ViewModelListTypes)
			: base(p_ViewModelListTypes)
		{
            DataObjectTypeForJournal = typeof(OrdReport);
            ID = lID;
			State = action;

			if (action == ViewModelState.Create)
			{
				Order = DataContainerFacade.GetByID<CbOrder>(lID);
				//При перекладке все даты должны быть равны дате поручения
				var isRelaying = GetIsRelaying(Order);
				var usedDate = isRelaying ? Order.RegDate : DateTime.Today;

				Report = new OrdReport
				{
					DateOrder = usedDate,
					ReportDate = usedDate,
					DateDEPO = usedDate,
					ContragentStatus = Contragents.First()
				};


			}
			else
			{
				Report = DataContainerFacade.GetByID<OrdReport, long>(lID);
				CbInOrder = Report.GetCbInOrder();
				Order = CbInOrder.GetOrder();
				IsRelaying = GetIsRelaying(Order);
			}
			if (this is RelayingReportViewModel)
			{
				m_CbInOrderList = (this as RelayingReportViewModel).GetCbInOrderListForCreate(Order.GetCbInOrderList().Cast<CbInOrder>().ToList());
				if (!m_CbInOrderList.Any()) return;
			}
			else
			{
				m_CbInOrderList = Order.GetCbInOrderList().Cast<CbInOrder>().ToList();
			}

			RefreshAlreadyAddedReports();
			m_OriginalCount = Count;

			Portfolio = Order.GetPortfolio();
			if ((this is RelayingReportViewModel) || IsRelaying && action == ViewModelState.Create)
			{
				DepoAccount = Order.GetPfrBankAccount(); //для перекладки
			}
			else
			{
				DepoAccount = Order.GetDEPOAccount();
			}



			if (DepoAccount != null)
				LegalEntity = DepoAccount.GetLegalEntity();

			RefreshSecuritiesList();
			RefreshRate();
			RecalcSumNom();
			RecalcNKDTotal();
			RefreshConnectedCardsViewModels = RefreshOrderViewModel;
		}

		#region Refresh
		public void RefreshSecuritiesList()
		{
			if (Order == null)
				return;

			SecuritiesList = BLServiceSystem.Client.GetSecuritiesListForOrderHib(Order.ID);
			if (this is RelayingReportViewModel)
			{
				SecuritiesList = (this as RelayingReportViewModel).GetSecurityListForCreate(SecuritiesList);
			}

			if (CbInOrder != null)
			{
				var ssec = SecuritiesList.Find(
					sec => sec.ID == CbInOrder.SecurityID
				);
				if (ssec != null)
					SelectedSecurity = ssec;
				else
					SelectedSecurity = SecuritiesList.FirstOrDefault();
			}
			else
				SelectedSecurity = SecuritiesList.FirstOrDefault();
		}

		public void RefreshPaymentsList()
		{
			if (CbInOrder != null && CbInOrder.SecurityID > 0)
				PaymentsList = BLServiceSystem.Client.GetPaymentsListHib(CbInOrder.SecurityID);
		}

		protected void RefreshOrderViewModel()
		{
			if (GetViewModel != null)
			{
				OrderViewModel vm = GetViewModel(typeof(OrderViewModel)) as OrderViewModel;
				if (vm != null && vm.ID == Order.ID)
					vm.RefreshReports();
			}
		}

		protected void RefreshOrderRelayingViewModel()
		{
			if (GetViewModel != null)
			{
				OrderRelayingViewModel vm = GetViewModel(typeof(OrderRelayingViewModel)) as OrderRelayingViewModel;
				if (vm != null && OrderParentId.HasValue && vm.ID == OrderParentId.Value)
					vm.RefreshReports();

			}
		}
		public void CloseWindow()
		{
			if (RequestClose != null)
				RequestClose(this, null);
		}

		public void BindingWindowsClose()
		{
			if (CloseBindingWindows != null)
				CloseBindingWindows(this, null);
		}

		protected void DeletingViewModel()
		{
			if (GetViewModel != null)
			{
				var vms = GetViewModel(typeof(SaleReportViewModel)) as SaleReportViewModel;
				if (vms != null) vms.BindingWindowsClose();
				var vmb = GetViewModel(typeof(BuyReportViewModel)) as BuyReportViewModel;
				if (vmb != null) vmb.BindingWindowsClose();
			}

		}
		#endregion

		#region Execute implementation
		public override bool CanExecuteDelete()
		{
			return !Report.IsReadOnly && State == ViewModelState.Edit;
		}

		protected override void ExecuteDelete(int delType)
		{

			if (IsRelaying && OrderParentId.HasValue)
			{
			    if (DialogHelper.ShowConfirmation("Выбранный отчет относится к поручению  «на перекладку»\nПри его удалении будут удалены отчеты о продаже\nи покупке для соответствующей ЦБ.\nПродолжить удаление?"))
			    {
			        CbOrder orderParent = DataContainerFacade.GetByID<CbOrder, long>(OrderParentId.Value);
			        orderParent.GetChildrenList().ToList().ForEach(DeleteReport);
			        DeleteReport(orderParent);
			        RefreshOrderRelayingViewModel();
			        DeletingViewModel();
			    } else
			    {
			        Deleting = false;
			        return;
			    }
			}
			else
			{
				DataContainerFacade.Delete(Report);
			}



			base.ExecuteDelete(DocOperation.Delete);
		}


		private void DeleteReport(CbOrder order)
		{
			CbInOrder cbio = order.GetCbInOrderList()
								  .Cast<CbInOrder>().FirstOrDefault(cb => cb.SecurityID == SelectedSecurity.ID);
			if (cbio != null)
			{
				var report =
					DataContainerFacade.GetListByProperty<OrdReport>("CbInOrderID", cbio.ID).FirstOrDefault();
				if (report != null)
				{
					DataContainerFacade.Delete(report);
				}

			}
		}


		//Проперти отвечает за необходимость контроля сделок
		protected bool NeedCheckBuyOrSaleReportCount
		{
			get
			{
				if (!CurrencyIsRubles && OrderTypeIdentifier.IsBuyOrder(Order.Type))
					//Не проверяются только валютные покупки
					return false;

				return true;
			}
		}

		protected override void ExecuteSave()
		{
			switch (State)
			{
				case ViewModelState.Read:
					break;
				case ViewModelState.Edit:
				case ViewModelState.Create:
					if (NeedCheckBuyOrSaleReportCount)
					{
						string wMsg = OrderReportsHelper.CheckCount(Count, m_MaxSaleOrBuySecurityCount);
						if (!string.IsNullOrEmpty(wMsg) &&
							MessageBox.Show(wMsg + CONTINUE_TO_SAVE_QUESTION, "Подтверждение", MessageBoxButton.YesNo, MessageBoxImage.Exclamation) != MessageBoxResult.Yes)
							throw new ViewModelSaveUserCancelledException();
					}
					ID = DataContainerFacade.Save<OrdReport, long>(Report);
					Report.ID = ID;
					m_OriginalCount = Count;
					RefreshAlreadyAddedReports();
					RefreshMaxSaleOrBuySecurityCount();
					if (IsRelaying)
					{
						RefreshOrderRelayingViewModel();
					}
					break;
			}
		}

		protected override bool BeforeExecuteSaveCheck()
		{
			if (this is RelayingReportViewModel && ViewModelState.Create == State)
			{
				var cbList = (this as RelayingReportViewModel).GetCbInOrderListForCreate(Order.GetCbInOrderList().Cast<CbInOrder>().ToList());
				if (!cbList.Any(x => x.ID == Report.CbInOrderID))
				{
					m_CbInOrderList = cbList;
					MessageBox.Show("Отчет для выбранной ЦБ уже создан");
					return false;
				}
			}


			return base.BeforeExecuteSaveCheck();
		}

		public override abstract bool CanExecuteSave();
		#endregion

		#region Validation
		public override abstract string this[string columnName] { get; }
		public abstract bool Validate();
		#endregion



		public IList<KeyValuePair<Type, long>> GetUpdatedList()
		{
			var list = new List<KeyValuePair<Type, long>>();

			list.Add(new KeyValuePair<Type, long>(typeof(OrdReport), Report.ID));

			return list;
		}
	}
}
