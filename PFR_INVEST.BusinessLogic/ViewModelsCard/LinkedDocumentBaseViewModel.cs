﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Constants.Files;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Helpers;

namespace PFR_INVEST.BusinessLogic
{
	public abstract class LinkedDocumentBaseViewModel : ViewModelCard, IUpdateRaisingModel
	{
		public Attach Attach { get; }

		public EventHandler OnAttachmentOpenFailed;
		protected void RaiseAttachmentOpenFailed()
		{
		    OnAttachmentOpenFailed?.Invoke(this, null);
		}

		#region properties
		public IList<AttachClassific> ClassificationList { get; private set; }

		private AttachClassific m_selectedClassification;
		public AttachClassific SelectedClassification
		{
			get
			{
				return m_selectedClassification;
			}
			set
			{
				if (value != null && m_selectedClassification != value)
				{
					m_selectedClassification = value;
					Attach.AttachClassificID = value.ID;
					OnPropertyChanged("SelectedClassification");
				}
			}
		}

		public string Context
		{
			get
			{
				return Attach.Additional;
			}
			set
			{
				if (value != Attach.Additional)
				{
					Attach.Additional = value;
					OnPropertyChanged("Context");
				}
			}
		}

		public string Comment
		{
			get
			{
				return Attach.Comment;
			}
			set
			{
				if (value != Attach.Comment)
				{
					Attach.Comment = value;
					OnPropertyChanged("Comment");
					OnPropertyChanged("SelectedFileName");
					OnPropertyChanged("IsFileSelected");
				}
			}
		}



		private Person m_selectedExecutor;
		public Person SelectedExecutor
		{
			get
			{
				return m_selectedExecutor;
			}
			set
			{
				if (m_selectedExecutor != value)
				{
					m_selectedExecutor = value;
					if (value != null)
						Attach.ExecutorID = value.ID;
					else
						Attach.ExecutorID = null;
					OnPropertyChanged("SelectedExecutor");
				}
			}
		}

		public DateTime? ControlDate
		{
			get { return Attach.ControlDate; }
			set
			{
				if (Attach.ControlDate != value)
				{
					Attach.ControlDate = value;
					OnPropertyChanged("ControlDate");
				}
			}
		}

		public DateTime? ExecuteDate
		{
			get { return Attach.ExecutionDate; }
			set
			{
				if (Attach.ExecutionDate != value)
				{
					Attach.ExecutionDate = value;
					OnPropertyChanged("ExecuteDate");
				}
			}
		}

		public string AdditionallyOnExecution
		{
			get
			{
				return Attach.AdditionalToExecution;
			}
			set
			{
				if (value != Attach.AdditionalToExecution)
				{
					Attach.AdditionalToExecution = value;
					OnPropertyChanged("AdditionallyOnExecution");
				}
			}
		}

		private IList<Element> m_StoragePlacesList;
		public IList<Element> StoragePlacesList
		{
			get { return m_StoragePlacesList; }
			private set
			{
				if (m_StoragePlacesList != value)
				{
					m_StoragePlacesList = value;
					OnPropertyChanged("StoragePlacesList");
				}
			}
		}

		public long? SelectedStoragePlaceID
		{
			get { return Attach.OriginalPlaceID; }
			set
			{
				if (Attach.OriginalPlaceID != value)
				{
					Attach.OriginalPlaceID = value;
					OnPropertyChanged("SelectedStoragePlaceID");
				}
			}
		}

		public string AdditionalInfo
		{
			get { return Attach.Additional; }
			set
			{
				if (value != Attach.Additional)
				{
					Attach.Additional = value;
					OnPropertyChanged("AdditionalInfo");
				}
			}
		}

		private string m_SelectedPathFileName;
		public string SelectedPathFileName
		{
			get { return m_SelectedPathFileName; }
			set
			{
				m_SelectedPathFileName = value;
				SelectedFileName = Path.GetFileName(value);
				OnPropertyChanged("SelectedFileName");
			}
		}

		public string SelectedFileName
		{
			get { return Attach.Comment; }
			set
			{
				Attach.Comment = FileHelper.TrimName(value, 128);
				OnPropertyChanged("SelectedFileName");
				OnPropertyChanged("Comment");
				OnPropertyChanged("IsFileSelected");
			}
		}

		public bool IsFileSelected => Attach.Comment != null;

	    public string ExecutorName
		{
			get { return Attach.ExecutorName; }
			set
			{
				Attach.ExecutorName = value;
				OnPropertyChanged("ExecutorName");
			}
		}

		public abstract bool HasExecutorList { get; }

		public ICommand ChooseAttachment { get; set; }

		public ICommand OpenAttachment { get; set; }
		#endregion

		#region Execute implementation
		protected override void ExecuteSave()
		{
			if (!CanExecuteSave())
				return;

			switch (State)
			{
				case ViewModelState.Read:
					return;
				case ViewModelState.Create:
				case ViewModelState.Edit:
					{
						Attach.Type = Type;
						//Сохраняем только исполнителя со списка или в свободной форме
						if (HasExecutorList)
							ExecutorName = null;
						else
							SelectedExecutor = null;

						if (!string.IsNullOrEmpty(SelectedPathFileName) && CheckAttachment(SelectedPathFileName))
						{
							var fs = new FileStream(SelectedPathFileName, FileMode.Open);
							if (fs.Length > 10048576) // 10MB
								throw new Exception(string.Format("Размер файла '{0}' превышает максимальный размер в 4МБ.", SelectedPathFileName));

							try
							{
								byte[] blob = new byte[fs.Length];
								fs.Read(blob, 0, blob.Length);
								//Attach.CommentFile = blob;
								Attach.FileContentId = BLServiceSystem.Client.SaveFileContent(Attach.FileContentId, blob);
							}
							catch (Exception ex)
							{
								throw new Exception(string.Format("Загрузка вложения '{0}' завершена аварийно: {1}", SelectedPathFileName, ex.Message));
							}
							finally
							{
								fs.Close();
							}
						}
						ID = DataContainerFacade.Save(Attach);
						Attach.ID = ID;
						break;
					}
			}

		}

		public override bool CanExecuteSave()
		{
			return Validate() && IsDataChanged;
		}

		public override bool CanExecuteDelete()
		{
			return ID > 0;
		}

		protected override void ExecuteDelete(int delType)
		{
			DataContainerFacade.Delete(Attach);
			if (Attach.FileContentId.HasValue)
			{
				DataContainerFacade.Delete<FileContent>(Attach.FileContentId.Value);
			}
			base.ExecuteDelete(DocOperation.Delete);

		}

		private void ExecuteChooseAttachment()
		{
			var file = DialogHelper.OpenFile("Все файлы (*.*)|*.*");
			if (!string.IsNullOrWhiteSpace(file) && CheckAttachment(file))
			{
				SelectedPathFileName = file;
			}
		}

		public bool CheckAttachment(string file)
		{
			if (string.IsNullOrWhiteSpace(file))
				return true;

			var fi = new FileInfo(file);

			if (!fi.Exists)
			{
				DialogHelper.ShowAlert(string.Format("Файл '{0}' не найден", file));
				return false;
			}
		    if (Extensions.Executables.Contains(fi.Extension))
		    {
		        DialogHelper.ShowAlert(string.Format("Недопустимое расширение файла '{0}'", fi.Extension));
		        return false;
		    }
		    if (fi.Length > Sizes.Size4Mb)
		    {
		        DialogHelper.ShowAlert("Размер вложения должен быть меньше 4 мегабайт.\nВложение не сохранено.");
		        return false;
		    }
		    return true;
		}

		public bool CanExecuteChooseAttachment()
		{
			return true;
		}

		private void ExecuteOpenAttachment()
		{
			string fullPath;

			if (File.Exists(SelectedPathFileName))
			{
				//если есть выбранный файл - открываем его
				fullPath = SelectedPathFileName;
			}
			else
				if (Attach.FileContentId.HasValue)
				{
					FileContent fileContent = DataContainerFacade.GetByID<FileContent>(Attach.FileContentId);
					if (fileContent == null || fileContent.FileContentData == null)
						return;

					//иначе пытаемся открыть вложение                    
					try
					{
						fullPath = Path.Combine(Path.GetTempPath(), DateTime.Now.Ticks + SelectedFileName);
						FileStream fs = new FileStream(fullPath, FileMode.Create, FileAccess.Write);
						fs.Write(fileContent.FileContentData, 0, fileContent.FileContentData.Length);
						fs.Close();
					}
					catch
					{
						return;
					}
				}
				else
					//иначе - ничего
					return;

			try
			{
				Process.Start(fullPath);
			}
			catch
			{
				RaiseAttachmentOpenFailed();
			}
		}

		public bool CanExecuteOpenAttachment()
		{
			return true;
		}

		#endregion

		const string chooseSelectedClassification = "Параметр 'Классификация документа' должен быть указан.";
		const string chooseSelectedExecutor = "Параметр 'Исполнитель' должен быть указан.";
		const string chooseSelectedStoragePlace = "Параметр 'Место хранения оригинала' должен быть указан.";
		const string chooseSelectedFileName = "Необходимо выбрать вложение.";

		public override string this[string columnName]
		{
			get
			{
				switch (columnName)
				{
					case "SelectedClassification": return SelectedClassification == null ? chooseSelectedClassification : null;
					case "SelectedExecutor": return (SelectedExecutor == null && HasExecutorList) ? chooseSelectedExecutor : null;
					case "ExecutorName": return (string.IsNullOrWhiteSpace(ExecutorName) && !HasExecutorList) ? chooseSelectedExecutor : null;
					case "SelectedStoragePlaceID": return SelectedStoragePlaceID.HasValue ? null : chooseSelectedStoragePlace;
					case "SelectedFileName": return string.IsNullOrEmpty(SelectedFileName) ? chooseSelectedFileName : null;
					case "Comment": return string.IsNullOrEmpty(Comment) ? chooseSelectedFileName : null;
					default:
						return string.Empty;
				}
			}
		}

		private bool Validate()
		{
			foreach (var prop in "SelectedClassification|SelectedExecutor|ExecutorName|SelectedStoragePlaceID|SelectedFileName".Split('|'))
			{
				if (!string.IsNullOrWhiteSpace(this[prop]))
					return false;
			}
			return true;
		}

		protected abstract Type ParentModelType { get; }
		protected abstract Document.Types Type { get; }

		protected abstract IList<Element> GetStoragePlacesList();

		protected abstract IList<AttachClassific> GetAttachClassificList();

		public LinkedDocumentBaseViewModel(long id, ViewModelState action)
		{
			DataObjectTypeForJournal = typeof(Attach);
			State = action;
			ClassificationList = GetAttachClassificList();//DataContainerFacade.GetList<AttachClassific>();
			//this.ExecutorList = this.GetExecutorsList();// DataContainerFacade.GetList<FIO>();

			if (State == ViewModelState.Create)
			{
				Attach = new Attach();
				Attach.DocumentID = id;
				ExecuteDate = DateTime.Today;
				ControlDate = DateTime.Today;
				SelectedClassification = ClassificationList.FirstOrDefault();
				//SelectedExecutor = ExecutorList.FirstOrDefault();
			}
			else
			{
				ID = id;
				Attach = DataContainerFacade.GetByID<Attach>(id);
				SelectedClassification = ClassificationList.FirstOrDefault(classification => classification.ID == Attach.AttachClassificID);
				//SelectedExecutor = ExecutorList.FirstOrDefault(executor => executor.ID == Attach.ExecutorID);
				SelectedExecutor = DataContainerFacade.GetByID<Person>(Attach.ExecutorID);
			}



			StoragePlacesList = GetStoragePlacesList().Where(x => x.Visible || x.ID == SelectedStoragePlaceID).ToList();

			RefreshConnectedCardsViewModels = RefreshDocumentsLinkedDocuments;
			ChooseAttachment = new DelegateCommand(o => CanExecuteChooseAttachment(), o => ExecuteChooseAttachment());
			OpenAttachment = new DelegateCommand(o => CanExecuteOpenAttachment(), o => ExecuteOpenAttachment());
		}

		protected void RefreshDocumentsLinkedDocuments()
		{
			if (GetViewModel != null)
			{
				var vmSIDocument = GetViewModel(ParentModelType) as DocumentBaseViewModel;
				if (vmSIDocument != null)
					vmSIDocument.RefreshLinkedDocuments();
			}
		}

		IList<KeyValuePair<Type, long>> IUpdateRaisingModel.GetUpdatedList()
		{
			return new List<KeyValuePair<Type, long>> { new KeyValuePair<Type, long>(typeof(Attach), Attach.ID) };
		}
	}
}
