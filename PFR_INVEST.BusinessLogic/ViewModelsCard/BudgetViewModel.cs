﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Journal;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
    public class BudgetViewModel : ViewModelCard
    {
        public Budget Budget { get; private set; }

        private List<Portfolio> _mPortfolioList;
        public List<Portfolio> PortfolioList
        {
            get
            {
                return _mPortfolioList;
            }
            set
            {
                _mPortfolioList = value;
                OnPropertyChanged("YearsList");
            }
        }

        Portfolio _mSelectedPortfolio;
        public Portfolio SelectedPortfolio
        {
            get
            {
                return _mSelectedPortfolio;
            }
            set
            {
                if (_mSelectedPortfolio != value && value != null)
                {
                    _mSelectedPortfolio = value;
                    Budget.PortfolioID = value.ID;
                    OnPropertyChanged("SelectedYear");
                }
            }
        }

        public DateTime? Date
        {
            get
            {
                return Budget.Date;
            }
            set
            {
                Budget.Date = value;
                OnPropertyChanged("Date");
            }
        }

        public DateTime? DIDate
        {
            get
            {
                return Budget.DIDate;
            }
            set
            {
                Budget.DIDate = value;
                OnPropertyChanged("DIDate");
            }
        }


        public decimal? Sum
        {
            get {
                return Budget.Sum.HasValue ? Budget.Sum : 0;
            }
            set
            {
                Budget.Sum = value;
                OnPropertyChanged("Sum");
            }
        }

        public string Comment
        {
            get
            {
                return Budget.Comment;
            }
            set
            {
                Budget.Comment = value;
                OnPropertyChanged("Comment");
            }
        }

        private List<BudgetCorrectionListItem> _mBudgetCorrections = new List<BudgetCorrectionListItem>();
        public List<BudgetCorrectionListItem> BudgetCorrections
        {
            get
            {
                return _mBudgetCorrections;
            }
            set
            {
                _mBudgetCorrections = value;
                OnPropertyChanged("BudgetCorrections");
            }
        }

        public bool BudgetIsNotSaved => !(ID > 0);

        private void ExecuteDeleteDop()
        {
            var item = DataContainerFacade.GetByID<BudgetCorrection>(SelectedBudgetCorrection.ID);
            if (item == null) return;
            DataContainerFacade.Delete(item);
            JournalLogger.LogModelEvent(this, JournalEventType.DELETE);
            RefreshCorrections();
            RefreshConnectedViewModels();
        }

        private bool CanExecuteDeleteDop()
        {
            return SelectedBudgetCorrection != null && DeleteAccessDenied;
        }

        private BudgetCorrectionListItem _selb;
        public BudgetCorrectionListItem SelectedBudgetCorrection
        {
            get { return _selb; }
            set
            {
                var wasChnaged = IsDataChanged;
                _selb = value;
                OnPropertyChanged("SelectedBudgetCorrection");
                IsDataChanged = wasChnaged;
            }
        }

        public override bool CanExecuteDelete()
        {
            return State != ViewModelState.Create;
        }

        protected override void ExecuteDelete(int delType)
        {

            BLServiceSystem.Client.DeleteBudgetSchils(Budget.ID);

            //directions
            /*foreach (var item in DataContainerFacade.GetListByProperty<Direction>("BudgetID", Budget.ID))
            {
                //transfers
                foreach (var tr in DataContainerFacade.GetListByProperty<Transfer>("DirectionID", item.ID))
                    DataContainerFacade.Delete(tr);
               

                DataContainerFacade.Delete(item);
            }
			//returns
			foreach (var ret in DataContainerFacade.GetListByProperty<Return>("BudgetID", Budget.ID))
				DataContainerFacade.Delete(ret);

            //corrections
            foreach (var item in DataContainerFacade.GetListByProperty<BudgetCorrection>("BudgetID", Budget.ID))
                DataContainerFacade.Delete(item);

            //budget
            DataContainerFacade.Delete(Budget);*/
            base.ExecuteDelete(DocOperation.Delete);
        }

        protected override void ExecuteSave()
        {
            if (!CanExecuteSave()) return;
            switch (State)
            {
                case ViewModelState.Read:
                    break;
                case ViewModelState.Edit:
                    DataContainerFacade.Save<Budget, long>(Budget);
                    break;
                case ViewModelState.Create:
                    ID = DataContainerFacade.Save<Budget, long>(Budget);
                    Budget.ID = ID;
                    OnPropertyChanged("BudgetIsNotSaved");
                    break;
            }
        }

        public override bool CanExecuteSave()
        {
            return Validate() && IsDataChanged;
        }

        private const string INVALID_DATE = "Не указана дата";
        private const string INVALID_SUM = "Неверно указана сумма";
        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "Date":
                        return Date != null ? null : INVALID_DATE;
                    case "Sum":
                        return Sum.HasValue && Sum.Value != 0 ? null : INVALID_SUM;
                    case "Comment":
                        return Comment.ValidateMaxLength(512);
                    default: return null;
                }
            }
        }

        private bool Validate()
        {
            return ValidateFields();
        }

        public void RefreshCorrections()
        {
            if (Budget != null)
            {
                try
                {
                    var corrs = BLServiceSystem.Client.GetCorrectionsForBudget(Budget.ID);

                    var items = new List<BudgetCorrectionListItem>();

                    var prevSum = Budget.Sum;
                    foreach (var corr in corrs)
                    {
                        var item = new BudgetCorrectionListItem(corr, prevSum);
                        items.Add(item);
                        prevSum = item.Total;
                    }

                    var alreadyChanged = IsDataChanged;
                    BudgetCorrections = items;
                    if (!alreadyChanged)
                        IsDataChanged = false;
                }
                catch (Exception e)
                {
                    if (DoNotThrowExceptionOnError == false)
                        throw;

                    Logger.WriteException(e);
                    if (IsInitialized)
                        RaiseGetDataError();
                    else
                        throw new ViewModelInitializeException();
                }
            }
        }

        public void Load(long lID)
        {
            try
            {
                ID = lID;

                Budget = DataContainerFacade.GetByID<Budget, long>(lID);

                if (Budget.PortfolioID.HasValue)
                {
                    var budgetYear = Budget.GetPortfolio();
                    PortfolioList.Add(budgetYear);
                    SelectedPortfolio = budgetYear;
                }

                RefreshCorrections();

                IsInitialized = true;
            }
            catch (Exception e)
            {
                if (DoNotThrowExceptionOnError == false)
                    throw;

                Logger.WriteException(e);
                throw new ViewModelInitializeException();
            }
        }

        public ICommand DeleteDop { get; private set; }

        public BudgetViewModel()
            : base(typeof(SchilsCostsListViewModel))
        {
            DataObjectTypeForJournal = typeof(Budget);
            PortfolioList = DataContainerFacade.GetList<Portfolio>();
            Budget = new Budget();

            DeleteDop = new DelegateCommand(o => CanExecuteDeleteDop(), o => ExecuteDeleteDop());

            try
            {
                SelectedPortfolio = PortfolioList.FirstOrDefault();
            }
            catch (Exception e)
            {
                Logger.WriteException(e);
            }

            Date = DateTime.Now;
        }
    }
}
