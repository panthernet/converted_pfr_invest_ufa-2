﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_directory_editor)]
    public class ERZLNotifyViewModel : ViewModelCard
    {
        public override string GetDocumentHeader()
        {
            return "НПФ";
        }

        private ERZL _erzl;

        #region ERZLNotifiesList
        private List<ERZLNotify> _erzlNotifiesList;
        public List<ERZLNotify> ERZLNotifiesList
        {
            get
            {
                return _erzlNotifiesList;
            }

            set
            {
                _erzlNotifiesList = value;
                OnPropertyChanged("ERZLNotifiesList");
            }
        }
        #endregion

        #region NPFList
        private IList<LegalEntity> _npfList;
        public IList<LegalEntity> NPFList
        {
            get { return _npfList; }
            set
            {
                _npfList = value;
                OnPropertyChanged("NPFList");
                OnPropertyChanged("SelectedNPF");
            }
        }
        #endregion

        #region SelectedNPF
        private LegalEntity _selectedNPF;
        public LegalEntity SelectedNPF
        {
            get
            {
                return _selectedNPF;
            }

            set
            {
                _selectedNPF = value;
                OnPropertyChanged("SelectedNPF");
                OnPropertyChanged("INN");
                try
                {
                    _selectedNpfzlAccount = BLServiceSystem.Client.GetContragentAccountByMeasure(_selectedNPF.ContragentID ?? 0, "зл");
                }
                catch
                {
                    _selectedNpfzlAccount = null;
                }
                OnPropertyChanged("SelectedNPFZLAccount");
            }
        }
        #endregion

        #region SelectedNPFZLAccount
        private Account _selectedNpfzlAccount;
        public Account SelectedNPFZLAccount
        {
            get
            {
                return _selectedNpfzlAccount;
            }

            set
            {
                _selectedNpfzlAccount = value;
                OnPropertyChanged("SelectedNPFZLAccount");
            }
        }
        #endregion

        public string INN => _selectedNPF != null ? _selectedNPF.INN : string.Empty;


        #region Company
        public string Company
        {
            get
            {
                if (_erzl != null)
                    try
                    {
                        return _erzl.Company.Trim();
                    }
                    catch
                    {
                        return "";
                    }
                return "";
            }
        }
        #endregion

        #region Content
        public string Content
        {
            get
            {
                if (_erzl != null)
                    try
                    {
                        return _erzl.Content.Trim();
                    }
                    catch
                    {
                        return "";
                    }
                return "";
            }
        }
        #endregion

        #region NotifyNum
        private string _notifyNum;
        public string NotifyNum
        {
            get
            {
                return _notifyNum;
            }

            set
            {
                _notifyNum = value;
                OnPropertyChanged("NotifyNum");
            }
        }
        #endregion

        #region Date
        public DateTime? Date
        {
            get
            {
                if (_erzl != null)
                    try
                    {
                        return _erzl.Date;
                    }
                    catch
                    {
                        return null;
                    }
                return null;
            }
            set { }
        }
        #endregion

        #region NPF2PFR_Notify
        private ERZLNotify _npf2PFRNotify;
        public ERZLNotify NPF2PFR_Notify
        {
            get
            {
                return _npf2PFRNotify;
            }

            set
            {
                _npf2PFRNotify = value;
                OnPropertyChanged("NPF2PFR_Notify");
            }
        }
        #endregion

        #region PFR2NPF_Notify
        private ERZLNotify _pfr2NPFNotify;
        public ERZLNotify PFR2NPF_Notify
        {
            get
            {
                return _pfr2NPFNotify;
            }

            set
            {
                _pfr2NPFNotify = value;
                OnPropertyChanged("PFR2NPF_Notify");
            }
        }
        #endregion

        #region ContragentsList_NPF2OTHER
        private List<NPFforERZLNotifyListItem> _contragentsListNPF2Other;
        public List<NPFforERZLNotifyListItem> ContragentsList_NPF2OTHER
        {
            get
            {
                return _contragentsListNPF2Other;
            }

            set
            {
                _contragentsListNPF2Other = value;
                OnPropertyChanged("ContragentsList_NPF2OTHER");
            }
        }
        #endregion

        #region ContragentsList_OTHER2NPF
        private List<NPFforERZLNotifyListItem> _contragentsListOther2NPF;
        public List<NPFforERZLNotifyListItem> ContragentsList_OTHER2NPF
        {
            get
            {
                return _contragentsListOther2NPF;
            }

            set
            {
                _contragentsListOther2NPF = value;
                OnPropertyChanged("ContragentsList_OTHER2NPF");
            }
        }
        #endregion

        #region NPF2PFRCount
        private long? _npf2PFRCount = 0;
        public long? NPF2PFRCount
        {
            get
            {
                return _npf2PFRCount;
            }

            set
            {
                _npf2PFRCount = value;
                OnPropertyChanged("NPF2PFRCount");
            }
        }
        #endregion

        #region NPF2OTHERCount
        private long? _npf2OtherCount = 0;
        public long? NPF2OTHERCount
        {
            get
            {
                return _npf2OtherCount;
            }

            set
            {
                _npf2OtherCount = value;
                OnPropertyChanged("NPF2OTHERCount");
            }
        }

        #endregion

        #region PFR2NPFCount
        private long? _pfr2NPFCount = 0;
        public long? PFR2NPFCount
        {
            get
            {
                return _pfr2NPFCount;
            }

            set
            {
                _pfr2NPFCount = value;
                OnPropertyChanged("PFR2NPFCount");
            }
        }
        #endregion

        #region OTHER2NPFCount
        private long? _other2NPFCount = 0;
        public long? OTHER2NPFCount
        {
            get
            {
                return _other2NPFCount;
            }

            set
            {
                _other2NPFCount = value;
                OnPropertyChanged("OTHER2NPFCount");
            }
        }
        #endregion

        public Visibility Is75FZ => _erzl.GetFZ().Name.Contains("75") ? Visibility.Visible : Visibility.Collapsed;

        public void RefreshNPFList()
        {
            try
            {
                NPFList = BLServiceSystem.Client.GetNPFListLEAccHib(new StatusFilter
                { new FilterOP { BOP = BOP.Or, OP = OP.EQUAL, Identifier = StatusIdentifier.S_ACTIVE }
                });
            }
            catch
            {
                NPFList = new List<LegalEntity>();
            }
            OnPropertyChanged("NPFList");
        }

        public void RefreshERZLNotifiesList()
        {
            try
            {
                _erzlNotifiesList = new List<ERZLNotify>(BLServiceSystem.Client.GetERZLNotifiesHib(_erzl.ID, _notifyNum));
            }
            catch
            {
                _erzlNotifiesList = new List<ERZLNotify>();
            }
            OnPropertyChanged("ERZLNotifiesList");
        }

        public void RefreshContragentsListsAndCounts(bool firsttime)
        {
            if (firsttime)
            {
                _pfr2NPFCount = 0;
                _npf2PFRCount = 0;
            }

            _npf2OtherCount = 0;
            _other2NPFCount = 0;

            try
            {
                _contragentsListNPF2Other = new List<NPFforERZLNotifyListItem>(
                    BLServiceSystem.Client.GetNPF2NPFListHib(_erzl.ID, _notifyNum, _selectedNpfzlAccount != null ? _selectedNpfzlAccount.ID : 0, true));


                _contragentsListOther2NPF = new List<NPFforERZLNotifyListItem>(
                    BLServiceSystem.Client.GetNPF2NPFListHib(_erzl.ID, _notifyNum, _selectedNpfzlAccount != null ? _selectedNpfzlAccount.ID : 0, false));


                if (_erzlNotifiesList != null && _erzlNotifiesList.Count > 0)
                {
                    foreach (var notify in _erzlNotifiesList)
                    {
                        if (notify.MainNPF == "0")
                        {
                            if (notify.GetDebitAccount().GetContragent().TypeName.ToLower().Trim() == "нпф")
                            {
                                _npf2OtherCount += notify.Count ?? 0;
                                var caLI = _contragentsListNPF2Other.Find(cali => cali.Contragent.ID == notify.GetDebitAccount().GetContragent().ID);
                                if (caLI != null)
                                {
                                    caLI.Count = notify.Count;
                                    caLI.ERZLNotify = notify;
                                }

                            }
                            else
                            {
                                if (firsttime)
                                {
                                    _npf2PFRCount += notify.Count ?? 0;
                                    _npf2PFRNotify = notify.Count != null ? notify : null;
                                }
                            }
                        }
                        else
                        {
                            if (notify.GetCreditAccount().GetContragent().TypeName.ToLower().Trim() == "нпф")
                            {
                                _other2NPFCount += notify.Count ?? 0;
                                var caLI = _contragentsListOther2NPF.Find(cali => cali.Contragent.ID == notify.GetCreditAccount().GetContragent().ID);
                                if (caLI != null)
                                {
                                    caLI.Count = notify.Count;
                                    caLI.ERZLNotify = notify;
                                }

                            }
                            else
                            {
                                if (firsttime)
                                {
                                    _pfr2NPFCount += notify.Count != null ? notify.Count.Value : 0;
                                    _pfr2NPFNotify = notify.Count != null ? notify : null;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            if (ContragentsList_NPF2OTHER != null)
                ContragentsList_NPF2OTHER = ContragentsList_NPF2OTHER.FindAll(f => StatusIdentifier.IsNPFStatusActivityCarries(f.Contragent.GetStatus().Name) || (f.Count != null && f.Count.Value > 0));
            if (ContragentsList_OTHER2NPF != null)
                ContragentsList_OTHER2NPF = ContragentsList_OTHER2NPF.FindAll(f => StatusIdentifier.IsNPFStatusActivityCarries(f.Contragent.GetStatus().Name) || (f.Count != null && f.Count.Value > 0));
            OnPropertyChanged("ContragentsList_NPF2OTHER");
            OnPropertyChanged("ContragentsList_OTHER2NPF");

            OnPropertyChanged("NPF2PFRCount");
            OnPropertyChanged("NPF2OTHERCount");
            OnPropertyChanged("PFR2NPFCount");
            OnPropertyChanged("OTHER2NPFCount");
        }


        public ERZLNotifyViewModel()
        {
            DataObjectTypeForJournal = typeof(ERZL);
#if !WEBCLIENT
            _addNPF2Other = new DelegateCommand(o => CanExecuteAddNPF2OTHER(),
                    o => ExecuteAddNPF2OTHER());
            _addOther2NPF = new DelegateCommand(o => CanExecuteAddOTHER2NPF(),
                o => ExecuteAddOTHER2NPF());
            OKCommand = new DelegateCommand(o => CanExecuteOK(),
                o => ExecuteOK());
#endif
            RefreshNPFList();
        }

        public void Init(long erzl_id, ViewModelState action)
        {
            State = action;
            try
            {
               
                _erzl = DataContainerFacade.GetByID<ERZL, long>(erzl_id);
            }
            catch
            {
                _erzl = new ERZL();
            }
            ID = _erzl.ID;

            switch (action)
            {
                case ViewModelState.Create:
                    _erzlNotifiesList = new List<ERZLNotify>();
                    break;
                case ViewModelState.Edit:
                    RefreshERZLNotifiesList();
                    break;
                default:
                    _erzlNotifiesList = new List<ERZLNotify>();
                    break;
            }

            RefreshContragentsListsAndCounts(true);
            if (ContragentsList_NPF2OTHER != null)
                RecalculateNPF2OTHER();
            if (ContragentsList_OTHER2NPF != null)
                RecalculateOther2NPF();

            OnPropertyChanged("NotifyNum");
            OnPropertyChanged("Date");
        }


#region Commands
        private readonly ICommand _addNPF2Other;
        public ICommand AddNPF2OTHER => _addNPF2Other;

        private readonly ICommand _addOther2NPF;
        public ICommand AddOTHER2NPF => _addOther2NPF;

        public ICommand OKCommand { get; private set; }

#endregion

#region Execute Implementations
        private void ExecuteAddNPF2OTHER()
        {
            if (ContragentsList_NPF2OTHER == null)
            {
                DialogHelper.ShowAlert("Список контрагентов пуст.");
                return;
            }

            var result = DialogHelper.AddNPFToOther(_contragentsListNPF2Other.Select(item => item.Clone()).ToList(), SelectedNPF.ContragentID.Value);

            if (result != null)
            {
                ContragentsList_NPF2OTHER = result;
                RecalculateNPF2OTHER();
                OnPropertyChanged("NPF2OTHERCount");
            }
        }

        public void RecalculateNPF2OTHER()
        {
            ERZLNotifiesList.RemoveAll(n => n.MainNPF.Equals("0") && n.ID == 0);

            _npf2OtherCount = 0;
            foreach (var item in ContragentsList_NPF2OTHER)
            {
                if (item.ERZLNotify != null)
                {
                    var notify = _erzlNotifiesList.Find(not => not.ID == item.ERZLNotify.ID);

                    if (notify != null)
                        notify.Count = item.Count == 0 ? null : item.Count;
                    else
                    {
                        item.ERZLNotify.Count = item.Count == 0 ? null : item.Count;
                        _erzlNotifiesList.Add(item.ERZLNotify);
                    }
                }
                else if (item.Count != null)
                {
                    if (SelectedNPF != null)
                    {
                        try
                        {
                            _erzlNotifiesList.Add(new ERZLNotify
                            {
                                ERZL_ID = _erzl.ID,
                                NotifyNum = _notifyNum,
                                Count = item.Count,
                                CrAccID = _selectedNpfzlAccount.ID,
                                DbtAccID = BLServiceSystem.Client.GetContragentAccountByMeasure(item.Contragent.ID, "зл").ID,
                                MainNPF = "0"
                            });
                        }
                        catch
                        {

                        }
                    }
                }

                if (item.Count != null)
                    _npf2OtherCount += item.Count;
            }
        }

        public bool CanExecuteAddNPF2OTHER()
        {
            return SelectedNPF != null && EditAccessAllowed;
        }

        private void ExecuteAddOTHER2NPF()
        {
            if (ContragentsList_OTHER2NPF == null)
            {
                DialogHelper.ShowAlert("Список контрагентов пуст.");
                return;
            }

            var result = DialogHelper.AddOther2NPF(_contragentsListOther2NPF.Select(item => item.Clone()).ToList(), SelectedNPF.ContragentID.Value);

            if (result != null)
            {
                ContragentsList_OTHER2NPF = result;

                RecalculateOther2NPF();

                OnPropertyChanged("OTHER2NPFCount");
            }
        }

        void RecalculateOther2NPF()
        {
            ERZLNotifiesList.RemoveAll(n => n.MainNPF.Equals("1") && n.ID == 0);

            _other2NPFCount = 0;
            foreach (var item in ContragentsList_OTHER2NPF)
            {
                if (item.ERZLNotify != null)
                {
                    var notify = _erzlNotifiesList.Find(not => not.ID != 0 && not.ID == item.ERZLNotify.ID);

                    if (notify != null)
                        notify.Count = item.Count == 0 ? null : item.Count;
                    else
                    {
                        item.ERZLNotify.Count = item.Count == 0 ? null : item.Count;
                        _erzlNotifiesList.Add(item.ERZLNotify);
                    }

                }
                else if (item.Count != null)
                {
                    if (SelectedNPF != null)
                    {
                        try
                        {
                            _erzlNotifiesList.Add(new ERZLNotify
                            {
                                ERZL_ID = _erzl.ID,
                                NotifyNum = _notifyNum,
                                Count = item.Count,
                                CrAccID = BLServiceSystem.Client.GetContragentAccountByMeasure(item.Contragent.ID, "зл").ID,
                                DbtAccID = _selectedNpfzlAccount.ID,
                                MainNPF = "1"
                            });
                        }
                        catch
                        {

                        }
                    }
                }

                if (item.Count != null)
                    _other2NPFCount += item.Count;
            }
        }

        public bool CanExecuteAddOTHER2NPF()
        {
            return SelectedNPF != null && EditAccessAllowed;
        }

        public void ExecuteOK()
        {
            if (_erzlNotifiesList != null)
            {
                ERZLNotify notifyToAdd = null;
                if (_npf2PFRNotify != null)
                {
                    notifyToAdd = _erzlNotifiesList.Find(not => not.ID == _npf2PFRNotify.ID);
                }
                if (notifyToAdd != null)
                    notifyToAdd.Count = _npf2PFRCount == 0 ? null : _npf2PFRCount;
                else
                    _erzlNotifiesList.Add(new ERZLNotify
                        {
                            ERZL_ID = _erzl.ID,
                            NotifyNum = _notifyNum,
                            Count = _npf2PFRCount == 0 ? null : _npf2PFRCount,
                            CrAccID = BLServiceSystem.Client.GetContragentAccountByMeasure(_selectedNPF.ContragentID ?? 0, "зл").ID,
                            DbtAccID = BLServiceSystem.Client.GetPFRAccountByMeasure("зл").ID,
                            MainNPF = "0"
                        });

                notifyToAdd = null;
                if (_pfr2NPFNotify != null)
                {
                    notifyToAdd = _erzlNotifiesList.Find(not => not.ID == _pfr2NPFNotify.ID);
                }
                if (notifyToAdd != null)
                    notifyToAdd.Count = _pfr2NPFCount == 0 ? null : _pfr2NPFCount;
                else
                    _erzlNotifiesList.Add(new ERZLNotify
                        {
                            ERZL_ID = _erzl.ID,
                            NotifyNum = _notifyNum,
                            Count = _pfr2NPFCount == 0 ? null : _pfr2NPFCount,
                            CrAccID = BLServiceSystem.Client.GetPFRAccountByMeasure("зл").ID,
                            DbtAccID = BLServiceSystem.Client.GetContragentAccountByMeasure(_selectedNPF.ContragentID ?? 0, "зл").ID,
                            MainNPF = "1"
                        });

                var addList = new List<ERZLNotify>();
                var delList = new List<ERZLNotify>();

                foreach (var notify in _erzlNotifiesList)
                {
                    if (notify.Count != null && notify.Count.Value != 0)
                    {
                        notify.ERZL_ID = _erzl.ID;
                        if (notify.MainNPF == "0")
                            notify.CrAccID = BLServiceSystem.Client.GetContragentAccountByMeasure(_selectedNPF.ContragentID ?? 0, "зл").ID;
                        else
                            notify.DbtAccID = BLServiceSystem.Client.GetContragentAccountByMeasure(_selectedNPF.ContragentID ?? 0, "зл").ID;
                        notify.NotifyNum = _notifyNum;
                        addList.Add(notify);
                    }
                    else if (notify.ID > 0)
                        delList.Add(notify);
                }
                if (!BLServiceSystem.Client.SaveERZLNotifiesList(addList) || !BLServiceSystem.Client.EraseERZLNotifiesList(delList))
                    throw new Exception("При обновлении/сохранении введённых значений произошла ошибка.");
            }
        }

        public bool CanExecuteOK()
        {
            if (NPF2OTHERCount == 0 &&
                OTHER2NPFCount == 0 &&
                PFR2NPFCount == 0 &&
                NPF2PFRCount == 0 || EditAccessDenied)
                return false;

            return string.IsNullOrEmpty(Validate());
        }

        public override bool CanExecuteSave()
        {
            return string.IsNullOrEmpty(Validate());
        }

        protected override void ExecuteSave()
        {
            if (CanExecuteSave())
            {
                switch (State)
                {
                    case ViewModelState.Read:
                    case ViewModelState.Edit:
                    case ViewModelState.Create:
                        break;
                    default:
                        break;
                }
            }
        }
#endregion

        private string Validate()
        {
            const string fieldNames =
               "SelectedNPF|NotifyNum";

            //if (fieldNames.Split("|".ToCharArray()).Any(fieldName => !String.IsNullOrEmpty(this[fieldName])))
            //    return "Неверный формат данных";
            return fieldNames.Split("|".ToCharArray()).Select(item => this[item]).FirstOrDefault(res => !string.IsNullOrEmpty(res));
        }

        public override string this[string columnName]
        {
            get
            {
                const string errorMessage = "Неверный формат данных";                
                switch (columnName)
                {
                    case "SelectedNPF": return SelectedNPF == null ? errorMessage : null;
                    case "NotifyNum": return string.IsNullOrWhiteSpace(NotifyNum) ? errorMessage : NotifyNum.ValidateMaxLength(50);
                    default: return null;
                }
            }
        }
    }
}