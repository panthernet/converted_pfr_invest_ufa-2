﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    //[Obsolete("Устаревший, использовать F26ViewModel", true)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class F24ViewModel : ViewModelCard
    {
        private EdoOdkF024 odk;
        public EdoOdkF024 Odk
        {
            get { return odk; }
            set
            {
                odk = value;
                OnPropertyChanged("Odk");
            }
        }

        private Contract contract;
        public Contract Contract
        {
            get { return contract; }
            set
            {
                contract = value;
                OnPropertyChanged("Contract");
            }
        }

        private LegalEntity legalEntity;
        public LegalEntity LegalEntity
        {
            get { return legalEntity; }
            set
            {
                legalEntity = value;
                OnPropertyChanged("LegalEntity");
            }
        }

        public F24ViewModel(long id)
        {
            DataObjectTypeForJournal = typeof(EdoOdkF024);
            if (id >= 0)
            {
                ID = id;
                Odk = DataContainerFacade.GetByID<EdoOdkF024, long>(id);
                //this.Contract = this.Odk.GetContract();
                //this.LegalEntity = this.Contract.GetLegalEntity();

                RefreshF24List();
            }
        }



        private List<F24ListItem> _F24List = new List<F24ListItem>();


        public List<F24ListItem> F24List
        {
            get { return _F24List; }
            set
            {
                _F24List = value;
                OnPropertyChanged("F24List");
            }
        }

        public override bool CanExecuteSave() { return false; }
        protected override void ExecuteSave() { }

        public override string this[string columnName] => string.Empty;

        public void RefreshF24List()
        {
            List<F24ListItem> list = new List<F24ListItem>();

            list.Add(new F24ListItem("1. Денежные средства на счетах в кредитных организациях (010):", Odk.Group1));
            list.Add(new F24ListItem("2. Депозиты в рублях в кредитных организациях (020):", Odk.Group2));
            list.Add(new F24ListItem("3. Государственные ценные бумаги Российской Федерации, обращающиеся на рынке ценных бумаг (030):", Odk.Group3));
            list.Add(new F24ListItem("4. Гос. ценные бумаги РФ, для размещения средств институциональных инвесторов (040):", Odk.Group4));
            list.Add(new F24ListItem("5. Облигации внешних облигационных займов РФ (050):", Odk.Group5));
            list.Add(new F24ListItem("6. Государственные ценные бумаги субъектов Российской Федерации (060):", Odk.Group6));
            list.Add(new F24ListItem("7. Муниципальные облигации (070):", Odk.Group7));
            list.Add(new F24ListItem("8. Облигации российских хозяйственных обществ (080):", Odk.Group8));
            list.Add(new F24ListItem("9. Акции российских эмитентов, созданных в форме открытых акционерных обществ (090):", Odk.Group9));
            list.Add(new F24ListItem("10. Облигации с ипотечным покрытием, выпущенные в соответствии с законодательством Российской Федерации об ипотечных ценных бумагах (100):", Odk.Group10));
            list.Add(new F24ListItem("11. Ипотечные сертификаты участия, выпущенные в соответствии с законодательством Российской Федерации об ипотечных ценных бумагах (110):", Odk.Group11));
            list.Add(new F24ListItem("12. Паи (акции, доли) индексных инвестиционных фондов, размещающих средства в государственные ценные бумаги иностранных государств, облигации и акции иностранных эмитентов (120):", Odk.Group12));
            list.Add(new F24ListItem("13. Дебиторская задолженность:(130)", Odk.Group13));
            list.Add(new F24ListItem("13.1. Дебиторская задолженность  - средства пенсионных накоплений на специальных брокерских счетах (131):", Odk.Subgroup1));
            list.Add(new F24ListItem("13.2. Дебиторская задолженность по процентному (купонному) доходу по облигациям (132):", Odk.Subgroup2));
            list.Add(new F24ListItem("13.3. Прочая дебиторская задолженность (133):", Odk.Subgroup3));
            list.Add(new F24ListItem("14. Итого рыночная стоимость активов, в которые инвестированы средства пенсионных накоплений :", Odk.TotalAmount));

            F24List = list;
        }

    }
}
