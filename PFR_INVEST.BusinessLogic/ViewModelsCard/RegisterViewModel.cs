﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Common.Extensions;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager)]
    public class RegisterViewModel : ViewModelCard, IUpdateRaisingModel
    {
        public event EventHandler CloseBindingWindows;
        public bool IsEventsRegisteredCloseBindingWindows => CloseBindingWindows != null;

        public Register register;

        #region Kind

        private static readonly List<string> kinds = new List<string>
        {
            RegisterIdentifier.PFRtoNPF,
            RegisterIdentifier.NPFtoPFR
        };

        public virtual List<string> Kinds => kinds;

        public static List<string> RegisterKinds => kinds;

        /// <summary>
        /// Видимость назначения платежа (override в дочерних моделях)
        /// </summary>
        public virtual bool IsPayAssVisible => true;

        /// <summary>
        /// Назначение платежа
        /// </summary>
        [DisplayName("Назначение платежа")]
        [StringLength(250)]
        public string PayAssignment { get { return register.PayAssignment; } set { register.PayAssignment = value; OnPropertyChanged("PayAssignment"); } }

        [Required]
        [DisplayName("Тип операции")]
        public string Kind
        {
            get
            {
                return register != null ? register.Kind : "";
            }
            set
            {
                if (register != null)
                {
                    register.Kind = value;
                    OnPropertyChanged("Kind");

                    contents = value == Kinds.First() ? PFRToNPFContents : NPFToPFRContents;

                    Content = contents.First();
                    OnPropertyChanged("Contents");
                }
            }
        }
        #endregion

        #region Content
        public virtual bool IsContentVisible => true;

        //
        private static List<string> _PFRToNPFContents;
        protected static List<string> PFRToNPFContents
        {
            get
            {
                if (_PFRToNPFContents == null)
                {
                    var contentList = BLServiceSystem.Client.GetElementByType(Element.Types.RegisterPFRtoNPF);
                    _PFRToNPFContents = contentList.Where(x => x.Visible).Select(e => e.Name).ToList();
                }
                return _PFRToNPFContents;
            }
        }

        private static List<string> _NPFToPFRContents;
        protected static List<string> NPFToPFRContents
        {
            get
            {
                if (_NPFToPFRContents == null)
                {
                    var contentList = BLServiceSystem.Client.GetElementByType(Element.Types.RegisterNPFtoPFR);
                    _NPFToPFRContents = contentList.Where(x => x.Visible).Select(e => e.Name).ToList();
                }
                return _NPFToPFRContents;
            }
        }
        //{
        //    "Переход ЗЛ ежегодно (75-ФЗ)",
        //    "Материнский семейный капитал (256-ФЗ)",
        //    "Возврат СПН ввиду отсутствия правопреемников (пост. 742)",
        //    "Договоры, не вступившие в силу (Расп. 118)",
        //    "Ликвидация НПФ",
        //    "Возврат СПН по решению суда",
        //    "Другое"
        //};

        protected List<string> contents;
        public List<string> Contents
        {
            get
            {
                return contents;
            }

            set
            {
                contents = value;
                OnPropertyChanged("Contents");
            }
        }

        private string _content = "";

        [Required]
        [DisplayName("Содержание операции")]
        public string Content
        {
            get
            {
                return _content;
            }
            set
            {
                if (_content != value)
                {
                    _content = value;
                    //if (value != null) RefreshPortfoliosList();
                    UpdatePaymentAssignment();
                    OnPropertyChanged("Content");
                    OnPropertyChanged("IsCompanyRequired");
                    OnPropertyChanged("Company");
                }
            }
        }
        #endregion

        #region Company
        [DisplayName("Кампания по приему заявлений")]
        [StringLength(256)]
        public string Company
        {
            get
            {
                if (register == null) return "";
                return !string.IsNullOrWhiteSpace(register.Company) ? register.Company : null;
            }

            set
            {
                if (register != null)
                {
                    register.Company = value;
                    OnPropertyChanged("Company");
                }
            }
        }

        public virtual bool IsCompanyRequired => (Content != "Ликвидация НПФ") && (Content != "Аннулирование лицензии") && IsCompanyVisible;

        public virtual bool IsCompanyVisible => true;

        #endregion

        #region Portfolio
        public virtual bool IsPortfolioVisible => false;
        public virtual bool IsPortfolioEnabled => false;
        private List<Portfolio> portfoliosList;
        public List<Portfolio> PortfoliosList
        {
            get
            {
                return portfoliosList;
            }

            set
            {
                portfoliosList = value;
                OnPropertyChanged("PortfoliosList");
            }
        }

        protected Portfolio selectedPortfolio;
        public virtual Portfolio SelectedPortfolio
        {
            get
            {
                return selectedPortfolio;
            }

            set
            {
                selectedPortfolio = value;
                if (selectedPortfolio != null)
                    register.PortfolioID = selectedPortfolio.ID;
                OnPropertyChanged("SelectedPortfolio");
            }
        }
        #endregion

        #region ApproveDoc
        private List<ApproveDoc> _approveDocsList;
        public List<ApproveDoc> ApproveDocsList
        {
            get
            {
                return _approveDocsList;
            }

            set
            {
                _approveDocsList = value;
                OnPropertyChanged("ApproveDocsList");
            }
        }

        protected ApproveDoc _selectedApproveDoc;
        public ApproveDoc SelectedApproveDoc
        {
            get
            {
                return _selectedApproveDoc;
            }

            set
            {
                _selectedApproveDoc = value;
                if (_selectedApproveDoc != null)
                    register.ApproveDocID = _selectedApproveDoc.ID;
                OnPropertyChanged("SelectedApproveDoc");
            }
        }
        #endregion

        #region RegNum
        [DisplayName("Номер документа")]
        [StringLength(50)]
        public string RegNum
        {
            get
            {
                if (register != null && register.RegNum != null)
                    return register.RegNum;
                return "";
            }
            set
            {
                if (register != null)
                {
                    register.RegNum = value;
                    OnPropertyChanged("RegNum");
                }
            }
        }

        private string OldRegNum { get; set; }
        #endregion

        #region RegDate
        [DisplayName("Дата документа")]
        public DateTime? RegDate
        {
            get
            {
                return register != null ? register.RegDate : null;
            }
            set
            {
                if (register != null && register.RegDate != value)
                {
                    register.RegDate = value;
                    OnPropertyChanged("RegDate");
                }
            }
        }

        private DateTime? OldRegDate { get; set; }
        #endregion

        #region SumSPN
        private decimal? _sumSPN;
        [DisplayName("Общая сумма СПН")]
        public decimal? SumSPN
        {
            get
            {
                return _sumSPN;
            }

            set
            {
                _sumSPN = value;
                OnPropertyChanged("SumSPN");
            }
        }
        #endregion

        #region SumZL
        public virtual bool IsSumZLVisible => true;
        private decimal? _sumZL;
        [DisplayName("Общая сумма ЗЛ")]
        public decimal? SumZL
        {
            get
            {
                return _sumZL;
            }

            set
            {
                _sumZL = value;
                OnPropertyChanged("SumZL");
            }
        }
        #endregion

        #region SumCFR
        public virtual bool IsSumCFRVisible => false;
        private decimal? _sumCFR;
        [DisplayName("Общая сумма ЧФР")]
        public decimal? SumCFR
        {
            get
            {
                return _sumCFR;
            }

            set
            {
                _sumCFR = value;
                OnPropertyChanged("SumCFR");
            }
        }
        #endregion

        public ERZL ERZL
        {
            get;
            protected set;
        }

        #region ERZL
        public virtual bool IsERZLVisible => true;

        [DisplayName("Ссылка на реестр уведомлений")]
        public string ERZLContent
        {
            get
            {
                if (ERZL == null) return "";
                return ERZL.Content == string.Empty ? "Реестр уведомлений" : ERZL.Content;
            }
        }
        #endregion

        #region Comment
        [DisplayName("Комментарий")]
        [StringLength(2500)]
        public string Comment
        {
            get
            {
                return register != null ? register.Comment : "";
            }
            set
            {
                if (register == null) return;
                register.Comment = value;
                OnPropertyChanged("Comment");
            }
        }
        #endregion

        #region Finregisters

        protected List<FinregisterWithCorrAndNPFListItem> finregWithNPFList;
        public List<FinregisterWithCorrAndNPFListItem> FinregWithNPFList
        {
            get
            {
                var list = new List<FinregisterWithCorrAndNPFListItem>();
                if (finregWithNPFList == null) return list;
                foreach (var item in finregWithNPFList)
                {
                    if (list.Count < 1)
                        list.Add(item);
                    if (list.Any(l => l.CorrCount == item.CorrCount && l.Count == item.Count && l.ID == item.ID && l.NPFName == item.NPFName))
                        continue;
                    list.Add(item);
                }
                return list;
            }

            set
            {
                finregWithNPFList = value;
                OnPropertyChanged("SumSPN");
                OnPropertyChanged("SumZL");
                OnPropertyChanged("SumCFR");
            }
        }
        #endregion

        #region TrancheDate
        [DisplayName("Дата формирования заявки")]
        public DateTime? TrancheDate
        {
            get
            {
                if (register != null && register.Kind.ToLower().Trim() == kinds[0].ToLower().Trim())
                    return register.TrancheDate;
                return null;
            }
            set
            {
                if (register != null && register.Kind.ToLower().Trim() == kinds[0].ToLower().Trim())
                {
                    register.TrancheDate = value;
                    OnPropertyChanged("TrancheDate");
                }
            }
        }
        #endregion

        #region GridColumnSettings
        public bool IsCorrColumnVisible => Kind != null && Kind.Trim().ToLower() == kinds[1].ToLower();

        public virtual bool IsCFRColumnVisible => false;
        public virtual bool IsZLColumnEditable => false;
        public virtual bool IsCountColumnEditable => false;
        public virtual bool IsCFRCountColumnEditable => false;

        #endregion

        public virtual bool IsKindEditable => State == ViewModelState.Create;

        public Visibility TrancheDateVisibility => register != null && register.Kind.ToLower().Trim() == kinds[0].ToLower().Trim() ? Visibility.Visible : Visibility.Collapsed;
        public bool IsTrancheDateVisible => TrancheDateVisibility == Visibility.Visible;

        private readonly ICommand _deleteFr;
        public ICommand DeleteFR => _deleteFr;

        #region Execute Implementations
        public bool CanExecuteDeleteFR()
        {
            return !IsReadOnly && SelectedFR != null && DeleteAccessAllowed;
        }

        public virtual void ExecuteDeleteFR()
        {
            var ids = new List<long> { SelectedFR.Finregister.ID };

            switch (BLServiceSystem.Client.DeleteFinregister(SelectedFR.Finregister.ID, true, "From: " + GetType().Name))
            {
                case WebServiceDataError.FinregisterHasChildren:
                    DialogHelper.ShowError("Невозможно удалить выбранный финреестр, т.к. он находится в статусе \"Финреестр не передан\" и имеет дочерние финреестры!");
                    return;
                case WebServiceDataError.FinregisterHasPP:
                    DialogHelper.ShowError("Невозможно удалить выбранный финреестр, т.к. для него заданы платежные поручения!");
                    return;
                default:
                    CreateArchiveEntry(typeof(FinRegisterViewModel).Name, "Финреестр с формы Реестра", SelectedFR.Finregister.ID);
                    break;
            }

            SelectedFR = null;

            RefreshFinregWithNPFList();
#if !WEBCLIENT
            RefreshConnectedViewModels();
            ViewModelManager.UpdateDataInAllModels(typeof(Finregister), ids.First());

            if (CloseBindingWindows != null)
            {
                var arg = new EventArgs();
                arg.SetAttachedProperty("ids", ids);
                CloseBindingWindows(this, arg);
            }
#endif
        }

        public FinregisterWithCorrAndNPFListItem SelectedFR { get; set; }



        public override bool CanExecuteDelete()
        {
            return State != ViewModelState.Create;
        }

        protected override void ExecuteDelete(int delType)
        {
            //не выгодно использовать BeforeExecuteDeleteCheck() т.к. исп. тот же список для удаления
            var ids = FinregWithNPFList.Select(a => a.ID).ToList();
            switch (BLServiceSystem.Client.DeleteRegister(register.ID))
            {
                case WebServiceDataError.RegisterHasFinregWithInvStatus:
                    DialogHelper.ShowError("Невозможно удалить выбранный реестр, т.к. у него присутствуют финреестры в статусе, отличном от начального!");
                    Deleting = false;
                    return;
                case WebServiceDataError.FinregisterHasPP:
                    DialogHelper.ShowError("Невозможно удалить выбранный реестр, т.к. у него присутствуют финреестры с платежными поручениями!");
                    Deleting = false;
                    return;
            }

            base.ExecuteDelete(DocOperation.Archive);
            if (CloseBindingWindows != null)
            {
                var arg = new EventArgs();
                arg.SetAttachedProperty("ids", ids);
                arg.SetAttachedProperty("pID", ID);
                CloseBindingWindows(this, arg);
            }

        }

        public override bool CanExecuteSave()
        {
            if (finregWithNPFList != null && finregWithNPFList.Any(x => x.Finregister.ZLCount < 0))
                return false;

            return Validate() && IsDataChanged;
        }

        protected override void OnCardSaved()
        {
            OnPropertyChanged("IsKindEditable");
            IsDataChanged = false;
            base.OnCardSaved();
            // IsDataChanged = false;
        }

        protected virtual void AddMainRecord()
        {
        }

        protected virtual void LoadFinregFromDB()
        {
            finregWithNPFList = BLServiceSystem.Client.GetFinregisterWithCorrAndNPFList(register.ID);
        }

        protected virtual void SaveReturnFinregisters()
        {
        }

        /// <summary>
        /// Получение списка доступных для добавления НПФ для данного реестра (используется для расчета доступных средств временного размещения)
        /// </summary>
        /// <param name="excludeExists">true - исключаются НПФ, которые уже привязаны к реестру, false - исключается учет сумм, которые содержатся в БД для НПф по текущему реестру</param>
        /// <returns></returns>
        public List<FinregisterListItem> GetAvailableNPFList(bool excludeExists = false)
        {
            if (register.PortfolioID.HasValue)
            {
                //var allListSPN = BLServiceSystem.Client.GetSPNAllocatedFinregisters(this.register.PortfolioID.Value, this.Content);
                //var retListSPN = BLServiceSystem.Client.GetSPNReturnedFinregisters(this.register.PortfolioID.Value, this.Content);

                var allListSPN = BLServiceSystem.Client.GetSPNAllocatedFinregisters(register.PortfolioID.Value);
                var retListSPN = BLServiceSystem.Client.GetSPNReturnedFinregisters(register.PortfolioID.Value);


                if (finregWithNPFList != null && finregWithNPFList.Any())
                {
                    if (!excludeExists)
                    {// удаление из списка вычетов, относящихся к текущему реестру (использование при определении исчерпания суммы)
                        retListSPN.RemoveAll(x => finregWithNPFList.Any(y => y.Finregister.ID > 0 && y.Finregister.ID == x.ID));
                    }
                    else
                    {// удаление из списка доступных НПФ, которые уже есть для текущего реестра
                        allListSPN.RemoveAll(x => finregWithNPFList.Any(y => y.Finregister.CrAccID == x.CrAccID && y.Finregister.DbtAccID == x.DbtAccID));
                    }
                }


                var allSPN = allListSPN.GroupBy(x => x.NPFName);
                var retSPN = retListSPN.GroupBy(x => x.NPFName);


                var avList = new List<FinregisterListItem>();

                foreach (var gr in allSPN)
                {
                    var ret = retSPN.FirstOrDefault(x => x.Key == gr.Key);
                    var item = new FinregisterListItem
                    {
                        ID = 0,
                        RegisterID = ID,
                        RegNum = RegNum,
                        Date = RegDate,
                        Count = gr.Sum(x => x.Count),
                        CFRCount = gr.Sum(x => x.CFRCount),
                        ZLCount = gr.Sum(x => x.ZLCount)
                    };
                    if (ret != null)
                    {
                        item.Count -= ret.Sum(x => x.Count);
                        item.CFRCount -= ret.Sum(x => x.CFRCount);
                        item.ZLCount -= ret.Sum(x => x.ZLCount);
                    }

                    if (item.Count > 0 || item.ZLCount > 0)
                    {
                        avList.Add(item);
                    }

                    item.CrAccID = gr.First().CrAccID;
                    item.DbtAccID = gr.First().DbtAccID;
                    item.NPFName = gr.First().NPFName;
                    item.Status = RegisterIdentifier.FinregisterStatuses.Created;
                }
                return avList;
            }

            return new List<FinregisterListItem>();
        }

        protected override bool BeforeExecuteSaveCheck()
        {
            // проверка при inline редактировании СПН для реестра, созданного по изъятию
            if (!(register.ReturnID > 0)) return base.BeforeExecuteSaveCheck();
            var avNPFList = GetAvailableNPFList(false);

            if ((avNPFList == null || avNPFList.Count == 0) && finregWithNPFList != null && finregWithNPFList.Count > 0)
            {
                DialogHelper.ShowAlert("Нет средств, доступных для изъятия в выбранном портфеле");
                return false;
            }

            foreach (var fr in finregWithNPFList)
            {
                var gr = avNPFList.FirstOrDefault(x => x.CrAccID == fr.Finregister.CrAccID && x.DbtAccID == fr.Finregister.DbtAccID);
                if (gr == null)
                {
                    DialogHelper.ShowAlert("Не удается добавить или отредактировать НПФ \"{0}\". Возможно, для изъятия недостаточно средств.", fr.NPFName);
                    return false;
                }

                if (gr.Count < fr.Count)
                {
                    DialogHelper.ShowAlert("Недостаточно средств находится в размещении НПФ \"{0}\" по выбранному портфелю.", gr.NPFName);
                    return false;
                }

                if (gr.ZLCount < fr.Count)
                {
                    DialogHelper.ShowAlert("Недостаточное количество ЗЛ находится в размещении НПФ \"{0}\" по выбранному портфелю.", gr.NPFName);
                    return false;
                }
            }

            return base.BeforeExecuteSaveCheck();
        }

        protected override void ExecuteSave()
        {
            if (!CanExecuteSave()) return;

            register.Content = Content;
            RegNum = RegNum.Trim();

            switch (State)
            {
                case ViewModelState.Read:
                    break;
                case ViewModelState.Edit:
                    if (register != null)
                    {
                        DataContainerFacade.Save<Register, long>(register);
                        AddMainRecord();
                    }

                    if (finregWithNPFList != null && finregWithNPFList.Count > 0)
                    {
                        RecalcSums();
                        if (OldRegNum != register.RegNum)
                        {
                            finregWithNPFList.ForEach(x => x.Finregister.RegNum = register.RegNum);
                        }

                        if (OldRegDate != register.RegDate)
                        {
                            finregWithNPFList.ForEach(x => x.Finregister.Date = register.RegDate);
                        }

                        if (OldRegNum != register.RegNum || OldRegDate != register.RegDate)
                        {
                            var result = BLServiceSystem.Client.SaveReturnFinregisters(finregWithNPFList);
                            for (int i = 0; i < result.Count; i++)
                                finregWithNPFList[i].Finregister.ID = result[i];
                        }
                        else
                        {
                            var list = finregWithNPFList.Where(a => a.IsItemChanged).ToList();
                            var result = BLServiceSystem.Client.SaveReturnFinregisters(list);
                            for (int i = 0; i < result.Count; i++)
                                list[i].Finregister.ID = result[i];
                            list.ForEach(a =>
                            {
                                var frModels = GetViewModelsList(typeof(AllocationNPFViewModel)).Cast<AllocationNPFViewModel>().ToList();
                                var model = frModels.FirstOrDefault(x => x.FinregisterID == a.Finregister.ID);
                                if (model != null)
                                    model.ReloadFRFromDB();
                            });

#region OLD
                            /* foreach (var item in finregWithNPFList)
                            {
								var frModels = GetViewModelsList(typeof(AllocationNPFViewModel)).Cast<AllocationNPFViewModel>().ToList();
								if (item.IsItemChanged)
								{
								    //DataContainerFacade.Save<Finregister, long>(item.Finregister);
									if (item.Finregister is FinregisterListItem)
									{
										var fr = new Finregister();
										item.Finregister.CopyTo(fr);
										item.Finregister.ID = DataContainerFacade.Save(fr);
									}
									else
									{
										item.Finregister.ID = DataContainerFacade.Save(item.Finregister);
									}								    
								    var model = frModels.FirstOrDefault(x => x.FinregisterID == item.Finregister.ID);
								    if (model != null)
								    {
								        model.ReloadFRFromDB();
								    }					    
								}
                            }*/
#endregion
                        }
                    }
                    break;
                case ViewModelState.Create:
                    if (register != null)
                    {
                        ID = register.ID = DataContainerFacade.Save<Register, long>(register);
                        AddMainRecord();
                        SaveReturnFinregisters();
                        OldRegNum = register.RegNum;
                        OldRegDate = register.RegDate;
                    }
                    break;
                default:
                    break;
            }
        }
#endregion

        public RegisterViewModel(long recordID, ViewModelState action)
        {
            DataObjectTypeForJournal = typeof(Register);
#if !WEBCLIENT
            _deleteFr = new DelegateCommand(o => CanExecuteDeleteFR(),
                o => ExecuteDeleteFR());
#endif

            State = action;
            ID = recordID;

            _paList = BLServiceSystem.Client.GetPayAssignmentsForNpf();
            if (action == ViewModelState.Create)
            {
                register = new Register();
                Kind = Kinds.First();
                _content = Contents.First();
                RegDate = DateTime.Now;
                UpdatePaymentAssignment();
            }
            else
            {
                LoadRegister();
            }

            //RefreshPortfoliosList();

            _approveDocsList = DataContainerFacade.GetList<ApproveDoc>();

            if (_approveDocsList != null && register != null)
                _selectedApproveDoc = _approveDocsList.Find(appdoc => appdoc.ID == register.ApproveDocID);

        }

        private readonly List<PaymentAssignment> _paList;

        protected string UpdatePaymentAssignment()
        {
            //var dirId = RegisterIdentifier.IsToNPF(Kind) ? (long)Element.SpecialDictionaryItems.CommonTransferDirectionFromPFR : (long)Element.SpecialDictionaryItems.CommonTransferDirectionToPFR;
            if (!RegisterIdentifier.IsToNPF(Kind))
                return PayAssignment = null;

            var pa =
                    _paList.Where(a => a.DirectionID == (long)Element.SpecialDictionaryItems.CommonTransferDirectionFromPFR && ((a.ExtensionData?.ContainsKey("Element") == true ? ((Element)a.ExtensionData["Element"]).Name : "") == _content || a.OperationContentID == 0))
                        .OrderByDescending(a => a.OperationContentID)
                        .FirstOrDefault();
            return PayAssignment = pa != null ? PaymentAssignmentHelper.ReplaceTagDataForNPF(pa.Name, RegDate) : null;
        }

        //protected string UpdatePaymentAssignment()
        //{
        //    if (!((this is TransferSIWizardViewModel) || (this is TransferVRWizardViewModel)) ||
        //        Kind == null || SelectedTransferType == null)
        //        return PayAssignment = string.Empty;

        //    var part = _contractType == (int)Document.Types.SI ? (long)Element.SpecialDictionaryItems.AppPartSI : (long)Element.SpecialDictionaryItems.AppPartVR;
        //    var payAss = BLServiceSystem.Client.GetPayAssForReport(part, Element.SiDirectionToCommonDirection(SelectedTransferDirection), SelectedTransferType.ID);
        //    return PayAssignment = payAss == null ? PayAssignment : PaymentAssignmentHelper.ReplaceTagDataForNPF(payAss.Name, SelectedDate);

        //}

        //public void RefreshPortfoliosList()
        //{
        //if (RegisterIdentifier.IsPFRtoNPF(Kind))
        //{
        //    if (RegisterIdentifier.Operation.IsInsuranceBid(Content))
        //        PortfoliosList = new List<Portfolio>(DataContainerFacade.GetListByProperty<Portfolio>("Type", PortfolioIdentifier.SPN).Where(a => a.StatusID == 0));
        //    else if (RegisterIdentifier.Operation.IsInsuranceDSP(Content))
        //        PortfoliosList = new List<Portfolio>(DataContainerFacade.GetListByProperty<Portfolio>("Type", PortfolioIdentifier.DSV).Where(a => a.StatusID == 0));
        //    else PortfoliosList = new List<Portfolio>(DataContainerFacade.GetListByProperty<Portfolio>("StatusID", 0).Where(a => string.IsNullOrEmpty(a.Type)));
        //}
        //else
        //{
        //    PortfoliosList = new List<Portfolio>(DataContainerFacade.GetListByProperty<Portfolio>("StatusID", 0));
        //}

        /*if (list != null && list.) this.portfoliosList = list.ToList();
        else list = null;*/

        //if (this.portfoliosList != null && this.register != null)
        //    this.SelectedPortfolio = this.portfoliosList.Find(delegate(Portfolio portfolio)
        //    {
        //        return portfolio.ID == this.register.PortfolioID;
        //    });

        //}

        public ICommand SelectERZL { get; private set; }

        private static bool CanExecuteSelectERZL()
        {
            return true;
        }

        public void SubscribeItems()
        {
            if (finregWithNPFList != null && finregWithNPFList.Count > 0)
            {
                foreach (var item in finregWithNPFList)
                {
                    item.OnItemChanged += itm_OnItemChanged;
                }
            }
        }

        public void UnsubscribeItems()
        {
            if (finregWithNPFList != null && finregWithNPFList.Count > 0)
            {
                foreach (var item in finregWithNPFList)
                {
                    item.OnItemChanged -= itm_OnItemChanged;
                }
            }
        }

        public void LoadRegister()
        {
            bool isdatachanged = IsDataChanged;
            if (ID > 0)
            {
                register = DataContainerFacade.GetByID<Register, long>(ID);
                OldRegDate = register.RegDate;
                OldRegNum = register.RegNum;

                if (register != null)
                {
                    var tmpPay = register.PayAssignment;
                    RefreshFinregWithNPFList();
                    ERZL = register.GetERZL();


                    if (register.Kind != null)
                        Kind = Kinds.SingleOrDefault(x => string.Equals(x, register.Kind.Trim(), StringComparison.InvariantCultureIgnoreCase));

                    if (register.PortfolioID != null && PortfoliosList != null)
                        SelectedPortfolio = PortfoliosList.SingleOrDefault(x => x.ID == register.PortfolioID.Value);

                    Content = register.Content;
                    register.PayAssignment = tmpPay;

                    if (!string.IsNullOrWhiteSpace(Content) && !Contents.Contains(Content))
                    {
                        Contents.Add(Content);
                    }
                }
            }
            IsDataChanged = isdatachanged;
        }

        protected virtual void RefreshFinregWithNPFList()
        {
            if (register != null)
            {
                UnsubscribeItems();

                try
                {
                    //
                    LoadFinregFromDB();
                    SubscribeItems();
                    RecalcSums();
                }
                catch(Exception ex)
                {
                    finregWithNPFList = new List<FinregisterWithCorrAndNPFListItem>();
                }

                OnPropertyChanged("FinregWithNPFList");
            }
        }

        protected void RecalcSums()
        {
            if (finregWithNPFList == null) return;
            decimal totalsumm = 0;
            long totalzl = 0;
            decimal totalcfr = 0;
            if (register.ID != 0)
                foreach (var item in finregWithNPFList.GroupBy(x => x.ID))
                {
                    if (item.First().Count.HasValue || item.First().CorrCount.HasValue)
                        totalsumm += item.First().CorrCount ?? item.First().Count.Value;
                    if (item.First().ZLCount.HasValue)
                        totalzl += item.First().ZLCount.Value;
                    if (item.First().CFRCount.HasValue)
                        totalcfr += item.First().CFRCount.Value;
                }
            else
            {
                totalsumm = finregWithNPFList.Where(x => x.CorrCount.HasValue || x.Count.HasValue).Sum(x => x.CorrCount.HasValue ? x.CorrCount.Value : x.Count.Value);
                totalzl = finregWithNPFList.Where(x => x.ZLCount.HasValue).Sum(x => x.ZLCount.Value);
                totalcfr = finregWithNPFList.Where(x => x.CFRCount.HasValue).Sum(x => x.CFRCount.Value);
            }

            SumSPN = totalsumm;
            SumZL = totalzl;
            SumCFR = totalcfr;
        }

        void itm_OnItemChanged()
        {
            OnPropertyChanged("FinregWithNPFList");
        }

        public override string this[string columnName]
        {
            get
            {
                switch (columnName.ToLower())
                {
                    case "kind": return Kind.ValidateRequired();
                    case "_content": return Content.ValidateRequired();
                    case "company":
                        var result = "";
                        if (IsCompanyRequired)
                            result = Company.ValidateRequired();
                        if (string.IsNullOrEmpty(result))
                            result = Company.ValidateMaxLength(256);
                        return result;
                    case "regnum": return RegNum.ValidateMaxLength(50);
                    case "comment": return Comment.ValidateMaxLength(2500);
                    case "payassignment":
                        return PayAssignment.ValidateMaxLength(250);
                    default: return null;
                }
            }
        }

        protected virtual bool Validate()
        {
            return ValidateFields();
        }

        public IList<KeyValuePair<Type, long>> GetUpdatedList()
        {
            return new List<KeyValuePair<Type, long>> { new KeyValuePair<Type, long>(typeof(Register), ID) };
        }

        public static IEnumerable<FinregisterWithCorrAndNPFListItem> WebGetFinregWithNPFList(long id)
        {
            return BLServiceSystem.Client.GetFinregisterWithCorrAndNPFList(id);
        }

    }
}
