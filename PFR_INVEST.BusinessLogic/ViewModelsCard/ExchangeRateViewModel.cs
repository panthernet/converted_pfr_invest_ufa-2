﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Input;
using Microsoft.Win32;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Proxy;

namespace PFR_INVEST.BusinessLogic
{
    public class RateItem
    {
        public DateTime Date { get; set; }
        public decimal USD { get; set; }
        public decimal EUR { get; set; }

        public RateItem()
        {
            Date = DateTime.MinValue;
            USD = 0;
            EUR = 0;
        }
    }

    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OVSI_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OKIP_manager, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OARRS_manager)]
    public class ExchangeRateViewModel : ViewModelBase
    {
        public bool SQLOnly { get; set; }

        private string selectedFilePath = string.Empty;
        public string SelectedFilePath
        {
            get
            {
                return selectedFilePath;
            }

            set
            {
                selectedFilePath = value;
                OnPropertyChanged("SelectedFilePath");
            }
        }

        private string generatedQuery = string.Empty;
        public string GeneratedQuery
        {
            get
            {
                return generatedQuery;
            }

            set
            {
                generatedQuery = value;
                OnPropertyChanged("GeneratedQuery");
            }
        }

        public ExchangeRateViewModel()
        {
            SQLOnly = true;

            BrowseSource = new DelegateCommand(o => CanExecuteBrowseSource(),
                o => ExecuteBrowseSource());

            UploadSource = new DelegateCommand(o => CanExecuteUploadSource(),
                o => ExecuteUploadSource());
        }

        private void ExecuteBrowseSource()
        {
            var dlg = new OpenFileDialog
            {
                DefaultExt = ".dot",
                Filter = "Курсы валют (.txt)|*.txt;",
                CheckPathExists = true,
                Multiselect = false
            };

            if (dlg.ShowDialog() == true)
            {
                SelectedFilePath = dlg.FileName;
            }
        }

        private bool CanExecuteBrowseSource()
        {
            return true;
        }

        private string ClearFromTrash(string val)
        {
            string result = string.Empty;

            for (int i = 0; i < val.Length; i++)
            {
                if (val[i] == ' ')
                {
                    val = val.Substring(0, i);
                    break;
                }
            }

            return val.Where(t => ((t >= '0') && (t <= '9')) || (t == ',')).Aggregate(result, (current, t) => current + t);
        }

        public void ExecuteUploadSource()
        {
            StreamReader reader = new StreamReader(selectedFilePath);
            string line = string.Empty;
            string tmp = string.Empty;
            List<RateItem> result = new List<RateItem>();
            RateItem newItem;
            DateTime date;

            while (!reader.EndOfStream)
            {
                line = reader.ReadLine().Trim();

                if (line.Contains("/") && line.Contains(",") &&
                   !line.Contains("К") && !line.Contains("а"))
                {
                    newItem = new RateItem();

                    tmp = line.Substring(0, (line.IndexOf(' ') > 0) ? line.IndexOf(' ') : line.Length);
                    if (DateTime.TryParse(tmp.Trim(), out date))
                    {
                        newItem.Date = date;
                        line = line.Substring(line.IndexOf(' ')).Trim();

                        tmp = ClearFromTrash(line.Substring(0, (line.IndexOf(' ') > 0) ? line.IndexOf(' ') : line.Length).Trim());
                        if (tmp != string.Empty)
                        {
                            newItem.USD = Convert.ToDecimal(tmp);
                            line = ClearFromTrash(line.Substring((line.IndexOf(' ') > 0) ? line.IndexOf(' ') : line.Length).Trim());

                            if (line != string.Empty)
                            {
                                newItem.EUR = Convert.ToDecimal(line);
                            }

                            //TODO: add newItem to list
                            result.Add(newItem);
                        }
                    }
                }
            }

            var rate = new Curs();
            long usdID = BLServiceSystem.Client.GetCurrencyID("Доллары США");
            long eurID = BLServiceSystem.Client.GetCurrencyID("Евро");
            string query = GenerateInsertQuery(new Dictionary<string, DBField>
            {
                { "CURS.ID", new DBField("CURS.ID", "ID", "PFR_BASIC", "CURS", FIELD_TYPE.PrimaryKey, DATA_TYPE.Long,  0) },
                { "CURS.CURR_ID", new DBField("CURS.CURR_ID", "CURR_ID", "PFR_BASIC", "CURS", FIELD_TYPE.Data, DATA_TYPE.Long,  0) },
                { "CURS.VALUE", new DBField("CURS.VALUE", "VALUE", "PFR_BASIC", "CURS", FIELD_TYPE.Data, DATA_TYPE.Double,  4) },
                { "CURS.DATE", new DBField("CURS.DATE", "DATE", "PFR_BASIC", "CURS", FIELD_TYPE.Data, DATA_TYPE.Date,  0) }
            }) + ";\n";

            foreach (RateItem item in result)
            {
                if (item.USD > 0)
                {
                    rate.ID = usdID;
                    if (rate.ID > 0)
                    {
                        rate.Value = item.USD;
                        rate.Date = item.Date;
                        string str = query.Replace("{CURR_ID}", rate.ID.ToString()).Replace("{VALUE}", rate.Value.ToString().Replace(",", ".")).Replace("{DATE}", "'" + rate.Date.Value.ToShortDateString() + "'");
                        generatedQuery += str;

                        if (!SQLOnly)
                            BLServiceSystem.Client.AddRate(rate);
                    }
                }

                if (item.EUR > 0)
                {
                    rate.ID = eurID;
                    if (rate.ID > 0)
                    {
                        rate.Value = item.EUR;
                        rate.Date = item.Date;

                        generatedQuery += query.Replace("{CURR_ID}", (rate.ID).ToString()).Replace("{VALUE}", rate.Value.ToString().Replace("{DATE}", "'" + rate.Date.Value.ToShortDateString() + "'").Replace(",", "."));

                        if (!SQLOnly)
                            BLServiceSystem.Client.AddRate(rate);
                    }
                }
            }

            OnPropertyChanged("GeneratedQuery");
            reader.Close();
        }

        private bool CanExecuteUploadSource()
        {
            return File.Exists(SelectedFilePath);
        }

        public ICommand BrowseSource { get; protected set; }
        public ICommand UploadSource { get; protected set; }

        #region generate insert query
        private string GenerateInsertQuery(Dictionary<string, DBField> flds)
        {
            if (flds == null)
                return string.Empty;

            if (flds.Count == 0)
                return string.Empty;

            string query = "INSERT INTO " + flds.Values.ElementAt(0).Scheme + "." + flds.Values.ElementAt(0).DbTable + " (";

            for (int i = 0; i < flds.Count; i++)
            {
                if (i == flds.Count - 1)
                    query += flds.Values.ElementAt(i).DbName + ") ";
                else
                    query += flds.Values.ElementAt(i).DbName + ", ";
            }

            query += " VALUES (";
            string sep = string.Empty;
            string val = string.Empty;

            for (int i = 0; i < flds.Count; i++)
            {
                if (flds.Values.ElementAt(i).FieldType == FIELD_TYPE.PrimaryKey)
                {
                    sep = string.Empty;
                    val = "DEFAULT";
                }
                else
                {
                    if (flds.Values.ElementAt(i).DataType == DATA_TYPE.Long ||
                        flds.Values.ElementAt(i).DataType == DATA_TYPE.Double)
                    {
                        sep = string.Empty;

                        if ((flds.Values.ElementAt(i).DataType == DATA_TYPE.Long) &&
                            (flds.Values.ElementAt(i).FieldType == FIELD_TYPE.ForeignKey) &&
                            (flds.Values.ElementAt(i).Value != null))
                        {
                            if ((long)flds.Values.ElementAt(i).Value < 1)
                                val = "DEFAULT";
                            else
                                val = flds.Values.ElementAt(i).Value.ToString().Replace(",", ".");
                        }
                        else
                        {
                            if (flds.Values.ElementAt(i).Value == null)
                            {
                                val = "DEFAULT";
                            }
                            else
                            {
                                val = flds.Values.ElementAt(i).Value.ToString();
                            }

                        }
                    }
                    else
                    {
                        if (flds.Values.ElementAt(i).DataType == DATA_TYPE.Date)
                        {
                            if (flds.Values.ElementAt(i).Value == null)
                            {
                                val = "DEFAULT";
                                sep = string.Empty;
                            }
                            else
                            {
                                if (flds.Values.ElementAt(i).Value != null)
                                {
                                    if (((DateTime)flds.Values.ElementAt(i).Value).Year < 1900)
                                    {
                                        val = "DEFAULT";
                                        sep = string.Empty;
                                    }
                                    else
                                    {
                                        val = ((DateTime)flds.Values.ElementAt(i).Value).ToString("yyyy-MM-dd");
                                        sep = "'";
                                    }
                                }
                                else
                                {
                                    val = "DEFAULT";
                                    sep = string.Empty;
                                }
                            }
                        }
                        else
                        {
                            if (flds.Values.ElementAt(i).Value == null)
                            {
                                val = "DEFAULT";
                                sep = string.Empty;
                            }
                            else
                            {
                                val = flds.Values.ElementAt(i).Value.ToString();
                                sep = "'";
                            }
                        }
                    }
                }

                val = val.Replace("\"", string.Empty);
                val = val.Replace("\\", string.Empty);

                if (i == flds.Count - 1)
                    query += sep + val + sep + ")";
                else
                    query += sep + val + sep + ", ";
            }

            return query;
        }
        #endregion
    }
}
