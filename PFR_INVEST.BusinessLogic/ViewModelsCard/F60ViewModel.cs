﻿using System;
using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Helpers;
using PFR_INVEST.Proxy;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class F60ViewModel : ViewModelCard
    {
        /// <summary>
        /// Поле хранения данных для старой ODBC системы
        /// </summary>
        public Dictionary<string, DBField> Fields { get; set; }

        #region QuartersList

        private List<int> quartersList;
        public List<int> QuartersList
        {
            get { return quartersList; }
            set { quartersList = value; OnPropertyChanged("QuartersList"); }
        }


        public int SelectedQuarter
        {
            get { return Util.isFieldExists(Fields, "QUARTER") ? Convert.ToInt32(Fields["QUARTER"].Value) : -1; }

            set
            {
                if (!Util.isFieldExists(Fields, "QUARTER")) return;

                Fields["QUARTER"].Value = value;
                OnPropertyChanged("SelectedQuarter");
            }
        }

        #endregion

        #region rx1-rx2 format list

        public String r11 => Util.format(Fields["EDO_ODKF060.SCHASTART1"].Value);

        public String r12 => Util.format(Fields["EDO_ODKF060.SCHASTART2"].Value);

        public String r21 => Util.format(Fields["EDO_ODKF060.PAYMENTRECEIVED1"].Value);

        public String r22 => Util.format(Fields["EDO_ODKF060.PAYMENTRECEIVED2"].Value);

        public String r31 => Util.format(Fields["EDO_ODKF060.PROFIT1"].Value);

        public String r32 => Util.format(Fields["EDO_ODKF060.PROFIT2"].Value);

        public String r41 => Util.format(Fields["EDO_ODKF060.PAYMENTTRANSFER1"].Value);

        public String r42 => Util.format(Fields["EDO_ODKF060.PAYMENTTRANSFER2"].Value);

        public String r51 => Util.format(Fields["EDO_ODKF060.DETAINED1"].Value);

        public String r52 => Util.format(Fields["EDO_ODKF060.DETAINED2"].Value);

        public String r61 => Util.format(Fields["EDO_ODKF060.DETAINEDUKREWARD1"].Value);

        public String r62 => Util.format(Fields["EDO_ODKF060.DETAINEDUKREWARD2"].Value);

        public String r71 => Util.format(Fields["EDO_ODKF060.SCHAEND1"].Value);

        public String r72 => Util.format(Fields["EDO_ODKF060.SCHAEND2"].Value);

        #endregion

        #region YearsList

        private List<DBEntity> yearsList;
        public List<DBEntity> YearsList
        {
            get { return yearsList; }
            set { yearsList = value; OnPropertyChanged("YearsList"); }
        }

        public DBEntity SelectedYear
        {
            get
            {
                if (yearsList != null)
                    foreach (DBEntity year in yearsList)
                        if ((long)year.Fields["ID"].Value == (long)Fields["ID_YEAR"].Value)
                            return year;

                return null;
            }

            set
            {
                Dictionary<string, DBField> dstFields = Fields;
                if (value != null) Util.setDBEntityField(ref dstFields, "ID_YEAR", value.Fields, "ID");

                OnPropertyChanged("SelectedYear");
            }
        }

        #endregion

        #region FullName

        private string fullName;
        public String FullName
        {
            get { return fullName; }
            set { fullName = value; OnPropertyChanged("FullName"); }
        }

        #endregion

        #region Date1
        public String Date1
        {
            get
            {
                return Convert.ToDateTime(Fields["CONTRACT.DOCDATE"].Value).ToShortDateString();
            }
            set
            {
                Fields["CONTRACT.DOCDATE"].Value = value;
                OnPropertyChanged("Date1");
            }
        }

        #endregion

        #region Date2
        public String Date2
        {
            get
            {
                DateTime dateTime = Convert.ToDateTime(Fields["CONTRACT.REGDATE"].Value);
                return dateTime.CompareTo(DateTime.MinValue) == 0 ? string.Empty : dateTime.ToShortDateString();
            }
            set
            {
                Fields["CONTRACT.REGDATE"].Value = value == string.Empty ? DateTime.MinValue.ToShortDateString() : value;

                OnPropertyChanged("Date2");
            }
        }

        #endregion

        #region INN

        private string inn;
        public String INN
        {
            get { return inn; }
            set { inn = value; OnPropertyChanged("INN"); }
        }

        #endregion

        #region ContractsList4NPF

        private List<DBEntity> contractsList4NPF;
        public List<DBEntity> ContractsList4NPF
        {
            get { return contractsList4NPF; }
            set { contractsList4NPF = value; OnPropertyChanged("ContractsList4NPF"); }
        }

        private DBEntity selectedContract;
        public DBEntity SelectedContract
        {
            get { return selectedContract; }
            set
            {
                selectedContract = value;

                if (!Util.isFieldExists(value, "REGDATE") || !Util.isFieldExists(value, "PORTFNAME")) return;

                ContractDate = Convert.ToDateTime(selectedContract.Fields["REGDATE"].Value);
                PortfolioName = selectedContract.Fields["PORTFNAME"].Value.ToString();


                Dictionary<string, DBField> dstFields = Fields;
                if (value != null) Util.setDBEntityField(ref dstFields, "CONTRACT_ID", value.Fields, "ID");

                OnPropertyChanged("SelectedContract");

            }
        }

        #endregion

        #region ContractDate

        private DateTime contractDate;
        public DateTime ContractDate
        {
            get { return contractDate; }
            set { contractDate = value; OnPropertyChanged("ContractDate"); }
        }

        #endregion

        #region PortfolioName

        private string portfolioName;
        public String PortfolioName
        {
            get { return portfolioName; }
            set { portfolioName = value; OnPropertyChanged("PortfolioName"); }
        }

        #endregion

        public DateTime? RegDate
        {
            get { return DBFieldHelper.GetDate("EDO_ODKF060.REGDATE", Fields); }
            set
            {
                DBFieldHelper.SetValue("EDO_ODKF060.REGDATE", value, Fields);
                OnPropertyChanged("RegDate");
            }
        }

        public DateTime? ContractRegDate
        {
            get { return DBFieldHelper.GetDate("CONTRACT.REGDATE", Fields); }
            set
            {
                DBFieldHelper.SetValue("CONTRACT.REGDATE", value, Fields);
                OnPropertyChanged("ContractRegDate");
            }
        }

        public F60ViewModel(long id)
        {
            ID = id;
            //DataObjectTypeForJournal = typeof(EdoOdkF020);
            Fields = BLServiceSystem.Client.GetF60(id);

            var leID = Convert.ToInt64(Fields["CONTRACT.LE_ID"].Value);
            var le = DataContainerFacade.GetByID<LegalEntity>(leID);
            INN = le == null ? null : le.INN;
        }

        protected override void ExecuteSave()
        {
        }

        public override bool CanExecuteSave() { return false; }


        public override string this[string columnName] => "";
    }
}
