﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [Obsolete("Устаревший, использовать F25ViewModel", false)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class F22ViewModel : ViewModelCard
    {
        private EdoOdkF022 odk;
        public EdoOdkF022 Odk
        {
            get { return odk; }
            set
            {
                odk = value;
                OnPropertyChanged("Odk");
            }
        }

        private Contract contract;
        public Contract Contract
        {
            get { return contract; }
            set
            {
                contract = value;
                OnPropertyChanged("Contract");
            }
        }

        private LegalEntity legalEntity;
        public LegalEntity LegalEntity
        {
            get { return legalEntity; }
            set
            {
                legalEntity = value;
                OnPropertyChanged("LegalEntity");
            }
        }

        public override bool CanExecuteSave() { return false; }

        protected override void ExecuteSave() { }

        public override string this[string columnName] => String.Empty;

        public F22ViewModel(long id)
        {
            DataObjectTypeForJournal = typeof(EdoOdkF022);
            if (id > 0)
            {
                ID = id;
                Odk = DataContainerFacade.GetByID<EdoOdkF022, long>(id);
                Contract = Odk.GetContract();
                LegalEntity = Contract.GetLegalEntity();

                Group1List = Odk.GetGroup<F022Group1>();
                Group2List = Odk.GetGroup<F022Group2>();
                Group3List = Odk.GetGroup<F022Group3>();
                Group4List = Odk.GetGroup<F022Group4>();
                Group5List = Odk.GetGroup<F022Group5>();
                Group6List = Odk.GetGroup<F022Group6>();
                Group7List = Odk.GetGroup<F022Group7>();
                Group8List = Odk.GetGroup<F022Group8>();
                Group9List = Odk.GetGroup<F022Group9>();
                Group10List = Odk.GetGroup<F022Group10>();
                Group11List = Odk.GetGroup<F022Group11>();
                Group12List = Odk.GetGroup<F022Group12>();
                SubGroup1List = Odk.GetGroup<F022SubGroup1>();
                SubGroup2List = Odk.GetGroup<F022SubGroup2>();
                SubGroup3List = Odk.GetGroup<F022SubGroup3>();
            }
        }

        #region GroupsLists

        public decimal? Total010 { get { return Group1List.Sum(r => r.Amount); } }
        public decimal? Total020 { get { return Group2List.Sum(r => r.Amount); } }
        public decimal? Total030 { get { return Group3List.Sum(r => r.Amount); } }
        public decimal? Total040 { get { return Group4List.Sum(r => r.Amount); } }
        public decimal? Total050 { get { return Group5List.Sum(r => r.Amount); } }
        public decimal? Total060 { get { return Group6List.Sum(r => r.Amount); } }
        public decimal? Total070 { get { return Group7List.Sum(r => r.Amount); } }
        public decimal? Total080 { get { return Group8List.Sum(r => r.Amount); } }
        public decimal? Total090 { get { return Group9List.Sum(r => r.Amount); } }
        public decimal? Total100 { get { return Group10List.Sum(r => r.Amount); } }
        public decimal? Total110 { get { return Group11List.Sum(r => r.Amount); } }
        public decimal? Total120 { get { return Group12List.Sum(r => r.Amount); } }

        public decimal? Total130 => Odk.Group13;
        public decimal? Total131 { get { return SubGroup1List.Sum(r => r.Amount); } }
        public decimal? Total132 { get { return SubGroup2List.Sum(r => r.Amount); } }
        public decimal? Total133 { get { return SubGroup3List.Sum(r => r.Amount); } }



        private IList<F022Group1> _group1List;
        public IList<F022Group1> Group1List
        {
            get { return _group1List; }
            set { _group1List = value; OnPropertyChanged("Group1List"); }
        }

        private IList<F022Group2> _group2List;
        public IList<F022Group2> Group2List
        {
            get { return _group2List; }
            set { _group2List = value; OnPropertyChanged("Group2List"); }
        }

        private IList<F022Group3> _group3List;
        public IList<F022Group3> Group3List
        {
            get { return _group3List; }
            set { _group3List = value; OnPropertyChanged("Group3List"); }
        }

        private IList<F022Group4> _group4List;
        public IList<F022Group4> Group4List
        {
            get { return _group4List; }
            set { _group4List = value; OnPropertyChanged("Group4List"); }
        }

        private IList<F022Group5> _group5List;
        public IList<F022Group5> Group5List
        {
            get { return _group5List; }
            set { _group5List = value; OnPropertyChanged("Group5List"); }
        }

        private IList<F022Group6> _group6List;
        public IList<F022Group6> Group6List
        {
            get { return _group6List; }
            set { _group6List = value; OnPropertyChanged("Group6List"); }
        }

        private IList<F022Group7> _group7List;
        public IList<F022Group7> Group7List
        {
            get { return _group7List; }
            set { _group7List = value; OnPropertyChanged("Group7List"); }
        }

        private IList<F022Group8> _group8List;
        public IList<F022Group8> Group8List
        {
            get { return _group8List; }
            set { _group8List = value; OnPropertyChanged("Group8List"); }
        }

        private IList<F022Group9> _group9List;
        public IList<F022Group9> Group9List
        {
            get { return _group9List; }
            set { _group9List = value; OnPropertyChanged("Group9List"); }
        }

        private IList<F022Group10> _group10List;
        public IList<F022Group10> Group10List
        {
            get { return _group10List; }
            set { _group10List = value; OnPropertyChanged("Group10List"); }
        }

        private IList<F022Group11> _group11List;
        public IList<F022Group11> Group11List
        {
            get { return _group11List; }
            set { _group11List = value; OnPropertyChanged("Group11List"); }
        }

        private IList<F022Group12> _group12List;
        public IList<F022Group12> Group12List
        {
            get { return _group12List; }
            set { _group12List = value; OnPropertyChanged("Group12List"); }
        }

        private IList<F022SubGroup1> _subGroup1List;
        public IList<F022SubGroup1> SubGroup1List
        {
            get { return _subGroup1List; }
            set { _subGroup1List = value; OnPropertyChanged("SubGroup1List"); }
        }

        private IList<F022SubGroup2> _subGroup2List;
        public IList<F022SubGroup2> SubGroup2List
        {
            get { return _subGroup2List; }
            set { _subGroup2List = value; OnPropertyChanged("SubGroup2List"); }
        }

        private IList<F022SubGroup3> _subGroup3List;
        public IList<F022SubGroup3> SubGroup3List
        {
            get { return _subGroup3List; }
            set { _subGroup3List = value; OnPropertyChanged("SubGroup3List"); }
        }

        #endregion GroupsLists
    }
}
