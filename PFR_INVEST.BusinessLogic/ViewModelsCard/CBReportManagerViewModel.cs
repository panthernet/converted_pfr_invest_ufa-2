﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsCard
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class CBReportManagerViewModel: ViewModelCardDialog, IRequestCustomAction, IUpdateListenerModel
    {
        public ICommand CreateReport { get; private set; }
        public ICommand RefreshReport { get; private set; }
        public ICommand ReportToWord { get; private set; }
        public ICommand ExportReport { get; private set; }

        public BOReportForm1 SelectedReport { get; set; }
        public ObservableCollection<BOReportForm1> Reports { get; set; }

        private readonly bool _isCentralized;
        public bool IsCentralized => _isCentralized;

        public CBReportManagerViewModel(bool centralized = false)
        {
            if (centralized)
            {
                _isCentralized = true;
                Header = " Отчеты в Банк России";
            }
            else
            {
                CreateReport = new DelegateCommand(a=> EditAccessAllowed ,a => DialogHelper.CreateReportForCB(true));
                RefreshReport = new DelegateCommand(a => SelectedReport != null && EditAccessAllowed, a => DialogHelper.RefreshBOReportForm1(SelectedReport));
                ReportToWord = new DelegateCommand(a => SelectedReport != null, a => { DialogHelper.CBReportExportToWord(SelectedReport.ID); });
                ExportReport = new DelegateCommand(a => SelectedReport != null, a => { DialogHelper.CBReportExportToXml(SelectedReport.ID); });                
            }
            
            RefreshGrid();
        }

        public void RefreshGrid(long iId = 0)
        {
            //var selid = SelectedReport == null ? 0 : SelectedReport.ID;
            Reports = new ObservableCollection<BOReportForm1>(BLServiceSystem.Client.GetGeneralBOReportForm1List(_isCentralized ? BOReportForm1.ReportTypeEnum.All : BOReportForm1.ReportTypeEnum.One).OrderByDescending(a=> a.Year).ThenBy(a=> a.Quartal));
            OnPropertyChanged("Reports");
            SelectedReport = iId != 0 ? Reports.FirstOrDefault(a => a.ID == iId) : Reports.FirstOrDefault();
            OnPropertyChanged("SelectedReport");
            //OnRequestCustomAction(iId);
        }

        public event EventHandler RequestCustomAction;

        private void OnRequestCustomAction(long iId)
        {
            RequestCustomAction?.Invoke(iId, null);
        }

        public Type[] UpdateListenTypes => new[] {typeof(BOReportForm1)};
        public void OnDataUpdate(Type type, long id)
        {
            var old = Reports.FirstOrDefault(a => a.ID == id);
            var n = DataContainerFacade.GetByID<BOReportForm1>(id);
            var index = -1;
            if (old != null)
            {
                index = Reports.IndexOf(old);
                Reports.Remove(old);            
            }
            if (n != null)
            {
                if (index == -1) Reports.Add(n);
                else Reports.Insert(index, n);
            }
            //OnPropertyChanged("Reports");
        }
    }
}
