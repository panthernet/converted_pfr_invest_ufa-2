﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.Constants.Files;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.ListExtensions;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Helpers;
using PFR_INVEST.DataObjects.Journal;
using LinqExtensions = PFR_INVEST.Core.Tools.LinqExtensions;

namespace PFR_INVEST.BusinessLogic
{
	public abstract class DocumentBaseViewModel : ViewModelCard, IUpdateRaisingModel
	{
		public abstract Document.Types Type { get; }


		public EventHandler OnAttachmentUploadFailed;
		protected void RaiseAttachmentUploadFailed()
		{
			if (OnAttachmentUploadFailed != null)
				OnAttachmentUploadFailed(this, null);
		}

		public EventHandler OnAttachmentSizeBig;
		protected void RaiseAttachmentSizeBig()
		{
			if (OnAttachmentSizeBig != null)
				OnAttachmentSizeBig(this, null);
		}

		public EventHandler OnAttachmentDeleteFailed;
		protected void RaiseAttachmentDeleteFailed()
		{
			if (OnAttachmentDeleteFailed != null)
				OnAttachmentDeleteFailed(this, null);
		}

		public EventHandler OnAttachmentOpenFailed;
		protected void RaiseAttachmentOpenFailed()
		{
			if (OnAttachmentOpenFailed != null)
				OnAttachmentOpenFailed(this, null);
		}

		private Document Document { get; set; }
		private DocFileBody DocFileBody { get; set; }

		public enum SIDocumentNumberType
		{
			Simple,
			Register
		}

		public bool CardSaved => ID > 0;

	    public bool CanAddLinkedDocuments => CardSaved && EditAccessAllowed;

	    public bool CanDeleteLinkedDocuments => CardSaved && LDList != null && LDList.Count > 0 && EditAccessAllowed;

	    public DateTime? IncomingDate
		{
			get { return Document.IncomingDate; }
			set
			{
				if (value.HasValue && (!Document.IncomingDate.HasValue || Document.IncomingDate.Value.Year != value.Value.Year))
				{
					UpdateNumbersOfCurrentYear(value);
					OnPropertyChanged("IncomingNumber");
				}

				Document.IncomingDate = value;

				OnPropertyChanged("IncomingDate");
				OnPropertyChanged("OutgoingDate");//Для валидатора
			}
		}

		public DateTime? OutgoingDate
		{
			get { return Document.OutgoingDate; }
			set
			{
				Document.OutgoingDate = value;
				OnPropertyChanged("OutgoingDate");
				OnPropertyChanged("IncomingDate");//Для валидатора
			}
		}

		public DateTime? ControlDate
		{
			get
			{
				return Document.ControlDate;
			}
			set
			{
				Document.ControlDate = value;
				OnPropertyChanged("ControlDate");
			}
		}

		public DateTime? ExecutionDate
		{
			get
			{
				return Document.ExecutionDate;
			}
			set
			{
				Document.ExecutionDate = value;
				OnPropertyChanged("ExecutionDate");
			}
		}

		public string IncomingNumber
		{
			get
			{
				if (Document != null && Document.IncomingNumber != null)
					return Document.IncomingNumber.Trim();
				return null;
			}
			set
			{
				Document.IncomingNumber = value;
				OnPropertyChanged("IncomingNumber");
			}
		}

		public string OutgoingNumber
		{
			get
			{
				if (Document != null && Document.OutgoingNumber != null)
					return Document.OutgoingNumber.Trim();
				return null;
			}
			set
			{
				Document.OutgoingNumber = value;
				OnPropertyChanged("OutgoingNumber");
			}
		}

		public string Comment
		{
			get
			{
				return Document.Comment;
			}
			set
			{
				Document.Comment = value;
				OnPropertyChanged("Comment");
			}
		}

		protected abstract IList<string> GetAdditionalExecutionInfo();

		public IList<string> AdditionalExecutionInfos
		{
			get
			{
				var visibleList = GetAdditionalExecutionInfo();
				if (SelectedAdditionalExecutionInfo != null && !visibleList.Contains(SelectedAdditionalExecutionInfo))
				{
					visibleList.Add(SelectedAdditionalExecutionInfo);
				}

				return visibleList;
			}
		}
		public string SelectedAdditionalExecutionInfo
		{
			get
			{
				return Document.AdditionalExecutionInfo;
			}
			set
			{
				if (HasAddExecutionInfoList)
				{
					Document.AdditionalExecutionInfo = value;
					OnPropertyChanged("SelectedAdditionalExecutionInfo");
				}
			}
		}

		public string AdditionalExecutionInfo
		{
			get { return Document.AdditionalExecutionInfo; }
			set
			{
				if (Document.AdditionalExecutionInfo != value)
				{
					Document.AdditionalExecutionInfo = value;
					OnPropertyChanged("AdditionalExecutionInfo");
				}
			}
		}


		protected abstract IList<string> GetOriginalStoragePlaces();
		public IList<string> OriginalStoragePlaces
		{
			get
			{
				var visibleList = GetOriginalStoragePlaces();
				if (SelectedOriginalStoragePlace != null && !visibleList.Contains(SelectedOriginalStoragePlace))
				{
					visibleList.Add(SelectedOriginalStoragePlace);
				}

				return visibleList;
			}
		}

		public string SelectedOriginalStoragePlace
		{
			get
			{
				return Document.OriginalStoragePlace;
			}
			set
			{
				Document.OriginalStoragePlace = value;
				OnPropertyChanged("SelectedOriginalStoragePlace");
			}
		}

		private SIDocumentNumberType m_SelectedSIDocumentNumberType = SIDocumentNumberType.Simple;
		public SIDocumentNumberType SelectedSIDocumentNumberType
		{
			get
			{
				return m_SelectedSIDocumentNumberType;
			}
			set
			{
				if (m_SelectedSIDocumentNumberType == value)
					return;
				m_SelectedSIDocumentNumberType = value;
				OnPropertyChanged("SelectedSIDocumentNumberType");
				OnPropertyChanged("IsNumberTypeRegister");
				OnPropertyChanged("IsNumberTypeSimple");
				//if (IncomingNumber == null) return;
				if (SelectedSIDocumentNumberType == SIDocumentNumberType.Simple)// && IncomingNumber.IndexOf(regStr) != -1)
				{
					if (IncomingNumber != null)
					{
						IncomingNumber = string.Empty;//IncomingNumber.Remove(IncomingNumber.IndexOf(regStr), regStr.Length);
					}
					OnPropertyChanged("IncomingNumber");
				}

				if (SelectedSIDocumentNumberType == SIDocumentNumberType.Register)// && !IncomingNumber.StartsWith(regStr.Trim()))
				{
					IncomingNumber = REG_STR;// +IncomingNumber.Trim();
					OnPropertyChanged("IncomingNumber");
				}
			}
		}

		public bool IsNumberTypeRegister
		{
			get
			{
				return SelectedSIDocumentNumberType == SIDocumentNumberType.Register;
			}
			set
			{
				SelectedSIDocumentNumberType = SIDocumentNumberType.Register;
			}
		}

		public bool IsNumberTypeSimple
		{
			get
			{
				return SelectedSIDocumentNumberType == SIDocumentNumberType.Simple;
			}
			set
			{
				SelectedSIDocumentNumberType = SIDocumentNumberType.Simple;
			}
		}

		private readonly List<LinkedDocumentItem> _mDocsToRemove = new List<LinkedDocumentItem>();
		private ObservableCollection<LinkedDocumentItem> _mLinkedDocs;
		public ObservableCollection<LinkedDocumentItem> LDList
		{
			get
			{
				return _mLinkedDocs;
			}
			set
			{
				_mLinkedDocs = value;
				OnPropertyChanged("LDList");
			}
		}



		private IList<LegalEntity> _leList = new List<LegalEntity>();
		public IList<LegalEntity> LEList
		{
			get
			{
				return _leList;
			}
			private set
			{
				_leList = value;
				OnPropertyChanged("LEList");
			}
		}

		private LegalEntity _mSelectedLe;
		public LegalEntity SelectedLE
		{
			get
			{
				return _mSelectedLe;
			}

			set
			{
				_mSelectedLe = value;
				if (value != null)
					Document.LegalEntityID = value.ID;
				OnPropertyChanged("SelectedLE");
			}
		}

		private IList<DocumentClass> _mDocumentClassList = new List<DocumentClass>();
		public IList<DocumentClass> DocumentClassList
		{
			get
			{
				return _mDocumentClassList;
			}

			set
			{
				_mDocumentClassList = value;
				OnPropertyChanged("DocumentClassList");
			}
		}

		private DocumentClass _mSelectedDocumentClass;
		public DocumentClass SelectedDocumentClass
		{
			get { return _mSelectedDocumentClass; }

			set
			{
				_mSelectedDocumentClass = value;
				if (value != null)
					Document.DocumentClassID = value.ID;
				OnPropertyChanged("SelectedDocumentClass");
			}
		}

		//private IList<Person> m_PersonList = new List<Person>();
		//public IList<Person> PersonList
		//{
		//    get
		//    {
		//        return m_PersonList;
		//    }

		//    set
		//    {
		//        m_PersonList = value;
		//        OnPropertyChanged("PersonList");
		//    }
		//}

		private Person _mSelectedPerson;
		public Person SelectedPerson
		{
			get
			{
				return _mSelectedPerson;
			}

			set
			{
				_mSelectedPerson = value;
			    if (value != null)
			    {
                    Document.ExecutorID = value.ID;
			        //Document.ExecutorName = value.LastName;
			    }
			    
				OnPropertyChanged("SelectedPerson");
			}
		}

		public string ExecutorName
		{
			get { return Document.ExecutorName; }
			set
			{
				if (Document.ExecutorName != value)
				{
					Document.ExecutorName = value;
					OnPropertyChanged("ExecutorName");
				}
			}
		}

		public string AddInfoText
		{
			get { return Document.AdditionalInfoText; }
			set
			{
				if (Document.AdditionalInfoText != value)
				{
					Document.AdditionalInfoText = value;
					OnPropertyChanged("AddInfoText");
				}
			}
		}


		private IList<AdditionalDocumentInfo> _mAdditionalInfoList = new List<AdditionalDocumentInfo>();
		public IList<AdditionalDocumentInfo> AdditionalInfoList
		{
			get { return _mAdditionalInfoList; }
			set
			{
				_mAdditionalInfoList = value;
				OnPropertyChanged("AddInfoList");
			}
		}

		private AdditionalDocumentInfo _mSelectedAdditionalInfo;
		public AdditionalDocumentInfo SelectedAdditionalInfo
		{
			get { return _mSelectedAdditionalInfo; }

			set
			{
				_mSelectedAdditionalInfo = value;
				if (value != null)
					Document.AdditionalInfoID = value.ID;
				OnPropertyChanged("SelectedAdditionalInfo");
			}
		}

		private string _mFullFilePath;
		private string FullFilePath
		{
			get { return _mFullFilePath; }
			set
			{
				_mFullFilePath = value;
				if (_mFullFilePath != null)
					SelectedFileName = FileHelper.TrimName(Path.GetFileName(value), 128);
			}
		}

		private string _mSelectedFileName = string.Empty;
		public string SelectedFileName
		{
			get { return _mSelectedFileName; }
			set
			{
				_mSelectedFileName = value;
				OnPropertyChanged("SelectedFileName");
			}
		}

		public List<string> NumbersOfCurrentYear { get; set; }

		#region Commands
		private ICommand selectFile;
		public ICommand SelectFile => selectFile;

	    private ICommand deleteFile;
		public ICommand DeleteFile => deleteFile;

	    private ICommand openAttachment;
		public ICommand OpenAttachment => openAttachment;

	    #endregion

		#region Execute Implementations

		private void ExecuteSelectFile()
		{
			var file = DialogHelper.OpenFile("Все файлы (*.*)|*.*");
			if (!string.IsNullOrWhiteSpace(file) && CheckAttachment(file))
			{
				FullFilePath = file;
			}
		}

		public bool CanExecuteSelectFile()
		{
			return EditAccessAllowed;
		}

		public void ExecuteDeleteFile()
		{
			FullFilePath = null;
			SelectedFileName = null;
		}

		public bool CanExecuteDeleteFile()
		{
			return EditAccessAllowed;
		}

		public void ExecuteOpenAttachment()
		{
			string fullPath;

			if (File.Exists(FullFilePath))
			{
				//если есть выбранный файл - открываем его
				fullPath = FullFilePath;
			}
			else
				if (DocFileBody != null && DocFileBody.FileContentId.HasValue)
				{
					//иначе пытаемся открыть вложение
					var fileContent = DataContainerFacade.GetByID<FileContent>(DocFileBody.FileContentId);
					if (fileContent == null || fileContent.FileContentData == null)
						return;

					//DocFileBody.Body;
					try
					{
						fullPath = Path.Combine(Path.GetTempPath(), DateTime.Now.Ticks + SelectedFileName);
						var fs = new FileStream(fullPath, FileMode.Create, FileAccess.Write);
						fs.Write(fileContent.FileContentData, 0, fileContent.FileContentData.Length);
						fs.Close();
					}
					catch
					{
						return;
					}
				}
				else
					//иначе - ничего
					return;

			try
			{
				Process.Start(fullPath);
			}
			catch
			{
				RaiseAttachmentOpenFailed();
			}
		}

		public bool CanExecuteOpenAttachment()
		{
			return true;
		}

		public bool CheckAttachment(string file)
		{
			if (string.IsNullOrWhiteSpace(file))
				return true;

			var fi = new FileInfo(file);

			if (!fi.Exists)
			{
				DialogHelper.ShowAlert(string.Format("Файл '{0}' не найден", file));
				return false;
			}
		    if (Extensions.Executables.Contains(fi.Extension))
		    {
		        DialogHelper.ShowAlert(string.Format("Недопустимое расширение файла '{0}'", fi.Extension));
		        return false;
		    }
		    if (fi.Length > Sizes.Size4Mb)
		    {
		        RaiseAttachmentSizeBig();
		        return false;
		    }
		    return true;
		}

		public void UpdateAttachment()
		{
			if (string.IsNullOrWhiteSpace(SelectedFileName) && DocFileBody != null)
			{
				try
				{
					BLServiceSystem.Client.DeleteDocFileBodyWithContent(DocFileBody.ID, DocFileBody.FileContentId);
					/* DataContainerFacade.Delete<DocFileBody>(DocFileBody);
					 if (DocFileBody.FileContentId.HasValue)
						 DataContainerFacade.Delete<FileContent>(DocFileBody.FileContentId.Value);*/
				}
				catch
				{
					//RaiseAttachmentDeleteFailed();
				}
				return;
			}

			try
			{
				if (ID <= 0 || !File.Exists(FullFilePath) || !CheckAttachment(FullFilePath)) return;
				var fs = new FileStream(FullFilePath, FileMode.Open, FileAccess.Read);
				if (fs.Length > 10048576)
					return;
				var blob = new byte[fs.Length];
				fs.Read(blob, 0, blob.Length);
				fs.Close();
				//DocFileBody.Body = blob;
				if (DocFileBody == null)
					DocFileBody = new DocFileBody();
				DocFileBody.FileName = SelectedFileName;
				DocFileBody.DocumentID = ID;
				try
				{
					DocFileBody.ID = BLServiceSystem.Client.SaveDocFileBodyForDocument(ID, DocFileBody, blob);
					//DataContainerFacade.Save<DocFileBody>(DocFileBody); ;
				}
				catch
				{
					RaiseAttachmentUploadFailed();
				}
			}
			catch
			{
				RaiseAttachmentUploadFailed();
			}
		}

		private void UpdateNumbersOfCurrentYear(DateTime? newDate = null)
		{
			var dt = newDate ?? (Document.IncomingDate ?? DateTime.Now);
		    NumbersOfCurrentYear = DataContainerFacade.GetListByPropertyConditions<Document>(new List<ListPropertyCondition>
		    {
		        new ListPropertyCondition
		        {
		            Operation = "ge",
		            Name = "IncomingDate",
		            Value = new DateTime(dt.Year, 1, 1)
		        },
		        new ListPropertyCondition
		        {
		            Operation = "lt",
		            Name = "IncomingDate",
		            Value = new DateTime(dt.Year + 1, 1, 1)
		        },
		        new ListPropertyCondition
		        {
		            Operation = "neq",
		            Name = "ID",
		            Value = Document.ID
		        }
		    }).Select(x => x.IncomingNumber).ToList();
		}

		private const string REG_STR = "Реестр ";

		protected override bool BeforeExecuteSaveCheck()
		{
			UpdateNumbersOfCurrentYear();
			if (!string.IsNullOrWhiteSpace(this["IncomingNumber"]))
			{
				OnPropertyChanged("IncomingNumber");
				DialogHelper.ShowAlert(WRONG_INCOMING_NUM_EXISTS);
				return false;
			}

			return base.BeforeExecuteSaveCheck();
		}

		protected override void ExecuteSave()
		{
			if (CanExecuteSave())
			{
				var docs = SelectedSIDocumentNumberType == SIDocumentNumberType.Simple ? BLServiceSystem.Client.GetDocumentsBypropertiesListHib(
					Type, Document.IncomingNumber, Document.IncomingDate) : null;

				switch (State)
				{
					case ViewModelState.Read:
						break;
					case ViewModelState.Edit:
						Document.Type = Type;

						//Сохраняем только ссылку или текстовое имя
						if (HasExecutorList)
							Document.ExecutorName = null;
						else
							Document.ExecutorID = null;

						//Сохраняем только ссылку или текстовое имя
						if (HasAddInfoList)
							AddInfoText = null;
						else
						{
							SelectedAdditionalInfo = null;
							Document.AdditionalInfoID = null;
						}

						//Если исходящий номер не используется - обнуляем его
						if (!HasOutgoingNumber)
							Document.OutgoingNumber = null;

						if (docs != null && (docs.Count > 1 || docs.Count == 1 && docs[0].ID != Document.ID))
						{
							DialogHelper.ShowAlert(string.Format("Документ с входящим номером '{0}' уже существует.", Document.IncomingNumber));
							//throw new ViewModelSaveUserCancelledException();
							return;
						}

						var delBody = string.IsNullOrWhiteSpace(SelectedFileName);
						var idsToRemove = _mDocsToRemove.Where(a => a.Attach != null).Select(a => a.ID).ToList();
						if (BLServiceSystem.Client.SaveAndCleanDocument(Document, idsToRemove, (delBody && DocFileBody != null) ? (long?)DocFileBody.ID : null,
							(delBody && DocFileBody != null) ? DocFileBody.FileContentId : null) == WebServiceDataError.DocumentDeleteError)
							RaiseAttachmentDeleteFailed();
						if (delBody && DocFileBody != null)
							DocFileBody = null;
						UpdateAttachment();

						/* DataContainerFacade.Save<Document, long>(Document);
						 UpdateAttachment();
						 foreach (LinkedDocumentItem item in _mDocsToRemove)
						 {
							 DataContainerFacade.Delete<Attach>(item.Attach);
						 }*/
						break;
					case ViewModelState.Create:
						if (docs != null && docs.Count > 0)
						{
							DialogHelper.ShowAlert(string.Format("Документ с входящим номером '{0}' уже существует.", Document.IncomingNumber));
							return;
							//throw new ViewModelSaveUserCancelledException();
						}
						ID = DataContainerFacade.Save<Document, long>(Document);
						Document.ID = ID;
						UpdateAttachment();
						break;
				}
			}
			OnPropertyChanged("CardSaved");
			OnPropertyChanged("CanDeleteLinkedDocuments");
		}

		public override bool CanExecuteDelete()
		{
			return State != ViewModelState.Create && DeleteAccessAllowed;
		}

		protected override void ExecuteDelete(int delType)
		{
			var attachList = new List<long>();
			var fileContentList = new List<long>();
			if (LDList != null)
				LinqListExtensions.ForEach(LDList, a =>
				{
					if (a.Attach == null) return;
					attachList.Add(a.Attach.ID);
					if (a.Attach.FileContentId.HasValue)
						fileContentList.Add(a.Attach.FileContentId.Value);
				});

			if (_mDocsToRemove != null)
			    LinqListExtensions.ForEach(_mDocsToRemove, a =>
				{
					if (a.Attach == null) return;
					attachList.Add(a.Attach.ID);
					if (a.Attach.FileContentId.HasValue)
						fileContentList.Add(a.Attach.FileContentId.Value);
				});
			BLServiceSystem.Client.DeleteDocument(Document.ID, attachList, fileContentList);

			base.ExecuteDelete(DocOperation.Archive);
			RefreshConnectedViewModels();
		}

		public void DeleteLinkedDocument(LinkedDocumentItem item)
		{
			LDList.Remove(item);
			_mDocsToRemove.Add(item);

			DataContainerFacade.Delete<Attach>(item.ID);

			if (item.Attach != null && item.Attach.FileContentId.HasValue)
				DataContainerFacade.Delete<FileContent>(item.Attach.FileContentId.Value);

			var tmp = IsDataChanged;
			OnPropertyChanged("CanDeleteLinkedDocuments");
			IsDataChanged = tmp;
			JournalLogger.LogModelEvent(this, JournalEventType.DELETE, "Связанный документ", "Документ ID: " + item.ID);

			ViewModelManager.UpdateDataInAllModels(new List<KeyValuePair<Type, long>> { new KeyValuePair<Type, long>(typeof(Attach), item.ID) });
			RefreshConnectedViewModels();
		}

		public override bool CanExecuteSave()
		{
			return Validate() && IsDataChanged;
		}

		#endregion


		protected abstract IList<LegalEntity> GetLegalEntityList();
		protected abstract IList<DocumentClass> GetDocumentClassList();
		//protected abstract IList<Person> GetPersonList();
		protected abstract IList<AdditionalDocumentInfo> GetAdditionalInfoList();

		public abstract bool HasOutgoingNumber { get; }

		public abstract bool HasExecutorList { get; }
		public bool HasAddInfoList => AdditionalInfoList.Count > 0;
	    public bool HasAddExecutionInfoList => AdditionalExecutionInfos.Count > 0;

	    protected DocumentBaseViewModel(long id, ViewModelState state)
		{
			DataObjectTypeForJournal = typeof(Document);
			selectFile = new DelegateCommand(o => CanExecuteSelectFile(), o => ExecuteSelectFile());
			deleteFile = new DelegateCommand(o => CanExecuteDeleteFile(), o => ExecuteDeleteFile());
			openAttachment = new DelegateCommand(o => CanExecuteOpenAttachment(), o => ExecuteOpenAttachment());

			DocumentClassList = GetDocumentClassList();// DataContainerFacade.GetList<DocumentClass>();
			//PersonList = this.GetPersonList();// DataContainerFacade.GetList<FIO>();
			AdditionalInfoList = GetAdditionalInfoList();// DataContainerFacade.GetList<AdditionalDocumentInfo>();
			LEList = GetLegalEntityList();

			if (state == ViewModelState.Create)
			{
				Document = new Document
				{
					Type = Type,
					IncomingDate = DateTime.Now,
					OutgoingDate = DateTime.Now
				};

				UpdateNumbersOfCurrentYear();
				DocFileBody = new DocFileBody();
				SelectedLE = LEList.FirstOrDefault();
				SelectedDocumentClass = DocumentClassList.FirstOrDefault();
			}
			else
			{
				ID = id;
				RefreshLinkedDocuments();
				Document = DataContainerFacade.GetByID<Document, long>(id);
				//если текущий УК документа отсутствует в списке, добавляем его
				var curLe = Document.GetLegalEntity();
				if (LEList.FirstOrDefault(a => a.ID == curLe.ID) == null) LEList.Add(curLe);

				if (Document.IncomingNumber != null && Document.IncomingNumber.ToLower().Contains("реестр"))
				{
					SelectedSIDocumentNumberType = SIDocumentNumberType.Register;
				}
				else
					SelectedSIDocumentNumberType = SIDocumentNumberType.Simple;

				if (Document.ExecutorID.HasValue)
					SelectedPerson = DataContainerFacade.GetByID<Person>(Document.ExecutorID);

				if (Document.DocumentClassID.HasValue)
					SelectedDocumentClass = DocumentClassList.FirstOrDefault(clazz => clazz.ID == Document.DocumentClassID);

				if (Document.LegalEntityID.HasValue)
					SelectedLE = LEList.FirstOrDefault(le => le.ID == Document.LegalEntityID);

				if (Document.AdditionalInfoID.HasValue)
					SelectedAdditionalInfo = AdditionalInfoList.FirstOrDefault(le => le.ID == Document.AdditionalInfoID);

				DocFileBody = Document.GetDocFileBody() ?? new DocFileBody();
				SelectedFileName = DocFileBody.FileName;
				UpdateNumbersOfCurrentYear();
			}
		}

		public void RefreshLinkedDocuments()
		{
			var wasChnaged = IsDataChanged;
			var attaches = BLServiceSystem.Client.GetLinkedDocumentsListHib(ID);
			LDList = new ObservableCollection<LinkedDocumentItem>();
			if (attaches != null)
				foreach (var attach in attaches)
				{
					LDList.Add(new LinkedDocumentItem(attach));
				}
			foreach (var itm in _mDocsToRemove.Select(item => LDList.FirstOrDefault(attach => attach.ID == item.ID)))
			{
				LDList.Remove(itm);
			}
			OnPropertyChanged("LDList");
			OnPropertyChanged("CanDeleteLinkedDocuments");
			IsDataChanged = wasChnaged;
		}

		private bool Validate()
		{
			return
				"IncomingDate|IncomingNumber|SelectedLE|OutgoingDate|OutgoingNumber|SelectedDocumentClass|Comments|ExecutorName|AddInfoText|SelectedAdditionalExecutionInfo".Split('|')
					.All(prop => string.IsNullOrWhiteSpace(this[prop]));
		}

		private const string WRONG_INCOMING_DATE = "Не указана дата поступления";
		private const string WRONG_INCOMING_NUM = "Не указан входящий номер";
		private const string WRONG_INCOMING_UK = "Не указан отправитель";
		private const string WRONG_OUTGOING_DATE = "Не указана дата отправления";
		private const string WRONG_DOC_CLASS = "Не указана классификация документа";
		private const string WRONG_INCOMING_NUM_EXISTS = "Выбранный входящий номер уже задан в выбранном году";
		public override string this[string columnName]
		{
			get
			{
				switch (columnName)
				{
					case "IncomingDate": return IncomingDate == null ? WRONG_INCOMING_DATE : ((OutgoingDate <= IncomingDate) ? null : "Дата отправления не может быть больше даты поступления");
					case "IncomingNumber":
						if (string.IsNullOrEmpty(IncomingNumber))
							return WRONG_INCOMING_NUM;
						if (SelectedSIDocumentNumberType == SIDocumentNumberType.Register && IncomingNumber.IndexOf(REG_STR) != -1)
							return IncomingNumber.Remove(IncomingNumber.IndexOf(REG_STR), REG_STR.Length).ValidateMaxLength(50);
						if (SelectedSIDocumentNumberType == SIDocumentNumberType.Simple && NumbersOfCurrentYear != null && NumbersOfCurrentYear.Any(x => x.ToLower().Trim().Equals(IncomingNumber.ToLower().Trim(), StringComparison.InvariantCultureIgnoreCase)))
							return WRONG_INCOMING_NUM_EXISTS;
						return IncomingNumber.ValidateMaxLength(50);
					case "SelectedLE": return SelectedLE == null ? WRONG_INCOMING_UK : null;
					case "OutgoingDate": return OutgoingDate == null ? WRONG_OUTGOING_DATE : ((OutgoingDate <= IncomingDate) ? null : "Дата отправления не может быть больше даты поступления");
					case "OutgoingNumber": return OutgoingNumber.ValidateMaxLength(50);
					case "SelectedDocumentClass": return SelectedDocumentClass == null ? WRONG_DOC_CLASS : null;
					case "Comments": return Comment.ValidateMaxLength(512);
					case "ExecutorName": return ExecutorName.ValidateMaxLength(512);
					case "AddInfoText": return AddInfoText.ValidateMaxLength(256);
					case "SelectedAdditionalExecutionInfo": return SelectedAdditionalExecutionInfo.ValidateMaxLength(256);
					default:
						return null;
				}
			}
		}

		IList<KeyValuePair<Type, long>> IUpdateRaisingModel.GetUpdatedList()
		{
			return new List<KeyValuePair<Type, long>> { new KeyValuePair<Type, long>(typeof(Document), Document.ID) };
		}
	}
}