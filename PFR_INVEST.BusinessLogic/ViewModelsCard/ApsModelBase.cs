﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
	public class ApsModelBase : ViewModelCard, IUpdateRaisingModel, IUpdateListenerModel
	{
		#region fields

		protected Aps APS;

		public PortfolioIdentifier.PortfolioTypes Type { get; private set; }

		private PortfolioFullListItem _srcAccount;
		/// <summary>
		/// Инвестиционный портфель (счёт)
		/// </summary>
		public PortfolioFullListItem SrcAccount
		{
			get
			{
				return _srcAccount;
			}
			set
			{
				_srcAccount = value;
				if (value != null && APS != null)
				{
					APS.SPFRBankAccountID = value.PfrBankAccount.ID;
					APS.SPortfolioID = value.Portfolio.ID;
				}
				OnPropertyChanged("AccountNumber");
				OnPropertyChanged("Bank");
				OnPropertyChanged("PFName");
				OnPropertyChanged("Account");
				OnPropertyChanged("SrcAccount");
			}
		}

		private PortfolioFullListItem _dstAccount;
		/// <summary>
		/// Целевой портфель (счёт)
		/// </summary>
		public PortfolioFullListItem DstAccount
		{
			get
			{
				return _dstAccount;
			}

			set
			{
				_dstAccount = value;

				if (value != null && APS != null)
				{
					APS.PFRBankAccountID = value.PfrBankAccount.ID;
					APS.PortfolioID = value.Portfolio.ID;
				}

				OnPropertyChanged("AccountNumber");
				OnPropertyChanged("Bank");
				OnPropertyChanged("PFName");
				OnPropertyChanged("DstAccount");
			}
		}

		public decimal? Summ
		{
			get
			{
				return APS == null ? null : APS.Summ;
			}
			set
			{
				if (APS != null)
					APS.Summ = value;
				OnPropertyChanged("Summ");
			}
		}

		public DateTime? OperationDate
		{
			get { return APS == null ? null : APS.OperationDate; }
			set
			{
				if (APS != null)
					APS.OperationDate = value;
				OnPropertyChanged("OperationDate");
			}
		}

		public DateTime? DocDate
		{
			get { return APS == null ? null : APS.DocDate; }
			set
			{
				if (APS != null)
					APS.DocDate = value;
				OnPropertyChanged("DocDate");
			}
		}

		public string RegNum
		{
			get { return APS == null ? null : APS.RegNum; }
			set
			{
				if (APS != null)
					APS.RegNum = value;
				OnPropertyChanged("RegNum");
			}
		}

		public string Comment
		{
			get { return APS == null ? null : APS.Comment; }
			set
			{
				if (APS != null)
					APS.Comment = value;
				OnPropertyChanged("Comment");
			}
		}

		public DateTime MaxDate
		{
			get
			{
				var maxdate = DateTime.MaxValue;

				var cnt = DopAPSs.Count;

				switch (State)
				{
					case ViewModelState.Create:
						return maxdate;
					default:
						if (cnt > 0)
							return GetMaxDate(0);
						break;
				}
				return maxdate;
			}
		}

		private List<DopAps> _dopAPSs = new List<DopAps>();
		/// <summary>
		/// Уточнения к страховым взносам умерших
		/// </summary>
		public List<DopAps> DopAPSs
		{
			get
			{
				return _dopAPSs;
			}
			set
			{
				_dopAPSs = value;

				OnPropertyChanged("DopAPSs");
				IsDataChanged = false;
			}
		}

		//public DopSPNListItem SelectedDop { get; set; }
		public DopAps SelectedDop { get; set; }

		public bool UserRoleType => State == ViewModelState.Read;

	    public bool PeriodEnabled => DopAPSs.Count == 0;

	    #endregion

		#region Execute implementation

		public override bool CanExecuteDelete()
		{
			return State == ViewModelState.Edit;
		}

		protected override void ExecuteDelete(int delType = 1)
		{
			BLServiceSystem.Client.DeleteAPS(ID);
			base.ExecuteDelete(DocOperation.Delete);
		}

		private void ExecuteDeleteDop()
		{
			var tmp = IsDataChanged;
			BLServiceSystem.Client.DeleteDopAPSList(new List<long> { SelectedDop.ID });
			SelectedDop = null;
			// вызов обновления гридов до обновления списка на форме, так как после обновления формы потеряется ID удаленной записи, а ее нужно передать в грид
			ViewModelManager.UpdateDataInAllModels(GetUpdatedList());
			RefreshDops(ID);
			RefreshConnectedViewModels();
			OnPropertyChanged("PeriodEnabled");
			if (!tmp) IsDataChanged = false;
		}

		private bool CanExecuteDeleteDop()
		{
			return SelectedDop != null && DeleteAccessAllowed && !UserRoleType;
		}

		/// <summary>
		/// Открытие диалога выбора инвестиционного портфеля
		/// </summary>
		private void ExecuteSelectSrcAccount()
		{
			//если уже выбран DstAccount и его тип "Без типа"
			var selected = GetValidPortfolioFullListItem();

			if (selected != null)
			{
				SrcAccount = selected;
				if (SrcAccount != null && DstAccount != null)
					ValidatePortfolioTypes(true);
			}
		}

		/// <summary>
		/// Проверка возможности выбора инвестиционного портфеля
		/// </summary>
		/// <returns>true, если выбирать можно</returns>
		private bool CanExecuteSelectSrcAccount()
		{
			return !UserRoleType;
		}

		/// <summary>
		/// Открытие диалога выбора целевого портфеля
		/// </summary>
		private void ExecuteSelectDstAccount()
		{

			//если уже выбран DstAccount и его тип "Без типа"
			var selected = GetValidPortfolioFullListItem();
			if (selected != null)
			{
				DstAccount = selected;
				if (SrcAccount != null && DstAccount != null)
					ValidatePortfolioTypes(true);
			}
		}

		/// <summary>
		/// Проверка возможности выбора целевого портфеля
		/// </summary>
		/// <returns>true, если выбирать можно</returns>
		private bool CanExecuteSelectDstAccount()
		{
			return !UserRoleType;
		}

		protected override void OnCardSaved()
		{
			IsDataChanged = ValidatePortfolioTypes(false);
			//OnPropertyChanged("DocDate");
		}

		public override bool CanExecuteSave()
		{
			return string.IsNullOrEmpty(Validate()) && IsDataChanged;
		}

		protected override bool BeforeExecuteSaveCheck()
		{
			switch (State)
			{

				case ViewModelState.Edit:
				case ViewModelState.Create:
					return !ValidatePortfolioTypes(true);
				default:
					return base.BeforeExecuteSaveCheck();
			}
		}

		protected override void ExecuteSave()
		{
			if (!CanExecuteSave()) return;
			switch (State)
			{
				case ViewModelState.Read:
					break;
				case ViewModelState.Edit:
			        APS.ID = BLServiceSystem.Client.SaveAps(APS, DopAPSs);
					/*DataContainerFacade.Save(APS);
					DopAPSs.ForEach(dop => {
						dop.PFRBankAccountID = APS.PFRBankAccountID;
						dop.PortfolioID = APS.PortfolioID;
						dop.SPFRBankAccountID = APS.SPFRBankAccountID;
						dop.SPortfolioID = APS.SPortfolioID;
						DataContainerFacade.Save(dop);
					});
                    */
					if (_dopAPSs != null && _dopAPSs.Count > 0)
					{
						var li = _dopAPSs[0];
						var summ = Summ ?? 0;
						var diff = summ - (li.Summ ?? 0);
						if ((li.ChSumm.HasValue && li.ChSumm.Value != diff) || !li.ChSumm.HasValue)
						{
							li.ChSumm = summ;
							Load(APS.ID);
						}
					}
					break;
				case ViewModelState.Create:
					APS.ID = DataContainerFacade.Save(APS);
					ID = APS.ID;
					DopAPSs.ForEach(dop =>
					{
						dop.PFRBankAccountID = APS.PFRBankAccountID;
						dop.PortfolioID = APS.PortfolioID;
						dop.SPFRBankAccountID = APS.SPFRBankAccountID;
						dop.SPortfolioID = APS.SPortfolioID;
						DataContainerFacade.Save(dop);
					});
					break;
			}
		}
		#endregion

		#region Validation
		public override string this[string columnName]
		{
			get
			{
				const string errorMessage = "Неверный формат данных";

				if (APS == null)
					return errorMessage;

				switch (columnName.ToUpper())
				{
					case "OPERATIONDATE": return !APS.OperationDate.HasValue || (APS.OperationDate.Value).Year < 1900 ? errorMessage : null;
					case "DOCDATE": return !APS.DocDate.HasValue || (APS.DocDate.Value).Year < 1900 ? errorMessage : null;
					case "REGNUM": return string.IsNullOrWhiteSpace(APS.RegNum) ? errorMessage : null;
					case "SUMM": return !APS.Summ.HasValue ||
						(APS.Summ.Value >= (decimal)Math.Pow(10, 15) || APS.Summ.Value <= -(decimal)Math.Pow(10, 15)) ? errorMessage : null; //|| this.aps.Summ.Value <= 0
					default: return null;
				}
			}
		}

		bool ValidatePortfolioTypes(bool showDialog)
		{
			switch (Type)
			{
			    case PortfolioIdentifier.PortfolioTypes.ALL:
			        return false;
			    case PortfolioIdentifier.PortfolioTypes.DSV:
			    case PortfolioIdentifier.PortfolioTypes.NOT_SPN:

			        var isSrcType = SrcAccount != null && SrcAccount.PFType == Portfolio.Types.DSV;
			        var isDstType = DstAccount != null && DstAccount.PFType == Portfolio.Types.DSV; 


			        var isInvalid = !isSrcType && !isDstType;
			        if (isInvalid && showDialog)
			            DialogHelper.ShowAlert(string.Concat("Хотя бы один выбранный портфель должен иметь тип ", PortfolioIdentifier.DSV));
			        return isInvalid;
			}
		    return false;
		}

		private string Validate()
		{
			const string errorMessage = "Неверный формат данных";
			const string validate = "OPERATIONDATE|DOCDATE|REGNUM|SUMM";

			if ((SrcAccount == null) || (DstAccount == null))
				return errorMessage;

			return validate.Split('|').Any(idx => !string.IsNullOrEmpty(this[idx])) ? errorMessage : null;
		}
		#endregion

		#region Commands

		public ICommand DeleteDop { get; private set; }

		private readonly ICommand _selectSrcAccount;
		public ICommand SelectSrcAccount => _selectSrcAccount;

	    private readonly ICommand _selectDstAccount;
		public ICommand SelectDstAccount => _selectDstAccount;

	    #endregion

		private DateTime GetDateByIndex(int idx)
		{
			if (idx >= DopAPSs.Count)
				return DateTime.Now;

			return DopAPSs[idx].Date ?? DateTime.Now;
		}

		private DateTime GetMaxDate(int idx)
		{
			var maxdate = GetDateByIndex(idx);

			if (!DocDate.HasValue)
				return maxdate.Date;

			var compare = DateTime.Compare(DocDate.Value, maxdate);
			return compare > 0 ? DocDate.Value.Date : maxdate.Date;
		}

		public void RefreshDops(long id)
		{
			DopAPSs = DataContainerFacade.GetListByProperty<DopAps>("ApsID", id).ToList();
		}

		/// <summary>
		/// Загрузка данных об страховых взносах умерших
		/// </summary>
		/// <param name="lId">ID страховых взносов умерших в базе</param>
		public void Load(long lId)
		{
			ID = lId;
			try
			{
				APS = DataContainerFacade.GetByID<Aps>(lId);

				if (APS == null)
					return;

				var srcType = Portfolio.Types.NoType;
				var dstType = Portfolio.Types.NoType;

				if (APS.SPFRBankAccountID.HasValue && APS.SPortfolioID.HasValue)
				{
					var srcAccID = APS.SPFRBankAccountID.Value;
					var srcPFID = APS.SPortfolioID.Value;
					SrcAccount = BLServiceSystem.Client.GetPortfolioFull(srcAccID, srcPFID);

					if (SrcAccount != null && SrcAccount.Portfolio != null)
					{
						srcType = SrcAccount.PFType;						
					}
				}
				if (APS.PFRBankAccountID.HasValue && APS.PortfolioID.HasValue)
				{
					var dstAccID = APS.PFRBankAccountID.Value;
					var dstPFID = APS.PortfolioID.Value;
					DstAccount = BLServiceSystem.Client.GetPortfolioFull(dstAccID, dstPFID);

					if (DstAccount != null && DstAccount.Portfolio != null)						
					{
						dstType = DstAccount.PFType;
						
					}
				}

				switch (srcType)
				{
				    case Portfolio.Types.SPN:
				        Type = dstType == Portfolio.Types.DSV ? PortfolioIdentifier.PortfolioTypes.ALL : PortfolioIdentifier.PortfolioTypes.NOT_DSV;
				        break;
				    case Portfolio.Types.DSV:
				        Type = dstType == Portfolio.Types.SPN ? PortfolioIdentifier.PortfolioTypes.ALL : PortfolioIdentifier.PortfolioTypes.NOT_SPN;
				        break;
				}
				

				RefreshDops(lId);
			}
			catch (Exception e)
			{
				if (DoNotThrowExceptionOnError == false)
					throw;

				Logger.WriteException(e);
				throw new ViewModelInitializeException();
			}
		}



		public ApsModelBase(//Type t, 
			PortfolioIdentifier.PortfolioTypes pType)
			//: base(t)
		{
			Type = pType;
            DataObjectTypeForJournal = typeof(Aps);

			_selectSrcAccount = new DelegateCommand(o => CanExecuteSelectSrcAccount(), o => ExecuteSelectSrcAccount());

			_selectDstAccount = new DelegateCommand(o => CanExecuteSelectDstAccount(), o => ExecuteSelectDstAccount());

			DeleteDop = new DelegateCommand(o => CanExecuteDeleteDop(), o => ExecuteDeleteDop());

		    APS = new Aps
		    {
		        DocDate = DateTime.Now,
		        OperationDate = DateTime.Now
		    };
		}

		/// <summary>
		/// Выбор портфеля 2 типов
		/// </summary>
		private PortfolioFullListItem GetValidPortfolioFullListItem()
		{
			var types = new List<Portfolio.Types>();
			switch (Type)
			{
			    case PortfolioIdentifier.PortfolioTypes.NOT_DSV:
					return DialogHelper.SelectPortfolio(true, true, Portfolio.Types.DSV);
			        // break;
			    case PortfolioIdentifier.PortfolioTypes.NOT_SPN:
					return DialogHelper.SelectPortfolio(true, true, Portfolio.Types.SPN);					
			        //break;
			    case PortfolioIdentifier.PortfolioTypes.ALL:
				default:
					return DialogHelper.SelectPortfolio(true);
			        //break;
			}			
		}


		public IList<KeyValuePair<Type, long>> GetUpdatedList()
		{
		    var list = new List<KeyValuePair<Type, long>> {new KeyValuePair<Type, long>(typeof (Aps), APS.ID)};
		    DopAPSs.ForEach(dop => 
			{
				list.Add(new KeyValuePair<Type, long>(typeof(DopAps), dop.ID));
			});
			return list;
		}

		Type[] IUpdateListenerModel.UpdateListenTypes => new[] { typeof(Aps), typeof(DopAps) };

	    void IUpdateListenerModel.OnDataUpdate(Type type, long id)
		{
			if (type == typeof(Aps))
			{
				if (APS != null && id == APS.ID)
					Load(id);
			}
			else if (type == typeof(DopAps))
			{
				// из-за такого условия может не обрабатываться ситуация с добавлением уточнения
				//if (this.DopAPSs.Any(dop => dop.ID == id))
				if (APS != null)
					RefreshDops(APS.ID);
			}
		}
	}
}
