﻿using PFR_INVEST.Auth.SharedData.Auth;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.User)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
    public class PrepaymentAccurateViewModel : ApsAccurateModelBase
	{
		public PrepaymentAccurateViewModel(PrepaymentViewModel parentVM)
			: base(parentVM
            //, typeof(DueListViewModel)
            )
		{
			Dopaps.DocKind = 1; // потому что уточнение авансового платежа
		}
	}
}
