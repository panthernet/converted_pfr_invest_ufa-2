﻿// -----------------------------------------------------------------------
// <copyright file="RatingAgenciesListModel.cs" company="">
// 
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsCard
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_directory_editor)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_manager)]
    public class RatingAgencyViewModel : ViewModelCard
    {
        private readonly IList<LegalEntity> _banks = BLServiceSystem.Client.GetBankListForDeposit();
        private readonly Dictionary<long, List<MultiplierRating>> _ratingBankMapping = new Dictionary<long, List<MultiplierRating>>();

        #region Properties
        private RatingAgency _ratingAgency;
        public RatingAgency RatingAgency
        {
            get { return _ratingAgency; }
            set
            {
                _ratingAgency = value;
                OnPropertyChanged("RatingAgency");
            }
        }

        public string Name
        {
            get { return _ratingAgency.Name; }
            set
            {
                _ratingAgency.Name = value;
                OnPropertyChanged("Name");
            }
        }

        private List<String> _agencyTypes;
        public List<String> AgencyTypes
        {
            get { return _agencyTypes; }
            set { _agencyTypes = value; OnPropertyChanged("AgencyTypes"); }
        }

        public String SelectedAgencyType
        {
            get
            {
                if (String.IsNullOrEmpty(RatingAgency.AgencyType)) RatingAgency.AgencyType = "Международное";
                return RatingAgency.AgencyType;
            }
            set
            {
                if (value == null)
                    throw new Exception("Тип рейтингового агенства невозможно сбросить в NULL");

                if (!string.IsNullOrWhiteSpace(RatingAgency.AgencyType) && RatingAgency.AgencyType.Equals(value))
                    return;

                RatingAgency.AgencyType = value;
                OnPropertyChanged("SelectedAgencyType");
            }
        }
        private const string INVALID_RATINGAGENCY_NAME = "Не введено наименование рейтингового агентства";
        private const string ALREADY_EXISTS_RATINGAGENCY_NAME = "Рейтинговое агентство с таким именем уже существует";
        private const string CAPTION = "Отказ в выполнении операции";
        #endregion

        #region конструкторы
        public RatingAgencyViewModel()
            : base(typeof(RatingAgenciesListViewModel))
        {
            DataObjectTypeForJournal = typeof(RatingAgency);
            _ratingAgency = new RatingAgency();
            Register();
        }

        public RatingAgencyViewModel(Type pListViewModel)
            : base(pListViewModel)
        {
            DataObjectTypeForJournal = typeof(RatingAgency);
            _ratingAgency = new RatingAgency();
            Register();
        }

        public RatingAgencyViewModel(IEnumerable<Type> pListViewModelCollection)
            : base(pListViewModelCollection)
        {
            DataObjectTypeForJournal = typeof(RatingAgency);
            _ratingAgency = new RatingAgency();
            Register();
        }

        public RatingAgencyViewModel(long identity)
            : base(typeof(RatingAgenciesListViewModel))
        {
            DataObjectTypeForJournal = typeof(RatingAgency);
            foreach (var bank in _banks)
            {
                _ratingBankMapping.Add(bank.ID, BLServiceSystem.Client.GetMultiplierRatingByLegalEntityID(bank.ID).ToList());
            }

            if (identity > 0)
            {
                ID = identity;
                _ratingAgency = DataContainerFacade.GetByID<RatingAgency>(identity);
            }
            else
            {
                _ratingAgency = new RatingAgency();
            }

            Register();
        }



        void Register()
        {

            AgencyTypes = new List<string>();
            foreach (var ratingAgencyType in RatingAgencyIdentifier.RatingAgencyTypes)
            {
                AgencyTypes.Add(ratingAgencyType.Value);
            }

            _ratingAgency.PropertyChanged += (sender, e) => { IsDataChanged = true; };
        }

        #endregion

        #region Execute implementation
        protected override void ExecuteSave()
        {
            if (!CanExecuteSave()) return;
            switch (State)
            {
                case ViewModelState.Read:
                    break;
                case ViewModelState.Edit:
                case ViewModelState.Create:
                    _ratingAgency.Name = _ratingAgency.Name.Trim();
                    if (_ratingAgency.Name.Equals(string.Empty))
                    {
                        DialogHelper.ShowWarning(INVALID_RATINGAGENCY_NAME, CAPTION);
                        return;
                    }
                    var list = BLServiceSystem.Client.GetRatingAgenciesList();

                    if (State == ViewModelState.Create
                       && list.Any(l => l.Name.ToLower().Equals(_ratingAgency.Name.ToLower())))
                    {
                        DialogHelper.ShowWarning(ALREADY_EXISTS_RATINGAGENCY_NAME, CAPTION);
                        return;
                    }
                    if (State == ViewModelState.Edit
                        && list.Any(l => l.Name.ToLower().Equals(_ratingAgency.Name.ToLower()) && l.ID != _ratingAgency.ID))
                    {
                        DialogHelper.ShowWarning(ALREADY_EXISTS_RATINGAGENCY_NAME, CAPTION);
                        return;
                    }
                    ID = _ratingAgency.ID = DataContainerFacade.Save(_ratingAgency);
                    break;
            }
        }

        public override bool CanExecuteSave()
        {
            return Validate() && IsDataChanged;
        }

        public override bool CanExecuteDelete()
        {
            return State != ViewModelState.Create && DeleteAccessAllowed & !IsUsed();
        }

        private bool? _isUsed;
        private bool IsUsed()
        {
            if (!_isUsed.HasValue)
            {
                var le = BLServiceSystem.Client.GetBankListForDeposit();
                _isUsed = le.Any(legalEntity => _ratingBankMapping[legalEntity.ID].Any(r => r.AgencyID == RatingAgency.ID));
            }

            return _isUsed.Value;
        }


        public override string this[string columnName] => "";

        public override bool BeforeExecuteDeleteCheck()
        {
            var le = BLServiceSystem.Client.GetBankListForDeposit();
            _isUsed = le.Any(legalEntity => _ratingBankMapping[legalEntity.ID].Any(r => r.AgencyID == RatingAgency.ID));
            if (!_isUsed.Value) return true;
            DialogHelper.ShowError("Выбранное рейтинговое агентство имеет привязки к рейтингам и не может быть удалено");
            return false;
        }

        protected override void ExecuteDelete(int delType)
        {
            DataContainerFacade.Delete(_ratingAgency);
            base.ExecuteDelete(DocOperation.Delete);
        }
        #endregion

        bool Validate()
        {

            return !String.IsNullOrEmpty(_ratingAgency.Name) && !String.IsNullOrEmpty(_ratingAgency.AgencyType);
        }

        public void RefreshRatingBankMapping()
        {
            _ratingBankMapping.Clear();
            foreach (var bank in _banks)
            {
                _ratingBankMapping.Add(bank.ID, BLServiceSystem.Client.GetMultiplierRatingByLegalEntityID(bank.ID).ToList());
            }
        }
    }
}
