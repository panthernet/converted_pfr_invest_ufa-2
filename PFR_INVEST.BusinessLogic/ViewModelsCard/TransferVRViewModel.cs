﻿using PFR_INVEST.Auth.SharedData.Auth;

namespace PFR_INVEST.BusinessLogic.ViewModelsCard
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_manager)]

    public class TransferVRViewModel : TransferViewModel
    {
        public TransferVRViewModel(long id, ViewModelState state) : base(id, state) { }
    }
}
