﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.BusinessLogic.Commands;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager)]
    public class ZLRedistViewModel : ViewModelCard
    {
#if WEBCLIENT
        protected long FzId;

        public static List<ERZLNotifiesListItem> GetNotices(long id)
        {
            return BLServiceSystem.Client.GetERZLNotifiesListByERZL_ID(id);
        }
#endif
        private ERZL _erzl;

        #region Company

        [Required]
        [DisplayName("Кампания")]
        [StringLength(64)]
        public string Company
        {
            get {
                return _erzl != null ? _erzl.Company : "";
            }

            set
            {
                if (_erzl == null) return;
                _erzl.Company = value;
                OnPropertyChanged("Company");
            }
        }
        #endregion

        #region NotifiesCount
        [DisplayName("Количество уведомлений ЕРЗЛ")]
        public int NotifiesCount => _notifiesList?.Count ?? 0;

        #endregion

        #region FocusedERZLNotify
        private ERZLNotifiesListItem _focusedERZLNotify;
        public ERZLNotifiesListItem FocusedERZLNotify
        {
            get
            {
                return _focusedERZLNotify;
            }

            set
            {
                var isdatachanged = IsDataChanged;
                _focusedERZLNotify = value;
                OnPropertyChanged("FocusedERZLNotify");
                IsDataChanged = isdatachanged;
            }
        }
        #endregion

        #region NotifiesList
        private List<ERZLNotifiesListItem> _notifiesList = new List<ERZLNotifiesListItem>();
        public List<ERZLNotifiesListItem> NotifiesList
        {
            get
            {
                return _notifiesList;
            }

            set
            {
                _notifiesList = value;
                OnPropertyChanged("NotifiesList");
                OnPropertyChanged("NotifiesCount");
            }
        }
        #endregion

        #region SelectedFZ
        private FZ _selectedFz;
        public FZ SelectedFZ
        {
            get
            {
                return _selectedFz;
            }

            set
            {
                _selectedFz = value;
                if (_erzl != null && _selectedFz != null)
                    _erzl.FZ_ID = _selectedFz.ID;
                OnPropertyChanged("SelectedFZ");
            }
        }
        #endregion

        #region FZList
        private List<FZ> _fzList;
        public List<FZ> FZList
        {
            get
            {
                return _fzList;
            }

            set
            {
                _fzList = value;
                OnPropertyChanged("FZList");
            }
        }
        #endregion

        #region Date

        public DateTime? Date
        {
            get
            {
                return _erzl?.Date;
            }

            set
            {
                if (_erzl == null) return;
                _erzl.Date = value;
                OnPropertyChanged("Date");
            }
        }
        #endregion

        #region Commands

        public ICommand AddNotify { get; }

        public ICommand EditNotify { get; }

        public ICommand DeleteNotify { get; }

        #endregion

        #region Execute Implementations
        public override bool CanExecuteSave()
        {
            return string.IsNullOrEmpty(Validate()) && IsDataChanged;
        }

        protected override void ExecuteSave()
        {
            if (CanExecuteSave())
            {
                switch (State)
                {
                    case ViewModelState.Read:
                        break;
                    case ViewModelState.Edit:
                    case ViewModelState.Create:
                        _erzl.Content = _erzl.Company != null ? $"Перераспределение СПН (кампания {_erzl.Company})" : "";

#if WEBCLIENT
                        _erzl.FZ_ID = FzId;
#endif
                        if (State == ViewModelState.Edit)
                        {
                            DataContainerFacade.Save<ERZL, long>(_erzl);
                        }
                        else
                        {
                            _erzl.ID = DataContainerFacade.Save<ERZL, long>(_erzl);
                            ID = _erzl.ID;
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        private bool CanExecuteAddNotify()
        {
            return _erzl != null && _erzl.ID > 0 && !EditAccessDenied;
        }

        private bool CanExecuteDeleteNotify()
        {
            return _focusedERZLNotify != null && !EditAccessDenied;
        }

        private void ExecuteDeleteNotify()
        {
            var list = BLServiceSystem.Client.GetERZLNotifiesHib(_erzl.ID, _focusedERZLNotify.NotifyNum);
            if (list != null)
            {
                var deletedItems = new StringBuilder();
                foreach (var obj in list)
                {
                    var item = BLServiceSystem.Client.GetERZLNotifyHib(obj.ID);
                    deletedItems.Append(item.ID).Append(", ");
                    DataContainerFacade.Delete(item);                    
                }
                JournalLogger.LogEvent("Перераспределения ЗЛ", JournalEventType.DELETE, ID, "Удаление НПФ",
                    $"НПФ Id: {_erzl.ID}. Записи ERZLNotify Ids: {deletedItems}");
                RefreshNotifiesList();
            }
            _focusedERZLNotify = null;
        }

        public override bool CanExecuteDelete()
        {
            return State != ViewModelState.Create;
        }

        public override string CustomDeleteMessage => "При удалении Перераспределения ЕРЗЛ будут так же удалены сопутствующие Реестры с Финреестрами. Продолжить?";

        protected override void ExecuteDelete(int delType)
        {
            var list = BLServiceSystem.Client.GetERZLNotifiesHib(_erzl.ID, null);
            foreach (var obj in list)
                DataContainerFacade.Delete(obj);

            var register = DataContainerFacade.GetListByProperty<Register>("ERZL_ID", _erzl.ID);
            foreach (var obj in register)
            {
                var fin = obj.GetFinregistersList();
                foreach (var item in fin)
                {
					foreach (var finTr in DataContainerFacade.GetListByProperty<AsgFinTr>("FinregisterID", item.ID))
					{
						//Отмена коммента - сделали маркируемым!
                        //Удаляем связь на финреестр, так как сущность ПП не удаляется, а маркируется удалённой
						//А финреестр удаляется физически
						//finTr.FinregisterID = null; 
						DataContainerFacade.Delete(finTr);
					}
                    DataContainerFacade.Delete(item);
                }
                //Удаляем связь реестра с ERZL т.к. оно еще не архивируется
                obj.ERZL_ID = null;
                DataContainerFacade.Delete(obj);
            }

            DataContainerFacade.Delete(_erzl);
            base.ExecuteDelete(DocOperation.Delete);
        }

        private void ExecuteAddNotify()
        {
            if (DialogHelper.AddNotify(_erzl.ID, _notifiesList))
                RefreshNotifiesList();
        }

        private static bool CanExecuteEditNotify()
        {
            return true;
        }

        private void ExecuteEditNotify()
        {
            if (DialogHelper.EditNotify(_erzl.ID, _notifiesList, _focusedERZLNotify))
                RefreshNotifiesList();
        }
        #endregion

        public ZLRedistViewModel()
            : base(typeof(ZLRedistListViewModel))
        {
            DataObjectTypeForJournal = typeof(ERZL);
            _erzl = new ERZL();
#if !WEBCLIENT
            AddNotify = new DelegateCommand(o => CanExecuteAddNotify(),
                o => ExecuteAddNotify());

            DeleteNotify = new DelegateCommand(o => CanExecuteDeleteNotify(),
                o => ExecuteDeleteNotify());

            EditNotify = new DelegateCommand(o => CanExecuteEditNotify(),
                o => ExecuteEditNotify());
#endif

            RefreshFZList();

            Date = DateTime.Now;
        }

        public void LoadZLRedist(long id)
        {
            try
            {
                ID = id;
                _erzl = DataContainerFacade.GetByID<ERZL, long>(id);

                RefreshNotifiesList();

                if (_fzList != null && _erzl != null)
                {
                    _selectedFz = _fzList.Find(fz => fz.ID == _erzl.FZ_ID);
#if WEBCLIENT
                    FzId = _selectedFz?.ID ?? 0;
#endif
                }
            }
            catch (Exception e)
            {
                if (DoNotThrowExceptionOnError == false)
                    throw;

                Logger.WriteException(e);
                throw new ViewModelInitializeException();
            }
        }

        public void RefreshNotifiesList()
        {
            var isdatachanged = IsDataChanged;

            try
            {
                _notifiesList = BLServiceSystem.Client.GetERZLNotifiesListByERZL_ID(_erzl.ID);
            }
            catch
            {
                _notifiesList = null;
            }

            OnPropertyChanged("NotifiesList");
            OnPropertyChanged("NotifiesCount");

            IsDataChanged = isdatachanged;
        }

        public void RefreshFZList()
        {
            _fzList = DataContainerFacade.GetList<FZ>();
#if WEBCLIENT
            if (FzId == 0) FzId = _fzList.FirstOrDefault()?.ID ?? 0;
#endif
            OnPropertyChanged("FZList");
        }

#region Validation
        public override string this[string columnName]
        {
            get
            {
                const string errorMessage = "Неверный формат данных";

                switch (columnName)
                {
                    case "Company": return string.IsNullOrEmpty(Company) ? errorMessage : Company.ValidateMaxLength(64);
                    case "Date": return Date == null || Date == DateTime.MinValue ? errorMessage : null;
#if WEBCLIENT
                    case "SelectedFZ": return FzId == 0 ? errorMessage : null;

#else
                    case "SelectedFZ": return SelectedFZ == null ? errorMessage : null;
#endif
                    default: return null;
                }
            }
        }

        private string Validate()
        {
            //const string errorMessage = "Неверный формат данных";

            const string fieldNames =
               "Company|Date|SelectedFZ";

            return fieldNames.Split("|".ToCharArray()).Any(fieldName => !string.IsNullOrEmpty(this[fieldName])) ? "Неверный формат данных" : null;
        }
#endregion
    }
}
