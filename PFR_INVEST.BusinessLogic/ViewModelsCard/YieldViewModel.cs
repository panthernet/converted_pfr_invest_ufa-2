﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataAccess.Client.ObjectsExtensions;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    //[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    //[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    //[DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_manager)]
    abstract public class YieldViewModel : ViewModelCard
    {
        private McProfitAbility _mProfit;

        #region QuartersList
        private List<long> quartersList;
        public List<long> QuartersList
        {
            get
            {
                return quartersList;
            }
            set
            {
                quartersList = value;
                OnPropertyChanged("QuartersList");
            }
        }

        public long? SelectedQuarter
        {
            get
            {
                return _mProfit.Quarter;
            }
            set
            {
                _mProfit.Quarter = value;
                OnPropertyChanged("SelectedQuarter");
                RefreshUKList();
            }
        }
        #endregion

        #region YearsList
        private List<Year> m_YearsList;
        public List<Year> YearsList
        {
            get
            {
                return m_YearsList;
            }
            set
            {
                m_YearsList = value;
                OnPropertyChanged("YearsList");
            }
        }

        private Year m_SelectedYear;
        public Year SelectedYear
        {
            get
            {
                return m_SelectedYear;
            }
            set
            {

                if (m_SelectedYear == value) return;
                m_SelectedYear = value;
                _mProfit.YearId = value.ID;

                OnPropertyChanged("SelectedYear");

                RefreshUKList();
            }
        }
        #endregion

        #region UKList

        private LegalEntity s_UK;


        private void RefreshUKList()
        {

            if (State != ViewModelState.Create || SelectedYear == null || SelectedQuarter == null)
                return;

            UKList = BLServiceSystem.Client.GetValidUKListByMcProfitAbilityHib((int)documentType,
                _mProfit.YearId.Value,
                _mProfit.Quarter.Value);

            if (UKList == null || !UKList.Any())
            {
                SelectedUK = null;
                DialogHelper.ShowAlert(string.Format("На {0} квартал {1} года нет доступных УК.", SelectedQuarter.Value, SelectedYear.Name));
                return;
            }

            if (s_UK == null)
            {
                SelectedUK = UKList.FirstOrDefault();
            }
            else
            {
                var suk = UKList.FirstOrDefault(uk => uk.ID == s_UK.ID);
                SelectedUK = suk ?? UKList.FirstOrDefault();
            }

            s_UK = SelectedUK;

        }
        private void RefreshOnEdit()
        {
            if (State == ViewModelState.Edit && SelectedContract != null && SelectedUK != null)
                RefreshMCProfitHistoryList();
        }

        private List<LegalEntity> m_UKList;
        public List<LegalEntity> UKList
        {
            get
            {
                return m_UKList;
            }
            set
            {
                m_UKList = value;
                OnPropertyChanged("UKList");
            }
        }


        private LegalEntity m_SelectedUK;
        public LegalEntity SelectedUK
        {
            get
            {
                return m_SelectedUK;
            }
            set
            {
                if (m_SelectedUK == value) return;
                m_SelectedUK = value;
                if (State == ViewModelState.Edit) return;


                ContractsList = m_SelectedUK == null ? new List<Contract>() : BLServiceSystem.Client.
                    GetValidContractListByMcProfitAbilityHib((int)documentType,
                    m_SelectedUK.ID,
                    SelectedYear.ID,
                    SelectedQuarter.Value);



                SelectedContract = ContractsList.FirstOrDefault();


                RefreshOnEdit();
                OnPropertyChanged("UKFullName");
                OnPropertyChanged("UKINN");
                OnPropertyChanged("SelectedUK");
            }
        }
        #endregion

        #region ContractsList

        private List<Contract> m_ContractsList;
        public List<Contract> ContractsList
        {
            get
            {
                return m_ContractsList;
            }
            set
            {
                m_ContractsList = value;
                SelectedContract = m_ContractsList == null ? null : m_ContractsList.FirstOrDefault();
                OnPropertyChanged("ContractsList");
            }
        }

        private Contract m_SelectedContract;
        public Contract SelectedContract
        {
            get
            {
                return m_SelectedContract;
            }
            set
            {
                m_SelectedContract = value;
                if (m_SelectedContract != null)
                    _mProfit.ContractId = m_SelectedContract.ID;
                else
                    _mProfit.ContractId = null;
                RefreshOnEdit();
                OnPropertyChanged("ContractDate");
                OnPropertyChanged("ContractPortfolioName");
                OnPropertyChanged("SelectedContract");
            }
        }
        #endregion

        #region Properties
        public string UKFullName
        {
            get
            {
                if (SelectedUK != null)
                    return SelectedUK.FullName;
                return null;
            }
        }

        public string UKINN
        {
            get
            {
                if (SelectedUK != null)
                    return SelectedUK.INN;
                return null;
            }
        }

        public string ContractPortfolioName
        {
            get
            {
                if (SelectedContract != null)
                    return SelectedContract.PortfolioName;
                return null;
            }
        }

        public DateTime? ContractDate
        {
            get
            {
                if (SelectedContract != null)
                    return SelectedContract.ContractDate;
                return null;
            }
        }

        public DateTime? ProfitDate
        {
            get
            {
                return _mProfit.Date;
            }
            set
            {
                _mProfit.Date = value;
                OnPropertyChanged("ProfitDate");
            }
        }

        private bool m_ProfitAbilityChanged;
        public decimal ProfitAbility
        {
            get
            {
                return _mProfit.ProfitAbility ?? 0;
            }
            set
            {
                _mProfit.ProfitAbility = value;
                OnPropertyChanged("ProfitAbility");
                m_ProfitAbilityChanged = true;
            }
        }

        public decimal ProfitAbility12
        {
            get
            {
                return _mProfit.ProfitAbility12 ?? 0;
            }
            set
            {
                _mProfit.ProfitAbility12 = value;
                OnPropertyChanged("ProfitAbility12");
                m_ProfitAbilityChanged = true;
            }
        }

        public decimal ProfitAbility3
        {
            get
            {
                return _mProfit.ProfitAbility3 ?? 0;
            }
            set
            {
                _mProfit.ProfitAbility3 = value;
                OnPropertyChanged("ProfitAbility3");
                m_ProfitAbilityChanged = true;
            }
        }

        public decimal ProfitAbilityContract
        {
            get
            {
                return _mProfit.ProfitAbilityContract ?? 0;
            }
            set
            {
                _mProfit.ProfitAbilityContract = value;
                OnPropertyChanged("ProfitAbilityContract");
                m_ProfitAbilityChanged = true;
            }
        }

        public string Comment
        {
            get
            {
                return _mProfit.Comment ?? "рассчитано на...";
            }
            set
            {
                _mProfit.Comment = value;
                OnPropertyChanged("Comment");
            }
        }

        public bool IsCreate => State == ViewModelState.Create;

        #endregion

        private void RefreshMCProfitHistoryList()
        {
            try
            {
                bool alreadyChanged = IsDataChanged;
                MCProfitHistoryList = BLServiceSystem.Client.GetMCProfitHistoryList(SelectedYear.ID, SelectedQuarter.Value, SelectedContract.ID, ID);
                IsDataChanged = alreadyChanged;
            }
            catch (Exception e)
            {
                HandleException(e);
            }
        }

        private List<McProfitAbility> _mMCProfitHistoryList;
        public List<McProfitAbility> MCProfitHistoryList
        {
            get
            {
                return _mMCProfitHistoryList;
            }
            set
            {
                _mMCProfitHistoryList = value;
                OnPropertyChanged("MCProfitHistoryList");
            }
        }

        public override bool CanExecuteDelete()
        {
            return State != ViewModelState.Create;
        }

        protected override void ExecuteDelete(int delType)
        {
            McProfitAbility mp = DataContainerFacade.GetByID<McProfitAbility>(ID);
            BLServiceSystem.Client.DeleteMcProfit(mp.YearId.Value, mp.Quarter.Value, mp.ContractId.Value);
            base.ExecuteDelete(DocOperation.Delete);
        }

        public Document.Types documentType { get; private set; }

        protected YieldViewModel(ViewModelState action, long id, Document.Types documentType)
            : base(typeof(YieldsListViewModel),
            typeof(YieldsSIListViewModel),
            typeof(YieldsVRListViewModel))
        {
            DataObjectTypeForJournal = typeof(McProfitAbility);
            this.documentType = documentType;
            YearsList = DataContainerFacade.GetList<Year>();
            QuartersList = new List<long>(new long[] { 1, 2, 3, 4 });
            State = action;

            if (State == ViewModelState.Create)
            {
                _mProfit = new McProfitAbility
                {
                    ProfitAbility = 0,
                    ProfitAbility12 = 0,
                    ProfitAbility3 = 0,
                    ProfitAbilityContract = 0

                };
                //SelectedQuarter = QuartersList.First();
                //SelectedYear = YearsList.First();

            }
            else
            {
                ID = id;
                _mProfit = DataContainerFacade.GetByID<McProfitAbility, long>(id);
                SelectedYear = YearsList.FirstOrDefault(y => y.ID == _mProfit.YearId);
                OnPropertyChanged(nameof(SelectedQuarter));
                Contract contract = _mProfit.GetContract();
                LegalEntity legalEntity = contract.GetLegalEntity();

                UKList = new List<LegalEntity> { legalEntity };
                ContractsList = new List<Contract> { contract };
                SelectedUK = legalEntity;
                SelectedContract = contract;
            }

            _mProfit.PropertyChanged += (sender, e) => { IsDataChanged = true; };            
        }


       
        private bool Validate()
        {
			return ValidateFields();

           
        }

        public override bool CanExecuteSave()
        {
            return IsDataChanged && Validate();
        }

        protected override void ExecuteSave()
        {
            switch (State)
            {
                case ViewModelState.Read:
                    break;
                case ViewModelState.Edit:
                    if (m_ProfitAbilityChanged && DialogHelper.ConfirmHistoryChanged())
                    {
                        _mProfit = new McProfitAbility
                        {
                            Comment = _mProfit.Comment,
                            ContractId = _mProfit.ContractId,
                            Date = _mProfit.Date,
                            ProfitAbility = _mProfit.ProfitAbility,
                            ProfitAbility3 = _mProfit.ProfitAbility3,
                            ProfitAbility12 = _mProfit.ProfitAbility12,
                            Quarter = _mProfit.Quarter,
                            YearId = _mProfit.YearId,
                            ProfitAbilityContract = _mProfit.ProfitAbilityContract
                        };
                    }
                    //RefreshMCProfitHistoryList();
                    ID = _mProfit.ID = DataContainerFacade.Save<McProfitAbility, long>(_mProfit);
                    break;
                case ViewModelState.Create:
                    ID = _mProfit.ID = DataContainerFacade.Save<McProfitAbility, long>(_mProfit);
                    State = ViewModelState.Edit;
                    OnPropertyChanged("IsCreate");
                    break;
                default:
                    break;
            }
        }

        protected override void OnCardSaved()
        {
            base.OnCardSaved();
            RefreshMCProfitHistoryList();
        }

        public void RefreshLists() { }

        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
					case "SelectedContract": return SelectedContract.ValidateRequired();
                    case "SelectedNPF": return SelectedUK.ValidateRequired();
					case "SelectedYear": return SelectedYear.ValidateRequired();
					case "SelectedQuarter": return SelectedQuarter.ValidateRequired();
					case "ProfitDate": return ProfitDate.ValidateRequired();

                    case "SelectedUK": return SelectedUK.ValidateRequired();
                    case "Comment": return Comment.ValidateMaxLength(512);

                    case "ProfitAbility": return ProfitAbility.ValidateRange(-1000, 1000,false);
					case "ProfitAbility3": return ProfitAbility3.ValidateRange(-1000, 1000, false);
					case "ProfitAbility12": return ProfitAbility12.ValidateRange(-1000, 1000, false);
					case "ProfitAbilityContract": return ProfitAbilityContract.ValidateRange(-1000, 1000, false);
                }
                return string.Empty;
            }
        }


    }
}
