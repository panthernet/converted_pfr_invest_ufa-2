﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OFPR_manager)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
    public class EnrollmentPercentsViewModel : AccModelBase, ICloseViewModelAfterSave
    {
        public EnrollmentPercentsViewModel()
        {
            ID = 0;
            acc.KindID = 1; // потому что зачисление процентов по счетам
            acc.Content = " "; // for validation
        }

        public override void Load(long lId)
        {
            ID = lId;
            base.Load(lId);
            acc.Content = " "; // for validation
        }
    }
}
