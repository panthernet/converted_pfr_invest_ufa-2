﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.ViewModelsCard;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataAccess.Server.DataObjects;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    public interface ITmpRepository<in T> where T : class
    {
        void Save(T obj);
        void Delete(T obj);
    }



    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
	[DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_manager)]
	public class TransferViewModel : ViewModelCard, ITmpRepository<ReqTransfer>, IUpdateListenerModel
    {
        public SITransfer TransferList { get; set; }
        public SIRegister TransferRegister { get; set; }
        public SPNOperation Operation { get; set; }
        public Year Year { get; set; }
        public int ConractTypeId { get; }

        public long InsuredPersonsCount
        {
            get
            {
                return TransferList.InsuredPersonsCount ?? 0;
            }
            set
            {
                TransferList.InsuredPersonsCount = value;
                OnPropertyChanged("InsuredPersonsCount");
            }
        }

        public long InsuredPersonsSum { get { return TransferRequests?.Sum(a => a.ZLMoveCount ?? 0) ?? 0; } }


        public string Register => $"{TransferRegister.ID}, {TransferRegister.RegisterDate?.ToShortDateString() ?? ""}";

        public bool IsTransferFromUKToPFR => TransferRegister?.GetDirection() != null && (TransferRegister.GetDirection().isFromUKToPFR ?? false);

        private List<LegalEntity> _mUKList = new List<LegalEntity>();
        public List<LegalEntity> UKList
        {
            get
            {
                return _mUKList;
            }

            set
            {
                _mUKList = value;
                SelectedUK = value.FirstOrDefault();
                OnPropertyChanged("UKList");
            }
        }

        private LegalEntity _mSelectedUK;
        public LegalEntity SelectedUK
        {
            get
            {
                return _mSelectedUK;
            }

            set
            {
                _mSelectedUK = value;
                OnPropertyChanged("SelectedUK");
                RefreshContractsList();
            }
        }

        private List<Contract> _mContractsList = new List<Contract>();
        public List<Contract> ContractsList
        {
            get
            {
                return _mContractsList;
            }
            set
            {
                _mContractsList = value;
                SelectedContract = value.FirstOrDefault();
                OnPropertyChanged("ContractsList");
            }
        }

        private Contract _mSelectedContract;
        public Contract SelectedContract
        {
            get
            {
                return _mSelectedContract;
            }
            set
            {
                if (value != null)
                {
                    _mSelectedContract = value;
                    TransferList.ContractID = value.ID;
                    OnPropertyChanged("SelectedContract");
                    OnPropertyChanged("ContractDate");
                }
            }
        }

        public PPTransferListItem SelectedTransferRequest { get; set; }
		private List<PPTransferListItem> _mTransferRequests = new List<PPTransferListItem>();
		public List<PPTransferListItem> TransferRequests
        {
            get
            {
                return _mTransferRequests;
            }
            set
            {
                _mTransferRequests = value;
                OnPropertyChanged("InsuredPersonsSum");
                OnPropertyChanged("TransferRequests");
            }
        }

        public decimal TotalTransferSum
        {
            get
            {
                return _mTransferRequests.Select(x => x.Sum ?? 0).Sum();
            }
        }

        public decimal TotalInvestment
        {
            get
            {
                return _mTransferRequests.Select(x => x.InvestmentIncome ?? 0).Sum();
            }
        }

        public override bool CanExecuteDelete()
        {
            return State != ViewModelState.Create;
        }

        public override bool BeforeExecuteDeleteCheck()
        {
            return IsAllowRemove() && DialogHelper.ConfirmSITransferListDeleting();
        }

        protected override void ExecuteDelete(int delType)
        {
            BLServiceSystem.Client.DeleteTransfer(
                TransferRequests.Select(
                    a =>
                        new ReqTransferDeleteListItem
                        {
                            ReqTransferId = a.ID,
                            ContractId = TransferList.GetContract().ID,
                            IsActSigned = TransferStatusIdentifier.IsStatusActSigned(a.TransferStatus)
                        }).ToList(), TransferList.ID);
            base.ExecuteDelete(DocOperation.Delete);
        }

        public override bool CanExecuteSave()
        {
            return Validate() && IsDataChanged;
        }

        protected override bool BeforeExecuteSaveCheck()
        {
            var fromUk = TransferList.GetTransferRegister().GetDirection().isFromUKToPFR;
            if (!fromUk.HasValue || !fromUk.Value || !TransferList.ContractID.HasValue) return true;
            var maxDate = TransferRequests.Max(r => r.TransferActDate);
            var d = maxDate ?? DateTime.Today;
            var zlCount = BLServiceSystem.Client.GetZLCountForDate(TransferList.ContractID.Value, d);
            return zlCount >= InsuredPersonsCount || DialogHelper.IgnorZLCountOwerflow(d, zlCount);
        }

        protected override void ExecuteSave()
        {
            if (!CanExecuteSave()) return;
            switch (State)
            {
                case ViewModelState.Read:
                    break;
                case ViewModelState.Edit:
                    DataContainerFacade.Save<SITransfer, long>(TransferList);
                    break;
                case ViewModelState.Create:
                    TransferList.ID = ID = BLServiceSystem.Client.SaveTransfer(TransferList, TransferRequests.Select(ReqTransferHib.MapFromPPTransfer).ToList());
                    RefreshTransfers();
                    break;
            }
        }

        private const string WRONG_INSURED_PERSONS_COUNT = "Неверно указано количество застрахованных лиц";
        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "SelectedUK":
                        return SelectedUK != null ? null : "Не выбрано значение";
                    case "SelectedContract":
                        return SelectedContract != null ? null : "Не выбрано значение";
                    case "InsuredPersonsCount":
                        return (InsuredPersonsCount >= 0) ? null : WRONG_INSURED_PERSONS_COUNT;
                    default:
                        return null;
                }
            }
        }

        private bool Validate()
        {
            return string.IsNullOrEmpty(this["InsuredPersonsCount"]) && TransferRequests.Count > 0;
        }

        private readonly Contract _initialContract;

        public override Type DataObjectTypeForJournal => typeof(SITransfer);

        public TransferViewModel(long id, ViewModelState state)
            : base(typeof(TransfersSIListViewModel), typeof(TransfersSIArchListViewModel), 
                                typeof(SPNMovementsSIListViewModel), typeof(ZLMovementsSIListViewModel),
                                typeof(TransfersVRListViewModel), typeof(TransfersVRArchListViewModel), 
                                typeof(SPNMovementsVRListViewModel), typeof(ZLMovementsVRListViewModel))
        {
            if (state == ViewModelState.Create)
            {
                TransferRegister = DataContainerFacade.GetByID<SIRegister, long>(id);
                var siTr = TransferRegister.GetListSITransfer().FirstOrDefault();
                if (siTr != null)
                {
                    ConractTypeId = siTr.GetContract().TypeID;
                    _mUKList = BLServiceSystem.Client.GetActiveUKListByTypeContractHibForRegister(ConractTypeId, id);
                }
                else
                    _mUKList = new List<LegalEntity>(BLServiceSystem.Client.GetActiveUKListHib());
                TransferList = new SITransfer { TransferRegisterID = id };
                SelectedUK = UKList.FirstOrDefault();
            }
            else
            {
                ID = id;
                TransferList = DataContainerFacade.GetByID<SITransfer, long>(id);
                var contract = TransferList.GetContract();
                if (contract.StatusID == -1)
                    _initialContract = contract; 
                ConractTypeId = contract.TypeID;
                TransferRegister = TransferList.GetTransferRegister();
                _mUKList = BLServiceSystem.Client.GetActiveUKListByTypeContractHibForRegister(ConractTypeId, TransferRegister.ID, contract.LegalEntityID, contract.ID);


                _loadedContractId = contract.ID;
                SelectedUK = UKList.FirstOrDefault(
                    le => le.ID == contract.LegalEntityID
                );

                SelectedContract = ContractsList.FirstOrDefault(
                    c => c.ID == _loadedContractId
                );
                RefreshTransfers(); 
            }

            Operation = TransferRegister.GetOperation();
            Year = TransferRegister.GetCompanyYear();

            RefreshConnectedCardsViewModels = RefreshParentViewModel;
        }

        private readonly long? _loadedContractId;

        private void RefreshContractsList()
        {
            if (_mSelectedUK == null) ContractsList = new List<Contract>();
            else 
            {
                ContractsList = ConractTypeId == (int) Document.Types.All ? _mSelectedUK.GetContracts().Where(a=> a.StatusID >=0).ToList() : 
                BLServiceSystem.Client.GetAvailableContractsForUkTransfer(ConractTypeId, TransferRegister.ID, _mSelectedUK.ID, _loadedContractId);
                if(_initialContract != null && _initialContract.StatusID == -1 && ! ContractsList.Contains(_initialContract))
                    ContractsList.Insert(0, _initialContract);
            }
        }


        public void RefreshTransfers()
        {
            if (ID <= 0) return;
            var isAlreadyChanged = IsDataChanged;
            //TransferList.ExtensionData.Remove("Transfers");
            TransferRequests = BLServiceSystem.Client.GetUKPPTransfersByTransferID(TransferList.ID); //TransferList.GetTransfers().Cast<ReqTransfer>().ToList();
            IsDataChanged = isAlreadyChanged;
            OnPropertyChanged("InsuredPersonsSum");
        }

        private static void RefreshParentViewModel()
        {
            if (GetViewModel == null) return;
            var vm = GetViewModel(typeof(SIVRRegisterSIViewModel)) as SIVRRegisterViewModel;
            vm?.RefreshTransferLists();
            vm = GetViewModel(typeof(SIVRRegisterVRViewModel)) as SIVRRegisterViewModel;
            vm?.RefreshTransferLists();
        }


        public void Save(ReqTransfer obj)
        {
            //для того что бы обновился грид нужно переназначить свойство
            var tmp = TransferRequests;
            TransferRequests = null;
			var mobj = new PPTransferListItem();
			obj.CopyTo(mobj);
            if (!tmp.Contains(obj)) tmp.Add(mobj);
            TransferRequests = tmp;
            OnPropertyChanged("InsuredPersonsSum");
        }

        public void Delete(ReqTransfer obj)
        {
            //для того что бы обновился грид нужно переназначить свойство
            var tmp = TransferRequests;
            TransferRequests = null;
			var mobj = new PPTransferListItem();
			obj.CopyTo(mobj);
            tmp.Remove(mobj);
            TransferRequests = tmp;
            OnPropertyChanged("InsuredPersonsSum");
        }

        private bool IsAllowRemove()
        {
            var trl = TransferList.GetTransferRegister();

            //не план
            if (TransferRequests.Any())
            {
                if (!TransferRequests.Any(req => req.IsSpnTransferredOver()))
                    return true;
                DialogHelper.ShowWarning("Удалить \"Список перечислений\" невозможно, так как были выполнены перечисления СПН.\nУдалите перечисления и повторите операцию.",
                    "Операция отменена");
                return false;
            }//план

            if (!BLServiceSystem.Client.IsUKPlansHasReqTransfers(new List<long> {TransferList.ID}))
                return true;
            DialogHelper.ShowWarning("Удалить \"Список перечислений\" плана невозможно, так как на него существуют перечисления.\nУдалите перечисления и повторите операцию.",
                "Операция отменена");
            return false;
        }


		public Type[] UpdateListenTypes => new[] { typeof(ReqTransfer) };

        public void OnDataUpdate(Type type, long idx)
		{
			if (type == typeof(ReqTransfer))
			{
				RefreshTransfers();
			}
		}
	}
}
