﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_directory_editor, DOKIP_ROLE_TYPE.OUFV_directory_editor)]
    public class F401402UKViewModel : ViewModelCard, IUpdateRaisingModel
    {
        private List<F401402UKStatus> _f401402UkStatuses;
        public string Month
        {
            get
            {
                if (_mDocument != null && _mDocument.FromDate.HasValue)
                    return _mDocument.FromDate.Value.ToString("MMMM", new CultureInfo("Ru-ru"));
                return null;
            }
        }

        public F402Status OriginalStatus;
        private F401402UK _mDetail;
        public F401402UK Detail
        {
            get { return _mDetail; }
            set
            {
                _mDetail = value;
                OnPropertyChanged("Detail");
                //так как вюха привязана к свойствам этой модели а не к свойствам 
                //объекта F401402UK, то нужно вызвать OnPropertyChanged для всех полей-делегатов
                OnPropertyChanged("ControlRegnum");
                OnPropertyChanged("ControlSendDate");
                OnPropertyChanged("ControlGetDate");
                OnPropertyChanged("ControlSetDate");
                OnPropertyChanged("ControlFactDate");
                OnPropertyChanged("StatusName");
                OnPropertyChanged("OutgoingDocDate");
                OnPropertyChanged("OutgoingDocNumber");
            }
        }

        private EdoOdkF401402 _mDocument;
        public EdoOdkF401402 Document
        {
            get { return _mDocument; }
            set
            {
                _mDocument = value;
                OnPropertyChanged("Document");
            }
        }

        private Contract _mContract;
        public Contract Contract
        {
            get { return _mContract; }
            set
            {
                _mContract = value;
                OnPropertyChanged("Contract");
            }
        }
        public Document.Types DocumentType => (Document.Types)Contract.TypeID;
        private LegalEntity _mLegalEntity;
        public LegalEntity LegalEntity
        {
            get { return _mLegalEntity; }
            set
            {
                _mLegalEntity = value;
                OnPropertyChanged("LegalEntity");
            }
        }

        public DateTime? OutgoingDocDate
        {
            get
            {
                return _mDetail.OutgoingDocDate;
            }
            set
            {
                _mDetail.OutgoingDocDate = value;
                OnPropertyChanged("OutgoingDocDate");
            }
        }

        public string OutgoingDocNumber
        {
            get
            {
                return _mDetail.OutgoingDocNumber;
            }
            set
            {
                _mDetail.OutgoingDocNumber = value;
                OnPropertyChanged("OutgoingDocNumber");
            }
        }

        public long? OriginalXmlID { get; set; }
        public bool IsDemand { get; private set; }

        public bool IsNotAdmin => !AP.Provider.CurrentUserSecurity.CheckUserInRole(DOKIP_ROLE_TYPE.Administrator);

        #region control

        public string ControlRegnum
        {
            get { return Detail.ControlRegnum == null ? null : Detail.ControlRegnum.Trim(); }
            set
            {
                Detail.ControlRegnum = value;
                OnPropertyChanged("ControlRegnum");
            }
        }

        public DateTime? ControlSendDate
        {
            get { return Detail.ControlSendDate; }
            set
            {
                Detail.ControlSendDate = value;
                UpdateStatus();
                UpdateDateProperties();
            }
        }

        public DateTime? ControlGetDate
        {
            get { return Detail.ControlGetDate; }
            set
            {
                Detail.ControlGetDate = value;
                if (Detail.ControlGetDate.HasValue)
                {
                    ControlSetDate = Detail.ControlGetDate.Value.AddDays(7);
                }
                UpdateStatus();
                UpdateDateProperties();
            }
        }

        public DateTime? ControlSetDate
        {
            get { return Detail.ControlSetDate; }
            set
            {
                Detail.ControlSetDate = value;
                UpdateStatus();
                UpdateDateProperties();
            }
        }

        public DateTime? ControlFactDate
        {
            get { return Detail.ControlFactDate; }
            set
            {
                Detail.ControlFactDate = value;
                UpdateStatus();
                UpdateDateProperties();
            }
        }

        private void UpdateDateProperties()
        {
            OnPropertyChanged("ControlFactDate");
            OnPropertyChanged("ControlSetDate");
            OnPropertyChanged("ControlGetDate");
            OnPropertyChanged("ControlSendDate");
        }

        public string StatusName
        {
            get { return Detail.StatusName; }
            set
            {
                Detail.StatusName = value;
                OnPropertyChanged("StatusName");
            }
        }

        private void UpdateStatus()
        {
            if (!ControlSendDate.HasValue)
            {
                SetStatus(OriginalStatus == F402Status.Started ? F402Status.Started : F402Status.Built);
                return;
            }

            if (!ControlGetDate.HasValue)
            {
                SetStatus(F402Status.Sent);
                return;
            }

            if (!ControlSetDate.HasValue || !ControlFactDate.HasValue)
            {
                SetStatus(F402Status.Delivered);
                return;
            }

            if (ControlFactDate.Value <= ControlSetDate.Value)
            {
                SetStatus(F402Status.PaidInTime);
            }
            else
            {
                SetStatus(F402Status.PaidTimeExceeded);
            }

            //if (ControlSetDate.HasValue && ControlFactDate.HasValue)
            //{
            //    if (this.ControlFactDate.Value <= ControlSetDate.Value)
            //    {
            //        SetStatus(F402Status.PaidInTime);
            //    }
            //    else
            //    {
            //        SetStatus(F402Status.PaidTimeExceeded);
            //    }
            //    return;
            //}

            //if (ControlGetDate.HasValue)
            //{
            //    // установлена дата получения, не установлена дата оплаты
            //    SetStatus(F402Status.Delivered);
            //    return;
            //}

            //if (ControlSendDate.HasValue)
            //{
            //    // не установлена дата получения, устновлена дата отправки
            //    SetStatus(F402Status.Sent);
            //    return;
            //}

            // не удалось получить новое значение статуса
            // SetStatus(OriginalStatus);
        }

        private void SetStatus(F402Status s)
        {
            var status = _f401402UkStatuses.FirstOrDefault(x => x.ID == (long)s);
            if (status != null)
            {
                Detail.StatusID = status.ID;
                StatusName = status.Name;
            }
        }


        #endregion

        public F401402UKViewModel(long id, bool isDemand)
            //: base(typeof(F401402UKListViewModel))
        {
            DataObjectTypeForJournal = typeof(F401402UK);
            Load(id);
            IsDemand = isDemand;
        }

        public void RefreshModel()
        {
            Load(ID);
        }

        private void Load(long id)
        {
            ID = id;
            Detail = DataContainerFacade.GetByID<F401402UK, long>(id);
            _f401402UkStatuses = DataContainerFacade.GetList<F401402UKStatus>();
            Document = Detail.GetDocument();
            if (Document != null)
                OriginalXmlID = BLServiceSystem.Client.GetOriginalXmlIDForEdoOdkF401402(Document.ID, Document.RegNumberOut);
            Contract = Detail.GetContract();
            LegalEntity = Contract.GetLegalEntity();
            IsDataChanged = false;
            OriginalStatus = (F402Status?)Detail.StatusID ?? F402Status.Built;
        }

        //public override bool CanExecuteSave() { return false; }
        public override bool CanExecuteSave()
        {
            return string.IsNullOrEmpty(Validate()) && IsDataChanged;
        }

        protected override void ExecuteSave()
        {
            if (!CanExecuteSave())
                return;
            DataContainerFacade.Save(Detail);
            OriginalStatus = (F402Status?)Detail.StatusID ?? F402Status.Built;
        }

        string Validate()
        {
            const string errorMessage = "Неверный формат данных";
            const string validate = "CONTROLREGNUM|CONTROLSENDDATE|CONTROLGETDATE|CONTROLSETDATE|CONTROLFACTDATE";

            if (Detail == null)
                return errorMessage;

            return validate.Split('|').Any(key => !string.IsNullOrEmpty(this[key])) ? errorMessage : null;
        }

        public override string this[string columnName]
        {
            get
            {
                const string errorMessage = "Неверный формат данных";
                if (Detail == null)
                    return errorMessage;
                const string dateOrderErrorMessage = "Некорректный порядок выставления дат";

                switch (columnName.ToUpper())
                {
                    case "CONTROLREGNUM": return string.IsNullOrEmpty(Detail.ControlRegnum) ? errorMessage : null;
                    case "CONTROLSENDDATE": 
                        if ((ControlGetDate.HasValue || ControlSetDate.HasValue || ControlFactDate.HasValue) && !ControlSendDate.HasValue) return dateOrderErrorMessage;
                        return ValidateSendDate();
                    case "CONTROLGETDATE": 
                        if ((ControlSetDate.HasValue || ControlFactDate.HasValue) && !ControlGetDate.HasValue) return dateOrderErrorMessage;
                        if (ControlGetDate.HasValue && !ControlSendDate.HasValue) return dateOrderErrorMessage;
                        if (ControlSetDate.HasValue ^ ControlGetDate.HasValue) return dateOrderErrorMessage;
                        return ValidateGetDate();
                    case "CONTROLSETDATE": 
                        if (ControlFactDate.HasValue && !Detail.ControlSetDate.HasValue) return dateOrderErrorMessage;
                        if (ControlSetDate.HasValue && (!ControlSendDate.HasValue || !ControlGetDate.HasValue)) return dateOrderErrorMessage;
                        if (ControlSetDate.HasValue ^ ControlGetDate.HasValue) return dateOrderErrorMessage;
                        return ValidateSetDate();
                    case "CONTROLFACTDATE": 
                        if (ControlFactDate.HasValue && (!ControlSendDate.HasValue || !ControlGetDate.HasValue || !ControlSetDate.HasValue)) return dateOrderErrorMessage;
                        return ValidateFactDate();
                    default: return null;
                }
            }
        }

        private string ValidateSetDate()
        {
            if (ControlSetDate.HasValue)
            {
                if (ControlSendDate.HasValue && ControlSetDate < ControlSendDate)
                {
                    return string.Format("Дата отправки ({0}) не может быть больше установленной даты оплаты", ControlSendDate.Value.ToShortDateString());
                }

                if (ControlGetDate.HasValue && ControlSetDate < ControlGetDate)
                {
                    return string.Format("Дата вручения ({0}) не может быть больше установленной даты оплаты поручения", ControlGetDate.Value.ToShortDateString());
                }
            }

            return null;
        }

        private string ValidateFactDate()
        {
            if (ControlFactDate.HasValue)
            {
                if (ControlSendDate.HasValue && ControlFactDate < ControlSendDate)
                {
                    return string.Format("Дата отправки ({0}) не может быть больше фактической даты оплаты", ControlSendDate.Value.ToShortDateString());
                }

                if (ControlGetDate.HasValue && ControlFactDate < ControlGetDate)
                {
                    return string.Format("Дата вручения ({0}) не может быть больше фактической даты оплаты поручения", ControlGetDate.Value.ToShortDateString());
                }
            }

            return null;
        }

        private string ValidateSendDate()
        {
            if (ControlSendDate.HasValue)
            {
                if (ControlGetDate.HasValue && ControlGetDate < ControlSendDate)
                {
                    return string.Format("Дата отправки не может быть больше даты вручения поручения ({0})", ControlGetDate.Value.ToShortDateString());
                }

                if (ControlSetDate.HasValue && ControlSetDate < ControlSendDate)
                {
                    return string.Format("Дата отправки не может быть больше установленной даты оплаты ({0})", ControlSetDate.Value.ToShortDateString());
                }

                if (ControlFactDate.HasValue && ControlFactDate < ControlSendDate)
                {
                    return string.Format("Дата отправки не может быть больше фактической даты оплаты ({0})", ControlFactDate.Value.ToShortDateString());
                }
            }
            return null;
        }

        private string ValidateGetDate()
        {
            if (ControlGetDate.HasValue)
            {
                if (ControlSetDate.HasValue && ControlSetDate < ControlGetDate)
                {
                    return string.Format("Дата вручения не может быть больше установленной даты оплаты поручения ({0})", ControlSetDate.Value.ToShortDateString());
                }

                if (ControlFactDate.HasValue && ControlFactDate < ControlGetDate)
                {
                    return string.Format("Дата вручения не может быть больше фактической даты оплаты поручения ({0})", ControlFactDate.Value.ToShortDateString());
                }
            }
            return null;
        }

        //private bool OriginalStatusIn(params F402Status[] t)
        //{
        //    return t.Contains(OriginalStatus);
        //}

        public void RefreshControlGroup()
        {
            OnPropertyChanged("Detail");
            Load(Detail.ID);
        }

        public IList<KeyValuePair<Type, long>> GetUpdatedList()
        {
            return new List<KeyValuePair<Type, long>>
            { 
				new KeyValuePair<Type, long>(typeof(F401402UKListItem), Detail.ID)
			};
        }
    }
}
