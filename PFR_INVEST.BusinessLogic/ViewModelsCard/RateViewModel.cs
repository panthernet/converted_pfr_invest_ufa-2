﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_directory_editor, DOKIP_ROLE_TYPE.OARRS_directory_editor)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OARRS_manager)]
    public class RateViewModel : ViewModelCard
    {

        #region CurrencyList

        private List<Currency> _currencyList;

        public List<Currency> CurrencyList
        {
            get { return _currencyList; }
            set { _currencyList = value; OnPropertyChanged("CurrencyList"); }
        }

        private Curs _curs;
        public Curs Curs { get { return _curs; } set { _curs = value; OnPropertyChanged("Curs"); } }

        public Currency SelectedCurrency
        {
            get
            { 
                return _currencyList.FirstOrDefault(currency => _curs.CurrencyID == currency.ID); 
            }

            set
            {
                if (value == null) _curs.CurrencyID = null;
                else _curs.CurrencyID = value.ID;
                OnPropertyChanged("SelectedCurrency");
            }
        }
        #endregion

        public RateViewModel(long _id): base(typeof(RatesListViewModel), typeof(MissedRatesListViewModel))
        {
            DataObjectTypeForJournal = typeof(Curs);
            _currencyList = BLServiceSystem.Client.GetCurrencyListHib();

            GetCourceFromCBRSite = new DelegateCommand(o => EditAccessAllowed,
                o => ExecuteGetCourceFromCBRSite());
            //удалем все ненужные валюты (рубли)
            _currencyList = _currencyList.Where(cur => !cur.Name.ToLower().Contains("руб")).ToList();

            if (_id > 0)
            {
                Curs = DataContainerFacade.GetByID<Curs>(_id);
                ID = _id;
            }
            else
            {
                Curs = new Curs {Date = DateTime.Now};
                SelectedCurrency = _currencyList.FirstOrDefault(entity => entity.Name.ToUpper().EndsWith("США"));
            }
        }

        public RateViewModel(RatesListItem ratesListItem)
            : base(typeof(RatesListViewModel), typeof(MissedRatesListViewModel))
        {
            DataObjectTypeForJournal = typeof(Curs);
            _currencyList = BLServiceSystem.Client.GetCurrencyListHib();

            GetCourceFromCBRSite = new DelegateCommand(o => EditAccessAllowed,
                o => ExecuteGetCourceFromCBRSite());

                        //удалем все ненужные валюты (рубли)
            _currencyList = _currencyList.Where(cur => !cur.Name.ToLower().Contains("руб")).ToList();

            Curs = new Curs { Date = ratesListItem.Date };
            SelectedCurrency = _currencyList.FirstOrDefault(entity => entity.Name.Equals(ratesListItem.Currency));
        }


        #region Course command
        public ICommand GetCourceFromCBRSite { get; private set; }

        protected void ExecuteGetCourceFromCBRSite()
        {
            RaiseStartedGettingCourseFromCBRSite();
            try
            {

                Curs.Value = CBRConnector.GetCourseRate(SelectedCurrency.ID, Curs.Date ?? DateTime.MinValue);
                OnPropertyChanged("Curs");
            }
            catch (Exception ex)
            {
                RaiseFinishedGettingCourseFromCBRSite(false);
                RaiseCantGetCourseFromCBRSite(ex.Message);
                return;
            }
            RaiseFinishedGettingCourseFromCBRSite(true);
        }

        public bool IsEventsRegisteredOnLoad = false;
        public EventHandler OnCourceEntered;
        protected void RaiseCourceEntered()
        {
            if (OnCourceEntered != null)
                OnCourceEntered(this, null);
        }

        public EventHandler OnStartedGettingCourseFromCBRSite;
        protected void RaiseStartedGettingCourseFromCBRSite()
        {
            if (OnStartedGettingCourseFromCBRSite != null)
                OnStartedGettingCourseFromCBRSite(this, null);
        }

        public EventHandler OnFinishedGettingCourseFromCBRSite;
        protected void RaiseFinishedGettingCourseFromCBRSite(bool success)
        {
            if (OnFinishedGettingCourseFromCBRSite != null)
                OnFinishedGettingCourseFromCBRSite(this, new CurseEventArgs { ShowSuccess = success });
        }

        public delegate void CantGetCourseFromCBRSiteDelegate(object Sender, CantGetCourseFromCBRSiteEventArgs e);
        public CantGetCourseFromCBRSiteDelegate OnCantGetCourseFromCBRSite;
        protected void RaiseCantGetCourseFromCBRSite(string message)
        {
            if (OnCantGetCourseFromCBRSite != null)
                OnCantGetCourseFromCBRSite(this, new CantGetCourseFromCBRSiteEventArgs(message));
        }
        #endregion

        #region Execute implementation
        public override bool CanExecuteSave()
        {
            return String.IsNullOrEmpty(Validate()) && IsDataChanged;
        }

        private string Validate()
        {
            if (SelectedCurrency == null || Curs == null)
                return "Неверный формат данных";

            if (Curs.Date == null)
                return "Неверный формат данных";

            if (Curs.Value == null || Curs.Value > 10000)
                return "Неверный формат данных";

            return null;
        }

        public override bool CanExecuteDelete()
        {
            return State != ViewModelState.Create && DeleteAccessAllowed;
        }

        public override bool BeforeExecuteDeleteCheck()
        {
            if (DataContainerFacade.GetListByProperty<Incomesec>("CursID", ID).Count == 0) return true;
            DialogHelper.ShowError("Курс валют не может быть удален, так как он учитывается в расходах по ценным бумагам.");               
            return false;
        }

        protected override void ExecuteDelete(int delType)
        {
            DataContainerFacade.Delete(DataContainerFacade.GetByID<Curs>(ID));
            base.ExecuteDelete(DocOperation.Delete);        
        }

        protected override void ExecuteSave()
        {
            if (!CanExecuteSave()) return;

            switch (State)
            {
                case ViewModelState.Read:
                    break;
                case ViewModelState.Edit:
                case ViewModelState.Create:
                    ID = Curs.ID = DataContainerFacade.Save(Curs);
                    break;
            }
        }
        #endregion

        public override string this[string columnName]
        {
            get { throw new NotImplementedException(); }
        }
    }
}
