﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OVSI_worker)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_directory_editor, DOKIP_ROLE_TYPE.OKIP_directory_editor, DOKIP_ROLE_TYPE.OVSI_directory_editor)]
	[DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OKIP_manager, DOKIP_ROLE_TYPE.OVSI_manager)]
	public class PFRBranchViewModel : ViewModelCard
	{
		public PFRBranch Branch { get; set; }
		public BankAccount BankAccount { get; set; }
		private BankAccount _bankAccountCopy { get; set; }

        private List<PFRBranch> _existsBranches;

        #region bankAccount
        public string BankAccountLegalEntityName
	    {
	        get { return BankAccount.LegalEntityName; }
	        set
	        {
	            BankAccount.LegalEntityName = value;
	            OnPropertyChanged("BankAccountLegalEntityName");
	        }
	    }

        public string BankAccountAccountNumber
        {
            get { return BankAccount.AccountNumber; }
            set
            {
                BankAccount.AccountNumber = value;
                OnPropertyChanged("BankAccountAccountNumber");
            }
        }

        public string BankAccountBankName
        {
            get { return BankAccount.BankName; }
            set
            {
                BankAccount.BankName = value;
                OnPropertyChanged("BankAccountBankName");
            }
        }

        public string BankAccountCorrespondentAccountNumber
        {
            get { return BankAccount.CorrespondentAccountNumber; }
            set
            {
                BankAccount.CorrespondentAccountNumber = value;
                OnPropertyChanged("BankAccountCorrespondentAccountNumber");
            }
        }

        public string BankAccountBankLocation
        {
            get { return BankAccount.BankLocation; }
            set
            {
                BankAccount.BankLocation = value;
                OnPropertyChanged("BankAccountBankLocation");
            }
        }

        public string BankAccountCorrespondentAccountNote
        {
            get { return BankAccount.CorrespondentAccountNote; }
            set
            {
                BankAccount.CorrespondentAccountNote = value;
                OnPropertyChanged("BankAccountCorrespondentAccountNote");
            }
        }

        public string BankAccountBIK
        {
            get { return BankAccount.BIK; }
            set
            {
                BankAccount.BIK = value;
                OnPropertyChanged("BankAccountBIK");
            }
        }

        public string BankAccountINN
        {
            get { return BankAccount.INN; }
            set
            {
                BankAccount.INN = value;
                OnPropertyChanged("BankAccountINN");
            }
        }

        public string BankAccountKPP
        {
            get { return BankAccount.KPP; }
            set
            {
                BankAccount.KPP = value;
                OnPropertyChanged("BankAccountKPP");
            }
        }
        #endregion

        public string Name
		{
			get { return Branch.Name; }
			set { Branch.Name = value; OnPropertyChanged("Name"); }
		}

		public string Address
		{
			get { return Branch.Address; }
			set { Branch.Address = value; OnPropertyChanged("Address"); }
		}

		public int? RegionNumber
		{
			get { return Branch.RegionNumber ?? 0; }
			set { Branch.RegionNumber = value; OnPropertyChanged("RegionNumber"); }
		}

		private List<FEDO> _fedOList = new List<FEDO>();
		public List<FEDO> FedOsList
		{
			get { return _fedOList; }
			set { _fedOList = value; OnPropertyChanged("FedOsList"); }
		}

		private List<PFRContact> _pfrContactsList = new List<PFRContact>();
		public List<PFRContact> PFRContactsList
		{
			get { return _pfrContactsList; }
			set { _pfrContactsList = value; OnPropertyChanged("PFRContactsList"); }
		}



		public string INN
		{
			get { return Branch.INN; }
			set { Branch.INN = value; OnPropertyChanged("INN"); }
		}

		public string KPP
		{
			get { return Branch.KPP; }
			set { Branch.KPP = value; OnPropertyChanged("KPP"); }
		}

		public FEDO SelectedFedO
		{
			get
			{
				var ff = _fedOList.FirstOrDefault(secKind => secKind.ID as long? == Branch.FEDO_ID);
				return ff;
			}

			set
			{
				if (value == null) return;
				Branch.FEDO_ID = value.ID;
				OnPropertyChanged("SelectedFedO");
			}
		}

		public bool CanExecuteAddContact => ID > 0;

	    public override bool CanExecuteDelete()
		{
			return State != ViewModelState.Create;
		}

		protected override void ExecuteDelete(int delType = 1)
		{
			DataContainerFacade.Delete(Branch);
			base.ExecuteDelete(DocOperation.Archive);
		}

		#region Commands

	    public ICommand DeleteContact { get; }

	    #endregion

		public override Type DataObjectTypeForJournal => typeof(PFRBranch);

	    public PFRBranchViewModel(long id, ViewModelState action)
			: base(typeof(PFRBranchesListViewModel))
		{
			State = action;
			_existsBranches = DataContainerFacade.GetList<PFRBranch>();
            if (action == ViewModelState.Create)
			{
				Branch = new PFRBranch();
                BankAccount = new BankAccount();
			}
			else
			{
				ID = id;
				Branch = DataContainerFacade.GetByID<PFRBranch, long>(ID);
			    BankAccount = DataContainerFacade.GetListByProperty<BankAccount>("PfrBranchID", id, true).FirstOrDefault() ?? new BankAccount { PfrBranchID = ID };
			    _bankAccountCopy = BankAccount.Copy();
				_existsBranches = _existsBranches.Where(x => x.ID != id).ToList();
			}

	        DeleteContact = new DelegateCommand(o => CanExecuteDeleteContact(),
	            o => ExecuteDeleteContact());
			RefreshLists();
		}
		public PFRBranchViewModel(Type pListViewModel)
			: base(pListViewModel)
		{
		}
		public PFRBranchViewModel(IEnumerable<Type> pListViewModelCollection)
			: base(pListViewModelCollection)
		{
		}
		public PFRBranchViewModel()
		{
		}


		#region Execute implementation

		public PFRContact SelectedContact { get; set; }
		public bool CanExecuteDeleteContact()
		{
			return SelectedContact != null && !EditAccessDenied;
		}

		public void ExecuteDeleteContact()
		{
			if (SelectedContact.ID != 0)
			{
				var contact = BLServiceSystem.Client.GetPFRContactHib(SelectedContact.ID);
				if (contact != null)
				{
					var m = new PFRContactViewModel(SelectedContact.ID, ViewModelState.Read);
					m.ExecuteDelete();
				}
			}
			else
			{
				PFRContactsList.Remove(SelectedContact);
			}
			RefreshLists(true);
		}

		public override bool CanExecuteSave()
		{
			return Validate() && IsDataChanged;
		}
		public PFRContact SaveContact(PFRContact contact)
		{
			if (contact.ID == 0)
			{
				contact.PFRB_ID = Branch.ID;
				contact.ID = DataContainerFacade.Save<PFRContact, long>(contact);
			}
			return contact;
		}

		protected override bool BeforeExecuteSaveCheck()
		{
			_existsBranches = DataContainerFacade.GetList<PFRBranch>().Where(x => x.ID != Branch.ID && x.StatusID != -1).ToList();
			if (_existsBranches.Any(x => x.RegionNumber == RegionNumber.Value))
			{
				OnPropertyChanged("RegionNumber");
				DialogHelper.ShowError(ALREADY_EXISTS_RATING_NUMBER);
				return false;
			}

			return base.BeforeExecuteSaveCheck();
		}

		protected override void ExecuteSave()
		{
		    if (!CanExecuteSave()) return;
		    switch (State)
		    {
		        case ViewModelState.Read:
		            break;
		        case ViewModelState.Edit:
		        case ViewModelState.Create:

		            BankAccount.PfrBranchID = Branch.ID = ID = DataContainerFacade.Save<PFRBranch, long>(Branch);
                    //если новый счет или счет был изменен
                    if(BankAccount.ID == 0 || (_bankAccountCopy!=null && !BankAccount.IsEqual(_bankAccountCopy)))
		                BankAccount.ID = BLServiceSystem.Client.SaveBankAccount(BankAccount);

                    foreach (var item in PFRContactsList)
		                SaveContact(item);

                    //запоминаем оригинал после сохранения
                    _bankAccountCopy = BankAccount.Copy();
                    break;
		    }
		}

	    #endregion

		private void RefreshLists()
		{
			RefreshLists(false);
		}


		public void RefreshLists(bool onlyContacts)
		{
			bool wasChnaged = IsDataChanged;
			if (!onlyContacts) FedOsList = BLServiceSystem.Client.GetFedoListHib();
			if (Branch.ID != 0)
				PFRContactsList = BLServiceSystem.Client.GetPFRContactsListHib(Branch.ID);
			IsDataChanged = wasChnaged;
		}

		private const string ALREADY_EXISTS_RATING_NUMBER = "Регион с таким кодом уже существует.";
		private const string INVALID_REGION_NAME = "Не введено наименование региона";
		private const string INVALID_REGION_CODE = "Данное поле должно содержать число от 1 до 999";

        private const string INVALID_BA_LE_NAME = "Не введено наименование получателя платежа";
        private const string INVALID_BA_ACCOUNT_NUMBER = "Неверно введен номер рассчетного счета получателя платежа";
        private const string INVALID_BA_BANK_NAME = "Не введен банк получателя платежа";
        private const string INVALID_BA_BANK_LOCATION = "Не введено местонахождение банка получателя платежа";
        private const string INVALID_BA_COORESPONDENT_ACCOUNT_NUMBER = "Неверно введен корреспондентский счет банка получателя платежа";
        private const string INVALID_BA_BIK = "Неверно введен БИК банка получателя платежа";
        private const string INVALID_BA_INN = "Неверно введен ИНН банка получателя платежа";
        private const string INVALID_BA_KPP = "Неверно введен КПП банка получателя платежа";

        private const string REQUIRED_FIELD_WARNING = "Поле обязательно для заполнения";
        private const string ACCOUNT_NUMBER_STORAGE_MASK = @"\d{20}";

        public override string this[string columnName]
		{
			get
			{
				const string required = "Поле обязательное для заполнения";
				Regex regex;
                switch (columnName)
				{
					case "Name":
						return string.IsNullOrWhiteSpace(Name) ? INVALID_REGION_NAME : Name.ValidateMaxLength(512);
					case "Address":
						return Address.ValidateMaxLength(512);
					case "RegionNumber": 
						if (!RegionNumber.HasValue)
							return required;

						if (RegionNumber.Value <= 0 || RegionNumber.Value > 999)
							return INVALID_REGION_CODE;

						return _existsBranches.Any(x => x.RegionNumber == RegionNumber.Value) ? ALREADY_EXISTS_RATING_NUMBER : null;
					case "SelectedFedO":
						return SelectedFedO == null ? "Не выбран округ" : string.Empty;
					case "INN": return INN.ValidateRequired() ?? INN.ValidateOnlyNumeric() ?? INN.ValidateFixedLength(10);
					case "KPP": return KPP.ValidateRequired() ?? KPP.ValidateOnlyNumeric() ?? KPP.ValidateFixedLength(9);


                    case "BankAccountLegalEntityName":
                        return string.IsNullOrWhiteSpace(BankAccountLegalEntityName) ? INVALID_BA_LE_NAME : null;

                    case "BankAccountAccountNumber":
                        if (BankAccountAccountNumber == null)
                            return REQUIRED_FIELD_WARNING;

                        regex = new Regex(ACCOUNT_NUMBER_STORAGE_MASK);
                        return regex.IsMatch(BankAccountAccountNumber)
                            ? null
                            : INVALID_BA_ACCOUNT_NUMBER;

                    case "BankAccountBankName":
                        return string.IsNullOrWhiteSpace(BankAccountBankName) ? INVALID_BA_BANK_NAME : null;

                    case "BankAccountBankLocation":
                        return string.IsNullOrWhiteSpace(BankAccountBankLocation) ? INVALID_BA_BANK_LOCATION : null;

                    case "BankAccountCorrespondentAccountNumber":
                        if (BankAccountCorrespondentAccountNumber == null)
                            return REQUIRED_FIELD_WARNING;

                        regex = new Regex(ACCOUNT_NUMBER_STORAGE_MASK);
                        return regex.IsMatch(BankAccountCorrespondentAccountNumber)
                            ? null
                            : INVALID_BA_COORESPONDENT_ACCOUNT_NUMBER;

                    case "BankAccountBIK":
                        return BankAccountBIK == null || BankAccountBIK.Length != 9 ? INVALID_BA_BIK : null;

                    case "BankAccountINN":
                        return BankAccountINN == null || BankAccountINN.Length != 10 ? INVALID_BA_INN : null;

                    case "BankAccountKPP":
                        return BankAccountKPP == null || BankAccountKPP.Length != 9 ? INVALID_BA_KPP : null;
                }

                return string.Empty;
			}
		}

		private bool Validate()
		{
			return ValidateFields();
			

		}

		public bool HasContacts => _pfrContactsList.Count > 0 && CanExecuteDeleteContact();

	    public void CreatePFRContact()
		{
	        if (!DialogHelper.CreatePFRContact(ID)) return;
	        RefreshLists(true);
	        OnPropertyChanged("PFRContactsList");
		}
	}
}
