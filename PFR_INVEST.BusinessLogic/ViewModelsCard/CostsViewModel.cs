﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Helpers;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OFPR_manager)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
    public sealed class CostsViewModel : ViewModelCard, IUpdateRaisingModel
    {
        #region Fields

        public Cost Cost;

        private List<Currency> _currencyList;
        public List<Currency> CurrencyList => _currencyList ?? (_currencyList = DataContainerFacade.GetList<Currency>());

        public decimal? Summ
        {
            get
            {
                return Cost?.Sum;
            }
            set
            {
                if (Cost == null)
                    return;
                Cost.Sum = value;
                OnPropertyChanged("Summ");
                CalcTotal();
            }
        }

        public decimal Rate
        {
            get
            {
                if (SelectedCurrency == null) return 0;
                var curr = SelectedCurrency;
                var curs = SelectedCurs;
                if (CurrencyHelper.IsRUB(curr))
                    return 1;
                if (curs != null)
                    return curs.Value ?? 0;
                return 0;
            }
        }

        private void CalcTotal()
        {
            Total = Rate * Summ;
        }


        public decimal? SumPP
        {
            get { return Cost.PaymentSumm; }
            set
            {
                if (Cost == null)
                    return;
                Cost.PaymentSumm = value;
                OnPropertyChanged("SumPP");
            }
        }

        public DateTime? Date
        {
            get { return Cost?.Date; }
            set
            {
                if (Cost == null)
                    return;
                Cost.Date = value;
                OnPropertyChanged("Date");
            }
        }

        public DateTime? DatePP
        {
            get
            {
                return Cost?.PaymentDate;
            }

            set
            {
                if (Cost == null)
                    return;
                Cost.PaymentDate = value;
                SelectedCurs = GetCursForSelectedCurrency();
                OnPropertyChanged("DatePP");
                OnPropertyChanged("IsSelectCourceButtonEnabled");
            }
        }

        public string DiDate => Cost?.DIDate?.Date.ToShortDateString();

        private Curs _selCurs;
        public Curs SelectedCurs
        {
            get { return _selCurs; }
            set
            {
                if (_selCurs != value)
                {
                    _selCurs = value;
                    CalcTotal();
                }
                OnPropertyChanged("Rate");
                OnPropertyChanged("Total");
                OnPropertyChanged("IsSelectCourceButtonEnabled");
            }
        }

        public decimal? Total
        {
            get
            {
                return Cost?.Total;
            }
            set
            {
                if (Cost == null || !value.HasValue)
                    return;
                Cost.Total = decimal.Round(value.Value, 2);
                OnPropertyChanged("Total");
            }
        }

        private Currency _selCurrency;
        public Currency SelectedCurrency
        {
            get
            {
                if (_selCurrency == null && Cost.CurrencyID.HasValue)
                    _selCurrency = CurrencyList.First(c => c.ID == Cost.CurrencyID.Value);
                return _selCurrency;
            }
            set
            {
                _selCurrency = value;
                if (Cost == null)
                    return;
                if (value != null)
                    Cost.CurrencyID = value.ID;
                else
                    Cost.CurrencyID = null;
                SelectedCurs = GetCursForSelectedCurrency();
                OnPropertyChanged("SelectedCurrency");
                OnPropertyChanged("IsSelectCourceButtonEnabled");
                //this.CalcTotal();
            }
        }

        private PortfolioFullListItem _account;
        /// <summary>
        /// Счет для оплаты расходов
        /// </summary>
        public PortfolioFullListItem Account
        {
            get
            {
                return _account;
            }
            set
            {
                _account = value;
                if (Cost == null)
                    return;
                if (value != null)
                {
                    Cost.PortfolioID = value.Portfolio.ID;

                    Cost.AccountID = value.PfrBankAccount.ID;
                    //SelectedCurrency = value.PfrBankAccount.GetCurrency();
                }
                OnPropertyChanged("AccountNumber");
                OnPropertyChanged("Bank");
                OnPropertyChanged("PFName");
                OnPropertyChanged("Account");
            }
        }

        private readonly IList<LegalEntity> _bl;
        private long _cID;
        private IList<LegalEntity> _banksList;
        public IList<LegalEntity> BanksList
        {
            get
            {
                var cst = DataContainerFacade.GetByID<Cost>(_cID);
                if (cst == null) return _banksList;
                var currentBank = cst.GetLegalEntity();
                var selBIsContains = _bl.Any(legalEntity => currentBank == null || legalEntity.ID == currentBank.ID);
                if (selBIsContains) return _banksList;
                _banksList = _bl.Union(new[] { currentBank }).ToList();
                _selectedBank = currentBank;
                SelectedBank = currentBank;
                return _banksList;
            }
            set
            {
                _banksList = value;
                OnPropertyChanged("BanksList");
            }
        }

        private LegalEntity _selectedBank;
        public LegalEntity SelectedBank
        {
            get
            {
                if (_selectedBank == null && Cost.BankAccountID.HasValue)
                    _selectedBank = BanksList.First(b => b.ID == Cost.BankAccountID);
                return _selectedBank;
            }
            set
            {
                if (Cost == null) return;
                _selectedBank = value;
                if (_selectedBank == null) Cost.BankAccountID = null;
                else Cost.BankAccountID = _selectedBank.ID;
                OnPropertyChanged("SelectedBank");
            }
        }

        public List<string> KindList { get; } = CostIdentifier.GetList();

        public string Kind
        {
            get { return Cost?.Kind; }
            set
            {
                if (Cost == null)
                    return;
                Cost.Kind = value;
                OnPropertyChanged("Kind");
            }
        }

        public string RegNum
        {
            get { return Cost?.RegNum; }
            set
            {
                if (Cost == null)
                    return;
                Cost.RegNum = value;
                OnPropertyChanged("RegNum");
            }
        }

        public string PaymentNum
        {
            get { return Cost?.PaymentNumber; }
            set
            {
                if (Cost == null)
                    return;
                Cost.PaymentNumber = value;
                OnPropertyChanged("PaymentNum");
            }
        }

        public string Comment
        {
            get { return Cost?.Comment; }
            set
            {
                if (Cost == null)
                    return;
                Cost.Comment = value;
                OnPropertyChanged("Comment");
            }
        }

        public bool IsSelectCourceButtonEnabled => (SelectedCurs?.Date == null || !SelectedCurs.Date.Value.Equals(DatePP)) && Rate == 0;

        #endregion

        #region Execute implementation

        public override bool CanExecuteDelete()
        {
            return State == ViewModelState.Edit;
        }

        protected override void ExecuteDelete(int delType = 1)
        {
            if (Cost != null)
                DataContainerFacade.Delete<Cost>(Cost.ID);
            base.ExecuteDelete(DocOperation.Delete);
        }

        /// <summary>
        /// Проверка возможности выбора счета
        /// </summary>
        /// <returns>true, если выбирать можно</returns>
        private bool CanExecuteSelectAccount()
        {
            return true;
        }

        /// <summary>
        /// Открытие диалога выбора счета
        /// </summary>
        private void ExecuteSelectAccount()
        {
            var selected = DialogHelper.SelectPortfolio(true);
            if (selected != null)
                Account = selected;
        }

        protected override void ExecuteSave()
        {
            if (!CanExecuteSave()) return;
            switch (State)
            {
                case ViewModelState.Read:
                    break;
                case ViewModelState.Edit:
                    DataContainerFacade.Save(Cost);
                    break;
                case ViewModelState.Create:
                    ID = DataContainerFacade.Save(Cost);
                    Cost.ID = ID;
                    break;
            }
        }

        public override bool CanExecuteSave()
        {
            return string.IsNullOrEmpty(Validate()) && IsDataChanged;
        }
        #endregion

        #region Validation
        public override string this[string columnName]
        {
            get
            {
                const string errorMessage = "Неверное значение";
                switch (columnName.ToUpper())
                {
                    //case "DIDATE": return this.cost == null || !this.cost.DIDate.HasValue || this.cost.DIDate.Value.Year < 1900 ? errorMessage : null;
                    case "REGNUM": return RegNum.ValidateRequired() ?? RegNum.ValidateMaxLength(50);
                    case "KIND": return string.IsNullOrEmpty(Kind) ? errorMessage : null;
                    case "DATE": return !Date.HasValue || Date.Value.Year < 1900 ? errorMessage : null;
                    case "SUMM": return Summ.ValidateRequired() ?? Summ.ValidateNonNegative() ?? Summ.ValidateMaxFormat();
                    case "PAYMENTNUM": return PaymentNum.ValidateRequired() ?? PaymentNum.ValidateMaxLength(32);
                    case "DATEPP": return !DatePP.HasValue || DatePP.Value.Year < 1900 ? errorMessage : null;
                    case "SUMPP": return SumPP.ValidateRequired() ?? SumPP.ValidateNonNegative() ?? SumPP.ValidateMaxFormat();
                    case "RATE": return Rate > 0 ? null : errorMessage;
                    case "TOTAL": return Total.ValidateMaxFormat();
                    default: return null;
                }
            }
        }

        private string Validate()
        {
            const string errorMessage = "Неверный формат данных";

            return Cost?.AccountID == null || 
                !Cost.PortfolioID.HasValue || 
                !Cost.BankAccountID.HasValue || 
                string.IsNullOrEmpty(Cost.RegNum) || 
                string.IsNullOrEmpty(Cost.Kind) || 
                !Cost.Date.HasValue || 
                !Cost.CurrencyID.HasValue || 
                !Cost.Sum.HasValue || 
                Cost.Sum <= 0 || 
                !Cost.PaymentDate.HasValue || 
                string.IsNullOrEmpty(PaymentNum) || 
                PaymentNum.Trim().Length == 0 || 
                string.IsNullOrEmpty(RegNum) || 
                RegNum.Trim().Length == 0 || 
                !Cost.PaymentSumm.HasValue || 
                Cost.PaymentSumm.Value <= 0 || 
                !string.IsNullOrEmpty(Cost.Total.ValidateMaxFormat()) ||
                Rate <= 0
                ? errorMessage : null ;
        }
        #endregion

        #region Commands

        public ICommand SelectAccount { get; private set; }

        #endregion

        private Curs GetCursForSelectedCurrency()
        {
            if (SelectedCurrency == null || Cost?.PaymentDate == null)
                return null;

            var curses = BLServiceSystem.Client.GetOnDateCurrentCursesHib(Cost.PaymentDate ?? DateTime.Now).ToList();
            return curses.FirstOrDefault(c => c.CurrencyID == SelectedCurrency.ID && c.Date.Value.Equals(Cost.PaymentDate.Value.Date));
        }

        public void GetCourceManuallyOrFromCBRSite()
        {
            if (SelectedCurrency == null || Cost?.PaymentDate == null)
                return;

            var requestedRate = DialogHelper.GetCourceManuallyOrFromCBRSite(SelectedCurrency.ID, Cost.PaymentDate.Value);
            if (!requestedRate.HasValue) return;
            var rate = new Curs
            {
                Value = requestedRate.Value,
                Date = Cost.PaymentDate,
                CurrencyID = SelectedCurrency.ID
            };
            rate.ID = DataContainerFacade.Save<Curs, long>(rate);
            SelectedCurs = rate;
        }
        /// <summary>
        /// Загрузка о расходах
        /// </summary>
        /// <param name="lId">ID раходов в базе</param>
        public void Load(long lId)
        {
            ID = lId;
            _cID = lId;
            try
            {
                Cost = DataContainerFacade.GetByID<Cost>(lId);

                if (Cost == null) return;
                if (Cost.AccountID.HasValue && Cost.PortfolioID.HasValue)
                    Account = BLServiceSystem.Client.GetPortfolioFull(Cost.AccountID.Value, Cost.PortfolioID.Value);

                // http://jira.dob.datateh.ru/browse/DOKIPIV-1277?focusedCommentId=295840&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-295840
                _selCurs = GetCursForSelectedCurrency();
            }
            catch (Exception e)
            {
                if (DoNotThrowExceptionOnError == false)
                    throw;

                Logger.WriteException(e);
                throw new ViewModelInitializeException();
            }
        }

        public CostsViewModel()
            : base(typeof(CostsListViewModel))
        {
            DataObjectTypeForJournal = typeof(Cost);
            try
            {
                BanksList = BLServiceSystem.Client.GetBankAgentListForDeposit().ToList();
                _bl = new List<LegalEntity>(BanksList);
            }
            catch (Exception e)
            {
                if (DoNotThrowExceptionOnError == false)
                    throw;

                Logger.WriteException(e);
                throw new ViewModelInitializeException();
            }

            SelectAccount = new DelegateCommand(o => CanExecuteSelectAccount(),
                o => ExecuteSelectAccount());

            Cost = new Cost
            {
                PaymentDate = DateTime.Now,
                Date = DateTime.Now,
                DIDate = DateTime.Now
            };
            _selCurs = GetCursForSelectedCurrency();
        }

        public IList<KeyValuePair<Type, long>> GetUpdatedList()
        {
            return new List<KeyValuePair<Type, long>> {new KeyValuePair<Type, long>(typeof (Cost), Cost.ID)};
        }
    }
}
