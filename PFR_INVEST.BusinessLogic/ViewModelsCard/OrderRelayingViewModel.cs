﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.ListExtensions;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_manager)]
    public class OrderRelayingViewModel : OrderBaseViewModel
    {

        //public event EventHandler RequestClose;
        public event EventHandler CloseBindingWindows;
        public bool IsEventsRegisteredOnLoad = false;

        #region const
        private const string msgDuplicatePBADetected = "Выбранный портфель/счёт-источник уже присутствует в списке портфелей/счетов-получателей.\nПортфель/счёт-получатель будет удалён из списка.";
        private const string msgDuplicateCurrencyDetected = "Выбранный счёт-источник отличается типом валюты от счётов-получателуй\nСчета-получатели будут удалены из списка.";
        private const string strRelaying = "Перекладка";
        private const string rub = "Рубли";
        private const string CHOICE_SOURCE_PORTFOLIO = "Выберите портфель-источник";
        private const string CHOICE_SOURCE_PPERSON = "Выберите уполномоченного";
        private const string COUNT_SECURITY_FALSE = "Количество ЦБ, распределенных по портфелям, не совпало с суммарным значением";
        public string ColumnPrefix = "PFName";
        private const string ColumnAll = "Всего";


        #endregion

        #region properties
        private readonly List<AccBankType> _accBankType;
        private readonly List<Currency> _currency;
        private readonly List<CbOrder> _deleteOrderList;

        private List<BuyOrSaleReportsListItem> m_ReportsList;
        public List<BuyOrSaleReportsListItem> ReportsList
        {
            get
            {
                return m_ReportsList;
            }
            set
            {
                m_ReportsList = value;
                OnPropertyChanged("ReportsList");
                OnPropertyChanged("IsReportsListNoEmpty");
            }
        }

        public bool IsReportsListNoEmpty => ReportsList != null && ReportsList.Any();

        public bool CbInOrderListEmpty => !(CbInOrderList.Any());
        public bool IsSelectedDestination => SelectedDestinationRelaying != null && CbInOrderListEmpty;
        private DestinationRelaying selectedDestinationRelaying;
        public DestinationRelaying SelectedDestinationRelaying
        {
            get { return selectedDestinationRelaying; }
            set
            {
                selectedDestinationRelaying = value;
                OnPropertyChanged("SelectedDestinationRelaying");
                OnPropertyChanged("IsSelectedDestination");
                IsDataChanged = false;
            }
        }

        public bool RelayingVisible => CbInOrderList.Any();

        ////Temporary solution
        //public override DateTime? RegDateMinValue
        //{
        //    get
        //    {
        //        if (ID > 0)
        //            return RegDate;
        //        else
        //            return DateTime.Today.AddMonths(-1);
        //    }
        //}

        private string errorMessage;
        public string ErrorMessage
        {
            get
            {
                return errorMessage;
            }
            set
            {
                //if (string.Equals(errorMessage, value)) return;
                errorMessage = value;
                OnPropertyChanged("ErrorMessage");
            }
        }


        public SecurityInOrderType SecurityInOrderType => CurrencyIdentifier.IsRUB(Order.GetPfrBankAccount().GetCurrency().Name.Trim())
            ? SecurityInOrderType.SaleRoubles
            : SecurityInOrderType.SaleCurrency;

        private BindingList<CbInOrderListItem> m_CbInOrderList;
        public BindingList<CbInOrderListItem> CbInOrderList
        {
            get
            {
                return m_CbInOrderList;
            }
            set
            {
                m_CbInOrderList = value;
                OnPropertyChanged("IsSecuritySave");
                OnPropertyChanged("IsSelectedDestinationPortfolio");
                OnPropertyChanged("RelayingVisible");
                RaiseDestinationPortfolioCountChanged();
                OnPropertyChanged("CbInOrderList");
            }
        }


		private Person selectedPerson;
		public Person SelectedPerson
        {
            get
            {
				return selectedPerson;
            }
            set
            {
				selectedPerson = value;
                if (value != null)
                    Order.FIO_ID = value.ID;

				OnPropertyChanged("SelectedPerson");
            }
        }

        private PortfolioFullListItem selectedPortfolioBankAccount;
        public PortfolioFullListItem SelectedPortfolioBankAccount
        {
            get { return selectedPortfolioBankAccount; }
            set
            {
                selectedPortfolioBankAccount = value;

                if (value != null)
                { //проверка на валюту
                    if (DestinationRelayingList.Any(i => i.PfrBankAccount.CurrencyID != value.PfrBankAccount.CurrencyID))
                    {
                        DialogHelper.ShowError(msgDuplicateCurrencyDetected, strRelaying);

                        var data = DestinationRelayingList.Where(d => d.Order != null).Select(d => d.Order);
                        foreach (var cbOrder in data.Where(cbOrder => !_deleteOrderList.Contains(cbOrder))) {
                            _deleteOrderList.Add(cbOrder);
                        }


                        DestinationRelayingList.Clear();

                    }
                    //проверка на дубль
                    var duplicate = DestinationRelayingList.SingleOrDefault(i => i.BankLE.ID.Equals(value.BankLE.ID) &&
                        i.Portfolio.ID.Equals(value.PortfolioID) &&
                        i.PfrBankAccount.ID.Equals(value.AccountID) && i.PfrBankAccount.ID.Equals(value.PfrBankAccount.ID));
                    if (duplicate != null)
                    {
                        DialogHelper.ShowError(msgDuplicatePBADetected, strRelaying);
                        DestinationRelayingList.Remove(duplicate);

                    }
                    Order.DEPOAccountID = GetAccountDEPO(value.PfrBankAccount, value.Portfolio);
                    if (!Order.DEPOAccountID.HasValue) { return; }
                    if (Order.DEPOAccountID < 1)
                    {
                        return;
                    }


                    sourceAccount = value.AccountNumber;
                    sourceBank = value.Bank;
                    sourcePortfolio = value.PFName;

                    Order.RegNumLong = GenerateCounter(value.Portfolio.YearID, OrderTypeIdentifier.SELLING);
                    RegNum = GenerateOrderRegNum(value.Portfolio, value.BankLE, OrderTypeIdentifier.SHORT_SELLING, Order.RegNumLong);



                    Order.PortfolioID = value.Portfolio.ID;
                    Order.BankAgentID = value.BankLE.ID;

                    SetOrderAccountID(Order, value.PfrBankAccount);



                    OnPropertyChanged("SelectedPortfolioBankAccount");
                    OnPropertyChanged("SourceAccount");
                    OnPropertyChanged("SourceBank");
                    OnPropertyChanged("SourcePortfolio");
                    OnPropertyChanged("RegNum");
                }

            }
        }

        private void SetOrderAccountID(CbOrder ord, PfrBankAccount bankAccount)
        {
            if (_accBankType.Any())
            {
                ord.TradeAccountID = null;
                ord.CurrentAccountID = null;
                ord.TransitAccountID = null;

                var accType = _accBankType.FirstOrDefault(a => a.ID == bankAccount.AccountTypeID);
                if (accType != null)
                {

                    if (string.Equals(accType.Name, "торговый", StringComparison.InvariantCultureIgnoreCase))
                    {
                        ord.TradeAccountID = bankAccount.ID;
                    }
                    else if (string.Equals(accType.Name, "текущий", StringComparison.InvariantCultureIgnoreCase))
                    {
                        ord.CurrentAccountID = bankAccount.ID;
                    }
                    else if (string.Equals(accType.Name, "транзитный", StringComparison.InvariantCultureIgnoreCase))
                    {
                        ord.TransitAccountID = bankAccount.ID;
                    }
                    //  else if (string.Equals(accType.Name, "ДЕПО", StringComparison.InvariantCultureIgnoreCase))
                    //{
                    //    ord.DEPOAccountID = bankAccount.ID;
                    //}
                }


            }
        }

        long? GetAccountDEPO(PfrBankAccount bankAccount, Portfolio portfolio)
        {
            var res = BLServiceSystem.Client.GetPFRBankAccounts(bankAccount.LegalEntityBankID.Value, portfolio.ID, "ДЕПО", false);

            if (!res.Any())
            {
                DialogHelper.ShowAlert("Счет \"ДЕПО\" не найден");
                return null;
            }
            if (res.Count() > 1)
            {
                DialogHelper.ShowAlert("Для выбранного банка-портфеля счетов \"ДЕПО\" больше 1");
                return null;
            }
            var firstOrDefault = res.FirstOrDefault();
            if (firstOrDefault != null)
            {
                return firstOrDefault.ID;

            }
            return null;
        }

        private string sourceAccount;
        public string SourceAccount => sourceAccount;

        private string sourceBank;
        public string SourceBank => sourceBank;

        private string sourcePortfolio;
        public string SourcePortfolio => sourcePortfolio;


        private ObservableList<DestinationRelaying> destinationRelayingList = new ObservableList<DestinationRelaying>();
        public ObservableList<DestinationRelaying> DestinationRelayingList
        {
            get { return destinationRelayingList; }
            set
            {
                if (destinationRelayingList != value)
                {
                    destinationRelayingList = value;
                    OnPropertyChanged("DestinationRelayingList");
                }

            }
        }
        #endregion

        #region execution

        #region SelectedPortfolioBankAccount
        public ICommand SelectedPortfolioBankAccountCommand { get; set; }

        private void SelectedPortfolioBankAccountAction(object o)
        {
            List<string> currencyName = new List<string>();
            var firstOrDefault = _currency.FirstOrDefault(c => c.Name.ToLower().Contains(rub.ToLower()));
            if (firstOrDefault != null)
            {
                currencyName.Add(firstOrDefault.Name);
            }

			var portfolioAccountLink = new List<PortfolioIdentifier.PortfolioPBAParams.PortfolioPBAParamsPortfolioAccountLink>();
            if (DestinationRelayingList.Any())
            {
                //pfrBankAccountID.AddRange(this.destinationRelayingList.Select(dpf => dpf.PfrBankAccount.ID));
                portfolioAccountLink.AddRange(
                    destinationRelayingList.Select(
                        dpf =>
							new PortfolioIdentifier.PortfolioPBAParams.PortfolioPBAParamsPortfolioAccountLink
                            {
                                PortfolioID = dpf.Portfolio.ID,
                                PfrBankAccountID = dpf.PfrBankAccount.ID
                            }));
            }


            var p = new PortfolioIdentifier.PortfolioPBAParams
            {
                AccountType = new[] { "текущий", "торговый" },
                CurrencyName = currencyName.Any() ? currencyName.ToArray() : null,
                HiddenPortfolioAccountLinks = portfolioAccountLink.Any() ? portfolioAccountLink.ToArray() : null
                // http://jira.vs.it.ru/browse/PFRPTK-1430?focusedCommentId=204322&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-204322
                //BankPortfolioContainAccountType = "депо"
            };



            var portfolioAccount = DialogHelper.SelectPortfolio(p);
            if (portfolioAccount != null)
            {
                SelectedPortfolioBankAccount = portfolioAccount;
            }




        }
        #endregion

        #region Add/Delete Destination
        public ICommand AddDestinationPortfolioCommand { get; set; }

        private void AddDestinationPortfolioAction(object o)
        {
            List<long> pfrBankAccountID = new List<long>();
			var portfolioAccountLink = new List<PortfolioIdentifier.PortfolioPBAParams.PortfolioPBAParamsPortfolioAccountLink>();
            List<string> currencyName = new List<string>();

            if (SelectedPortfolioBankAccount != null)
            {
                //pfrBankAccountID.Add(SelectedPortfolioBankAccount.PfrBankAccount.ID);
				portfolioAccountLink.Add(new PortfolioIdentifier.PortfolioPBAParams.PortfolioPBAParamsPortfolioAccountLink
                {
                    PortfolioID = SelectedPortfolioBankAccount.Portfolio.ID,
                    PfrBankAccountID = SelectedPortfolioBankAccount.PfrBankAccount.ID
                });
            
            }


            if (DestinationRelayingList.Any())
            {                
				portfolioAccountLink.AddRange(destinationRelayingList.Select(dpf => new PortfolioIdentifier.PortfolioPBAParams.PortfolioPBAParamsPortfolioAccountLink
                {
                    PortfolioID = dpf.Portfolio.ID,
                    PfrBankAccountID = dpf.PfrBankAccount.ID
                }));
               
            }

            var firstOrDefault = _currency.FirstOrDefault(c => c.Name.ToLower().Contains(rub.ToLower()));
            if (firstOrDefault != null)
            {
                currencyName.Add(firstOrDefault.Name);
            }

            var p = new PortfolioIdentifier.PortfolioPBAParams
            {
                HiddenPfrBankAccountID = pfrBankAccountID.Any() ? pfrBankAccountID.ToArray() : null,
                AccountType = new[] { "текущий", "торговый" },
                CurrencyName = currencyName.Any() ? currencyName.ToArray() : null,
                HiddenPortfolioAccountLinks = portfolioAccountLink.Any() ? portfolioAccountLink.ToArray() : null
                // http://jira.vs.it.ru/browse/PFRPTK-1430?focusedCommentId=204322&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-204322
                //BankPortfolioContainAccountType = "депо"
            };

            var portfolioFullListItem = DialogHelper.SelectPortfolio(p);
            if (portfolioFullListItem != null)
            {
                var id = GetAccountDEPO(portfolioFullListItem.PfrBankAccount, portfolioFullListItem.Portfolio);
                if (!id.HasValue) { return; }
                if (id < 1)
                {
                    return;
                }


                long RegNumLong;
                if (destinationRelayingList.Any(dr => dr.Portfolio.YearID == portfolioFullListItem.Portfolio.YearID && string.Equals(dr.Portfolio.Year, portfolioFullListItem.Portfolio.Year)))
                {
					RegNumLong =
					   destinationRelayingList.Where(dr => string.Equals(dr.Portfolio.Year, portfolioFullListItem.Portfolio.Year) && dr.Portfolio.YearID == portfolioFullListItem.Portfolio.YearID)
						   .Select(dr => dr.Order.RegNumLong)
						   .Max() + 1;

                }
                else
                {
                    RegNumLong = GenerateCounter(portfolioFullListItem.Portfolio.YearID, OrderTypeIdentifier.BUYING);
                }

                DestinationRelaying destinationRelaying = new DestinationRelaying
                {

                    Portfolio = portfolioFullListItem.Portfolio,
                    BankLE = portfolioFullListItem.BankLE,
                    PfrBankAccount = portfolioFullListItem.PfrBankAccount,
                    AccountType = portfolioFullListItem.AccountType

                };


                destinationRelaying.Order.RegNumLong = RegNumLong;
                destinationRelaying.Order.RegNum = GenerateOrderRegNum(portfolioFullListItem.Portfolio, portfolioFullListItem.BankLE, OrderTypeIdentifier.SHORT_BUYING, RegNumLong);

                destinationRelayingList.Add(destinationRelaying);
                OnPropertyChanged("DestinationRelayingList");
                RefreshComment();

                RaiseDestinationPortfolioCountChanged();
            }

        }

		/// <summary>
		/// Перегенерирует номера отчетов и проверяет их изменение
		/// </summary>
		/// <returns>True - номера были изменены, false - номера были проверены и остались без изменений</returns>
		protected bool RegenerateRegNum()
		{
			bool changed = false;
			var grDest = destinationRelayingList.ToList()
				.Where(x => x.Order.ID <= 0) // выбираются только записи, которые не были сохранены в БД, только для них возможны коллизии с другими формами по генерируемым номерам
				.GroupBy(x => x.Portfolio.YearID);

			foreach (var g in grDest)
			{
				var newNum = GenerateCounter(g.Key, OrderTypeIdentifier.BUYING);
				if (g.First().Order.RegNumLong != newNum)
				{
					changed = true;

					var first = g.First();
					first.Order.RegNumLong = newNum;
					first.Order.RegNum = GenerateOrderRegNum(first.Portfolio, first.BankLE, OrderTypeIdentifier.SHORT_BUYING, newNum);
					for (int i = 1; i < g.Count(); i++)
					{
						var t = g.ElementAt(i);
						t.Order.RegNumLong = newNum + i;
						t.Order.RegNum = GenerateOrderRegNum(first.Portfolio, first.BankLE, OrderTypeIdentifier.SHORT_BUYING, newNum + i);
					}
				}
			}

			if (changed)
				DestinationRelayingList = new ObservableList<DestinationRelaying>(DestinationRelayingList);
				//OnPropertyChanged("DestinationRelayingList");

			if (Order.ID <= 0)
			{
				var sellNum = GenerateCounter(SelectedPortfolioBankAccount.Portfolio.YearID, OrderTypeIdentifier.SELLING);
				if (Order.RegNumLong != sellNum)
				{
					changed = true;
					Order.RegNumLong = sellNum;
					RegNum = GenerateOrderRegNum(SelectedPortfolioBankAccount.Portfolio, SelectedPortfolioBankAccount.BankLE, OrderTypeIdentifier.SHORT_SELLING, sellNum);
				}
			}

			return changed;
		}

        public ICommand DeleteDestinationPortfolioCommand { get; set; }

        private void DeleteDestinationPortfolioAction(object o)
        {
            if (SelectedDestinationRelaying != null)
            {
                var deleteItem = SelectedDestinationRelaying;
                if (deleteItem.Order != null)
                {
                    if (!_deleteOrderList.Contains(deleteItem.Order))
                    {
                        _deleteOrderList.Add(deleteItem.Order);
                    }

                }

                DestinationRelayingList.Remove(deleteItem);

                RefreshComment();

                RaiseDestinationPortfolioCountChanged();
                OnPropertyChanged("DestinationRelayingList");
            }
        }


        #endregion;

        #region AddSecurities
        public void AddSecurities(BindingList<CbInOrderListItem> items)
        {
            var temp = CbInOrderList;
            foreach (CbInOrderListItem item in items)
            {
                temp.Add(item);
            }

            CbInOrderList = temp;
            OnPropertyChanged("CbInOrderListEmpty");
            OnPropertyChanged("IsSelectedDestination");
        }

        public bool CanExecuteAddSecurities()
        {
			return ID > 0 &&
				OrderStatusIdentifier.IsStatusOnSigning(InitialStatus);
				// условие не имеет смысла, можно сначала добавить ЦБ, а потом поменять значения на форме
                //!this.IsDataChanged;
        }

        #endregion

        #region Save\Delete
        public override bool CanExecuteDelete()
        {
            return State != ViewModelState.Create;
        }

        protected override void ExecuteDelete(int delType)
        {

            if (Order != null)
            {
                if (!_deleteOrderList.Contains(Order))
                {
                    _deleteOrderList.Add(Order);
                }
            }

            var data = DestinationRelayingList.Where(dl => dl.Order != null).Select(dl => dl.Order);
            foreach (var cbOrder in data)
            {
                if (!_deleteOrderList.Contains(cbOrder))
                {
                    _deleteOrderList.Add(cbOrder);
                }
            }

            ExecuteDeleteOrders(_deleteOrderList);


            base.ExecuteDelete(DocOperation.Delete);
            if (CloseBindingWindows != null)
            {
                CloseBindingWindows(this, null);
            }


        }

        private void ExecuteDeleteOrders(List<CbOrder> deleteOrderList)
        {
            if (deleteOrderList == null) return;

            foreach (var order in deleteOrderList)
            {

                var cbInOrderList = order.GetCbInOrderList();
                if (cbInOrderList != null)
                {
                    foreach (CbInOrder cbinorder in cbInOrderList)
                    {
                        //отчеты
                        var reports = DataContainerFacade.GetListByProperty<OrdReport>("CbInOrderID", cbinorder.ID);
                        foreach (var item in reports)
                            DataContainerFacade.Delete(item);
                        DataContainerFacade.Delete(cbinorder);
                    }

                }
                DataContainerFacade.Delete(order);

            }
        }

        private void ExecuteSaveOrders()
        {
            Order.ID = DataContainerFacade.Save<CbOrder, long>(Order);
            ID = Order.ID;


            foreach (CbInOrderListItem cbinorderLI in CbInOrderList)
            {
                cbinorderLI.CbInOrder.OrderID = ID;
                cbinorderLI.CbInOrder.SecurityID = cbinorderLI.Security.ID;
                cbinorderLI.CbInOrder.ID = DataContainerFacade.Save<CbInOrder, long>(cbinorderLI.CbInOrder);
            }


            ExecuteDeleteOrders(_deleteOrderList);
            _deleteOrderList.Clear();

            var newDestinationRelayingList = new ObservableList<DestinationRelaying>();
            foreach (var item in DestinationRelayingList)
            {

                CbOrder orderDestination = new CbOrder
                {
                    ID = item.Order == null ? 0 : item.Order.ID,
                    BankAgentID = item.BankLE.ID,
                    Comment =
                        string.Format(
                            "Перекладка из портфеля {0}",
                            SelectedPortfolioBankAccount.Portfolio.Year),
                    Commissioner = Order.Commissioner,
                    DEPOAccountID = GetAccountDEPO(item.PfrBankAccount, item.Portfolio),
                    FIO_ID = Order.FIO_ID,
                    NoncompReq = Order.NoncompReq,
                    ParentId = Order.ID,
                    Place = Order.Place,
                    PortfolioID = item.Portfolio.ID,
                    PostID = Order.PostID,
                    RegDate = Order.RegDate,
                    RegNum = item.Order.RegNum,
                    RegNumLong = item.Order.RegNumLong,
                    Start = Order.Start,
                    StartTime = Order.StartTime,
                    Status = Order.Status,
                    Term = Order.Term,
                    //TradeAccountID выставляем в SetOrderAccountID ,
                    Type = OrderTypeIdentifier.BUYING
                };

                SetOrderAccountID(orderDestination, item.PfrBankAccount);

                orderDestination.ID = DataContainerFacade.Save<CbOrder, long>(orderDestination);
                item.Order = orderDestination;


                newDestinationRelayingList.Add(SaveCbinOrder(item));
                //ReportsList = BLServiceSystem.Client.GetBuyOrSaleReportsListHib(forSale, ID);
            }
            DestinationRelayingList = newDestinationRelayingList;
        }

        private DestinationRelaying SaveCbinOrder(DestinationRelaying destinationRelaying)
        {


            foreach (var propertyExpandoItem in PropertyExpandoItemsSource)
            {

                if (
                    destinationRelaying.CbInOrderListItems.Any(
                        cbio => cbio.CbInOrder.SecurityID == propertyExpandoItem.SecurityID))
                {

                    foreach (var orderID in propertyExpandoItem.OrderIDs)
                    {
                        if (Convert.ToInt64(orderID.Value) == destinationRelaying.Order.ID)
                        {
                            object obj;
                            if (((IDictionary<string, object>)propertyExpandoItem.Properties).TryGetValue(
                                orderID.Key, out obj))
                            {
                                var cbInOrderItem =
                                    destinationRelaying.CbInOrderListItems.SingleOrDefault(
                                        cbio => cbio.CbInOrder.SecurityID == propertyExpandoItem.SecurityID);

                                cbInOrderItem.CbInOrder.Count = Convert.ToInt64(obj);

                                DataContainerFacade.Save<CbInOrder, long>(cbInOrderItem.CbInOrder);
                            }

                        }

                    }


                }
                else
                {
                    var cbInOrderSale =
                        CbInOrderList.SingleOrDefault(
                            cbInOr => cbInOr.CbInOrder.SecurityID == propertyExpandoItem.SecurityID).CbInOrder;
                    foreach (var orderID in propertyExpandoItem.OrderIDs)
                    {
                        if (Convert.ToInt64(orderID.Value) == destinationRelaying.Order.ID)
                        {
                            object obj;
                            if (((IDictionary<string, object>)propertyExpandoItem.Properties).TryGetValue(
                                orderID.Key, out obj))
                            {

                                CbInOrder cbInOrder = new CbInOrder
                                {
                                    DateDI = cbInOrderSale.DateDI,
                                    OrderID = Convert.ToInt64(orderID.Value),
                                    Count = Convert.ToInt64(obj),
                                    MaxPriceBay = cbInOrderSale.MinPriceSell,
                                    SecurityID = propertyExpandoItem.SecurityID
                                };

                                cbInOrder.ID = DataContainerFacade.Save<CbInOrder, long>(cbInOrder);
                                var sec = DataContainerFacade.GetByID<Security>(cbInOrder.SecurityID);
                                CbInOrderListItem cbInOrderListItem = new CbInOrderListItem
                                {
                                    CbInOrder = cbInOrder,
                                    Security = sec,
                                    Currency =
                                        _currency.FirstOrDefault
                                        (
                                            c =>
                                            c.ID == sec.CurrencyID)
                                };
                                destinationRelaying.CbInOrderListItems.Add(cbInOrderListItem);
                            }
                        }
                    }
                }

            }
            return destinationRelaying;
        }

		protected override bool BeforeExecuteSaveCheck()
		{
			if (RegenerateRegNum() && !DialogHelper.ShowConfirmation(
					"Номера поручений были изменены, продолжить сохранение?"))
			{
				return false;
			}

			string destinationRegNum = "поручения на покупку:";
			foreach (var item in DestinationRelayingList)
			{
				destinationRegNum += "\n" + item.Order.RegNum;
			}

			if (State == ViewModelState.Create &&
				!DialogHelper.ShowConfirmation(
					string.Format("Создать поручение на продажу {0}\n{1}", Order.RegNum, destinationRegNum)))
			{
				return false;
			}

			return base.BeforeExecuteSaveCheck();
		}

        protected override void ExecuteSave()
        {

            if (OrderStatusIdentifier.IsStatusOnExecution(SelectedStatus) && !CbInOrderList.Any())
            {
                DialogHelper.ShowAlert("Чтобы перевести поручение на исполнение необходимо добавить хотя бы одну ценную бумагу!");
                return;
            }

            switch (State)
            {
                case ViewModelState.Read:
                    break;
                case ViewModelState.Edit:
                    ExecuteSaveOrders();
                    OnPropertyChanged("IsSecuritySave");
                    break;
                case ViewModelState.Create:
                    ExecuteSaveOrders();

                    break;
            }

            RefreshStatuses(SelectedStatus, OrderStatusIdentifier.PARTIALLY_EXECUTED);
            InitialStatus = Order.Status;
            //RefreshCanEditOrder();
            RefreshCanEditStatus();
        }

        public override bool CanExecuteSave()
        {
            return Validated() && IsDataChanged;
        }
        #endregion

        #endregion

        #region Validated


        public override string this[string columnName]
        {
            get
            {
                var bs = base[columnName];
                if (!String.IsNullOrEmpty(bs)) return bs;
                switch (columnName)
                {
                    case "SelectedPortfolioBankAccount":
                    case "SourceBank":
                    case "SourcePortfolio":
                        return SelectedPortfolioBankAccount == null ? CHOICE_SOURCE_PORTFOLIO : string.Empty;
					case "SelectedPerson":
						return SelectedPerson == null ? CHOICE_SOURCE_PPERSON : string.Empty;
                    case "ItemsSource":
                        return ErrorMessage;

                    default: return null;
                }
            }
        }

        private bool Validated()
        {
            return
                Validate() &&
                string.IsNullOrEmpty(this["SelectedPortfolioBankAccount"]) &&
                //string.IsNullOrEmpty(this["RegDate"]) &&
                //string.IsNullOrEmpty(this["Term"]) &&
                string.IsNullOrEmpty(this["SelectedStatus"]) &&
				string.IsNullOrEmpty(this["SelectedPerson"]) &&
                string.IsNullOrEmpty(this["ItemsSource"]) &&
                DestinationRelayingList != null &&
                DestinationRelayingList.Count > 0;
        }

        #endregion

        public override Type DataObjectTypeForJournal => typeof(CbOrder);

        public OrderRelayingViewModel(ViewModelState action, long lID)
        {
            State = action;
            _accBankType = DataContainerFacade.GetList<AccBankType>();
            _currency = DataContainerFacade.GetList<Currency>();
            _deleteOrderList = new List<CbOrder>();

            AddDestinationPortfolioCommand = new DelegateCommand(o => !IsReadOnly, AddDestinationPortfolioAction);
            DeleteDestinationPortfolioCommand = new DelegateCommand(o => !IsReadOnly, DeleteDestinationPortfolioAction);
            SelectedPortfolioBankAccountCommand = new DelegateCommand(o => !IsReadOnly, SelectedPortfolioBankAccountAction);

            if (State == ViewModelState.Create)
            {
                Order = new CbOrder
                {
                    RegDate = RegDate,
                    Start = RegDate,
                    DIDate = DateTime.Today,
                    Comment = "Перекладка",
                    Term = Term,
                    Type = OrderTypeIdentifier.SELLING
                };

                DestinationRelayingList = new ObservableList<DestinationRelaying>();
                CbInOrderList = new BindingList<CbInOrderListItem>();
            }
            else
            {
                ID = lID;
                Order = DataContainerFacade.GetByID<CbOrder, long>(ID);
                RegDate = Order.RegDate.Value;
                Comment = Order.Comment;
                Term = Order.Term.Value;
				SelectedPerson = Order.GetPerson();

                selectedPortfolioBankAccount = new PortfolioFullListItem
                {
                    Portfolio = Order.GetPortfolio(),
                    BankLE = Order.GetBankAgent(),
                    PfrBankAccount = Order.GetPfrBankAccount()

                };

                sourceAccount = selectedPortfolioBankAccount.AccountNumber;
                sourceBank = selectedPortfolioBankAccount.Bank;
                sourcePortfolio = selectedPortfolioBankAccount.PFName;

                DestinationRelayingList = new ObservableList<DestinationRelaying>(Order.GetChildrenList().Cast<CbOrder>().Select(o =>
                   new DestinationRelaying
                   {
                       Order = o,
                       Portfolio = o.GetPortfolio(),
                       BankLE = o.GetBankAgent(),
                       PfrBankAccount = o.GetPfrBankAccount(),
                       AccountType = o.GetPfrBankAccount().GetAccountType(),
                       CbInOrderListItems = BLServiceSystem.Client.GetCbInOrderList(o.ID)
                   }
                    ));


                CbInOrderList = new BindingList<CbInOrderListItem>(BLServiceSystem.Client.GetCbInOrderList(ID));
                RefreshReports();

            }



            RefreshStatuses(SelectedStatus, OrderStatusIdentifier.PARTIALLY_EXECUTED);
            InitialStatus = Order.Status;
            IsDataChanged = false;
            RefreshCanEditStatus();
            DivideSecurity();

        }

        public void RefreshReports()
        {
            bool alreadyChanged = IsDataChanged;
            List<BuyOrSaleReportsListItem> ReportsListSale = new List<BuyOrSaleReportsListItem>();
            List<BuyOrSaleReportsListItem> ReportsListBuy = new List<BuyOrSaleReportsListItem>();
            List<BuyOrSaleReportsListItem> ReportsListResult = new List<BuyOrSaleReportsListItem>();

            ReportsListSale = BLServiceSystem.Client.GetBuyOrSaleReportsListHib(true, ID);
            DestinationRelayingList.Select(d => d.Order).Where(o => o.ID > 0).ForEach(o =>
                ReportsListBuy.AddRange(BLServiceSystem.Client.GetBuyOrSaleReportsListHib(false, o.ID)));

            foreach (var saleReport in ReportsListSale)
            {
                ReportsListResult.Add(saleReport);
                ReportsListResult.AddRange(ReportsListBuy.Where(r => r.SecurityID == saleReport.SecurityID));
            }
            ReportsList = ReportsListResult;
            DivideSecurity();
            IsDataChanged = alreadyChanged;
        }


        #region Перекладка
        private BindingList<PropertyExpandoObject> propertyExpandoItemsSource = new BindingList<PropertyExpandoObject>();

        public BindingList<PropertyExpandoObject> PropertyExpandoItemsSource
        {
            get { return propertyExpandoItemsSource; }
            set
            {
                propertyExpandoItemsSource = value;
                OnPropertyChanged("propertyExpandoItemsSource");
            }
        }

        public EventHandler OnDestinationPortfolioCountChanged;
        public void RaiseDestinationPortfolioCountChanged()
        {
            if (OnDestinationPortfolioCountChanged != null)
            {
                DivideSecurity();
                OnDestinationPortfolioCountChanged(this, null);
            }

        }

        private void RefreshComment()
        {
            var destinationPortfolio = DestinationRelayingList.Select(x => x.Portfolio.Year).Distinct().ToList();
            switch (destinationPortfolio.Count)
            {
                case 0:
                    Comment = "Перекладка";
                    break;
                case 1:
                    Comment = "Перекладка в портфель " + destinationPortfolio.First();
                    break;
                default:
                    Comment = "Перекладка в портфели " + destinationPortfolio.Aggregate((x, y) => x + ", " + y);
                    break;
            }   
        }

        /// <summary>
        /// пересчитать кол-во ценных бумаг в источниках
        /// </summary>
        private void DivideSecurity()
        {
            PropertyExpandoItemsSource.Clear();
            CbInOrderList.ForEach(SetDataPropertyExpandoItem);

        }


        private void SetDataPropertyExpandoItem(CbInOrderListItem cbInOrder)
        {
            if (DestinationRelayingList.Count == 0) return;

            dynamic expandoProperty = new ExpandoObject();

            dynamic expandoOrderIDs = new ExpandoObject();

            //dynamic expandoAccountNumbers = new ExpandoObject();

            ((INotifyPropertyChanged)expandoProperty).PropertyChanged +=
            Program_PropertyChanged;

            expandoProperty.SecurityName = cbInOrder.SecurityName;
            expandoProperty.CurrencyName = cbInOrder.Currency.Name;



            int selector = 0;
            foreach (var dr in DestinationRelayingList)
            {
                long countSecurity = 0;
                if (dr.CbInOrderListItems.Any())
                {
                    var cbInOrderTemp =
                        dr.CbInOrderListItems
                           .Where(item => item.CbInOrder.SecurityID == cbInOrder.CbInOrder.SecurityID);
                    if (cbInOrderTemp.Any())
                    {
                        countSecurity = cbInOrderTemp.FirstOrDefault().Count.Value;
                    }
                    else
                    {
                        countSecurity = GetCountSecurity(
                            (int)cbInOrder.Count.Value, DestinationRelayingList.Count, selector++);
                    }

                }
                else
                {
                    countSecurity = GetCountSecurity(
                            (int)cbInOrder.Count.Value, DestinationRelayingList.Count, selector++);
                }

                string prefixDuplicate = string.Empty;

                ((IDictionary<String, Object>)expandoProperty).Add(String.Format("{0}{1}*{2}",
                    ColumnPrefix, dr.Portfolio.Name, dr.PfrBankAccount.AccountNumber).Replace(".", "@"), countSecurity);


                ((IDictionary<String, Object>)expandoOrderIDs).Add(String.Format("{0}{1}*{2}",
                   ColumnPrefix, dr.Portfolio.Name, dr.PfrBankAccount.AccountNumber).Replace(".", "@"), dr.Order.ID);

                //((IDictionary<String, Object>)expandoAccountNumbers).Add(String.Format("{0}{1}({2})",
                //    ColumnPrefix, dr.Portfolio.Name, dr.PfrBankAccount.AccountNumber), dr.PfrBankAccount.AccountNumber);

            }
            expandoProperty.Count = cbInOrder.Count;
			expandoProperty.PriceSell = cbInOrder.MinPriceSell.HasValue ? cbInOrder.MinPriceSell.Value.ToString("F4", CultureInfo.CreateSpecificCulture("ru-RU")) : string.Empty;


            PropertyExpandoItemsSource.Add(new PropertyExpandoObject
            {
                Properties = expandoProperty,
                //AccountNumbers = expandoAccountNumbers,
                OrderIDs = expandoOrderIDs,
                IsReadOnly = IsReportsListNoEmpty && ReportsList.Any(r => r.SecurityID == cbInOrder.Security.ID),
                SecurityID = cbInOrder.CbInOrder.SecurityID,
                MaxValue = cbInOrder.Count.HasValue ? cbInOrder.Count.Value : -1
            });



        }


        int GetCountSecurity(int sum, int count, int selector)
        {
            if (sum == 0) return 0;
            if (sum % count == 0)
            {
                return sum / count;
            }
            if (sum / count > 0)
            {
                int res = sum / count;
                if (count == selector + 1)
                {
                    return sum - res * (selector);
                }
                return res;
            }
            return sum >= selector + 1 ? 1 : 0;
        }


        /// <summary>
        /// Изменение значения поля в  таблице "Перекладка бумаг"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Program_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Contains(ColumnPrefix))
            {

                errorMessage = null;
                PropertyExpandoItemsSource.ForEach(
                   propertyExpandoItem =>
                   {
                       var dictionary = (IDictionary<string, object>)propertyExpandoItem.Properties;
                       decimal count = propertyExpandoItem.MaxValue;
                       decimal sum = dictionary.Where(d => d.Key.Contains(ColumnPrefix)).Sum(d => Convert.ToDecimal(d.Value));
                       if (sum != count)
                       {
                           errorMessage = COUNT_SECURITY_FALSE;
                       }

                   });

                OnPropertyChanged("ErrorMessage");
                //var dictionary = (IDictionary<string, object>)sender;

                //object v;
                //if (dictionary.TryGetValue("Count", out v))
                //{
                //    decimal count = Convert.ToDecimal(v);
                //    decimal sum = dictionary.Where(d => d.Key.Contains(ColumnPrefix)).Sum(d => Convert.ToDecimal(d.Value));
                //    if (sum != count)
                //    {
                //        ErrorMessage = COUNT_SECURITY_FALSE;
                //    }
                //    else
                //    {
                //        ErrorMessage = null;
                //    }
                //}
            }

            IsDataChanged = true;
        }




        #endregion

    }


    public class DestinationRelaying
    {
        public string PFName => Portfolio.Year;
        public string Bank => BankLE.FormalizedName;
        public string AccountNumber => PfrBankAccount.AccountNumber;

        public CbOrder Order { get; set; }
        public List<CbInOrderListItem> CbInOrderListItems { get; set; }
        public AccBankType AccountType { get; set; }
        public Portfolio Portfolio { get; set; }
        public LegalEntity BankLE { get; set; }
        public PfrBankAccount PfrBankAccount { get; set; }
        public string RegNum => Order.RegNum;


        public DestinationRelaying()
        {
            Order = new CbOrder();
            CbInOrderListItems = new List<CbInOrderListItem>();
            AccountType = new AccBankType();
            Portfolio = new Portfolio();
            BankLE = new LegalEntity();
            PfrBankAccount = new PfrBankAccount();

        }

    }

    public class PropertyExpandoObject : INotifyPropertyChanged, IDataErrorInfo
    {
        private ExpandoObject properties;
        public ExpandoObject Properties
        {
            get
            {
                return properties;
            }
            set
            {
                properties = value;
            }
        }


        public ExpandoObject OrderIDs { get; set; }

        //public ExpandoObject AccountNumbers { get; set; }

        public long? SecurityID { get; set; }

        public decimal MaxValue { get; set; }

        public bool IsReadOnly { get; set; }


        public event PropertyChangedEventHandler PropertyChanged;

        public string this[string columnName]
        {

            get
            {
                if (columnName.Contains("PFName"))
                {
                    var dictionary = Properties as IDictionary<string, object>;
                    object valueObj;
                    decimal value;

                    if (dictionary.TryGetValue(columnName.Replace("Properties.", string.Empty), out valueObj) &&
                      decimal.TryParse(valueObj.ToString(), out value))
                    {
                        if (value < 0)
                        {
                            return "значение не может быть меньше 0";
                        }
                        if (value > MaxValue)
                        {
                            return string.Format("значение не может быть больше {0}", MaxValue);
                        }
                    }
                }
                return null;
            }
        }

        public string Error { get; private set; }


        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
