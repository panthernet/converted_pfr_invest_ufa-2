﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OVSI_worker)]
	[DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_manager)]
	public class F10ViewModel : ViewModelCard
	{
		#region Fields

		private List<NetWealthsInnerListItem> netWealthsList;
		public List<NetWealthsInnerListItem> NetWealthsList
		{
			get
			{
				return netWealthsList;
			}

			set
			{
				netWealthsList = value;
				OnPropertyChanged("NetWealthsList");
			}
		}

		private EdoOdkF010 odk;
		public EdoOdkF010 Odk
		{
			get { return odk; }
			set
			{
				odk = value;
				OnPropertyChanged("Odk");
			}
		}

		private Contract contract;
		public Contract Contract
		{
			get { return contract; }
			set
			{
				contract = value;
				OnPropertyChanged("Contract");
			}
		}

		private LegalEntity legalEntity;
		public LegalEntity LegalEntity
		{
			get { return legalEntity; }
			set
			{
				legalEntity = value;
				OnPropertyChanged("LegalEntity");
			}
		}


		public string PortfolioName => Odk.Portfolio ?? Contract.PortfolioName;

	    #endregion

		#region Execute implementation

		public override bool CanExecuteSave() { return false; }

		protected override void ExecuteSave() { }
		#endregion

		#region Validation
		public override string this[string columnName] => "";

	    #endregion

		public F10ViewModel(long id)
			: base(typeof(NetWealthsListViewModel))
		{
            DataObjectTypeForJournal = typeof(EdoOdkF010);
            if (id > 0)
			{
				ID = id;
				Odk = DataContainerFacade.GetByID<EdoOdkF010, long>(id);
				Contract = Odk.GetContract();
				LegalEntity = Contract.GetLegalEntity();
				RefreshNetWealthsList();
			}
		}

		public void RefreshNetWealthsList()
		{
			List<NetWealthsInnerListItem> tmpList = new List<NetWealthsInnerListItem>();

			tmpList.Add(new NetWealthsInnerListItem("1. Активы, денежные средства (010):", Odk.Funds1, Odk.Funds2));
			tmpList.Add(new NetWealthsInnerListItem("2. Активы, депозиты (020):", Odk.Deposit1, Odk.Deposit2));
			tmpList.Add(new NetWealthsInnerListItem("3. Ценные бумаги (030):", Odk.Cb1, Odk.Cb2));
			tmpList.Add(new NetWealthsInnerListItem("3.1. Государственные ценные бумаги (031):", Odk.Gcbrf1, Odk.Gcbrf2));
			tmpList.Add(new NetWealthsInnerListItem("3.2. Государственные ценные бумаги субъектов РФ (032):", Odk.Gcbsubrf1, Odk.Gcbsubrf2));
			tmpList.Add(new NetWealthsInnerListItem("3.3. Облигации муниципальных образований (033):", Odk.Obligaciimo1, Odk.Obligaciimo2));
			tmpList.Add(new NetWealthsInnerListItem("3.4. Облигации российских хозяйсвтенных обществ (034):", Odk.Obligaciirho1, Odk.Obligaciirho2));
			tmpList.Add(new NetWealthsInnerListItem("3.5. Акции российских эмитентов в форме (035):", Odk.Akciire1, Odk.Akciire2));
			tmpList.Add(new NetWealthsInnerListItem("3.6. Паи индексных инвестиционных фондов (036):", Odk.Pai1, Odk.Pai2));
			tmpList.Add(new NetWealthsInnerListItem("3.7. Ипотечные ценные бумаги (037):", Odk.Ipotech1, Odk.Ipotech2));
			tmpList.Add(new NetWealthsInnerListItem("4. Дебиторская задолженность (040):", Odk.Debit1, Odk.Debit2));
			tmpList.Add(new NetWealthsInnerListItem("4.1. Средства предоставленные профессиональным участникам (041):", Odk.Prof1, Odk.Prof2));
			tmpList.Add(new NetWealthsInnerListItem("4.2. Дебиторская задолженность по процентному доходу (042):", Odk.Proc1, Odk.Proc2));
			tmpList.Add(new NetWealthsInnerListItem("4.3. Прочая дебиторская задолженность (043):", Odk.OtherDebit1, Odk.OtherDebit2));
			tmpList.Add(new NetWealthsInnerListItem("5. Прочие активы (050):", Odk.OtherActiv1, Odk.OtherActiv2));
			tmpList.Add(new NetWealthsInnerListItem("6. Итого ИМУЩЕСТВА (060):", Odk.Itogo1, Odk.Itogo2));
			tmpList.Add(new NetWealthsInnerListItem("7. Кредиторская задолженность (070):", Odk.Credit1, Odk.Credit2));
			tmpList.Add(new NetWealthsInnerListItem("7.1. Обязательства перед проф. участниками (071):", Odk.ProfObiaz1, Odk.ProfObiaz2));
			tmpList.Add(new NetWealthsInnerListItem("7.2. Кредиторская задолженность по возмещению расходов (072):", Odk.CreditReward1, Odk.CreditReward2));
			tmpList.Add(new NetWealthsInnerListItem("7.3. Прочая кредиторская задолженность (073):", Odk.OtherCredit1, Odk.OtherCredit2));
			tmpList.Add(new NetWealthsInnerListItem("8. Итого сумма обязательств (080):", Odk.ItogoObiaz1, Odk.ItogoObiaz2));
			tmpList.Add(new NetWealthsInnerListItem("9. Итого стоимость чистых активов (090):", Odk.ItogoScha1, Odk.ItogoScha2));
			NetWealthsList = tmpList;
		}

		private const string TABLE_NAME = "EDO_ODKF010.";
	}
}
