﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.Common.Extensions;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OFPR_worker)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
	[DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager)]
    public class FinRegisterViewModel : ViewModelCard, IRequestCloseViewModel, IUpdateListenerModel, IUpdateRaisingModel, IRequestCustomAction
	{
		public event EventHandler RequestClose;

		private const decimal MAX_DECIMAL_19_4 = 1000000000000000;

		protected Finregister Finregister;

		protected Register Register;

		/// <summary>
		/// Является ли текущий финреестр дочерним от финреестра в статусе Не передан
		/// </summary>
		public bool IsChildFinregister => Finregister?.ParentFinregisterID != null;


	    public virtual Visibility TempAllocationVisibility => Visibility.Collapsed;
	    public bool TempAllocationVisibilityBool => TempAllocationVisibility == Visibility.Visible;
	    public virtual Visibility CommonFinregisterVisibility => Visibility.Visible;

	    public bool CommonFinregisterVisibilityBool => CommonFinregisterVisibility == Visibility.Visible;

	    public bool IsInputPropsCountBlocked { get; set; }

        /// <summary>
        /// Имеет ли родительский реестр флаг принудительного нахождения в архиве
        /// </summary>
        public bool IsForcedArchiveRegister => Register != null && Register.IsCustomArchive == 1;

	    public event EventHandler RequestCustomAction;

	    protected void OnRequestCustomAction()
	    {
	        RequestCustomAction?.Invoke(null, null);
	    }

		#region FinregisterID
		public long? FinregisterID => Finregister?.ID;

	    #endregion

		#region RegisterID
		public long? RegisterID => Finregister != null ? Finregister.RegisterID : null;

	    #endregion

		#region RegisterKind
		public string RegisterKind => Register != null ? Register.Kind : null;

	    #endregion

		#region NPF

		private List<LegalEntity> _npfList = new List<LegalEntity>();

		//private List<LegalEntity> npfList
		//{
		//    get 
		//    { 
		//        return _npfList; 
		//    }
		//    set
		//    {
		//        _npfList = value;
		//    }
		//}

		public List<LegalEntity> NPFList
		{
			get { return _npfList; }
			set
			{
				_npfList = value;
				OnPropertyChanged("NPFList");
			}
		}

		private LegalEntity _selectedNPF;
		public LegalEntity SelectedNPF
		{
			get { return _selectedNPF; }
			set
			{
				_selectedNPF = value;
				OnPropertyChanged("SelectedNPF");
			}
		}
		#endregion

		#region GarantACB

		private List<Element> _npfGarantList = new List<Element>();
		public List<Element> NPFGarantList
		{
			get { return _npfGarantList; }
			set
			{
				_npfGarantList = value;
				OnPropertyChanged("NPFGarantList");
			}
		}

		private Element _selectedNPFGarant;
		public Element SelectedNPFGarant
		{
			get { return _selectedNPFGarant; }
			set
			{
				_selectedNPFGarant = value;
				FilterNPFList();
				OnPropertyChanged("SelectedNPFGarant");
				OnPropertyChanged("IsActiveNPFStatus");
				OnPropertyChanged("IsInactiveNPFStatus");
				OnPropertyChanged("IsStatusFieldEnabled");
			}
		}
		#endregion

		#region StatusNPF
		private List<Status> _statusNPFList = new List<Status>();
		public List<Status> StatusNPFList
		{
			get { return _statusNPFList; }
			set
			{
				_statusNPFList = value;
				FilterNPFList();
				OnPropertyChanged("StatusNPFList");
			}
		}

		private Status _selectedStatusNpf;
		public Status SelectedStatusNPF
		{
			get { return _selectedStatusNpf; }
			set
			{
				if (SelectedStatusNPF != null && value != null && !StatusIdentifier.IsNPFStatusActivityCarries(SelectedStatusNPF.Name) && StatusIdentifier.IsNPFStatusActivityCarries(value.Name))
				{
					RestoreActiveFinregister();
				}

				_selectedStatusNpf = value;
				FilterNPFList();
				OnPropertyChanged("SelectedStatusNPF");
				OnPropertyChanged("IsActiveNPFStatus");
				OnPropertyChanged("IsInactiveNPFStatus");
				OnPropertyChanged("IsActiveNPFStatusBool");
				OnPropertyChanged("IsStatusFieldEnabled");
				OnPropertyChanged("IsNpfGarantVisible");


			}
		}

		/// <summary>
		/// Видимость поля Система гарантирования
		/// </summary>
		public bool IsNpfGarantVisible => IsActiveNPFStatusBool;

	    /// <summary>
		/// Выбрано состояние НПФ: Деятельность осуществляется
		/// </summary>
		public bool IsActiveNPFStatusBool => _selectedStatusNpf != null && StatusIdentifier.IsNPFStatusActivityCarries(_selectedStatusNpf.Name) && !RegisterIdentifier.FinregisterStatuses.IsNotTransferred(Status);

	    public string IsActiveNPFStatus => IsActiveNPFStatusBool ? "True" : "False";

	    public string IsActiveNPFStatusReadonly => (IsReadOnly || IsActiveNPFStatusBool) ? "True" : "False";

	    public string IsInactiveNPFStatus => ((IsForDeletedNPF && RegisterIdentifier.FinregisterStatuses.IsTransferred(Finregister.Status)) || IsActiveNPFStatusBool || RegisterIdentifier.FinregisterStatuses.IsNotTransferred(Finregister.Status) || IsAvailableInBoNotComplete) ? "False" : "True";

	    public bool IsInactiveNPFStatusBool => IsInactiveNPFStatus == "True";
	    #endregion

		#region Content
        [DisplayName("Содержание операции")]
		public string Content
		{
			get
			{
				return Register != null ? Register.Content : "";
			}

			set { }
		}
		#endregion

		#region Count
		public string CountLabelContent => RegisterIdentifier.IsFromNPF(Register.Kind) ? "Сумма СПН:" : "Сумма СПН*:";

	    public decimal? Count
		{
			get
			{
				return Finregister?.Count;
			}

			set
			{
				Finregister.Count = value ?? 0;
				OnPropertyChanged("Count");
			}
		}

		public bool IsCountReadOnly
		{
			get
			{
				if (IsReturnedFromBackOffice) return true;
				//return isReadOnlyFinregister || (corrList != null && corrList.Any());
				if (_isVirtualAddNpfMode)
					return false;

				return IsReadOnlyFinregister || !(RegisterIdentifier.FinregisterStatuses.IsCreated(Finregister.Status) || RegisterIdentifier.FinregisterStatuses.IsNotTransferred(Finregister.Status));
			}
		}

		/// <summary>
		/// Если финрееcтр доступен в БО и п/п не залочены
		/// </summary>
		public bool IsAvailableInBoNotComplete
		{
			get
			{
				//передача
				if (!string.IsNullOrEmpty(RegisterKind) && RegisterIdentifier.IsToNPF(Register.Kind))
					return State == ViewModelState.Edit && Register.TrancheDate != null && (!DraftSum.HasValue || DraftSum != Count);
				return State == ViewModelState.Edit && Status != null && RegisterIdentifier.FinregisterStatuses.IsIssued(Status) && (!DraftSum.HasValue || DraftSum != Count);
			}
		}

		/// <summary>
		/// Если финреестр вернулся из БО c заполненными п/п
		/// </summary>
		public bool IsReturnedFromBackOffice => _draftsList.Count > 0 && !IsAvailableInBoNotComplete;

	    #endregion

		#region CFRCount
		public virtual bool IsCFRCountVisible => false;

	    public decimal? CFRCount
		{
			get
			{
				return Finregister != null ? Finregister.CFRCount : null;
			}

			set
			{
				if (Finregister == null) return;
				Finregister.CFRCount = Convert.ToDecimal(value);
				//corr.CurrentSum = Convertion.ToDecimal(value);
				OnPropertyChanged("CFRCount");
			}
		}
		#endregion

		#region Status
        [DisplayName("Состояние")]
		public string Status
		{
			get
			{
				return Finregister != null ? Finregister.Status : string.Empty;
			}

			set
			{
				if (Finregister == null)
					return;

				if (value == null)
					throw new Exception("Статус финреестра невозможно сбросить в NULL");

				if (Finregister.Status == null)
				{
					var msg = "Статус финреестра: " + value;
					JournalLogger.LogChangeStateEvent(this, msg);
				}
				else
				{
					if (Finregister.Status.Equals(value))
						return;

					var msg = "Статус финреестра: " + Finregister.Status + " -> " + value;
					JournalLogger.LogChangeStateEvent(this, msg);
				}

				Finregister.Status = value;
				OnPropertyChanged("IsCountReadOnly");
				OnPropertyChanged("Status");
			}
		}
		#endregion

		#region FinStatuses

		private List<string> _finStatuses;
		public List<string> FinStatuses
		{
			get
			{
				return _finStatuses;
			}

			set
			{
				_finStatuses = value;
				OnPropertyChanged("FinStatuses");
			}
		}

		#endregion

		#region TrancheDate
        [DisplayName("Дата формирования заявки")]
		public DateTime? TrancheDate
		{
			get { return Register != null && RegisterIdentifier.IsToNPF(Register.Kind.ToLower()) ? Register.TrancheDate : null; }
			set
			{
				if (Register == null || !RegisterIdentifier.IsToNPF(Register.Kind.ToLower())) return;
				Register.TrancheDate = value;
				OnPropertyChanged("TrancheDate");
			}
		}

		public Visibility TrancheDateVisibility => Register != null && RegisterIdentifier.IsToNPF(Register.Kind.ToLower()) ? Visibility.Visible : Visibility.Collapsed;
	    public bool TrancheDateVisibilityBool => TrancheDateVisibility == Visibility.Visible;
	    #endregion

		#region RegNum

        [DisplayName("Номер документа")]
		public string RegNum
		{
			get { return Finregister.RegNum; }
			set
			{
				Finregister.RegNum = value;
				OnPropertyChanged("RegNum");
				OnPropertyChanged("Date");
			}
		}
		#endregion

		#region Date

		public DateTime? Date
		{
			get { return Finregister.Date; }
			set
			{
				Finregister.Date = value;
				OnPropertyChanged("RegNum");
				OnPropertyChanged("Date");
			}
		}
		#endregion

		#region ZLCount
        [Required]
        [DisplayName("Количество ЗЛ по документу")]
		public long? ZLCount
		{
			get
			{
				return Finregister?.ZLCount;
			}

			set
			{
				if (Finregister == null) return;
                Finregister.ZLCount = Convert.ToInt64(value);
				OnPropertyChanged("ZLCount");
			}
		}

		/// <summary>
		/// Определяет доступность кол-ва ЗЛ для ввода
		/// </summary>
		public bool IsZLCountReadOnly => IsReturnedFromBackOffice || IsReadOnlyFinregister;

	    #endregion

		#region FinMoveType
        [DisplayName("Способ передачи документа")]
		public string FinMoveType
		{
			get
			{
				return Finregister != null ? Finregister.FinMoveType : "";
			}

			set
			{
				if (Finregister == null) return;
				Finregister.FinMoveType = value;
				OnPropertyChanged("FinMoveType");
			}
		}
		#endregion

		#region FinMoveTypes

		private List<string> _finMoveTypes;
		public List<string> FinMoveTypes
		{
			get
			{
				return _finMoveTypes;
			}
			set
			{
				_finMoveTypes = value;
				OnPropertyChanged("FinMoveTypes");
			}
		}

		#endregion

		#region FinDate
        [DisplayName("Дата передачи документа")]
		public DateTime? FinDate
		{
			get
			{
				return Finregister?.FinDate;
			}

			set
			{
				if (Finregister == null) return;
				Finregister.FinDate = value;
				OnPropertyChanged("FinDate");
			}
		}
		#endregion

		#region FinNum
        [DisplayName("Номер сопроводительного письма")]
		public string FinNum
		{
			get { return Finregister?.FinNum ?? ""; }

			set
			{
				if (Finregister == null) return;
				Finregister.FinNum = value;
				OnPropertyChanged("FinNum");
			}
		}
		#endregion

		#region PPList
		//private List<Contragent> ppList;
		//public List<Contragent> PPList
		//{
		//    get
		//    {
		//        return this.ppList;
		//    }

		//    set
		//    {
		//        this.ppList = value;
		//        base.OnPropertyChanged("PPList");
		//    }
		//}
		#endregion


		#region DraftsList
		private List<AsgFinTr> _draftsList = new List<AsgFinTr>();
		public List<AsgFinTr> DraftsList
		{
			get
			{
				return _draftsList;
			}

			set
			{
				_draftsList = value;
				OnPropertyChanged("DraftsList");
			}
		}

		public Visibility DraftsListVisibility => _draftsList != null && _draftsList.Count > 0 ? Visibility.Visible : Visibility.Collapsed;
	    public bool DraftsListVisibilityBool => DraftsListVisibility == Visibility.Visible;
	    #endregion

		#region DraftSum
		public decimal? DraftSum
		{
			get
			{
				if (_draftsList != null && _draftsList.Count > 0)
				{
					decimal? sum = null;
					foreach (AsgFinTr dr in _draftsList)
					{
						if (sum == null && dr.DraftAmount != null)
							sum = 0;
						sum += dr.DraftAmount;
					}
					return sum;
				}
				return null;
			}
		}
		#endregion

		#region Comment

        [DisplayName("Комментарий")]
		public string Comment
		{
			get { return Finregister?.Comment ?? string.Empty; }

			set
			{
				if (Finregister == null) return;
				Finregister.Comment = value;
				OnPropertyChanged("Comment");
			}
		}
		#endregion

		#region Execute Implementations

		public override bool CanExecuteDelete()
		{
			//Не удаляем финреестр в статусе "финреестр не передан", если у него есть дочерние финреестры
			if (_isNotTransferred && NT_NpfList.Any()) return false;
			//проверяем на наличие П/П, удаляем только если их нет - ОТМЕНЯЕТСЯ. Разрешаем удалять, п/п теряют связь.
			return State == ViewModelState.Edit;// && !DraftsList.Any();
		}

		protected override void ExecuteDelete(int delType)
		{
			BLServiceSystem.Client.DeleteFinregister(Finregister.ID);
			/*foreach (var item in DataContainerFacade.GetListByProperty<AsgFinTr>("FinregisterID", Finregister.ID))
				DataContainerFacade.Delete(item);
			// DataContainerFacade.Delete<Register>(register);
			var e = Finregister.ExtensionData;
			Finregister.ExtensionData = null;
			DataContainerFacade.Delete(Finregister);
			Finregister.ExtensionData = e;*/
			base.ExecuteDelete(DocOperation.Archive);
		}

		public override bool CanExecuteSave()
		{
			return string.IsNullOrEmpty(Validate()) && IsDataChanged;
		}

	    public void SetTransferedFlag(int value)
	    {
	        Finregister.IsTransfered = value;
	    }

		private void ExecuteSaveInternal()
		{
			BindNPFAccounts();
			FinNum = (FinNum ?? string.Empty).Trim();
			RegNum = (RegNum ?? string.Empty).Trim();

			//сохранение выбранных сущностей для статуса Финреестр не передан
			if (IsNotTransferred && Finregister != null)
			{
				Finregister.SelectedContentID = NT_Content == null ? null : (long?)NT_Content.ID;
				Finregister.SelectedRegisterID = NT_Register == null ? null : (long?)NT_Register.ID;
			}
			switch (State)
			{
				case ViewModelState.Read:
					break;
				case ViewModelState.Edit:
#if !WEBCLIENT
                    var ee = Finregister.ExtensionData;
					Finregister.ExtensionData = null;
#endif
				    //обновляем статус передачи для финреестра в статусе не передан
				    if (IsNotTransferred)
				        SetTransferedFlag(NT_NpfList.Count == 0 || !NT_NpfList.All(a=> RegisterIdentifier.FinregisterStatuses.IsTransferred(a.Status)) ? 0 : 1);
				    else
				    {
				        //обновляем статус передачи для финреестра в остальных статусах
				        SetTransferedFlag(RegisterIdentifier.FinregisterStatuses.IsTransferred(Finregister.Status) ? 1 : 0);
				        //обновляем статус передачи для родительского финреестра
				        if (_ntParentId != 0)
				        {
				            bool result = DataContainerFacade.GetListByProperty<Finregister>("ParentFinregisterID", _ntParentId, true)
				                .All(a => RegisterIdentifier.FinregisterStatuses.IsTransferred(a.Status));
				            BLServiceSystem.Client.UpdateFinregisterIsTransferred(_ntParentId, result ? 1 : 0);
				        }
				    }
					ID = DataContainerFacade.Save<Finregister, long>(Finregister);
#if !WEBCLIENT
					Finregister.ExtensionData = ee;
#endif

					break;
				case ViewModelState.Create:

					//Если добавляем НПФ для финреестра в статусе "Финреестр не передан" - запоминаем ID родителя
					if (_isVirtualAddNpfMode)
						Finregister.ParentFinregisterID = _ntParentId;

#if !WEBCLIENT
					var ce = Finregister.ExtensionData;
					Finregister.ExtensionData = null;
#endif
					ID = Finregister.ID = DataContainerFacade.Save<Finregister, long>(Finregister);
#if !WEBCLIENT
					Finregister.ExtensionData = ce;
#endif

					break;
				default:
					break;
			}
		}

		protected override bool BeforeExecuteSaveCheck()
		{
			// проверка целостности временного размещения - возможно редактирование финреестра, созданного по изъятию
			// проверка, что финреестр был создан по изъятию
			if (Register.ReturnID > 0)
			{
				var allowedFR = GetFinregWithAllowedSums();
				if (allowedFR == null) return false;
				// для изъятия суммы не должны превосходить рассчетные
				if (Count > allowedFR.Count)
				{
					DialogHelper.ShowAlert("Недостаточно средств в размещении. Максимально допустимая сумма {0}", allowedFR.Count);
					return false;
				}

				if (ZLCount > allowedFR.ZLCount)
				{
					DialogHelper.ShowAlert("Неверно указано количество ЗЛ. Максимально допустимое количество {0}", allowedFR.ZLCount);
					return false;
				}
			}

			return base.BeforeExecuteSaveCheck();
		}

		protected override void ExecuteSave()
		{
			ExecuteSaveInternal();
		}

		// метод получения финреестра с полями суммы и количества ЗЛ, соответствующими текущим остаткам (без учета сумм редактируемого финреестра, если он есть в БД)
		protected Finregister GetFinregWithAllowedSums()
		{
			if (Register != null && Register.PortfolioID.HasValue && SelectedNPF != null)
			{
				var portfID = Register.PortfolioID.Value;
				var selName = SelectedNPF.GetContragent().Name;

				var allListSPN = BLServiceSystem.Client.GetSPNAllocatedFinregisters(portfID).Where(x => x.NPFName == selName && x.ID != Finregister.ID);
				var retListSPN = BLServiceSystem.Client.GetSPNReturnedFinregisters(portfID).Where(x => x.NPFName == selName && x.ID != Finregister.ID);

				var fr = new Finregister();

				fr.Count = allListSPN.Sum(x => x.Count) - retListSPN.Sum(x => x.Count);
				fr.ZLCount = allListSPN.Sum(x => x.ZLCount) - retListSPN.Sum(x => x.ZLCount);

				return fr;
			}

			return null;
		}

		protected void BindNPFAccounts()
		{
			if (SelectedNPF != null && SelectedNPF.ContragentID.HasValue && Finregister != null)
			{
				var accNPF = BLServiceSystem.Client.GetContragentRoubleAccount(SelectedNPF.ContragentID.Value);
				var accPFR = BLServiceSystem.Client.GetPFRRoubleAccount();

				if (accNPF == null)
					throw new Exception("Лиц счет НПФ не найден");

				if (accPFR == null)
					throw new Exception("Лиц счет ПФР не найден");

				if (RegisterIdentifier.IsFromNPF(Register.Kind.ToLower()))
				{
					try
					{
						Finregister.CrAccID = accNPF.ID;
						Finregister.DbtAccID = accPFR.ID;
					}
					catch
					{
						Finregister.CrAccID = 0;
						Finregister.DbtAccID = 0;
					}
				}
				else
				{
					try
					{
						Finregister.CrAccID = accPFR.ID;
						Finregister.DbtAccID = accNPF.ID;
					}
					catch
					{
						Finregister.CrAccID = 0;
						Finregister.DbtAccID = 0;
					}
				}
			}
		}
#endregion

		public bool IsTextFieldsEnabled => false;

	    protected bool IsReadOnlyFinregister
		{
			get
			{
				if (IsAvailableInBoNotComplete) return true;
				return State == ViewModelState.Read || RegisterIdentifier.FinregisterStatuses.IsNotTransferred(Finregister.Status);
			}
		}


        /// <summary>
        /// Связан ли финреестр с удаленным НПФ
        /// </summary>
        public bool IsForDeletedNPF => SelectedNPF != null && ((Contragent)SelectedNPF.ExtensionData["Contragent"]).StatusID < 0;

	    public bool isEnabled
		{
			get
			{
				if (_isVirtualAddNpfMode)
					return true;
				return !IsReadOnlyFinregister;
			}
		}

		public FinRegisterViewModel()
		//:base()
		{
			DataObjectTypeForJournal = typeof(Finregister);
			NT_NpfList = new List<NT_NpfListItem>();
		}

	    public FinRegisterViewModel(long id, long parentId, ViewModelState action)
            : this(id == 0? parentId : id, action)
	    {
	    }

		public FinRegisterViewModel(long recordID, ViewModelState action)
			: this()
		//: base(typeof(RegistersListViewModel), typeof(RegistersArchiveListViewModel))
		{
			State = action;

#if !WEBCLIENT
            RefreshConnectedCardsViewModels = RefreshConnectedCards;
#endif
            //выбираем все НПФ, включая удаленные
			NPFList = BLServiceSystem.Client.GetNPFListLEHib(new StatusFilter()).ToList();


			StatusNPFList = DataContainerFacade.GetList<Status>().Where(x => x.ID > 0).ToList();
			NPFGarantList = BLServiceSystem.Client.GetElementByType(Element.Types.GarantACB).Where(x => x.Visible).ToList();

			FinMoveTypes = RegisterIdentifier.FinregisterMoveTypes.ToList;
			FinStatuses = RegisterIdentifier.FinregisterStatuses.ToList;

			if (recordID > 0)
			{

				if (action == ViewModelState.Create)
				{
					Finregister = new Finregister { RegisterID = recordID, Count = 0 };
					Register = DataContainerFacade.GetByID<Register, long>(recordID);
					RegNum = Register.RegNum;
					Date = Register.RegDate;

#if !WEBCLIENT
                    ZLCount = 0;
#endif
                    CFRCount = 0;

					var finregisters = Register.GetFinregistersList();
					finregisters.PopulateBankAccounts();

					SelectedNPFGarant = NPFGarantList.FirstOrDefault();
					SelectedStatusNPF = StatusNPFList.FirstOrDefault();

					Status = FinStatuses.First(RegisterIdentifier.FinregisterStatuses.IsCreated);

					FinMoveType = FinMoveTypes.First();
				}
				else
				{
					ID = recordID;
					ReloadFRFromDB();
				}
				RefreshAddedNpfList();
			}
#if WEBCLIENT
			else
			{
                //новая запись, пустая, с 0 реестром!
			    Finregister = new Finregister { Count = 0 };
			    SelectedNPFGarant = NPFGarantList.FirstOrDefault();
			    SelectedStatusNPF = StatusNPFList.FirstOrDefault();
			    Status = FinStatuses.First(RegisterIdentifier.FinregisterStatuses.IsCreated);
			    FinMoveType = FinMoveTypes.First();
			}
#endif
            //очищаем все удаленные НПФ кроме потенциально выбранного
            NPFList.RemoveAll(a => ((Contragent) a.ExtensionData["Contragent"]).StatusID < 0 && a != SelectedNPF);

			_isNotTransferred = RegisterIdentifier.FinregisterStatuses.IsNotTransferred(Finregister.Status);

#if !WEBCLIENT
            SelectRegisterCommand = new DelegateCommand(o => CanExecuteSelectRegisterCommand(), o => ExecuteSelectRegisterCommand());
			AddNpfCommand = new DelegateCommand(o => CanExecuteAddNpfCommandCommand(), o => ExecuteAddNpfCommandCommand());
			DeleteNpfCommand = new DelegateCommand(o => CanExecuteDeleteNpfCommandCommand(), o => ExecuteDeleteNpfCommandCommand());
#endif
            //загрузка выбранных сущностей для статуса Финреестр не передан
			if (NT_ContentList == null)
				NT_ContentList = BLServiceSystem.Client.GetElementByType(Element.Types.RegisterPFRtoNPF).Where(x => x.Visible).ToList();

			if (IsNotTransferred && Finregister != null)
			{
				if (Finregister.SelectedContentID != null)
					NT_Content = NT_ContentList.FirstOrDefault(a => a.ID == Finregister.SelectedContentID);
				if (Finregister.SelectedRegisterID != null)
				{
					var lRegister = DataContainerFacade.GetByID<Register>((long)Finregister.SelectedRegisterID);
					var lAppDoc = lRegister.GetApproveDoc();
					if (lRegister != null)
						NT_Register = new CommonRegisterListItem(lRegister.ID, lRegister.RegDate, lRegister.RegNum, lAppDoc == null ? null : lAppDoc.Name);
				}
				OnPropertyChanged("NT_Content");
				OnPropertyChanged("NT_Register");
			}
            OnPropertyChanged("IsInactiveNPFStatus");
			IsDataChanged = false;
		}

		public void ReloadFRFromDB()
		{
			Finregister = DataContainerFacade.GetByID<Finregister, long>(ID);
			//_oldRegNum = RegNum;

			Register = Finregister.GetRegister();

			if (string.IsNullOrEmpty(RegNum))
				RegNum = Register.RegNum;
			if (Date == null)
				Date = Register.RegDate;

			if (_npfList != null && Finregister != null)
			{

				var accountDebit = Finregister.GetDebitAccount();
				var accountCredit = Finregister.GetCreditAccount();

				SelectedNPF = _npfList.FirstOrDefault(ca =>
					ca.ContragentID == (RegisterIdentifier.IsFromNPF(Register.Kind.ToLower()) ? accountCredit.ContragentID : accountDebit.ContragentID));


				if (SelectedNPF != null)
				{
					var contragent = SelectedNPF.GetContragent();
					SelectedStatusNPF = StatusNPFList.FirstOrDefault(x => x.ID == contragent.StatusID);
					SelectedNPFGarant = NPFGarantList.FirstOrDefault(a => a.ID == Element.SpecialDictionaryItems.ACBAll.ToLong()); //NPFGarantList.FirstOrDefault(x => x.ID == SelectedNPF.GarantACBID);
					
				}


				SelectedNPF = _npfList.FirstOrDefault(ca =>
					ca.ContragentID == (RegisterIdentifier.IsFromNPF(Register.Kind.ToLower()) ? accountCredit.ContragentID : accountDebit.ContragentID));
			}

			RefreshDraftsList();

			Status = FinStatuses.FirstOrDefault(x => x.Equals(Status, StringComparison.InvariantCultureIgnoreCase)) ?? FinStatuses.First(RegisterIdentifier.FinregisterStatuses.IsCreated); ;

			FinMoveType = FinMoveTypes.FirstOrDefault(x => x.Equals(FinMoveType, StringComparison.InvariantCultureIgnoreCase));
		}

#region Refresh

		public virtual void RefreshConnectedCards()
		{
			if (GetViewModel != null)
			{
				var rVM = GetViewModel(typeof(RegisterViewModel)) as RegisterViewModel;
				if (rVM != null)
					rVM.LoadRegister();
				var frVM = GetViewModel(typeof(FinRegisterViewModel)) as FinRegisterViewModel;
				if (frVM != null)
					frVM.RefreshStatus();
			}
		}

		protected virtual void FilterNPFList()
		{

			if (Register == null) return;
			long savedNPF = 0;
			if (SelectedNPF != null)
				savedNPF = SelectedNPF.ID;

            if (SelectedStatusNPF != null)
            {
                if (!IsActiveNPFStatusBool || SelectedNPFGarant == null)
                {
                    _npfList = BLServiceSystem.Client.GetNPFListLEHib(new StatusFilter
                    { 
                        new FilterOP { BOP = BOP.And, OP = OP.EQUAL, Identifier = SelectedStatusNPF.Name}
                    }).ToList();
                }
                else
                {
                    _npfList = BLServiceSystem.Client.GetNPFGarantedListLEHib(new StatusFilter
                    { 
                        new FilterOP { BOP = BOP.And, OP = OP.EQUAL, Identifier = SelectedStatusNPF.Name}
                    }, SelectedNPFGarant.ID)
                        .ToList();
                }
            }
            else
            {
                //финреестр для удаленного НПФ - оставляем в списке только выбранный НПФ
                _npfList = IsForDeletedNPF ? _npfList.Where(a => a == SelectedNPF).ToList() : new List<LegalEntity>();
            }


			var accountDebit = Finregister.GetDebitAccount();
			var accountCredit = Finregister.GetCreditAccount();
			bool IsNPFtoPFR = RegisterIdentifier.IsFromNPF(Register.Kind.ToLower());
			var caCurrent = IsNPFtoPFR ?
				(accountCredit == null ? null : accountCredit.ContragentID) :
				(accountDebit == null ? null : accountDebit.ContragentID);


			//var finregisters = Register.GetFinregistersList();
			//var usedAcc = finregisters.Where(a => a.StatusID != -1).Select(fr => IsNPFtoPFR ? fr.CrAccID : fr.DbtAccID).ToList();
			var usedNPF = BLServiceSystem.Client.GetUsedContragentIdListForRegister(Register.ID);

			NPFList = _npfList.Where(le => le.ContragentID == null || !usedNPF.Contains(le.ContragentID.Value) || le.ContragentID == caCurrent).ToList();

			//Востанавливаем по возможности старое выбраное значение
			SelectedNPF = _npfList.FirstOrDefault(le => le.ID == savedNPF);
		}


		private void RestoreActiveFinregister()
		{
			if (Finregister == null || Finregister.ID <= 0) return;
			var dbReg = DataContainerFacade.GetByID<Finregister>(Finregister.ID);
			if (dbReg == null) return;
			if (!StatusIdentifier.IsNPFStatusActivityCarries
				(
					(
						RegisterIdentifier.IsFromNPF(Register.Kind.ToLower()) ?
							dbReg.GetCreditAccount().GetContragent().GetStatus() :
							dbReg.GetDebitAccount().GetContragent().GetStatus()
						).Name
				)) return;
			Status = dbReg.Status;
			FinMoveType = dbReg.FinMoveType;
			FinDate = dbReg.FinDate;
			FinNum = dbReg.FinNum;
		}

		public void RefreshProps()
		{
			bool isdatachanged = IsDataChanged;
			OnPropertyChanged("RegNum");
			OnPropertyChanged("Count");
			OnPropertyChanged("Date");
			IsDataChanged = isdatachanged;
		}

		public void RefreshGiveFields()
		{
			bool isdatachanged = IsDataChanged;
			OnPropertyChanged("FinDate");
			OnPropertyChanged("FinNum");
			OnPropertyChanged("FinMoveType");
			IsDataChanged = isdatachanged;
		}

		public void RefreshStatus()
		{
			bool isdatachanged = IsDataChanged;
            _isNotTransferred = RegisterIdentifier.FinregisterStatuses.IsNotTransferred(Finregister.Status);
            OnRequestCustomAction();
            OnPropertyChanged("Status");
            OnPropertyChanged("isEnabled");
			IsDataChanged = isdatachanged;
		}



		public void RefreshDraftsList()
		{
			bool isdatachanged = IsDataChanged;
#if !WEBCLIENT
            if (Finregister.ExtensionData != null)
				Finregister.ExtensionData.Remove("DraftsList");
#endif
			DraftsList = new List<AsgFinTr>(Finregister.GetDraftsList().Cast<AsgFinTr>());
			OnPropertyChanged("DraftsListVisibility");
			IsDataChanged = isdatachanged;
		}
#endregion

#region Validation
		public override string this[string columnName]
		{
			get
			{
				const string errorMessage = "Неверный формат данных";
				//const string errorRegNum = "Номер договора и дата договора должны быть пусты или заполнены одновременно";
				switch (columnName.ToLower())
				{
					case "selectednpf": return SelectedNPF == null ? errorMessage : null;

					case "count":
						{
							decimal count = Count ?? 0;
							if (count > MAX_DECIMAL_19_4)
								return errorMessage;

							if (_isVirtualAddNpfMode)
							{
								if (count > NT_SpnLimit)
									return string.Format("Значение СПН должно быть не более {0}", NT_SpnLimit);
								if (count != NT_SpnLimit && ZLCount == NT_ZlLimit)
									return string.Format("Сумма СПН должна быть максимальной ({0}), т.к. для распределения указаны все доступные ЗЛ", NT_SpnLimit);
							}

							if (RegisterIdentifier.IsFromNPF(Register.Kind))
								return Count < 0 ? errorMessage : null;
							return Count < 0 ? errorMessage : null;
						}

					case "regnum": return RegNum.ValidateMaxLength(50);
					//case "date": return (string.IsNullOrWhiteSpace(this.RegNum) ^ this.Date == null) ? errorRegNum : null;

					case "finnum": return FinNum != null && FinNum.Length > 49 ? errorMessage : null;
					case "findate": return FinDate == null ? errorMessage : null;
					case "finmovetype": return string.IsNullOrEmpty(FinMoveType) ? errorMessage : null;
					case "cfrcount": return CFRCount.ValidateNonNegative() ?? CFRCount.ValidateMaxFormat();
					case "comment": return Comment.ValidateMaxLength(512);

					case "ntregister":
						return NT_Register == null ? errorMessage : null;

					case "zlcount":
						if (ZLCount == null || ZLCount == 0 || !DataTools.IsNumericString(ZLCount.ToString()))
							return errorMessage;
						if (_isVirtualAddNpfMode)
						{
							if (ZLCount > NT_ZlLimit)
								return string.Format("Значение ЗЛ должно быть не более {0}", NT_ZlLimit);
							decimal count = Count ?? 0;
							if (count == NT_SpnLimit && ZLCount != NT_ZlLimit)
								return string.Format("Кол-во ЗЛ должно быть максимальным ({0}), т.к. для распределения указана максимально допустимая сумма СПН", NT_ZlLimit);
						}
						return null;
					default: return null;
				}
			}
		}

		protected virtual string Validate()
		{
			var fieldNames =
				string.Format("selectednpf|count|zlcount|regnum|date|finnum|cfrcount|comment{0}", _isNotTransferred ? "|ntregister" : null);

			return fieldNames.Split("|".ToCharArray()).Any(fieldName => !String.IsNullOrEmpty(this[fieldName])) ? "Неверный формат данных" : null;
		}
#endregion


		public void CloseWindow()
		{
			if (RequestClose != null)
			{
				// необходимо заблокировать запрос о сохранении при закрытии окна
				Deleting = true;
				RequestClose(this, EventArgs.Empty);
			}
		}


#region МЕТОДЫ И СВОЙСТВА ДЛЯ ФИНРЕЕСТРА В СТАТУСЕ "ФИНРЕЕСТР НЕ ПЕРЕДАН"

		//РЕЖИМ ДОБАВЛЕНИЯ НПФ = когда финреестр используется с формой Добавить НПФ, для добавления НПФ к финреестру в статусе "Финреестр не передан"

#region NT Props

		public NT_NpfListItem NT_SelectedNpf { get; set; }

		/// <summary>
		/// Команда выбора реестра для добавления НПФ для финреестра в статусе "Финреестр не передан"
		/// </summary>
		public ICommand SelectRegisterCommand { get; private set; }
		/// <summary>
		/// Команда на добавлениен НПФ для финреестра в статусе "Финреестр не передан"
		/// </summary>
		public ICommand AddNpfCommand { get; private set; }
		/// <summary>
		/// Команда на удаление НПФ для финреестра в статусе "Финреестр не передан"
		/// </summary>
		public ICommand DeleteNpfCommand { get; private set; }
		/// <summary>
		/// Команда на успешное добавление НПФ в режиме добавления НПФ для финреестра в статусе "Финреестр не передан"
		/// </summary>
		public ICommand OKAddNpfCommand { get; private set; }
		/// <summary>
		/// Выбранный реестр для финреестра в статусе "Финреестр не передан"
		/// </summary>
		public CommonRegisterListItem NT_Register { get; set; }
		/// <summary>
		/// Выбранное содержание операции для финреестра в статусе "Финреестр не передан"
		/// </summary>
		public Element NT_Content { get; set; }
		/// <summary>
		/// Список содержаний операций для финреестра в статусе "Финреестр не передан"
		/// </summary>
		public List<Element> NT_ContentList { get; set; }
		/// <summary>
		/// Список добавленных НПФ для для финреестра в статусе "Финреестр не передан" </summary>
		public List<NT_NpfListItem> NT_NpfList { get; set; }
		/// <summary>
		/// Макс. кол-во ЗЛ в режиме добавления НПФ (для финреестра в статусе "Финреестр не передан")
		/// </summary>
		public long NT_ZlLimit { get; set; }
		/// <summary>
		/// Макс. кол-во СПН в режиме добавления НПФ (для финреестра в статусе "Финреестр не передан")
		/// </summary>
		public decimal NT_SpnLimit { get; set; }
		/// <summary>
		/// Ссылка на родительский ID (для финреестра в статусе "Финреестр не передан")
		/// </summary>
		private long? _ntParentId;

		/// <summary>
		/// Режим добавления НПФ (для работы с финреестром в статусе "Финреестр не передан")
		/// </summary>
		private bool _isVirtualAddNpfMode;

		//ublic bool IsVirtualAddNpfMode { get { return _isVirtualAddNpfMode; } }

		private bool _isNotTransferred;
		/// <summary>
		/// Статус финреестра = "Финреестр не передан"
		/// </summary>
		public bool IsNotTransferred => _isNotTransferred;

#endregion

		/// <summary>
		/// Получить макс. допустимые значения СПН и ЗЛ для режима добавления НПФ для финреестра в статусе "Финреестр не передан"
		/// </summary>
		/// <returns>0 - СПН, 1 - ЗЛ</returns>
		private object[] NT_GetMaxValueCounts()
		{
			return new object[]
                {
                    Count - NT_NpfList.Sum(a=>a.Summ),
                    ZLCount - NT_NpfList.Sum(a => a.ZlCount)
                };
		}

		/// <summary>
		/// Обновить список добавленных НПФ для финреестра в статусе "Финреестр не передан"
		/// </summary>
		private void RefreshAddedNpfList()
		{
			NT_NpfList = DataContainerFacade.GetListByProperty<Finregister>("ParentFinregisterID", Finregister.ID, true).Select(a =>
			{
				var fr = new NT_NpfListItem(a);
				var cid = RegisterIdentifier.IsFromNPF(a.GetRegister().Kind.ToLower()) ? a.GetCreditAccount().ContragentID : a.GetDebitAccount().ContragentID;
				fr.Name = DataContainerFacade.GetByID<Contragent>(cid).GetLegalEntity().FormalizedName;

				return fr;
			}).ToList();

			OnPropertyChanged("NT_NpfList");
		}

#region Выполнение команд

		private void ExecuteDeleteNpfCommandCommand()
		{
			var oldId = NT_SelectedNpf.Id;
			switch (BLServiceSystem.Client.DeleteFinregister(NT_SelectedNpf.Id, false, null, true))
			{
				case WebServiceDataError.FinregisterNotInCreatedStatus:
					DialogHelper.ShowError("Невозможно удалить НПФ т.к финреестр находится встатусе, отличном от начального!");
					return;
			}
			/*var res = DataContainerFacade.GetByID<Finregister>(NT_SelectedNpf.Id);
			if (res.GetDraftsListCount() > 0)
			{
				DialogHelper.ShowError("Для выбранного НПФ заданы платежные поручения!");
				return;
			}
			if (!RegisterIdentifier.FinregisterStatuses.IsCreated(res.Status))
			{
				DialogHelper.ShowError("Выбранный НПФ не находится в статусе \"Финреестр создан\"!");
				return;
			}
			DataContainerFacade.Delete(res);*/
			RefreshAddedNpfList();
			var list = GetUpdatedList();
			list.Add(GetDeletedFinregisterForUpdate(oldId));
			ViewModelManager.UpdateDataInAllModels(list);
			IsDataChanged = false;
		}

		public bool CanExecuteDeleteNpfCommandCommand()
		{
			return NT_SelectedNpf != null && NT_SelectedNpf.IsCreatedState;
		}

		private void ExecuteOkAddNpfCommandCommand()
		{
			ExecuteSave();
			CloseWindow();
		}

		private bool CanExecuteOkAddNpfCommandCommand()
		{
			return _isVirtualAddNpfMode && String.IsNullOrEmpty(Validate());
		}

		private void ExecuteAddNpfCommandCommand()
		{


			var res = NT_GetMaxValueCounts();
			var count = (decimal?)res[0];
			var zl = (long?)res[1];

			if (count == 0 && zl == 0)
			{
				DialogHelper.ShowError("Невозможно добавить новый НПФ, т.к. все доступные средства уже распределены!");
				return;
			}

			var result = DialogHelper.AddNpfForFinregister(NT_Register.ID, zl ?? 0, count ?? 0, Finregister.ID);
			if (!result) return;

			RefreshAddedNpfList();
			ExecuteSave();

			res = NT_GetMaxValueCounts();
			count = (decimal?)res[0];
			zl = (long?)res[1];
			if (count < 0 || zl < 0)
				;
			//выполнили разнесение средств по НПФ
			//помещаем в архив
			if (count == 0 && zl == 0)
			{
				//уходит само
				//base.ExecuteDelete(DocOperation.Archive);
			}
			ViewModelManager.UpdateDataInAllModels(GetUpdatedList());
			IsDataChanged = false;
		}

		public bool CanExecuteAddNpfCommandCommand()
		{
			return NT_Register != null;
		}

		private void ExecuteSelectRegisterCommand()
		{
			NT_Register = DialogHelper.ShowSelectRegister(AsgFinTr.Sections.NPF, AsgFinTr.Directions.FromPFR, NT_Content.Name);
			OnPropertyChanged("NT_Register");
		}

		public bool CanExecuteSelectRegisterCommand()
		{
			if (NT_Content == null) return false;
			if (NT_NpfList != null)
			{
				var res = NT_GetMaxValueCounts();
				if ((long?)res[1] == 0 && (decimal?)res[0] == 0) return false;
			}
			return true;
		}

#endregion

		/// <summary>
		/// Подготовка модели к режиму добавления НПФ для финреестра  в статусе Не передан
		/// </summary>
		/// <param name="limitZl">Ограничение по ЗЛ</param>
		/// <param name="limitSpn">Ограничение по СПН</param>
		public void PrepareAddNotTransferredNpfMode(long limitZl, decimal limitSpn, long parentId)
		{
			NT_ZlLimit = limitZl;
			NT_SpnLimit = limitSpn;
			_ntParentId = parentId;
			_isVirtualAddNpfMode = true;
			OKAddNpfCommand = new DelegateCommand(o => CanExecuteOkAddNpfCommandCommand(), o => ExecuteOkAddNpfCommandCommand());
			OnPropertyChanged("IsVirtualAddNpfMode");
		}

#endregion

#region IUpdateListenerModel implementation
		public Type[] UpdateListenTypes => new[] { typeof(RegistersListViewModel), typeof(RegistersArchiveListViewModel) };

	    public void OnDataUpdate(Type type, long id)
		{
			if (type != typeof(Finregister) || id <= 0) return;
			ID = id;
			ReloadFRFromDB();
		}
#endregion

#region IUpdateRaisingModel implementation
		public virtual IList<KeyValuePair<Type, long>> GetUpdatedList()
		{
			var list = new List<KeyValuePair<Type, long>> { new KeyValuePair<Type, long>(typeof(Finregister), ID) };

			if (Finregister.ParentFinregisterID != null)
			{
				list.Add(new KeyValuePair<Type, long>(typeof(Finregister), (long)Finregister.ParentFinregisterID));
			}

			if (NT_NpfList != null)
			{
				list.AddRange(NT_NpfList.Select(item => new KeyValuePair<Type, long>(typeof(Finregister), item.Id)));
			}
			return list;
		}

		private KeyValuePair<Type, long> GetDeletedFinregisterForUpdate(long id)
		{
			return new KeyValuePair<Type, long>(typeof(Finregister), id);
		}

#endregion

	}
}
