﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager)]
    public class DocumentNpfViewModel : DocumentBaseViewModel
    {
        public DocumentNpfViewModel() : this(0, ViewModelState.Create) { }
        public DocumentNpfViewModel(long id) : this(id, ViewModelState.Edit) { }
        public DocumentNpfViewModel(long id, ViewModelState state)
            : base(id, state) { }

        public override Document.Types Type => Document.Types.Npf;

        protected override IList<string> GetAdditionalExecutionInfo()
        {
            return new List<string>();
            //return BLServiceSystem.Client.GetElementByType(Element.Types.NpfDocumentAdditionalExecutionInfo).Select(e => e.Name).ToList();           
        }

        protected override IList<string> GetOriginalStoragePlaces()
        {
            return BLServiceSystem.Client.GetElementByType(Element.Types.NpfDocumentStoragePlaces).Where(x=>x.Visible).Select(e => e.Name).ToList();   
        }



        protected override IList<LegalEntity> GetLegalEntityList()
        {
            return BLServiceSystem.Client.GetActiveNpfListHib();
        }

        protected override IList<DocumentClass> GetDocumentClassList()
        {
            return DataContainerFacade.GetList<DocumentClass>();
        }

		public override bool HasExecutorList => false;

        //protected override IList<Person> GetPersonList()
		//{
		//    return new List<Person>();
		//}

        protected override IList<AdditionalDocumentInfo> GetAdditionalInfoList()
        {
            return new List<AdditionalDocumentInfo>();
            //return DataContainerFacade.GetList<AdditionalDocumentInfo>();
        }

        public override bool HasOutgoingNumber => false;
    }
}