﻿using System;
using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Helpers;
using PFR_INVEST.Proxy;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class F70ViewModel : ViewModelCard
    {
        /// <summary>
        /// Поле хранения данных для старой ODBC системы
        /// </summary>
        public Dictionary<string, DBField> Fields { get; set; }

        #region QuartersList

        private List<int> quartersList;
        public List<int> QuartersList
        {
            get { return quartersList; }
            set { quartersList = value; OnPropertyChanged("QuartersList"); }
        }


        public int SelectedQuarter
        {
            get { return Util.isFieldExists(Fields, "QUARTER") ? Convert.ToInt32(Fields["QUARTER"].Value) : -1; }

            set
            {
                if (!Util.isFieldExists(Fields, "QUARTER")) return;

                Fields["QUARTER"].Value = value;
                OnPropertyChanged("SelectedQuarter");
            }
        }

        #endregion

        #region YearsList

        private List<DBEntity> yearsList;
        public List<DBEntity> YearsList
        {
            get { return yearsList; }
            set { yearsList = value; OnPropertyChanged("YearsList"); }
        }

        public DBEntity SelectedYear
        {
            get
            {
                if (yearsList != null)
                    foreach (DBEntity year in yearsList)
                        if (Util.isFieldExists(year, "ID") && Util.isFieldExists(Fields, "ID_POST"))
                            if ((long)year.Fields["ID"].Value == (long)Fields["ID_YEAR"].Value)
                                return year;

                return null;
            }

            set
            {
                Dictionary<string, DBField> dstFields = Fields;
                if (value != null) Util.setDBEntityField(ref dstFields, "ID_YEAR", value.Fields, "ID");


                OnPropertyChanged("SelectedYear");
            }
        }

        #endregion

        #region NPFList
        private List<DB2LegalEntityCard> npfList;
        public List<DB2LegalEntityCard> NPFList
        {
            get { return npfList; }
            set { npfList = value; OnPropertyChanged("NPFList"); }
        }

        private long npfID;
        public DB2LegalEntityCard SelectedNPF
        {
            get
            {
                if (npfList != null)
                    foreach (var npf in npfList)
                        if (npf.ID == npfID)
                            return npf;

                return null;
            }

            set
            {
                if (value == null) return;

                npfID = value.ID;
                FullName = value.FullName;
                INN = value.INN;
               // if (SelectedNPF != null)
               //     ContractsList4NPF = new List<DBEntity>(BLServiceSystem.Client.GetContractsListForLegalEntity(SelectedNPF.ID));

                OnPropertyChanged("SelectedNPF");
            }
        }

        #endregion

        #region FullName

        private string fullName;
        public String FullName
        {
            get { return fullName; }
            set { fullName = value; OnPropertyChanged("FullName"); }
        }

        #endregion

        #region INN

        private string inn;
        public String INN
        {
            get { return inn; }
            set { inn = value; OnPropertyChanged("INN"); }
        }

        #endregion

        #region ContractsList4NPF

       /* private List<DBEntity> contractsList4NPF;
        public List<DBEntity> ContractsList4NPF
        {
            get { return contractsList4NPF; }
            set { contractsList4NPF = value; OnPropertyChanged("ContractsList4NPF"); }
        }*/

        private DBEntity selectedContract;
        public DBEntity SelectedContract
        {
            get { return selectedContract; }
            set
            {
                if (!Util.isFieldExists(value, "REGDATE") || !Util.isFieldExists(value, "PORTFNAME")) return;

                selectedContract = value;
                ContractDate = Convert.ToDateTime(selectedContract.Fields["REGDATE"].Value);
                PortfolioName = selectedContract.Fields["PORTFNAME"].Value.ToString();

                Dictionary<string, DBField> dstFields = Fields;
                if (selectedContract != null)
                    Util.setDBEntityField(ref dstFields, "CONTRACT_ID", selectedContract.Fields, "ID");

                OnPropertyChanged("SelectedContract");

            }
        }

        #endregion

        #region ContractDate

        private DateTime contractDate;
        public DateTime ContractDate
        {
            get { return contractDate; }
            set { contractDate = value; OnPropertyChanged("ContractDate"); }
        }

        #endregion

        #region Fields

        private string portfolioName;
        public String PortfolioName
        {
            get { return portfolioName; }
            set { portfolioName = value; OnPropertyChanged("PortfolioName"); }
        }

        public decimal S010r1 => DBFieldHelper.GetDecimal("EDO_ODKF070.PROFIT1", Fields) ?? 0.00m;

        public decimal S010r2 => DBFieldHelper.GetDecimal("EDO_ODKF070.PROFIT2", Fields) ?? 0.00m;

        public bool S010HasDiff => S010r1 != S010r2;

        public decimal S020r1 => DBFieldHelper.GetDecimal("EDO_ODKF070.RETAINED1", Fields) ?? 0.00m;

        public decimal S020r2 => DBFieldHelper.GetDecimal("EDO_ODKF070.RETAINED2", Fields) ?? 0.00m;

        public bool S020HasDiff => S020r1 != S020r2;

        public decimal S021r1 => DBFieldHelper.GetDecimal("EDO_ODKF070.SDPAY1", Fields) ?? 0.00m;

        public decimal S021r2 => DBFieldHelper.GetDecimal("EDO_ODKF070.SDPAY2", Fields) ?? 0.00m;

        public bool S021HasDiff => S021r1 != S021r2;


        public decimal P010 => DBFieldHelper.GetDecimal("EDO_ODKF070.MIDVALUE", Fields) ?? 0.00m;

        public decimal P030 => DBFieldHelper.GetDecimal("EDO_ODKF070.MAXUKPAY", Fields) ?? 0.00m;

        public decimal P040 => DBFieldHelper.GetDecimal("EDO_ODKF070.FACTUKPAY", Fields) ?? 0.00m;

        public decimal P050 => P030 - P040;


        public decimal P060 => DBFieldHelper.GetDecimal("EDO_ODKF070.MAXSDPAY", Fields) ?? 0.00m;

        public decimal P070 => DBFieldHelper.GetDecimal("EDO_ODKF070.FACTSDPAY", Fields) ?? 0.00m;

        public decimal P080 => P060 - P070;

        public decimal P090 => DBFieldHelper.GetDecimal("EDO_ODKF070.PROFIT2", Fields) ?? 0.00m;

        public decimal P100 => DBFieldHelper.GetDecimal("EDO_ODKF070.UKREWARD2", Fields) ?? 0.00m;

        public decimal P100Fake => DBFieldHelper.GetDecimal("EDO_ODKF070.UKREWARD", Fields) ?? 0.00m;

        public decimal P100Proc
        {
            get
            {
                if (P100Fake != 0)
                    return (P100Fake / P090) * 100;
                if (P100 != 0)
                    return (P100 / P090) * 100;
                return 0.00m;
            }
        }



        #endregion Fields

        public F70ViewModel(long id)
        {
            ID = id;
            DataObjectTypeForJournal = typeof(EdoOdkF070);
            Fields = BLServiceSystem.Client.GetF70(id);
            foreach (string key in Fields.Keys)
            {
                if (Fields[key].Value is DateTime)
                    Fields[key].Value = DBFieldHelper.GetDate(key, Fields);
            }
        }

        public override bool CanExecuteSave() { return false; }

        protected override void ExecuteSave()
        {
        }

        public override string this[string columnName] => "";
    }
}
