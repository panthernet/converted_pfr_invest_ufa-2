﻿using System;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.ServiceModel.Configuration;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Core.Properties;
using PFR_INVEST.DataAccess.Client;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.User)]
    //[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.User)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator)]
    public class PreferencesViewModel : ViewModelCard
    {
        private bool ConfigChanged;

        private string _mqManagerName;
        public string MQManagerName
        {
            get
            {
                return _mqManagerName;
            }
            set
            {
                _mqManagerName = value;
                OnPropertyChanged("MQManagerName");
            }
        }

        private string _mqQueueName;
        public string MQQueueName
        {
            get
            {
                return _mqQueueName;
            }
            set
            {
                _mqQueueName = value;
                OnPropertyChanged("MQQueueName");
            }
        }

        private string _mqXMLPath;
        public string MQXMLPath
        {
            get
            {
                return _mqXMLPath;
            }
            set
            {
                _mqXMLPath = value;
                OnPropertyChanged("MQXMLPath");
            }
        }

        private string _dbfPath;
        public string DBFPath
        {
            get
            {
                return _dbfPath;
            }
            set
            {
                _dbfPath = value;
                OnPropertyChanged("DBFPath");
            }
        }

        private string _iisServicePath;
        public string IISServicePath
        {
            get
            {
                return _iisServicePath;
            }
            set
            {
                ConfigChanged = true;
                _iisServicePath = value; OnPropertyChanged("IISServicePath");
            }
        }

        private string _cognosServerPath;
        public string CognosServerPath
        {
            get
            {
                return _cognosServerPath;
            }
            set
            {
                _cognosServerPath = value; OnPropertyChanged("CognosServerPath");
            }
        }

        private string _cognosServerPath2;
        public string CognosServerPath2
        {
            get
            {
                return _cognosServerPath2;
            }
            set
            {
                _cognosServerPath2 = value;
                OnPropertyChanged("CognosServerPath2");
            }
        }

        private string _cognosServerPath3;
        public string CognosServerPath3
        {
            get
            {
                return _cognosServerPath3;
            }
            set
            {
                _cognosServerPath3 = value;
                OnPropertyChanged("CognosServerPath3");
            }
        }

        private string _cognosServerPath4;
        public string CognosServerPath4
        {
            get
            {
                return _cognosServerPath4;
            }
            set
            {
                _cognosServerPath4 = value;
                OnPropertyChanged("CognosServerPath4");
            }
        }

        private string _cognosServerPath5;
        public string CognosServerPath5
        {
            get
            {
                return _cognosServerPath5;
            }
            set
            {
                _cognosServerPath5 = value; OnPropertyChanged("CognosServerPath5");
            }
        }

        private string _cognosServerPath6;
        public string CognosServerPath6
        {
            get
            {
                return _cognosServerPath6;
            }
            set
            {
                _cognosServerPath6 = value; OnPropertyChanged("CognosServerPath6");
            }
        }

        private string _cognosDomain;
        public string CognosDomain
        {
            get
            {
                return _cognosDomain;
            }
            set
            {
                _cognosDomain = value;
                OnPropertyChanged("CognosDomain");
            }
        }

        public string ClientVersion => Assembly.GetExecutingAssembly().GetName().Version.ToString();

        private readonly Version _serverVersion;
        public string ServerVersion => _serverVersion.ToString();

        public string DBText { get; }

        public PreferencesViewModel()
        {
            DataObjectTypeForJournal = typeof(object);
            IsNotSavable = true;
            try
            {
                RefreshServerSettings();
                PropertyChanged += (sender, e) =>
                {
                    IsDataChanged = SettingCognosManager.NPFUrl != CognosServerPath2 ||
                        SettingCognosManager.SIUrl != CognosServerPath3 ||
                        SettingCognosManager.CBUrl != CognosServerPath4 ||
                        SettingCognosManager.BOUrl != CognosServerPath5 ||
                        SettingCognosManager.VRUrl != CognosServerPath6;
                };
                var sd = BLServiceSystem.Client.GetServiceVersion();
                _serverVersion = sd.Version;
                DBText = sd.DBText;
                ConfigChanged = false;
            }
            catch (Exception e)
            {
                Logger.WriteException(e);
            }
        }

        private void RefreshServerSettings()
        {
            MQManagerName = Settings.Default.MQManagerName;
            MQQueueName = Settings.Default.MQQueueName;
            MQXMLPath = Settings.Default.MQXMLPath;
            DBFPath = Settings.Default.DBFPath;

            CognosServerPath = Settings.Default.CognosDictionaryUrl;
            //CognosServerPath2 = PFR_INVEST.Core.Properties.Settings.Default.CognosNPFUrl;
            //CognosServerPath3 = PFR_INVEST.Core.Properties.Settings.Default.CognosSIUrl;
            //CognosServerPath4 = PFR_INVEST.Core.Properties.Settings.Default.CognosCBUrl;
            //CognosServerPath5 = PFR_INVEST.Core.Properties.Settings.Default.CognosBOUrl;
            //CognosServerPath6 = PFR_INVEST.Core.Properties.Settings.Default.CognosVRUrl;
            CognosServerPath2 = SettingCognosManager.NPFUrl;
            CognosServerPath3 = SettingCognosManager.SIUrl;
            CognosServerPath4 = SettingCognosManager.CBUrl;
            CognosServerPath5 = SettingCognosManager.BOUrl;
            CognosServerPath6 = SettingCognosManager.VRUrl;
            CognosDomain = Settings.Default.CognosDomainName;

            var appConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var serviceModel = ServiceModelSectionGroup.GetSectionGroup(appConfig);
            if (serviceModel != null)
            {
                var authType = ConfigurationManager.AppSettings.AllKeys.Contains("ClientAuthType") &&
                               ConfigurationManager.AppSettings["ClientAuthType"] == "NONE"
                    ? ClientAuthType.None
                    : ClientAuthType.ECASAHTTPS;

                foreach (ChannelEndpointElement endpoint in serviceModel.Client.Endpoints)
                {
                    if (endpoint.Name == "WSHttpBinding_IDB2Connector" && authType == ClientAuthType.None)
                    {
                        IISServicePath = endpoint.Address.ToString();
                        break;
                    }
                    if (endpoint.Name == "WSHttpsBinding_IDB2Connector" && authType == ClientAuthType.ECASAHTTPS)
                    {
                        IISServicePath = endpoint.Address.ToString();
                        break;
                    }
                }

                appConfig.Save();                
            }
            ConfigChanged = false;
            IsDataChanged = false;
        }

        protected override void ExecuteSave()
        {
            SettingCognosManager.NPFUrl = CognosServerPath2;
            SettingCognosManager.SIUrl = CognosServerPath3;
            SettingCognosManager.CBUrl = CognosServerPath4;
            SettingCognosManager.BOUrl = CognosServerPath5;
            SettingCognosManager.VRUrl = CognosServerPath6;
        }

        public override bool CanExecuteSave()
        {
            return IsDataChanged && !EditAccessDenied;
        }

        public override string this[string columnName] => null;
    }
}
