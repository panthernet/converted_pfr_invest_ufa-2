﻿using System;
using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class F26ViewModel : ViewModelCard
    {
        private EdoOdkF026 odk;
        public EdoOdkF026 Odk
        {
            get { return odk; }
            set
            {
                odk = value;
                OnPropertyChanged("Odk");
            }
        }

        //private Contract contract;
        //public Contract Contract
        //{
        //    get { return contract; }
        //    set
        //    {
        //        contract = value;
        //        this.OnPropertyChanged("Contract");
        //    }
        //}

        //private LegalEntity legalEntity;
        //public LegalEntity LegalEntity
        //{
        //    get { return legalEntity; }
        //    set
        //    {
        //        legalEntity = value;
        //        this.OnPropertyChanged("LegalEntity");
        //    }
        //}

        public override bool CanExecuteSave() { return false; }

        protected override void ExecuteSave() { }

        public override string this[string columnName] => String.Empty;

        public F26ViewModel(long id)
        {
            DataObjectTypeForJournal = typeof(EdoOdkF026);
            if (id > 0)
            {
                ID = id;
                Odk = DataContainerFacade.GetByID<EdoOdkF026, long>(id);
                //this.Contract = this.Odk.GetContract();
                //this.LegalEntity = this.Contract.GetLegalEntity();

                Group1List = Odk.GetGroup<F026Group1>();
                Group2List = Odk.GetGroup<F026Group2>();
                Group3List = Odk.GetGroup<F026Group3>();
                Group4List = Odk.GetGroup<F026Group4>();
                Group5List = Odk.GetGroup<F026Group5>();
                Group6List = Odk.GetGroup<F026Group6>();
                Group7List = Odk.GetGroup<F026Group7>();
                Group8List = Odk.GetGroup<F026Group8>();
                Group81List = Odk.GetGroup<F026Group81>();
                Group9List = Odk.GetGroup<F026Group9>();
                Group10List = Odk.GetGroup<F026Group10>();
                Group11List = Odk.GetGroup<F026Group11>();
                Group12List = Odk.GetGroup<F026Group12>();
                SubGroup1List = Odk.GetGroup<F026SubGroup1>();
                SubGroup2List = Odk.GetGroup<F026SubGroup2>();
                SubGroup3List = Odk.GetGroup<F026SubGroup3>();
            }
        }

        #region GroupsLists

        public decimal? Total010 => Odk.Group1;
        public decimal? Total020 => Odk.Group2;
        public decimal? Total030 => Odk.Group3;
        public decimal? Total040 => Odk.Group4;
        public decimal? Total050 => Odk.Group5;
        public decimal? Total060 => Odk.Group6;
        public decimal? Total070 => Odk.Group7;
        public decimal? Total080 => Odk.Group8;
        public decimal? Total081 => Odk.Group81;
        public decimal? Total090 => Odk.Group9;
        public decimal? Total100 => Odk.Group10;
        public decimal? Total110 => Odk.Group11;
        public decimal? Total120 => Odk.Group12;

        public decimal? Total130 => Odk.Group13;
        public decimal? Total131 => Odk.Subgroup1;
        public decimal? Total132 => Odk.Subgroup2;
        public decimal? Total133 => Odk.Subgroup3;

        //public decimal? Total010 { get { return this.Group1List.Sum(r => r.Amount); } }
        //public decimal? Total020 { get { return this.Group2List.Sum(r => r.Amount); } }
        //public decimal? Total030 { get { return this.Group3List.Sum(r => r.Amount); } }
        //public decimal? Total040 { get { return this.Group4List.Sum(r => r.Amount); } }
        //public decimal? Total050 { get { return this.Group5List.Sum(r => r.Amount); } }
        //public decimal? Total060 { get { return this.Group6List.Sum(r => r.Amount); } }
        //public decimal? Total070 { get { return this.Group7List.Sum(r => r.Amount); } }
        //public decimal? Total080 { get { return this.Group8List.Sum(r => r.Amount); } }
        //public decimal? Total081 { get { return this.Group81List.Sum(r => r.Amount); } }
        //public decimal? Total090 { get { return this.Group9List.Sum(r => r.Amount); } }
        //public decimal? Total100 { get { return this.Group10List.Sum(r => r.Amount); } }
        //public decimal? Total110 { get { return this.Group11List.Sum(r => r.Amount); } }
        //public decimal? Total120 { get { return this.Group12List.Sum(r => r.Amount); } }

        //public decimal? Total130 { get { return this.Odk.Group13; } }
        //public decimal? Total131 { get { return this.SubGroup1List.Sum(r => r.Amount); } }
        //public decimal? Total132 { get { return this.SubGroup2List.Sum(r => r.Amount); } }
        //public decimal? Total133 { get { return this.SubGroup3List.Sum(r => r.Amount); } }



        private IList<F026Group1> _group1List;
        public IList<F026Group1> Group1List
        {
            get { return _group1List; }
            set { _group1List = value; OnPropertyChanged("Group1List"); }
        }

        private IList<F026Group2> _group2List;
        public IList<F026Group2> Group2List
        {
            get { return _group2List; }
            set { _group2List = value; OnPropertyChanged("Group2List"); }
        }

        private IList<F026Group3> _group3List;
        public IList<F026Group3> Group3List
        {
            get { return _group3List; }
            set { _group3List = value; OnPropertyChanged("Group3List"); }
        }

        private IList<F026Group4> _group4List;
        public IList<F026Group4> Group4List
        {
            get { return _group4List; }
            set { _group4List = value; OnPropertyChanged("Group4List"); }
        }

        private IList<F026Group5> _group5List;
        public IList<F026Group5> Group5List
        {
            get { return _group5List; }
            set { _group5List = value; OnPropertyChanged("Group5List"); }
        }

        private IList<F026Group6> _group6List;
        public IList<F026Group6> Group6List
        {
            get { return _group6List; }
            set { _group6List = value; OnPropertyChanged("Group6List"); }
        }

        private IList<F026Group7> _group7List;
        public IList<F026Group7> Group7List
        {
            get { return _group7List; }
            set { _group7List = value; OnPropertyChanged("Group7List"); }
        }

        private IList<F026Group8> _group8List;
        public IList<F026Group8> Group8List
        {
            get { return _group8List; }
            set { _group8List = value; OnPropertyChanged("Group8List"); }
        }

        private IList<F026Group81> _group81List;
        public IList<F026Group81> Group81List
        {
            get { return _group81List; }
            set { _group81List = value; OnPropertyChanged("Group81List"); }
        }

        private IList<F026Group9> _group9List;
        public IList<F026Group9> Group9List
        {
            get { return _group9List; }
            set { _group9List = value; OnPropertyChanged("Group9List"); }
        }

        private IList<F026Group10> _group10List;
        public IList<F026Group10> Group10List
        {
            get { return _group10List; }
            set { _group10List = value; OnPropertyChanged("Group10List"); }
        }

        private IList<F026Group11> _group11List;
        public IList<F026Group11> Group11List
        {
            get { return _group11List; }
            set { _group11List = value; OnPropertyChanged("Group11List"); }
        }

        private IList<F026Group12> _group12List;
        public IList<F026Group12> Group12List
        {
            get { return _group12List; }
            set { _group12List = value; OnPropertyChanged("Group12List"); }
        }

        private IList<F026SubGroup1> _subGroup1List;
        public IList<F026SubGroup1> SubGroup1List
        {
            get { return _subGroup1List; }
            set { _subGroup1List = value; OnPropertyChanged("SubGroup1List"); }
        }

        private IList<F026SubGroup2> _subGroup2List;
        public IList<F026SubGroup2> SubGroup2List
        {
            get { return _subGroup2List; }
            set { _subGroup2List = value; OnPropertyChanged("SubGroup2List"); }
        }

        private IList<F026SubGroup3> _subGroup3List;
        public IList<F026SubGroup3> SubGroup3List
        {
            get { return _subGroup3List; }
            set { _subGroup3List = value; OnPropertyChanged("SubGroup3List"); }
        }

        #endregion GroupsLists
    }
}
