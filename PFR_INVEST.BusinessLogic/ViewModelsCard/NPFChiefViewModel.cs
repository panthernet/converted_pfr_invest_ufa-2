﻿using System;
using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator)]
    public class NPFChiefViewModel : ViewModelCard
    {
        public EventHandler OnListIsEmpty;
        protected void RaiseListIsEmpty()
        {
            if (OnListIsEmpty != null)
                OnListIsEmpty(this, new EventArgs());
        }


        public LegalEntityChief LegalEntityChiefOriginal { get; private set; }
        public LegalEntityChief LegalEntityChief { get; private set; }
        public NPFViewModel Model { get; private set; }

        //используеться для отключения полей, после сохранения объектов
        public bool IsCardSaved => State != ViewModelState.Create || EditAccessDenied;


        #region Руководство
        public string HeadFirstName
        {
            get
            {
                return LegalEntityChief.FirstName;
            }
            set
            {
                string x = TrimNull(value);
                LegalEntityChief.FirstName = x;
                OnPropertyChanged("HeadFirstName");
            }
        }

        private string TrimNull(string value)
        {
            if (value == null)
                return null;
            return value.Trim();
        }

        public string HeadLastName
        {
            get
            {
                return LegalEntityChief.LastName;
            }
            set
            {
                string x = TrimNull(value);
                LegalEntityChief.LastName = x;
                OnPropertyChanged("HeadLastName");
            }
        }

        public string HeadPatronymicName
        {
            get
            {
                return LegalEntityChief.PatronymicName;
            }
            set
            {
                string x = TrimNull(value);
                LegalEntityChief.PatronymicName = x;
                OnPropertyChanged("HeadPatronymicName");
            }
        }

        private string GetFullName()
        {
            if (string.IsNullOrEmpty(LegalEntityChief.PatronymicName))
                return string.Format("{0} {1}", LegalEntityChief.LastName, LegalEntityChief.FirstName);
            return string.Format("{0} {1} {2}", LegalEntityChief.LastName, LegalEntityChief.FirstName, LegalEntityChief.PatronymicName);
        }

        public string HeadPosition
        {
            get
            {
                return LegalEntityChief.Position;
            }
            set
            {
                string x = TrimNull(value);
                LegalEntityChief.Position = x;
                OnPropertyChanged("HeadPosition");
            }
        }

        public string HeadAccordingTo
        {
            get
            {
                return LegalEntityChief.AccordingTo;
            }
            set
            {
                string x = TrimNull(value);
                LegalEntityChief.AccordingTo = x;
                OnPropertyChanged("HeadAccordingTo");
            }
        }

        public string HeadWorkPhone
        {
            get
            {
                return LegalEntityChief.WorkPhone;
            }
            set
            {
                string x = TrimNull(value);
                LegalEntityChief.WorkPhone = x;
                OnPropertyChanged("HeadWorkPhone");
            }
        }

        public string HeadWorkPhoneExtension
        {
            get
            {
                return LegalEntityChief.WorkPhoneExtension;
            }
            set
            {
                string x = TrimNull(value);
                LegalEntityChief.WorkPhoneExtension = x;
                OnPropertyChanged("HeadWorkPhoneExtension");
            }
        }

        public string HeadMobilePhone
        {
            get
            {
                return LegalEntityChief.MobilePhone;
            }
            set
            {
                string x = TrimNull(value);
                LegalEntityChief.MobilePhone = x;
                OnPropertyChanged("HeadMobilePhone");
            }
        }

        public string HeadEmail
        {
            get
            {
                return LegalEntityChief.Email;
            }
            set
            {
                string x = TrimNull(value);
                LegalEntityChief.Email = x;
                OnPropertyChanged("HeadEmail");
            }
        }

        public DateTime? HeadInaugurationDate
        {
            get
            {
                return LegalEntityChief.InaugurationDate;
            }
            set
            {
                LegalEntityChief.InaugurationDate = value;
                OnPropertyChanged("HeadInaugurationDate");
                OnPropertyChanged("HeadRetireDate");
            }
        }

        public DateTime? HeadRetireDate
        {
            get
            {
                return LegalEntityChief.RetireDate;
            }
            set
            {
                LegalEntityChief.RetireDate = value;
                OnPropertyChanged("HeadInaugurationDate");
                OnPropertyChanged("HeadRetireDate");
            }
        }

        public string Comment
        {
            get
            {
                return LegalEntityChief.Comment;
            }
            set
            {
                string x = TrimNull(value);
                LegalEntityChief.Comment = x;
                OnPropertyChanged("Comment");
            }
        }
        #endregion


        #region Commands

        private void InitCommands()
        {
        }
        #endregion

        //public NPFChiefViewModel(ViewModelState action, LegalEntityChief value)
        public NPFChiefViewModel(ViewModelState action, NPFViewModel.LegalEntityChiefParamContainer value)
            : base(typeof(NPFListViewModel))
        {
            DataObjectTypeForJournal = typeof(LegalEntityChief);
            InitCommands();
            State = action;

            if (State == ViewModelState.Create)
            {
                throw new NotSupportedException();
            }
            ID = value.LegalEntityChief.ID;
                
            LegalEntityChiefOriginal = value.LegalEntityChief;
            Model = value.Model;

            LegalEntityChief = new LegalEntityChief();
            LegalEntityChief.ApplyValues(LegalEntityChiefOriginal);
        }

        public NPFChiefViewModel(ViewModelState action, long id)
            : base(typeof(NPFListViewModel))
        {
            DataObjectTypeForJournal = typeof(LegalEntityChief);
            InitCommands();
            State = action;

            if (State == ViewModelState.Create)
            {
                throw new NotSupportedException();
            }
            ID = id;

            LegalEntityChief = DataContainerFacade.GetByID<LegalEntityChief>(ID);
        }

        public void RefreshConnectedLists()
        {
            RefreshListViewModels(new List<Type> { typeof(NPFListViewModel) });
        }


        #region Execute Implementations

        protected override void ExecuteSave()
        {
            switch (State)
            {
                case ViewModelState.Read:
                    break;
                case ViewModelState.Edit:


                    ID = DataContainerFacade.Save(LegalEntityChief);
                    OnPropertyChanged("IsCardSaved");
                    IsDataChanged = false;
                    LegalEntityChiefOriginal.ApplyValues(LegalEntityChief);
                    break;
                case ViewModelState.Create:
                    throw new NotSupportedException();
                    //break;
                default:
                    break;
            }
        }


        protected override void OnCardSaved()
        {
            base.OnCardSaved();
        }

        public override bool CanExecuteSave()
        {
            if (!(String.IsNullOrEmpty(Validate()) && IsDataChanged && !EditAccessDenied && LegalEntityChief.LegalEntityID!=0))
            return false;
            
            return true;
        }

        #endregion

        private string Validate()
        {
            const string fieldNames = "headfirstname|headlastname|headpatronymicname|headposition|headaccordingto|headinaugurationdate|headretiredate|comment";
            foreach (string fieldName in fieldNames.Split("|".ToCharArray()))
            {
                if (!String.IsNullOrEmpty(this[fieldName]))
                {
                    return "Неверный формат данных";
                }
            }

            return null;
        }

        public override string this[string columnName]
        {
            get
            {
                const string errorMessage = "Неверный формат данных";
                switch (columnName.ToLower())
                {
                    //Руководство
                    case "headfirstname": return IsValidLength(HeadFirstName, 100) ? errorMessage : null;
                    case "headlastname": return IsValidLength(HeadLastName, 100) ? errorMessage : null;
                    case "headpatronymicname":
                        if (HeadPatronymicName != null && HeadPatronymicName.Length > 100)
                            return errorMessage;
                        break;
                    case "headposition": return IsValidLength(HeadPosition, 200) ? errorMessage : null;
                    case "headaccordingto": return IsValidLength(HeadAccordingTo, 300) ? errorMessage : null;
                    case "headinaugurationdate": 
                        if (HeadInaugurationDate == null || HeadInaugurationDate == DateTime.MinValue)
                            return errorMessage;
                        if (HeadRetireDate != null && HeadRetireDate < HeadInaugurationDate)
                            return errorMessage;
                        break;
                    case "headretiredate":
                        if (HeadRetireDate == null || HeadRetireDate == DateTime.MinValue)
                            return errorMessage;
                        if (HeadInaugurationDate != null && HeadRetireDate != null && HeadRetireDate < HeadInaugurationDate)
                            return errorMessage;
                        if (Model != null && !Model.IsValidChiefValue(LegalEntityChief, LegalEntityChiefOriginal))
                            return errorMessage;
                        break;
                    case "comment":
                        if (Comment != null && Comment.Length > 512)
                            return errorMessage;
                        break;

                    default: return null;
                }
                return null;
            }
        }

        private bool IsValidLength(string value, int maxLength)
        {
            return string.IsNullOrEmpty(value) || value.Length > maxLength;
        }


        #region 3rd code

        public enum enRetireMode
        {
            CanRetire = 0,
            Retired = 1,
            NoRetire = 2
        }

        private enRetireMode _RetireMode;
        public enRetireMode RetireMode
        {
            get
            {
                return enRetireMode.Retired;
            }
            set
            {
                _RetireMode = value;
                OnPropertyChanged("RetireMode");
            }
        }

        #endregion

        public bool IsEnabled => State != ViewModelState.Read;
    }
}