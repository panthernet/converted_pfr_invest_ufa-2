﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.ListExtensions;
using PFR_INVEST.Core.Properties;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	[DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
    public class CommonPPViewModel : ViewModelCard, IUpdateRaisingModel, ICloseViewModelAfterSave
	{

		public List<Element> DirectionList { get; private set; }
		public List<Element> SectionList { get; private set; }
		public List<Element> LinkList { get; private set; }
		public List<PaymentDetail> PaymentDetailList { get; private set; }
		public List<KBK> KBKList { get; private set; }

		public ICommand SelectRegisterCommand { get; private set; }
		public ICommand SelectTransferCommand { get; private set; }
		public ICommand ClearRegisterCommand { get; private set; }
		public ICommand ClearTransferCommand { get; private set; }
		public ICommand SelectAccountCommand { get; private set; }

		//public string SaveQuestion { get; set; }

		public AsgFinTr PP { get; private set; }

	    public string ContragentName
	    {
	        get { return _contragentName; }
	        set { _contragentName = value; OnPropertyChanged(nameof(ContragentName)); }
	    }

	    public List<long> AllowedSections;
        
		protected CommonPPViewModel(bool isOpfr)
			: base(typeof(TransfersSIListViewModel), typeof(TransfersVRListViewModel), typeof(SIAssignPaymentsListViewModel), typeof(VRAssignPaymentsListViewModel), typeof(SIAssignPaymentsArchiveListViewModel), typeof(VRAssignPaymentsArchiveListViewModel), typeof(TransferFromOrToNPFArchiveListViewModel))
		{
			DataObjectTypeForJournal = typeof(AsgFinTr);
            _isOpfr = isOpfr;



            AllowedSections = _isOpfr
		        ? new List<long> {(long) AsgFinTr.Sections.BackOffice}
		        : new List<long> {(long) AsgFinTr.Sections.NPF, (long) AsgFinTr.Sections.SI, (long) AsgFinTr.Sections.VR};


            DirectionList = BLServiceSystem.Client.GetElementByType(Element.Types.PPDirection);
			var sl = BLServiceSystem.Client.GetElementByType(Element.Types.PPSection).Where(s => AllowedSections.Contains(s.ID)).ToList();
            if(!_isOpfr)
			    sl.Insert(0, new Element { ID = -1, Name = "(не указан)" });
			SectionList = sl;

			LinkList = BLServiceSystem.Client.GetElementByType(Element.Types.PPLink);
			PaymentDetailList = DataContainerFacade.GetList<PaymentDetail>();
			KBKList = DataContainerFacade.GetList<KBK>(false);

			SelectRegisterCommand = new DelegateCommand(o => !IsAdminReadOnly && DirectionElID.HasValue && SectionElID.HasValue, o => { SelectRegisterExecute(); });
			ClearRegisterCommand = new DelegateCommand(o => !IsAdminReadOnly && DirectionElID.HasValue && SectionElID.HasValue && LinkedRegisterID.HasValue,
														o => { SetRegister(null); });
			SelectTransferCommand = new DelegateCommand(o => !IsAdminReadOnly && LinkedRegisterID.HasValue, o => { SelectTransferExecute(); });
			ClearTransferCommand = new DelegateCommand(o => !IsAdminReadOnly && LinkedRegisterID.HasValue && HasTransfer,
														o => { ResetTransfer(); });
			SelectAccountCommand = new DelegateCommand(o => !IsReadOnly, o => { SelectAccountExecute(); });

			//SaveQuestion = "Внимание! При сохранении данного п/п, перечисление будет переведено в архив и п/п перестанет быть редактируемым.\r\nПродолжить сохранение?";
		}

	    public bool IsSectionReadOnly => IsAdminReadOnly || _isOpfr;


        private readonly bool _isOpfr;

		public CommonPPViewModel(long id, ViewModelState action, bool isOpfr = false)
			: this(isOpfr)
		{

            ID = id;
			State = action;

		    if (_isOpfr)
		        PrepareOpfr();

            if (State == ViewModelState.Create)
			{
				PP = new AsgFinTr
				{
					DIDate = DateTime.Now.Date,
					PayDate = DateTime.Now.Date,
					SPNDate = DateTime.Now.Date,
                    PayerIssueDate = null,
					PaySum = 0.00M,
					LinkElID = (long)AsgFinTr.Links.NoLink,
                    StatusID = 1
				};
			    if (_isOpfr)
			        SectionElID = (long) AsgFinTr.Sections.BackOffice;

				RestoreDirection();
				RestoreSection();
				RestorePayDetails();
				RestoreAccount();
				RestoreDates();
			}
			else
			{
				PP = DataContainerFacade.GetByID<AsgFinTr>(id);

				UpdateReadOnly();

				if (PP.PFRBankAccountID.HasValue && PP.PortfolioID.HasValue)
					Account = BLServiceSystem.Client.GetPortfolioFull(PP.PFRBankAccountID.Value, PP.PortfolioID.Value);

				LoadLinkedRegister();

				//Если назначения платежа "удалили", то подгружаем его в список для коректного отображения
				if (PaymentDetailList.FirstOrDefault(pd => pd.ID == PP.PaymentDetailsID) == null)
				{
					var pd = DataContainerFacade.GetByID<PaymentDetail>(PP.PaymentDetailsID);
					if (pd != null)
					{
						PaymentDetailList.Insert(0, pd);
						OnPropertyChanged("PaymentDetailList");
					}
				}

                //Если КБК "удалили", подгружаем его в список
                //http://jira.dob.datateh.ru/browse/DOKIPIV-1631
                //var pd2 = DataContainerFacade.GetByID<PaymentDetail>(PP.PaymentDetailsID);
                if (KBKList.All(k => k.ID != PP.KBKID))
				{
					var kbk = DataContainerFacade.GetByID<KBK>(PP.KBKID);
					if (kbk != null)
						KBKList.Insert(0, kbk);
				}

			    var contr = PP.InnLegalEntityID != null ? DataContainerFacade.GetByID<LegalEntity>(PP.InnLegalEntityID) : null;
                var num = PP.InnContractID != null ? DataContainerFacade.GetByID<Contract>(PP.InnContractID) : null;
			    string txt = "";
			    if (contr != null)
			        txt = contr.FormalizedName;
			    if (num != null)
			        txt += $" {num.ContractNumber}";
			    ContragentName = txt;
			}
		}

		/// <summary>
		/// Загружает данные по связаному реестру
		/// </summary>
		private void LoadLinkedRegister()
		{
			var register = BLServiceSystem.Client.GetCommonRegister((AsgFinTr.Sections?)PP.SectionElID, PP.LinkedRegisterID ?? 0);
			//Если по какой-то причине реестр не нашли, но связь уже есть
			if (register == null && (PP.FinregisterID != null || PP.ReqTransferID != null))
			{
				long regID = 0;
				if (PP.FinregisterID != null)
				{
					var fr = DataContainerFacade.GetByID<Finregister>(PP.FinregisterID.Value);
					var reg = fr.GetRegister();
					regID = reg.ID;

				}
				else if (PP.ReqTransferID != null)
				{
					var rt = DataContainerFacade.GetByID<ReqTransfer>(PP.ReqTransferID.Value);
					var st = rt.GetTransferList();
					regID = st.TransferRegisterID ?? 0;

				}
				register = BLServiceSystem.Client.GetCommonRegister((AsgFinTr.Sections?)PP.SectionElID, regID);
			}

			//Для мигрированых из сальдо записей нет реестров в принципе
			if (!PP.IsMigratedFromSaldo)// || PP.FinregisterID.HasValue || PP.ReqTransferID.HasValue)
			{
				SetRegister(register, true);
				IsRegisterAllowed = PP.SectionElID.HasValue;

				//Загрузка связаного финреестра/перечисления
				if (PP.FinregisterID.HasValue)
				{
					var fr = BLServiceSystem.Client.GetFinregisterForCommonPPByID(PP.FinregisterID.Value);
					SetTransfer(fr);
				}
				else if (PP.ReqTransferID.HasValue)
				{
					var rt = BLServiceSystem.Client.GetReqTransferForCommonPPByID(PP.ReqTransferID.Value);
					SetTransfer(rt);
				}
				else if (_isOpfr)
				{
				    var rtidList = DataContainerFacade.GetListByProperty<LinkPPOPFRTransfer>("PPID", PP.ID);
				    if (rtidList != null && rtidList.Any())
				    {
				        for (int i = 0; i < rtidList.Count; i++)
				        {
                            var rt = BLServiceSystem.Client.GetReqTransferForCommonPPByID(rtidList[i].TransferID, true);
				            SetTransfer(rt);
                            _opfrLinkTransferIDOld =rt.ID;
                        }


                    }
                    else ResetTransfer();
				}
                else ResetTransfer();
			}
		}

		#region Fields
		public string AccountNumber => (Account ?? PortfolioFullListItem.Empty).AccountNumber;

	    public string PFName => (Account ?? PortfolioFullListItem.Empty).PFName;

	    private string _linkedRegisterText;
		public string LinkedRegisterText
		{
			get { return _linkedRegisterText; }
			set { _linkedRegisterText = value; OnPropertyChanged("LinkedRegisterText"); }
		}

		private string _linkedTransferText;
		public string LinkedTransferText
		{
			get { return _linkedTransferText; }
			set { _linkedTransferText = value; OnPropertyChanged("LinkedTransferText"); }
		}

		private PortfolioFullListItem _mAccount;
		public PortfolioFullListItem Account
		{
			get
			{
				return _mAccount;
			}
			set
			{
				_mAccount = value;
				if (_mAccount != null)
				{
					PP.PFRBankAccountID = value.AccountID;
					PP.PortfolioID = value.PortfolioID;

					OnPropertyChanged("Account");
					OnPropertyChanged("AccountNumber");
					OnPropertyChanged("PFName");
				}
			}
		}


		private PaymentDetail _paymentDetail;
		public PaymentDetail PaymentDetail
		{
			get { return _paymentDetail; }
			set
			{
				_paymentDetail = value;
                //http://jira.dob.datateh.ru/browse/DOKIPIV-1631
                //KBKID = value != null ? value.KBKID : null;
                OnPropertyChanged("PaymentDetail");
			}
		}



	    public DateTime? PayerIssueDate
	    {
            get { return PP.PayerIssueDate; }
            set { PP.PayerIssueDate = value; OnPropertyChanged("PayerIssueDate"); }
	    }


	    public long? FinregisterID
		{
			get { return PP.FinregisterID; }
			set { if (PP.FinregisterID != value) { PP.FinregisterID = value; OnPropertyChanged("FinregisterID"); } }
		}
		public decimal? DraftAmount
		{
			get { return PP.DraftAmount; }
			set { if (PP.DraftAmount != value) { PP.DraftAmount = value; OnPropertyChanged("DraftAmount"); } }
		}
		public DateTime? DraftDate
		{
			get { return PP.DraftDate; }
			set { if (PP.DraftDate != value) { PP.DraftDate = value; OnPropertyChanged("DraftDate"); } }
		}
		public DateTime? DIDate
		{
			get { return PP.DIDate; }
			set { if (PP.DIDate != value) { PP.DIDate = value; OnPropertyChanged("DIDate"); } }
		}
		public string DraftRegNum
		{
			get { return PP.DraftRegNum; }
			set { if (PP.DraftRegNum != value) { PP.DraftRegNum = value; OnPropertyChanged("DraftRegNum"); } }
		}
		public DateTime? DraftPayDate
		{
			get { return PP.DraftPayDate; }
			set { if (PP.DraftPayDate != value) { PP.DraftPayDate = value; OnPropertyChanged("DraftPayDate"); } }
		}
		public long? PFRBankAccountID
		{
			get { return PP.PFRBankAccountID; }
			set { if (PP.PFRBankAccountID != value) { PP.PFRBankAccountID = value; OnPropertyChanged("PFRBankAccountID"); } }
		}
		public string Comment
		{
			get { return PP.Comment; }
			set { if (PP.Comment != value) { PP.Comment = value; OnPropertyChanged("Comment"); } }
		}
		public long? PortfolioID
		{
			get { return PP.PortfolioID; }
			set { if (PP.PortfolioID != value) { PP.PortfolioID = value; OnPropertyChanged("PortfolioID"); } }
		}
		public DateTime? PayDate
		{
			get { return PP.PayDate; }
			set
			{
				if (PP.PayDate != value)
				{
					PP.PayDate = value; OnPropertyChanged("PayDate");
					SaveDates();
				}
			}
		}
		public string PayNumber
		{
			get { return PP.PayNumber; }
			set { if (PP.PayNumber != value) { PP.PayNumber = value; OnPropertyChanged("PayNumber"); } }
		}
		public decimal? PaySum
		{
			get { return PP.PaySum; }
			set { if (PP.PaySum != value) { PP.PaySum = value; OnPropertyChanged("PaySum"); } }
		}

		private decimal? _remainingSum;
		public decimal? RemainingSum
		{
			get { return _remainingSum; }
			set { if (_remainingSum != value) { _remainingSum = value; OnPropertyChanged("RemainingSum"); } }
		}

		private decimal? _remainingSumInvest;
		public decimal? RemainingSumInvest
		{
			get { return _remainingSumInvest; }
			set { if (_remainingSumInvest != value) { _remainingSumInvest = value; OnPropertyChanged("RemainingSumInvest"); } }
		}

		public DateTime? SPNDate
		{
			get { return PP.SPNDate; }
			set
			{
				if (PP.SPNDate != value)
				{
					PP.SPNDate = value;
					OnPropertyChanged("SPNDate");
					SaveDates();
				}
			}
		}
		public long? DirectionElID
		{
			get { return PP.DirectionElID; }
			set
			{
				if (PP.DirectionElID != value)
				{
					PP.DirectionElID = value; OnPropertyChanged("DirectionElID");
					//При смене напрвления сбрасываем связи
					ResetTransfer();
					//Проверяем можно ли востановить реестр из запомненого
					ResoreLinkedRegister();
					if (StoredPPDirection2)
						Settings.Default.StoredPPDirectionElId = PP.DirectionElID;

				}
			}
		}
		public long? SectionElID
		{
			get { return PP.SectionElID; }
			set
			{
				if (PP.SectionElID != value)
				{
					if (value == -1)
						value = null;
					PP.SectionElID = value;
					OnPropertyChanged("SectionElID");
					IsRegisterAllowed = value.HasValue;
					//Сброс выбраного реестра
					LinkedRegisterID = null;
					LinkedRegisterText = null;
					//При смене реестра сбрасываем связи
					ResetTransfer();
					//Проверяем можно ли востановить реестр из запомненого
					ResoreLinkedRegister();
					if (StoredPPSection2 && !_isOpfr)
						Settings.Default.StoredPPSection2 = PP.SectionElID;
				}

			}
		}
		public long? LinkElID
		{
			get { return PP.LinkElID; }
			set { if (PP.LinkElID != value) { PP.LinkElID = value; OnPropertyChanged("LinkElID"); } }
		}
		public long? PaymentDetailsID
		{
			get { return PP.PaymentDetailsID; }
			set
			{
				if (PP.PaymentDetailsID != value)
				{
					PP.PaymentDetailsID = value;
					if (StoredPPPayDetails) Settings.Default.StoredPPPayDetails = PP.PaymentDetailsID;
					OnPropertyChanged("PaymentDetailsID");
				}
			}
		}
		public long? KBKID
		{
			get { return PP.KBKID; }
			set { if (PP.KBKID != value) { PP.KBKID = value; OnPropertyChanged("KBKID"); } }
		}
		public long? LinkedRegisterID
		{
			get { return PP.LinkedRegisterID; }
			set { if (PP.LinkedRegisterID != value) { PP.LinkedRegisterID = value; OnPropertyChanged("LinkedRegisterID"); } }
		}

		private bool _isRegisterAllowed;
		public bool IsRegisterAllowed
		{
			get { return _isRegisterAllowed; }
			set { if (_isRegisterAllowed != value) { _isRegisterAllowed = value; OnPropertyChanged("IsRegisterAllowed"); } }
		}

		private bool _isTransferAllowed;
		public bool IsTransferAllowed
		{
			get { return _isTransferAllowed; }
			set { if (_isTransferAllowed != value) { _isTransferAllowed = value; OnPropertyChanged("IsTransferAllowed"); } }
		}

		#endregion Fields

		#region Stored Props
		public bool StoredPPDatesActive
		{
			get
			{
				return Settings.Default.StoredPPDatesActive;
			}
			set
			{
				if (Settings.Default.StoredPPDatesActive != value)
				{
					Settings.Default.StoredPPDatesActive = value;
					if (!value)
						ResetStoredDates();
					else
						SaveDates();
					OnPropertyChanged("StoredPPDatesActive");
				}
			}
		}

		/// <summary>
		/// Первый параметр хранится для линкованного реестра, второй отдельно
		/// </summary>
		public bool StoredPPDirection2
		{
			get { return Settings.Default.StoredPPDirection2; }
			set
			{
				if (Settings.Default.StoredPPDirection2 != value)
				{
					Settings.Default.StoredPPDirection2 = value;
					OnPropertyChanged("StoredPPDirection2");
					Settings.Default.StoredPPDirectionElId = !value ? null : DirectionElID;
					Settings.Default.Save();
				}

			}
		}

		/// <summary>
		/// Первый параметр хранится для линкованного реестра, второй отдельно
		/// </summary>
		public bool StoredPPSection2
		{
			get { return Settings.Default.StoredPPSection2.HasValue; }
			set
			{
				if (value)
				{
					if (SectionElID != Settings.Default.StoredPPSection2)
					{
						Settings.Default.StoredPPSection2 = SectionElID;
						OnPropertyChanged("StoredPPSection2");
						Settings.Default.Save();
					}
				}
				else
				{
					Settings.Default.StoredPPSection2 = null;
					Settings.Default.Save();
				}
			}
		}

		public bool StoredPPPayDetails
		{
			get { return Settings.Default.StoredPPPayDetails.HasValue; }
			set
			{
				if (value)
				{
					if (PaymentDetailsID != Settings.Default.StoredPPPayDetails)
					{
						Settings.Default.StoredPPPayDetails = PaymentDetailsID;
						OnPropertyChanged("StoredPPPayDetails");
						Settings.Default.Save();
					}
				}
				else
				{
					Settings.Default.StoredPPPayDetails = null;
					Settings.Default.Save();
				}
			}
		}

		public bool StoredPPActive
		{
			get
			{
				return Settings.Default.StoredPPActive;
			}
			set
			{
				if (Settings.Default.StoredPPActive != value)
				{
					Settings.Default.StoredPPActive = value;
					if (!value)
						ResetStoredAccount();
					else
						SaveAccount();
					OnPropertyChanged("StoredPPActive");
				}
			}
		}

		public bool StoredPPLinkedRegisterActive
		{
			get
			{
				return Settings.Default.StoredPPLinkedRegisterActive;
			}
			set
			{
				if (Settings.Default.StoredPPLinkedRegisterActive != value)
				{
					Settings.Default.StoredPPLinkedRegisterActive = value;
					OnPropertyChanged("StoredPPLinkedRegisterActive");
				}
			}
		}
		#endregion




		/// <summary>
		/// Установка регистра
		/// </summary>
		/// <param name="register">Регистр</param>
		/// <param name="forLoad">Флаг, указывающий, что регистр загружается при открытии формы (пропуск сброса данных трансфера)</param>
		private void SetRegister(CommonRegisterListItem register, bool forLoad = false)
		{
			if (register != null)
			{
				LinkedRegisterID = register.ID;
				LinkedRegisterText = register.Name;
				IsTransferAllowed = true;
			}
			else
			{
				LinkedRegisterID = null;
				LinkedRegisterText = null;
				IsTransferAllowed = false;
			}
			if (!forLoad)
				ResetTransfer();
			OnPropertyChanged("IsDirectionReadOnly");


		}

		private bool HasTransfer => PP.ReqTransferID.HasValue || PP.FinregisterID.HasValue || _opfrLinkTransferID != 0;

	    private void SetTransfer(FinregisterListItem register)
		{
			if (register != null)
			{
				PP.ReqTransferID = null;
				PP.FinregisterID = register.ID;
				LinkedTransferText = register.NPFName;
				LinkElID = (long)AsgFinTr.Links.NPF;

				//Показваем связь
				IsTransferAllowed = true;
			}
			else
			{
				ResetTransfer();
			}
			OnPropertyChanged("IsDirectionReadOnly");
			RecalcRemainingSum(PP);
		}

	    private long _opfrLinkTransferIDOld;
        private long _opfrLinkTransferID;

	    private bool _enableSaveToFixSomePPValues;
		private void SetTransfer(ReqTransferListItem transfer)
		{
		    if (_isOpfr)
		    {
		        if (transfer == null) ResetTransfer();
		        else
		        {
		            _opfrLinkTransferID = transfer.ID;
		            LinkedTransferText = transfer.UKName;
		            if (LinkElID != (long) AsgFinTr.Links.OPFR)
                        _enableSaveToFixSomePPValues = true;
		            LinkElID = (long) AsgFinTr.Links.OPFR;
                    IsTransferAllowed = true;
		        }
		    }
		    else
		    {

		        if (transfer != null)
		        {
		            PP.FinregisterID = null;
		            PP.ReqTransferID = transfer.ID;
		            LinkedTransferText = transfer.UKName;
		            LinkElID = (long) AsgFinTr.Links.UK;

		            //Показваем связь
		            IsTransferAllowed = true;
		        }
		        else
		        {
		            ResetTransfer();
		        }
		    }
		    OnPropertyChanged("IsDirectionReadOnly");
			RecalcRemainingSum(PP);
		}

		private void ResetTransfer()
		{
			PP.ReqTransferID = null;
			PP.FinregisterID = null;
            LinkedTransferText = null;
            LinkElID = (long)AsgFinTr.Links.NoLink;
		    _opfrLinkTransferID = 0;


            OnPropertyChanged("IsDirectionReadOnly");
			RecalcRemainingSum(PP);
		}

		private void SelectRegisterExecute()
		{
            var selected = DialogHelper.ShowSelectRegisterForPP((AsgFinTr.Sections)SectionElID, DirectionElID ?? 0);
			if (selected != null)
			{
				SetRegister(selected);
				//При смене реестра сбрасываем связи
				ResetTransfer();
			}
		}

		private void SelectTransferExecute()
		{
			if (!LinkedRegisterID.HasValue)
				return;
			switch ((AsgFinTr.Sections?)SectionElID)
			{
				case AsgFinTr.Sections.NPF:
					var fr = DialogHelper.SelectFinregister(LinkedRegisterID.Value);
					if (fr != null)
						SetTransfer(fr);
					break;
				case AsgFinTr.Sections.SI:
					var si = DialogHelper.SelectReqTransferSI(LinkedRegisterID.Value);
					if (si != null)
						SetTransfer(si);
					break;
				case AsgFinTr.Sections.VR:
					var vr = DialogHelper.SelectReqTransferVR(LinkedRegisterID.Value);
					if (vr != null)
						SetTransfer(vr);
					break;
                case AsgFinTr.Sections.BackOffice:
			        var opfr = DialogHelper.SelectReqTransferOPFR(LinkedRegisterID.Value);
                    if (opfr != null)
                        SetTransfer(opfr);
			        break;
			}
		}

		private void SelectAccountExecute()
		{
			var selected = DialogHelper.SelectPortfolio();


			if (selected != null)
			{
				Account = selected;
				SaveAccount();
			}
		}

		#region StoreAccount
		private void SaveAccount()
		{
			if (Settings.Default.StoredPPActive && Account != null)
			{
				Settings.Default.StoredPPPortfolioID = Account.Portfolio.ID;
				Settings.Default.StoredPPPfrBankAccountID = Account.PfrBankAccount.ID;
				Settings.Default.Save();
			}
			else
				ResetStoredAccount();
		}

		private void ResetStoredAccount()
		{
			Settings.Default.StoredPPPortfolioID = null;
			Settings.Default.StoredPPPfrBankAccountID = null;
			Settings.Default.Save();
		}

		private void RestoreAccount()
		{
			var pfID = Settings.Default.StoredPPPortfolioID;
			var pbaID = Settings.Default.StoredPPPfrBankAccountID;
			if (pfID.HasValue && pbaID.HasValue)
			{
				Account = BLServiceSystem.Client.GetPortfolioFull(pbaID.Value, pfID.Value);
			}
		}

		private void RestoreDirection()
		{
			DirectionElID = Settings.Default.StoredPPDirectionElId;
		}


		private void RestoreSection()
		{
		    if (_isOpfr)
		        SectionElID = (long) AsgFinTr.Sections.BackOffice;
			else SectionElID = Settings.Default.StoredPPSection2;
		}

		private void RestorePayDetails()
		{
			if (Settings.Default.StoredPPPayDetails.HasValue && PaymentDetailList.FirstOrDefault(a => a.ID == Settings.Default.StoredPPPayDetails) != null)
				PaymentDetailsID = Settings.Default.StoredPPPayDetails;
		}


		#endregion  StoreAccount

		#region StoreDates
		private void SaveDates()
		{
			if (Settings.Default.StoredPPDatesActive)
			{
				Settings.Default.StoredPPPayDate = PP.PayDate;
				Settings.Default.StoredPPSPNDate = PP.SPNDate;
				Settings.Default.Save();
			}
			else
				ResetStoredDates();
		}

		private void ResetStoredDates()
		{
			Settings.Default.StoredPPPayDate = null;
			Settings.Default.StoredPPSPNDate = null;
			Settings.Default.Save();
		}

		private void RestoreDates()
		{
			if (Settings.Default.StoredPPDatesActive)
			{
				//Если сохранённые даты отсутствуют - оставляем теущие
				PP.PayDate = Settings.Default.StoredPPPayDate ?? PP.PayDate;
				PP.SPNDate = Settings.Default.StoredPPSPNDate ?? PP.SPNDate;
				OnPropertyChanged("PayDate");
				OnPropertyChanged("SPNDate");
			}
		}

		#endregion StoreDates

		#region StoreLinkedRegister
		private void ResoreLinkedRegister()
		{
			StoredPPLinkedRegisterActive = false;
			//Если направление и раздел, как у запомненого - выставляем запомненый реестр
			if (DirectionElID.HasValue && Settings.Default.StoredPPDirection == DirectionElID
				&& SectionElID.HasValue && Settings.Default.StoredPPSection == SectionElID)
			{
				LinkedRegisterID = Settings.Default.StoredPPLinkedRegister;
				if (LinkedRegisterID.HasValue)
				{
					//Если реестр не имеет доступных для платёжек перечислений/финреестров, то игнорируем его
					int countAllowed = 0;
					switch ((AsgFinTr.Sections?)SectionElID)
					{
						case AsgFinTr.Sections.NPF:
							countAllowed = BLServiceSystem.Client.GetFinregisterForCommonPP(LinkedRegisterID.Value).Count;
							break;
						case AsgFinTr.Sections.SI:
							countAllowed = BLServiceSystem.Client.GetReqTransferForCommonPP(LinkedRegisterID.Value, Document.Types.SI).Count;
							break;
                        case AsgFinTr.Sections.VR:
                            countAllowed = BLServiceSystem.Client.GetReqTransferForCommonPP(LinkedRegisterID.Value, Document.Types.VR).Count;
                            break;
                        case AsgFinTr.Sections.BackOffice:
                            countAllowed = BLServiceSystem.Client.GetReqTransferForCommonPP(LinkedRegisterID.Value, Document.Types.OPFR).Count;
                            break;
                    }

                    if (countAllowed == 0)
						LinkedRegisterID = null;

					LoadLinkedRegister();
					if (LinkedRegisterID.HasValue)
						StoredPPLinkedRegisterActive = true;
				}
			}
		}
		private void SaveLinkedRegister()
		{
			//Если при сохранении была галочка - запоминаем, если р
			if (StoredPPLinkedRegisterActive)
			{
				Settings.Default.StoredPPDirection = DirectionElID;
				Settings.Default.StoredPPSection = SectionElID;
				Settings.Default.StoredPPLinkedRegister = LinkedRegisterID;
			}
			//Если нет - зачищаем сохранённые
			else
			{
				Settings.Default.StoredPPDirection = null;
				Settings.Default.StoredPPSection = null;
				Settings.Default.StoredPPLinkedRegister = null;
			}
		}
		#endregion StoreLinkedRegister

		#region Execute implementation
		//private long m_FinregisterPortfolioID;
		//private PortfolioIdentifier.PortfolioPBAParams FinRegisterPortfolioPBAParams = new PortfolioIdentifier.PortfolioPBAParams()
		//{
		//    CurrencyName = new string[] { "рубли" },
		//    AccountType = new string[] { "текущий" },
		//    LegalEntityFormalizedNameContains = new string[] { "цб" }
		//};

		private void ExecuteSelectAccount()
		{
		    var selected = DialogHelper.SelectPortfolio();
		    if (selected == null) return;
		    Account = selected;
		    SaveAccount();
		}

	    protected override bool BeforeExecuteSaveCheck()
		{

			var fr = DataContainerFacade.GetByID<Finregister>(FinregisterID);
			if (fr != null && RegisterIdentifier.FinregisterStatuses.IsNotTransferred(fr.Status))
			{
				DialogHelper.ShowError("Выбранный финреестр был переведен в статус \"Финреестр не передан\"! Добавление п/п невозможно.");
				return false;
			}
			if (_isAdminEditable && _isClosedPayment)
				return DialogHelper.ShowConfirmation("Внимание! Изменение данного платежного поручения повлечет изменения/пересчет данных в других разделах ПТК ДОКИП.\r\nПродолжить сохранение?");

			if (_isAdminEditableSaldo && _isClosedPayment)
				return DialogHelper.ShowConfirmation("Внимание! Данное платежное поручение было мигрировано из Сальдо Лотуса. Изменение данного платежного поручения повлечет изменения/пересчет данных в других разделах ПТК ДОКИП.\r\nПродолжить сохранение?");

			//Выдаём предупреждение если при сохранении ПП финреестр/перечисление будет заполнено на всю  сумму
            //убираем для направления Расход согласно http://jira.vs.it.ru/browse/DOKIPIV-669
            Debug.WriteLine("OLD: " + (DirectionElID != (long)Element.SpecialDictionaryItems.PPDirOutcome));
            Debug.WriteLine("NEW: " + (DirectionElID != (long)AsgFinTr.Directions.FromPFR));

		    if (DirectionElID != (long)AsgFinTr.Directions.FromPFR && IsClosedPP(PP))
		    {
		        DialogHelper.ShowAlert("Внимание! Сумма выбранного перечисления заполнена. Привязка платежного поручения к выбранному перечислению невозможна.");
		        return false;
		    }

			return true;
		}

		public override bool CanExecuteSave()
		{
			return Validate() && (IsDataChanged || _enableSaveToFixSomePPValues);
		}


		protected override void ExecuteSave()
		{
		    _enableSaveToFixSomePPValues = false;

            switch (State)
			{
				case ViewModelState.Read:
					break;
				case ViewModelState.Edit:
				case ViewModelState.Create:
					//Хотфикс сохранения, если КБК удалили
			        if (PP.KBKID != null)
			        {
			            var kbk = DataContainerFacade.GetByID<KBK>(PP.KBKID);
			            if (kbk == null)
			                PP.KBKID = null;
			        }
			        //TODO оптимизировать путем переноса логики в транзакцию в SaveCommonPP
					//запоминаем перед сохранением, не находится ли перечисление в статусе СПН перечислены
					var rt = PP.ReqTransferID.HasValue ? DataContainerFacade.GetByID<ReqTransfer>(PP.ReqTransferID.Value) : null;
					bool isInSpnStatus = rt != null && TransferStatusIdentifier.IsStatusSPNTransfered(rt.TransferStatus);

				    if (_isOpfr)
				        LinkElID = OpfrTransfersList.Any() ? (long) AsgFinTr.Links.OPFR : (long) AsgFinTr.Links.NoLink;

                    //сохраняем п/п
                    PP.ID = ID = BLServiceSystem.Client.SaveCommonPP(PP);
					//если до сохранения, перечисление находилось в статусе СПН перечислены, то, очевидно, происходит правка п/п из БО, когда само перечисление
					//уже в разделе Работа с СИ/ВР. Требуется проверка на необходимость отката перечисления обратно в БО.
					if (isInSpnStatus)
						CheckReqTransferAndRollbackIfPossible();

					UpdateReadOnly();

                  /*  if (_isOpfr && _opfrLinkTransferIDOld != _opfrLinkTransferID)
                    {
                        if (_opfrLinkTransferIDOld > 0)
                            BLServiceSystem.Client.DeleteOpfrPPLink(_opfrLinkTransferIDOld, PP.ID);
                        if (_opfrLinkTransferID > 0)
                            DataContainerFacade.Save(new LinkPPOPFRTransfer { PPID = PP.ID, TransferID = _opfrLinkTransferID });
                    }*/

				    if (_isOpfr)
				    {
				        if (OpfrTransfersList.Any())
				        {
				            OpfrTransfersList.ForEach(a =>
				            {
				                var res = DataContainerFacade.GetListByPropertyConditions<LinkPPOPFRTransfer>(ListPropertyCondition.Equal("PPID", ID),
				                    ListPropertyCondition.Equal("TransferID", a.ID));
				                if (!res.Any())
				                    DataContainerFacade.Save(new LinkPPOPFRTransfer {PPID = PP.ID, TransferID = a.ID});
				            });
				        }

				        if (_opfrTransferToRemove.Any())
				            _opfrTransferToRemove.ForEach(a => BLServiceSystem.Client.RemoveOpfrTransferLink(ID, a));
				    }

                    SaveDates();//Сохраняем даты, если была галочка
					SaveLinkedRegister();//Сохраняем реестр, если была галочка
					break;
			}
		}

		public override bool CanExecuteDelete()
		{
			return State != ViewModelState.Read && (
				//http://jira.vs.it.ru/browse/DOKIPIV-394
				//(LinkElID == null || LinkElID == (long)AsgFinTr.Links.NoLink) //При наличи связи не удалять
									_isAbleToDeletePP
									);
		}

		public override bool BeforeExecuteDeleteCheck()
		{
			bool res;
			if (_isAdminEditableSaldo && _isClosedPayment)
				res = DialogHelper.ShowConfirmation("Внимание! Данное платежное поручение было мигрировано из Сальдо Лотуса. Удаление данного платежного поручения повлечет изменения/пересчет данных в других разделах ПТК ДОКИП.\r\nПродолжить удаление?");
			else res = DialogHelper.ShowQuestion("Вы действительно хотите удалить платежное поручение?", "Внимание");
			return res;
		}

		protected override void ExecuteDelete(int delType = 1)
		{
			CheckReqTransferAndRollbackIfPossible(true);
			DataContainerFacade.Delete(PP);
			base.ExecuteDelete(delType);
		}



		/// <summary>
		/// Проверяем перечисление на наличие статуса СПН перечислены и
		/// откатываем статус перечисления для возврата в БО
		/// </summary>
		private void CheckReqTransferAndRollbackIfPossible(bool isDelete = false)
		{
			if (!PP.ReqTransferID.HasValue) return;
			var rt = DataContainerFacade.GetByID<ReqTransfer>(PP.ReqTransferID.Value);
			if (rt != null && TransferStatusIdentifier.IsStatusSPNTransfered(rt.TransferStatus))
			{
				//Если сумма ПП равна перечислению, то ничего не откатываем (При редактировании)
				if (!isDelete && IsClosingPP(PP))
					return;

				var directionObject = rt.GetTransferList().GetTransferRegister().GetDirection();
				bool directionToUk = false;
				if (directionObject != null && directionObject.isFromUKToPFR != null)
					directionToUk = !directionObject.isFromUKToPFR.Value;
				//переводим статус без удаления всех п/п
				ReqTranserStatusHelper.RollbackStatusFromSPNTransferred(rt, directionToUk, true);
				ViewModelManager.RefreshCardViewModel(typeof(ReqTransferViewModel), rt.ID);
			}
		}

		#endregion
		/// <summary>
		/// Условие возможности удаления п/п
		/// </summary>
		private bool _isAbleToDeletePP;
		private bool _isAdmin;
		private void UpdateReadOnly()
		{
			_isAdminEditable = false;
			_isAdminEditableSaldo = false;
			_isClosedPayment = false;
            //даем права так же работнику ОФПР http://jira.vs.it.ru/browse/DOKIPIV-1066
			_isAdmin = AP.Provider.CurrentUserSecurity.CheckUserInRole(DOKIP_ROLE_TYPE.Administrator);
		    var isOFPRUser = AP.Provider.CurrentUserSecurity.CheckUserInRole(DOKIP_ROLE_TYPE.OFPR_worker);
			_isAdminEditableSaldo = _isAdmin && PP.IsMigrated && PP.IsMigratedFromSaldo;

			if (PP.ReqTransferID.HasValue)
			{
				var rt = DataContainerFacade.GetByID<ReqTransfer>(PP.ReqTransferID.Value);
				if (rt == null) return;
				_isAdminEditable = _isAdmin && !PP.IsMigrated && TransferStatusIdentifier.IsStatusSPNTransfered(rt.TransferStatus);
				//можно удалить, если админ и: запись мигрирована или перечисление вышло из БО в статус СПН перечислены или перечисление в БО
				_isAbleToDeletePP = _isAdminEditable || _isAdminEditableSaldo || (_isAdmin && (TransferStatusIdentifier.IsStatusRequestSentToUK(rt.TransferStatus) || TransferStatusIdentifier.IsStatusRequestRegistered(rt.TransferStatus)));
				_isClosedPayment = TransferStatusIdentifier.IsStatusSPNTransfered(rt.TransferStatus)
									|| TransferStatusIdentifier.IsStatusActSigned(rt.TransferStatus)
									|| TransferStatusIdentifier.IsStatusRequestReceivedByUK(rt.TransferStatus);
                //второй раз для проверки на юзера
                _isAdminEditable = (_isAdmin || isOFPRUser) && !PP.IsMigrated && TransferStatusIdentifier.IsStatusSPNTransfered(rt.TransferStatus);
            }
			else if (PP.FinregisterID.HasValue)
			{
				var fr = DataContainerFacade.GetByID<Finregister>(PP.FinregisterID.Value);
				if (fr == null) return;
				var reg = fr.GetRegister();

				var sumPP = fr.GetDraftsList().Cast<AsgFinTr>().Sum(p => p.DraftAmount);
				//1.3.1.1 финреестр создан (при Типе операции "Передача из ПФР в НПФ")
				//1.3.1.2 финреестр оформлен (при Типе операции "Возврат из НПФ в ПФР")
				_isAdminEditable = _isAdmin && !PP.IsMigrated
									&& ((RegisterIdentifier.FinregisterStatuses.IsCreated(fr.Status) && RegisterIdentifier.IsToNPF(reg.Kind))
											|| (RegisterIdentifier.FinregisterStatuses.IsIssued(fr.Status) && RegisterIdentifier.IsFromNPF(reg.Kind)));
				//можно удалить, если админ и: запись мигрирована или финреестр находится в начальных статусах
				_isAbleToDeletePP = _isAdminEditable || _isAdminEditableSaldo;
                //второй раз для проверки на юзера
                _isAdminEditable = (_isAdmin || isOFPRUser) && !PP.IsMigrated
                                    && ((RegisterIdentifier.FinregisterStatuses.IsCreated(fr.Status) && RegisterIdentifier.IsToNPF(reg.Kind))
                                            || (RegisterIdentifier.FinregisterStatuses.IsIssued(fr.Status) && RegisterIdentifier.IsFromNPF(reg.Kind)));
				_isClosedPayment = (sumPP == fr.Count);
			}
			else _isAbleToDeletePP = true;

            //второй раз для проверки на юзера
            _isAdminEditableSaldo = (_isAdmin || isOFPRUser) && PP.IsMigrated && PP.IsMigratedFromSaldo;

			//Смигрированые записи считаются закрытыми всегда
			if (PP.IsMigrated)
				_isClosedPayment = true;

			OnPropertyChanged(nameof(IsReadOnly));
			OnPropertyChanged(nameof(IsAdminReadOnly));
			OnPropertyChanged(nameof(IsSumReadOnly));
            OnPropertyChanged(nameof(IsSectionReadOnly));
        }


		private bool _isAdminEditable;
		private bool _isAdminEditableSaldo;
		private bool _isClosedPayment;
	    private string _contragentName;

	    public override bool IsReadOnly => State == ViewModelState.Read || (_isClosedPayment && !_isAdminEditable && !_isAdminEditableSaldo);

	    public bool IsAdminReadOnly => IsReadOnly || (_isClosedPayment && _isAdminEditable);

	    public bool IsSumReadOnly
		{
			get
			{
				//если редактируем и разешено редактировать админу
				if (State == ViewModelState.Edit && _isAdminEditable) return false;
				//если редактируем и разешено редактировать админу смигрированое
				if (State == ViewModelState.Edit && _isAdminEditableSaldo) return false;

				return IsReadOnly || IsAdminReadOnly || _isAdminEditableSaldo;
			}
		}

		public bool IsDirectionReadOnly => IsReadOnly || PP.ReqTransferID.HasValue || PP.FinregisterID.HasValue || LinkedRegisterID.HasValue || _opfrLinkTransferID != 0;

	    #region Validation
		private bool Validate()
		{
			return ValidateFields();
		}

		public override string this[string columnName]
		{
			get
			{
				switch (columnName)
				{
					case "DIDate": return DIDate.ValidateRequired();
					case "PayDate": return PayDate.ValidateRequired();
					case "SPNDate": return SPNDate.ValidateRequired();
					case "PayNumber": return PayNumber.ValidateRequired() ?? PayNumber.ValidateMaxLength(50);
					case "DirectionElID": return DirectionElID.ValidateRequired();
					case "PaymentDetailsID": return PaymentDetailsID.ValidateRequired();
					case "AccountNumber": return AccountNumber.ValidateRequired();
					case "PFName": return PFName.ValidateRequired();
					case "PaySum": return PaySum.ValidateMaxFormat() ?? PaySum.ValidateNonZero();
					case "Comment": return Comment.ValidateMaxLength(2500);
					//case "LinkedRegisterText":
					//    if (SectionElID.HasValue)
					//        return LinkedRegisterText.ValidateRequired();
					//    else
					//        return null;
					//case "LinkedRegisterID":
					//    if (SectionElID.HasValue)
					//        return LinkedRegisterID.ValidateRequired();
					//    else
					//        return null;

					default:
						return null;
				}
			}
		}
		#endregion

		IList<KeyValuePair<Type, long>> IUpdateRaisingModel.GetUpdatedList()
		{
			var list = new List<KeyValuePair<Type, long>> { new KeyValuePair<Type, long>(typeof(AsgFinTr), PP.ID) };

			if (PP.ReqTransferID.HasValue)
				list.Add(new KeyValuePair<Type, long>(typeof(ReqTransfer), PP.ReqTransferID.Value));
			else if (PP.FinregisterID.HasValue)
				list.Add(new KeyValuePair<Type, long>(typeof(Finregister), PP.FinregisterID.Value));
            else if(_opfrLinkTransferID != 0)
                list.Add(new KeyValuePair<Type, long>(typeof(OpfrTransfer), _opfrLinkTransferID));

            return list;
		}

        /// <summary>
        /// Проверка на совпадение сумм п/п с суммой перечисления с учетом нового добавляемого п/п
        /// </summary>
        /// <param name="pp">П/п</param>
		public static bool IsClosingPP(AsgFinTr pp)
		{
            bool isClosing = false;
			if (pp.ReqTransferID.HasValue)
			{
				var rt = DataContainerFacade.GetByID<ReqTransfer>(pp.ReqTransferID.Value);
				var sumPP = rt.GetCommonPPList().Where(p => p.ID != pp.ID).Sum(p => p.DraftAmount);
                isClosing = (sumPP + pp.DraftAmount) == rt.Sum;
            }
			else if (pp.FinregisterID.HasValue)
			{
			    var fr = DataContainerFacade.GetByID<Finregister>(pp.FinregisterID.Value);
			    var sumPP = fr.GetDraftsList().Cast<AsgFinTr>().Where(p => p.ID != pp.ID).Sum(p => p.DraftAmount);
			    isClosing = (sumPP + pp.DraftAmount) == fr.Count;
            }
            else
            {
                var trId = DataContainerFacade.GetListByProperty<LinkPPOPFRTransfer>("PPID", pp.ID, true).FirstOrDefault()?.TransferID;
                if (trId.HasValue)
                {
                    var rt = DataContainerFacade.GetByID<OpfrTransfer>(trId);
                    var ppIdList = DataContainerFacade.GetListByProperty<LinkPPOPFRTransfer>("TransferID", trId, true).Where(p => p.PPID != pp.ID).Select(a => a.PPID);
                    var sumPP = DataContainerFacade.GetListByPropertyConditions<AsgFinTr>(ListPropertyCondition.In("ID", ppIdList.Cast<object>().ToArray())).Sum(a => a.DraftAmount) ?? 0;
                    isClosing = sumPP + pp.DraftAmount == rt.Sum;
                }
            }
            return isClosing;
		}

		/// <summary>
        /// Проверка, что п/п закрывает связаное финреестр/перечисление или производится попытка привязать п/п к финреестру/перечислению с исчерпанной суммой
		/// </summary>
		/// <param name="pp">П/п</param>
		public static bool IsClosedPP(AsgFinTr pp)
		{
			var isClosed = false;
			if (pp.ReqTransferID.HasValue)
			{
				var rt = DataContainerFacade.GetByID<ReqTransfer>(pp.ReqTransferID.Value);
				var sumPP = rt.GetCommonPPList().Where(p => p.ID != pp.ID).Sum(p => p.DraftAmount);
				isClosed = sumPP == rt.Sum && (pp.DirectionElID != (long)AsgFinTr.Directions.FromPFR);
			}
			else if (pp.FinregisterID.HasValue)
			{
			    var fr = DataContainerFacade.GetByID<Finregister>(pp.FinregisterID.Value);
			    var sumPP = fr.GetDraftsList().Cast<AsgFinTr>().Where(p => p.ID != pp.ID).Sum(p => p.DraftAmount);
			    isClosed = sumPP == fr.Count && (pp.DirectionElID != (long) AsgFinTr.Directions.FromPFR);
			}
			else
			{
			    var trId = DataContainerFacade.GetListByProperty<LinkPPOPFRTransfer>("PPID", pp.ID, true).FirstOrDefault()?.TransferID;
			    if (trId.HasValue)
			    {
			        var rt = DataContainerFacade.GetByID<OpfrTransfer>(trId);
			        var ppIdList = DataContainerFacade.GetListByProperty<LinkPPOPFRTransfer>("TransferID", trId, true).Where(p => p.PPID != pp.ID).Select(a => a.PPID);
			        var sumPP = DataContainerFacade.GetListByPropertyConditions<AsgFinTr>(ListPropertyCondition.In("ID", ppIdList.Cast<object>().ToArray())).Sum(a => a.DraftAmount) ?? 0;
			        isClosed = sumPP == rt.Sum;
			    }
			}

            return isClosed;
		}


		/// <summary>
		/// Пересчитывает сумму, необходимую для закрытия перечисления/финреестра без учёта указаного ПП
		/// </summary>
		/// <param name="pp"></param>
		/// <returns></returns>
		private void RecalcRemainingSum(AsgFinTr pp)
		{
			RemainingSum = null;
			RemainingSumInvest = null;
		    if (_isOpfr && _opfrLinkTransferID != 0)
		    {
		        //var rt = DataContainerFacade.GetByID<OpfrTransfer>(_opfrLinkTransferID);
		        var ppIdList = DataContainerFacade.GetListByProperty<LinkPPOPFRTransfer>("TransferID", _opfrLinkTransferID, true).Where(p=> p.PPID != PP.ID).Select(a=> a.PPID);
		        var sumPP = DataContainerFacade.GetListByPropertyConditions<AsgFinTr>(ListPropertyCondition.In("ID", ppIdList.Cast<object>().ToArray())).Sum(a=> a.DraftAmount) ?? 0;
                var sumRequired = OpfrTransfersList.Sum(a=> a.Sum);
                RemainingSum = sumRequired - sumPP;
            }
			else if (pp.ReqTransferID.HasValue)
			{
				var rt = DataContainerFacade.GetByID<ReqTransfer>(pp.ReqTransferID.Value);
				var sumPP = rt.GetCommonPPList().Where(p => p.ID != pp.ID).Sum(p => p.DraftAmount) ?? 0;

				var sumRequired = rt.Sum ?? 0;
				RemainingSum = sumRequired - sumPP;

				bool isFromUK = TransferDirectionIdentifier.IsFromUKToPFR(rt.GetTransferList().GetTransferRegister().DirectionID);
				//если Направление = "Приход" И Раздел, которому необходимо видеть п/п = "Работа с СИ" или "Работа с ВР" И инвестдоход больше нуля (положительный и не равен нулю) 
				//
				if (isFromUK && rt.InvestmentIncome > 0)
				{
					var sumInvest = rt.InvestmentIncome ?? 0;
					var sumSPN = sumRequired - sumInvest;

					RemainingSum = Math.Max(0, sumSPN - sumPP);
					RemainingSumInvest = Math.Min(sumInvest, sumRequired - sumPP);
				}


			}
			else if (pp.FinregisterID.HasValue)
			{
				var fr = DataContainerFacade.GetByID<Finregister>(pp.FinregisterID.Value);
				var sumPP = fr.GetDraftsList().Cast<AsgFinTr>().Where(p => p.ID != pp.ID).Sum(p => p.DraftAmount);
				var sumRequired = fr.Count;
				RemainingSum = sumRequired - sumPP;
			}
		}

        #region OPFR VIEW STUFF

        public OpfrTransferListItem SelectedOpfrTransfer { get; set; }
        public ObservableCollection<OpfrTransferListItem> OpfrTransfersList { get; set; } = new ObservableList<OpfrTransferListItem>();
        private List<long> _opfrTransferToRemove = new List<long>();

        public ICommand SelectOpfrTransferCommand { get; set; }
        public ICommand RemoveOpfrTransferCommand { get; set; }

	    public void PrepareOpfr()
	    {
	        SelectOpfrTransferCommand = new DelegateCommand(o => !IsAdminReadOnly && DirectionElID.HasValue && SectionElID.HasValue && (!OpfrTransfersList.Any() || OpfrTransfersList.All(a=> a == null || a.Sum > 0 && !string.IsNullOrEmpty(a.Opfr))), o =>
	        {
	            var result = DialogHelper.ShowAddTransferDialog(DirectionElID.Value);
	            if (result != null)
	            {
	                var tr = BLServiceSystem.Client.GetOpfrTransferListItem(result[1]);
	                OpfrTransfersList.Add(tr);
                    IsDataChanged = true;
                    RecalcRemainingSum(PP);
	            }
	        });

	        RemoveOpfrTransferCommand = new DelegateCommand(o => !IsAdminReadOnly && SelectedOpfrTransfer != null, o =>
	        {
	            _opfrTransferToRemove.Add(SelectedOpfrTransfer.ID);
	            OpfrTransfersList.Remove(SelectedOpfrTransfer);
	            IsDataChanged = true;
                RecalcRemainingSum(PP);
            });

            if(ID > 0)
	            OpfrTransfersList = new ObservableCollection<OpfrTransferListItem>(BLServiceSystem.Client.GetOpfrTransferListItemsByPP(ID));

	    }

        #endregion

    }
}
