﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_manager)]
    public class OrderViewModel : OrderBaseViewModel
    {
        public event EventHandler OnNeedReloadOrderData;
        public event EventHandler CloseBindingWindows;
        public bool IsEventsRegisteredOnLoad = false;

        private void RaiseNeedReloadOrderData()
        {
            if (OnNeedReloadOrderData != null)
                OnNeedReloadOrderData(this, null);
        }

        public event EventHandler OnNonCompetitiveRequestVisibilityChanged;
        private void RaiseNonCompetitiveRequestVisibilityChanged()
        {
            if (OnNonCompetitiveRequestVisibilityChanged != null)
                OnNonCompetitiveRequestVisibilityChanged(this, null);
        }

        #region Types
        private List<string> m_Types;
        public List<string> Types
        {
            get
            {
                return m_Types;
            }
            set
            {
                m_Types = value;
                OnPropertyChanged("Types");
            }
        }

        public string SelectedType
        {
            get
            {
                return Order.Type;
            }
            set
            {
                Order.Type = value;
                RefreshPlaces(false);
                RefreshSecurityInOrderType();
                if (SelectedPortfolio != null)
                {
                    Order.RegNumLong = GenerateCounter(SelectedPortfolio.YearID, SelectedType);
                    RegNum = GenerateOrderRegNum(SelectedPortfolio, SelectedBankAgent, OrderTypeIdentifier.GetOrderShortType(SelectedType), Order.RegNumLong);
                }
                OnPropertyChanged("SelectedType");
                OnPropertyChanged("IsReportsGridColumnsVisible");
            }
        }
        #endregion

        #region Places
        public void RefreshPlaces(bool firstTime)
        {
            if (OrderTypeIdentifier.IsBuyOrder(SelectedType))
            {
                PlacesVisible = true;
                if (SelectedDEPOAccount != null && SelectedDEPOAccount.CurrencyIsRubles)
                    Places = new List<string>(new[] { OrderPlacesIdentifier.SECOND_MARKET, OrderPlacesIdentifier.AUCTION });
                else
                    Places = new List<string>(new[] { OrderPlacesIdentifier.SECOND_MARKET });

                if (firstTime)
                    if (SelectedPlace != null && Places.Any(place => SelectedPlace.CompareTo(place) == 0))
                        SelectedPlace = Places.FirstOrDefault(place => SelectedPlace.CompareTo(place) == 0);
                    else
                        SelectedPlace = Places.First();
                else
                    SelectedPlace = Places.First();
            }
            else
            {
                PlacesVisible = false;
                SelectedPlace = null;
            }

            RefreshNoncompetitiveRequest();
        }

        private List<string> m_Places;
        public List<string> Places
        {
            get
            {
                return m_Places;
            }
            set
            {
                if (value != null && value.Count > 0)
                {
                    m_Places = value;
                    OnPropertyChanged("Places");
                }
            }
        }

        public string SelectedPlace
        {
            get
            {
                return Order.Place;
            }
            set
            {
                Order.Place = value;
                RefreshNoncompetitiveRequest();
                RefreshSecurityInOrderType();
                OnPropertyChanged("SelectedPlace");
            }
        }

        private bool m_PlacesVisible;
        public bool PlacesVisible
        {
            get
            {
                return m_PlacesVisible;
            }
            set
            {
                m_PlacesVisible = value;
                OnPropertyChanged("PlacesVisible");
            }
        }
        #endregion

        #region SelectedPortfolioBankAccount
        public ICommand SelectedPortfolioBankAccountCommand { get; set; }

        private void SelectedPortfolioBankAccountAction(object o)
        {
            var p = new PortfolioIdentifier.PortfolioPBAParams
            {
                AccountType = new[] { "депо" }
                //CurrencyName = currencyName.Any() ? currencyName.ToArray() : null,
                //HiddenPortfolioAccountLinks = portfolioAccountLink.Any() ? portfolioAccountLink.ToArray() : null,
                //BankPortfolioContainAccountType = "депо"
            };

            var portfolioAccount = DialogHelper.SelectPortfolio(p);
            if (portfolioAccount != null)
            {
                SelectedPortfolioBankAccount = portfolioAccount;
            }
        }
        #endregion

        #region Account_Bank_Portfolio Selection
        private string sourceAccount;
        public string SourceAccount => sourceAccount;

        private string sourceBank;
        public string SourceBank => sourceBank;

        private string sourcePortfolio;
        public string SourcePortfolio => sourcePortfolio;
        private PortfolioFullListItem selectedPortfolioBankAccount;
        public PortfolioFullListItem SelectedPortfolioBankAccount
        {
            get { return selectedPortfolioBankAccount; }
            set
            {
                selectedPortfolioBankAccount = value;

                if (value != null)
                {
                    SelectedPortfolio = value.Portfolio;
                    SelectedBankAgent = value.BankLE;
                    SelectedDEPOAccount = new AccountsListItem(value.PfrBankAccount);
                }

            }
        }
        #endregion

        #region BankAgents
        private List<LegalEntity> m_BankAgents;
        public List<LegalEntity> BankAgents
        {
            get
            {
                return m_BankAgents;
            }
            set
            {
                if (value != null && value.Count > 0)
                {
                    m_BankAgents = value;
                    OnPropertyChanged("BankAgents");
                }
            }
        }

        private LegalEntity m_SelectedBankAgent;
        public LegalEntity SelectedBankAgent
        {
            get
            {
                return m_SelectedBankAgent;
            }
            set
            {
                if (value != null && SelectedPortfolio != null)
                {
                    Order.BankAgentID = value.ID;
                    m_SelectedBankAgent = value;
                    RefreshAccounts(true, State);
                    Order.RegNumLong = GenerateCounter(SelectedPortfolio.YearID, SelectedType);
                    RegNum = GenerateOrderRegNum(SelectedPortfolio, SelectedBankAgent, OrderTypeIdentifier.GetOrderShortType(SelectedType), Order.RegNumLong);
                    sourceBank = value.FormalizedName;
                }
                OnPropertyChanged("SelectedBankAgent");
                OnPropertyChanged("SourceBank");
            }
        }
        #endregion

        #region Portfolios
        private List<Portfolio> m_Portfolios;
        public List<Portfolio> Portfolios
        {
            get
            {
                return m_Portfolios;
            }
            set
            {
                if (value != null && value.Count > 0)
                {
                    m_Portfolios = value;
                    OnPropertyChanged("Portfolios");
                }
            }
        }

        private Portfolio m_SelectedPortfolio;
        public Portfolio SelectedPortfolio
        {
            get
            {
                return m_SelectedPortfolio;
            }
            set
            {
                if (value != null)
                {
                    Order.PortfolioID = value.ID;
                    m_SelectedPortfolio = value;
                    RefreshAccounts(true, State);
                    Order.RegNumLong = GenerateCounter(SelectedPortfolio.YearID, SelectedType);
                    RegNum = GenerateOrderRegNum(SelectedPortfolio, SelectedBankAgent, OrderTypeIdentifier.GetOrderShortType(SelectedType), Order.RegNumLong);
                    OnPropertyChanged("SelectedPortfolio");
                    sourcePortfolio = value.Name;
                    OnPropertyChanged("SourcePortfolio");
                }
            }
        }
        #endregion

        #region DEPOAccounts
        private List<AccountsListItem> m_DEPOAccountsList;
        public List<AccountsListItem> DEPOAccountsList
        {
            get
            {
                return m_DEPOAccountsList;
            }
            set
            {
                m_DEPOAccountsList = value;
                OnPropertyChanged("DEPOAccountsList");
            }
        }

        public long SelectedDepoAccountCurrencyID => SelectedDEPOAccount.CurrencyID;

        private AccountsListItem m_SelectedDEPOAccount;
        public AccountsListItem SelectedDEPOAccount
        {
            get
            {
                return m_SelectedDEPOAccount;
            }
            set
            {
                m_SelectedDEPOAccount = value;
                RefreshPlaces(false);
                RefreshSecurityInOrderType();
                if (value != null)
                {
                    Order.DEPOAccountID = value.PfrBankAccountID;
                    sourceAccount = value.ItemName;
                }
                OnPropertyChanged("SelectedDEPOAccount");
                OnPropertyChanged("SourceAccount");
            }
        }
        #endregion

        #region TradeAccounts
        private List<AccountsListItem> m_TradeAccountsList;
        public List<AccountsListItem> TradeAccountsList
        {
            get
            {
                return m_TradeAccountsList;
            }
            set
            {
                m_TradeAccountsList = value;
                OnPropertyChanged("TradeAccountsList");
            }
        }

        private AccountsListItem m_SelectedTradeAccount;
        public AccountsListItem SelectedTradeAccount
        {
            get
            {
                return m_SelectedTradeAccount;
            }
            set
            {
                m_SelectedTradeAccount = value;
                if (value != null)
                    Order.TradeAccountID = value.PfrBankAccountID;
                OnPropertyChanged("SelectedTradeAccount");
            }
        }
        #endregion

        #region NoncompetitiveRequest
        public decimal NoncompetitiveRequest
        {
            get
            {
                if (Order != null)
                    return Order.NoncompReq ?? 0;
                return 0;
            }
            set
            {
                if (Order != null)
                {
                    Order.NoncompReq = value;
                    OnPropertyChanged("NoncompetitiveRequest");
                }
            }
        }

        private void RefreshNoncompetitiveRequest()
        {
            if (SelectedDEPOAccount == null || !SelectedDEPOAccount.CurrencyIsRubles)
            {
                //Не рубли - точно не аукцион
                NoncompetitiveRequestVisible = false;
            }
            if (SelectedPlace == null || SelectedType == null)
            {
                //Не выбрано место или тип - точно не аукцион
                NoncompetitiveRequestVisible = false;
            }
            else
            {
                if (OrderTypeIdentifier.IsBuyOrder(Order.Type) && OrderPlacesIdentifier.IsAuction(Order.Place))
                {
                    //Покупка рублей на аукционе - показываем
                    NoncompetitiveRequestVisible = true;
                }
                else
                {
                    //Не покупка или не аукцион - не показываем
                    NoncompetitiveRequestVisible = false;
                }
            }
        }
        private bool m_NoncompetitiveRequestVisible = true;
        public bool NoncompetitiveRequestVisible
        {
            get
            {
                return m_NoncompetitiveRequestVisible;
            }
            set
            {
                m_NoncompetitiveRequestVisible = value;
                RaiseNonCompetitiveRequestVisibilityChanged();
            }
        }
        #endregion

		#region Person
		//private List<Person> m_PersonList;
		//public List<Person> PersonList
		//{
		//    get
		//    {
		//        return m_PersonList;
		//    }
		//    set
		//    {
		//        m_PersonList = value;
		//        OnPropertyChanged("PersonList");
		//    }
		//}

		private Person m_SelectedPerson;
		public Person SelectedPerson
        {
            get
            {
				return m_SelectedPerson;
            }
            set
            {
				m_SelectedPerson = value;
                if (value != null)
                    Order.FIO_ID = value.ID;
				OnPropertyChanged("SelectedPerson");
            }
        }
        #endregion

		//#region PostList
		//private List<Post> m_PostList;
		//public List<Post> PostList
		//{
		//    get
		//    {
		//        return m_PostList;
		//    }
		//    set
		//    {
		//        m_PostList = value;
		//        OnPropertyChanged("PostList");
		//    }
		//}

		//private Post m_SelectedPost;
		//public Post SelectedPost
		//{
		//    get
		//    {
		//        return m_SelectedPost;
		//    }
		//    set
		//    {
		//        m_SelectedPost = value;
		//        if (value != null)
		//            Order.PostID = value.ID;
		//        OnPropertyChanged("SelectedPost");
		//    }
		//}
		//#endregion

        #region StartTime & Start
        public TimeSpan StartTime
        {
            get
            {
                return Order.StartTime.HasValue ? Order.StartTime.Value : DateTime.Now.TimeOfDay;
            }
            set
            {
                Order.StartTime = value;
                OnPropertyChanged("StartTime");
            }
        }

        public DateTime Start
        {
            get
            {
                if (!Order.Start.HasValue)
                    Order.Start = DateTime.Today;
                return Order.Start.Value;
            }
        }

        ////Temporary solution
        //public override DateTime? RegDateMinValue
        //{
        //    get
        //    {
        //        if (ID > 0)
        //            return RegDate;
        //        else
        //            return DateTime.Today.AddMonths(-1);
        //    }
        //}

        #endregion

        #region Reports
        private List<BuyOrSaleReportsListItem> m_ReportsList;
        public List<BuyOrSaleReportsListItem> ReportsList
        {
            get
            {
                return m_ReportsList;
            }
            set
            {
                m_ReportsList = value;
                OnPropertyChanged("ReportsList");
            }
        }

        public bool ReportsVisible => BuyCurrencyVisible ||
                                      SellCurrencyVisible ||
                                      BuyRubAuctionVisible ||
                                      BuyRubSecondMarketVisible ||
                                      SellRubVisible;

        #endregion

        #region Securities Visibility
        public void RefreshSecurityInOrderType()
        {
            if (SelectedDEPOAccount == null)
                SecurityInOrderType = SecurityInOrderType.Unknown;
            else
            {
                bool CurrencyIsRubles = SelectedDEPOAccount.CurrencyIsRubles;

                if (OrderTypeIdentifier.IsBuyOrder(SelectedType))
                {
                    if (CurrencyIsRubles)
                    {
                        if (SelectedPlace == null)
                            SecurityInOrderType = SecurityInOrderType.Unknown;
                        else if (OrderPlacesIdentifier.IsAuction(SelectedPlace))
                            SecurityInOrderType = SecurityInOrderType.BuyRoublesOnAuction;
                        else if (OrderPlacesIdentifier.IsSecondMarket(SelectedPlace))
                            SecurityInOrderType = SecurityInOrderType.BuyRublesOnSecondMarket;
                        else
                            SecurityInOrderType = SecurityInOrderType.Unknown;
                    }
                    else
                        SecurityInOrderType = SecurityInOrderType.BuyCurrency;
                }
                else if (OrderTypeIdentifier.IsSellOrder(SelectedType))
                    SecurityInOrderType = CurrencyIsRubles ? SecurityInOrderType.SaleRoubles : SecurityInOrderType.SaleCurrency;
                else
                    SecurityInOrderType = SecurityInOrderType.Unknown;
            }
        }

        private SecurityInOrderType m_SecurityInOrderType;
        public SecurityInOrderType SecurityInOrderType
        {
            get
            {
                return m_SecurityInOrderType;
            }
            set
            {
                m_SecurityInOrderType = value;
                OnPropertyChanged("BuyCurrencyVisible");
                OnPropertyChanged("SellCurrencyVisible");
                OnPropertyChanged("BuyRubAuctionVisible");
                OnPropertyChanged("BuyRubSecondMarketVisible");
                OnPropertyChanged("SellRubVisible");
                OnPropertyChanged("ReportsVisible");
            }
        }

        public bool BuyCurrencyVisible => SecurityInOrderType == SecurityInOrderType.BuyCurrency;
        public bool SellCurrencyVisible => SecurityInOrderType == SecurityInOrderType.SaleCurrency;

        public bool BuyRubAuctionVisible => SecurityInOrderType == SecurityInOrderType.BuyRoublesOnAuction;

        public bool BuyRubSecondMarketVisible => SecurityInOrderType == SecurityInOrderType.BuyRublesOnSecondMarket;

        public bool SellRubVisible => SecurityInOrderType == SecurityInOrderType.SaleRoubles;

        public bool IsReportsGridColumnsVisible // SumWithoutNKD & Yield columns should be hidden for SellOrder type selected
            => !OrderTypeIdentifier.IsSellOrder(SelectedType);

        #endregion

        #region CanEdit
        private void RefreshCanEditOrder()
        {
            if (ID < 0)
                //Несохраненное поручение можно редактировать
                CanEditOrder = true;
            else 
                //Сохраненное можно редактировать, только если статус "На подписи"
                CanEditOrder = !Order.IsReadOnly && OrderStatusIdentifier.IsStatusOnSigning(InitialStatus);
        }



        public bool CanEditCbInOrderRelatedProperties => !Order.IsReadOnly && CanEditOrder && CbInOrderListEmpty;

        public bool CanEditTerm => !Order.IsReadOnly && (OrderTypeIdentifier.IsBuyOrder(SelectedType) ? CanEditCbInOrderRelatedProperties : CanEditOrder);

        private bool m_CanEditOrder;
        public bool CanEditOrder
        {
            get
            {
                return m_CanEditOrder;
            }
            set
            {
                m_CanEditOrder = value;
                OnPropertyChanged("CanEditOrder");
                OnPropertyChanged("CanEditCbInOrderRelatedProperties");
                OnPropertyChanged("CanEditTerm");
            }
        }
        #endregion

        #region CbInOrderList
        public bool CbInOrderListEmpty => (CbInOrderList == null || CbInOrderList.Count <= 0);

        public CbInOrderListItem SelectedCbInOrderListItem { get; set; }
        private BindingList<CbInOrderListItem> m_CbInOrderList;
        public BindingList<CbInOrderListItem> CbInOrderList
        {
            get
            {
                return m_CbInOrderList;
            }
            set
            {
                m_CbInOrderList = value;
                OnPropertyChanged("CbInOrderList");
            }
        }
        #endregion

        #region ToRemove
        private List<CbInOrderListItem> toRemoveList;
        public List<CbInOrderListItem> ToRemoveList
        {
            get
            {
                return toRemoveList;
            }

            set
            {
                toRemoveList = value;
                OnPropertyChanged("ToRemoveList");
            }
        }
        #endregion

        public void AddSecurities(BindingList<CbInOrderListItem> items)
        {
            foreach (CbInOrderListItem item in items)
                CbInOrderList.Add(item);
            OnPropertyChanged("CanEditCbInOrderRelatedProperties");
            OnPropertyChanged("CanEditTerm");
        }

        public bool CanExecuteAddSecurities()
        {
            return ID > 0 &&
                   SelectedPortfolio != null &&
                   SelectedBankAgent != null &&
                   SelectedDEPOAccount != null &&
                   OrderStatusIdentifier.IsStatusOnSigning(InitialStatus);
        }

        public ICommand RemoveSecurity { get; private set; }
        private void ExecuteRemoveSecurity()
        {
            if (CbInOrderList != null &&
                CbInOrderList.Count > 0 &&
                SelectedCbInOrderListItem != null)
            {
                if (ToRemoveList == null)
                    ToRemoveList = new List<CbInOrderListItem>();
                ToRemoveList.Add(SelectedCbInOrderListItem);
                CbInOrderList.Remove(SelectedCbInOrderListItem);
                RaiseNeedReloadOrderData();
                RefreshCanEditOrder();
            }
        }

        private bool CanExecuteRemoveSecurity()
        {
			return !Order.IsReadOnly && !CbInOrderListEmpty && (SelectedCbInOrderListItem != null) && (OrderStatusIdentifier.IsStatusOnSigning(InitialStatus));
        }

        public void RefreshAccounts(bool needSelectFirstAccounts, ViewModelState action)
        {
            if ((Order.PortfolioID ?? 0) < 1 || (Order.BankAgentID ?? 0) < 1)
                return;

            //var listDEPO = BLServiceSystem.Client.GetPFRBankAccounts(Order.BankAgentID.Value, Order.PortfolioID.Value, "депо").ConvertAll<AccountsListItem>(pba => new AccountsListItem(pba));
            var listTORG = BLServiceSystem.Client.GetPFRBankAccounts(Order.BankAgentID.Value, Order.PortfolioID.Value, "торговый", true).ConvertAll(pba => new AccountsListItem(pba));
            var listAKT = BLServiceSystem.Client.GetPFRBankAccounts(Order.BankAgentID.Value, Order.PortfolioID.Value, "текущий", true).ConvertAll(pba => new AccountsListItem(pba));

            if (action == ViewModelState.Create)
            {
                //DEPOAccountsList = listDEPO.Where(l => l.Status_ID != -1).ToList();
                TradeAccountsList = listTORG.Where(l => l.Status_ID != -1).ToList();
                TradeAccountsList.AddRange(listAKT.Where(l => l.Status_ID != -1));
            }
            else
            {
                //DEPOAccountsList = listDEPO;
                TradeAccountsList = listTORG.Where(l => l.Status_ID != -1 || l.PfrBankAccountID == Order.TradeAccountID).ToList();
                TradeAccountsList.AddRange(listAKT.Where(l => l.Status_ID != -1 || l.PfrBankAccountID == Order.TradeAccountID).ToList());
            }

            if (needSelectFirstAccounts)
            {
                //SelectedDEPOAccount = DEPOAccountsList.FirstOrDefault();
                SelectedTradeAccount = TradeAccountsList.FirstOrDefault();
            }
        }

        public void RefreshReports()
        {
            bool alreadyChanged = IsDataChanged;
            bool forSale = OrderTypeIdentifier.IsSellOrder(SelectedType);
            ReportsList = BLServiceSystem.Client.GetBuyOrSaleReportsListHib(forSale, ID);
            IsDataChanged = alreadyChanged;
        }

        public override bool CanExecuteDelete()
        {
			return !Order.IsReadOnly && State != ViewModelState.Create;
        }

        public override string CustomDeleteMessage => "При удалении поручения будут удалены все сопутствующие записи и отчеты. Продолжить?";

        protected override void ExecuteDelete(int delType)
        {
            foreach (CbInOrderListItem cbinorderLI in CbInOrderList)
            {
                var reports = DataContainerFacade.GetListByProperty<OrdReport>("CbInOrderID", cbinorderLI.ID);
                foreach (var item in reports)
                    DataContainerFacade.Delete(item);
                DataContainerFacade.Delete(cbinorderLI.CbInOrder);
            }

            if (ToRemoveList != null && ToRemoveList.Count > 0)
                ToRemoveList.ForEach(item => DataContainerFacade.Delete(item.CbInOrder));

            DataContainerFacade.Delete(Order);

            base.ExecuteDelete(DocOperation.Delete);
            if (CloseBindingWindows != null)
            {
                CloseBindingWindows(this, null);
            }
        }

        public override Type DataObjectTypeForJournal => typeof (CbOrder);

        public OrderViewModel(ViewModelState action, long lID)
        {
            RemoveSecurity = new DelegateCommand(o => CanExecuteRemoveSecurity(),
                o => ExecuteRemoveSecurity());

            /*Portfolios = DataContainerFacade.GetListByProperty<Portfolio>("StatusID", 0)
                .Where(a => PortfolioIdentifier.IsType(a.Type, PortfolioIdentifier.PortfolioTypes.DSV) || PortfolioIdentifier.IsType(a.Type, PortfolioIdentifier.PortfolioTypes.SPN))
                .OrderBy(a => a.Year)
                .OrderByDescending(a => a.Type.Trim())
                .ToList();*/

            List<PortfolioFullListItem> list = new List<PortfolioFullListItem>();

            list.AddRange(BLServiceSystem.Client.GetPortfoliosByType());
            

            var c = new PortfolioFullListComparer();
            var PortfolioList = list.Distinct(c).ToList();

            var portfolios = new List<Portfolio>();

            foreach (var item in PortfolioList)
            {
                if (portfolios.Contains(item.Portfolio))
                    continue;
                portfolios.Add(item.Portfolio);
            }

            SelectedPortfolioBankAccountCommand = new DelegateCommand(o => !IsReadOnly, SelectedPortfolioBankAccountAction);
            Portfolios = portfolios.OrderBy(a => a.Year).OrderByDescending(a => a.TypeID).ToList();

            BankAgents = action == ViewModelState.Create ?
                new List<LegalEntity>(BLServiceSystem.Client.GetBankAgentListForDeposit()).Where(a => a.ShortName.Trim() != "ММВБ").ToList() :
                new List<LegalEntity>(BLServiceSystem.Client.GetFullBankAgentListForDeposit()).Where(a => a.ShortName.Trim() != "ММВБ").ToList();
            Types = new List<string>(new[] { OrderTypeIdentifier.BUYING, OrderTypeIdentifier.SELLING });
			//PersonList = DataContainerFacade.GetList<Person>();
			//PostList = new List<Post>(DataContainerFacade.GetList<Post>());

            State = action;

            if (State == ViewModelState.Create)
            {
                Order = new CbOrder();
                RegDate = DateTime.Today;
                DIDate = DateTime.Today;
                Term = DateTime.Today;
                StartTime = DateTime.Now.TimeOfDay;
                SelectedType = Types.FirstOrDefault();
                //SelectedPortfolio = Portfolios.FirstOrDefault();
                //SelectedBankAgent = BankAgents.FirstOrDefault();
                //SelectedDEPOAccount = DEPOAccountsList.FirstOrDefault();
                //SelectedTradeAccount = TradeAccountsList.FirstOrDefault();
                SelectedPlace = Places.FirstOrDefault();
                CbInOrderList = new BindingList<CbInOrderListItem>();
            }
            else
            {
                ID = lID;
                Order = DataContainerFacade.GetByID<CbOrder, long>(ID);

                //m_Counter = Convertion.ToInt64(Order.RegNum.Split('/').Last<string>());
                //m_OriginalCounter = m_Counter;
                //m_OriginalPortfolioYearID = Order.PortfolioID.Value;

                if (Order.PortfolioID.HasValue)
                    m_SelectedPortfolio = Portfolios.Find(
                        pf => pf.ID == Order.PortfolioID
                    );

                if (Order.BankAgentID.HasValue)
                    m_SelectedBankAgent = BankAgents.Find(
                        le => le.ID == Order.BankAgentID
                    );

                RefreshAccounts(false, State);

                selectedPortfolioBankAccount = new PortfolioFullListItem
                {
                    Portfolio = Order.GetPortfolio(),
                    BankLE = Order.GetBankAgent(),
                    PfrBankAccount = Order.GetDEPOAccount()
                };

                sourceAccount = selectedPortfolioBankAccount.AccountNumber;
                sourceBank = selectedPortfolioBankAccount.Bank;
                sourcePortfolio = selectedPortfolioBankAccount.PFName;
                OnPropertyChanged("SourceAccount");
                OnPropertyChanged("SourceBank");
                OnPropertyChanged("SourcePortfolio");
                m_SelectedPortfolio = selectedPortfolioBankAccount.Portfolio;
                m_SelectedBankAgent = selectedPortfolioBankAccount.BankLE;
                m_SelectedDEPOAccount = new AccountsListItem(selectedPortfolioBankAccount.PfrBankAccount);
                OnPropertyChanged("SelectedDEPOAccount");
                OnPropertyChanged("SelectedBankAgent");
                OnPropertyChanged("SelectedPortfolio");

                //if (Order.DEPOAccountID.HasValue)
                //    m_SelectedDEPOAccount = DEPOAccountsList.Find(
                //        acc => acc.PfrBankAccountID == Order.DEPOAccountID
                //    );

                if (Order.TradeAccountID.HasValue)
                    m_SelectedTradeAccount = TradeAccountsList.Find(
                        acc => acc.PfrBankAccountID == Order.TradeAccountID
                    );

                OnPropertyChanged("SelectedTradeAccount");

                RefreshNoncompetitiveRequest();
                RefreshSecurityInOrderType();
                RefreshPlaces(true);

				if (Order.FIO_ID.HasValue)
					SelectedPerson = DataContainerFacade.GetByID<Person>(Order.FIO_ID);
                    

				//if (Order.PostID.HasValue)
				//    SelectedPost = PostList.Find(
				//        post => post.ID == Order.PostID
				//    );

                CbInOrderList = new BindingList<CbInOrderListItem>(BLServiceSystem.Client.GetCbInOrderList(ID));
                if (!CbInOrderListEmpty)
                    SelectedCbInOrderListItem = CbInOrderList.First();

                RefreshReports();
            }

            CbInOrderList.ListChanged += CbInOrderList_ListChanged;
            RefreshStatuses(SelectedStatus);
            InitialStatus = Order.Status;
            RefreshCanEditOrder();
            RefreshCanEditStatus();
        }

        protected override void PostOpenInternal()
        {
            base.PostOpenInternal();
            if (State == ViewModelState.Create)
                IsDataChanged = true;
        }

        void CbInOrderList_ListChanged(object sender, ListChangedEventArgs e)
        {
            OnPropertyChanged("CbInOrderList");
        }

        #region Execute implementation
        public override bool CanExecuteSave()
        {
			return !Order.IsReadOnly && Validate() && IsDataChanged;
        }

        public EventHandler OnMissingSecurities;
        protected void RaiseMissingSecurities()
        {
            if (OnMissingSecurities != null)
                OnMissingSecurities(this, null);
        }

        protected override void ExecuteSave()
        {
            if (!CanExecuteSave())
                return;

            if (OrderStatusIdentifier.IsStatusOnExecution(SelectedStatus) && CbInOrderListEmpty)
            {
                RaiseMissingSecurities();
                return;
            }

            if (!NoncompetitiveRequestVisible && CanEditOrder)
                NoncompetitiveRequest = 0;

            switch (State)
            {
                case ViewModelState.Read:
                    break;
                case ViewModelState.Edit:
                    DataContainerFacade.Save<CbOrder, long>(Order);

                    foreach (CbInOrderListItem cbinorderLI in CbInOrderList)
                    {
                        cbinorderLI.CbInOrder.OrderID = ID;
                        cbinorderLI.CbInOrder.SecurityID = cbinorderLI.Security.ID;
                        bool log = cbinorderLI.CbInOrder.ID == 0;
                        cbinorderLI.CbInOrder.ID = DataContainerFacade.Save<CbInOrder, long>(cbinorderLI.CbInOrder);
                        if(log)
                            JournalLogger.LogSingleEvent<CbInOrder>(JournalEventType.INSERT,  cbinorderLI.CbInOrder.ID, "Добавление ЦБ к Поручению");
                    }

                    if (ToRemoveList != null && ToRemoveList.Count > 0)
                    {
                        ToRemoveList.ForEach(item => DataContainerFacade.Delete(item.CbInOrder));
                        ToRemoveList.Clear();
                    }
                    break;
                case ViewModelState.Create:
                    Order.ID = DataContainerFacade.Save<CbOrder, long>(Order);
                    ID = Order.ID;

                    foreach (CbInOrderListItem cbinorderLI in CbInOrderList)
                    {
                        cbinorderLI.CbInOrder.ID = DataContainerFacade.Save<CbInOrder, long>(cbinorderLI.CbInOrder);
                    }
                    break;
            }

            RefreshStatuses(SelectedStatus);
            InitialStatus = Order.Status;
            RefreshCanEditOrder();
            RefreshCanEditStatus();
        }
        #endregion

        protected override bool Validate()
        {
            return base.Validate() &&
                string.IsNullOrEmpty(this["SelectedDEPOAccount"]) &&
                string.IsNullOrEmpty(this["SelectedTradeAccount"]) &&
                string.IsNullOrEmpty(this["NoncompetitiveRequest"]);
        }

        private const string INVALID_DEPO_ACC = "Не выбран счет ДЕПО";
        private const string INVALID_TRADE_ACC = "Не выбран счет";
        private const string INVALID_NONCOMPETITIVE_REQUEST = "Не указана неконкурентная заявка";
        public override string this[string columnName]
        {
            get
            {
                var bs = base[columnName];
                if (!String.IsNullOrEmpty(bs)) return bs;
                switch (columnName)
                {
                    case "SelectedDEPOAccount": return SelectedDEPOAccount == null ? INVALID_DEPO_ACC : null;
                    case "SelectedTradeAccount": return SelectedTradeAccount == null ? INVALID_TRADE_ACC : null;
                    case "NoncompetitiveRequest": return NoncompetitiveRequestVisible && NoncompetitiveRequest <= 0
                                                    ? INVALID_NONCOMPETITIVE_REQUEST : null;
                    default: return null;
                }
            }
        }

    }

    /// <summary>
    /// Тип операций с ценными бумагами
    /// </summary>
    public enum SecurityInOrderType
    {
        [Description("Покупка валютных ценных бумаг")]
        BuyCurrency,
        [Description("Продажа валютных ценных бумаг")]
        SaleCurrency,
        [Description("Покупка рублёвых ценных бумаг с аукциона")]
        BuyRoublesOnAuction,
        [Description("Покупка рублёвых ценных бумаг на вторичном рынке")]
        BuyRublesOnSecondMarket,
        [Description("Продажа рублёвых ценных бумаг")]
        SaleRoubles,
        [Description("Неопределённый тип")]
        Unknown
    }

    public class AccountsListItem
    {
        public long PfrBankAccountID { get; private set; }
        public string ItemName { get; private set; }
        public bool CurrencyIsRubles { get; private set; }
        public long CurrencyID { get; private set; }
        public long Status_ID { get; private set; }

        public AccountsListItem(PfrBankAccount p_PBA)
        {
            PfrBankAccountID = p_PBA.ID;
            CurrencyID = p_PBA.CurrencyID ?? 0;
            string currencyName = p_PBA.GetCurrency().Name.Trim();
            CurrencyIsRubles = CurrencyIdentifier.IsRUB(currencyName);
            ItemName = String.Format("{0} ({1})", p_PBA.AccountNumber, currencyName);
            Status_ID = p_PBA.StatusID ?? 0;
        }
    }
}
