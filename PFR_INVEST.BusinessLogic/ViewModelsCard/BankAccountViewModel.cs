﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_directory_editor, DOKIP_ROLE_TYPE.OARRS_directory_editor, DOKIP_ROLE_TYPE.OFPR_directory_editor)]
	[DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OARRS_manager)]
	public class BankAccountViewModel : ViewModelCard, IUpdateRaisingModel
	{
		public BankAccount BankAccount { get; private set; }

		public bool ReadAccessOnly => false;

	    private LegalEntity _mLegalEntity;
		public LegalEntity LegalEntity
		{
			get { return _mLegalEntity; }
			set
			{
				if (_mLegalEntity == value) return;
				_mLegalEntity = value;
				OnPropertyChanged("LegalEntity");
			}
		}

        [DisplayName("Наименование получателя платежа")]
        [StringLength(512, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        [Required]
		public string BankAccountLegalEntityName
		{
			get { return BankAccount.LegalEntityName; }
			set
			{
				if (BankAccount.LegalEntityName == value) return;
				BankAccount.LegalEntityName = value;
				OnPropertyChanged("BankAccountLegalEntityName");
			}
		}

	    [DisplayName("Номер рассчетного счета получателя платежа")]
	    [StringLength(40, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
	    [Required]
        public string BankAccountAccountNumber
		{
			get { return BankAccount.AccountNumber; }
			set
			{
			    var val = value;
#if WEBCLIENT
			    val = val.Replace(" ", "");
#endif
                if (BankAccount.AccountNumber == val) return;
				BankAccount.AccountNumber = val;
				OnPropertyChanged("BankAccountAccountNumber");
			}
		}

	    [DisplayName("Банк получателя платежа")]
	    [StringLength(512, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
	    [Required]
        public string BankAccountBankName
		{
			get
			{
				return BankAccount.BankName;
			}
			set
			{
				if (BankAccount.BankName == value) return;
				BankAccount.BankName = value;
				OnPropertyChanged("BankAccountBankName");
			}
		}

	    [DisplayName("Местонахождение банка получателя платежа")]
	    [StringLength(512, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
	    [Required]
        public string BankAccountBankLocation
		{
			get { return BankAccount.BankLocation; }
			set
			{
				if (BankAccount.BankLocation == value) return;
				BankAccount.BankLocation = value;
				OnPropertyChanged("BankAccountBankLocation");
			}
		}

	    [DisplayName("Местонахождение банка получателя платежа")]
	    [StringLength(512, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
	    [Required]
        public string BankAccountCorrespondentAccountNumber
		{
			get { return BankAccount.CorrespondentAccountNumber; }
			set
			{
			    var val = value;
#if WEBCLIENT
			    val = val.Replace(" ", "");
#endif
                if (BankAccount.CorrespondentAccountNumber == val) return;
				BankAccount.CorrespondentAccountNumber = val;
				OnPropertyChanged("BankAccountCorrespondentAccountNumber");
			}
		}

	    [DisplayName("ГРКЦ")]
	    [StringLength(256, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string BankAccountCorrespondentAccountNote
		{
			get { return BankAccount.CorrespondentAccountNote; }
			set
			{
				if (BankAccount.CorrespondentAccountNote == value) return;
				BankAccount.CorrespondentAccountNote = value;
				OnPropertyChanged("BankAccountCorrespondentAccountNote");
			}
		}

	    [DisplayName("БИК банка получателя платежа")]
	    [StringLength(50, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
	    [Required]
        public string BankAccountBIK
		{
			get
			{
				return BankAccount.BIK != null ? BankAccount.BIK.Trim() : null;
			}
			set
			{
				if (BankAccount.BIK == value) return;
				BankAccount.BIK = value;
				OnPropertyChanged("BankAccountBIK");
			}
		}

	    [DisplayName("ИНН банка получателя платежа")]
	    [StringLength(50, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
	    [Required]
        public string BankAccountINN
		{
			get
			{
				return BankAccount.INN != null ? BankAccount.INN.Trim() : null;
			}
			set
			{
				if (BankAccount.INN == value) return;
				BankAccount.INN = value;
				OnPropertyChanged("BankAccountINN");
			}
		}

	    [DisplayName("КПП банка получателя платежа")]
	    [StringLength(50, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
	    [Required]
        public string BankAccountKPP
		{
			get
			{
				return BankAccount.KPP != null ? BankAccount.KPP.Trim() : null;
			}
			set
			{
				if (BankAccount.KPP == value) return;
				BankAccount.KPP = value;
				OnPropertyChanged("BankAccountKPP");
			}
		}

		protected override void ExecuteSave()
		{
			if (!CanExecuteSave()) return;
			switch (State)
			{
				case ViewModelState.Read:
					break;
				case ViewModelState.Edit:
				case ViewModelState.Create:
				    if (State == ViewModelState.Create)
				        BankAccount.LegalEntityID = LegalEntity.ID;
				    BankAccount.ID = ID = BLServiceSystem.Client.SaveBankAccount(BankAccount);					
					break;
			}
			IsDataChanged = false;
		}

		protected void RefreshAccountsInLegalEntityCards()
		{
			//Обновление списка аккаунтов в карточках НПФ
			if (LegalEntity == null || GetViewModelsList == null) return;
			var list = GetViewModelsList(typeof(NPFViewModel));
			if (list != null)
				list.Cast<NPFViewModel>()
					.Where(leVM => leVM.ID == LegalEntity.ID).ToList()
					.ForEach(leVM => leVM.RefreshBankAccountsList());
		}

		public override bool CanExecuteSave()
		{
			return Validate() && IsDataChanged;
		}

		public BankAccountViewModel(long id, ViewModelState state)
			: base(typeof(BankAccountsListViewModel))
		{
			State = state;
			DataObjectTypeForJournal = typeof(BankAccount);

			if (State == ViewModelState.Create)
			{
				ID = 0;
				LegalEntity = DataContainerFacade.GetByID<LegalEntity, long>(id);
				BankAccount = new BankAccount();
			}
			else
			{
				ID = id;
				if (id > 0)
				{
					BankAccount = DataContainerFacade.GetByID<BankAccount, long>(id);
					LegalEntity = DataContainerFacade.GetByID<LegalEntity, long>(BankAccount.LegalEntityID);
				}
			}
#if !WEBCLIENT
            RefreshConnectedCardsViewModels = RefreshAccountsInLegalEntityCards;
#endif
		}

		private const string INVALID_BA_LE_NAME = "Не введено наименование получателя платежа";
		private const string INVALID_BA_ACCOUNT_NUMBER = "Неверно введен номер рассчетного счета получателя платежа";
		private const string INVALID_BA_BANK_NAME = "Не введен банк получателя платежа";
		private const string INVALID_BA_BANK_LOCATION = "Не введено местонахождение банка получателя платежа";
		private const string INVALID_BA_COORESPONDENT_ACCOUNT_NUMBER = "Неверно введен корреспондентский счет банка получателя платежа";
		private const string INVALID_BA_BIK = "Неверно введен БИК банка получателя платежа";
		private const string INVALID_BA_INN = "Неверно введен ИНН банка получателя платежа";
		private const string INVALID_BA_KPP = "Неверно введен КПП банка получателя платежа";

		private const string REQUIRED_FIELD_WARNING = "Поле обязательно для заполнения";
		//private const string AccountNumberPresentationMask = @"\d{1} \d{2} \d{2} \d{3} \d{1} \d{4} \d{3} \d{4}";
		private const string ACCOUNT_NUMBER_STORAGE_MASK = @"\d{20}";

		public override string this[string columnName]
		{
			get
			{
				Regex regex;
				switch (columnName)
				{
					case "BankAccountLegalEntityName":
						return string.IsNullOrWhiteSpace(BankAccountLegalEntityName) ? INVALID_BA_LE_NAME : null;

					case "BankAccountAccountNumber":
						if (BankAccountAccountNumber == null)
							return REQUIRED_FIELD_WARNING;

						regex = new Regex(ACCOUNT_NUMBER_STORAGE_MASK);
						return regex.IsMatch(BankAccountAccountNumber)
							? null
							: INVALID_BA_ACCOUNT_NUMBER;

					case "BankAccountBankName":
						return string.IsNullOrWhiteSpace(BankAccountBankName) ? INVALID_BA_BANK_NAME : null;

					case "BankAccountBankLocation":
						return string.IsNullOrWhiteSpace(BankAccountBankLocation) ? INVALID_BA_BANK_LOCATION : null;

					case "BankAccountCorrespondentAccountNumber":
						if (BankAccountCorrespondentAccountNumber == null)
							return REQUIRED_FIELD_WARNING;

						regex = new Regex(ACCOUNT_NUMBER_STORAGE_MASK);
						return regex.IsMatch(BankAccountCorrespondentAccountNumber)
							? null
							: INVALID_BA_COORESPONDENT_ACCOUNT_NUMBER;

					case "BankAccountBIK":
						return BankAccountBIK == null || BankAccountBIK.Length != 9 ? INVALID_BA_BIK : null;

					case "BankAccountINN":
						return BankAccountINN == null || BankAccountINN.Length != 10 ? INVALID_BA_INN : null;

					case "BankAccountKPP":
						return BankAccountKPP == null || BankAccountKPP.Length != 9 ? INVALID_BA_KPP : null;

					default:
						return null;
				}
			}
		}

		private bool Validate()
		{
			return
				string.IsNullOrEmpty(this["BankAccountLegalEntityName"]) &&
				string.IsNullOrEmpty(this["BankAccountAccountNumber"]) &&
				string.IsNullOrEmpty(this["BankAccountBankName"]) &&
				string.IsNullOrEmpty(this["BankAccountBankLocation"]) &&
				string.IsNullOrEmpty(this["BankAccountCorrespondentAccountNumber"]) &&
				string.IsNullOrEmpty(this["BankAccountBIK"]) &&
				string.IsNullOrEmpty(this["BankAccountINN"]) &&
				string.IsNullOrEmpty(this["BankAccountKPP"]);
		}

		IList<KeyValuePair<Type, long>> IUpdateRaisingModel.GetUpdatedList()
		{
			return new List<KeyValuePair<Type, long>>
			{
				new KeyValuePair<Type, long>(typeof(BankAccount), BankAccount.ID),
				new KeyValuePair<Type, long>(typeof(LegalEntity), BankAccount.LegalEntityID??0)
			};

		}
	}
}
