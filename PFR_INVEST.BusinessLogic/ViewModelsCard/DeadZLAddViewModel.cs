﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager)]
    public class DeadZLAddViewModel : ViewModelCard
    {
        private BindingList<NPFforERZLNotifyListItem> _contragentsList;
        public BindingList<NPFforERZLNotifyListItem> ContragentsList
        {
            get { return _contragentsList; }
            set { _contragentsList = value; OnPropertyChanged("ContragentsList"); }
        }

        private DateTime? _date;
        public DateTime? Date
        {
            get { return _date; }

            set
            {
                if (_date == value) return;
                _date = value;
                OnPropertyChanged("Date");
#if !WEBCLIENT
                RefreshLists();
#endif
            }
        }

#region Execute Implementations

        public override bool CanExecuteDelete()
        {
            return false;
        }

        protected override void ExecuteDelete(int delType) { }

        protected override void ExecuteSave()
        {
            foreach (var caLI in _contragentsList)
            {
                if (caLI.ERZLNotify != null)
                {
                    if (caLI.Count == null || caLI.Count == 0)
                    {
                        DataContainerFacade.Delete(caLI.ERZLNotify);
                        base.ExecuteDelete();
                    }
                    else
                    {
                        caLI.ERZLNotify.Count = caLI.Count;
                        DataContainerFacade.Save(caLI.ERZLNotify);    
                    }
                }
                else if (caLI.Contragent != null && caLI.Count != null)
                {
                    var crAcc = BLServiceSystem.Client.GetContragentAccountByMeasure(caLI.Contragent.ID, AccountIdentifier.MeasureNames.ZL);
                    var dbtAcc = BLServiceSystem.Client.GetContragentAccountByMeasure(caLI.Contragent.ID, AccountIdentifier.MeasureNames.DeadZL);

                    if (crAcc == null)
                    {
                        crAcc = new Account
                        {
                            ContragentID = caLI.Contragent.ID,
                            AccountTypeID = AccountIdentifier.MeasureIDs.ZL,
                            Name = AccountIdentifier.MeasureNames.ZL
                        };
                        crAcc.ID = DataContainerFacade.Save(crAcc);
                    }
                    if (dbtAcc == null)
                    {
                        dbtAcc = new Account
                        {
                            ContragentID = caLI.Contragent.ID,
                            AccountTypeID = AccountIdentifier.MeasureIDs.DeadZL,
                            Name = AccountIdentifier.MeasureNames.DeadZL
                        };
                        dbtAcc.ID = DataContainerFacade.Save(dbtAcc);
                    }
                    
                    var erzlNotify = new ERZLNotify
                    {
                        Count = caLI.Count,
                        Date = Date,
                        CrAccID = crAcc.ID,
                        DbtAccID = dbtAcc.ID,
                        MainNPF = "0"
                    };
                    erzlNotify.ID = DataContainerFacade.Save<ERZLNotify, long>(erzlNotify);            
                }
            }
#if !WEBCLIENT
            RefreshLists();
            RefreshConnectedViewModels();
#endif
            IsDataChanged = false;
        }

        public override bool CanExecuteSave()
        {
            return IsDataChanged && IsValid();
        }
#endregion


#if WEBCLIENT
        public static List<NPFforERZLNotifyListItem> WebRefreshList(DateTime? date)
        {
            return !date.HasValue ? null : BLServiceSystem.Client.GetDeadZLForAllNPF(date.Value);
        }
#endif

        public void RefreshLists()
        {
#if WEBCLIENT
            ContragentsList = new BindingList<NPFforERZLNotifyListItem>(WebRefreshList(_date));
            IsDataChanged = false;
            return;
#endif

            if (!Date.HasValue) return;
            try
            {
                
                var sourceList = BLServiceSystem.Client.GetDeadZLForAllNPF(Date.Value);
                _contragentsList = new BindingList<NPFforERZLNotifyListItem>(sourceList);
                _contragentsList.ListChanged += contragentsList_ListChanged;
                OnPropertyChanged("ContragentsList");
            }
            catch (Exception e)
            {
                if (DoNotThrowExceptionOnError == false)
                    throw;

                Logger.WriteException(e);
                throw new ViewModelInitializeException();
            }
            IsDataChanged = false;
        }

        void contragentsList_ListChanged(object sender, ListChangedEventArgs e)
        {
            IsDataChanged = !_contragentsList.All(a => a.Count == a.DefaultCount || (a.Count == null && a.DefaultCount == 0));
        }

        public DeadZLAddViewModel()
#if !WEBCLIENT
            : base(typeof(DeadZLListViewModel))
            {
#else
        { 
            Date = DateTime.Today;
#endif
            DataObjectTypeForJournal = typeof(ERZLNotify);
            RefreshLists();
        }

        public override string this[string columnName] => null;

        private bool IsValid()
        {
            return Date.HasValue && ContragentsList != null;
        }

    }
}
