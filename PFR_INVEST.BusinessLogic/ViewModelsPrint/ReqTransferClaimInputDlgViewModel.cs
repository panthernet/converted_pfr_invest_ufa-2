﻿using System;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
	public class ReqTransferClaimInputDlgViewModel : ViewModelCardDialog, IRequestCloseViewModel
	{
        public DelegateCommand OkCommand { get; set; }

	    private DateTime? _trancheDate;
		public DateTime? TrancheDate
		{
			get { return _trancheDate; }
			set
			{
			    _trancheDate = value;
				OnPropertyChanged("TrancheDate");
				OnPropertyChanged("IsValid");
			}
		}

	    private string _trancheNumber;
        public string TrancheNumber
        {
            get { return _trancheNumber; }
            set
            {
                _trancheNumber = value;
                OnPropertyChanged("TrancheNumber");
                OnPropertyChanged("IsValid");
            }
        }

	    public ReqTransferClaimInputDlgViewModel(bool isVr, string regNumber)
	    {
            OkCommand = new DelegateCommand(o => string.IsNullOrWhiteSpace(Validate()), o=> OnRequestClose());
            TrancheDate = DateTime.Today;
	        TrancheNumber = string.Format(@"{0}-{1}/-", isVr ? "ВР" : "СИ", regNumber);
	    }

		public override string this[string columnName]
		{
			get
			{
				switch (columnName.ToLower())
				{
                    case "tranchedate": return this.TrancheDate == null ? "Необходимо указать дату заявки" : null;
                    case "tranchenumber": return string.IsNullOrWhiteSpace(TrancheNumber) ? "Необходимо указать номер заявки" : TrancheNumber.ValidateMaxLength(50);
					default: return null;
				}
			}
		}

		public bool IsValid => string.IsNullOrWhiteSpace(Validate());

	    private string Validate()
		{
			const string fieldNames =
				"tranchedate|tranchenumber";

			return fieldNames.Split("|".ToCharArray()).Any(fieldName => !string.IsNullOrEmpty(this[fieldName])) ? "Неверный формат данных" : null;
		}

	    public event EventHandler RequestClose;

	    protected void OnRequestClose()
	    {
	        if (RequestClose != null)
	            RequestClose(this, null);
	    }
	}
}
