﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Office.Interop.Word;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Helpers;
using Application = Microsoft.Office.Interop.Word.Application;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class PrintOrder : ViewModelBase
    {
        private const string wtBuyVEB = "Поручение покупка ВЭБ.doc";
        public const string wtBuyCB = "Поручение Покупка ЦБ.doc";
        private const string wtBuyCB_Auc = "Поручение Покупка ЦБ Аукцион.doc";
        private const string wtSaleVeb = "Поручение продажа ВЭБ.doc";
        private const string wtSaleCB = "Поручение продажа ЦБ.doc";
        private const string wtBuyCBRF = "Поручение покупка СБРФ.doc";

        private Application wApp;
        private object missing = System.Reflection.Missing.Value;
        private object isVisible = true;

        private readonly CbOrder order;

        //private readonly Dictionary<string, DBField> order;
        private Microsoft.Office.Interop.Word.Document wDoc;

        public decimal FixedPercent { get; private set; }

        public PrintOrder(long or_id, Decimal _fixedPercent)
            : base()
        {
            this.order = DataContainerFacade.GetByID<CbOrder, long>(or_id);
            //order = BLServiceSystem.Client.GetOrder(or_id);
            FixedPercent = _fixedPercent;
        }

        /// <summary>
        /// Имя шаблона word
        /// </summary>
        public string getTemplateName()
        {
            bool isBuy = (this.order != null && OrderTypeIdentifier.IsBuyOrder(this.order.Type));
            bool isAuction = (this.order != null && OrderPlacesIdentifier.IsAuction(this.order.Place));


            bool isVEB = false;
            bool isCB = false;
            bool isSBRF = false;

            bool isRUB = false;

            try
            {
                LegalEntity legalEntity = this.order.GetBankAgent();
                isVEB = BankHelper.IsVEB(legalEntity);
                isCB = BankHelper.IsCB(legalEntity);
                isSBRF = BankHelper.IsSBRF(legalEntity);

                isRUB = CurrencyHelper.IsRUB(order.GetDEPOAccount().GetCurrency());
            }
            catch { }


            if (isBuy)
            {
                if (isVEB)
                    return isRUB ? "" : wtBuyVEB;
                if (isSBRF)
                    return wtBuyCBRF;
                if (isCB)
                    return isAuction ? wtBuyCB_Auc : wtBuyCB;
            }
            else
            {
                if (isVEB) return wtSaleVeb;
                if (isCB) return wtSaleCB;
            }

            return string.Empty;
        }

        /// <summary>
        /// заполнение документа
        /// </summary>
        public void generateDoc(string fName)
        {

            PfrBankAccount trAcc = null;
            try { trAcc = this.order.GetTradeAccount(); }
            catch { }

            PfrBankAccount depoAcc = null;
            try { depoAcc = this.order.GetDEPOAccount(); }
            catch { }

            List<CbInOrder> cbinorderList = null;
            try { cbinorderList = new List<CbInOrder>(this.order.GetCbInOrderList().Cast<CbInOrder>()); }
            catch { }

            Person person = null;
			try { person = this.order.GetPerson(); }
            catch { }

            string term = string.Empty;
            if (this.order != null && this.order.Term != null)
            {
                if (fName == wtBuyCB || fName == wtBuyCB_Auc || fName == wtSaleCB)
                    //исполнить до = испольнить по + 1
                    term = this.order.Term.Value.AddDays(1).ToString("dd.MM.yyyy");
                else
                    term = this.order.Term.Value.ToString("dd.MM.yyyy");
            }

            Dictionary<string, string> replaces = new Dictionary<string, string>();

            replaces.Add("{TERM}", term);
            replaces.Add("{REGNUM}", this.order != null ? this.order.RegNum : "");
			replaces.Add("{COMMISSIONER}", this.order != null ? (this.order.Commissioner ?? (person ?? new Person()).LastName) : "");
            replaces.Add("{TORG}", trAcc != null ? trAcc.AccountNumber : "");
            replaces.Add("{DEPO}", depoAcc != null ? depoAcc.AccountNumber : "");
            replaces.Add("{ORDER.DATE}", order.RegDate.Value.ToString("dd.MM.yyyy"));

            foreach (string key in replaces.Keys)
                OfficeTools.FindAndReplace(wApp, key, replaces[key]);

            if (cbinorderList == null || cbinorderList.Count == 0)
                return;

            String replaceStr = String.Empty;
            Table cbTable = wDoc.Tables[1];

            if (fName == wtBuyCBRF)
            {
                var cbinord = cbinorderList.FirstOrDefault();
                if (cbinord != null)
                {
                    var sec = cbinord.GetSecurity();
                    if (sec != null)
                    {
                        Dictionary<string, string> cbFields = new Dictionary<string, string>();
                        cbFields.Add("{CB.REGNUM}", sec.SecurityId != null ? sec.SecurityId.Trim() : "");
                        cbFields.Add("{CB.COUNT}", cbinord.Count != null ? cbinord.Count.ToString() : "");
                        cbFields.Add("{CB.COUNT.PROP}", Число.Пропись((decimal)(cbinord.Count ?? 0), new ЕдиницаИзмерения(РодЧисло.Женский, "", "", "")));
                        cbFields.Add("{CB.SUMM}", Util.format(cbinord.SumMoney));
                        cbFields.Add("{CB.SUMM.PROP}", Число.Пропись(cbinord.SumMoney ?? 0, new ЕдиницаИзмерения(РодЧисло.Мужской, "", "", "")));
                        cbFields.Add("{CB.CURR.ISO}", "RUB");
                        foreach (string key in cbFields.Keys)
                            OfficeTools.FindAndReplace(wApp, key, cbFields[key]);
                    }
                }
            }
            else if (fName.CompareTo(wtBuyVEB) == 0)
            {
                foreach (CbInOrder cbinord in cbinorderList)
                {
                    Security sec = null;
                    try { sec = cbinord.GetSecurity(); }
                    catch { sec = null; }

                    if (sec != null)
                    {
                        Row row = cbTable.Rows.Add(ref missing);
                        row.Cells[1].Range.Text = sec.Name != null ? sec.Name.Trim() : "";
                        row.Cells[2].Range.Text = cbinord.NomValue.HasValue ? (string.Format("{0:0.00} \r\n({1})", cbinord.NomValue, Сумма.Пропись(cbinord.NomValue.Value, Валюта.Доллары))) : "";
                        row.Cells[3].Range.Text = cbinord.MaxNomValue.HasValue ? (string.Format("{0:0.00} \r\n({1})", cbinord.MaxNomValue, Сумма.Пропись(cbinord.MaxNomValue.Value, Валюта.Доллары))) : "";
                        row.Cells[4].Range.Text = string.Format("{0:N4}", (cbinord.MaxPrice ?? 0.0000M));
                        row.Cells[5].Range.Text = term;
                    }
                }
            }
            else if (fName.CompareTo(wtBuyCB) == 0 || fName.CompareTo(wtBuyCB_Auc) == 0)
            {
                string lastSecurityName = "";

                foreach (CbInOrder cbinord in cbinorderList)
                {
                    Security sec = null;
                    try { sec = cbinord.GetSecurity(); }
                    catch { sec = null; }

                    if (sec != null)
                    {
                        Row row = cbTable.Rows.Add(ref missing);
                        row.Cells[1].Range.Text = sec.SecurityId != null ? sec.SecurityId.Trim() : "";
                        row.Cells[2].Range.Text = cbinord.Count != null ? cbinord.Count.ToString() : "";
                        row.Cells[3].Range.Text = Util.format(cbinord.SumMoney);

                        decimal summ = 0;
                        if (FixedPercent != -1)
                            summ = cbinord.FixPriceBay ?? 0;
                        else
                            summ = cbinord.MaxPriceBay ?? 0;

                        row.Cells[4].Range.Text = string.Format("{0:n4}", summ);

                        lastSecurityName = row.Cells[1].Range.Text;
                    }
                }

                if (FixedPercent != -1)
                {
                    Row row = cbTable.Rows.Add(ref missing);

                    row.Cells[1].Range.Text = lastSecurityName;
                    row.Cells[2].Range.Text = "";
                    row.Cells[3].Range.Text = string.Format("На сумму {0}{1}({2})",
                        Math.Round(FixedPercent, 2),
                        Environment.NewLine,
                        Сумма.Пропись(FixedPercent, Валюта.Рубли));
                    row.Cells[4].Range.Text = "Неконкурентная Заявка";
                }
            }
            else if (fName.CompareTo(wtSaleVeb) == 0)
            {
                foreach (CbInOrder cbinord in cbinorderList)
                {
                    Security sec = null;
                    try { sec = cbinord.GetSecurity(); }
                    catch { sec = null; }

                    if (sec != null)
                    {
                        Row row = cbTable.Rows.Add(ref missing);
                        row.Cells[1].Range.Text = sec.Name != null ? sec.Name.Trim() : "";
                        row.Cells[2].Range.Text = cbinord.NomValue.HasValue ? (string.Format("{0:0.00} \r\n({1})", cbinord.NomValue, Сумма.Пропись(cbinord.NomValue.Value, Валюта.Доллары))) : "";
                        row.Cells[3].Range.Text = cbinord.MinValueNKD.HasValue ? (string.Format("{0:0.00} \r\n({1})", cbinord.MinValueNKD, Сумма.Пропись(cbinord.MinValueNKD.Value, Валюта.Доллары))) : "";
                        row.Cells[4].Range.Text = string.Format("{0:N4}", (cbinord.MinPrice ?? 0.0000M));
                        row.Cells[5].Range.Text = term;
                    }
                }
            }
            else if (fName.CompareTo(wtSaleCB) == 0)
            {
                foreach (CbInOrder cbinord in cbinorderList)
                {
                    Security sec = null;
                    try { sec = cbinord.GetSecurity(); }
                    catch { sec = null; }

                    if (sec != null)
                    {
                        Row row = cbTable.Rows.Add(ref missing);
                        row.Cells[1].Range.Text = sec.SecurityId != null ? sec.SecurityId.Trim() : "";
                        row.Cells[2].Range.Text = cbinord.Count != null ? cbinord.Count.ToString() : "";
                        row.Cells[3].Range.Text = string.Format("{0:N4}", (cbinord.MinPriceSell ?? 0.0000M));
                    }
                }
            }
        }

        public static String GenMoneyString(bool rub, decimal money)
        {
            switch (Convert.ToInt32(money % 10))
            {
                case 0:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                    return money.ToString() + " " + ((rub) ? "рублей" : "копеек");
                case 1:
                    return money.ToString() + " " + ((rub) ? "рубль" : "копейка");
                case 2:
                case 3:
                case 4:
                    return money.ToString() + " " + ((rub) ? "рубля" : "копейки");
                default:
                    return money.ToString() + " " + ((rub) ? "рублей" : "копеек");
            }
        }

        /// <summary>
        /// вызов на печать
        /// </summary>
        public void print()
        {
            if (order == null) return;

            string wordTemplateName = getTemplateName();
            object filePath = TemplatesManager.ExtractTemplate(wordTemplateName);

            if (File.Exists(filePath.ToString()))
            {
                wApp = new Application();
                wDoc = wApp.Documents.Add(ref filePath, ref missing, ref missing, ref isVisible);

                generateDoc(wordTemplateName);

                wApp.Visible = true;
            }
            else
            {
                RaiseErrorLoadingTemplate(wordTemplateName);
            }
        }
    }

}
