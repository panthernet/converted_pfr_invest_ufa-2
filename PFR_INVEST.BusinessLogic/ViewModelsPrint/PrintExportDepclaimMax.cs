﻿
using System.Runtime.InteropServices;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Common.Extensions;

namespace PFR_INVEST.BusinessLogic.ViewModelsPrint
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Threading;

    using Microsoft.Office.Interop.Excel;
    using DataObjects;
    using DataObjects.ListItems;

    using Application = Microsoft.Office.Interop.Excel.Application;
    using DataAccess.Client;

    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class PrintExportDepclaimMax : ViewModelBase
    {
        private const string TEMPLATE_DEP_CLAIM_MAX = "Список заявок с учетом максимальной суммы размещения.xls";
        private const string TEMPLATE_DEP_CLAIM = "Сводный реестр заявок.xls";
        private Application _excelApp;
        private Worksheet _worksheet;
        private readonly List<DepClaimMaxListItem> _depClaimMaxList;
        private readonly DepClaimMaxListItem _depClaimMax;
        private DateTime _dtCreate;
        private readonly int _typeDepClaimMax;
        private const string FORMAT = "dd.MM.yyyy";
        private readonly DepClaimSelectParams _auction;
        private readonly string _filePath;

        public PrintExportDepclaimMax(List<DepClaimMaxListItem> depClaimMaxList, int typeDepClaimMax, DateTime dtCreate, string filePath = null)
        {
            _filePath = filePath;
            _depClaimMaxList = depClaimMaxList;
            _dtCreate = dtCreate;
            _typeDepClaimMax = typeDepClaimMax;
            _depClaimMax = _depClaimMaxList.Any() ? _depClaimMaxList[0] : null;
            if (depClaimMaxList != null && depClaimMaxList.Count > 0)
                _auction = DataContainerFacade.GetByID<DepClaimSelectParams>(depClaimMaxList[0].DepclaimselectparamsId);
        }

        private void GenerateDoc()
        {

            if (_depClaimMax == null) return;
            _worksheet.Range["E4"].Cells.Value2 = _dtCreate.ToString(FORMAT);
            _worksheet.Range["C2"].Cells.Value2 = _depClaimMax.TradeDate.ToString(FORMAT);
            _worksheet.Range["B5"].Cells.Value2 = _depClaimMax.BoardId;
            //Worksheet.get_Range("B6").Cells.Value2 = depClaimMax.SecurityId;
            //Worksheet.get_Range("B6").Cells.Value2 = _auction != null ? (string.Format("PFDEP_T{0}DT", _auction.Period.ToString("D3"))) : string.Empty;
            _worksheet.Range["B6"].Cells.Value2 = _auction != null ? _auction.SecurityId : string.Empty;
            _worksheet.Range["B7"].Cells.Value2 = _depClaimMax.SettleDate.ToString(FORMAT);
            _worksheet.Range["E7"].Cells.Value2 = _depClaimMax.SettleDate2.ToString(FORMAT);

            int item = 10;
            long quantPart = _depClaimMaxList.First().QuantPart;
            long quantReq = _depClaimMaxList.First().QuantReq;
            decimal totval1 = _depClaimMaxList.Sum(dcml => dcml.Totval1);

            object obj = _worksheet.Range["A" + item].EntireRow.Copy();
            foreach (var depClaimMaxItem in _depClaimMaxList)
            {
                _worksheet.Range["A" + item].EntireRow.Insert(obj);
                _worksheet.Range["A" + item].Cells.Value2 = depClaimMaxItem.Rate.ToString();
                _worksheet.Range["B" + item].Cells.Value2 = depClaimMaxItem.Totval1.ToString();
                _worksheet.Range["C" + item].Cells.Value2 = depClaimMaxItem.Totval1N.ToString();
                _worksheet.Range["D" + item].Cells.Value2 = depClaimMaxItem.Totval2.ToString();
                _worksheet.Range["E" + item].Cells.Value2 = depClaimMaxItem.Warate.ToString();
                item++;
            }
            _worksheet.Range["A" + item].EntireRow.Delete();
            _worksheet.Range["B" + item].Cells.Value2 = totval1.ToString();
            _worksheet.Range["B" + (item + 1)].Cells.Value2 = quantPart.ToString();
            _worksheet.Range["B" + (item + 2)].Cells.Value2 = quantReq.ToString();
            _worksheet.Range["B" + (item + 4)].Cells.Value2 = totval1.ToString();
            _worksheet.Range["A1"].Select();
        }

        public bool Print()
        {
            string templateName = string.Empty;
            if (_typeDepClaimMax == (int)DepClaimMaxListItem.Types.Max) templateName = TEMPLATE_DEP_CLAIM_MAX;
            if (_typeDepClaimMax == (int)DepClaimMaxListItem.Types.Common) templateName = TEMPLATE_DEP_CLAIM;
            object filePath = TemplatesManager.ExtractTemplate(templateName);

            if (File.Exists(filePath.ToString()))
            {
                RaiseStartedPrinting();
                var oldCi = Thread.CurrentThread.CurrentCulture;
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                try
                {
                    _excelApp = new Application { Visible = false, DisplayAlerts = false};
                    _worksheet = (Worksheet)_excelApp.Workbooks.Open(filePath.ToString()).Sheets[1];
                    GenerateDoc();
                    if (!string.IsNullOrEmpty(_filePath))
                    {
                        _worksheet.SaveAs(
                            Path.Combine(_filePath ?? "",
                                (_typeDepClaimMax == (int) DepClaimMaxListItem.Types.Common ? TEMPLATE_DEP_CLAIM : TEMPLATE_DEP_CLAIM_MAX).AttachUniqueTimeStamp(true)));
                        _excelApp.Quit();
                    }
                    else _excelApp.Visible = true;
                }
                finally
                {
                    Thread.CurrentThread.CurrentCulture = oldCi;
                    if (_worksheet != null)
                        Marshal.FinalReleaseComObject(_worksheet);
                    if (_excelApp != null)
                        Marshal.FinalReleaseComObject(_excelApp);
                    RaiseFinishedPrinting();
                }
                return true;
            }
            RaiseErrorLoadingTemplate(templateName);
            return false;
        }
    }
}
