﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using Microsoft.Office.Interop.Word;
using ApplicationWord = Microsoft.Office.Interop.Word.Application;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.Common.Extensions;

namespace PFR_INVEST.BusinessLogic
{
    public abstract class PrintToDoc : ViewModelBase
    {
        protected Dictionary<string, string> Fields { get; }
        protected string TemplateName { get; set; }
        protected string Name { get; set; }
        protected string FilePathSave { get; set; }

        public bool IsVisibleAfterGeneration { get; set; } = true;

        protected PrintToDoc()
        {
            Name = "Создание документа";
            Fields = new Dictionary<string, string>();
        }

		public bool Print(string folder, string filename = null)
		{
		    FilePathSave = !string.IsNullOrEmpty(folder) ? Path.Combine(folder, filename ?? TemplateName) : (filename ?? TemplateName);
			return Print();
		}

        public bool Print()
        {
            var oldCi = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            try
            {
                return PrintInInvariantCulture();
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = oldCi;
            }
        }

        private bool PrintInInvariantCulture()
        {
            if (string.IsNullOrEmpty(TemplateName))
                throw new Exception("Не указан темплейт");

            object filePath = TemplatesManager.ExtractTemplate(TemplateName);

            if (File.Exists(filePath.ToString()))
            {
                var app = new ApplicationWord { Visible = false };
                object missing = Missing.Value;
                var doc = app.Documents.Add(ref filePath, ref missing, ref missing, ref missing);
                //if (FilePathSave != null && string.IsNullOrEmpty(Path.GetFileName(FilePathSave)))
                //    FilePathSave = Path.Combine(FilePathSave, Path.GetFileName((string)filePath) ?? "");
                try
                {
                    FillData();

                    GenerateDocument(app, doc);
                    try
                    {
                        doc.SaveAs((string.IsNullOrEmpty(FilePathSave) ? (string)filePath : FilePathSave).AttachUniqueTimeStamp(true));
                        app.Visible = IsVisibleAfterGeneration;
                        JournalLogger.LogEvent("Экспорт данных", DataObjects.Journal.JournalEventType.EXPORT_DATA, null,
                            "Экспорт документов", Name);
                        if (!IsVisibleAfterGeneration)
                            app.Quit(false, missing, missing);
                    }
                    catch (COMException ex)
                    {
                        DialogHelper.ShowAlert(ex.Message);
                        return false;
                    }
                    catch (Exception)
                    {
                        DialogHelper.ShowAlert("Во время экспорта данных произошла ошибка");
                        return false;
                    }
                }
                finally
                {
                    Marshal.FinalReleaseComObject(doc);
                    Marshal.FinalReleaseComObject(app);
                }
                return true;
            }
            RaiseErrorLoadingTemplate(TemplateName);
            return false;
        }

        protected abstract void FillData();

        protected virtual void GenerateDocument(ApplicationWord app, Document doc)
        {
            foreach (var tokenName in Fields.Keys)
            {
                OfficeTools.FindAndReplace(app,
                    $@"{{{tokenName}}}",
                                        Fields[tokenName]);
            }
        }


        private static readonly string[] MonthesGenitive = { "января", "февраля", "марта", "апреля", "мая", "июня",
                                                              "июля", "августа", "сентября", "октября", "ноября", "декабря"};

        private static readonly string[] MonthesPrepositional = { "январе", "феврале", "марте", "апреле", "мае", "июне",
                                                              "июле", "августе", "сентябре", "октябре", "ноябре", "декабре"};


        public static string GetReportDateWordsPrepositional(int year, int? month)
        {
            if (month == null || month < 1)
            {
                var d = new DateTime(year, 12, 1);
                return $"{d.Year} году";
            }

            {
                var d = new DateTime(year, month.Value, 1);
                return $"{MonthesPrepositional[d.Month - 1]} {d.Year} года";
            }
        }

        public static string GetReportDateWords(int year, int? month)
        {
            if (month == null)
            {
                var d = new DateTime(year, 12, 1).AddMonths(1);
                return $"1 {MonthesGenitive[d.Month - 1]} {d.Year} года";
            }

            {
                var d = new DateTime(year, month.Value, 1).AddMonths(1);
                return $"1 {MonthesGenitive[d.Month - 1]} {d.Year} года";
            }
        }
    }
}
