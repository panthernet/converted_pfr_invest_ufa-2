﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Threading;
using Microsoft.Office.Interop.Excel;
using Application = Microsoft.Office.Interop.Excel.Application;
using PFR_INVEST.BusinessLogic.Journal;

namespace PFR_INVEST.BusinessLogic
{
    public abstract class PrintToExcel : ViewModelBase
    {
        protected Dictionary<string, string> Fields { get; private set; }
        protected string TemplateName { get; set; }
        protected string Name { get; set; }
        private Application _app;
        protected Workbook Workbook;

        protected PrintToExcel()
        {
            Name = "Создание документа";
            Fields = new Dictionary<string, string>();
        }

        public void Save(string filePath)
        {
            try
            {
                Print(false);
                Workbook.SaveAs(filePath);
                //_app.Save(filePath);
            }
            catch (Exception e)
            {
                Logger.WriteException(e, "Ошибка при экспорте в Excel документ.");
            }
            finally
            {
                for (var i = 0; i < _app.Sheets.Count; i++)
                {
                    var sheet = (Worksheet)_app.Sheets[i + 1];
                    ReleaseComObject(sheet);
                }

                _app.Workbooks.Close();
                for (var i = 0; i < _app.Workbooks.Count; i++)
                {
                    var workbook = _app.Workbooks[i + 1];
                    ReleaseComObject(workbook);
                }

                ReleaseComObject(Workbook);
                Workbook = null;
                _app.Quit();
                ReleaseComObject(_app);
                _app = null;
            }
        }

        private static void ReleaseComObject(object comObject)
        {
            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(comObject);
        }

        public void Print(bool showFile = true)
        {
            var oldCi = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            try
            {
                PrintInInvariantCulture(showFile);
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = oldCi;
            }
        }

        private bool PrintInInvariantCulture(bool showFile)
        {
            if (string.IsNullOrEmpty(TemplateName))
                throw new Exception("Не указан темплейт");

            object filePath = TemplatesManager.ExtractTemplate(TemplateName);

            if (File.Exists(filePath.ToString()))
            {
                var oldCi = Thread.CurrentThread.CurrentCulture;
                try
                {
                    RaiseStartedPrinting();
                    _app = new Application
                    {
                        DisplayAlerts = false,
                        Visible = false
                    };
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                    Workbook = _app.Workbooks.Open(filePath.ToString());
                    FillData();
                    GenerateDocument(_app);
                    JournalLogger.LogModelEvent(this, DataObjects.Journal.JournalEventType.EXPORT_DATA, String.IsNullOrEmpty(Header) ? Name : "");
                }
                finally
                {
                    Thread.CurrentThread.CurrentCulture = oldCi;
                    RaiseFinishedPrinting();
                }
                
                _app.Visible = showFile;
                return true;
            }

            RaiseErrorLoadingTemplate(TemplateName);
            return false;
        }

        protected abstract void FillData();

        protected virtual void GenerateDocument(Application app)
        {
            foreach (var tokenName in Fields.Keys)
            {
                OfficeTools.FindAndReplace(app,$"{{{tokenName}}}",Fields[tokenName]);
            }
        }
    }
}
