﻿using System;
using System.IO;

namespace PFR_INVEST.BusinessLogic
{
    using System.Globalization;

    public class TemplatesManager
    {
        public static string ExtractTemplate(string fileName)
        {
            string fullPath = string.Empty;
            byte[] blob;
            var t = BLServiceSystem.Client.GetTemplateByName(fileName);
            if (t == null)
                return fullPath;
            else
                blob = BLServiceSystem.Client.GetTemplateByName(fileName).Body;

            if (blob == null)
                return fullPath;
    
           
            try
            {
                
                //long tiks = DateTime.Now.Ticks;
                DateTime dt = DateTime.Now;
                string dateTime = string.Format("_{0}_{1}", dt.ToString("yyyyMMdd"), dt.ToString("HH-mm-ss",CultureInfo.CreateSpecificCulture("ru-RU")));
                //var ff = Path.GetTempFileName();
                fileName = string.Format(
                    "{0}{1}{2}", Path.GetFileNameWithoutExtension(fileName), dateTime, Path.GetExtension(fileName));
                fullPath = Path.Combine(Path.GetTempPath(), fileName);
                var fs = new FileStream(fullPath, FileMode.Create, FileAccess.Write);
                fs.Write(blob, 0, blob.Length);
                fs.Close();
            }
            catch
            {
                return string.Empty;
            }
            finally
            {
               
            }

            return fullPath;
        }
    }
}
