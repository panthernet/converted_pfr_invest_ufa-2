﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PFR_INVEST.BusinessLogic.Misc
{
    /// <summary>
    /// Дублирует значения PFR_INVEST.Ribbon.RibbonStates для слоя BL
    /// </summary>
    public enum RibbonStateBL
    {
        ListsState,
        NPFState,
        SIState,
        VRState,
        CBState,
        RegionsState,
        BackOfficeState,
        SettingsState
    }
}
