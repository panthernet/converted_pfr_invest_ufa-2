﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.Misc
{
	public class OpenSettingsProvider : NotifyPropertyChangedBase, IDataErrorInfo
	{
		public string FormName { get; private set; }
		public string FormComment { get; private set; }
		/// <summary>
		/// настройки для фильтра данных формы
		/// </summary>
		//[Obsolete("Use .Filter instead")]
		private SettingOpenForm SettingForm { get; set; }

		public struct FilterInfo
		{
			public DateTime? PeriodStart { get; set; }
			public DateTime? PeriodEnd { get; set; }
		}

		public FilterInfo Filter => new FilterInfo()
		{
		    PeriodStart = this.SettingForm.PeriodStart.HasValue ? (DateTime?)this.SettingForm.PeriodStart.Value.Date : null,
		    PeriodEnd = this.SettingForm.PeriodEnd.HasValue ? (DateTime?)this.SettingForm.PeriodEnd.Value.Date : null
		};

	    public ICommand UpdateDataCommand { get; private set; }

		private Action<OpenSettingsProvider> Action { get; set; }

		public OpenSettingsProvider(string formName, string formComment, Action<OpenSettingsProvider> action)
		{
			if (action == null)
				throw new ArgumentNullException("action");

			this.FormName = formName;
			this.FormComment = formComment;
			this.Action = action;
			this.IsPeriodVisible = true;
			this.UpdateDataCommand = new DelegateCommand((o) => CanExecuteInternal(), (o) => ExecuteInternal());


			GetSettingOpenForm();
		}

		private bool CanExecuteInternal()
		{
			return string.IsNullOrEmpty(this["PeriodEnd"]) &&
				   string.IsNullOrEmpty(this["PeriodStart"]);

		}

		private void ExecuteInternal()
		{
			SettingForm.PeriodStart = PeriodStart;
			SettingForm.PeriodEnd = PeriodEnd;
			SaveSettingOpenForm();
			if (IsAllPeriodChecked)
			{
				if (
					!ViewModelBase.DialogHelper.ShowConfirmation(
						string.Join(
							"\n",
							"Загрузка данных за \"Весь период\" может занять продолжительное время!",
							"Начать загрузку?")))
				{
					return;
				}
			}

			this.Action(this);
		}

		private DateTime? periodStart;
		public DateTime? PeriodStart
		{
			get
			{
				return periodStart;
			}
			set
			{
				periodStart = value;
				SettingForm.PeriodStart = value;
				this.OnPropertyChanged("PeriodStart");
				this.OnPropertyChanged("PeriodEnd");
			}
		}

		private DateTime? periodEnd;
		public DateTime? PeriodEnd
		{
			get
			{
				return periodEnd;
			}
			set
			{
				periodEnd = value;
				SettingForm.PeriodEnd = value;
				this.OnPropertyChanged("PeriodEnd");
				this.OnPropertyChanged("PeriodStart");
			}

		}

		private bool isAllPeriodChecked;
		public bool IsAllPeriodChecked
		{
			get
			{
				return isAllPeriodChecked;
			}
			set
			{

				isAllPeriodChecked = value;
				this.OnPropertyChanged("IsAllPeriodChecked");
				this.OnPropertyChanged("PeriodStart");
				this.OnPropertyChanged("PeriodEnd");

				//UpdateDataCommand.Execute(null);
			}
		}

		private bool isPeriodVisible;
		public bool IsPeriodVisible
		{
			get
			{
				return isPeriodVisible;
			}
			set
			{
				isPeriodVisible = value;
				this.OnPropertyChanged("IsPeriodVisible");
			}
		}

		private bool isPeriodChecked;
		public bool IsPeriodChecked
		{
			get
			{
				return isPeriodChecked;
			}
			set
			{
				isPeriodChecked = value;
				this.OnPropertyChanged("IsPeriodChecked");
				this.OnPropertyChanged("PeriodStart");
				this.OnPropertyChanged("PeriodEnd");

			}
		}

		public bool ShowForm
		{
			get
			{
				return SettingForm.ShowForm == 1;
			}
			set
			{
				SettingForm.ShowForm = value ? 1 : 0;
			}
		}


		/// <summary>
		/// получить настройки
		/// </summary>
		/// <param name="formName"></param>
		/// <param name="comment"></param>
		private void GetSettingOpenForm()
		{

			if (!string.IsNullOrEmpty(this.FormName))
			{
				SettingForm =
					DataContainerFacade.GetListByPropertyConditions<SettingOpenForm>(
						new List<ListPropertyCondition>()
                            {
                                new ListPropertyCondition()
                                {
                                    Name = "FormName",
                                    Value = this.FormName,
                                    Operation = "eq"
                                },
                                new ListPropertyCondition()
                                {
                                    Name = "UserName",
                                    Value =  AP.Provider.UserName,
                                   // Value =  AP.Provider.Domain +@"\"+ AP.Provider.UserName,
                                    Operation = "eq"
                                }
                            })
						.FirstOrDefault();
			}

			if (SettingForm == null)
			{
				SettingForm = this.GetSettingOpenFormDefault();
				SaveSettingOpenForm();
			}
			else
			{
				if (!SettingForm.PeriodStart.HasValue || !SettingForm.PeriodEnd.HasValue)
				{
					SettingForm.PeriodStart = DefaultPeriodStart;
					SettingForm.PeriodEnd = DefaultPeriodEnd;
				}
			}

			SettingForm.Comment = this.FormComment;

			if (SettingForm.PeriodEnd.HasValue && SettingForm.PeriodStart.HasValue)
			{
				IsPeriodChecked = true;
				periodStart = SettingForm.PeriodStart;
				periodEnd = SettingForm.PeriodEnd;
			}
			else
				IsAllPeriodChecked = true;
		}

		DateTime DefaultPeriodStart => new DateTime(DateTime.Now.Year, 1, 1);

	    DateTime DefaultPeriodEnd => DefaultPeriodStart.AddYears(1);

	    /// <summary>
		/// получить настройки по умолчанию
		/// </summary>
		/// <param name="formName"></param>
		/// <param name="comment"></param>
		public SettingOpenForm GetSettingOpenFormDefault()
		{
			if (string.IsNullOrEmpty(this.FormName))
			{
				return new SettingOpenForm() { };
			}

			return new SettingOpenForm()
			{
				Comment = this.FormComment,
				Date = DateTime.Now,
				PeriodStart = DefaultPeriodStart,
				PeriodEnd = DefaultPeriodEnd,
				FormName = this.FormName,
				UserName = AP.Provider.UserName,
				//UserName = AP.Provider.Domain + @"\" + AP.Provider.UserName
				ShowForm = 1,
			};
		}


		/// <summary>
		/// сохранить настройки
		/// </summary>        
		public void SaveSettingOpenForm()
		{
			if (string.IsNullOrEmpty(SettingForm.UserName) || string.IsNullOrEmpty(SettingForm.FormName))
				return;

			if (IsAllPeriodChecked)
			{
				SettingForm.PeriodStart = null;
				SettingForm.PeriodEnd = null;
			}
			SettingForm.Date = DateTime.Now;
			SettingForm.ID = DataContainerFacade.Save<SettingOpenForm>(SettingForm);
		}

		public string this[string columnName]
		{
			get
			{
				const string errorMessagePeriod = "Неверное значение";

				switch (columnName)
				{
					case "PeriodStart":
					case "PeriodEnd":

						if (this.IsPeriodChecked)
						{
							return this.PeriodEnd.HasValue && this.PeriodStart.HasValue ?
								this.PeriodEnd < this.PeriodStart ? errorMessagePeriod : null : errorMessagePeriod;
						}
						else
						{
							return null;
						}

					default: return null;
				}
			}
		}

		public string Error { get; private set; }

	}
}
