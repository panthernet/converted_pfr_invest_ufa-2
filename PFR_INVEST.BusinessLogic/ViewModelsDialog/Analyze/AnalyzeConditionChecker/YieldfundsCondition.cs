﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects.Analyze;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog.Analyze.AnalyzeConditionChecker
{
    public class YieldfundsN107Condition : AnalyzeConditionViewModelBase
    {
        private List<SubjectSPN> _availableSubjectsSPN = new List<SubjectSPN>();

        protected override void PrepareDataOptions()
        {
            var spns = DataContainerFacade.GetList<SubjectSPN>().ToList();
            var myRoles = CurrentUserRoles ?? AP.Provider.CurrentUserSecurity.GetRoles();
            _availableSubjectsSPN.AddRange(
                spns.Where(s => myRoles.Any(r => ((DOKIP_ROLE_TYPE)s.UserRoleFlags).HasFlag(r))));

            _normalDataCount = _availableSubjectsSPN.Count();
        }

        public override IEnumerable<AnalyzeReportDataBase> LoadDataFromDB()
        {
            var result = new List<AnalyzeReportDataBase>();
            result.AddRange(
                DataContainerFacade.GetClient()
                    .GetYieldfundsDataByYearKvartal(DateTime.Now.Year, null, 107));

          result.AddRange(DataContainerFacade.GetClient()
                .GetYieldfundsDataByYearKvartal(DateTime.Now.Year - 1, 4, 107));
           
            return result;
        }

        public override List<AnalyzeReportDataBase> ApplyFilters(IEnumerable<AnalyzeReportDataBase> data)
        {
            return data.Where(d => _availableSubjectsSPN.Any(s => s.Id == (d as AnalyzeYieldfundsData).SubjectId)).ToList();
            //данные для оповещения о не закрытом отчете 4 кв предыдущего года
        }
    }

    public class YieldfundsN140Condition : AnalyzeConditionViewModelBase
    {
        private List<SubjectSPN> _availableSubjectsSPN = new List<SubjectSPN>();

        protected override void PrepareDataOptions()
        {
            var spns = DataContainerFacade.GetList<SubjectSPN>().ToList();
            var myRoles = CurrentUserRoles ?? AP.Provider.CurrentUserSecurity.GetRoles();

            _availableSubjectsSPN.AddRange(
                spns.Where(s => myRoles.Any(r => ((DOKIP_ROLE_TYPE)s.UserRoleFlags).HasFlag(r))));

            _normalDataCount = _availableSubjectsSPN.Count();
        }

        public override IEnumerable<AnalyzeReportDataBase> LoadDataFromDB()
        {
            var result = new List<AnalyzeReportDataBase>();
            var items = DataContainerFacade.GetClient()
                .GetYieldfundsDataByYearKvartal(DateTime.Now.Year, null, 140);
            result.AddRange(items);

            items = DataContainerFacade.GetClient()
                .GetYieldfundsDataByYearKvartal(DateTime.Now.Year - 1, 4, 140);
            result.AddRange(items);
            //данные для оповещения о не закрытом отчете предыдущего года
            return result;
        }
        public override List<AnalyzeReportDataBase> ApplyFilters(IEnumerable<AnalyzeReportDataBase> data)
        {
            return data.Where(d => _availableSubjectsSPN.Any(s => s.Id == (d as AnalyzeYieldfundsData).SubjectId)).ToList();
            //данные для оповещения о не закрытом отчете 4 кв предыдущего года
        }
    }

}
