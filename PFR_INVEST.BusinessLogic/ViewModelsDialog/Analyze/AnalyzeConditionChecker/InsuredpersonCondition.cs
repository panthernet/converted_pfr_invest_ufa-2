﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects.Analyze;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog.Analyze.AnalyzeConditionChecker
{
    public class InsuredpersonCondition : AnalyzeConditionViewModelBase
    {
        private List<SubjectSPN> _availableSubjectsSPN = new List<SubjectSPN>();


        protected override void PrepareDataOptions()
        {

            //var spns = DataContainerFacade.GetList<SubjectSPN>().ToList();
            //var myRoles = AP.Provider.CurrentUserSecurity.GetRoles();
            //_availableSubjectsSPN.AddRange(
            //    spns.Where(s => myRoles.Any(r => ((DOKIP_ROLE_TYPE)s.UserRoleFlags).HasFlag(r))));

          
            var myRoles = CurrentUserRoles ?? AP.Provider.CurrentUserSecurity.GetRoles();

            List<int> allowedFounds = new List<int>();
            //if (myRoles.Contains(((DOKIP_ROLE_TYPE)4)))
            //{
            //    allowedFounds.AddRange(new[] { 0, 1 });
            //}
            //else if (myRoles.Contains(((DOKIP_ROLE_TYPE)1024)))
            //{
            //    allowedFounds.Add(0);
            //}
            //else if (myRoles.Contains(((DOKIP_ROLE_TYPE)8192)))
            //{
            //    allowedFounds.Add(1);
            //}
            //_availableSubjectsSPN.AddRange(DataContainerFacade.GetList<SubjectSPN>().Where(s => allowedFounds.Contains(s.Fondtype)));

            var ofprConstraints = new[]
            {
                "ГУК расширенный инвестиционный портфель", "ГУК инвестиционный портфель государственных ценных бумаг", "ЧУК"
            };
            var okipConstraints = new[]
            {
                "НПФ"
            };
            if (myRoles.Contains(((DOKIP_ROLE_TYPE)4)))//админ
            {
                _availableSubjectsSPN.AddRange(DataContainerFacade.GetList<SubjectSPN>().Where(s => ofprConstraints.Contains(s.FondTypeName, StringComparer.CurrentCultureIgnoreCase)));
                _availableSubjectsSPN.AddRange(DataContainerFacade.GetList<SubjectSPN>().Where(s => okipConstraints.Contains(s.FondTypeName, StringComparer.CurrentCultureIgnoreCase)));
            }
            else if (myRoles.Contains(((DOKIP_ROLE_TYPE)1024)))//BackOfficeState
            {
                _availableSubjectsSPN.AddRange(DataContainerFacade.GetList<SubjectSPN>().Where(s => ofprConstraints.Contains(s.FondTypeName, StringComparer.CurrentCultureIgnoreCase)));
            }
            else if (myRoles.Contains(((DOKIP_ROLE_TYPE)8192)))//НПФ NPFState
            {
                _availableSubjectsSPN.AddRange(DataContainerFacade.GetList<SubjectSPN>().Where(s => okipConstraints.Contains(s.FondTypeName, StringComparer.CurrentCultureIgnoreCase)));
            }
            
            _normalDataCount = _availableSubjectsSPN.Count();
        }

        public override IEnumerable<AnalyzeReportDataBase> LoadDataFromDB()
        {
            var result = new List<AnalyzeReportDataBase>();
            result.AddRange(DataContainerFacade.GetClient().GetInsuredpersonDataByYearKvartal(DateTime.Now.Year, null));

            result.AddRange(DataContainerFacade.GetClient().GetInsuredpersonDataByYearKvartal(DateTime.Now.Year - 1, 4));//данные для оповещения о не закрытом отчете 4 кв предыдущего года
            return result;
        }
        public override List<AnalyzeReportDataBase> ApplyFilters(IEnumerable<AnalyzeReportDataBase> data)
        {
            return data.Where(d => _availableSubjectsSPN.Any(s => s.Id == (d as AnalyzeInsuredpersonData).SubjectId)).ToList();
            //данные для оповещения о не закрытом отчете 4 кв предыдущего года
        }
    }

}
