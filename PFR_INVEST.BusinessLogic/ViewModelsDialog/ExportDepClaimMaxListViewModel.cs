﻿
using PFR_INVEST.Auth.SharedData.Auth;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    using System;
    using System.Linq;
    using PFR_INVEST.BusinessLogic.ViewModelsPrint;
    using PFR_INVEST.DataObjects.ListItems;

    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class ExportDepClaimMaxListViewModel : ExportDepClaimBaseViewModel
    {
        public override string GetDocumentHeader()
        {
            return "Экспорт сводного реестра заявок";
        }


        private DepClaimMaxListItem.Types _type;
        private long? _id;
        public ExportDepClaimMaxListViewModel(DepClaimMaxListItem.Types type, long? id)
        {
            this._type = type;
            this._id = id;
            this.ID = id ?? 0;
			this.AuctionID = id ?? 0;
        }
        private DateTime _date = DateTime.Now;
        public DateTime Date
        {
            get { return _date; }
            set
            {
                if (_date != value)
                {
                    _date = value;
                    OnPropertyChanged("Date");
                }
            }
        }

        protected override void ExecuteSave()
        {
           var list = BLServiceSystem.Client.GetDepClaimMaxList((int)this._type);
            if (this._id != null)
            {
                list = list.Where(l => l.DepclaimselectparamsId == this._id).ToList();
            }
            var print = new PrintExportDepclaimMax(list, (int)this._type, _date);
            print.Print();            
        }

        public override bool CanExecuteSave()
        {
            return true;
        }

        public override string this[string columnName]
        {
            get
            {
                throw new System.NotImplementedException();
            }
        }
    }
}
