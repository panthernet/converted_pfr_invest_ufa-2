﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using Microsoft.Office.Core;
using PFR_INVEST.DataObjects;
using ex = Microsoft.Office.Interop.Excel;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.Common.Exceptions;

namespace PFR_INVEST.BusinessLogic
{
    public abstract class PrintAPYearPlanDlgViewModel : ViewModelCardDialog
    {
        private const string TEMPLATE_NAME = "План на год.xls";

        private ex.Application _mApp;
        private ex.Worksheet _mSheet;

        #region Selected Type

        public List<Element> TypeList { get; set; }

        private Element _selectedType;
        public Element SelectedType
        {
            get
            {
                return _selectedType;
            }
            set
            {
                _selectedType = value;
                OnPropertyChanged("SelectedType");
                YearsList = GetYearsListUkPlan(_selectedType.ID);
                var year = YearsList.FirstOrDefault(y => int.Parse(y.Name) == DateTime.Today.Year);
                SelectedYear = year ?? YearsList.FirstOrDefault();
                OnPropertyChanged("YearsList");
            }
        }

        #endregion

        #region Selected Year

        private List<Year> _yearsList;
        public List<Year> YearsList
        {
            get { return _yearsList; }
            set { _yearsList = value; OnPropertyChanged("YearsList"); }
        }

        private Year _selectedYear;
        public Year SelectedYear
        {
            get
            {
                return _selectedYear;
            }
            set
            {
                _selectedYear = value;
                OnPropertyChanged("SelectedYear");
            }
        }

        #endregion

        protected PrintAPYearPlanDlgViewModel()
        {
            try
            {
                TypeList = GetOperationTypeListUkPlan();
                if (TypeList == null || !TypeList.Any())
                {
                    DialogHelper.ShowAlert("Типы операции не заведены в базе. Пожалуйста, обратитесь к администратору!");
                    return;
                }

                var type = TypeList.FirstOrDefault(t => t.Key == (long)Element.Types.RegisterOperationType);
                SelectedType = type ?? TypeList.FirstOrDefault();
                
                YearsList = GetYearsListUkPlan(_selectedType.ID);

                if (!HasYearsListByAllTypes())
                {
                    DialogHelper.ShowAlert("Для выбранного типа операции \"Планы\" не созданы!");
                    return;
                }

                //YearsList = GetYearsListUkPlan(_selectedType.ID);
                var year = YearsList.FirstOrDefault(y => int.Parse(y.Name) == DateTime.Today.Year);
                SelectedYear = year ?? YearsList.FirstOrDefault();
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex);
            }
        }

        /// <summary>
        /// Проверяет, есть ли планы по какой-либо операции
        /// </summary>
        /// <returns></returns>
        public bool HasYearsListByAllTypes()
        {
            // Получаем все года
            var allYearsList = new List<Year>();
            TypeList.ForEach(x => allYearsList.AddRange(GetYearsListUkPlan(x.ID)));

            return allYearsList.Any();
        }

        private void Generate()
        {
            var transferLists = GetUKPlansForYear(_selectedYear.ID, _selectedType.ID);//.Where(l => l.GetUKPlans().Count > 0 && l.GetContract().Status != -1).ToList();

            if (transferLists.Count > 0)
            {
                var r = transferLists.First().GetTransferRegister();
                var header1 = _mSheet.Range["Q1"].Cells.Value2.ToString();
                header1 = header1.Replace("{DATE}", r.RegisterDate?.ToString("dd.MM.yy") ?? string.Empty);
                header1 = header1.Replace("{NUMBER}", r.RegisterNumber ?? string.Empty);
                _mSheet.Range["Q1"].Cells.Value2 = header1;
            }

            var header2 = _mSheet.Range["A2"].Cells.Value2.ToString();
            _mSheet.Range["A2"].Cells.Value2 = header2.Replace("{YEAR}", _selectedYear.Name).Replace("{PAYMENT_TYPE}", SelectedType.ID == (long)Element.SpecialDictionaryItems.PaymentZLType ? "выплат правопреемникам умерших застрахованных лиц" : "выплатного резерва"); 

            var rowCounter = 5;
            foreach (var t in transferLists)
            {
                var plans = new List<SIUKPlan>(t.GetUKPlans().Cast<SIUKPlan>().OrderBy(p => p.ID));

                _mSheet.Range["A" + rowCounter].Cells.Value2 = t.GetContract().GetLegalEntity().FormalizedName;
                _mSheet.Range["B" + rowCounter].Cells.Value2 = t.GetContract().ContractNumber;
                var contarctDate = t.GetContract().ContractDate;
                _mSheet.Range["C" + rowCounter].Cells.Value2 = contarctDate?.ToString("dd.MM.yy") ?? string.Empty;

                //итоги по кварталам                
                _mSheet.Range["G" + rowCounter].Formula = string.Format("=SUM(D{0}:F{0})", rowCounter);
                _mSheet.Range["K" + rowCounter].Formula = string.Format("=SUM(H{0}:J{0})", rowCounter);
                _mSheet.Range["O" + rowCounter].Formula = string.Format("=SUM(L{0}:N{0})", rowCounter);
                _mSheet.Range["S" + rowCounter].Formula = string.Format("=SUM(P{0}:R{0})", rowCounter);

                //расставляем планы по месяцам, с пропуском одной колонки для итога по кварталу
                foreach (var plan in plans)
                {
                    var ch = (char)(67 + plan.MonthID + (plan.MonthID - 1) / 3);
                    _mSheet.Range[ch.ToString() + rowCounter].Cells.Value2 = plan.PlanSum;
                }

                _mSheet.Range["T" + rowCounter].Formula =
                    string.Format("=SUM(D{0}:F{0}) + SUM(H{0}:J{0}) + SUM(L{0}:N{0}) + SUM(P{0}:R{0})", rowCounter);

                if (t != transferLists.Last())
                    _mSheet.Range["A" + (rowCounter + 1)].EntireRow.Insert(ex.XlInsertShiftDirection.xlShiftDown);

                rowCounter++;
            }

            _mSheet.Range["D" + rowCounter].Formula = $"=SUM(D5:D{rowCounter - 1})";
            _mSheet.Range["E" + rowCounter].Formula = $"=SUM(E5:E{rowCounter - 1})";
            _mSheet.Range["F" + rowCounter].Formula = $"=SUM(F5:F{rowCounter - 1})";
            _mSheet.Range["G" + rowCounter].Formula = $"=SUM(G5:G{rowCounter - 1})";

            _mSheet.Range["H" + rowCounter].Formula = $"=SUM(H5:H{rowCounter - 1})";
            _mSheet.Range["I" + rowCounter].Formula = $"=SUM(I5:I{rowCounter - 1})";
            _mSheet.Range["J" + rowCounter].Formula = $"=SUM(J5:J{rowCounter - 1})";
            _mSheet.Range["K" + rowCounter].Formula = $"=SUM(K5:K{rowCounter - 1})";

            _mSheet.Range["L" + rowCounter].Formula = $"=SUM(L5:L{rowCounter - 1})";
            _mSheet.Range["M" + rowCounter].Formula = $"=SUM(M5:M{rowCounter - 1})";
            _mSheet.Range["N" + rowCounter].Formula = $"=SUM(N5:N{rowCounter - 1})";
            _mSheet.Range["O" + rowCounter].Formula = $"=SUM(O5:O{rowCounter - 1})";

            _mSheet.Range["P" + rowCounter].Formula = $"=SUM(P5:P{rowCounter - 1})";
            _mSheet.Range["Q" + rowCounter].Formula = $"=SUM(Q5:Q{rowCounter - 1})";
            _mSheet.Range["R" + rowCounter].Formula = $"=SUM(R5:R{rowCounter - 1})";
            _mSheet.Range["S" + rowCounter].Formula = $"=SUM(S5:S{rowCounter - 1})";

            _mSheet.Range["T" + rowCounter].Formula = $"=SUM(T5:T{rowCounter - 1})";

            _mSheet.Range["A1"].Select();
        }

        protected abstract List<SITransfer> GetUKPlansForYear(long yearID, long operationTypeId);
        protected abstract List<Year> GetYearsListUkPlan(long operationTypeId);
        protected abstract List<Element> GetOperationTypeListUkPlan();
        protected override string PrintingInformation => $"Печать плана за {SelectedYear.Name}г. ...";

        public bool IsSelectedYearValid()
        {
            return SelectedYear?.ID > 0;
        }

        public void Print()
        {
            try
            {
                object filePath = TemplatesManager.ExtractTemplate(TEMPLATE_NAME);

                if (File.Exists(filePath.ToString()))
                {
                    RaiseStartedPrinting();
                    _mApp = new ex.Application();

                    var oldCI = Thread.CurrentThread.CurrentCulture;
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                    try
                    {
                        var workBook = _mApp.Workbooks.Open(filePath.ToString());
                        _mApp.AutomationSecurity = MsoAutomationSecurity.msoAutomationSecurityForceDisable;
                        _mApp.DisplayAlerts = false;
                        _mSheet = (ex.Worksheet) workBook.Sheets[1];

                        Generate();
                        JournalLogger.LogModelEvent(this, DataObjects.Journal.JournalEventType.SELECT);
                    }
                    finally
                    {
                        Thread.CurrentThread.CurrentCulture = oldCI;
                    }

                    RaiseFinishedPrinting();
                    _mApp.Visible = true;
                }
                else
                {
                    RaiseErrorLoadingTemplate(TEMPLATE_NAME);
                }
            }
            catch (Exception ex)
            {
                throw new MSOfficeException(ex.Message);
            }
            finally
            {
                if(_mSheet != null)
                    Marshal.FinalReleaseComObject(_mSheet);
                if(_mApp != null)
                    Marshal.FinalReleaseComObject(_mApp);
            }
        }
    }
}
