﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Proxy;
using PFR_INVEST.BusinessLogic.Interfaces;

namespace PFR_INVEST.BusinessLogic
{
    public class ERZLDlgItem
    {
        public long ID { get; set; }
        public String Content { get; set; }
        public String Company { get; set; }
    }

    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    public class ERZLDialogViewModel : ViewModelCard, IVmNoDefaultLogging
    {
        //private List<DB2LegalEntityCard> npfList = null;
        //public List<DB2LegalEntityCard> NPFList
        //{
        //    get { return this.npfList; }
        //    set { this.npfList = value; OnPropertyChanged("NPFList");}
        //}

        //private List<int> countList = null;
        //public List<int> CountList
        //{
        //    get { return this.countList; }
        //    set { this.countList = value;
        //        OnPropertyChanged("CountList"); }
        //}
        public List<ERZLDlgItem> Items { get; set; }
        public ERZLDlgItem SelectedItem { get; set; }

        public ERZLDialogViewModel()
        {
            ID = -1;
            var erzlList = BLServiceSystem.Client.GetERZLList();
            var fzList = BLServiceSystem.Client.GetFZList();
            Items = new List<ERZLDlgItem>();

            foreach (var erzl in erzlList)
            {
                var item = new ERZLDlgItem
                {
                    ID = erzl.ID,
                    Content = erzl.Content
                };
                long fzID = erzl.FZ_ID ?? 0;
                foreach (var fz in fzList.Where(fz => fz.ID == fzID))
                    item.Company = fz.Name;
                Items.Add(item);
            }
        }

        protected override void ExecuteSave()
        {
        }

        public override bool CanExecuteSave()
        {
            return true;
        }

        public override string this[string columnName]
        {
            get { throw new NotImplementedException(); }
        }
    }
}