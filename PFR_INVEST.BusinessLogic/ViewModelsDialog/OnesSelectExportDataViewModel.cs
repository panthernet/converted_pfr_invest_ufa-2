﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{

    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class OnesSelectExportDataViewModel : ViewModelListDialog, IRequestCloseViewModel
    {
        private List<OnesExportDataItem> _list = new List<OnesExportDataItem>();
        public List<OnesExportDataItem> List
        {
            get { return _list; }
            set
            {
                _list = value;
                OnPropertyChanged("List");
            }
        }

        public ICommand OkCommand { get; set; }

        public OnesSelectExportDataViewModel(OnesSettingsDef.IntGroup part)
            :base(part)
        {
            OkCommand = new DelegateCommand(CanExecute, Execute);
        }

        private void Execute(object o)
        {
            RequestClose?.Invoke(null, null);
        }

        private bool CanExecute(object o)
        {
            return List.Any(a => a.Selected);
        }


        protected override void ExecuteRefreshList(object param)
        {
            List = BLServiceSystem.Client.GetExportDataItemsList((OnesSettingsDef.IntGroup)param);
            Debug.WriteLine("List count: "  + List.Count);
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }

        public event EventHandler RequestClose;
    }
}
