﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	[DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
    public class SelectReqTransferDlgViewModel : ViewModelCardDialog
	{
		public long RegisterID { get; private set; }
		public Document.Types Type { get; private set; }
		public IList<ReqTransferListItem> List { get; private set; }

	    public string Title { get; set; } = "Выбор перечисления";

		public SelectReqTransferDlgViewModel(long registerID, Document.Types type)
		{
		    if (type == Document.Types.OPFR)
		        Title += " ОПФР";

			this.RegisterID = registerID;
			this.Type = type;
            this.List = BLServiceSystem.Client.GetReqTransferForCommonPP(this.RegisterID, this.Type);

			//Вынесено для ускорения в сервис
				//.Where(a => a.Sum != a.GetCommonPPList()
				//    .Sum(b => b.PaySum))
				//.ToList();
		}


		private ReqTransferListItem _selectedItem;
		public ReqTransferListItem SelectedItem
		{
			get { return _selectedItem; }
			set {
			    if (_selectedItem == value) return;
			    _selectedItem = value; OnPropertyChanged("SelectedItem");
			}
		}

		public override bool CanExecuteSave()
		{
			return SelectedItem != null;
		}
	}
}
