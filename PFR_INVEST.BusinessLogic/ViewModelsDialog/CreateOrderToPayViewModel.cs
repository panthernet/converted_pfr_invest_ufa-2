﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OUFV_manager)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OUFV_manager)]
    public class CreateOrderToPayViewModel : ViewModelCard, IUpdateRaisingModel
    {
        private F401402UK m_Detail;
        public F401402UK Detail
        {
            get { return m_Detail; }
            set
            {
                if (m_Detail != value)
                {
                    m_Detail = value;
                    OnPropertyChanged("Detail");
                }
            }
        }

        private AddressTypeListItem _selectedAddressType;
        public AddressTypeListItem SelectedAddressType
        {
            get { return _selectedAddressType; }
            set
            {
                _selectedAddressType = value;
                OnPropertyChanged("SelectedAddressType");
            }
        }

        private List<AddressTypeListItem> _addressTypes;
        public List<AddressTypeListItem> AddressTypes
        {
            get
            {
                return _addressTypes;
            }
            set
            {
                _addressTypes = value;
                OnPropertyChanged("AddressTypes");
            }
        }

        public void RefreshF402View()
        {
            if (GetViewModelsList != null)
            {
                List<ViewModelBase> lst = GetViewModelsList(typeof(F401402UKViewModel));
                if (lst != null && lst.Count > 0)
                    lst.Cast<F401402UKViewModel>()
                        .Where(card => card.Detail.ID == Detail.ID)
                        .ToList()
                        .ForEach(card => card.RefreshControlGroup());
            }
        }

        public CreateOrderToPayViewModel(long id)
            : base(typeof(F401402UKListViewModel))
        {
            DataObjectTypeForJournal = typeof (F401402UK);
            InitAddressBox();
            Detail = DataContainerFacade.GetByID<F401402UK, long>(id);
            RefreshConnectedCardsViewModels = RefreshF402View;            
        }

        private void InitAddressBox()
        {
            AddressTypes = new List<AddressTypeListItem>();
            AddressTypes.Add(new AddressTypeListItem() { Value = AddressType.Post, Label = "Почтовый адрес" });
            AddressTypes.Add(new AddressTypeListItem() { Value = AddressType.Fact, Label = "Фактический адрес" });
            AddressTypes.Add(new AddressTypeListItem() { Value = AddressType.Legal, Label = "Юридический адрес" });

            SelectedAddressType = AddressTypes[0];
        }

        protected override void ExecuteSave()
        {
            if (Detail != null)
            {
                PrintF402 printer = new PrintF402(Detail.ID, SelectedAddressType.Value);
                if (!printer.Print())
                    throw new ViewModelSaveUserCancelledException();
                Detail.StatusID = (int)F402Status.Built;
                ID = DataContainerFacade.Save<F401402UK, long>(Detail);
            }
        }

        public override bool CanExecuteSave() { return true; }

        public override string this[string columnName] { get { throw new NotImplementedException(); } }

        public IList<KeyValuePair<Type, long>> GetUpdatedList()
        {
            return new List<KeyValuePair<Type, long>>() { 
				new KeyValuePair<Type, long>(typeof(F401402UKListItem), this.Detail.ID)
			};
        }
    }

    public class AddressTypeListItem
    {
        public AddressType Value { get; set; }
        public string Label { get; set; }

        public override string ToString()
        {
            return Label;
        }
    }
}
