﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;
using PFR_INVEST.BusinessLogic.ViewModelsPrint;

namespace PFR_INVEST.BusinessLogic
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class ExportDepClaimAllocationViewModel : ExportDepClaimBaseViewModel
    {
        private readonly DepClaimSelectParams _activeAuction;
        private readonly PrintExportDepClaimAllocation _print;
        public IList<DepClaimOfferListItem> Offers => _print.Offers;

        public ExportDepClaimAllocationViewModel(long auctionId)
        {
			this.AuctionID = auctionId;
            _activeAuction = DataContainerFacade.GetByID<DepClaimSelectParams>(auctionId);
            _print = new PrintExportDepClaimAllocation(_activeAuction.ID);
            ClaimNumber = _activeAuction.AuctionNum.ToString();
            ClaimDate = DateTime.Today;
        }

        protected override void ExecuteSave()
        {
            BLServiceSystem.Client.UpdateOfferClaimData(_activeAuction.ID, ClaimNumber, ClaimDate);
            _print.Print();
            JournalLogger.LogEvent("Генерация заявки на перечисление средств", DataObjects.Journal.JournalEventType.EXPORT_DATA, null, "Экспорт заявки", "");                          
        }

        private Person _performer;
        public Person Performer
        {
            get { return _performer; }
            set
            {
                if (_performer != value)
                {
                    _performer = value;
                    _print.Performer = value;
                    OnPropertyChanged("Performer");                   
                }
            }
        }

        private Person _authorizedPerson;
        public Person AuthorizedPerson
        {
            get { return _authorizedPerson; }
            set
            {
                if (_authorizedPerson != value)
                {
                    _authorizedPerson = value;
                    _print.AuthorizedPerson = value;
                    OnPropertyChanged("AuthorizedPerson");
                   
                }
            }
        }
		 
      

        public override bool CanExecuteSave()
        {
            return IsValid();
        }

        private DateTime? _claimDate;
        public DateTime? ClaimDate
        {
            get { return _claimDate; }
            set { _claimDate = value;
                _print.ClaimDate = _claimDate;
                OnPropertyChanged("ClaimDate"); }
        }

        private string _claimNumber;
        public string ClaimNumber
        {
            get { return _claimNumber; }
            set { _claimNumber = value; OnPropertyChanged("ClaimNumber"); }
        }

        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "ClaimDate": 
                        if (!ClaimDate.HasValue) return "Введите дату заявки";
                        break;
                    case "ClaimNumber":
                        return string.IsNullOrWhiteSpace(ClaimNumber) ? "Введите номер заявки" : ClaimNumber.ValidateMaxLength(50);
                }
                return null;
            }
        }

        private const string FIELDS = "ClaimNumber|ClaimDate";

        private bool IsValid()
        {
            return FIELDS.Split('|').All(field => this[field] == null);
        }

        private string _filePath;
        public string FilePath
        {
            get { return _filePath; }
            set
            {
                _filePath = value;
                OnPropertyChanged("FilePath");
            }
        }

		public override bool ValidateCanExport()
		{
			if (!base.ValidateCanExport())
			{
				return false;
			}

            if (!Offers.Any())
            {
                DialogHelper.ShowAlert("Аукцион не содержит акцептованных оферт.");
                return false;
            }
            
            return true;
		}
    }
}
