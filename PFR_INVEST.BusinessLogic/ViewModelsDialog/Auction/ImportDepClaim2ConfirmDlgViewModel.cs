﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.BusinessLogic.XMLModels;
using PFR_INVEST.Common.MessageCompressor;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.ListExtensions;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.XmlSchemas;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class ImportDepClaim2ConfirmDlgViewModel : ImportDepClaimBaseViewModel, IVmNoDefaultLogging
    {
        //public long AuctionID { get; private set; }
        public DelegateCommand<object> OpenFileCommand { get; private set; }
        public DelegateCommand ImportCommand { get; private set; }
        public ObservableList<DepClaim2> Items { get; private set; }
        public DepClaimSelectParams Auction { get; private set; }
        public IList<DepClaim2> ExistingsDepClaim2List { get; private set; }

		private RepositoryImpExpFile _repositoryLogRecord;

		private const string OP_NAME = "Импорт выписки из реестра заявок, подлежащих удовлетворению";

        private string _fileName;
        public string FileName
        {
            get { return _fileName; }
            set { if (_fileName != value) { _fileName = value; OnPropertyChanged("FileName"); } }
        }


        public ImportDepClaim2ConfirmDlgViewModel(long auctionID)
            : base(typeof(DepClaim2ListViewModel),
                    typeof(DepClaim2ConfirmListViewModel),
                    typeof(DepClaimSelectParamsListViewModel),
                    typeof(DepClaimSelectParamsViewModel)
            )
        {
            ID = auctionID;
            //this.AuctionID = auctionID;
            OpenFileCommand = new DelegateCommand<object>(OnOpenFile);
            ImportCommand = new DelegateCommand(o => Items.Count > 0, ExecuteImport);
            Items = new ObservableList<DepClaim2>();
            Auction = DataContainerFacade.GetByID<DepClaimSelectParams>(auctionID);
            ExistingsDepClaim2List = Auction.GetDepClaim2List();
        }

        public bool ValidateCanImport()
        {
			if (!base.ValidateCanImport())
			{
				return false;
			}

            if (ExistingsDepClaim2List.Count == 0)
            {
                DialogHelper.ShowAlert("У аукциона отсутствуют заявки. Импорт заявок, подлежащих удовлетворению невозможен.");
                return false;
            }
            // Первый импорт 
            if (ExistingsDepClaim2List.All(c => c.Status == DepClaim2.Statuses.New))
            {
                return true;
            }
            // Повторный импорт
            if (ExistingsDepClaim2List.All(c => c.Status == DepClaim2.Statuses.Confirm || c.Status == DepClaim2.Statuses.NotConfirm))
            {
                return ExistingsDepClaim2List.Count <= 0 || DialogHelper.ConfirmImportDepClaim2Rewrite();
            }
            DialogHelper.ShowAlert("Аукцион находится в статусе, для которого невозможен импорт заявок, подлежащих удовлетворению");
            return false;
        }


        private DepClaim2 GetFirstIncorrectClaim()
        {
           return Items.FirstOrDefault(c => !ExistingsDepClaim2List.Any(ec => ec.RecNum == c.RecNum || (ec.FirmID == c.FirmID
                                                                                                              && ec.FirmName == c.FirmName
                                                                                                              && ec.Rate == c.Rate)));
        }

       /* private bool IsCorrectSettleDate()
        {
            var isCorrect = !Items.All(c => c.SettleDate > c.ReturnDate);
            return isCorrect;
        }*/

        private void ExecuteImport(object o)
        {
            IsAttemptFailed = false;
            if (!ValidateCanImport())
            {
                IsAttemptFailed = true;
                return;
            }

            try
            {
                var repID = DataContainerFacade.Save(_repositoryLogRecord);
                foreach (var i in Items)
                {
                    i.ConfirmRepositoryID = repID;
                }

                var result = BLServiceSystem.Client.AuctionImportDepClaim2Confirm(ID, Items, true);
                if (result.IsSuccess)
                {
                    if (IsVerbose) DialogHelper.ShowAlert("Импорт завершён");
                    if (Items != null && Items.Count > 0)
                    {
                        JournalLogger.LogEvent("Импорт данных", JournalEventType.IMPORT_DATA, null, OP_NAME,
                            string.Join(",", Items.Select(item => item.FirmName).ToArray())
                        );
                    }
                }
                else
                {
                    // сохраняются только успешно импортированные отчеты
                    DataContainerFacade.Delete<RepositoryImpExpFile>(repID);
                    DialogHelper.ShowError(result.ErrorMessage);
                }

                RefreshConnectedViewModels();
            }
            catch
            {
                IsAttemptFailed = true;
                throw;
            }
        }

        private void OnOpenFile(object o)
        {
            FileName = DialogHelper.OpenFile("Файлы заявок PDX06 (*.xml)|MD03562_PDX06_*_*.xml");


            Items.Clear();
            if (FileName == null)
                return;
            if (!DocumentBase.IsValidFileName(FileName))
            {
                DialogHelper.ShowAlert("Неверный формат названия импортируемого файла.");
                return;
            }

            var serializer = new XmlSerializer(typeof(DocumentPDX06));
            var settings = new XmlReaderSettings {ValidationType = ValidationType.Schema};
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema("PDX06.xsd"));

            try
            {
                using (var reader = XmlReader.Create(FileName, settings))
                {
                    var doc = (DocumentPDX06)serializer.Deserialize(reader);
                    if (Auction.SelectDate != doc.Body.Tab.TradeDate)
                    {
                        DialogHelper.ShowAlert("Дата раcсмотрения заявок не соответствует дате аукциона");
                        return;
                    }
                    foreach (var r in doc.Body.Records)
                    {
                        var hasDate = (doc.Info.DateValue != DateTime.MinValue);
                        var item = new DepClaim2()
                        {
                            Amount = r.AMOUNT,
                            Rate = r.RATE,
                            Payment = r.PAYMENT,
                            SettleDate = r.SETTLEDATE,
                            ReturnDate = r.RETURNDATE,
                            SecurityID = r.SECURITYID,
							RecNum = r.ORDER_NUMBER,
                            //Status = DepClaim2.STATUS_NEW,
                            FirmName = r.FIRMNAME,
                            FirmID = r.FIRMID,
                            AuctionDate = Auction.SelectDate,
                            AuctionID = Auction.ID,
                            DocumentDate = hasDate ? (DateTime?)doc.Info.DateValue : null,
                            DocumentTime = hasDate ? (TimeSpan?)doc.Info.TimeValue : null,
                            Part2Sum = r.PART2_SUM,
                            Term = r.TERM
                        };
                        Items.Add(item);
                    }
                }
                //if (!IsCorrectSettleDate())
                //{
                //    DialogHelper.Alert("В импортируемых заявках дата размещения средств в депозит больше даты возврата средств.");
                //    this.Items.Clear();
                //}
                var claim = GetFirstIncorrectClaim();
                if (claim != null)
                {
                    //string.Format("Импортируемые заявки не соответствуют заявкам, существующим в базе.\nДля номера заявки {0} реестра заявок, подлежащих удовлетворению не найден соответствующий номер заявки из реестра заявок",idString));
                    DialogHelper.ShowError(string.Format("Импортируемые заявки не соответствуют заявкам, существующим в базе.\nДля номера заявки {3} реестра заявок, подлежащих удовлетворению не найден соответствующий номер заявки из реестра заявок.\nПоля: Идентификатор участника (FIRMID2 = '{0}'), Процентная ставка (RATE = '{1}'), Дата размещения (SETTLEDATE = '{2:yyyy-MM-dd}').",
                                    claim.FirmID, claim.Rate, claim.SettleDate, claim.RecNum));
                    Items.Clear();
                }
                else
                {
                    var idstr = new StringBuilder();
                    Items.GroupBy(x => x.RecNum)
                        .Where(group => group.Count() > 1)
                        .Select(group => group.Key).Distinct().ForEach(a =>
                        {
                            idstr.Append(a);
                            idstr.Append(",");
                        });
                    var idString = idstr.ToString();
                    if (!string.IsNullOrEmpty(idString))
                    {
                        idString = idString.Remove(idString.Length - 1, 1);
                        DialogHelper.ShowError("Импортируемые заявки содержат повторяющиеся идентификаторы: " + idString);
                        Items.Clear();
                    }

                }



				var dt = DateTime.Now;
				_repositoryLogRecord = new RepositoryImpExpFile()
				{
					AuctionDate = Auction.SelectDate,
					AuctionLocalNum = Auction.LocalNum,
					DDate = dt,
					ImpExp = 1, 
					Repository = GZipCompressor.Compress(ExcelTools.SafeReadFile(FileName)),
					Status = 0,
					TTime = new TimeSpan(dt.TimeOfDay.Hours, dt.TimeOfDay.Minutes, dt.TimeOfDay.Seconds),
					UserName = AP.Provider.UserName,
                    Key = (int)RepositoryImpExpFile.Keys.DepClaimImportZ,
					Operacia = OP_NAME,
					Comment = "Работа с ЦБ и депозитами - Депозиты - Импорт - Выписка из реестра заявок, подлежащих удовлетворению",
				};
			
            }
            catch (InvalidOperationException ex)
            {
                if (ex.InnerException is XmlSchemaValidationException)
                {
                    if (ex.InnerException.InnerException == null)
                    {
                        DialogHelper.ShowAlert(
                            string.Format(
                                "Неверный формат документа: {0}\n\nПодробности - {1}",
                                ex.Message,
                                ex.InnerException.Message));
                    }
                    else
                    {
                        DialogHelper.ShowAlert(
                            string.Format(
                                "Неверный формат документа: {0}\n\nОписание - {1}\n\nПодробности - {2}",
                                ex.Message,
                                ex.InnerException.InnerException.Message,
                                ex.InnerException.Message));
                    }
                }

                else
                {
                    DialogHelper.ShowAlert("Неверный формат документа");
                    Logger.WriteException(ex);
                }

            }
            catch (Exception ex)
            {
                DialogHelper.ShowAlert("Ошибка открытия документа");
                Logger.WriteException(ex);
            }
        }

        protected override void ExecuteSave()
        {
            throw new NotImplementedException();
        }

        public override bool CanExecuteSave()
        {
            return false;
        }

        public override string this[string columnName] => string.Empty;
    }
}
