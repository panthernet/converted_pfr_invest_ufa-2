﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_directory_editor, DOKIP_ROLE_TYPE.OARRS_directory_editor, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_directory_editor, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OUFV_directory_editor)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OARRS_manager, DOKIP_ROLE_TYPE.OKIP_manager, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_manager)]
    public class LegalEntityIdentifierDlgViewModel : ViewModelBase, ILEIdentifierListProvider
    {
        readonly Dictionary<Guid, string> _usedIdentifiers;
        public LegalEntityIdentifier Item { get; private set; }

        public DelegateCommand<object> SaveCard { get; private set; }

        public LegalEntityIdentifierDlgViewModel(LegalEntityIdentifier item, List<LegalEntityIdentifier> existingLEIdentifiers)
        {
            this.ID = item.ID;
            this.Item = item;

            _usedIdentifiers = GetUsedLEIdentifiers(item, existingLEIdentifiers);

            this.Item.PropertyChanged += (sender, e) => { this.IsDataChanged = true; };
            this.Item.LEIdentifierListProvider = this;

            this.SaveCard = new DelegateCommand<object>(o => IsValid(), o => { });
        }

        public static Dictionary<Guid, string> GetUsedLEIdentifiers(LegalEntityIdentifier item, List<LegalEntityIdentifier> existingLEIdentifiers)
        {
            var idList = new Dictionary<Guid,string>();

            if (existingLEIdentifiers != null)
            {
                foreach (var id in existingLEIdentifiers)
                {
                    if (!idList.ContainsKey(id.AdditionInfo))
                        idList.Add(id.AdditionInfo, id.Identifier);
                    else
                        idList[id.AdditionInfo] = id.Identifier;
                }
            }

            if (!idList.ContainsKey(item.AdditionInfo))
                idList.Add(item.AdditionInfo, item.Identifier);
            else
                idList[item.AdditionInfo] = item.Identifier;
            
            return idList;
        }


        private bool IsValid()
        {
            return this.IsDataChanged && this.Item.Validate(); 
        }




        public bool? AskSaveOnClose()
        {
            //IsDataChanged = true;
            if (base.IsDataChanged)
            {
                return DialogHelper.ConfirmLegalEntityIdentifierSaveOnClose();
            }
            return false;
        }


        public bool CanSaveOnClose()
        {
            bool b = IsValid();
            if (b) return true;

            DialogHelper.ShowAlert("Невозможно сохранить: некорректные значения полей");
            return false;
        }

        public string ValidateNewIdentifier(LegalEntityIdentifier x)
        {
            if (_usedIdentifiers == null || _usedIdentifiers.Count == 0)
                return null;

            var id = _usedIdentifiers.Where(i => i.Value == x.Identifier).Select(p => new { Key = p.Key, Value = p.Value }).FirstOrDefault();
            if(id == null)
                return null;

            if (id.Key != x.AdditionInfo)
            {
                return "Идентификатор уже существует.";
            }

            return null;
        }   
        
    }
}
