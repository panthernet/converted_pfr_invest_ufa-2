﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.DataObjects;
using System.Windows;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_manager, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class BankPersonsExpiringModel : ViewModelBase
    {
        public bool ShowNextTime { get; set; }

        public string LOA1W { get; set; }

        public string LOA2W { get; set; }

        public string sign1W { get; set; }

        public string sign2W { get; set; }

        public Visibility LOA1WVisibility => !string.IsNullOrWhiteSpace(LOA1W) ? Visibility.Visible : Visibility.Collapsed;

        public Visibility LOA2WVisibility => !string.IsNullOrWhiteSpace(LOA2W) ? Visibility.Visible : Visibility.Collapsed;

        public Visibility Sign1WVisibility => !string.IsNullOrWhiteSpace(sign1W) ? Visibility.Visible : Visibility.Collapsed;

        public Visibility Sign2WVisibility => !string.IsNullOrWhiteSpace(sign2W) ? Visibility.Visible : Visibility.Collapsed;

        public BankPersonsExpiringModel()
        {
            ShowNextTime = true;
            var loaExpiring = DataContainerFacade.GetListByPropertyConditions<LegalEntityCourier>(
            new List<ListPropertyCondition>(2)
                {
                    new ListPropertyCondition
                        {
                            Operation = "ge",
                            Name = "LetterOfAttorneyExpireDate",
                            Value = DateTime.Now,
                        },
                    new ListPropertyCondition
                        {
                            Operation = "lt",
                            Name = "LetterOfAttorneyExpireDate",
                            Value = DateTime.Now.AddDays(14),
                        },
                }).Where(x => x.Type == LegalEntityCourier.Types.Person && !x.IsDeleted).ToList();

            var signExpiring = DataContainerFacade.GetListByPropertyConditions<LegalEntityCourier>(
            new List<ListPropertyCondition>(2)
                {
                    new ListPropertyCondition
                        {
                            Operation = "ge",
                            Name = "SignatureExpireDate",
                            Value = DateTime.Now,
                        },
                    new ListPropertyCondition
                        {
                            Operation = "lt",
                            Name = "SignatureExpireDate",
                            Value = DateTime.Now.AddDays(14),
                        },
                }).Where(x => x.Type == LegalEntityCourier.Types.Person && !x.IsDeleted).ToList();

            var LOA1w = new List<LegalEntityCourier>();
            var LOA2w = new List<LegalEntityCourier>();
            var sign1w = new List<LegalEntityCourier>();
            var sign2w = new List<LegalEntityCourier>();

            var dt = DateTime.Now;

            foreach (var p in loaExpiring)
            {
                if (p.LetterOfAttorneyExpireDate.HasValue)
                {
                    var dd = (p.LetterOfAttorneyExpireDate.Value - dt).Days;
                    if (dd < 14)
                    {
                        if (dd >= 7)
                        {
                            LOA2w.Add(p);
                        }
                        else if (dd >= 0)
                        {
                            LOA1w.Add(p);
                        }
                    }
                }
            }

            foreach (var p in signExpiring)
            {
                if (p.SignatureExpireDate.HasValue)
                {
                    var dd = (p.SignatureExpireDate.Value - dt).Days;
                    if (dd < 14)
                    {
                        if (dd >= 7)
                        {
                            sign2w.Add(p);
                        }
                        else if (dd >= 0)
                        {
                            sign1w.Add(p);
                        }
                    }
                }
            }

            if (LOA1w.Any())
                LOA1W = LOA1w.Select(p => string.Format("- {0} {1} (\"{3}\", {2})", p.FirstName, p.LastName, p.LetterOfAttorneyExpireDate.Value.ToString("dd/MM/yyyy"), p.LegalEntityName)).Aggregate((r1, r2) => string.Format("{0},\r\n{1}", r1, r2));

            if (LOA2w.Any())
                LOA2W = LOA2w.Select(p => string.Format("- {0} {1} (\"{3}\", {2})", p.FirstName, p.LastName, p.LetterOfAttorneyExpireDate.Value.ToString("dd/MM/yyyy"), p.LegalEntityName)).Aggregate((r1, r2) => string.Format("{0},\r\n{1}", r1, r2));

            if (sign1w.Any())
                sign1W = sign1w.Select(p => string.Format("- {0} {1} (\"{3}\", {2})", p.FirstName, p.LastName, p.SignatureExpireDate.Value.ToString("dd/MM/yyyy"), p.LegalEntityName)).Aggregate((r1, r2) => string.Format("{0},\r\n{1}", r1, r2));

            if (sign2w.Any())
                sign2W = sign2w.Select(p => string.Format("- {0} {1} (\"{3}\", {2})", p.FirstName, p.LastName, p.SignatureExpireDate.Value.ToString("dd/MM/yyyy"), p.LegalEntityName)).Aggregate((r1, r2) => string.Format("{0},\r\n{1}", r1, r2));
        }
    }
}
