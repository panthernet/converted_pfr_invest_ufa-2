﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_directory_editor, DOKIP_ROLE_TYPE.OARRS_directory_editor)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OARRS_manager)]
    public class DepositReturnDlgViewModel : ViewModelCardDialog
    {
        public DateTime? Date { get; set; }
        public IList<DepositReturnListItem> DepositList;

        public DepositReturnDlgViewModel()
        {
            Date = DateTime.Now;
            InitCommands();
        }

        public ICommand OkCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        private void InitCommands()
        {
            OkCommand = new DelegateCommand(o => CanExecuteClickOK(), o => ExecuteClickOK());
            CancelCommand = new DelegateCommand(o => CanExecuteClickCancel(), o => ExecuteClickCancel());
        }

        private bool CanExecuteClickOK()
        {
            return Date != null;
        }

        private void ExecuteClickOK()
        {
            DepositList = BLServiceSystem.Client.GetDepositReturnList(Date.Value);
            OnRequestClose(true);
        }

        private static bool CanExecuteClickCancel()
        {
            return true;
        }

        private void ExecuteClickCancel()
        {
            OnRequestClose(false);
        }
    }
}
