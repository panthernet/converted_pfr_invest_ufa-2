﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataAccess.Client;
using System.IO;
using ex = Microsoft.Office.Interop.Excel;
using System.Threading;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using PFR_INVEST.Common.Exceptions;
using PFR_INVEST.Constants.Identifiers;

namespace PFR_INVEST.BusinessLogic
{
    public class Line030CalculationViewModel : BalanceListViewModel
    {
        private const string TEMPLATE_NAME = "Расчет строки 030.xls";
        private ex.Application _mApp;
        private ex.Worksheet _mSheet;
        //private ex.Range m_range;

        public Line030CalculationViewModel()
            : base(null)
        {
            YearsList = DataContainerFacade.GetList<Year>();
            if (YearsList.Count > 0)
                SelectedYear = YearsList[0];

            QuartersList = new[] { 1, 2, 3, 4 };
            SelectedQuartal = QuartersList[0];
        }

        public new volatile bool IsReadOnly = true;

        public List<Year> YearsList
        {
            get;
            set;
        }

        public Year SelectedYear
        {
            get;
            set;
        }

        public int[] QuartersList
        {
            get;
            set;
        }

        public int SelectedQuartal
        {
            get;
            set;
        }

        public void Generate()
        {
            ex.Workbook workBook = null;
            try
            {
                object filePath = TemplatesManager.ExtractTemplate(TEMPLATE_NAME);
                if (File.Exists(filePath.ToString()))
                {
                    _mApp = new ex.Application
                    {
                        DisplayAlerts = false,
                        Visible = false
                    };

                    var oldCi = Thread.CurrentThread.CurrentCulture;
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

                    try
                    {
                        workBook = _mApp.Workbooks.Open(filePath.ToString());
                        _mSheet = (ex.Worksheet) workBook.Sheets[1];
                        Generate(int.Parse(SelectedYear.Name), SelectedQuartal);
                    }
                    finally
                    {
                        Thread.CurrentThread.CurrentCulture = oldCi;
                    }
                }
            }
            catch (Exception e)
            {
                throw new MSOfficeException(e.Message);
            }
            finally
            {
                if (_mSheet != null)
                    Marshal.FinalReleaseComObject(_mSheet);
                if (workBook != null)
                    Marshal.FinalReleaseComObject(workBook);
                if (_mApp != null)
                    Marshal.FinalReleaseComObject(_mApp);
            }
        }

        private IEnumerable<BalanceListItem> GetList()
        {
            return GridList.Where(item => item.PORTFOLIO_TYPE == (long)Portfolio.Types.SPN).ToList();
        }

        protected override void AfterListRefreshFinished()
        {
            base.AfterListRefreshFinished();
            IsReadOnly = false;
        }

        private void Generate(int year, int quartal)
        {
            //ожидаем окончания загрузки сальдо
            if (!Task.Factory.StartNew(() =>
            {
                while (IsReadOnly)
                {
                    Thread.Sleep(2);
                }
            }).Wait(180000))
                throw new Exception("Не удалось дождаться завершения формирования списка Сальдо за 3 минуты!");

            var month = quartal * 3;
            var resultTable = new Dictionary<string, decimal[]>();

            decimal startUsd = 32, endUsd = 32;

            foreach (var c in BLServiceSystem.Client.GetOnDateCurrentCursesHibForLine30(new DateTime(year, 1, 1), 2))//.Where(c => c.CurrencyID == 2 && c.Value.HasValue))
                startUsd = c.Value.Value;
            foreach (var c in BLServiceSystem.Client.GetOnDateCurrentCursesHibForLine30(new DateTime(year, month, DateTime.DaysInMonth(year, quartal * 3)), 2))//.Where(c => c.CurrencyID == 2  && c.Value.HasValue))
                endUsd = c.Value.Value;

            var selectedPeriodBalanceList = GetList();


            /*var q1 = selectedPeriodBalanceList.Count(a => a.DATE.Year < year || (a.DATE.Year == year && a.DATE.Month <= 3));
            var q2 = selectedPeriodBalanceList.Count(a => a.DATE.Year < year || (a.DATE.Year == year && a.DATE.Month <= 6));
            var q3 = selectedPeriodBalanceList.Count(a => a.DATE.Year < year || (a.DATE.Year == year && a.DATE.Month <= 9));
            var q4 = selectedPeriodBalanceList.Count(a => a.DATE.Year < year || (a.DATE.Year == year && a.DATE.Month <= 12));*/

            foreach (var item in selectedPeriodBalanceList)
            {
                var pYear = item.PORTFOLIO_YEAR;

                if (!resultTable.ContainsKey(pYear))
                    resultTable.Add(pYear, new decimal[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 });

                var data = resultTable[pYear];

                if (item.DATE.Year < year || (item.DATE.Year == year && item.DATE.Month <= month))
                    //if ((item.DATE.Year == year && item.DATE.Month <= month))
                {
                    switch (item.Operation.Trim())
                    {
                        case DueDocKindIdentifier.Due:
                        case DueDocKindIdentifier.DueAccurate:
                        case DueDocKindIdentifier.Treasurity:
                            data[1] += item.Plus + item.Minus;
                            data[13] += item.Plus + item.Minus;
                            data[17] += item.Plus + item.Minus;
                            break;
                        case DueDocKindIdentifier.Prepayment:
                        case DueDocKindIdentifier.DueUndistributed:
                        case DueDocKindIdentifier.DueExcess:
                            data[2] += item.Plus + item.Minus;
                            data[13] += item.Plus + item.Minus;
                            data[17] += item.Plus + item.Minus;
                            break;
                        case IncomeSecIdentifier.s_CouponYield:
                            data[3] += item.Plus + item.Minus;
                            data[13] += item.Plus + item.Minus;
                            data[17] += item.Plus + item.Minus;
                            break;
                        case DueDocKindIdentifier.Penalty:
                        case DueDocKindIdentifier.PenaltyTreasurity:
                            data[4] += item.Plus + item.Minus;
                            data[13] += item.Plus + item.Minus;
                            data[17] += item.Plus + item.Minus;
                            break;
                        case AccDocKindIdentifier.EnrollmentPercents:
                            data[5] += item.Plus + item.Minus;
                            data[13] += item.Plus + item.Minus;
                            data[17] += item.Plus + item.Minus;
                            break;
                        case AccDocKindIdentifier.EnrollmentOther:
                            data[7] += item.Plus + item.Minus;
                            data[13] += item.Plus + item.Minus;
                            data[17] += item.Plus + item.Minus;
                            break;
                        case BalanceIdentifier.s_BuyingCB:
                            data[8] += Math.Abs(item.SUMM);
                            data[13] -= Math.Abs(item.SUMM);
                            data[17] -= Math.Abs(item.SUMM);
                            break;
                        case AccDocKindIdentifier.Withdrawal:
                        case AccDocKindIdentifier.Transfer:
                            data[9] += item.Plus + item.Minus;
                            data[13] += item.Plus + item.Minus;
                            data[17] += item.Plus + item.Minus;
                            break;
                        case CostIdentifier.s_DepositServicesCommission:
                        case CostIdentifier.s_TradingCommission:
                            data[10] += Math.Abs(item.SUMM);
                            data[13] -= Math.Abs(item.SUMM);
                            data[17] -= Math.Abs(item.SUMM);
                            break;
                        case BalanceIdentifier.s_TransferCostsILS:
                        case BalanceIdentifier.s_ReturnCostsILS:
                            data[11] += Math.Abs(item.Plus + item.Minus);
                            data[13] -= item.Plus + item.Minus;
                            data[17] -= item.Plus + item.Minus;
                            break;
                        case IncomeSecIdentifier.s_SecurityRedemption:
                            data[12] += item.Plus + item.Minus;
                            data[13] += item.Plus + item.Minus;
                            data[17] += item.Plus + item.Minus;
                            break;
                        case BalanceIdentifier.s_SellingCB:
                            data[14] += item.Plus + item.Minus;
                            data[17] += item.Plus + item.Minus;
                            break;
                        case BalanceIdentifier.s_TransferFromPFRToUK:
                            data[15] += Math.Abs(item.SUMM);
                            data[17] -= Math.Abs(item.SUMM);
                            break;
                        default:
                            if (item.Operation.Trim().Equals(RegisterIdentifier.PFRtoNPF, StringComparison.CurrentCultureIgnoreCase))
                            {
                                data[16] += Math.Abs(item.SUMM);
                                data[17] -= Math.Abs(item.SUMM);
                            }
                            break;
                    }
                    data[18] += item.Plus + item.Minus;
                    if (item.CURRENCY.Contains("Доллары"))
                    {

                        var val = (item.SUMM - item.CURRENCYSUMM * endUsd);

                        // decimal val = -(item.SUMM - item.CURRENCYSUMM * endUsd);
                        data[6] += val;
                        data[13] += val;
                        data[17] += val;
                        data[18] += val;
                    }
                }

                if (item.DATE.Year < year)
                {
                    data[0] += item.Plus + item.Minus;
                    data[18] += item.Plus + item.Minus;
                    if (item.CURRENCY.Contains("Доллары"))
                    {

                        var val = -(item.SUMM - item.CURRENCYSUMM * startUsd);

                        //decimal val = item.SUMM - item.CURRENCYSUMM * starUsd; 
                        data[6] += val;
                        data[13] += val;
                        data[17] += val;
                        data[18] += val;
                    }
                }

            }

            var keys = resultTable.Keys.ToList();
            keys.Sort();

            var x = 4;
            foreach (var y in keys)
            {
                //проверяем, имеет ли колонка какие ни будь данные
                var isEmpty = resultTable[y].All(v => v == 0);
                //не отображаем еге если данных нету
                if (isEmpty) continue;

                _mSheet.Range[_mSheet.Cells[4, x], _mSheet.Cells[4, x]].Value = y;

                _mSheet.Range[_mSheet.Cells[5, x], _mSheet.Cells[5, x]].Value = resultTable[y][0];
                _mSheet.Range[_mSheet.Cells[6, x], _mSheet.Cells[6, x]].Value = resultTable[y][1];
                _mSheet.Range[_mSheet.Cells[7, x], _mSheet.Cells[7, x]].Value = resultTable[y][2];
                _mSheet.Range[_mSheet.Cells[8, x], _mSheet.Cells[8, x]].Value = resultTable[y][3];
                _mSheet.Range[_mSheet.Cells[9, x], _mSheet.Cells[9, x]].Value = resultTable[y][4];
                _mSheet.Range[_mSheet.Cells[10, x], _mSheet.Cells[10, x]].Value = resultTable[y][5];
                _mSheet.Range[_mSheet.Cells[11, x], _mSheet.Cells[11, x]].Value = resultTable[y][6];
                _mSheet.Range[_mSheet.Cells[12, x], _mSheet.Cells[12, x]].Value = resultTable[y][7];
                _mSheet.Range[_mSheet.Cells[13, x], _mSheet.Cells[13, x]].Value = resultTable[y][8];
                _mSheet.Range[_mSheet.Cells[14, x], _mSheet.Cells[14, x]].Value = resultTable[y][9];
                _mSheet.Range[_mSheet.Cells[15, x], _mSheet.Cells[15, x]].Value = resultTable[y][10];
                _mSheet.Range[_mSheet.Cells[16, x], _mSheet.Cells[16, x]].Value = resultTable[y][11];
                _mSheet.Range[_mSheet.Cells[17, x], _mSheet.Cells[17, x]].Value = resultTable[y][12];
                _mSheet.Range[_mSheet.Cells[18, x], _mSheet.Cells[18, x]].Value = resultTable[y][13];
                _mSheet.Range[_mSheet.Cells[19, x], _mSheet.Cells[19, x]].Value = resultTable[y][14];
                _mSheet.Range[_mSheet.Cells[20, x], _mSheet.Cells[20, x]].Value = resultTable[y][15];
                _mSheet.Range[_mSheet.Cells[21, x], _mSheet.Cells[21, x]].Value = resultTable[y][16];
                _mSheet.Range[_mSheet.Cells[22, x], _mSheet.Cells[22, x]].Value = resultTable[y][17];
                _mSheet.Range[_mSheet.Cells[23, x], _mSheet.Cells[23, x]].Value = resultTable[y][18];
                _mSheet.Range[_mSheet.Cells[24, x], _mSheet.Cells[24, x]].Value = resultTable[y][17] - resultTable[y][18];

                x++;
            }

            var lastColumn = GetColumnChar(x - 1);
            _mSheet.Range[_mSheet.Cells[5, x], _mSheet.Cells[5, x]].Value = "=SUM( D5:" + lastColumn + "5) / 1000";
            _mSheet.Range[_mSheet.Cells[6, x], _mSheet.Cells[6, x]].Value = "=SUM( D6:" + lastColumn + "6) / 1000";
            _mSheet.Range[_mSheet.Cells[7, x], _mSheet.Cells[7, x]].Value = "=SUM( D7:" + lastColumn + "7) / 1000";
            _mSheet.Range[_mSheet.Cells[8, x], _mSheet.Cells[8, x]].Value = "=SUM( D8:" + lastColumn + "8) / 1000";
            _mSheet.Range[_mSheet.Cells[9, x], _mSheet.Cells[9, x]].Value = "=SUM( D9:" + lastColumn + "9) / 1000";
            _mSheet.Range[_mSheet.Cells[10, x], _mSheet.Cells[10, x]].Value = "=SUM( D10:" + lastColumn + "10) / 1000";
            _mSheet.Range[_mSheet.Cells[11, x], _mSheet.Cells[11, x]].Value = "=SUM( D11:" + lastColumn + "11) / 1000";
            _mSheet.Range[_mSheet.Cells[12, x], _mSheet.Cells[12, x]].Value = "=SUM( D12:" + lastColumn + "12) / 1000";
            _mSheet.Range[_mSheet.Cells[13, x], _mSheet.Cells[13, x]].Value = "=SUM( D13:" + lastColumn + "13) / 1000";
            _mSheet.Range[_mSheet.Cells[14, x], _mSheet.Cells[14, x]].Value = "=SUM( D14:" + lastColumn + "14) / 1000";
            _mSheet.Range[_mSheet.Cells[15, x], _mSheet.Cells[15, x]].Value = "=SUM( D15:" + lastColumn + "15) / 1000";
            _mSheet.Range[_mSheet.Cells[16, x], _mSheet.Cells[16, x]].Value = "=SUM( D16:" + lastColumn + "16) / 1000";
            _mSheet.Range[_mSheet.Cells[17, x], _mSheet.Cells[17, x]].Value = "=SUM( D17:" + lastColumn + "17) / 1000";
            _mSheet.Range[_mSheet.Cells[18, x], _mSheet.Cells[18, x]].Value = "=SUM( D18:" + lastColumn + "18) / 1000";
            _mSheet.Range[_mSheet.Cells[19, x], _mSheet.Cells[19, x]].Value = "=SUM( D19:" + lastColumn + "19) / 1000";
            _mSheet.Range[_mSheet.Cells[20, x], _mSheet.Cells[20, x]].Value = "=SUM( D20:" + lastColumn + "20) / 1000";
            _mSheet.Range[_mSheet.Cells[21, x], _mSheet.Cells[21, x]].Value = "=SUM( D21:" + lastColumn + "21) / 1000";
            _mSheet.Range[_mSheet.Cells[22, x], _mSheet.Cells[22, x]].Value = "=SUM( D22:" + lastColumn + "22) / 1000";
            _mSheet.Range[_mSheet.Cells[23, x], _mSheet.Cells[23, x]].Value = "=SUM( D23:" + lastColumn + "23) / 1000";
            _mSheet.Range[_mSheet.Cells[24, x], _mSheet.Cells[24, x]].Value = "=SUM( D24:" + lastColumn + "24) / 1000";
            x++;

            _mSheet.Range[_mSheet.Cells[5, x], _mSheet.Cells[5, x]].Value = "=SUM( D5:" + lastColumn + "5) / 1000000";
            _mSheet.Range[_mSheet.Cells[6, x], _mSheet.Cells[6, x]].Value = "=SUM( D6:" + lastColumn + "6) / 1000000";
            _mSheet.Range[_mSheet.Cells[7, x], _mSheet.Cells[7, x]].Value = "=SUM( D7:" + lastColumn + "7) / 1000000";
            _mSheet.Range[_mSheet.Cells[8, x], _mSheet.Cells[8, x]].Value = "=SUM( D8:" + lastColumn + "8) / 1000000";
            _mSheet.Range[_mSheet.Cells[9, x], _mSheet.Cells[9, x]].Value = "=SUM( D9:" + lastColumn + "9) / 1000000";
            _mSheet.Range[_mSheet.Cells[10, x], _mSheet.Cells[10, x]].Value = "=SUM( D10:" + lastColumn + "10) / 1000000";
            _mSheet.Range[_mSheet.Cells[11, x], _mSheet.Cells[11, x]].Value = "=SUM( D11:" + lastColumn + "11) / 1000000";
            _mSheet.Range[_mSheet.Cells[12, x], _mSheet.Cells[12, x]].Value = "=SUM( D12:" + lastColumn + "12) / 1000000";
            _mSheet.Range[_mSheet.Cells[13, x], _mSheet.Cells[13, x]].Value = "=SUM( D13:" + lastColumn + "13) / 1000000";
            _mSheet.Range[_mSheet.Cells[14, x], _mSheet.Cells[14, x]].Value = "=SUM( D14:" + lastColumn + "14) / 1000000";
            _mSheet.Range[_mSheet.Cells[15, x], _mSheet.Cells[15, x]].Value = "=SUM( D15:" + lastColumn + "15) / 1000000";
            _mSheet.Range[_mSheet.Cells[16, x], _mSheet.Cells[16, x]].Value = "=SUM( D16:" + lastColumn + "16) / 1000000";
            _mSheet.Range[_mSheet.Cells[17, x], _mSheet.Cells[17, x]].Value = "=SUM( D17:" + lastColumn + "17) / 1000000";
            _mSheet.Range[_mSheet.Cells[18, x], _mSheet.Cells[18, x]].Value = "=SUM( D18:" + lastColumn + "18) / 1000000";
            _mSheet.Range[_mSheet.Cells[19, x], _mSheet.Cells[19, x]].Value = "=SUM( D19:" + lastColumn + "19) / 1000000";
            _mSheet.Range[_mSheet.Cells[20, x], _mSheet.Cells[20, x]].Value = "=SUM( D20:" + lastColumn + "20) / 1000000";
            _mSheet.Range[_mSheet.Cells[21, x], _mSheet.Cells[21, x]].Value = "=SUM( D21:" + lastColumn + "21) / 1000000";
            _mSheet.Range[_mSheet.Cells[22, x], _mSheet.Cells[22, x]].Value = "=SUM( D22:" + lastColumn + "22) / 1000000";
            _mSheet.Range[_mSheet.Cells[23, x], _mSheet.Cells[23, x]].Value = "=SUM( D23:" + lastColumn + "23) / 1000000";
            _mSheet.Range[_mSheet.Cells[24, x], _mSheet.Cells[24, x]].Value = "=SUM( D24:" + lastColumn + "24) / 1000000";

            _mSheet.Range[_mSheet.Cells[4, 4], _mSheet.Cells[24, x]].BorderAround(ex.XlLineStyle.xlContinuous);
            _mSheet.Range[_mSheet.Cells[4, 4], _mSheet.Cells[24, x]].Borders.LineStyle = ex.XlLineStyle.xlContinuous;
            _mSheet.Range[_mSheet.Cells[4, 4], _mSheet.Cells[24, x]].Borders.Weight = ex.XlBorderWeight.xlThin;

            _mSheet.Range[_mSheet.Cells[5, 4], _mSheet.Cells[43, x]].NumberFormat = "#,##0.00";

            _mSheet.Range[_mSheet.Cells[18, 2], _mSheet.Cells[18, x]].Interior.Color = ex.XlRgbColor.rgbLightGray;
            _mSheet.Range[_mSheet.Cells[22, 2], _mSheet.Cells[22, x]].Interior.Color = ex.XlRgbColor.rgbLightBlue;
            _mSheet.Range[_mSheet.Cells[23, 2], _mSheet.Cells[23, x]].Interior.Color = ex.XlRgbColor.rgbGreen;
            _mSheet.Range[_mSheet.Cells[24, 2], _mSheet.Cells[24, x]].Interior.Color = ex.XlRgbColor.rgbOrange;

            _mSheet.Range[_mSheet.Cells[4, x - 1], _mSheet.Cells[4, x - 1]].Value = "Итог в тыс.";
            _mSheet.Range[_mSheet.Cells[4, x], _mSheet.Cells[4, x]].Value = "Итог в млн.";
            _mSheet.Range[_mSheet.Cells[4, x - 1], _mSheet.Cells[24, x]].Font.Bold = true;

            var endDate = (new DateTime(year, 1, 1)).AddMonths(quartal * 3).AddDays(-1);
            _mSheet.Range[_mSheet.Cells[5, 3], _mSheet.Cells[5, 3]].Value = string.Format("Остатки на счетах на 01.01.{0} (расчетно)", year);
            _mSheet.Range[_mSheet.Cells[22, 3], _mSheet.Cells[22, 3]].Value = string.Format("Остатки на счетах на {0:dd.MM.yyyy} (расчетно)", endDate);
            _mSheet.Range[_mSheet.Cells[23, 3], _mSheet.Cells[23, 3]].Value = string.Format("Остатки на счетах на {0:dd.MM.yyyy}", endDate);

            _mSheet.Range[_mSheet.Cells[1, 3], _mSheet.Cells[1, 7]].Value = string.Format("Расчет строки 030 \nc 01.01.{0} по {1:dd.MM.yyyy}", year, endDate);

            _mApp.DisplayAlerts = true;
            _mApp.Visible = true;
        }

        private string GetColumnChar(int columnNumber)
        {
            if (columnNumber > 26)
            {
                var first = Convert.ToChar(64 + ((columnNumber - 1) / 26));
                var second = Convert.ToChar(64 + (columnNumber % 26 == 0 ? 26 : columnNumber % 26));
                return first + second.ToString();
            }
            // ASCII index 65 represent char. 'A'. So, we use 64 in this calculation as a starting point
            return Convert.ToChar(64 + columnNumber).ToString();
        }
    }
}
