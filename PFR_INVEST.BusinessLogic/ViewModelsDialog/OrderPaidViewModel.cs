﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OUFV_manager)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OUFV_manager)]
    public class OrderPaidViewModel : ViewModelCard, IUpdateRaisingModel
    {
        private F401402UK m_Detail;
        public F401402UK Detail
        {
            get { return m_Detail; }
            set
            {
                if (m_Detail != value)
                {
                    m_Detail = value;
                    OnPropertyChanged("Detail");
                }
            }
        }

        public DateTime? SetDate
        {
            get
            {
                return Detail.ControlSetDate;
            }
            set
            {
                Detail.ControlSetDate = value;
                OnPropertyChanged("SetDate");
            }
        }

        public DateTime? FactDate
        {
            get
            {
                return Detail.ControlFactDate;
            }
            set
            {
                Detail.ControlFactDate = value;
                OnPropertyChanged("FactDate");
            }
        }

        public void RefreshF402View()
        {
            if (GetViewModelsList != null)
            {
                List<ViewModelBase> lst = GetViewModelsList(typeof(F401402UKViewModel));
                if (lst != null && lst.Count > 0)
                    lst.Cast<F401402UKViewModel>()
                        .Where(card => card.Detail.ID == Detail.ID)
                        .ToList()
                        .ForEach(card => card.RefreshControlGroup());
            }
        }

        public OrderPaidViewModel(long id)
            : base(typeof(F401402UKListViewModel))
        {
            DataObjectTypeForJournal = typeof(F401402UK);
            ID = id;
            Detail = DataContainerFacade.GetByID<F401402UK, long>(id);
            Detail.ControlFactDate = DateTime.Today;
            RefreshConnectedCardsViewModels = RefreshF402View;
        }

        protected override void ExecuteSave()
        {
            if (Detail.ControlSetDate.HasValue)
                Detail.ControlSetDate = Detail.ControlSetDate.Value.Date;
            if (Detail.ControlFactDate.HasValue)
                Detail.ControlFactDate = Detail.ControlFactDate.Value.Date;


            if (Detail.ControlSetDate >= Detail.ControlFactDate)
                Detail.StatusID = (int)F402Status.PaidInTime;
            else
                Detail.StatusID = (int)F402Status.PaidTimeExceeded;

            ID = DataContainerFacade.Save<F401402UK, long>(Detail);
        }

        public override bool CanExecuteSave() { return string.IsNullOrEmpty(Validate()); }

        string Validate()
        {
            const string errorMessage = "Неверный формат данных";
            const string validate = "SetDate|FactDate";

            if (Detail == null)
                return errorMessage;

            foreach (string key in validate.Split('|'))
            {
                if (!String.IsNullOrEmpty(this[key]))
                {
                    return errorMessage;
                }
            }

            return null;
        }

        public override string this[string columnName]
        {
            get {
                const string errorMessage = "Необходимо ввести данные";

                switch (columnName.ToUpper())
                {
                    case "SETDATE": if (!SetDate.HasValue) return errorMessage;
                        if (SetDate.HasValue && Detail.ControlSendDate.HasValue && Detail.ControlSendDate > SetDate)
                            return string.Format("Дата отправки ({0}) не может быть больше установленной даты оплаты", Detail.ControlSendDate.Value.ToShortDateString());
                        if (SetDate.HasValue && Detail.ControlGetDate.HasValue && Detail.ControlGetDate > SetDate)
                            return string.Format("Дата вручения требования ({0}) не может быть больше установленной даты оплаты", Detail.ControlGetDate.Value.ToShortDateString());
                        return null;
                    case "FACTDATE": if (!FactDate.HasValue) return errorMessage;
                        if (FactDate.HasValue && Detail.ControlSendDate.HasValue && Detail.ControlSendDate > FactDate)
                            return string.Format("Дата отправки ({0}) не может быть больше фактической даты оплаты", Detail.ControlSendDate.Value.ToShortDateString());
                        if (FactDate.HasValue && Detail.ControlGetDate.HasValue && Detail.ControlGetDate > FactDate)
                            return string.Format("Дата вручения требования ({0}) не может быть больше фактической даты оплаты", Detail.ControlGetDate.Value.ToShortDateString());
                        return null;
                    default: return null;
                }
            }
        }

        public IList<KeyValuePair<Type, long>> GetUpdatedList()
        {
            return new List<KeyValuePair<Type, long>>() { 
				new KeyValuePair<Type, long>(typeof(F401402UKListItem), this.Detail.ID)
			};
        }
    }
}
