﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
	public class ExportDepClaimOfferAcceptedViewModel : ExportDepClaimBaseViewModel
	{
		private readonly DepClaimSelectParams _activeAuction;
		private readonly PrintDepClaimOfferAccepted _print;

		public ExportDepClaimOfferAcceptedViewModel(long auctionId)
		{
			this.AuctionID = auctionId;
			_activeAuction = DataContainerFacade.GetByID<DepClaimSelectParams>(auctionId);
			_print = new PrintDepClaimOfferAccepted(_activeAuction);
			Number = _activeAuction.AuctionNum;
			Date = _activeAuction.SelectDate;
		}

		protected override void ExecuteSave()
		{
			//Сохраняем аукцион, для сохранения ставки отсечения
			DataContainerFacade.Save(_activeAuction);

			_print.Print();
			JournalLogger.LogEvent("Экспорт данных", JournalEventType.EXPORT_DATA, null, "Экспорт оферт", "");
		}

		public List<DepClaimOfferListItem> Offers => _print.Offers;

	    public bool IsMoscowStock => _activeAuction.StockId == (long)Stock.StockID.MoscowStock;

	    private DateTime? _date = DateTime.Now;
		public DateTime? Date
		{
			get { return _date; }
			set
			{
				if (_date != value)
				{
					_date = value;
					_print.Date = value;
					OnPropertyChanged("Date");
				}
			}
		}

		private int _number;
		public int Number
		{
			get { return _number; }
			set
			{
				if (_number != value)
				{
					_number = value;
					_print.Number = _number;
					OnPropertyChanged("Number");
				}
			}
		}

		public decimal? Rate
		{
			get { return _activeAuction.CutoffRate; }
			set
			{
				if (_activeAuction.CutoffRate != value)
				{
					_activeAuction.CutoffRate = value ?? 0;
					OnPropertyChanged("Rate");
				}
			}
		}

		private Person _performer;
		public Person Performer
		{
			get { return _performer; }
			set
			{
				if (_performer != value)
				{
					_performer = value;
					_print.Performer = value;
					OnPropertyChanged("Performer");

				}
			}
		}

		private Person _authorizedPerson;
		public Person AuthorizedPerson
		{
			get { return _authorizedPerson; }
			set
			{
				if (_authorizedPerson != value)
				{
					_authorizedPerson = value;
					_print.AuthorizedPerson = value;
					OnPropertyChanged("AuthorizedPerson");

				}
			}
		}

		private Person _deputy;
		public Person Deputy
		{
			get { return _deputy; }
			set
			{
				if (_deputy != value)
				{
					_deputy = value;
					_print.Deputy = value;
					OnPropertyChanged("Deputy");

				}
			}
		}



		public override bool CanExecuteSave()
		{
			return IsValid();
		}

		public override string this[string columnName]
		{
			get
			{
				switch (columnName)
				{
					case "Rate": return Rate.ValidateRange(0.0m, 100.0m, true);

					case "Number": return Number > 99999999999999999m ? "Недопустимо большое значение" : null;
					case "Date":
						return Date.HasValue ? null : "Заполните дату реестра";
				}
				return null;
			}
		}

		private readonly string _fields = "Rate|Number|Date";
		private bool IsValid()
		{
			return _fields.Split('|').All(field => this[field] == null);
		}

		private string _filePath;
		public string FilePath
		{
			get { return _filePath; }
			set
			{
				_filePath = value;
				OnPropertyChanged("FilePath");
			}
		}

		public override bool ValidateCanExport()
		{
			if (!base.ValidateCanExport())
			{
				return false;
			}

			if (!Offers.Any())
			{
				DialogHelper.ShowAlert("Аукцион не содержит акцептованных оферт.");
				return false;
			}
			return true;
		}
	}
}
