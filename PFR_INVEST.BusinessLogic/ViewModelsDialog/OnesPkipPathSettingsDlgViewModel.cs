﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.DataObjects.Journal;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator)]
    public class OnesPkipPathSettingsDlgViewModel : ViewModelBase, IDataErrorInfo
    {
        private bool? _isServerChecked;
        public bool? IsServerChecked { get { return _isServerChecked; } set { _isServerChecked = value; OnPropertyChanged("IsServerChecked"); } }
        private bool? _isClientChecked;
        public bool? IsClientChecked { get { return _isClientChecked; } set { _isClientChecked = value; OnPropertyChanged("IsClientChecked"); } }

        private string _exportPath;
        public string ExportPath { get { return _exportPath; } set { _exportPath = value; OnPropertyChanged("ExportPath"); } }
        private string _importPath;
        public string ImportPath { get { return _importPath; } set { _importPath = value; OnPropertyChanged("ImportPath"); } }

        public DelegateCommand SaveCommand { get; private set; }
        public DelegateCommand OpenExportCommand { get; private set; }
        public DelegateCommand OpenImportCommand { get; private set; }

        private readonly SettingsPathFile _import;
        private readonly SettingsPathFile _export;

		private readonly int SettingsType;

        public OnesPkipPathSettingsDlgViewModel(int setting)
        {
			SettingsType = setting;
            int importId;
            int exportId;

            switch (setting)
            {
                case 0: //1c
                    importId = (int)SettingsPathFile.IdDefinition.OnesImport;
                    exportId = (int)SettingsPathFile.IdDefinition.OnesExport;
                    break;
                case 1: //Pkip
                    importId = (int)SettingsPathFile.IdDefinition.PkipImport;
                    exportId = (int)SettingsPathFile.IdDefinition.PkipExport;
                    break;
                default:
                    throw new Exception("Неизвестный тип диалога настройки ручного экспорта/импорта!");
            }


            _import = DataContainerFacade.GetByID<SettingsPathFile>(importId);
            _export = DataContainerFacade.GetByID<SettingsPathFile>(exportId);
            if (_import == null || _export == null)
                throw new Exception("Не найдена запись настроек импорта или экспорта для ручного экспорта/импорта!");

            IsClientChecked = _import.ServClient == (int)SettingsPathFile.FileLocation.Client;
            IsServerChecked = !IsClientChecked;
            ExportPath = _export.Path;
            ImportPath = _import.Path;

            SaveCommand = new DelegateCommand(CanExecuteSaveCommand, ExecuteSaveCommand);
            OpenExportCommand = new DelegateCommand(CanExecuteOpenExportCommand, ExecuteOpenExportCommand);
            OpenImportCommand = new DelegateCommand(CanExecuteOpenImportCommand, ExecuteOpenImportCommand);
        }

        private void ExecuteOpenImportCommand(object obj)
        {
            var dlg = new FolderBrowserDialog { Description = @"Выбор пути для импорта", SelectedPath = ImportPath };
            if (dlg.ShowDialog() != DialogResult.OK) return;
            ImportPath = dlg.SelectedPath;
        }

        private bool CanExecuteOpenImportCommand(object arg)
        {
            return IsClientChecked == true;
        }

        private void ExecuteOpenExportCommand(object obj)
        {
            var dlg = new FolderBrowserDialog {Description = @"Выбор пути для экспорта", SelectedPath = ExportPath };
            if(dlg.ShowDialog() != DialogResult.OK) return;
            ExportPath = dlg.SelectedPath;
        }

        private bool CanExecuteOpenExportCommand(object arg)
        {
            return IsClientChecked == true;
        }

        private void ExecuteSaveCommand(object obj)
        {
            _export.Path = _exportPath;
            _import.Path = _importPath;
            _export.ServClient = _import.ServClient = IsClientChecked == true ? (int)SettingsPathFile.FileLocation.Client : (int)SettingsPathFile.FileLocation.Server;
            _export.Date = _import.Date = DateTime.Today;
            DataContainerFacade.Save(_export);
            DataContainerFacade.Save(_import);

			switch (SettingsType)
			{
			 case 0: //1c
					JournalLogger.LogEvent("Настройка путей 1С", JournalEventType.UPDATE, null, typeof(SettingsPathFile).Name, string.Format("Импорт:{0} Экспорт:{1}", _importPath, _exportPath));
					break;

			 case 1: //Pkip
					JournalLogger.LogEvent("Настройка путей КИП", JournalEventType.UPDATE, null, typeof(SettingsPathFile).Name, string.Format("Импорт:{0} Экспорт:{1}", _importPath, _exportPath));
					break;
			}
			
        }

        private bool CanExecuteSaveCommand(object arg)
        {
            return IsClientChecked != null && IsServerChecked != null && string.IsNullOrEmpty(Validate());
        }

        string IDataErrorInfo.Error => this.Error;
        public string Error { get; private set; }

        public string this[string columnName]
        {
            get
            {
                //string error = "Неверное значение";
                //string errToBig = "Длина пути превышает 250 символов!";
                switch (columnName)
                {
                    case "ExportPath":
                        if (string.IsNullOrEmpty(ExportPath == null ? null : ExportPath.Trim()))
                            return "Не задан путь для папки экспорта!";
                        return ExportPath.Length > 250 ? "Длина пути для экспорта превышает 250 символов!" : string.Empty;
                    case "ImportPath":
                         if (string.IsNullOrEmpty(ImportPath == null ? null : ImportPath.Trim()))
                            return "Не задан путь для папки импорта!";
                         return ImportPath.Length > 250 ? "Длина пути для импорта превышает 250 символов!" : string.Empty;
                    default: return string.Empty;
                }
            }
        }

        private const string FIELDS = "ExportPath|ImportPath";

        public string Validate()
        {
            foreach (var res in FIELDS.Split('|').Select(key => this[key]).Where(res => !string.IsNullOrEmpty(res))) {
                return res;
            }

            return string.Empty;
        }
    }

}
