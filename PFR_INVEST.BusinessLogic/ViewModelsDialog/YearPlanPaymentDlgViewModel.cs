﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using PFR_INVEST.DataObjects;
using ex = Microsoft.Office.Interop.Excel;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.Common.Exceptions;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.ListExtensions;
using PFR_INVEST.Core.Tools;

namespace PFR_INVEST.BusinessLogic
{
    public abstract class YearPlanPaymentDlgViewModel : ViewModelBase
    {
        private const string TEMPLATE_NAME = "Письмо о выплатах на год.doc";
        private const string TEMPLATE_TRPLAN_NAME = "Письмо о выплатах на год (план передачи).doc";

        public event EventHandler UkListChanged;

        protected void OnUkListChanged()
        {
            UkListChanged?.Invoke(null, null);
        }

        #region Selected Type

        public List<Element> TypeList { get; set; }
        private Dictionary<long, string> _ukList;

        public Dictionary<long, string> UkList
        {
            get { return _ukList; }
            set { _ukList = value; OnPropertyChanged("UkList"); }
        }

        private long _selectedUk;

        public long SelectedUk
        {
            get { return _selectedUk; }
            set { _selectedUk = value; OnPropertyChanged("SelectedUk"); }
        }

        private Element _selectedType;
        public Element SelectedType
        {
            get
            {
                return _selectedType;
            }
            set
            {
                _selectedType = value;
                OnPropertyChanged("SelectedType");
                YearsList = GetYearsListUkPlan(_selectedType.ID);
                var year = YearsList.FirstOrDefault(y => int.Parse(y.Name) == DateTime.Today.Year);
                SelectedYear = year ?? YearsList.FirstOrDefault();
				//if (YearsList == null || !YearsList.Any())
				//{
				//	DialogHelper.ShowAlert("Для выбранного типа операции \"Планы\" не созданы!");
				//}
                OnPropertyChanged("YearsList");
            }
        }

        #endregion

        #region Selected Year

        private List<Year> _yearsList;
        public List<Year> YearsList
        {
            get { return _yearsList; }
            set { _yearsList = value; OnPropertyChanged("YearsList"); }
        }

        private Year _selectedYear;
        public Year SelectedYear
        {
            get
            {
                return _selectedYear;
            }
            set
            {
                _selectedYear = value;
                OnPropertyChanged("SelectedYear");
                if (_ukList == null) _ukList = new Dictionary<long, string>();
                else _ukList.Clear();
                UkList.Add(0, "Все");
                if (_selectedYear != null)
                    GetUKListForYear(_selectedYear.ID, _selectedType.ID).ForEach(a=> UkList.Add(a.Key, a.Value));
                OnUkListChanged();
                SelectedUk = UkList.First().Key;
            }
        }

        #endregion

        protected YearPlanPaymentDlgViewModel()
        {
            try
            {
                TypeList = GetOperationTypeListUkPlan();
                if (TypeList == null || !TypeList.Any())
                {
                    DialogHelper.ShowAlert("Типы операции не заведены в базе. Пожалуйста, обратитесь к администратору!");
                    return;
                }

                var type = TypeList.FirstOrDefault(t => t.Key == (long)Element.Types.RegisterOperationType);
                SelectedType = type ?? TypeList.FirstOrDefault();

                //YearsList = GetYearsListUkPlan(_selectedType.ID);
                /*if (YearsList == null || !YearsList.Any())
                {
                    DialogHelper.ShowAlert("Для выбранного типа операции \"Планы\" не созданы!");
                    return;
                }*/

                //var year = YearsList.FirstOrDefault(p_y => p_y.Name == DateTime.Today.Year.ToString());
               // SelectedYear = year ?? YearsList.FirstOrDefault();
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex);
            }
        }

        private void Generate(string filepath, string endfolder)
        {
            if (!Directory.Exists(endfolder))
                Directory.CreateDirectory(endfolder);
           /* try
            {
                //foreach (var item in Directory.GetFiles(endfolder))
                //    File.Delete(item);
            }
            catch
            {
                // ignored
            }*/


            var transferLists = GetUKPlansForYear(_selectedYear.ID, _selectedType.ID, _selectedUk).Where(l => l.GetUKPlans().Count > 0).ToList();

            var save = new Dictionary<long, int>();
            try
            {
                foreach (var t in transferLists)
                {
                    if (t.GetContract().Status == -1 || t.GetContract().GetLegalEntity().GetContragent().StatusID == -1) continue;

                    var contractID = t.GetContract().GetLegalEntity().ID;
                    if (!save.ContainsKey(contractID)) 
                        save.Add(contractID, 0);

                    var ukName = t.GetContract().GetLegalEntity().FormalizedName;
                    var ukAdress = t.GetContract().GetLegalEntity().PostAddress;
                    var dogovorNum = t.GetContract().ContractNumber;

                    using (var w = new MyWord())
                    {
                        w.OpenFile(filepath);
                        var plans = new List<SIUKPlan>(t.GetUKPlans().Cast<SIUKPlan>().OrderBy(p => p.ID));

                        w.FindAndReplace("{UKNAME}", ukName);
                        w.FindAndReplace("{UKNAMEFULL}", t.GetContract().GetLegalEntity().FullName);
                        w.FindAndReplace("{UKDIRECTOR}", t.GetContract().GetLegalEntity().LetterWho);
                        w.FindAndReplace("{UKNUM}", dogovorNum);
                        w.FindAndReplace("{YEAR}", SelectedYear.Name);
                        w.FindAndReplace("{UKADRESS}", ukAdress);

                        var months = new Dictionary<long?, decimal?>();
                        for (var i = 1; i <= 12; i++)
                            months.Add(i, 0);
                        foreach (var item in plans)
                            months[item.MonthID] += item.PlanSum;
                        var nf = CultureInfo.CreateSpecificCulture("ru-RU");

                        w.FindAndReplace("{JANUARY}", months[1].Value.ToString("n2",nf));
                        w.FindAndReplace("{FEBRUARY}", months[2].Value.ToString("n2", nf));
                        w.FindAndReplace("{MARCH}", months[3].Value.ToString("n2", nf));
                        w.FindAndReplace("{APRIL}", months[4].Value.ToString("n2", nf));
                        w.FindAndReplace("{MAY}", months[5].Value.ToString("n2", nf));
                        w.FindAndReplace("{JUNE}", months[6].Value.ToString("n2", nf));
                        w.FindAndReplace("{JULY}", months[7].Value.ToString("n2", nf));
                        w.FindAndReplace("{AUGUST}", months[8].Value.ToString("n2", nf));
                        w.FindAndReplace("{SEPTEMBER}", months[9].Value.ToString("n2", nf));
                        w.FindAndReplace("{OCTOBER}", months[10].Value.ToString("n2", nf));
                        w.FindAndReplace("{NOVEMBER}", months[11].Value.ToString("n2", nf));
                        w.FindAndReplace("{DECEMBER}", months[12].Value.ToString("n2", nf));

                        var summ = plans.Sum(a => a.PlanSum);
                        w.FindAndReplace("{RESULTN}", (summ ?? 0).ToString("n2", nf));
                        w.FindAndReplace("{RESULT}", Сумма.Пропись(summ ?? 0, Валюта.Рубли, Заглавные.Первая));
                        var name = Path.Combine(endfolder, string.Format("{0}_{1}", ukName, save[contractID]));
                        var newname = name;
                        while (File.Exists(newname + ".doc"))
                        {
                            newname = Path.Combine(endfolder, string.Format("{0}_{1}", ukName, save[contractID]++));
                        }

                        w.SaveAs(newname + ".doc");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex, "Ошибка при генерации Плана выплат на год");
            };
        }

        protected abstract Dictionary<long, string> GetUKListForYear(long yearID, long operationTypeId);
        protected abstract List<SITransfer> GetUKPlansForYear(long yearID, long operationTypeId, long ukId);
        protected abstract List<Year> GetYearsListUkPlan(long operationTypeId);
        protected abstract List<Element> GetOperationTypeListUkPlan();
        private bool _isOriginal;

        public void Print(string endfolder)
        {
            try
            {
                if (_selectedYear == null || _selectedType == null)
                {
                    DialogHelper.ShowError("Не выбран год или тип операции!");
                    return;
                }
                if (_ukList ==null || _ukList.Count < 2)
                {
                    DialogHelper.ShowError("Не найдено доступных УК!");
                    return;
                }

                _isOriginal = _selectedType.ID == (long) Element.SpecialDictionaryItems.PaymentZLType;
	            // Имя файла необходимого шаблона
				var tmplFileName = _isOriginal ? TEMPLATE_NAME : TEMPLATE_TRPLAN_NAME;
				object filePath = TemplatesManager.ExtractTemplate(tmplFileName);

                if (File.Exists(filePath.ToString()))
                {
                    RaiseStartedPrinting();

                    var oldCI = Thread.CurrentThread.CurrentCulture;
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");
                    try
                    {
                        Generate(filePath.ToString(), endfolder);
                        JournalLogger.LogModelEvent(this, DataObjects.Journal.JournalEventType.EXPORT_DATA);
                    }
                    finally
                    {
                        Thread.CurrentThread.CurrentCulture = oldCI;
                    }

                    RaiseFinishedPrinting();
                }
                else
                {
					RaiseErrorLoadingTemplate(tmplFileName);
                }
            }
            catch (Exception ex)
            {
                throw new MSOfficeException(ex.Message);
            }
        }
    }
}
