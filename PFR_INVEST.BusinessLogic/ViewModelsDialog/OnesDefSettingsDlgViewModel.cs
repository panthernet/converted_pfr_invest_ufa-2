﻿using System.Collections.Generic;
using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.ListExtensions;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator)]
    public class OnesDefSettingsDlgViewModel : ViewModelCard
    {
        public EventHandler OnListUpdate;

        /// <summary>
        /// Список всех оставшихся операций
        /// </summary>
        public ObservableCollection<Element> NpfLeftOperations { get; set; }
        public ObservableCollection<SPNOperation> UkLeftOperations { get; set; }
        public ObservableCollection<Element> DepositLeftOperations { get; set; }

        /// <summary>
        /// Список выбранных операций
        /// </summary>
        public ObservableCollection<Element> NpfSelectedOperations { get; set; }
        public ObservableCollection<SPNOperation> UkSelectedOperations { get; set; }
        public ObservableCollection<Element> DepositSelectedOperations { get; set; }

        /// <summary>
        /// Списки всех определений
        /// </summary>
        public List<OnesSettingsDef> NpfAllDefinitions { get; set; }
        public List<OnesSettingsDef> UkAllDefinitions { get; set; }
        public List<OnesSettingsDef> DepositAllDefinitions { get; set; }

        //выбор из комбо
        private OnesSettingsDef _npfSelectedContentItem;
        public OnesSettingsDef NpfSelectedContentItem
        {
            get { return _npfSelectedContentItem; }
            set { _npfSelectedContentItem = value; OnPropertyChanged("NpfSelectedContentItem"); OnPropertyChanged("NpfComment"); }
        }

        private OnesSettingsDef _ukSelectedContentItem;
        public OnesSettingsDef UkSelectedContentItem
        {
            get { return _ukSelectedContentItem; }
            set { _ukSelectedContentItem = value; OnPropertyChanged("UkSelectedContentItem"); OnPropertyChanged("UkComment"); }
        }

        private OnesSettingsDef _depositSelectedContentItem;
        public OnesSettingsDef DepositSelectedContentItem
        {
            get { return _depositSelectedContentItem; }
            set { _depositSelectedContentItem = value; OnPropertyChanged("DepositSelectedContentItem"); OnPropertyChanged("DepositComment"); }
        }

        //комменты
        public string NpfComment => _npfSelectedContentItem == null ? "" : _npfSelectedContentItem.Comment;

        public string UkComment => _ukSelectedContentItem == null ? "" : _ukSelectedContentItem.Comment;
        public string DepositComment => _depositSelectedContentItem == null ? "" : _depositSelectedContentItem.Comment;

        //списки всех операций НПФ, УК, Депозиты
        public List<Element> NpfAllOperations { get; set; }
        public List<SPNOperation> UkAllOperations { get; set; }
        public List<Element> DepositAllOperations { get; set; }
        private List<OnesOpSettings> _allSettings;
        private readonly List<ChangedItem<Element>> _changedNpf;
        private readonly List<ChangedItem<SPNOperation>> _changedUk;
        private readonly List<ChangedItem<Element>> _changedDeposit;

        public ICommand SaveCommand { get; set; }

        public OnesDefSettingsDlgViewModel()
        {
            UpdateSourceData();

            var allDefinitions = DataContainerFacade.GetList<OnesSettingsDef>();
            NpfAllDefinitions = allDefinitions.Where(a => a.ExportGroup == OnesSettingsDef.Groups.NPF).OrderBy(a=> a.Name).ToList();
            OnPropertyChanged("NpfAllDefinitions");
            UkAllDefinitions = allDefinitions.Where(a => a.ExportGroup == OnesSettingsDef.Groups.UK).OrderBy(a=> a.Name).ToList();
            OnPropertyChanged("UkAllDefinitions");
            DepositAllDefinitions = allDefinitions.Where(a => a.ExportGroup == OnesSettingsDef.Groups.DEPOSIT).OrderBy(a=> a.Name).ToList();
            OnPropertyChanged("DepositAllDefinitions");

            NpfAllOperations = DataContainerFacade.GetListByProperty<Element>("Key", (long)Element.Types.RegisterPFRtoNPF).ToList();
            DepositAllOperations = DataContainerFacade.GetListByProperty<Element>("Key", (long)Element.Types.PortfolioType).ToList();
            UkAllOperations = DataContainerFacade.GetListByPropertyConditions<SPNOperation>(new List<ListPropertyCondition>
            {
                ListPropertyCondition.In("ID", new object[]
                {
                    (long)SIRegister.Operations.SPNRedistribution,
                    (long)SIRegister.Operations.SPNRedistributionDSV,
                    (long)SIRegister.Operations.SPNRedistributionMSK,
                    (long)SIRegister.Operations.SPNReturn,
                    (long)SIRegister.Operations.SPNYearPortfolioDissolution,
                    (long)SIRegister.Operations.DSVQuarterPortfolioDissolution,
                    (long)SIRegister.Operations.MSKHalfYearPortfolioDissolution,
                    (long)SIRegister.Operations.DSVCofinancing,
                    (long)SIRegister.Operations.SPNTransferToControl})
            }).ToList();

            NpfSelectedContentItem = NpfAllDefinitions.FirstOrDefault();
            UkSelectedContentItem = UkAllDefinitions.FirstOrDefault();
            DepositSelectedContentItem = DepositAllDefinitions.FirstOrDefault();

            _changedNpf = new List<ChangedItem<Element>>();
            _changedUk = new List<ChangedItem<SPNOperation>>();
            _changedDeposit = new List<ChangedItem<Element>>();
            SaveCommand = new DelegateCommand(o => CanExecuteSave(), o => ExecuteSave());
        }

        private void UpdateSourceData()
        {
            _allSettings = DataContainerFacade.GetList<OnesOpSettings>();
        }

        public void UpdateDefinitions(string page = null)
        {
            try
            {
                BusyHelper.Start();
                _changedDeposit.Clear();
                _changedNpf.Clear();
                _changedUk.Clear();
                if (page == null)
                {
                    UpdateDefinitionsInternal(OnesSettingsDef.Groups.NPF, _allSettings);
                    UpdateDefinitionsInternal(OnesSettingsDef.Groups.UK, _allSettings);
                    UpdateDefinitionsInternal(OnesSettingsDef.Groups.DEPOSIT, _allSettings);
                }
                else UpdateDefinitionsInternal(page, _allSettings);
            }
            finally
            {
                BusyHelper.Stop();
                IsDataChanged = false;
            }
        }

        private void UpdateDefinitionsInternal(string page, IEnumerable<OnesOpSettings> allSettings )
        {
            switch (page)
            {
                case OnesSettingsDef.Groups.NPF:
                    NpfSelectedOperations = new ObservableCollection<Element>(NpfAllOperations.Where(a => allSettings.Count(b => a.ID == b.OperationContentID && b.SetDefID == _npfSelectedContentItem.ID) != 0));
                    OnPropertyChanged("NpfSelectedOperations");
                    var npfIdList = NpfAllDefinitions.Select(a => a.ID);
                    var npfAllSelected = _allSettings.Where(a => npfIdList.Contains(a.SetDefID)).Select(a => a.OperationContentID).Distinct();
                    NpfLeftOperations = new ObservableCollection<Element>(NpfAllOperations.Where(a => !npfAllSelected.Contains(a.ID)));
                    OnPropertyChanged("NpfLeftOperations");
                    break;
                case OnesSettingsDef.Groups.UK:
                    UkSelectedOperations = new ObservableCollection<SPNOperation>(UkAllOperations.Where(a => allSettings.Count(b => a.ID == b.OperationContentID && b.SetDefID == _ukSelectedContentItem.ID) != 0));
                    OnPropertyChanged("UkSelectedOperations");
                    var ukIdList = UkAllDefinitions.Select(a => a.ID);
                    var ukAllSelected = _allSettings.Where(a => ukIdList.Contains(a.SetDefID)).Select(a => a.OperationContentID).Distinct();
                    UkLeftOperations = new ObservableCollection<SPNOperation>(UkAllOperations.Where(a => !ukAllSelected.Contains(a.ID)));
                    OnPropertyChanged("UkLeftOperations");
                    break;
                case OnesSettingsDef.Groups.DEPOSIT:
                    DepositSelectedOperations = new ObservableCollection<Element>(DepositAllOperations.Where(a => allSettings.Count(b => a.ID == b.PortfolioTypeID && b.SetDefID == _depositSelectedContentItem.ID) != 0));
                    OnPropertyChanged("DepositSelectedOperations");
                    var depoIdList = DepositAllDefinitions.Select(a => a.ID);
                    var depoAllSelected = _allSettings.Where(a => depoIdList.Contains(a.SetDefID)).Select(a => a.PortfolioTypeID).Distinct();
                    DepositLeftOperations = new ObservableCollection<Element>(DepositAllOperations.Where(a => !depoAllSelected.Contains(a.ID)));
                    OnPropertyChanged("DepositLeftOperations");
                    break;
            }
        }

        protected override void ExecuteSave()
        {
            var saveList = new List<OnesOpSettings>();
            var remList = new List<long>();
            _changedNpf.Where(a => a.IsAdded).Select(a => a.Item).ForEach(a => saveList.Add(new OnesOpSettings() { SetDefID = NpfSelectedContentItem.ID, OperationContentID = a.ID }));
            _changedUk.Where(a => a.IsAdded).Select(a => a.Item).ForEach(a => saveList.Add(new OnesOpSettings() { SetDefID = UkSelectedContentItem.ID, OperationContentID = a.ID }));
            _changedDeposit.Where(a => a.IsAdded).Select(a => a.Item).ForEach(a => saveList.Add(new OnesOpSettings() { SetDefID = DepositSelectedContentItem.ID, PortfolioTypeID = a.ID }));
            _changedNpf.Where(a => !a.IsAdded).Select(a => _allSettings.First(b => b.SetDefID == NpfSelectedContentItem.ID && b.OperationContentID == a.Item.ID).ID).ForEach(a => remList.Add(a));
            _changedUk.Where(a => !a.IsAdded).Select(a => _allSettings.First(b => b.SetDefID == UkSelectedContentItem.ID && b.OperationContentID == a.Item.ID).ID).ForEach(a => remList.Add(a));
            _changedDeposit.Where(a => !a.IsAdded).Select(a => _allSettings.First(b => b.SetDefID == DepositSelectedContentItem.ID && b.PortfolioTypeID == a.Item.ID).ID).ForEach(a => remList.Add(a));

            var counter = 0;
            BLServiceSystem.Client.SaveOnesOpSettings(saveList, remList).ForEach(
                a=> saveList[counter++].ID = a);
            UpdateSourceData();
            _changedNpf.Clear();
            _changedUk.Clear();
            _changedDeposit.Clear();
            IsDataChanged = false;
        }

        public override bool CanExecuteSave()
        {
            return IsDataChanged && (_changedNpf.Any() || _changedUk.Any() || _changedDeposit.Any());
        }

        public override string this[string columnName] => string.Empty;

        public void AddDefinitions(IList selectedItems, string page)
        {
            IsDataChanged = true;
            var items = selectedItems.Cast<object>().ToList();
            items.ForEach(a =>
            {
                switch (page)
                {
                    case OnesSettingsDef.Groups.NPF:
                        var element = (Element) a;
                        NpfLeftOperations.Remove(element);
                        NpfSelectedOperations.Add(element);
                        var present = _changedNpf.FirstOrDefault(b => b.Item.ID == element.ID);
                        if (present == null)
                            _changedNpf.Add(new ChangedItem<Element>(element, true));
                        else _changedNpf.Remove(present);
                        break;
                    case OnesSettingsDef.Groups.UK:
                        var op = (SPNOperation) a;
                        UkLeftOperations.Remove(op);
                        UkSelectedOperations.Add(op);
                        var presentUk = _changedUk.FirstOrDefault(b => b.Item.ID == op.ID);
                        if (presentUk == null)
                            _changedUk.Add(new ChangedItem<SPNOperation>(op, true));
                        else _changedUk.Remove(presentUk);
                        break;
                    case OnesSettingsDef.Groups.DEPOSIT:
                        var dElement = (Element)a;
                        DepositLeftOperations.Remove(dElement);
                        DepositSelectedOperations.Add(dElement);
                        var dPresent = _changedDeposit.FirstOrDefault(b => b.Item.ID == dElement.ID);
                        if (dPresent == null)
                            _changedDeposit.Add(new ChangedItem<Element>(dElement, true));
                        else _changedDeposit.Remove(dPresent);
                        break;
                }
            });
            if (_changedNpf.Count == 0 && _changedUk.Count == 0 && _changedDeposit.Count == 0)
                IsDataChanged = false;
        }

        public void RemoveDefinitions(IList selectedItems, string page)
        {
            IsDataChanged = true;
            var items = selectedItems.Cast<object>().ToList();
            items.ForEach(a =>
            {
                switch (page)
                {
                    case OnesSettingsDef.Groups.NPF:
                        var element = (Element) a;
                        NpfLeftOperations.Add(element);
                        NpfSelectedOperations.Remove(element);
                        var present = _changedNpf.FirstOrDefault(b => b.Item.ID == element.ID);
                        if(present == null)
                            _changedNpf.Add(new ChangedItem<Element>(element, false));
                        else _changedNpf.Remove(present);
                        break;
                    case OnesSettingsDef.Groups.UK:
                        var op = (SPNOperation) a;
                        UkLeftOperations.Add(op);
                        UkSelectedOperations.Remove(op);
                        var presentUk = _changedUk.FirstOrDefault(b => b.Item.ID == op.ID);
                        if (presentUk == null)
                            _changedUk.Add(new ChangedItem<SPNOperation>(op, false));
                        else _changedUk.Remove(presentUk);
                        break;
                    case OnesSettingsDef.Groups.DEPOSIT:
                        var dElement = (Element)a;
                        DepositLeftOperations.Add(dElement);
                        DepositSelectedOperations.Remove(dElement);
                        var dPresent = _changedDeposit.FirstOrDefault(b => b.Item.ID == dElement.ID);
                        if (dPresent == null)
                            _changedDeposit.Add(new ChangedItem<Element>(dElement, false));
                        else _changedDeposit.Remove(dPresent);
                        break;
                }
            });
            if(_changedNpf.Count == 0 && _changedUk.Count == 0 && _changedDeposit.Count == 0)
                IsDataChanged = false;                
        }

        private class ChangedItem<T>
        {
            public readonly bool IsAdded;
            public readonly T Item;

            public ChangedItem(T item, bool isadded)
            {
                Item = item;
                IsAdded = isadded;
            }
        }
    }
}
