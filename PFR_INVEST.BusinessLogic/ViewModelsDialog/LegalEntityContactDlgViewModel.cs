﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OKIP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_directory_editor, DOKIP_ROLE_TYPE.OARRS_directory_editor, DOKIP_ROLE_TYPE.OKIP_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OARRS_manager, DOKIP_ROLE_TYPE.OKIP_manager)]
    public class LegalEntityContactDlgViewModel : ViewModelBase
    {
        public LegalEntityCourier Item { get; private set; }

        public DelegateCommand<object> SaveCard { get; private set; }

        public LegalEntityContactDlgViewModel(LegalEntityCourier item)
        {
            ID = item.ID;
            Item = item;
            Item.PropertyChanged += (sender, e) => { IsDataChanged = true; };


            SaveCard = new DelegateCommand<object>(o => IsValid(), o => { });
        }


        protected bool IsValid()
        {
            return IsDataChanged && Item.Validate();// .IsValid();
                    //&& !string.IsNullOrWhiteSpace(Item.FirstName)
                    //&& !string.IsNullOrWhiteSpace(Item.LastName)
                    //;
        }




        public bool? AskSaveOnClose()
        {
            //IsDataChanged = true;
            if (IsDataChanged)
            {
                return DialogHelper.ConfirmLegalEntityCourierSaveOnClose();
            }
            return false;
        }


        public bool CanSaveOnClose()
        {
            bool b = IsValid();
            if (b) return true;

            DialogHelper.ShowAlert("Невозможно сохранить: некорректные значения полей");
            return false;
        }
    }
}
