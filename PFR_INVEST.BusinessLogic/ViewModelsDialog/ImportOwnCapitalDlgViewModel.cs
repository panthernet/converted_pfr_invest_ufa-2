﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.Core.DBFLoader;
using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class ImportOwnCapitalDlgViewModel : ViewModelCard, IVmNoDefaultLogging
    {
        public DelegateCommand<object> OpenFileCommand { get; private set; }
        public DelegateCommand ImportCommand { get; private set; }
        public ObservableList<OwnCapitalHistory> Items { get; private set; }

        private IList<BankItem> m_BanksList;

        private string _TempFolder;
        public string TempFolder
        {
            get { return _TempFolder; }
            set
            {
                if (_TempFolder != value)
                {
                    _TempFolder = value;
                    OnPropertyChanged("TempFolder");
                    OnPropertyChanged("ImportDate");
                    OnPropertyChanged("ReportDate");
                    OnPropertyChanged("OwnCapitalImport");
                };
            }
        }

        private string _FileName;
        public string FileName
        {
            get { return _FileName; }
            set
            {
                if (_FileName != value)
                {
                    _FileName = value;
                    OnPropertyChanged("FileName");
                    OnPropertyChanged("ImportDate");
                    OnPropertyChanged("ReportDate");                    
                    OnPropertyChanged("OwnCapitalImport");
                };
            }
        }

        public OwnCapitalHistory _OwnCapitalImport;
        public OwnCapitalHistory OwnCapitalImport
        {
            get
            {
                return _OwnCapitalImport;
            }
            set
            {
                _OwnCapitalImport = value;
                OnPropertyChanged("OwnCapitalImport");
            }
        }
        
        public DateTime _ImportDate;
        public DateTime ImportDate 
        {
            get
            {
                return _ImportDate;
            }
            set
            {
                _ImportDate = value;
                OnPropertyChanged("ImportDate");
            }
        }

        public DateTime _ReportDate;
        public DateTime ReportDate
        {
            get
            {
                return _ReportDate;
            }
            set
            {
                _ReportDate = value;
                OnPropertyChanged("ReportDate");
                OnPropertyChanged("ReportDateString");
            }
        }

        public string ReportDateString => _ReportDate == null || _ReportDate == DateTime.MinValue ? string.Empty : ReportDate.ToString("dd.MM.yyyy");


        public ImportOwnCapitalDlgViewModel(): this(string.Empty){}
        
        public ImportOwnCapitalDlgViewModel(string tempFolder)
            : base(typeof(ImportOwnCapitalDlgViewModel))
        {
            this.ID = 0;
            this.OpenFileCommand = new DelegateCommand<object>(OnOpenFile);
            this.ImportCommand = new DelegateCommand(o => this.Items.Count > 0, ExecuteImport);
            this.Items = new ObservableList<OwnCapitalHistory>();
            this.m_BanksList = BLServiceSystem.Client.GetBankItemListForDeposit();

            this.TempFolder = Path.Combine(tempFolder, "Import");
        }

        public bool ValidateCanImport()
        {
            if (string.IsNullOrWhiteSpace(FileName)) return true;

            if (OwnCapitalImport == null)
                OwnCapitalImport = DataContainerFacade.GetListByProperty<OwnCapitalHistory>("ReportDate", ReportDate).FirstOrDefault();

            // Первый импорт 
            if (OwnCapitalImport == null)
                return true;
            // Повторный импорт
            return DialogHelper.ConfirmImportOwnCapitalRewrite(ReportDate);
        }

        private void ExecuteImport(object o)
        {
            if (!this.ValidateCanImport())
                return;

            try
            {
                if (BLServiceSystem.Client.ImportOwnCapital(ReportDate, this.Items, true))
                {
                    JournalLogger.LogModelEvent(this, JournalEventType.IMPORT_DATA); 
                    DialogHelper.ShowAlert("Импорт завершён");                    
                }
                else
                    DialogHelper.ShowAlert("Во время выполнения операции импорта возникла ошибка'." + Environment.NewLine + "Детальная информация об ошибке может быть найдена в логах приложения.");
            }
            catch (Exception ex)
            {
                DialogHelper.ShowAlert(string.Format("Во время выполнения операции импорта возникла ошибка '{0}'." + Environment.NewLine + "Детальная информация об ошибке может быть найдена в логах приложения.", ex.Message));
                Logger.WriteException(ex);
            }

            base.RefreshConnectedViewModels();
        }

        private void OnOpenFile(object o)
        {
            this.FileName = DialogHelper.OpenFile("Файлы собственных средствах (*.dbf)|*_123d.dbf");
            this.Items.Clear();

            if (this.FileName == null)
                return;

            CultureInfo oldCI = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            string newFileName = string.Empty;

            try
            {
                string error = string.Empty;
                DateTime reportDate = DateTime.MinValue;
                string key = string.Empty;

                if (!CheckOwnCapitalDateFileName(FileName, out reportDate, out key, out error))
                    throw new Exception(error);

                newFileName = PrepareFileForImport();

                var loader = new DBFLoader2(Path.GetDirectoryName(newFileName), Path.GetFileName(newFileName));
                var data = loader.LoadOwnCapitalFromDBF(reportDate, key).OfType<OwnCapitalHistory>();//.Where(c => m_BanksList.First(b => b.ID == c.LegalEntityID) != null);

                if (data != null && m_BanksList != null)
                {
                    foreach (var bank in m_BanksList)
                    {
                        OwnCapitalHistory cap = data.FirstOrDefault(c => c.LicenseNumber.Trim() == bank.RegistrationNum);
                        if (cap == null) continue;

                        var item = new OwnCapitalHistory()
                        {
                            LegalEntityID = bank.ID,
                            LegalEntityName = bank.Entity.FormalizedName,
                            Key = cap.Key,
                            LicenseNumber = cap.LicenseNumber.Trim(),
                            Summ = cap.Summ,
                            ImportDate = cap.ImportDate,
                            ReportDate = cap.ReportDate,
                            IsImported = cap.IsImported
                        };

                        this.Items.Add(item);
                    }
                }

                if (this.Items.Count > 0)
                {
                    this.ReportDate = this.Items[0].ReportDate;
                    this.ImportDate = this.Items[0].ImportDate;
                }
                else
                {
                    DialogHelper.ShowAlert(string.Format("В импортируемом файле не найдено информации о собственных средствах зарегистрированных в системе банков." + Environment.NewLine + "Проверьте содержимое файла или номера лицезий банков. "));
                }
            }
            catch (Exception ex)
            {
                DialogHelper.ShowAlert(ex.Message);
                Logger.WriteException(ex);
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = oldCI;
                if (!string.IsNullOrWhiteSpace(newFileName) && File.Exists(newFileName))
                    File.Delete(newFileName);
            }
        }

        private string PrepareFileForImport()
        {
            if (string.IsNullOrWhiteSpace(TempFolder))
                throw new FileNotFoundException("Не задана временная папка для сопирования файла импорта для дальнейшей обработки.");

            DateTime now = DateTime.Now;
            string newFileName = Path.Combine(TempFolder, string.Format("{0}.dbf", now.ToString("yyMMddmm")));
            if(!Directory.Exists(TempFolder))
                Directory.CreateDirectory(TempFolder);
            File.Copy(FileName, newFileName, true);
            return newFileName;
        }

        private bool CheckOwnCapitalDateFileName(string FileName, out DateTime reportDate, out string key, out string error)
        {
            error = string.Empty;
            key = string.Empty;
            reportDate = DateTime.MinValue;

            string fileName = Path.GetFileNameWithoutExtension(FileName);
            if (string.IsNullOrWhiteSpace(fileName) || fileName.Length != 11 || !Path.GetFileNameWithoutExtension(fileName).ToLower().EndsWith("_123d"))
            {
                error = string.Format("Некорректное имя файла '{0}'. " + Environment.NewLine + "Имя файла должно иметь формат вида <mmyyyy_123D.dbf>.", fileName);
                return false;
            }

            key = Path.GetFileNameWithoutExtension(fileName).Substring(0, 6);
            if (!key.ToCharArray().ToList().TrueForAll(l => char.IsDigit(l)))
            {
                error = string.Format("Некорректное имя файла '{0}.' " + Environment.NewLine + "Имя файла должно начинаться с даты в формате <mmyyyy>.", fileName);
                return false;
            };

            if (!DateTime.TryParseExact(key, "MMyyyy", CultureInfo.CurrentCulture, DateTimeStyles.AssumeUniversal, out reportDate))
            {
                error = string.Format("Произошла ошибка во время получения даты из названия файла импорта'{0}.'" + Environment.NewLine + "Имя файла должно начинаться с даты в формате <mmyyyy>.", fileName);
                return false;
            }

            reportDate = reportDate.AddMonths(1);

            return true;
        }

        protected override void ExecuteSave()
        {
            throw new NotImplementedException();
        }

        public override bool CanExecuteSave()
        {
            return false;
        }

        public override string this[string columnName] => string.Empty;
    }
}
