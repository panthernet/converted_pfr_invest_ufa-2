﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Office.Interop.Word;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_directory_editor)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_directory_editor)]
    public class LetterToNPFViewModel : ViewModelBase
    {
        public List<string> BlanksList { get; } = new List<string>(new[]{
            "Бланк исполнительной дирекции",
            "Бланк ПФР"
        });

        private string _mSelectedBlank;
        public string SelectedBlank
        {
            get
            {
                return _mSelectedBlank;
            }
            set
            {
                _mSelectedBlank = value;
                OnPropertyChanged("SelectedBlank");
            }
        }

        private List<LegalEntity> _mNPFList;
        public List<LegalEntity> NPFList
        {
            get
            {
                return _mNPFList;
            }
            set
            {
                _mNPFList = value;
                OnPropertyChanged("NPFList");
            }
        }

        private LegalEntity _mSelectedNPF;
        public LegalEntity SelectedNPF
        {
            get
            {
                return _mSelectedNPF;
            }
            set
            {
                _mSelectedNPF = value;
                OnPropertyChanged("SelectedNPF");
            }
        }

        private AddressTypeListItem _selectedAddressType;
        public AddressTypeListItem SelectedAddressType
        {
            get
            {
                return _selectedAddressType;
            }
            set
            {
                _selectedAddressType = value;
                OnPropertyChanged("SelectedAddressType");
            }
        }

        private List<AddressTypeListItem> _addressTypes;
        public List<AddressTypeListItem> AddressTypes
        {
            get
            {
                return _addressTypes;
            }
            set
            {
                _addressTypes = value;
                OnPropertyChanged("AddressTypes");
            }
        }

        private void InitAddressBox()
        {
            AddressTypes = new List<AddressTypeListItem>
            {
                new AddressTypeListItem {Value = AddressType.Post, Label = "Почтовый адрес"},
                new AddressTypeListItem {Value = AddressType.Fact, Label = "Фактический адрес"},
                new AddressTypeListItem {Value = AddressType.Legal, Label = "Юридический адрес"}
            };
            SelectedAddressType = AddressTypes.First();
        }

        public LetterToNPFViewModel(long id)
        {
			var sf = StatusFilter.NotDeletedFilter;
            NPFList = BLServiceSystem.Client.GetNPFListLEHib(sf).ToList();
            SelectedNPF = id > 0 ? NPFList.First(npf => npf.ID == id) : NPFList.First();
            InitAddressBox();
            SelectedBlank = BlanksList.First();
        }

        private _Application _mWApp;
        private _Application WordApp => _mWApp ?? (_mWApp = new Application());

        private void GenerateDoc()
        {
            var address = string.Empty;
            if (SelectedAddressType.Value == AddressType.Fact)
                address = SelectedNPF.StreetAddress;
            else if (SelectedAddressType.Value == AddressType.Legal)
                address = SelectedNPF.LegalAddress;
            else if (SelectedAddressType.Value == AddressType.Post)
                address = SelectedNPF.PostAddress;

            OfficeTools.FindAndReplace(WordApp, "{ADDRESS}", address);
            OfficeTools.FindAndReplace(WordApp, "{FULLNAME}", string.IsNullOrEmpty(SelectedNPF.FullName) ? "" : SelectedNPF.FullName);

            var chiefs = SelectedNPF.GetChiefs();
            var lastChief = chiefs.LastOrDefault(x => x.RetireDate == null);

            var headFullName = lastChief != null ? $"{lastChief.LastName} {lastChief.FirstName} {lastChief.PatronymicName}".Trim() : string.Empty;

            OfficeTools.FindAndReplace(WordApp, "{HEADNAME}", string.IsNullOrEmpty(headFullName) ? "" : GetProperHeadName(headFullName));
        }

        private static string GetProperHeadName(string pName)
        {
            var names = pName.Trim().Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            return names.Length < 3 ? names[0] : string.Join(" ", names[1], names[2]);
        }

        public void Print()
        {
            string wordTemplateName = $"{SelectedBlank}.doc";
            var extractedFilePath = TemplatesManager.ExtractTemplate(wordTemplateName);
            if (!File.Exists(extractedFilePath))
            {
                RaiseErrorLoadingTemplate(wordTemplateName);
                return;
            }

            WordApp.Visible = false;
            WordApp.Documents.Open(extractedFilePath);
            GenerateDoc();
            WordApp.Visible = true;
        }
    }
}
