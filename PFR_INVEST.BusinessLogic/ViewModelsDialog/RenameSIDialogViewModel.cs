﻿using System;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Journal;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class RenameSIDialogViewModel : ViewModelCard, IVmNoDefaultLogging//, IDataErrorInfo
    {
        public bool IsEditable { get; set; }
        public bool IsReadOnly => !IsEditable;

        private string newFullName;
        public string NewFullName { get { return newFullName; } set { if (this.newFullName != value) { this.newFullName = value; OnPropertyChanged("NewFullName"); } } }

        //public string NewShortName { get; set; }
        private string newShortName;
        public string NewShortName { get { return newShortName; } set { if (this.NewShortName != value) { this.newShortName = value; OnPropertyChanged("NewShortName"); } } }

        //public string NewFormalizedName { get; set; }
        private string newFormalizedName;
        public string NewFormalizedName { get { return newFormalizedName; } set { if (this.NewFormalizedName != value) { this.newFormalizedName = value; OnPropertyChanged("NewFormalizedName"); } } }

        //public string NewInn { get; set; }
        private string newInn;
        public string NewInn { get { return newInn; } set { if (this.newInn != value) { this.newInn = value; OnPropertyChanged("NewInn"); } } }

        //public string Num { get; set; }
        private string num;
        public string Num { get { return num; } set { if (num != value) { num = value; OnPropertyChanged("Num"); } } }

        //public DateTime? Date { get; set; }
        private DateTime? date;
        public DateTime? Date { get { return date; }
            set {
                if (date != value)
                {
                    date = value; OnPropertyChanged("Date");
                }
            } }

        public long LEID;

        public OldSIName osn { get; set; }

        public RenameSIDialogViewModel(long ID)
            : base()
        {
            DataObjectTypeForJournal = typeof(OldSIName);
            osn = DataContainerFacade.GetByID<OldSIName>(ID);
            IsEditable = false;

            NewFullName = osn.OldFullName;
            NewShortName = osn.OldShortName;
            NewFormalizedName = osn.OldFormalizedName;
            NewInn = osn.OldINN;
            date = osn.AgreementDate;
            Num = osn.AgreementNumber;
        }

        public RenameSIDialogViewModel(LegalEntity le)
            : base(typeof(SIListViewModel), typeof(SIContractsListViewModel), typeof(VRContractsListViewModel))
        {
            DataObjectTypeForJournal = typeof(OldSIName);
            IsEditable = true;
            osn = new OldSIName {LegalEntityID = LEID = le.ID};
            NewFullName = osn.OldFullName = le.FullName;
            NewShortName = osn.OldShortName = le.ShortName;
            NewFormalizedName = osn.OldFormalizedName = le.FormalizedName;
            NewInn = osn.OldINN = le.INN;
            var dt = DateTime.Now;
            date = osn.AgreementDate = osn.Date = new DateTime(dt.Year, dt.Month, dt.Day); //DateTime.Now.Date;//new DateTime(dt.Year, dt.Month, dt.Day);
            Num = osn.AgreementNumber = string.Empty;
        }

        public void Save()
        {
            osn.AgreementDate = Date;
            osn.AgreementNumber = Num;
            ID = DataContainerFacade.Save<OldSIName, long>(osn);
            JournalLogger.LogEvent("Старое наименование СИ", JournalEventType.INSERT, ID, "From: " + this.GetType().Name, null, typeof(OldSIName));
            this.RefreshConnectedViewModels();
        }

        public bool IsNameChanged => (NewFullName != osn.OldFullName
                                      || NewShortName != osn.OldShortName
                                      || NewFormalizedName != osn.OldFormalizedName
                                      || (!string.IsNullOrWhiteSpace(NewInn) && NewInn != osn.OldINN)
                                      || !string.IsNullOrWhiteSpace(Num)) &&
                                     !string.IsNullOrWhiteSpace(NewFullName) &&
                                     !string.IsNullOrWhiteSpace(NewShortName) &&
                                     !string.IsNullOrWhiteSpace(NewFormalizedName) &&
                                     !string.IsNullOrWhiteSpace(Num) &&
                                     IsEditable && 
                                     string.IsNullOrEmpty(Validate());

        private string Validate()
        {
            string data = "newfullname|newshortname|newformalizedname|newinn|num|date";
            foreach (string key in data.ToLower().Split('|'))
            {
                string res = this[key];
                if (!string.IsNullOrEmpty(res))
                    return res;
            }
            return string.Empty;
        }

        #region Validation
        public override string this[string key]
        {
            get
            {
                try
                {
                    switch (key.ToLower())
                    {
                        case "newfullname": return NewFullName == null || NewFullName.Length == 0 || NewFullName.Length > 256 ? DataTools.DATAERROR_NEED_DATA : null;
                        case "newshortname": return NewShortName == null || NewShortName.Length == 0 || NewShortName.Length > 128 ? DataTools.DATAERROR_NEED_DATA : null;
                        case "newformalizedname": return NewFormalizedName == null || NewFormalizedName.Length == 0 || NewFormalizedName.Length > 128 ? DataTools.DATAERROR_NEED_DATA : null;
                        case "newinn": return NewInn != null && NewInn.Length > 0 && NewInn.Length != 10 ? "Неверное количество цифр в коде ИНН" : null;
                        case "num": return Num == null || Num.Length == 0 || Num.Length > 128 ? DataTools.DATAERROR_NEED_DATA : null;
                        //case "date": return Date == null || Date > DateTime.Now ? "Выбранная дата не может быть больше текущей" : null;

                        default: return null;
                    }
                }
                catch { return DataTools.DATAERROR_NEED_DATA; }
            }
        }

        public string Error => string.Empty;

        #endregion

        protected override void ExecuteSave()
        {
            throw new NotImplementedException();
        }

        public override bool CanExecuteSave()
        {
            throw new NotImplementedException();
        }
    }
}
