﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Collections;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog.Common
{
    [ReadAccess(DOKIP_ROLE_TYPE.User)]
    [EditAccess(DOKIP_ROLE_TYPE.User)]
    public class SelectYearMeasureViewModel : ViewModelBase
    {
        public bool IsValid => _selectedMeasure.HasValue && SelectedYear != null;
        public List<KeyValuePair<int, string>> Measurements { get; private set; }

        private int? _selectedMeasure;
        public RangeObservableCollection<Year> Years { get; private set; }

        public int? SelectedMeasure
        {
            get { return _selectedMeasure; }
            set
            {
                if (SetPropertyValue(nameof(SelectedMeasure), ref _selectedMeasure, value))
                    OnPropertyChanged(nameof(IsValid));
            }
        }

        private Year _selectedYear;

        public Year SelectedYear
        {
            get { return _selectedYear; }
            set
            {
                if (SetPropertyValue(nameof(SelectedMeasure), ref _selectedYear, value))
                    OnPropertyChanged(nameof(IsValid));
            }
        }
        
        public SelectYearMeasureViewModel()
        {
            Years = new RangeObservableCollection<Year>(DataContainerFacade.GetList<Year>());
            Measurements = new List<KeyValuePair<int, string>>()
            {
                new KeyValuePair<int, string>(1, "Рубль"),
                new KeyValuePair<int, string>(1000, "Тысяча рублей")
            };
        }
    }
}
