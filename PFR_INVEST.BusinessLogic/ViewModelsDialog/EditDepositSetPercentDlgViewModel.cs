﻿using System.Collections.Generic;
using PFR_INVEST.DataObjects.ListItems;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;

namespace PFR_INVEST.BusinessLogic
{
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	public sealed class EditDepositSetPercentDlgViewModel : ViewModelCardDialog
	{
		public List<DepositReturnListItem> Items { get; private set; }
		public decimal Sum { get; private set; }

        public DelegateCommand OkCommand { get; set; }

		public EditDepositSetPercentDlgViewModel(List<DepositReturnListItem> items)
            :base(false)
		{			
			Items = items;
			Sum = items.Sum(d => d.Amount);
			Header = string.Format("Корректировка процентов по депозиту №{0}", Items.First().OfferNumber);
            OkCommand = new DelegateCommand(o =>
            {
                OnPropertyChanged("IsValidSumm");
                return Items.Sum(d => d.Amount) != 0;
            }, o =>
            {
                if (!(IsValidSumm || DialogHelper.ShowWarning("Внимание! Сумма откорректированных процентов не совпадает с начальной суммой.\nСохранить суммы процентов?", "Внимание", false, true)))
                    return;
                RequestCloseView(true);
            });
		}

		public bool IsValidSumm
		{
			get { return Sum == Items.Sum(d => d.Amount); }
		}
	}
}
