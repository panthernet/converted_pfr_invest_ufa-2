﻿using System.Windows.Input;
using PFR_INVEST.BusinessLogic.Commands;

namespace PFR_INVEST.BusinessLogic
{
    /// <summary>
    /// Этот класс должен использоваться как базовый для диалогов со списком
    /// с возможностью выбора одного или более элементов
    /// </summary>
    public abstract class ViewModelListSelector : ViewModelListDialog
    {
        public int FocusedRowHandle { get; set; }

        public ICommand Ok { get; private set; }

        protected ViewModelListSelector(object param = null)
            : base(param)
        {
            Ok = new DelegateCommand(CanExecuteOk, ExecuteOk);
            FocusedRowHandle = -1;
        }

        protected virtual bool CanExecuteOk(object param)
        {
            return FocusedRowHandle >= 0;
        }

        protected virtual void ExecuteOk(object param)
        {
        }
    }
}
