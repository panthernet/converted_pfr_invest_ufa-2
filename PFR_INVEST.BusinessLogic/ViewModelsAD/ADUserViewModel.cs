﻿using System.Collections.Generic;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.BusinessLogic.Journal;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator)]
    public class ADUserViewModel : ViewModelCard
    {
        /// <summary>
        /// Пользователь
        /// </summary>
        public User User { get; private set; }

        public string Login
        {
            get
            {
                return User != null ? User.Login : string.Empty;
            }
            set
            {
                if (User != null) 
                    User.Login = value;
                OnPropertyChanged("Login");
            }
        }

        public string Name
        {
            get
            {
                return User != null ? User.Name : string.Empty;
            }
            set
            {
                if (User != null)
                    User.Name = value;
                OnPropertyChanged("Name");
            }
        }

        /// <summary>
        /// Список ролей, которых лишается пользователь
        /// </summary>
        private List<Role> _otherRoles = new List<Role>();
        public List<Role> OtherRoles 
        {
            get { return _otherRoles; }
            set
            {
                _otherRoles = value;
                OnPropertyChanged("RemovedRoles");
            }
        }

        private List<Role> _selectedRemovedRole;
        public List<Role> SelectedRemovedRole
        {
            get
            {
                return _selectedRemovedRole;
            }
            set
            {
                _selectedRemovedRole = value;
            }
        }

        /// <summary>
        /// Список ролей, которые получил пользователь
        /// </summary>
        private List<Role> _currentRoles = new List<Role>();
        public List<Role> CurrentRoles 
        {
            get { return _currentRoles; }
            set
            {
                _currentRoles = value;
                OnPropertyChanged("AddedRoles");
            }
        }

        private List<Role> _selectedAddedRole;
        public List<Role> SelectedAddedRole
        {
            get
            {
                return _selectedAddedRole;
            }
            set
            {
                _selectedAddedRole = value;
            }
        }

        private ObservableCollection<Role> _addedRoles;
        public ObservableCollection<Role> AddedRoles
        {
            get
            {
                return _addedRoles;
            }
            set
            {
                _addedRoles = value;
                OnPropertyChanged("AddedRoles");
            }
        }

        private ObservableCollection<Role> _removedRoles;
        public ObservableCollection<Role> RemovedRoles
        {
            get
            {
                return _removedRoles;
            }
            set
            {
                _removedRoles = value;
                OnPropertyChanged("RemovedRoles");
            }
        }

        public ADUserViewModel(User User)
            : base()
        {
            if (User == null) 
                return;
            
			


            Logger.WriteLine(string.Format("Получение информации о пользователе и список его ролей из Active Directory, время: {0}.", DateTime.Now.ToString("HH:mm:ss:ffff")));

            this.User = User;

            Reload();

            Logger.WriteLine(string.Format("Информации о пользователе и список его ролей получены из Active Directory, время: {0}.", DateTime.Now.ToString("HH:mm:ss:ffff")));

            _saveChanges = new Commands.DelegateCommand(o => CanExecuteSave(), o => ExecuteSave());
            _addRole = new Commands.DelegateCommand(o => CanExecuteAddRole(), o => ExecuteAddRole());
            _removeRole = new Commands.DelegateCommand(o => CanExecuteRemoveRole(), o => ExecuteRemoveRole());

            JournalLogger.LogTypeEvent(this, JournalEventType.ROLES_CHANGE, "Просмотр ролей пользователя", String.Format("Пользователь: {0}", User.Login));
        }

        private readonly ICommand _saveChanges;
        public ICommand SaveChanges => _saveChanges;

        private readonly ICommand _addRole;
        public ICommand AddRole => _addRole;

        private readonly ICommand _removeRole;
        public ICommand RemoveRole => _removeRole;

        public void ExecuteAddRole()
        {
            foreach (var role in SelectedRemovedRole.Where(role => RemovedRoles.Contains(role))) {
                AddedRoles.Add(role);
                RemovedRoles.Remove(role);
            }
            OnPropertyChanged("RemovedRoles");
            OnPropertyChanged("AddedRoles");            
        }

        public bool CanExecuteAddRole()
        {
            return OtherRoles.Count > 0 && SelectedRemovedRole != null;
        }

        public void ExecuteRemoveRole()
        {
            foreach (var role in SelectedAddedRole.Where(role => AddedRoles.Contains(role))) {
                RemovedRoles.Add(role);
                AddedRoles.Remove(role);
            }
            OnPropertyChanged("RemovedRoles");
            OnPropertyChanged("AddedRoles");
        }

        public bool CanExecuteRemoveRole()
        {
            return CurrentRoles.Count > 0 && SelectedAddedRole != null;
        }

        protected override void ExecuteSave()
        {
            var addFails = new Dictionary<Role, string>();
            var removeFails = new Dictionary<Role, string>();
            string message;

            var rolesToAdd = _addedRoles.ToList().Except(CurrentRoles);
            var rolesToRemove = _removedRoles.ToList().Intersect(CurrentRoles);

            if (AddedRoles != null)
            {

                foreach (var role in rolesToAdd)
                {
                    if (AP.Provider.AddToRole(role, User, out message))
                    {
                        JournalLogger.LogTypeEvent(this, JournalEventType.ROLES_CHANGE, "Добавление роли пользователю", String.Format("Добавлена роль: {0}, Пользователь: {1}", role.ShowName, User.Login));
                    }
                    else 
                    {
                        addFails.Add(role, message);
                    }
                }
            }
            if (RemovedRoles != null)
            {
                foreach (var role in rolesToRemove)
                {                    
                    if (AP.Provider.RemoveFromRole(role, User, out message))
                    {
                        JournalLogger.LogTypeEvent(this, JournalEventType.ROLES_CHANGE, "Удаление роли пользователю", String.Format("Удалена роль: {0}, Пользователь: {1}", role.ShowName, User.Login));
                    }
                    else 
                    {
                        removeFails.Add(role, message);
                    }
                }
            }
            //Сообщение об ошибках
            if (addFails.Count > 0 || removeFails.Count > 0) 
            {
                var errors = addFails.Select(f => string.Format("Ошибка добавления пользователя в роль {0}: {1}", f.Key.ShowName, f.Value))
                                .Concat(
                                    removeFails.Select(f => string.Format("Ошибка удаления пользователя из роли {0}: {1}", f.Key.ShowName, f.Value))
                                ).ToArray();
                var msg = string.Join(Environment.NewLine, errors);
                DialogHelper.ShowError(msg);
            }
            Reload(); 
            IsDataChanged = false;
        }

        public override bool CanExecuteSave()
        {
            return IsDataChanged && ((CurrentRoles != null) && (OtherRoles != null) && ((CurrentRoles.Count > 0) || (OtherRoles.Count > 0)));
        }

        public void Reload() 
        {
            //  Сначала получим список всех ролей
            OtherRoles = AP.Provider.GetRoles();

            //  Теперь получим список ролей принадлежащих пользователю
            User.Roles = AP.Provider.GetRoles(User);
            CurrentRoles = User.Roles;
                        
            

            foreach (var cRole in CurrentRoles)
                _otherRoles.ForEach(delegate(Role r)
                {
                    if (cRole.Name == r.Name)
                        _otherRoles.Remove(r);
                });

            AddedRoles = new ObservableCollection<Role>(CurrentRoles);
            RemovedRoles = new ObservableCollection<Role>(OtherRoles);

            IsDataChanged = false;
        }

        public override string this[string columnName] => string.Empty;
    }
}