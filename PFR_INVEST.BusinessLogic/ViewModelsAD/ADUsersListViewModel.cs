﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.DataObjects.Journal;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator)]
    public class ADUsersListViewModel : ViewModelBase
    {
        const string LOG_PREFIX = "Логирование грида 'Список пользователей'";

        public ObservableCollection<User> users;
        /// <summary>
        /// Контролируемый список ролей
        /// </summary>
        public ObservableCollection<User> Users
        {
            get
            {
                return users;
            }
            set
            {
                users = value;
                OnPropertyChanged("Users");
            }
        }

        #region Commands
        private readonly ICommand _addUser;
        public ICommand AddUser => _addUser;

        private readonly ICommand _deleteUser;
        public ICommand DeleteUser => _deleteUser;

        #endregion

        #region Execute implementation
        private void ExecuteAddUser()
        {
            if (DialogHelper.AddUser())
            {
                RefreshList();
            }
        }

        private bool CanExecuteAddUser()
        {
            return true;
        }

        private void ExecuteDeleteUser()
        {
            if (DialogHelper.DeleteUser())
            {
                RefreshList();
            }
        }

        private void RefreshList()
        {
            var users = AP.Provider.GetAuthUsers();
            if (users != null) Users = new ObservableCollection<User>(users);
        }

        private bool CanExecuteDeleteUser()
        {
            return true;
        }
        #endregion

        public ADUsersListViewModel()
        {
            try
            {
                Log(string.Format(LOG_PREFIX));

                _addUser = new DelegateCommand(o => CanExecuteAddUser(),
                    o => ExecuteAddUser());
                _deleteUser = new DelegateCommand(o => CanExecuteDeleteUser(),
                    o => ExecuteDeleteUser());

                if (AP.Provider != null)
                {
                    Log(string.Format("{0}: обращение к Active Directory для получения списка пользоателей, время: {1}.", LOG_PREFIX, DateTime.Now.ToString("HH:mm:ss:ffff")));
                    var userList = AP.Provider.GetAuthUsers();
                    if (Logger != null) 
                        Log(string.Format("{0}: список пользователей получен из Active Directory, количество пользователей - {1}, время: {2}.", 
                        LOG_PREFIX, 
                        userList == null ? 0 : userList.Count,
                        DateTime.Now.ToString("HH:mm:ss:ffff")));

                    if (userList != null)
                    {
                        Log(string.Format("{0}: обработка полученного списка пользователей, время: {1}.", LOG_PREFIX, DateTime.Now.ToString("HH:mm:ss:ffff")));
                        //this.Users = new ObservableCollection<ADUserViewModel>(userList.Select(b => new ADUserViewModel(b)));
                        Users = new ObservableCollection<User>(userList);
                        Log(string.Format("{0}: список пользователей обработан и передан в качестве источника данных в грид, время: {1}.", LOG_PREFIX, DateTime.Now.ToString("HH:mm:ss:ffff")));
                    }
                }

                JournalLogger.LogTypeEvent(this, JournalEventType.USERS_CHANGES, "Просмотр списка пользователей");
            }
            finally
            {
                Log(string.Format("{0}: Грид загружен: {1}", LOG_PREFIX, DateTime.Now.ToString("HH:mm:ss:ffff")));
            }
        }

        public void Log(string message)
        {
            if (Logger != null)
                Logger.WriteLine(string.Format(LOG_PREFIX));

        }
    }

}