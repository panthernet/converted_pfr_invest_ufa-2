﻿using PFR_INVEST.BusinessLogic.XLSModels;

namespace PFR_INVEST.BusinessLogic.ViewModelImportExport.XLSModels
{
    public class XLSBaseKOImport: XLSBase
    {
        public override int ColumnsCount { get; set; } = 3;

        public XLSBaseKOImport(string fileName) : base(fileName)
        {
        }
    }
}
