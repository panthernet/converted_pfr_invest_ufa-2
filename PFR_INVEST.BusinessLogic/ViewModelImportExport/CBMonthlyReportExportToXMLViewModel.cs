﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Schema;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Common.MessageCompressor;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.XMLModel;
using PFR_INVEST.XmlSchemas;

namespace PFR_INVEST.BusinessLogic.ViewModelImportExport
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class CBMonthlyReportExportToXMLViewModel : ViewModelCardDialog
    {
        private const string XML_SCHEME  = "URKI_Deposits.xsd";
        private const string NS_URI = "http://www.it.ru/Schemas/Avior/УРКИ";
        private const string NS_PREFIX = "av";


        private static void SaveToRepo(byte[] content, long reportId, string comment)
        {
            BLServiceSystem.Client.SaveCBReportToRepository(GZipCompressor.Compress(content), reportId, comment);
        }

        public bool GenerateXML(BOReportForm1 report, Person selectedPerson, string fileName)
        {
            return GenerateReport(report, selectedPerson, fileName, XML_SCHEME);
        }

        private void SaveReport(string content, string filename, XmlSchemaSet schema, long id, string comment)
        {
            #region Save & Validate
            var bytes = Encoding.Default.GetBytes(content);

            if(!string.IsNullOrEmpty(filename))
                using (var writer = File.CreateText(filename))
                {
                    writer.Write(Encoding.Default.GetChars(bytes));
                }

            SaveToRepo(bytes, id, comment);

            var valResult = XML.ValidateXmlFile(bytes, schema);
            if (!string.IsNullOrEmpty(valResult))
            {
                var errorsList = valResult.Split('|').ToList();

                Logger.WriteLine("При валидации XML файла произошли следующие ошибки:");
                errorsList.ForEach(a => Logger.WriteLine(a));
            }
            #endregion
        }

        private bool GenerateReport(BOReportForm1 report, Person selectedPerson, string fileName, string schemeName)
        {
            try
            {
                var schema = Util.GetXmlSchemaSet(schemeName, typeof(DocumentBase));
               // var pf = report.PortfolioID.HasValue ? DataContainerFacade.GetByID<Portfolio>(report.PortfolioID) : null;

                #region Body
                var doc = XML.CreateXmlDocumentWithRoot("РазмещенныеПенсионнымФондомДепозиты", NS_URI, NS_PREFIX);
                doc.DocumentElement.SetAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
                doc.DocumentElement.SetAttribute("xmlns:xserializer", "http://www.it.ru/Schemas/Avior/XSerializer");
                doc.DocumentElement.SetAttribute("appVersion", "2.16.3");

                XML.CreateAddXmlNodeWithValue(doc.FirstChild, "ДатаПредставления", DateTime.Today.ToСBXmlString(), NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(doc.FirstChild, "ИНН_1", report.INN, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(doc.FirstChild, "ОГРН_1", report.OGRN, NS_URI, NS_PREFIX);

                var qNode = XML.CreateAddXmlNode(doc.FirstChild, "РазмещенныеДепозиты", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(qNode, "SimpleEdit_43", DateTime.Now.ToСBXmlString(), NS_URI, NS_PREFIX);
                var depTableNode = XML.CreateAddXmlNode(qNode, "Таблица_РазмещенныеДепозиты", NS_URI, NS_PREFIX);
                var dtRows = XML.CreateAddXmlNode(depTableNode, "Row_49", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(dtRows, "Cell_50", "№", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(dtRows, "Cell_52", "Сокращенное наименование кредитной организации", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(dtRows, "Cell_54", "Валюта", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(dtRows, "Cell_56", "Сумма депозита, тыс. руб.", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(dtRows, "Cell_58", "Дата размещения средств на депозит", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(dtRows, "Cell_60", "Дата погашения депозита", NS_URI, NS_PREFIX);

                var list = BLServiceSystem.Client.GetDepositsListForCBMonthlyReportForm3(report.Year, report.Quartal);

                int count = 1;
                list.ForEach(item =>
                {
                    var dtString = XML.CreateAddXmlNode(depTableNode, "Таб_РазмещенныеДепозитыСтроки_1", NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(dtString, "Кол_НомерСтроки", count.ToString(), NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(dtString, "Кол_СокращНаимОрг", item.BankShortName, NS_URI, NS_PREFIX);
                    var dtCur = XML.CreateAddXmlNode(dtString, "Кол_Валюта", NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(dtCur, "Код", 99.ToString(), NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(dtCur, "Описание", "643 (Российский рубль)", NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(dtCur, "Активно", "true", NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(dtString, "Кол_СуммаДепозита", GetDecVal(item.Sum), NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(dtString, "Кол_ДатаРазмещения", item.SettleDate.ToСBXmlString(), NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(dtString, "Кол_ДатаПогашения", item.ReturnDate.ToСBXmlString(), NS_URI, NS_PREFIX);
                    count++;
                });

                //формирование сопр. письма
                var pLetter = XML.CreateAddXmlNode(doc.FirstChild, "СопроводительноеПисьмо", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetter, "ОКАТОСП", report.OKATO, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetter, "ИНН", report.INN, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetter, "ОГРН", report.OGRN, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetter, "ПолноеНаименование", report.FullName, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetter, "КраткоеНаименование", report.ShortName, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetter, "ДатаРегистрации", report.RegDate.Value.ToСBXmlString(), NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetter, "НомерРегистрации", report.RegNum, NS_URI, NS_PREFIX);
                var pLetterSign = XML.CreateAddXmlNode(doc.FirstChild, "ИдинИсполнитОрганСП", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetterSign, "ФамилияЕИО", selectedPerson.LastName, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetterSign, "ИмяЕИО", selectedPerson.FirstName, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetterSign, "ОтчествоЕИО", selectedPerson.MiddleName, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetterSign, "ДолжностьЕИО", selectedPerson.Position, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetterSign, "ТелефонЕИО", selectedPerson.Phone, NS_URI, NS_PREFIX);
                #endregion

                SaveReport(doc.OuterXml, fileName, schema, report.ID, $"Месячный отчет в ЦБ - {report.MonthText} {report.Year} ({fileName})");

                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex, "При экспорте XML файла месячного отчета в ЦБ");
                return false;
            } 
        }

        private string GetVal(decimal? value)
        {
            return value == null ? "0" : Convert.ToInt64(Math.Round(value.Value)).ToString();
        }

        private string GetDecVal(decimal? value)
        {
            return value?.ToString() ?? "0";
        }
    }
}
