﻿using System.ServiceModel;
using db2connector;

namespace PFR_INVEST.BusinessLogic
{
    public class BLDataServiceClient : ClientBase<IDB2Connector>
    {
        internal BLDataServiceClient()
        {
        }

        internal BLDataServiceClient(string endpointConfigurationName)
            : base(endpointConfigurationName)
        {
        }

        public IDB2Connector GetChannel()
        {
            return Channel;
        }
    }
}
