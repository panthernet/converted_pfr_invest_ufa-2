﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.DataObjects.Reports;

namespace PFR_INVEST.BusinessLogic.ViewModelsReport
{
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OKIP_manager)]
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OKIP_manager)]
    public class ReportErrorNpfSpreadViewModel : ReportNpfErrorBaseViewModel
	{
        private const string TEMPLATE_NAME = "Распределение ошибок по НПФ.docx";

		protected override void ExecuteSave()
		{
            var filePath = string.Empty;
            OnRequestLoadingIndicator("Создание отчета...");
            OnRequestTopMost(true);
		    try
		    {
		        var serviceData = BLServiceSystem.Client.GetReportErrorNpfSpreadData(new ReportErrorTypesPrompt {DateFrom = DateFrom, DateTo = DateTo}).ToList();

		        if (serviceData.Count == 0)
		        {
                    OnRequestLoadingIndicator();
                    OnRequestTopMost(false);
                    DialogHelper.ShowWarning("Данные для отчета за указанный период отсутствуют!", "Внимание");
                    return;
		        }

		        //подготовка данных
		        var columns = new List<WordReportDataColumn>();

		        serviceData.ForEach(a =>
		        {
		            if (!string.IsNullOrEmpty(a.Code) && columns.FirstOrDefault(b => b.Value == a.Code) == null)
		                columns.Add(new WordReportDataColumn(a.CodeName, a.Code));
		        });


		        var blankList = new List<object>(columns.Count);
		        for (int i = 0; i < columns.Count; i++)
		            blankList.Add(0);
		        var pieChartData = new Dictionary<string, object>();
		        var barChartData = new Dictionary<string, List<object>>();

		        //fill chart data
		        //добавляем наименования колонок для бар чарта (коды стыковки)
		        var barColumnNamesList = new List<string>();
		        columns.ForEach(a => barColumnNamesList.Add(a.Name));


		        /* Random rnd = new Random();
            for (int i = 0; i < 20; i++)
            {
                serviceData.Add(new ReportErrorSpreadQueryItem(new object[]
                    {
                        "Npf test name "+ i,
                        "11",
                        "NNNN",
                        rnd.Next(1, 1000)
                    }));
            }*/

		        foreach (var item in serviceData)
		        {
		            var columnIndex = columns.FindIndex(a => a.Value == item.Code);
		            //индекс < 0, не нашли колонку: возможно отсеяли колонки, которые не влезли на страницу
		            if (columnIndex < 0) continue;

		            if (pieChartData.ContainsKey(item.Name))
		                pieChartData[item.Name] = (int) pieChartData[item.Name] + item.Summ;
		            else pieChartData.Add(item.Name, item.Summ);

		            if (barChartData.ContainsKey(item.Name))
		                barChartData[item.Name][columnIndex] = item.Summ;
		            else
		            {
		                //проверка на переполнение кол-ва записей
		                //если превышаем - добавляем запись об остальных НПФ
		                if (barChartData.Count == MAX_BAR_CHART_RECORDS)
		                {
		                    var list2 = blankList.ToList();
		                    list2[columnIndex] = item.Summ;
		                    barChartData.Add("Остальные НПФ", list2);
		                    continue;
		                }
		                //если запись об остальных НПФ уже добавлена, обновляем
		                if (barChartData.Count > MAX_BAR_CHART_RECORDS)
		                {
		                    barChartData.Last().Value[columnIndex] = (int) barChartData.Last().Value[columnIndex] + item.Summ;
		                    continue;
		                }
		                var list = blankList.ToList();
		                list[columnIndex] = item.Summ;
		                barChartData.Add(item.Name, list);
		            }
		        }

		        //сортировка
		        pieChartData = pieChartData.OrderByDescending(a => a.Value).ToDictionary(a => a.Key, a => a.Value);
		        barChartData = barChartData.OrderByDescending(a => a.Value.Sum(b => b as int? ?? 0)).ToDictionary(a => a.Key, a => a.Value);


		        //подготовка шаблона
		        filePath = TemplatesManager.ExtractTemplate(TEMPLATE_NAME);
		        using (var word = new WordReportProxy(filePath))
		        {
		            word.ReportManager.FindAndReplace("{DateFrom}", DateFrom.HasValue ? DateFrom.Value.ToShortDateString() : "");
		            word.ReportManager.FindAndReplace("{DateTo}", DateTo.HasValue ? DateTo.Value.ToShortDateString() : "");
		            word.ReportManager.PopulatePieChart(pieChartData, 1, 5);
		            word.ReportManager.PopulateBarChart(barChartData, barColumnNamesList, 2);
		            word.ReportManager.SaveAndClose();
		        }
		    }
            finally
            {
                OnRequestLoadingIndicator();
                OnRequestTopMost(false);
            }
            Process.Start(filePath);
            Close();
		}
	}
}
