﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Office.Interop.Excel;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.BranchReport;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
    public class PfrBranchReportImportHandler_SignatureContract : PfrBranchReportImportHandlerBase
    {
        public override PfrBranchReportType.BranchReportType ReportType => PfrBranchReportType.BranchReportType.SignatureContract;

        private const string Mask = "SRP_05_*.xls;SRP_05_*.xlsx";

        public override string GetOpenDirectoryMask()
        {
            return Mask;
        }

        public override string GetOpenFileMask()
        {
            return string.Format("Отчет по региону SRP_05_(.xls, .xlsx)|{0}", Mask);
        }

        public override bool IsValidFileName(string fileName, out string errorMessage)
        {
            PfrBranchReport r;

            if (TryParseFileName(fileName, out r))
            {
                errorMessage = null;
                return true;
            }
            errorMessage = string.Format("Некорректное имя файла '{0}'", fileName);
            return false;
        }

        public override bool LoadFile(string fileName, PfrBranchReport r)
        {
            var x = new PfrBranchReportImportContent_SignatureContract
            {
                PfrBranchReport = r,
                ItemsCredit = new List<PfrBranchReportSignatureContract>(),
                ItemsNpf = new List<PfrBranchReportSignatureContract>()
            };

            if (LoadFile(fileName, LoadData, x))
            {
                Items.Add(x);
                return true;
            }
            return false;
        }

        public override bool CanExecuteOpenFile()
        {
            return true;
        }

        public override bool CanExecuteImport()
        {
            return Items.Count > 0;
        }

        public void LoadData(Worksheet worksheet, PfrBranchReportImportContentBase paramter, out bool isloadFileCorrect)
        {
            var x = (PfrBranchReportImportContent_SignatureContract)paramter;


            var d = new DataFeeder(worksheet);

            PfrBranchReportSignatureContract r;

            if (!d.IsSectionHeader("кредитные организации"))
                throw new PfrBranchReportImportException("Раздел отчета 'кредитные организации' не найден");
            d.SkipRow();

            while (!d.IsSectionHeader("НПФ") && ((r = d.GetCreditRow()) != null))
                x.AddCredit(r);


            if (!d.IsSectionHeader("НПФ"))
                throw new PfrBranchReportImportException("Раздел отчета 'НПФ' не найден");
            d.SkipRow();

            while (!d.IsSectionHeader("организации связи") && ((r = d.GetNpfRow()) != null))
                x.AddNpf(r);

            isloadFileCorrect = true;
        }


        private class DataFeeder
        {
            public Worksheet S;
            public int RowNumber;

            private Dictionary<string, long> NpfIndex;
            private Dictionary<string, long> CreditIndex;

            public DataFeeder(Worksheet s)
            {
                S = s;
                RowNumber = 5;

                LoadCreditIndex();
                LoadNpfIndex();
            }

            private void LoadCreditIndex()
            {
                CreditIndex = new Dictionary<string, long>();

                var xL = BLServiceSystem.Client.GetBankListForDeposit();

                foreach (var x in xL)
                {
                    string s = x.FormalizedName.ToLower().Trim();

                    if (!CreditIndex.ContainsKey(s))
                        CreditIndex.Add(s, x.ID);
                }
            }

            private void LoadNpfIndex()
            {
                NpfIndex = new Dictionary<string, long>();

                var xL = BLServiceSystem.Client.GetNPFListLEReorgHib().ToList();

                foreach (var x in xL)
                {
                    if (x.FormalizedName == null)
                        continue;

                    string s = x.FormalizedName.ToLower().Trim();

                    if (!NpfIndex.ContainsKey(s))
                        NpfIndex.Add(s, x.ID);
                }
            }


            internal PfrBranchReportSignatureContract GetCreditRow()
            {
                Array a = GetRowValue();

                if (IsEmpty(a))
                {
                    a = GetRowValue();
                    if (IsEmpty(a))//Two empty lines, import done
                    {
                        return null;
                    }
                }

                try
                {
                    PfrBranchReportSignatureContract res = new PfrBranchReportSignatureContract();
                    res.DataType = PfrBranchReportSignatureContract.enType.Credit;
                    res.LegalEntityID = GetCreditID(a.GetValue(1, 2).ToString());

                    res.Name = GetString(a.GetValue(1, 2));

                    res.ContractNumber = GetString(a.GetValue(1, 3));
                    res.ContractDate = GetDate(a.GetValue(1, 4));

                    res.PostAddress = GetString(a.GetValue(1, 5));
                    res.Phone = GetString(a.GetValue(1, 6));
                    res.ContactName = GetString(a.GetValue(1, 7));

                    return res;
                }
                catch (PfrBranchReportImportException)
                { throw; }
                catch
                {
                    throw new PfrBranchReportImportException("Ошибка чтения значений");
                }
            }

            internal PfrBranchReportSignatureContract GetNpfRow()
            {
                Array a = GetRowValue();

                if (IsEmpty(a))
                {
                    a = GetRowValue();
                    if (IsEmpty(a))//Two empty lines, import done
                    {
                        return null;
                    }
                }

                try
                {
                    PfrBranchReportSignatureContract res = new PfrBranchReportSignatureContract();
                    res.DataType = PfrBranchReportSignatureContract.enType.Npf;
                    res.LegalEntityID = GetNpfID(a.GetValue(1, 2).ToString());

                    res.Name = GetString(a.GetValue(1, 2));

                    res.ContractNumber = GetString(a.GetValue(1, 3));
                    res.ContractDate = GetDate(a.GetValue(1, 4));

                    res.PostAddress = GetString(a.GetValue(1, 5));
                    res.Phone = GetString(a.GetValue(1, 6));
                    res.ContactName = GetString(a.GetValue(1, 7));

                    return res;
                }
                catch (PfrBranchReportImportException)
                { throw; }
                catch 
                {
                    throw new PfrBranchReportImportException("Ошибка чтения значений");
                }
            }

            private string GetString(object x)
            {
                if (x == null)
                    return null;

                return x.ToString().Trim();
            }

            private DateTime GetDate(object x)
            {
                if (x == null)
                    throw new PfrBranchReportImportException("Ошибка: вместо даты пустое значение");
                DateTime dt = new DateTime();

                
                if(!DateTime.TryParse(x.ToString(), out dt))
                    throw new PfrBranchReportImportException(String.Format("Ошибка: вместо даты неправильное значение. Строка: {0}, Колонка :{1}. Текущее значение: {2}.",  RowNumber, 3, x));

                return Convert.ToDateTime(x);
            }

            private long GetLong(object x)
            {
                if (x == null)
                    throw new PfrBranchReportImportException("Ошибка: вместо числового значения пустое значение");

                return Convert.ToInt64(x);
            }

            private Array GetRowValue()
            {
                Range r = GetRow();
                Array a = (Array)r.Value;
                return a;
            }

            private Array GetRowValueSkipped()
            {
                Range r = GetRowSkipped();
                Array a = (Array)r.Value;
                return a;
            }


            private bool IsEmpty(Array a)
            {
                string s = Convert.ToString(a.GetValue(1, 2));

                return string.IsNullOrEmpty(s);
            }

            private Range GetRow()
            {
                Range r = S.Range["A" + RowNumber, "G" + RowNumber];
                RowNumber++;
                return r;
            }

            private Range GetRowSkipped()
            {
                Range r = S.Range["A" + RowNumber, "G" + RowNumber];
                return r;
            }


            private long GetCreditID(string name)
            {
                if (string.IsNullOrEmpty(name))
                    throw new PfrBranchReportImportException("Некорректное значение наименования кредитной организации");

                name = name.Trim().ToLower();

                if (CreditIndex.ContainsKey(name))
                    return CreditIndex[name];


                throw new PfrBranchReportImportException(string.Format("Наименование кредитной организации '{0}' отсутствует в перечне банков", name));
            }

            private long GetNpfID(string name)
            {
                if (string.IsNullOrEmpty(name))
                    throw new PfrBranchReportImportException("Некорректное значение наименования НПФ");

                name = name.Trim().ToLower();

                if (NpfIndex.ContainsKey(name))
                    return NpfIndex[name];


                throw new PfrBranchReportImportException(string.Format("Наименование НПФ '{0}' отсутствует в справочнике НПФ", name));
            }


            internal bool IsSectionHeader(string text)
            {
                var a = GetRowValueSkipped();
                string s = Convert.ToString(a.GetValue(1, 2));

                if (string.IsNullOrEmpty(s))
                    return false;

                s = s.Trim().ToLower();
                return s == text.Trim().ToLower();
            }

            internal void SkipRow()
            {
                RowNumber++;
            }
        }
    }
}
