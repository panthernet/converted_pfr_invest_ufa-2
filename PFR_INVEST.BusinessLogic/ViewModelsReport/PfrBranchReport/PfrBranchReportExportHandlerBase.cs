﻿using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
    public abstract class PfrBranchReportExportHandlerBase
    {
        public abstract PfrBranchReportType.BranchReportType ReportType { get; }

        public abstract bool CanExecuteExport();

        public abstract void ExecuteExport();

        public virtual bool IsYearEnabled => true;

        public virtual bool IsMonthEnabled => true;

        public virtual bool IsResponsibleEnabled => true;

        public virtual bool IsPerformerEnabled => true;

        public virtual bool IsPeriodLengthVisible => false;

        public virtual bool IsPeriodLengthEnabled => false;


        public virtual bool IsMonthly
        {
            get { return !_IsAnnual; }
            set { _IsAnnual = !value; }
        }

        private bool _IsAnnual;

        public virtual bool IsAnnual
        {
            get { return _IsAnnual; }
            //get { return true; }
            set { _IsAnnual = value; }
        }
    }
}
