﻿using PFR_INVEST.BusinessLogic.ViewModelsDialog;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
    public class PfrBranchReportExportHandler_SignatureContract : PfrBranchReportExportHandlerBase
    {
        private RegionReportExportDlgViewModel Model;

        public PfrBranchReportExportHandler_SignatureContract()
        {
        }

        public PfrBranchReportExportHandler_SignatureContract(RegionReportExportDlgViewModel model)
            : this()
        {
            Model = model;
        }

        public override PfrBranchReportType.BranchReportType ReportType => PfrBranchReportType.BranchReportType.SignatureContract;

        public override bool CanExecuteExport()
        {
            if (Model.SelectedYear == null || !Model.Validate())
                return false;

            if (Model.SelectedMonth == null && Model.IsMonthly)
                return false;

            return true;
        }


        public override void ExecuteExport()
        {
            if (!CanExecuteExport())
                return;

            ExecuteExport(Model.SelectedYear.Value, Model.SelectedMonth, Model.Responsible, Model.Performer);
        }

        public void ExecuteExport(int periodYear, int? periodMonth, Person responsible, Person performer)
        {
            var x = new PrintSignatureContract
            {
                PeriodYear = periodYear,
                PeriodMonth = periodMonth,
                Responsible = responsible,
                Performer = performer
            };
            x.LoadData();


            // if we don't have complete data for report, ask user if he wants to make report
            if (x.IsReportDataIncomplete() && Model!=null && !Model.AskReportDataIncomplete(x.GetDataIncompleteMessage()))
                return;

            x.Print(true);
        }

        public override bool IsPeriodLengthVisible => true;

        public override bool IsPeriodLengthEnabled => true;
    }
}
