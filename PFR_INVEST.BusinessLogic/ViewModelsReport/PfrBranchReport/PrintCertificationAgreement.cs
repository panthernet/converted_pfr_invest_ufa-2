﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Office.Interop.Excel;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OSRP_worker, DOKIP_ROLE_TYPE.OSRP_directory_editor, DOKIP_ROLE_TYPE.OSRP_manager)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OSRP_worker, DOKIP_ROLE_TYPE.OSRP_directory_editor, DOKIP_ROLE_TYPE.OSRP_manager)]
    public class PrintCertificationAgreement : PrintBase
    {
        public int PeriodYear { get; set; }
        public int? PeriodMonth { get; set; }
        public Person Responsible { get; set; }
        public Person Performer { get; set; }


        DataContainer Data;


        public PrintCertificationAgreement()
        {
            Name = "Отчет по соглашениям о взаимном удостоверении подписей";
            TemplateName = Name + ".xlsx";
        }

        protected override void FillData()
        {
            Fields.Add("REPORT_DATE", GetReportDateWords(PeriodYear, PeriodMonth));

            PrintResponsiblePerformer(Responsible, Performer);
        }


        public override void GenerateDocument(Worksheet s)
        {
            PrintHelper ph = new PrintHelper(s);
            Data.Print(ph);
        }

        public void LoadData()
        {
            Data = new DataContainer();
            Data.LoadData(PeriodYear, PeriodMonth);
        }

        internal bool IsReportDataIncomplete()
        {
            return Data.IsDataIncomplete();
        }

        internal string GetDataIncompleteMessage()
        {
            List<PFRBranch> xL = Data.GetDataIncompleteRegions();
            return GetDataIncompleteMessage(xL);
        }

        class PrintHelper
        {
            public PrintHelper()
            { }

            public PrintHelper(Worksheet s)
                : this()
            {
                S = s;
                ItemCount = 0;
                RowNumber = 9;
            }

            public Worksheet S;
            public int RowNumber;
            public int ItemCount;

            internal void AddEmptyRows(int count)
            {
                Range r = S.get_Range("A9", "G" + (9 - 1 + count));
                r.Insert(XlInsertShiftDirection.xlShiftDown, Missing.Value);

                r = S.get_Range("A9", "G" + (9 - 1 + count));
                r.Borders[XlBordersIndex.xlEdgeLeft].LineStyle = XlLineStyle.xlContinuous;
                r.Borders[XlBordersIndex.xlEdgeRight].LineStyle = XlLineStyle.xlContinuous;
                r.Borders[XlBordersIndex.xlEdgeBottom].LineStyle = XlLineStyle.xlContinuous;
                r.Borders[XlBordersIndex.xlInsideHorizontal].LineStyle = XlLineStyle.xlContinuous;
                r.Borders[XlBordersIndex.xlInsideVertical].LineStyle = XlLineStyle.xlContinuous;
            }

            private Range GetRow()
            {
                Range r = S.get_Range("A" + RowNumber, "G" + RowNumber);
                RowNumber++;
                return r;
            }

            private void SkipRows(int count)
            {
                RowNumber += count;
            }

            private Range GetNumRows(int count)
            {
                Range r = S.get_Range("D" + RowNumber, "G" + (RowNumber + count - 1));
                return r;
            }

            private Range GetARows(int count)
            {
                Range r = S.get_Range("A" + RowNumber, "A" + (RowNumber + count - 1));
                return r;
            }

            private Range GetBRows(int count)
            {
                Range r = S.get_Range("B" + RowNumber, "B" + (RowNumber + count - 1));
                return r;
            }

            private Range GetCRows(int count)
            {
                Range r = S.get_Range("C" + RowNumber, "C" + (RowNumber + count - 1));
                return r;
            }

            private Range GetRows(string colFrom, string colTo, int count)
            {
                Range r = S.get_Range(colFrom + RowNumber, colTo + (RowNumber + count - 1));
                return r;
            }

            private Range AddRows(int count)
            {
                Range r = S.get_Range("A" + RowNumber, "G" + (RowNumber + count - 1));
                RowNumber += count;
                return r;
            }


            internal void Print(DataContainer x)
            {
                Range r;
                Range r2;
                object[] a;
                object[,] aa;

                r = GetNumRows(1);
                a = new object[4];
                a[0] = x.S.Sum_NE;
                a[1] = x.S.Sum_N;
                a[2] = x.S.Sum_E;
                a[3] = x.S.Sum_T;
                r.Value2 = a;
                r.Style = "PfrNumberTotal";

                r2 = GetRows("A", "B", 1);
                r2.Merge();
                r2.Value2 = "Итого по Российской Федерации";
                r2.Style = "PfrTotal1";
                SkipRows(1);

                r = GetRow();
                r2 = r.get_Range("B1", "G1");
                r2.Merge();
                r2.Value2 = "в т.ч. соглашений, заключенных с:";
                r.Style = "PfrTotal2";



                r = GetNumRows(4);
                aa = new object[4, 4];

                aa[0, 0] = x.S.Sum_NPF_NE;
                aa[0, 1] = x.S.Sum_NPF_N;
                aa[0, 2] = x.S.Sum_NPF_E;
                aa[0, 3] = x.S.Sum_NPF_T;

                aa[1, 0] = x.S.Sum_Cre_NE;
                aa[1, 1] = x.S.Sum_Cre_N;
                aa[1, 2] = x.S.Sum_Cre_E;
                aa[1, 3] = x.S.Sum_Cre_T;

                aa[2, 0] = x.S.Sum_Emp_NE;
                aa[2, 1] = x.S.Sum_Emp_N;
                aa[2, 2] = x.S.Sum_Emp_E;
                aa[2, 3] = x.S.Sum_Emp_T;

                aa[3, 0] = x.S.Sum_Com_NE;
                aa[3, 1] = x.S.Sum_Com_N;
                aa[3, 2] = x.S.Sum_Com_E;
                aa[3, 3] = x.S.Sum_Com_T;

                r.Value2 = aa;
                r.Style = "PfrNumberTotal";

                r = GetBRows(4);
                aa = new object[4, 1];
                aa[0, 0] = "НПФ";
                aa[1, 0] = "кредитными организациями";
                aa[2, 0] = "организациями работодателями";
                aa[3, 0] = "организациями связи";

                r.Value2 = aa;
                r.Style = "PfrTotal3";
                SkipRows(4);
            }



            internal void Print(FedoContainer x)
            {
                if (x.Fedo.Special == 1)
                    return;

                Range r;
                Range r2;
                object[] a;
                object[,] aa;

                r = GetNumRows(1);
                a = new object[4];
                a[0] = x.S.Sum_NE;
                a[1] = x.S.Sum_N;
                a[2] = x.S.Sum_E;
                a[3] = x.S.Sum_T;
                r.Value2 = a;
                r.Style = "PfrNumberTotal";

                r2 = GetRows("A", "B", 1);
                r2.Merge();
                r2.Value2 = x.Fedo.Name;
                r2.Style = "PfrFedo1";
                SkipRows(1);


                r = GetBRows(5);
                r.Merge();

                r = GetRows("C", "G", 1);
                r.Merge();
                r.Value2 = "в т.ч. соглашений, заключенных с:";
                r.Style = "PfrFedo2";
                SkipRows(1);


                r = GetNumRows(4);
                aa = new object[4, 4];

                aa[0, 0] = x.S.Sum_NPF_NE;
                aa[0, 1] = x.S.Sum_NPF_N;
                aa[0, 2] = x.S.Sum_NPF_E;
                aa[0, 3] = x.S.Sum_NPF_T;

                aa[1, 0] = x.S.Sum_Cre_NE;
                aa[1, 1] = x.S.Sum_Cre_N;
                aa[1, 2] = x.S.Sum_Cre_E;
                aa[1, 3] = x.S.Sum_Cre_T;

                aa[2, 0] = x.S.Sum_Emp_NE;
                aa[2, 1] = x.S.Sum_Emp_N;
                aa[2, 2] = x.S.Sum_Emp_E;
                aa[2, 3] = x.S.Sum_Emp_T;

                aa[3, 0] = x.S.Sum_Com_NE;
                aa[3, 1] = x.S.Sum_Com_N;
                aa[3, 2] = x.S.Sum_Com_E;
                aa[3, 3] = x.S.Sum_Com_T;

                r.Value2 = aa;
                r.Style = "PfrNumberTotal";

                r = GetCRows(4);
                aa = new object[4, 1];
                aa[0, 0] = "НПФ";
                aa[1, 0] = "кредитными организациями";
                aa[2, 0] = "организациями работодателями";
                aa[3, 0] = "организациями связи";

                r.Value2 = aa;
                r.Style = "PfrFedo3";
                SkipRows(4);
            }

            internal void Print(BranchContainer x)
            {
                Range r;
                //Range r2;
                //object[] a;
                object[,] aa;
                int n = x.NpfList.Count;


                ItemCount++;
                r = GetARows(n + 5);
                r.Merge();
                r.Value2 = ItemCount;
                r.Style = "PfrBranch1";


                r = GetBRows(n + 5);
                r.Merge();
                r.Value2 = x.Branch.Name;
                r.Style = "PfrBranch2";


                //Titles
                r = GetCRows(n + 4);
                aa = new object[n + 4, 1];
                aa[0, 0] = "НПФ всего, в том числе:";

                for (int j = 0; j < n; j++)
                    aa[j + 1, 0] = x.NpfList[j].Npf.FormalizedName;

                aa[n + 1, 0] = "Кредитные организации";
                aa[n + 2, 0] = "Организации работодатели";
                aa[n + 3, 0] = "Организации связи";

                r.Value2 = aa;
                r.Style = "PfrBranch3";


                r = GetNumRows(n + 5);
                aa = new object[n + 5, 4];

                int i = 0;
                aa[i, 0] = x.S.Sum_NPF_NE;
                aa[i, 1] = x.S.Sum_NPF_N;
                aa[i, 2] = x.S.Sum_NPF_E;
                aa[i, 3] = x.S.Sum_NPF_T;

                //npf
                foreach (var npf in x.NpfList)
                {
                    i++;
                    //Print(npf);
                    aa[i, 0] = npf.S.Sum_NPF_NE;
                    aa[i, 1] = npf.S.Sum_NPF_N;
                    aa[i, 2] = npf.S.Sum_NPF_E;
                    aa[i, 3] = npf.S.Sum_NPF_T;
                }
                i++;
                aa[i, 0] = x.S.Sum_Cre_NE;
                aa[i, 1] = x.S.Sum_Cre_N;
                aa[i, 2] = x.S.Sum_Cre_E;
                aa[i, 3] = x.S.Sum_Cre_T;

                i++;
                aa[i, 0] = x.S.Sum_Emp_NE;
                aa[i, 1] = x.S.Sum_Emp_N;
                aa[i, 2] = x.S.Sum_Emp_E;
                aa[i, 3] = x.S.Sum_Emp_T;

                i++;
                aa[i, 0] = x.S.Sum_Com_NE;
                aa[i, 1] = x.S.Sum_Com_N;
                aa[i, 2] = x.S.Sum_Com_E;
                aa[i, 3] = x.S.Sum_Com_T;

                i++;
                aa[i, 0] = x.S.Sum_NE;
                aa[i, 1] = x.S.Sum_N;
                aa[i, 2] = x.S.Sum_E;
                aa[i, 3] = x.S.Sum_T;

                r.Value2 = aa;
                r.Style = "PfrNumber";


                SkipRows(4 + n);

                //Last row of branch: total
                r = GetCRows(1);
                r.Value2 = "Всего по ОПФР";
                r.Style = "PfrBranch4";


                r = GetNumRows(1);
                r.Style = "PfrBranchTotal";
                SkipRows(1);
            }

            private void Print(NpfContainer n)
            {
                //throw new NotImplementedException();
            }
        }


        class Sum
        {
            public const int ARRAY_SIZE = 12;

            public decimal[] s = new decimal[ARRAY_SIZE];

            public decimal Sum_NE => Sum_N + Sum_E;

            public decimal Sum_N => Sum_NPF_N + Sum_Cre_N + Sum_Emp_N + Sum_Com_N;
            public decimal Sum_E => Sum_NPF_E + Sum_Cre_E + Sum_Emp_E + Sum_Com_E;
            public decimal Sum_T => Sum_NPF_T + Sum_Cre_T + Sum_Emp_T + Sum_Com_T;


            public decimal Sum_NPF_NE => Sum_NPF_N + Sum_NPF_E;

            public decimal Sum_NPF_N { get { return s[0]; } set { s[0] = value; } }
            public decimal Sum_NPF_E { get { return s[1]; } set { s[1] = value; } }
            public decimal Sum_NPF_T { get { return s[2]; } set { s[2] = value; } }


            public decimal Sum_Cre_NE => Sum_Cre_N + Sum_Cre_E;

            public decimal Sum_Cre_N { get { return s[3]; } set { s[3] = value; } }
            public decimal Sum_Cre_E { get { return s[4]; } set { s[4] = value; } }
            public decimal Sum_Cre_T { get { return s[5]; } set { s[5] = value; } }


            public decimal Sum_Emp_NE => Sum_Emp_N + Sum_Emp_E;

            public decimal Sum_Emp_N { get { return s[6]; } set { s[6] = value; } }
            public decimal Sum_Emp_E { get { return s[7]; } set { s[7] = value; } }
            public decimal Sum_Emp_T { get { return s[8]; } set { s[8] = value; } }


            public decimal Sum_Com_NE => Sum_Com_N + Sum_Com_E;

            public decimal Sum_Com_N { get { return s[9]; } set { s[9] = value; } }
            public decimal Sum_Com_E { get { return s[10]; } set { s[10] = value; } }
            public decimal Sum_Com_T { get { return s[11]; } set { s[11] = value; } }

            public void Add(Sum x)
            {
                for (int i = 0; i < ARRAY_SIZE; i++)
                    s[i] += x.s[i];
            }

            public void Clear()
            {
                for (int i = 0; i < ARRAY_SIZE; i++)
                    s[i] = 0;
            }

            internal void AddCre(PfrBranchReportCertificationAgreement x)
            {
                if (x == null) return;

                Sum_Cre_N += x.New;
                //Sum_Cre_E = x.Existing;
                Sum_Cre_T += x.Terminated;
            }

            internal void AddEmp(PfrBranchReportCertificationAgreement x)
            {
                if (x == null) return;

                Sum_Emp_N += x.New;
                //Sum_Emp_E += x.Existing;
                Sum_Emp_T += x.Terminated;
            }

            internal void AddCom(PfrBranchReportCertificationAgreement x)
            {
                if (x == null) return;

                Sum_Com_N += x.New;
                //Sum_Com_E += x.Existing;
                Sum_Com_T += x.Terminated;
            }

            internal void AddNpf(PfrBranchReportCertificationAgreement x)
            {
                Sum_NPF_N += x.New;
                //Sum_NPF_E += x.Existing;
                Sum_NPF_T += x.Terminated;
            }
        }

        class DataContainer
        {
            public PfrBranchReportType.BranchReportType ReportType => PfrBranchReportType.BranchReportType.CertificationAgreement;

            public DataContainer()
            {
                FedoList = new List<FedoContainer>();
                S = new Sum();
            }

            public List<FedoContainer> FedoList;
            public Sum S;

            public void CalcSum()
            {
                S.Clear();

                foreach (var x in FedoList)
                {
                    x.CalcSum();
                    S.Add(x.S);
                }
            }

            internal void LoadData(int periodYear, int? periodMonth)
            {
                bool isAnnual = periodMonth == null;

                List<FEDO> Fedos;
                List<PFRBranch> Branches;
                List<PfrBranchReport> Reports;
                List<PfrBranchReportCertificationAgreement> ReportData;
                List<LegalEntity> NPFs;

                Fedos = DataContainerFacade.GetList<FEDO>().OrderBy(x => x.ID).ToList();
                Branches = DataContainerFacade.GetList<PFRBranch>().ToList();

                {
                    var pL = new List<ListPropertyCondition>();
                    pL.Add(new ListPropertyCondition { Name = "ReportTypeID", Value = (long)ReportType });
                    pL.Add(new ListPropertyCondition { Name = "PeriodYear", Value = periodYear });
                    if (!isAnnual)
                        pL.Add(new ListPropertyCondition { Name = "PeriodMonth", Value = periodMonth });
                    pL.Add(new ListPropertyCondition { Name = "StatusID", Value = 1L });

                    Reports = DataContainerFacade.GetListByPropertyConditions<PfrBranchReport>(pL).ToList();
                }

                {
                    var pL = new List<ListPropertyCondition>();
                    pL.Add(new ListPropertyCondition { Name = "ReportId", Values = (from x in Reports select x.ID).Cast<object>().ToArray(), Operation = "in" });
                    ReportData = DataContainerFacade.GetListByPropertyConditions<PfrBranchReportCertificationAgreement>(pL).ToList();
                }

                {
                    object[] ids = (from x in ReportData where x.NpfId != null select x.NpfId).Distinct().Cast<object>().ToArray();

                    var pL = new List<ListPropertyCondition>();
                    pL.Add(new ListPropertyCondition { Name = "ID", Values = ids, Operation = "in" });
                    NPFs = DataContainerFacade.GetListByPropertyConditions<LegalEntity>(pL).ToList();
                }

                LoadData(Fedos, Branches, Reports, ReportData, NPFs, isAnnual);
            }

            internal void LoadData(List<FEDO> fedos, List<PFRBranch> branches, List<PfrBranchReport> reports, List<PfrBranchReportCertificationAgreement> reportData, List<LegalEntity> npfs, bool isAnnual)
            {
                Dictionary<long, FedoContainer> fD = new Dictionary<long, FedoContainer>();

                foreach (var x in fedos.OrderBy(x => x.ID))
                {
                    var y = new FedoContainer { Fedo = x };
                    FedoList.Add(y);
                    fD.Add(x.ID, y);
                }

                Dictionary<long, BranchContainer> bD = new Dictionary<long, BranchContainer>();

                foreach (var x in from y in branches orderby y.RegionNumber, y.Name select y)
                {
                    var y = new BranchContainer { Branch = x };
                    fD[x.FEDO_ID].AddBranch(y);
                    bD.Add(x.ID, y);
                }

                Dictionary<long, LegalEntity> nD = new Dictionary<long, LegalEntity>();

                foreach (var x in npfs)
                {
                    nD.Add(x.ID, x);
                }

                Dictionary<long, PfrBranchReport> rD = new Dictionary<long, PfrBranchReport>();

                foreach (var x in reports)
                {
                    rD.Add(x.ID, x);
                }


                List<ReportDataContainer> rdL = new List<ReportDataContainer>();

                foreach (var x in reportData)
                {
                    var y = new ReportDataContainer { Report = rD[x.ReportId], Data = x };
                    rdL.Add(y);
                }

                foreach (var x in from y in rdL orderby y.Report.PeriodYear, y.Report.PeriodMonth select y)
                {
                    var n = x.Data.NpfId == null ? null : nD[x.Data.NpfId.Value];
                    var b = bD[x.Report.BranchID];


                    b.AddReportData(x, n, isAnnual);
                }


                CalcSum();
            }

            internal void Print(PrintHelper ph)
            {
                int n = GetRowCount();
                ph.AddEmptyRows(n);

                ph.Print(this);

                foreach (var x in FedoList)
                    x.Print(ph);
            }

            private int GetRowCount()
            {
                int n = 6;
                foreach (var x in FedoList)
                    n += x.GetRowCount();

                return n;
            }

            internal bool IsDataIncomplete()
            {
                if (FedoList == null || FedoList.Count == 0)
                    return true;

                foreach (var x in FedoList)
                    if (x.IsDataIncomplete())
                        return true;

                return false;
            }


            internal List<PFRBranch> GetDataIncompleteRegions()
            {
                List<PFRBranch> res = new List<PFRBranch>();

                foreach (var x in FedoList)
                    res.AddRange(x.GetDataIncompleteRegions());

                return res;
            }
        }

        class FedoContainer
        {
            public FedoContainer()
            {
                BranchList = new List<BranchContainer>();
                S = new Sum();
            }

            public FEDO Fedo { get; set; }
            public List<BranchContainer> BranchList { get; set; }
            public Sum S;

            public void CalcSum()
            {
                S.Clear();

                foreach (var x in BranchList)
                {
                    x.CalcSum();
                    S.Add(x.S);
                }
            }

            internal void AddBranch(BranchContainer y)
            {
                BranchList.Add(y);
            }

            internal void Print(PrintHelper ph)
            {
                ph.Print(this);
                foreach (var x in BranchList)
                    x.Print(ph);
            }

            internal int GetRowCount()
            {
                int n = Fedo.Special == 1 ? 0 : 6;

                foreach (var x in BranchList)
                    n += x.GetRowCount();

                return n;
            }

            internal bool IsDataIncomplete()
            {
                if (BranchList == null || BranchList.Count == 0)
                    return true;

                foreach (var x in BranchList)
                    if (x.IsDataIncomplete())
                        return true;

                return false;
            }

            internal List<PFRBranch> GetDataIncompleteRegions()
            {
                List<PFRBranch> res = new List<PFRBranch>();

                foreach (var x in BranchList)
                    if (x.IsDataIncomplete())
                        res.Add(x.Branch);

                return res;
            }
        }

        class BranchContainer
        {
            public BranchContainer()
            {
                NpfList = new List<NpfContainer>();
                S = new Sum();
                Data_Cre = new List<ReportDataContainer>();
                Data_Emp = new List<ReportDataContainer>();
                Data_Com = new List<ReportDataContainer>();
            }

            public PFRBranch Branch { get; set; }
            public List<NpfContainer> NpfList { get; set; }
            public Sum S;
            //public PfrBranchReport Report;
            public List<ReportDataContainer> Data_Cre;
            public List<ReportDataContainer> Data_Emp;
            public List<ReportDataContainer> Data_Com;


            public void CalcSum()
            {
                S.Clear();

                if (NpfList == null)
                    return;

                foreach (var x in NpfList)
                {
                    x.CalcSum();
                    S.Add(x.S);
                }

                foreach (var x in Data_Cre)
                    S.AddCre(x.Data);
                foreach (var x in Data_Emp)
                    S.AddEmp(x.Data);
                foreach (var x in Data_Com)
                    S.AddCom(x.Data);

                if (Data_Cre.Count > 0)
                S.Sum_Cre_E = Data_Cre[0].Data.Existing;
                if (Data_Emp.Count > 0)
                S.Sum_Emp_E = Data_Emp[0].Data.Existing;
                if (Data_Com.Count > 0)
                S.Sum_Com_E = Data_Com[0].Data.Existing;
            }

            internal void AddItem(NpfContainer r)
            {
                NpfList.Add(r);
            }

            internal void Print(PrintHelper ph)
            {
                ph.Print(this);

                //foreach (var x in NpfList)
                //    x.Print(ph);
            }

            public int GetRowCount()
            {
                return 5 + NpfList.Count;
            }

            internal bool IsDataIncomplete()
            {
                if (NpfList.Count == 0)
                    return true;

                foreach (var x in NpfList)
                    if (x.IsDataIncomplete())
                        return true;

                return false;
            }

            internal void AddReportData(ReportDataContainer x, LegalEntity npf, bool isAnnual)
            {
                switch (x.Data.DataType)
                {
                    case PfrBranchReportCertificationAgreement.enType.Npf:
                        var y = AddNpf(npf, isAnnual);
                        y.DataList.Add(x);
                        break;

                    case PfrBranchReportCertificationAgreement.enType.Credit:
                        Data_Cre.Add(x);
                        break;
                    case PfrBranchReportCertificationAgreement.enType.Employer:
                        Data_Emp.Add(x);
                        break;
                    case PfrBranchReportCertificationAgreement.enType.Communication:
                        Data_Com.Add(x);
                        break;
                }
            }

            internal NpfContainer AddNpf(LegalEntity n, bool isAnnual)
            {
                NpfContainer nc = NpfList.FirstOrDefault(x => x.Npf == n);
                if (nc == null)
                {
                    nc = new NpfContainer { Npf = n, IsAnnual = isAnnual };
                    NpfList.Add(nc);
                }

                return nc;
            }
        }

        class NpfContainer
        {
            public bool IsAnnual;


            public NpfContainer()
            {
                DataList = new List<ReportDataContainer>();
                S = new Sum();
            }

            public LegalEntity Npf;
            public List<ReportDataContainer> DataList;
            public Sum S;


            internal bool IsDataIncomplete()
            {
                if (Npf == null)
                    return true;

                if (DataList.Count != (IsAnnual ? 12 : 1))
                    return true;

                foreach (var x in DataList)
                    if (x.IsDataIncomplete())
                        return true;

                return false;
            }

            //internal void Print(PrintHelper ph)
            //{
            //    throw new NotImplementedException();
            //}

            internal void AddReportData(ReportDataContainer x)
            {
                DataList.Add(x);
            }

            internal void CalcSum()
            {
                S.Clear();

                foreach (var x in DataList)
                {
                    S.AddNpf(x.Data);
                }

                if (DataList.Count > 0)
                    S.Sum_NPF_E = DataList[0].Data.Existing;
            }
        }

        class ReportDataContainer
        {
            public PfrBranchReport Report;
            public PfrBranchReportCertificationAgreement Data;

            internal bool IsDataIncomplete()
            {
                if (Report == null || Data == null)
                    return true;

                return false;
            }
        }
    }
}
