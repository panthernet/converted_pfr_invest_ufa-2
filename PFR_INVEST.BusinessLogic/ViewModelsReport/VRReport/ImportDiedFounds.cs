﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Office.Interop.Excel;
using PFR_INVEST.BusinessLogic.VRReport;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.SIReport
{
    public class ImportDiedFounds : ReportImportVRHandlerBase
    {
        private static List<string> header1Table = new List<string>
        { "Наименование инвестиционного портфеля", "Сумма СПН, руб.", "в т.ч., инвест. доход, руб.", "Количество ЗЛ, чел.",
                                                                         "в том числе, за счет:", null,null,null,null,null,null,null,null};

        private static List<string> header2Table = new List<string>
        { null, null, null, null, "ДСВ, руб.", "в т.ч., инвест. доход, руб.", "ДСВ, Количество ЗЛ, чел.", "ОПС, руб.", "в т.ч., инвест. доход, руб.", "ОПС, Количество ЗЛ, чел.", "МСК, руб.",
                                                                        "в т.ч., инвест. доход, руб.", "МСК, Количество ЗЛ, чел."};

        List<List<string>> _headerTable;
        protected override List<List<string>> HeaderTable
        {
            get
            {
                if (_headerTable == null)
                    _headerTable = new List<List<string>> { header1Table, header2Table };
                return _headerTable;
            }
        }

        SiRegisterTransferData mskRegData;
        SiRegisterTransferData opsRegData;
        SiRegisterTransferData dsvRegData;

        SiImportOperation mskDiedZlOperation = new SiImportOperation("МСК умерших ЗЛ");
        SiImportOperation opsDiedZlOperation = new SiImportOperation("ОПС умерших ЗЛ");
        SiImportOperation dsvDiedZlOperation = new SiImportOperation("ДСВ умерших ЗЛ");

        protected override SiImportOperation Operation => dsvDiedZlOperation;

        protected override int RowDataIterator => 5;

        protected override string EndDataColumn => "M";

        protected override bool SetDataImport(Worksheet worksheet, out string message)
        {
            //dsv operation
            //this.ImportData = new SiRegisterTransferData(this.SelectedMonth.ID, this.SelectedYear.ID, this.Operation.DirectionId, this.Operation.OperationId);
            dsvRegData = new SiRegisterTransferData(SelectedMonth.ID, SelectedYear.ID, Operation.DirectionId, dsvDiedZlOperation.OperationId);
            mskRegData = new SiRegisterTransferData(SelectedMonth.ID, SelectedYear.ID, Operation.DirectionId, mskDiedZlOperation.OperationId);
            opsRegData = new SiRegisterTransferData(SelectedMonth.ID, SelectedYear.ID, Operation.DirectionId, opsDiedZlOperation.OperationId);

            return base.SetDataImport(worksheet, out message);
        }

        protected override List<SIRegister> GetSiRegistersList()
        {
            return SIRegisters.Where(
                                    sir => sir.CompanyMonthID == SelectedMonth.ID &&
                                        sir.CompanyYearID == SelectedYear.ID &&
                                        (sir.OperationID == mskDiedZlOperation.OperationId ||
                                            sir.OperationID == opsDiedZlOperation.OperationId ||
                                            sir.OperationID == dsvDiedZlOperation.OperationId)
                                        )
                                           .ToList();
        }

        protected override String ImportRegistersData()
        {
            String msg = "";
            long resaltDSV = dsvRegData.ExecuteSave();
            long resaltOPS = opsRegData.ExecuteSave();
            long resaltMSK = mskRegData.ExecuteSave();

            if (resaltDSV != 0)
                msg += string.Format("№ {0} по операции «{1}»\n", resaltDSV, dsvDiedZlOperation.OperationName);

            if (resaltOPS != 0)
                msg += string.Format("№ {0} по операции «{1}»\n", resaltOPS, opsDiedZlOperation.OperationName);

            if (resaltMSK != 0)
                msg += string.Format("№ {0} по операции «{1}»\n", resaltMSK, mskDiedZlOperation.OperationName);

            dsvRegData = new SiRegisterTransferData();
            mskRegData = new SiRegisterTransferData();
            opsRegData = new SiRegisterTransferData();

            return msg;
        }

        protected override List<SiRegisterTransferData> GetDataToImport()
        {
            return new List<SiRegisterTransferData> { dsvRegData, opsRegData, mskRegData };
        }

        protected override bool SetTransfer(int row, object[,] obj, out string message)
        {
            try
            {
                String docType = GetDocumnetType(obj[1, 1].ToString());
                List<Contract> contracts = BLServiceSystem.Client.GetGukConractsByContractName(docType);
                if (contracts.Count > 1)
                    throw new Exception(String.Format("Для «{0}» - договор типа «{1}» найдено больше 1 контракта.\nНайденные номера контрактов:{2}",
                                                        obj[1, 1], docType,
                                                        String.Join(",", contracts.Select(item => item.ContractNumber).ToArray())));               

                //dsv
                long dsvZlcount = 0;
                if (obj[1, 7] == null || !long.TryParse(obj[1, 7].ToString(), out dsvZlcount))
                    throw new Exception(String.Format("Неверное значение для столбца 7 'ДСВ, Количество ЗЛ, чел.', предпологалось целое число. Текущее значение = {0}, строка = {1}", obj[1, 7], row));

                decimal dsvSum = 0;
                if (obj[1, 5] == null || !decimal.TryParse(obj[1, 5].ToString(), out dsvSum))
                    throw new Exception(String.Format("Неверное значение для столбца 5 'ДСВ, руб', предпологалось число. Текущее значение = {0}, строка = {1}", obj[1, 5], row));

                decimal dsvInvestDohod = 0;
                if (obj[1, 6] != null && !decimal.TryParse(obj[1, 6].ToString(), out dsvInvestDohod))
                    throw new Exception(String.Format("Неверное значение для столбца 6 'в т.ч., инвест. доход, руб.', предпологалось число. Текущее значение = {0}, строка = {1}", obj[1, 6], row));
                //ops
                long opsZlcount = 0;
                if (obj[1, 10] == null || !long.TryParse(obj[1, 10].ToString(), out opsZlcount))
                    throw new Exception(String.Format("Неверное значение для столбца 10 'ОПС, Количество ЗЛ, чел.', предпологалось целое число. Текущее значение = {0}, строка = {1}", obj[1, 10], row));

                decimal opsSum = 0;
                if (obj[1, 8] == null || !decimal.TryParse(obj[1, 8].ToString(), out opsSum))
                    throw new Exception(String.Format("Неверное значение для столбца 8 'ОПС, руб', предпологалось число. Текущее значение = {0}, строка = {1}", obj[1, 8], row));

                decimal opsInvestDohod = 0;
                if (obj[1, 9] != null && !decimal.TryParse(obj[1, 9].ToString(), out opsInvestDohod))
                    throw new Exception(String.Format("Неверное значение для столбца 9 'в т.ч., инвест. доход, руб.', предпологалось число. Текущее значение = {0}, строка = {1}", obj[1, 9], row));
                //msk

                long mskZlcount = 0;
                if (obj[1, 13] == null || !long.TryParse(obj[1, 13].ToString(), out mskZlcount))
                    throw new Exception(String.Format("Неверное значение для столбца 13 'МСК, Количество ЗЛ, чел.', предпологалось целое число. Текущее значение = {0}, строка = {1}", obj[1, 13], row));

                decimal mskSum = 0;
                if (obj[1, 11] != null && !decimal.TryParse(obj[1, 11].ToString(), out mskSum))
                    throw new Exception(String.Format("Неверное значение для столбца 11 'МСК, руб.', предпологалось число. Текущее значение = {0}, строка = {1}", obj[1, 11], row));

                decimal mskInvestDohod = 0;
                if (obj[1, 12] != null && !decimal.TryParse(obj[1, 12].ToString(), out mskInvestDohod))
                    throw new Exception(String.Format("Неверное значение для столбца 12 'в т.ч., инвест. доход, руб.', предпологалось число. Текущее значение = {0}, строка = {1}", obj[1, 12], row));


                if (contracts.Count > 0)
                {
                    contracts.ForEach(item =>
                    {
                        //dsv
                        SITransfer siTransfer = new SITransfer
                        {
                            ContractID = item.ID,
                            InsuredPersonsCount = dsvZlcount,
                            StatusID = 1
                        };

                        ReqTransfer reqTransfer = new ReqTransfer
                        {
                            DIDate = DateTime.Now.Date,
                            Sum = dsvSum,
                            TransferStatus = Status,
                            InvestmentIncome = dsvInvestDohod,
                            StatusID = 1
                        };

                        dsvRegData.siTransferList.Add(new SiRegisterTransferData.SITransferData
                            {
                            siTransfer = siTransfer,
                            reqTransferList = new List<ReqTransfer> { reqTransfer }
                        }
                        );

                        //ops
                        siTransfer = new SITransfer
                        {
                            ContractID = item.ID,
                            InsuredPersonsCount = opsZlcount,
                            StatusID = 1
                        };

                        reqTransfer = new ReqTransfer
                        {
                            DIDate = DateTime.Now.Date,
                            Sum = opsSum,
                            TransferStatus = Status,
                            InvestmentIncome = opsInvestDohod,
                            StatusID = 1
                        };

                        opsRegData.siTransferList.Add(new SiRegisterTransferData.SITransferData
                            {
                            siTransfer = siTransfer,
                            reqTransferList = new List<ReqTransfer> { reqTransfer }
                        }
                        );
                        //msk
                        if (mskSum > 0)
                        {
                            siTransfer = new SITransfer
                            {
                                ContractID = item.ID,
                                InsuredPersonsCount = mskZlcount,
                                StatusID = 1
                            };

                            reqTransfer = new ReqTransfer
                            {
                                DIDate = DateTime.Now.Date,
                                Sum = mskSum,
                                TransferStatus = Status,
                                InvestmentIncome = mskInvestDohod,
                                StatusID = 1
                            };

                            mskRegData.siTransferList.Add(new SiRegisterTransferData.SITransferData
                                {
                                siTransfer = siTransfer,
                                reqTransferList = new List<ReqTransfer> { reqTransfer }
                            }
                            );
                        }

                    });
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }

            message = null;
            return true;
        }

        public override string ReportType => "04";

        public override string OpenFileMask => string.Format("Отзыв из ГУК ВР СПН умерших ЗЛ(.xls, .xlsx)|{0}", "*.xls;*.xlsx");
    }
}