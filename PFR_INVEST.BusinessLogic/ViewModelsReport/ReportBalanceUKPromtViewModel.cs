﻿using System;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Reports;

namespace PFR_INVEST.BusinessLogic.ViewModelsReport
{
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_viewer, DOKIP_ROLE_TYPE.OKIP_viewer, DOKIP_ROLE_TYPE.OARRS_viewer, DOKIP_ROLE_TYPE.OFPR_viewer, DOKIP_ROLE_TYPE.OUFV_viewer, DOKIP_ROLE_TYPE.OSRP_viewer, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_manager)]
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_manager)]
    public class ReportBalanceUKPromtViewModel : ViewModelCardDialog
	{
		private DateTime? _reportDate;
		public DateTime? ReportDate { get { return _reportDate; } set { _reportDate = value; OnPropertyChanged("ReportDate"); } }
			
		public ICommand RunReportCommand { get; private set; }

		public ReportBalanceUKPromtViewModel()
		{
			RunReportCommand = new DelegateCommand(o => IsValid(), o => { });			

		}

		public override bool CanExecuteSave()
		{
			return IsValid();
		}

		public bool IsValid()
		{
		    return "ReportDate".Split('|').All(f => string.IsNullOrEmpty(this[f]));
		}

	    public override string this[string columnName]
		{
			get
			{
				switch (columnName)
				{
					case "ReportDate": return ReportDate.ValidateRequired() ;
				}
				return null;
			}
		}


		public ReportBalanceUKPrompt GetReportPrompt()
		{
			if (!IsValid())
				return null;
			var prompt = new ReportBalanceUKPrompt
			{
				Date = ReportDate.Value
			};

			return prompt;
		}
	}
}
