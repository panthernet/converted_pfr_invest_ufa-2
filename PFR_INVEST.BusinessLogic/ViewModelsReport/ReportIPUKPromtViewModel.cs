﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Reports;

namespace PFR_INVEST.BusinessLogic.ViewModelsReport
{
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_viewer, DOKIP_ROLE_TYPE.OKIP_viewer, DOKIP_ROLE_TYPE.OARRS_viewer, DOKIP_ROLE_TYPE.OFPR_viewer, DOKIP_ROLE_TYPE.OUFV_viewer, DOKIP_ROLE_TYPE.OSRP_viewer, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_manager)]
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_manager)]
	public class ReportIPUKPromtViewModel : ViewModelCardDialog
	{
		public Document.Types Type { get; private set; }
		private readonly List<Contract> _allContract;
		private long? _ukId;
		public long? UKID
		{
			get { return _ukId; }
			set
			{
				if (_ukId == value) return;
				_ukId = value;
				ContractList = _allContract
								.Where(c => c.LegalEntityID == UKID)
								.OrderBy(c => c.ContractNumber).ToList();
				//ContractList = BLServiceSystem.Client.GetContractsListForLegalEntityHibByContractType(UKID ?? 0, Document.Types.SI).OrderBy(c => c.ContractNumber).ToList();
				if (ContractList.Any())
					ContractID = ContractList.First().ID;
				OnPropertyChanged("UKID");
			}
		}

		private long? contractID;
		public long? ContractID { get { return contractID; } set { contractID = value; OnPropertyChanged("ContractID"); } }

		private DateTime? dateFrom;
		public DateTime? DateFrom { get { return dateFrom; } set { dateFrom = value; OnPropertyChanged("DateTo"); OnPropertyChanged("DateFrom"); } }

		private DateTime? dateTo;
		public DateTime? DateTo { get { return dateTo; } set { dateTo = value; OnPropertyChanged("DateTo"); OnPropertyChanged("DateFrom"); } }

		public List<LegalEntity> UKList { get; private set; }

		private List<Contract> contractList = new List<Contract>();
		public List<Contract> ContractList { get { return contractList; } private set { contractList = value; OnPropertyChanged("ContractList"); } }

		public ICommand RunReportCommand { get; private set; }

		public bool IsTotalReport { get; private set; }
		public string ReportName { get; private set; }


		public ReportIPUKPromtViewModel(bool isTotal = false, Document.Types type = Document.Types.SI)
		{
			Type = type;
			RunReportCommand = new DelegateCommand(o => IsValid(), o => { });
			if (!isTotal)
			{
				_allContract = BLServiceSystem.Client.GetContractsForUKs().Where(c => c.TypeID == (int)Type).ToList();
				//Фильтруем УК. Показываем только УК для которых есть договора
				var IDs = _allContract.Select(c => c.LegalEntityID).Distinct();
				UKList = BLServiceSystem.Client.GetUKListHib().Where(le => IDs.Contains(le.ID)).OrderBy(le => le.FormalizedName).ToList();
				if (UKList.Any())
					UKID = UKList.First().ID;
			}

			ReportName = isTotal ? "Параметры отчета Журнал ИП совокупный" : "Параметры отчёта Журнал ИП УК";

			DateFrom = new DateTime(DateTime.Today.Year - 1, 1, 1);
			DateTo = new DateTime(DateTime.Today.Year, 1, 1);
			IsTotalReport = isTotal;
			//OnPropertyChanged("IsTotalReport");

		}

		public override bool CanExecuteSave() { return IsValid(); }

		public bool IsValid()
		{
			return ("DateFrom|DateTo" + (IsTotalReport ? null : "|ContractID|UKID")).Split('|').All(f => string.IsNullOrEmpty(this[f]));
		}

		public override string this[string columnName]
		{
			get
			{
				switch (columnName)
				{
					case "DateFrom": return DateFrom.ValidateRequired() ?? DateFrom.ValidatePeriodStart(DateTo);
					case "DateTo": return DateTo.ValidateRequired() ?? DateTo.ValidatePeriodEnd(DateFrom);
					case "ContractID": return ContractID.ValidateRequired();
					case "UKID": return UKID.ValidateRequired();
				}
				return null;
			}
		}


		public ReportIPUKPrompt GetReportPrompt()
		{
			if (!IsValid())
				return null;
			var prompt = new ReportIPUKPrompt
			{
				ContractID = ContractID ?? 0,
				DateFrom = DateFrom,
				DateTo = DateTo
			};

			return prompt;
		}
	}
}
