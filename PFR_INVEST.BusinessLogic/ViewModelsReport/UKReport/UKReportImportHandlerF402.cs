﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.Common.Logger;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataAccess.Client.ObjectsExtensions;
using PFR_INVEST.DataAccess.Server.UKReport;
using PFR_INVEST.DataObjects;
using PFR_INVEST.XmlSchemas;

namespace PFR_INVEST.BusinessLogic.UKReport
{
    class UKReportImportHandlerF402 : UKReportImportHandlerBase
    {
        private readonly List<F401402UKStatus> _f401402UkStatuses;

        public UKReportImportHandlerF402()
        {
            Mask = "*F402.xml";
            Shema = "F402.xsd";
            _f401402UkStatuses = DataContainerFacade.GetList<F401402UKStatus>();
        }

        public override string GetOpenFileMask(bool b)
        {
            return $"Отчет F402 (.xml)|{Mask}";
        }

        public override ActionResult LoadFile(string fileName, bool isXbrl)
        {
            var edo = new EDO_ODKF402();
            if (string.IsNullOrEmpty(fileName))
            {
                return ActionResult.Error("Ошибка при выборе файла");
            }

            var fName = Path.GetFileName(fileName);
            if (Items.Any(i => i.FileName.ToLower().Equals(fName.ToLower())))
            {
                return ActionResult.Error($"Файл {fName} уже загружен");
            }

            var serializer = new XmlSerializer(typeof(EDO_ODKF402));
            var ci = Thread.CurrentThread.CurrentCulture;
            var ciUi = Thread.CurrentThread.CurrentUICulture;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("ru-RU");
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("ru-RU");

            try
            {
                using (var reader = XmlReader.Create(fileName, GetReaderSettings()))
                {
                    var doc = (EDO_ODKF402)serializer.Deserialize(reader);
                    doc.TrimAllStringProperties();
                    //удалим данные Всего
                    doc.Details = doc.Details.Where(d => !string.IsNullOrEmpty(d.DogPFRNum)).ToList();

                    if (doc.Details.Count == 0)
                    {
                        return ActionResult.Error($"Документ с регистрационным номером {doc.RegNumberOut}\nне содержит информации к требованию на оплату.");
                    }

                    if (Items.Any(i => string.Equals((i as ImportEdoOdkF401402).edoLog.DocRegNumberOut, doc.RegNumberOut, StringComparison.OrdinalIgnoreCase)))
                    {
                        return ActionResult.Error($"Документ с регистрационным номером {doc.RegNumberOut} уже загружен для импорта");
                    }

                    var Requirements = DataContainerFacade.GetList<EdoOdkF401402>();
                    Requirements = Requirements.Where(r =>DateUtil.IsDateEquals(r.FromDate,DateUtil.ParseImportDate(doc.FromDate)) && DateUtil.IsDateEquals(r.ToDate , DateUtil.ParseImportDate(doc.ToDate))).ToList();

                    if (Requirements == null || Requirements.Count == 0)
                    {
                        return ActionResult.Error(
                            $"Требования на оплату в период\nс {DateUtil.ParseImportDate(doc.FromDate).Value.ToString("dd.MM.yyyy")} по {DateUtil.ParseImportDate(doc.ToDate).Value.ToString("dd.MM.yyyy")}\n не найдены.");
                    }

                    var xdoc = XDocument.Load(fileName);
                    xdoc.Root.AddFirst(new XElement("FILENAME", fName));
                    
                    if (
                        // для 402 формы в таблицу данные не пишутся, проверку можно выполнить на основе данных таблицы EDO_LOG
                        //Requirements.Any(r => string.Equals(r.RegNumberOut, doc.RegNumberOut, StringComparison.OrdinalIgnoreCase)))
                        DataContainerFacade.GetListByProperty<EDOLog>("DocRegNumberOut", doc.RegNumberOut).Any(x => x.DocForm == xdoc.Root.Name.ToString())) 
                    {
                        return ActionResult.Error($"Документ с регистрационным номером {doc.RegNumberOut} уже был импортирован");
                    }

                    var importEdoOdkF401402 = new ImportEdoOdkF401402
                    {
                        xmlBody = xdoc.ToString(),
                        F401402 = new EdoOdkF401402
                        {
                            ReportDate = DateUtil.ParseImportDate(doc.ReportDate),
                            RegDate = DateTime.Now.Date,
                            AuthorizedPerson = doc.AuthorizedPerson,
                            Executor = doc.Executor,
                            FromDate = DateUtil.ParseImportDate(doc.FromDate),
                            ToDate = DateUtil.ParseImportDate(doc.ToDate),
                            RegNumberOut = doc.RegNumberOut
                        }
                    };

                    var rewriteList = new List<string>();
                    foreach (var info in doc.Details)
                    {
                        var contract = GetContractByDogovorNum(info.DogPFRNum);

                        if (contract == null)
                        {
                            return ActionResult.Error($"Договор с регистрационным номером {info.DogPFRNum} не найден");
                        }

                        var reqs = Requirements.Where(r => r.GetDetailList().Any(det => det.ContractId == contract.ID)).ToList();

                        if (reqs == null || reqs.Count == 0)
                        {
                            return ActionResult.Error(
                                $"Требование на оплату в период\nс {DateUtil.ParseImportDate(doc.FromDate).Value:dd.MM.yyyy} по {DateUtil.ParseImportDate(doc.ToDate).Value:dd.MM.yyyy}\nдля  договора с регистрационным номером {info.DogPFRNum}\nне найден.");

                        }
                        else if (reqs.Count > 1)
                        {
                            return ActionResult.Error(
                                $"Требований на оплату в период\nс {DateUtil.ParseImportDate(doc.FromDate).Value:dd.MM.yyyy} по {DateUtil.ParseImportDate(doc.ToDate)?.ToString("dd.MM.yyyy")}\nдля  договора с регистрационным номером {info.DogPFRNum}\nбольше одного.");
                        }

                        var detail = reqs.SingleOrDefault().GetDetailList().FirstOrDefault(d => d.ContractId == contract.ID);

                        // определяем по полю статуса, был ли выполнен импорт ранее, если нет - сохраняем состояние
                        if (detail.InitStatusID == null)
                        {
                            detail.InitControlFactDate = detail.ControlFactDate;
                            detail.InitStatusID = detail.StatusID;
                            detail.InitOutgoingDocNumber = detail.OutgoingDocNumber;
                            detail.InitOutgoingDocDate = detail.OutgoingDocDate;
                        }
                        else
                        {
                            rewriteList.Add(info.DogPFRNum);
                        }

                        detail.ControlFactDate = DateUtil.ParseImportDate(info.PayDate);
                        if (detail.ControlSetDate.HasValue && detail.ControlFactDate.HasValue)
                        {
                            SetStatus(detail.ControlFactDate.Value <= detail.ControlSetDate.Value ? F402Status.PaidInTime : F402Status.PaidTimeExceeded, detail);
                        }
                        detail.OutgoingDocNumber = doc.RegNumberOut;
                        detail.OutgoingDocDate = DateUtil.ParseImportDate(doc.ReportDate);
                        importEdoOdkF401402.F401402UK.Add(detail);
                    }

                    if (rewriteList.Any() && 
                        !ViewModelBase.DialogHelper.ShowConfirmation(
                            $"Для требований на оплату в период \nс {DateUtil.ParseImportDate(doc.FromDate).Value:dd.MM.yyyy} по {DateUtil.ParseImportDate(doc.ToDate).Value:dd.MM.yyyy}\nдля договоров с регистрационными номерами {rewriteList.Aggregate((x, y) => x + ", " + y)}\n уже были загружены отчеты F402.\n Выполнить повторную загрузку?"))
                    {
                        return ActionResult.Success();
                    }

                    var rd = DateUtil.ParseImportDate(doc.ReportDate);
                    importEdoOdkF401402.edoLog = new EDOLog
                    {
                        DocTable = xdoc.Root.Name.ToString(),
                        DocForm = xdoc.Root.Name.ToString(),
                        Filename = fName,
                        DocRegNumberOut = doc.RegNumberOut,
                        RegDate = DateTime.Now.Date,
                        DocId = Requirements.First().ID
                    };


                    importEdoOdkF401402.FileName = Path.GetFileName(fileName);
                    importEdoOdkF401402.PathName = Path.GetDirectoryName(fileName);
                    importEdoOdkF401402.ReportOnDate = rd.HasValue? rd.Value.ToString("dd.MM.yyyy"):string.Empty;
                    importEdoOdkF401402.ReportOnDateNative = rd;

                    Items.Add(importEdoOdkF401402);
                }

            }
            catch (IOException ex)
            {
                Logger.Instance.Error("Open file exception", ex);
                return new ActionResult { ErrorMessage = "Ошибка доступа к документу. Убедитесь, что документ не открыт в другом приложении и пользователь имеет права доступа к документу.", IsSuccess = false };
            }
            catch (InvalidOperationException ex)
            {
                if (ex.InnerException is XmlSchemaValidationException)
                {
                    if (ex.InnerException.InnerException == null)
                    {
                        ViewModelBase.DialogHelper.ShowAlert(
                            $"Неверный формат документа: {ex.Message}\n\nПодробности - {ex.InnerException.Message}");
                    }
                    else
                    {
                        ViewModelBase.DialogHelper.ShowAlert(
                            $"Неверный формат документа: {ex.Message}\n\nОписание - {ex.InnerException.InnerException.Message}\n\nПодробности - {ex.InnerException.Message}");
                    }

                }

                else
                {
                    ViewModelBase.DialogHelper.ShowAlert("Неверный формат документа");
                    ViewModelBase.Logger.WriteException(ex);
                }

            }
            catch (Exception ex)
            {
                ViewModelBase.DialogHelper.ShowAlert("Ошибка открытия документа");
                ViewModelBase.Logger.WriteException(ex);
            }
            finally
            {
                Thread.CurrentThread.CurrentUICulture = ciUi;
                Thread.CurrentThread.CurrentCulture = ci;
            }

            return ActionResult.Success();
        }

        private void SetStatus(F402Status s, F401402UK detail)
        {
            var status = _f401402UkStatuses.FirstOrDefault(x => x.ID == (long)s);
            if (status != null)
            {
                detail.StatusID = status.ID;
                detail.StatusName = status.Name;
            }
        }

        public override ActionResult Import()
        {
            var ci = Thread.CurrentThread.CurrentCulture;
            var ciUi = Thread.CurrentThread.CurrentUICulture;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("ru-RU");
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("ru-RU");
            var actionResult = ActionResult.Success();
            try
            {

                foreach (ImportEdoOdkF401402 import in Items)
                {
                    actionResult = BLServiceSystem.Client.ImportUKF402(import.F401402UK, import.edoLog, import.xmlBody);
                }

            }
            catch (Exception ex)
            {

                ViewModelBase.Logger.WriteException(ex);
                return ActionResult.Error(ex.Message);
            }
            finally
            {
                Thread.CurrentThread.CurrentUICulture = ciUi;
                Thread.CurrentThread.CurrentCulture = ci;

            }
            return actionResult;
        }

        public override void RefreshViewModels()
        {
            ViewModelBase.ViewModelManager.RefreshViewModels(typeof(F401402UKListViewModel), typeof(LoadedODKLogListSIViewModel));
            if (GetViewModelsList != null)
            {
                var targetModels = GetViewModelsList(typeof(F401402UKViewModel));
                var contractList = new List<long>();
                foreach (var ids in Items.Select(x => (x as ImportEdoOdkF401402).F401402UK.Select(y => y.ID)))
                {
                    contractList.AddRange(ids);
                }
                targetModels.ForEach(x =>
                {
                    if (contractList.Any(y => y == x.ID))
                    {
                        (x as F401402UKViewModel).RefreshModel();
                    }
                });
            }
        }

        private DateTime? GetDateTime(string date)
        {
            DateTime dt;
            if (DateTime.TryParse(date, out dt))
            {
                return dt;
            }
            return null;

        }
    }
}
