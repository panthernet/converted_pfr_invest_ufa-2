﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataAccess.Server.UKReport;
using PFR_INVEST.DataObjects;
using PFR_INVEST.XmlSchemas;

namespace PFR_INVEST.BusinessLogic.UKReport
{
    internal class UKReportImportHandlerF140 : UKReportImportHandlerBase
    {
        private readonly List<CategoryF140> _categories;

        private readonly Element _statusF140Default;

        private readonly List<Element> _statusF140;

        public UKReportImportHandlerF140()
        {
            Mask = "*F140.xml;*F141.xml";
            Shema = "F140.xsd";
            _categories = DataContainerFacade.GetList<CategoryF140>();
            _statusF140 = BLServiceSystem.Client.GetElementByType(Element.Types.StatusF140);
            _statusF140Default = _statusF140.FirstOrDefault();
        }

        public override string GetOpenFileMask(bool b)
        {
            return $"Отчет F140, F141 (.xml)|{Mask}";
        }

        public override ActionResult LoadFile(string fileName, bool isXbrl)
        {
            //var edo = new EDO_ODKF140();
            if (string.IsNullOrEmpty(fileName))
            {
                return ActionResult.Error("Ошибка при выборе файла");
            }

            var fName = Path.GetFileName(fileName);
            if (Items.Any(i => i.FileName.ToLower().Equals(fName.ToLower())))
            {
                return ActionResult.Error($"Файл {fName} уже загружен");
            }

            var serializer = new XmlSerializer(typeof(EDO_ODKF140));
            var ci = Thread.CurrentThread.CurrentCulture;
            var ciUi = Thread.CurrentThread.CurrentUICulture;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("ru-RU");
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("ru-RU");

            try
            {
                using (var reader = XmlReader.Create(fileName, GetReaderSettings()))
                {
                    EDO_ODKF140 doc = null;
                    try
                    {
                        doc = (EDO_ODKF140) serializer.Deserialize(reader);
                    }
                    catch (Exception ex)
                    {
                        if (ex.InnerException?.Message.Contains("EDO_ODKF141") ?? false)
                        {
                            var serializer141 = new XmlSerializer(typeof(EDO_ODKF141));
                            using (var reader141 = XmlReader.Create(fileName))
                            {
                                var doc141 = (EDO_ODKF141) serializer141.Deserialize(reader141);
                                doc = new EDO_ODKF140();
                                doc.SpecDepTreat = doc141.SpecDepTreat;
                                //doc.StopDate = doc141.StopDate;
                                doc.RealStopDate = doc141.RealStopDate;
                                doc.DateOfReport = doc141.DateOfReport;
                                doc.OperatorPost = doc141.OperatorPost;
                                doc.OperatorFIO = doc141.OperatorFIO;
                                doc.StopDateDay = doc141.StopDateDay;
                                doc.ContentReport = doc141.NPFViolationContent;
                                //doc.DogovorNumber = doc141.DogovorNumber;
                                //doc.DogovorDate = doc141.DogovorDate;
                                doc.IDReportType = 1;
                                doc.LicenseNFNPF = doc141.LicenseNPF;
                                doc.CodeInFirm1 = doc141.Code1;
                                doc.CodeInFirm2 = doc141.Code2;
                                //!doc.CodeInFirm3 = doc141.Code3;
                                doc.UKName = doc141.UKName;
                                //doc.UKINN = doc141.UKINN;
                                doc.UKFilial = "00";
                                doc.TheDate = doc141.TheDate;
                                doc.DocNumber = doc141.DocNumber;
                                doc.BaseDocNumber = doc141.BaseDocNumber;
                                doc.BaseDocDate = doc141.BaseDocDate;
                                doc.InvestcaseName = doc141.InvestcaseName;
                                doc.ViolationDate = doc141.ViolationDate;
                                doc.ViolationType = doc141.ViolationBody;
                                doc.ViolationNorma = doc141.ViolationNormal;
                                doc.ViolationFact = doc141.ViolationFact;
                                doc.RegNumberOut = doc141.RegNumberOut;
                                doc.VERSION = doc141.VERSION;
                            }
                        }
                        else throw;
                    }

                    doc.TrimAllStringProperties();
                    if (
                        Items.Any(i => (i as ImportEdoOdkF140).F140.DocNumber == doc.DogovorNumber
                                            && (i as ImportEdoOdkF140).F140.DateOfReport == DateUtil.ParseImportDate(doc.DateOfReport)))
                    {
                        return
                            ActionResult.Error(
                                $"Отчет от {doc.DateOfReport} с  номером договора {doc.DogovorNumber}\nуже загружен для импорта");
                    }

                    var contract = GetContractByDogovorNum(doc.DogovorNumber);
                    if (contract == null)
                    {
                        return
                            ActionResult.Error(
                                $"Договор с регистрационым номером {doc.DogovorNumber} не найден");
                    }

                    if (doc.IDReportType > 5)
                    {
                        return
                            ActionResult.Error(
                                $"Импорт невозможен!\nНеизвестный тип отчета.\nТип отчета {doc.IDReportType} - неопределен. ");
                    }

                    var xdoc = XDocument.Load(fileName);
                    xdoc.Root.AddFirst(new XElement("FILENAME", fName));

                    if (string.IsNullOrEmpty(doc.CodeInFirm1) || doc.CodeInFirm1 == "0" || doc.CodeInFirm1 == "00")
                        doc.CodeInFirm1 = "99"; //прочие

                    if (string.IsNullOrEmpty(doc.CodeInFirm2) || doc.CodeInFirm2 == "0" || doc.CodeInFirm2 == "00" || doc.CodeInFirm2 == "000")
                        doc.CodeInFirm2 = "999"; //прочие

                    var category =
                        _categories.SingleOrDefault(
                            c =>
                            c.ViolationCharacterCode.Equals(doc.CodeInFirm2)
                            && c.ViolationTypeCode.Equals(doc.CodeInFirm1));

                    if (category == null)
                    {
                        //делаем дефолтувую запись в БД для прочие/прочие если таковой еще нет
                        if (doc.CodeInFirm1 == "99" && doc.CodeInFirm2 == "999" && _categories.SingleOrDefault(
                                c =>
                                    c.ViolationCharacterCode.Equals(doc.CodeInFirm2)
                                    && c.ViolationTypeCode.Equals(doc.CodeInFirm1)) == null)
                        {
                            var cId = DataContainerFacade.Save(new CategoryF140
                            {
                                ViolationCharacter = "прочие",
                                ViolationCharacterCode = "999",
                                ViolationType = "прочие",
                                ViolationTypeCode = "99"
                            });
                            category = DataContainerFacade.GetByID<CategoryF140>(cId);
                            _categories.Add(category);
                        }
                        else
                        {
                            ViewModelBase.DialogHelper.AddViolationCategory();
                            return
                                ActionResult.Error(
                                    $"Категория нарушения с кодом вида нарушения - {doc.CodeInFirm1}\n и кодом характера нарушения - {doc.CodeInFirm2} не найдена.\nДобавьте категорию нарушения и повторите импорт.");
                        }
                    }

                    var theDate = string.IsNullOrEmpty(doc.TheDate) ? null : (DateTime?)GetDateTime(doc.TheDate).Value;
                    var edoOdkF140 = BLServiceSystem.Client.GetEdoOdkF140ByReportData(
                        contract.ID, category.ID, theDate);

                    var importEdoOdkF140 = new ImportEdoOdkF140 { F140 = new EdoOdkF140() };
                    //логика
                    if (edoOdkF140 == null)
                    {
                        if (doc.IDReportType == (int)EdoOdkF140.IDReportType.Report
                            || doc.IDReportType == (int)EdoOdkF140.IDReportType.DivergenceReport)
                        {
                            importEdoOdkF140.F140 = GetF140New(doc, contract, category);
                        }
                        else
                        {
                            return ActionResult.Error(
                                $"Импорт невозможен. Так как для\n\"{EdoOdkF140.GetReportType(Convert.ToInt32(doc.IDReportType))}\" \nотсутствует уведомление о возникновении нарушения.");
                        }
                    }
                    else
                    {
                        if (!edoOdkF140.ReportTypeID.HasValue)
                        {
                            return ActionResult.Error(
                                $"Импорт невозможен! Ошибка типа отчета в существующей записи о нарушении\n с кодом вида нарушения - {doc.CodeInFirm1}, кодом характера нарушения - {doc.CodeInFirm2}\nи типом {doc.IDReportType} за {(edoOdkF140.TheDate.HasValue ? edoOdkF140.TheDate.Value.ToString("dd.MM.yyyy") : "\"период\"")} для договора № {contract.ContractNumber}.");
                        }

                        var valid = isValidReportType((int)edoOdkF140.ReportTypeID.Value, Convert.ToInt32(doc.IDReportType));
                        switch (valid)
                        {
                            case 1:
                                if (
                               ViewModelBase.DialogHelper.ShowConfirmation(
                                   $"Отчет о нарушении с кодом вида нарушения - {doc.CodeInFirm1}\n и кодом характера нарушения - {doc.CodeInFirm2}\nи типом {doc.IDReportType} за {(edoOdkF140.TheDate.HasValue ? edoOdkF140.TheDate.Value.ToString("dd.MM.yyyy") : "\"период\"")} для договора № {contract.ContractNumber} уже существует.\nПерезаписать данные?"))
                                {

                                    importEdoOdkF140.F140 = GetF140New(doc, contract, category);
                                    importEdoOdkF140.F140.ID = edoOdkF140.ID;
                                    importEdoOdkF140.F140.LinkReport = edoOdkF140.LinkReport;
                                    importEdoOdkF140.F140.LinkNoStopReport = edoOdkF140.LinkNoStopReport;
                                    importEdoOdkF140.F140.LinkStopReport = edoOdkF140.LinkStopReport;
                                    importEdoOdkF140.F140.StopNumber = edoOdkF140.StopNumber;
                                    importEdoOdkF140.F140.StopReport = edoOdkF140.StopReport;
                                    importEdoOdkF140.F140.NoStopNumber = edoOdkF140.NoStopNumber;
                                    importEdoOdkF140.F140.NoStopDate = edoOdkF140.NoStopDate;
                                }
                                else
                                {
                                    return ActionResult.Success();
                                }
                                break;
                            case 2:
                                if (ViewModelBase.DialogHelper.ShowConfirmation(
                                    $"Отчет о нарушении с кодом вида нарушения - {doc.CodeInFirm1}\n и кодом характера нарушения - {doc.CodeInFirm2}\nи типом {doc.IDReportType} за {(edoOdkF140.TheDate.HasValue ? edoOdkF140.TheDate.Value.ToString("dd.MM.yyyy") : "\"период\"")} для договора № {contract.ContractNumber} уже существует.\nПерезаписать данные?")
                                )
                                {
                                    if (string.IsNullOrWhiteSpace(doc.StopDate) || string.IsNullOrWhiteSpace(doc.RealStopDate))
                                    {
                                        if (ViewModelBase.DialogHelper.ShowConfirmation(
                                            "\"Срок установленный ФКЦБ России для устранения нарушения\" или \"Дата фактического устранения нарушения управляющей компанией\" в загружаемом файле пустые! Действительно переписать существующие даты пустыми значениями?")
                                        )
                                        {
                                            importEdoOdkF140.F140 = GetF140Rewrite(edoOdkF140, doc, contract, category);
                                        }
                                        else
                                        {
                                            return ActionResult.Error("Процесс импорта прерван по инициативе пользователя");
                                        }
                                    }
                                    else
                                    {
                                        importEdoOdkF140.F140 = GetF140Rewrite(edoOdkF140, doc, contract, category);
                                    }
                                }
                                else
                                {
                                    return ActionResult.Success();
                                }

                                break;
                            case 3:
                                if (string.IsNullOrWhiteSpace(doc.StopDate) || string.IsNullOrWhiteSpace(doc.RealStopDate))
                                {
                                    if (ViewModelBase.DialogHelper.ShowConfirmation("\"Срок установленный ФКЦБ России для устранения нарушения\" или \"Дата фактического устранения нарушения управляющей компанией\" в загружаемом файле пустые! Действительно переписать существующие даты пустыми значениями?"))
                                    {
                                        importEdoOdkF140.F140 = GetF140Rewrite(edoOdkF140, doc, contract, category);
                                    }
                                    else
                                    {
                                        return ActionResult.Error("Процесс импорта прерван по инициативе пользователя");
                                    }
                                }
                                else
                                {
                                    importEdoOdkF140.F140 = GetF140Rewrite(edoOdkF140, doc, contract, category);
                                }
                                break;
                            case 4:
                                return ActionResult.Error(
                                    $"Импорт невозможен. Нарушение с кодом вида нарушения - {doc.CodeInFirm1}\n и кодом характера нарушения - {doc.CodeInFirm2}\nза {(edoOdkF140.TheDate.HasValue ? edoOdkF140.TheDate.Value.ToString("dd.MM.yyyy") : "\"период\"")} для договора № {contract.ContractNumber} устранено {(edoOdkF140.RealStopDate.HasValue ? edoOdkF140.RealStopDate.Value.ToString("dd.MM.yyyy") : string.Empty)}.");
                                

                            default:
                                return ActionResult.Error("Импорт невозможен! Ошибка типа отчета.");
                                
                        }


                    }

                    importEdoOdkF140.reportTypeID = Convert.ToInt32(doc.IDReportType);
                    importEdoOdkF140.xmlBody = xdoc.ToString();
                    importEdoOdkF140.edoLog = new EDOLog
                    {
                        DocTable = xdoc.Root.Name.ToString(),
                        DocForm = xdoc.Root.Name.ToString(),
                        Filename = fName,
                        RegDate = DateTime.Now.Date,
                        ContractNum = doc.DogovorNumber
                    };

                    importEdoOdkF140.FileName = Path.GetFileName(fileName);
                    importEdoOdkF140.PathName = Path.GetDirectoryName(fileName);
                    importEdoOdkF140.ReportOnDate = doc.DateOfReport;
                    importEdoOdkF140.ReportOnDateNative = DateUtil.ParseImportDate(doc.DateOfReport);

                    Items.Add(importEdoOdkF140);
                }

            }
            catch (InvalidOperationException ex)
            {
                if (ex.InnerException is XmlSchemaValidationException)
                {
                    ViewModelBase.DialogHelper.ShowAlert(
                        ex.InnerException.InnerException == null
                            ? $"Неверный формат документа: {ex.Message}\n\nПодробности - {ex.InnerException.Message}"
                            : $"Неверный формат документа: {ex.Message}\n\nОписание - {ex.InnerException.InnerException.Message}\n\nПодробности - {ex.InnerException.Message}");
                }
                else
                {
                    ViewModelBase.DialogHelper.ShowAlert("Неверный формат документа");
                    ViewModelBase.Logger.WriteException(ex);
                }
            }
            catch (Exception ex)
            {
                ViewModelBase.DialogHelper.ShowAlert("Ошибка открытия документа");
                ViewModelBase.Logger.WriteException(ex);
            }
            finally
            {
                Thread.CurrentThread.CurrentUICulture = ciUi;
                Thread.CurrentThread.CurrentCulture = ci;
            }

            return ActionResult.Success();
        }

        public override ActionResult Import()
        {
            var ci = Thread.CurrentThread.CurrentCulture;
            var ciUi = Thread.CurrentThread.CurrentUICulture;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("ru-RU");
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("ru-RU");
            var actionResult = ActionResult.Success();
            try
            {
                foreach (ImportEdoOdkF140 import in Items)
                {
                    actionResult = BLServiceSystem.Client.ImportUKF140(
                        import.F140, import.edoLog, import.xmlBody, import.reportTypeID);
                }
            }
            catch (Exception ex)
            {
                ViewModelBase.Logger.WriteException(ex);
                return ActionResult.Error(ex.Message);
            }
            finally
            {
                Thread.CurrentThread.CurrentUICulture = ciUi;
                Thread.CurrentThread.CurrentCulture = ci;
            }
            return actionResult;
        }

        public override void RefreshViewModels()
        {
            ViewModelBase.ViewModelManager.RefreshViewModels(
                typeof(ViolationsSIListViewModel),
                typeof(ViolationsVRListViewModel),
                typeof(LoadedODKLogListSIViewModel));
        }

        private DateTime? GetDateTime(string date)
        {
            DateTime dt;
            if (DateTime.TryParse(date, out dt))
            {
                return dt;
            }
            return null;
        }

        private string GetStatus(DateTime? stopDate, DateTime? realStopDate)
        {
            var order = 0;
            if (realStopDate == null || stopDate == null)
            {
                order = 1;
            }
            else if (stopDate < realStopDate)
            {
                order = 2;
            }
            else if (stopDate >= realStopDate)
            {
                order = 3;
            }

            return _statusF140.FirstOrDefault(el => el.Order == order).Name;
        }


        private string GetStatus(int reportType)
        {
            var order = 1;
            switch (reportType)
            {
                case 2:
                case 4:
                    order = 3;
                    break;
            }

            return _statusF140.FirstOrDefault(el => el.Order == order).Name;
        }

        private EdoOdkF140 GetF140New(EDO_ODKF140 doc, Contract contract, CategoryF140 category)
        {
            return new EdoOdkF140
            {
                RegDate = DateTime.Now.Date,
                ContractID = contract.ID,
                BaseDocDate = GetDateTime(doc.BaseDocDate),
                UKNameFull = doc.UKName,
                UKINN = doc.UKINN,
                UKFilial = doc.UKFilial,
                UKName = doc.UKName,
                TheDate = GetDateTime(doc.TheDate),
                DocNumber = doc.DocNumber,
                BaseDocNumber = doc.BaseDocNumber,
                //LinkReport = string.Empty,//====================
                //LinkNoStopReport = string.Empty,//=============
                //LinkStopReport = string.Empty,//==================
                InvestcaseName = doc.InvestcaseName,
                ViolationDate = GetDateTime(doc.ViolationDate),
                ViolationType = doc.ViolationType,
                ViolationNorma = doc.ViolationNorma,
                ViolationFact = doc.ViolationFact,
                SpecDepTreat = doc.SpecDepTreat,
                StopDate = GetDateTime(doc.StopDate),
                RealStopDate = GetDateTime(doc.RealStopDate),
                DateOfReport = DateUtil.ParseImportDate(doc.DateOfReport),
                OperatorPost = doc.OperatorPost,
                OperatorFIO = doc.OperatorFIO,
                StopDateDay = GetDateTime(doc.StopDateDay),
                ContentReport = doc.ContentReport,
                ContractNumber = doc.DogovorNumber,
                ContractDate = GetDateTime(doc.DogovorDate),
                ReportTypeID = doc.IDReportType,
                LicenseNfnpf = doc.LicenseNFNPF,
                RegNumberOut = doc.RegNumberOut,
                //StopNumber = string.Empty,//================
                //NoStopNumber = string.Empty,//=================
                //StopReport = this.GetDateTime(string.Empty),//=================
                //NoStopDate = this.GetDateTime(string.Empty),//=================
                //RealStopValue = string.Empty,//======================
                CategoryId = category.ID,
                Status = _statusF140Default?.Name
                //======================
            };

        }

        private EdoOdkF140 GetF140Rewrite(EdoOdkF140 edoOdkF140, EDO_ODKF140 doc, Contract contract, CategoryF140 category)
        {
            //edoOdkF140.SpecDepTreat = string.IsNullOrEmpty(edoOdkF140.SpecDepTreat)
            //                              ? doc.SpecDepTreat
            //                              : edoOdkF140.SpecDepTreat;
            //edoOdkF140.StopDate = edoOdkF140.StopDate.HasValue ? edoOdkF140.StopDate : this.GetDateTime(doc.StopDate);
            //edoOdkF140.RealStopDate = edoOdkF140.RealStopDate.HasValue
            //                              ? edoOdkF140.RealStopDate
            //                              : this.GetDateTime(doc.RealStopDate);
            //edoOdkF140.BaseDocNumber = string.IsNullOrEmpty(edoOdkF140.BaseDocNumber)
            //                               ? doc.BaseDocNumber
            //                               : edoOdkF140.BaseDocNumber;
            //edoOdkF140.BaseDocDate = edoOdkF140.BaseDocDate.HasValue
            //                             ? edoOdkF140.BaseDocDate
            //                             : this.GetDateTime(doc.BaseDocDate);
            //edoOdkF140.ViolationFact = string.IsNullOrEmpty(doc.RealStopDate)
            //                               ? doc.ViolationFact
            //                               : edoOdkF140.ViolationFact;

            //edoOdkF140.RealStopValue = !string.IsNullOrEmpty(doc.RealStopDate)
            //                               ? doc.ViolationFact
            //                               : edoOdkF140.RealStopValue;

            //edoOdkF140.ReportTypeID = doc.IDReportType == (int)EdoOdkF140.IDReportType.NoStopReport
            //                              ? edoOdkF140.ReportTypeID
            //                              : doc.IDReportType;
            edoOdkF140.SpecDepTreat = doc.SpecDepTreat;
            edoOdkF140.StopDate = GetDateTime(doc.StopDate);
            edoOdkF140.RealStopDate = GetDateTime(doc.RealStopDate);
            edoOdkF140.BaseDocNumber = edoOdkF140.BaseDocNumber;
            edoOdkF140.BaseDocDate = GetDateTime(doc.BaseDocDate);
            edoOdkF140.ViolationFact = string.IsNullOrEmpty(doc.RealStopDate)
                                           ? doc.ViolationFact
                                           : null;

            edoOdkF140.RealStopValue = !string.IsNullOrEmpty(doc.RealStopDate)
                                           ? doc.ViolationFact
                                           : null;

            edoOdkF140.ReportTypeID = doc.IDReportType == (int)EdoOdkF140.IDReportType.NoStopReport
                                          ? edoOdkF140.ReportTypeID
                                          : doc.IDReportType;

            if (GetDateTime(doc.StopDate).HasValue && GetDateTime(doc.RealStopDate).HasValue)
            {
                edoOdkF140.Status = GetStatus(GetDateTime(doc.StopDate), GetDateTime(doc.RealStopDate));
            }
            else
            {
                edoOdkF140.Status = GetStatus(Convert.ToInt32(doc.IDReportType));
            }

            if (doc.IDReportType == (int)EdoOdkF140.IDReportType.StopReport
                || doc.IDReportType == (int)EdoOdkF140.IDReportType.StopDivergenceReport)
            {
                edoOdkF140.StopNumber = doc.DocNumber;
                edoOdkF140.StopReport = DateUtil.ParseImportDate(doc.DateOfReport);

            }

            if (doc.IDReportType == (int)EdoOdkF140.IDReportType.NoStopReport)
            {
                edoOdkF140.NoStopNumber = doc.DocNumber;
                edoOdkF140.NoStopDate = DateUtil.ParseImportDate(doc.DateOfReport);
            }

            return edoOdkF140;

        }

        private int isValidReportType(int edoTypeId, int docTypeId)
        {

            if (edoTypeId == docTypeId && (docTypeId == 1 || docTypeId == 3))
            {
                return 1; //перезаписать после уточнения нарушение 
            }

            if (edoTypeId == docTypeId && (docTypeId == 2 || docTypeId == 4 || docTypeId == 5))
            {
                return 2; //дописать после уточнения
            }

            if (edoTypeId == 1 && (docTypeId == 2 || docTypeId == 5))
            {
                return 3; //дописать
            }

            if (edoTypeId == 3 && (docTypeId == 4 || docTypeId == 5))
            {
                return 3; //дописать
            }

            if (edoTypeId == 2 && (docTypeId == 1 || docTypeId == 5))
            {
                return 4; //на устраненный нельзя записать нарушение
            }
            if (edoTypeId == 4 && (docTypeId == 3 || docTypeId == 5))
            {
                return 4; //на устраненный нельзя записать нарушение
            }


            return 0;//ошибка
        }
    }
}