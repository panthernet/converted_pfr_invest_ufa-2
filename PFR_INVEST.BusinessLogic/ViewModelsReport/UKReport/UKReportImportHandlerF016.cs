﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.Common.Logger;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Server.UKReport;
using PFR_INVEST.DataObjects;
using PFR_INVEST.XmlSchemas;

namespace PFR_INVEST.BusinessLogic.UKReport
{
    class UKReportImportHandlerF016 : UKReportImportHandlerBase
    {
        public UKReportImportHandlerF016()
        {
            Mask = "*F016.xml";
            Shema = "F016.xsd";

        }

        public override string GetOpenFileMask(bool b)
        {
            return $"Отчет F016 (.xml)|{Mask}";
        }

        public override ActionResult LoadFile(string fileName, bool isXbrl)
        {

            var edo = new EDO_ODKF016();
            if (String.IsNullOrEmpty(fileName))
            {
                return ActionResult.Error("Ошибка при выборе файла");
            }

            var fName = Path.GetFileName(fileName);

            if (Items.Any(i => i.FileName.ToLower().Equals(fName.ToLower())))
            {
                return ActionResult.Error($"Файл {fName} уже загружен");
            }


            var serializer = new XmlSerializer(typeof(EDO_ODKF016));

            //XmlReaderSettings settings = new XmlReaderSettings();
            //settings.ValidationType = ValidationType.Schema;
            //settings.Schemas.Add(string.Empty, DocumentBase.GetShema(Shema));

            var ci = Thread.CurrentThread.CurrentCulture;
            var ciUi = Thread.CurrentThread.CurrentUICulture;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("ru-RU");
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("ru-RU");

            try
            {
                using (var reader = XmlReader.Create(fileName, GetReaderSettings()))
                {

                    var doc = (EDO_ODKF016)serializer.Deserialize(reader);
                    doc.TrimAllStringProperties();


                    if (Items.Any(i => (i as ImportEdoOdkF016).edoLog.DocRegNumberOut.ToLower().Equals(doc.RegNumberOut.ToLower())))
                    {
                        return ActionResult.Error($"Документ с регистрационным номером {doc.RegNumberOut} уже загружен для импорта");
                    }


                    var xdoc = XDocument.Load(fileName);
                    xdoc.Root.AddFirst(new XElement("FILENAME", fName));

                    var IdDelete = GetIdReportloaded(xdoc.Root.Name.ToString(), doc.RegNumberOut);
                    var importEdoOdkF016 = new ImportEdoOdkF016();


                    if (IdDelete != null)
                    {

                        if (!ViewModelBase.DialogHelper.ShowConfirmation(
                            $"Отчет по форме F016\nс регистрационным номером {doc.RegNumberOut} существует.\nУдалить старый и импортировать новый"))
                        {
                            return ActionResult.Success();
                        }

                    }

                    importEdoOdkF016.xmlBody = xdoc.ToString();
                    var rDate = DateUtil.ParseImportDate(doc.title.report_date);
                    importEdoOdkF016.F016 = new EdoOdkF016
                    {
                                 ID = IdDelete ?? 0,
                                 ReportOnDate = DateUtil.ParseImportDate(doc.title.report_on_date),
                                 Name = doc.title.promoter.name,
                                 INN = doc.title.promoter.inn,
                                 KPP = doc.title.promoter.kpp,
                                 ReportDate = rDate,
                                 ReportTime = rDate?.TimeOfDay,
                                 RegNumberOut = doc.RegNumberOut,
                                 RegDate = DateTime.Now,
                                 NetWealthSum = 0,
                                 RegNum = string.Empty,
                                 ApPost = doc.authorized_persons.specdep_person.post,
                                 ApName = doc.authorized_persons.specdep_person.name,
                                 A010 = doc.a010,
                                 A020 = doc.a020,
                                 A030 = doc.securities.a030,
                                 A031 = doc.securities.a031,
                                 A032 = doc.securities.a032,
                                 A033 = doc.securities.a033,
                                 A034 = doc.securities.a034,
                                 A035 = doc.securities.a035,
                                 A036 = doc.securities.a036,
                                 A037 = doc.securities.a037,
                                 A038 = doc.securities.a038,
                                 A039 = doc.securities.a039,
                                 A040 = doc.deb.a040,
                                 A041 = doc.deb.a041,
                                 A042 = doc.deb.a042,
                                 A043 = doc.deb.a043,
                                 A050 = doc.a050,
                                 A060 = doc.a060,
                                 A070 = doc.cred.a070,
                                 A071 = doc.cred.a071,
                                 A072 = doc.cred.a072,
                                 A073 = doc.cred.a073,
                                 A074 = doc.cred.a074,
                                 A075 = doc.cred.a075,
                                 A080 = doc.a080,
                                 A090 = doc.a090
                             };


                    importEdoOdkF016.edoLog = new EDOLog
                    {
                                                    DocTable = xdoc.Root.Name.ToString(),
                                                    DocForm = xdoc.Root.Name.ToString(),
                                                    Filename = fName,
                                                    DocRegNumberOut = doc.RegNumberOut,
                                                    RegDate = DateTime.Now.Date
                                                };

                    importEdoOdkF016.FileName = Path.GetFileName(fileName);
                    importEdoOdkF016.PathName = Path.GetDirectoryName(fileName);
                    importEdoOdkF016.ReportOnDate = importEdoOdkF016.F016.ReportOnDate == null
                                                        ? string.Empty
                                                        : ((DateTime)importEdoOdkF016.F016.ReportOnDate).ToString(
                                                            "dd.MM.yyyy");
                    importEdoOdkF016.ReportOnDateNative = importEdoOdkF016.F016.ReportOnDate;

                    Items.Add(importEdoOdkF016);
                }

            }
            catch (IOException ex)
            {
                Logger.Instance.Error("Open file exception", ex);
                return new ActionResult { ErrorMessage = "Ошибка доступа к документу. Убедитесь, что документ не открыт в другом приложении и пользователь имеет права доступа к документу.", IsSuccess = false };
            }

            catch (InvalidOperationException ex)
            {
                if (ex.InnerException is XmlSchemaValidationException)
                {
                    ViewModelBase.DialogHelper.ShowAlert(
                        ex.InnerException.InnerException == null
                            ? $"Неверный формат документа: {ex.Message}\n\nПодробности - {ex.InnerException.Message}"
                            : $"Неверный формат документа: {ex.Message}\n\nОписание - {ex.InnerException.InnerException.Message}\n\nПодробности - {ex.InnerException.Message}");
                }

                else
                {
                    ViewModelBase.DialogHelper.ShowAlert("Неверный формат документа");
                    ViewModelBase.Logger.WriteException(ex);
                }

            }
            catch (Exception ex)
            {
                ViewModelBase.DialogHelper.ShowAlert("Ошибка открытия документа");
                ViewModelBase.Logger.WriteException(ex);
            }
            finally
            {
                Thread.CurrentThread.CurrentUICulture = ciUi;
                Thread.CurrentThread.CurrentCulture = ci;
            }

            return ActionResult.Success();
        }
      
        public override ActionResult Import()
        {
            var ci = Thread.CurrentThread.CurrentCulture;
            var ciUi = Thread.CurrentThread.CurrentUICulture;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("ru-RU");
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("ru-RU");
            var actionResult = ActionResult.Success();
            try
            {

                foreach (ImportEdoOdkF016 import in Items)
                {
                    actionResult = BLServiceSystem.Client.ImportUKF016(import.F016, import.edoLog, import.xmlBody);
                }

            }
            catch (Exception ex)
            {

                ViewModelBase.Logger.WriteException(ex);
                return ActionResult.Error(ex.Message);
            }
            finally
            {
                Thread.CurrentThread.CurrentUICulture = ciUi;
                Thread.CurrentThread.CurrentCulture = ci;

            }
            return actionResult;
        }

        public override void RefreshViewModels()
        {
            ViewModelBase.ViewModelManager.RefreshViewModels(typeof(NetWealthsSumListViewModel), typeof(LoadedODKLogListSIViewModel));

        }

    }
}
