﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.Common.Logger;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Server.UKReport;
using PFR_INVEST.DataObjects;
using PFR_INVEST.XmlSchemas;

namespace PFR_INVEST.BusinessLogic.UKReport
{
    class UKReportImportHandlerF015 : UKReportImportHandlerBase
    {
        public UKReportImportHandlerF015()
        {
            Mask = "*F015.xml";
            Shema = "F015.xsd";

        }

        public override string GetOpenFileMask(bool b)
        {
            return $"Отчет F015 (.xml)|{Mask}";
        }

        public override ActionResult LoadFile(string fileName, bool isXbrl)
        {
            EDO_ODKF015 edo = new EDO_ODKF015();
            if (String.IsNullOrEmpty(fileName))
            {
                return ActionResult.Error("Ошибка при выборе файла");
            }

            string fName = Path.GetFileName(fileName);

            if (Items.Any(i => i.FileName.ToLower().Equals(fName.ToLower())))
            {
                return ActionResult.Error($"Файл {fName} уже загружен");
            }


            XmlSerializer serializer = new XmlSerializer(typeof(EDO_ODKF015));

            //XmlReaderSettings settings = new XmlReaderSettings();
            //settings.ValidationType = ValidationType.Schema;
            //settings.Schemas.Add(string.Empty, DocumentBase.GetShema(Shema));

            CultureInfo ci = Thread.CurrentThread.CurrentCulture;
            CultureInfo ciUi = Thread.CurrentThread.CurrentUICulture;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("ru-RU");
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("ru-RU");

            try
            {
                using (XmlReader reader = XmlReader.Create(fileName, GetReaderSettings()))
                {

                    var doc = (EDO_ODKF015)serializer.Deserialize(reader);
                    doc.TrimAllStringProperties();


                    if (Items.Any(i => (i as ImportEdoOdkF015).edoLog.DocRegNumberOut.ToLower().Equals(doc.RegNumberOut.ToLower())))
                    {
                        return ActionResult.Error($"Документ с регистрационным номером {doc.RegNumberOut} уже загружен для импорта");
                    }

                    var contract = GetContractByDogovorNum(doc.title.dogovor_num);

                    if (contract == null)
                    {
                        return ActionResult.Error($"Договор с регистрационным номером {doc.title.dogovor_num} не найден");
                    }


                    XDocument xdoc = XDocument.Load(fileName);
                    xdoc.Root.AddFirst(new XElement("FILENAME", fName));

                    long? IdDelete = GetIdReportloaded(xdoc.Root.Name.ToString(), doc.RegNumberOut);
                    ImportEdoOdkF015 importEdoOdkF015 = new ImportEdoOdkF015();


                    if (IdDelete != null)
                    {

                        if (!ViewModelBase.DialogHelper.ShowConfirmation(
                            $"Отчет по форме F015\nс регистрационным номером {doc.RegNumberOut} существует.\nУдалить старый и импортировать новый"))
                        {
                            return ActionResult.Success();
                        }

                    }

                    importEdoOdkF015.xmlBody = xdoc.ToString();
                    var rDate = DateUtil.ParseImportDate(doc.title.report_date);
                    importEdoOdkF015.F015 = new EdoOdkF015
                    {
                                 ID = IdDelete ?? 0,
                                 ContractId = contract.ID,
                                 ReportOnDate = DateUtil.ParseImportDate(doc.title.report_on_date),
                                 Name = doc.title.promoter.name,
                                 INN = doc.title.promoter.inn,
                                 KPP = doc.title.promoter.kpp,
                                 ReportDate = rDate,
                                 ReportTime = rDate?.TimeOfDay,
                                 RegNumberOut = doc.RegNumberOut,
                                 RegDate = DateTime.Now,
                                 NetWealthSum = 0,
                                 RegNum = string.Empty,
                                 ApPost = doc.authorized_persons.manage_company_person.post,
                                 ApName = doc.authorized_persons.manage_company_person.name,
                                 A010 = doc.a010,
                                 A020 = doc.a020,
                                 A030 = doc.securities.a030,
                                 A031 = doc.securities.a031,
                                 A032 = doc.securities.a032,
                                 A033 = doc.securities.a033,
                                 A034 = doc.securities.a034,
                                 A035 = doc.securities.a035,
                                 A036 = doc.securities.a036,
                                 A037 = doc.securities.a037,
                                 A038 = doc.securities.a038,
                                 A039 = doc.securities.a039,
                                 A40 = doc.deb.a040,
                                 A41 = doc.deb.a041,
                                 A42 = doc.deb.a042,
                                 A43 = doc.deb.a043,
                                 A50 = doc.a050,
                                 A60 = doc.a060,
                                 A70 = doc.cred.a070,
                                 A071 = doc.cred.a071,
                                 A072 = doc.cred.a072,
                                 A073 = doc.cred.a073,
                                 A074 = doc.cred.a074,
                                 A075 = doc.cred.a075,
                                 A080 = doc.a080,
                                 A090 = doc.a090
                             };


                    importEdoOdkF015.edoLog = new EDOLog
                    {
                                                    DocTable = xdoc.Root.Name.ToString(),
                                                    DocForm = xdoc.Root.Name.ToString(),
                                                    Filename = fName,
                                                    DocRegNumberOut = doc.RegNumberOut,
                                                    RegDate = DateTime.Now.Date,
                                                    ContractNum = doc.title.dogovor_num

                                                };

                    importEdoOdkF015.FileName = Path.GetFileName(fileName);
                    importEdoOdkF015.PathName = Path.GetDirectoryName(fileName);
                    importEdoOdkF015.ReportOnDate = importEdoOdkF015.F015.ReportOnDate == null
                                                        ? string.Empty
                                                        : ((DateTime)importEdoOdkF015.F015.ReportOnDate).ToString(
                                                            "dd.MM.yyyy");
                    importEdoOdkF015.ReportOnDateNative = importEdoOdkF015.F015.ReportOnDate;

                    Items.Add(importEdoOdkF015);
                }

            }
            catch (IOException ex)
            {
                Logger.Instance.Error("Open file exception", ex);
                return new ActionResult { ErrorMessage = "Ошибка доступа к документу. Убедитесь, что документ не открыт в другом приложении и пользователь имеет права доступа к документу.", IsSuccess = false };
            }
            catch (InvalidOperationException ex)
            {
                if (ex.InnerException is XmlSchemaValidationException)
                {
                    if (ex.InnerException.InnerException == null)
                    {
                        ViewModelBase.DialogHelper.ShowAlert(
                            $"Неверный формат документа: {ex.Message}\n\nПодробности - {ex.InnerException.Message}");
                    }
                    else
                    {
                        ViewModelBase.DialogHelper.ShowAlert(
                            $"Неверный формат документа: {ex.Message}\n\nОписание - {ex.InnerException.InnerException.Message}\n\nПодробности - {ex.InnerException.Message}");
                    }

                }

                else
                {
                    ViewModelBase.DialogHelper.ShowAlert("Неверный формат документа");
                    ViewModelBase.Logger.WriteException(ex);
                }

            }
            catch (Exception ex)
            {
                ViewModelBase.DialogHelper.ShowAlert("Ошибка открытия документа");
                ViewModelBase.Logger.WriteException(ex);
            }
            finally
            {
                Thread.CurrentThread.CurrentUICulture = ciUi;
                Thread.CurrentThread.CurrentCulture = ci;
            }

            return ActionResult.Success();
        }
      
        public override ActionResult Import()
        {
            CultureInfo ci = Thread.CurrentThread.CurrentCulture;
            CultureInfo ciUi = Thread.CurrentThread.CurrentUICulture;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("ru-RU");
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("ru-RU");
            ActionResult actionResult = ActionResult.Success();
            try
            {

                foreach (ImportEdoOdkF015 import in Items)
                {
                    actionResult = BLServiceSystem.Client.ImportUKF015(import.F015, import.edoLog, import.xmlBody);
                }

            }
            catch (Exception ex)
            {

                ViewModelBase.Logger.WriteException(ex);
                return ActionResult.Error(ex.Message);
            }
            finally
            {
                Thread.CurrentThread.CurrentUICulture = ciUi;
                Thread.CurrentThread.CurrentCulture = ci;

            }
            return actionResult;
        }

        public override void RefreshViewModels()
        {
            ViewModelBase.ViewModelManager.RefreshViewModels(typeof(NetWealthsSIListViewModel), typeof(NetWealthsVRListViewModel), typeof(LoadedODKLogListSIViewModel));

        }

    }
}
