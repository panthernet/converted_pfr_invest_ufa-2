﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.DataObjects.Reports;

namespace PFR_INVEST.BusinessLogic.ViewModelsReport
{
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OKIP_manager)]
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OKIP_manager)]
    public class ReportErrorMonthSpreadViewModel : ReportNpfErrorBaseViewModel
	{
        private const string TEMPLATE_NAME = "Распределение ошибок по месяцам.docx";

        public Dictionary<string, int> YearsList { get; set; }
        public KeyValuePair<string, int> SelectedYear { get; set; }

        public ReportErrorMonthSpreadViewModel()
        {
            YearsList = DateTools.GetYearsDictionaryPlus(0);//.Values.ToList();
            SelectedYear = YearsList.First(a=> a.Value == (DateTime.Now.Year - 1));
        }

	    protected override bool IsValid()
	    {
            return "SelectedYear".Split('|').All(f => string.IsNullOrEmpty(this[f]));
	    }

        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "SelectedYear": return SelectedYear.Value == 0 ? "Не выбран год!" : "";
                }
                return null;
            }
        }

	    protected override void ExecuteSave()
		{
		    var filePath = string.Empty;
            OnRequestLoadingIndicator("Создание отчета...");
            OnRequestTopMost(true);
		    try
		    {
		        var serviceData = BLServiceSystem.Client.GetReportErrorMonthSpreadData(new ReportErrorTypesPrompt {Year = SelectedYear.Value}).ToList();


		        if (serviceData.Count == 0)
		        {
                    OnRequestLoadingIndicator();
                    OnRequestTopMost(false);
                    DialogHelper.ShowWarning("Данные для отчета за указанный период отсутствуют!", "Внимание");
		            return;
		        }

		        //подготовка данных
		        var columns = new List<WordReportDataColumn>();

		        serviceData.ForEach(a =>
		        {
		            if (!string.IsNullOrEmpty(a.Code) && columns.FirstOrDefault(b => b.Value == a.Code) == null)
		                columns.Add(new WordReportDataColumn(a.CodeName, a.Code));
		        });


		        var blankList = new List<object>(columns.Count);
		        for(var i = 0; i < columns.Count; i++)
		            blankList.Add(0);
		        var barChartData = new Dictionary<string, List<object>>();
                for(var i =1 ; i <=12; i++) 
                    barChartData.Add(i.ToString(), blankList.ToList());

		        //fill chart data
		        //добавляем наименования колонок для бар чарта (коды стыковки)
		        var barColumnNamesList = new List<string>();
		        columns.ForEach(a => barColumnNamesList.Add(a.Name));


		        /* Random rnd = new Random();
            for (int i = 0; i < 20; i++)
            {
                serviceData.Add(new ReportErrorSpreadQueryItem(new object[]
                    {
                        "Npf test name "+ i,
                        "11",
                        "NNNN",
                        rnd.Next(1, 1000)
                    }));
            }*/

		        foreach (var item in serviceData)
		        {
		            var columnIndex = columns.FindIndex(a => a.Value == item.Code);
		            //индекс < 0, не нашли колонку: возможно отсеяли колонки, которые не влезли на страницу
		            if (columnIndex < 0) continue;

		            if (barChartData.ContainsKey(item.Name))
		                barChartData[item.Name][columnIndex] = item.Summ;
		            else throw new Exception("Неизвестный месяц в данных!");
		        }

		        //сортировка
		        //barChartData = barChartData.OrderByDescending(a => a.Value.Sum(b =>  b as int? ?? 0)).ToDictionary(a => a.Key, a => a.Value);


		        //подготовка шаблона
		        filePath = TemplatesManager.ExtractTemplate(TEMPLATE_NAME);
		        using (var word = new WordReportProxy(filePath))
		        {
		            word.ReportManager.FindAndReplace("{Year}", SelectedYear.Value);
		            word.ReportManager.PopulateBarChart(barChartData, barColumnNamesList, 1, 2);
		            word.ReportManager.PopulateBarChart(barChartData, barColumnNamesList, 2, 2);
		            word.ReportManager.SaveAndClose();
		        }
		    }
            finally
            {
                OnRequestLoadingIndicator();
                OnRequestTopMost(false);
                OnRequestClose(true);
            }
            Process.Start(filePath);
            Close();
		    
		}
	}
}
