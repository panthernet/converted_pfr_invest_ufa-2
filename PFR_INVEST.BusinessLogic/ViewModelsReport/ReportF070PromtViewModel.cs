﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Reports;

namespace PFR_INVEST.BusinessLogic.ViewModelsReport
{
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_viewer, DOKIP_ROLE_TYPE.OKIP_viewer, DOKIP_ROLE_TYPE.OARRS_viewer, DOKIP_ROLE_TYPE.OFPR_viewer, DOKIP_ROLE_TYPE.OUFV_viewer, DOKIP_ROLE_TYPE.OSRP_viewer, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_manager, DOKIP_ROLE_TYPE.OUFV_worker)]
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_manager, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class ReportF070PromtViewModel : ViewModelCardDialog
	{


		public Document.Types Type { get; private set; }
		public List<SimpleObjectValue> QuarterList { get; private set; }
		public List<SimpleObjectValue> UnitsList { get; private set; }
		public List<Year> YearList { get; private set; }

		private long? yearID;
		public long? YearID { get { return yearID; } set { yearID = value; OnPropertyChanged("YearID"); } }

		private long? quarterID;
		public long? QuarterID { get { return quarterID; } set { quarterID = value; OnPropertyChanged("QuarterID"); } }

		private long? units;
		public long? Units { get { return units; } set { units = value; OnPropertyChanged("Units"); } }

        public ObservableCollection<Person> PersonList { get; set; }

        private Person _selectedPerson;
        public Person SelectedPerson
        {
            get { return _selectedPerson; }
            set { _selectedPerson = value; OnPropertyChanged("SelectedPerson"); }
        }

		public ReportF070PromtViewModel(Document.Types type)
		{
			QuarterList = new List<SimpleObjectValue>
			{
				new SimpleObjectValue {ID =1, Name = "I"},
				new SimpleObjectValue {ID =2, Name = "II"},
				new SimpleObjectValue {ID =3, Name = "III"},
				new SimpleObjectValue {ID =4, Name = "IV"}
			};

			UnitsList = new List<SimpleObjectValue>
			{
				new SimpleObjectValue {ID =1, Name = "руб."},
				new SimpleObjectValue {ID =1000, Name = "тыс. руб."}
			};

			YearList = DataContainerFacade.GetList<Year>();

			Type = type;
			YearID = DateTime.Now.Year - 2000;
			QuarterID = ((DateTime.Now.Month - 1) / 3) + 1;
			Units = 1;

            PersonList = new ObservableCollection<Person>(PersonHelper.GetAvailablePersons(type));
            SelectedPerson = PersonList.FirstOrDefault();
		}

		public override bool CanExecuteSave()
		{
			return IsValid();
		}

		public bool IsValid()
		{
			return ValidateFields();
		}

		public override string this[string columnName]
		{
			get
			{
				switch (columnName)
				{
					case "YearID": return YearID.ValidateRequired();
					case "QuarterID": return QuarterID.ValidateRequired();
					case "Units": return Units.ValidateRequired();
					case "SelectedPerson": return _selectedPerson.ValidateRequired();
				}
				return null;
			}
		}


		public ReportF070Prompt GetReportPrompt()
		{
			if (!IsValid())
				return null;
			var prompt = new ReportF070Prompt
			{
				YearID = YearID ?? 0,
				Quarter = (int)(QuarterID ?? 0),
				Units = (int)(Units ?? 0),
				Type = Type,
				Person = _selectedPerson,
			};

			return prompt;
		}
	}
}
