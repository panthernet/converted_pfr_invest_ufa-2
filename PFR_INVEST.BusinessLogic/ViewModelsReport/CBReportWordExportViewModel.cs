﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Word;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class CBReportWordExportViewModel: ViewModelCardDialog
    {
        private const string TEMPLATE_NAME = "Отчет о СПН для БО номер 1.docx";

        private readonly BOReportForm1 _report;
        private readonly Person _person;
        private readonly CultureInfo _culture = CultureInfo.CreateSpecificCulture("ru-RU");
        private readonly object _oMissing = Missing.Value;
        private Table _myTable;
        public CBReportWordExportViewModel(BOReportForm1 report, Person person)
        {
            _report = report;
            _person = person;
        }

        private _Application _mWApp;
        private _Application WordApp => _mWApp ?? (_mWApp = new Application());

        private void GenerateDoc()
        {
            var pf = _report.PortfolioID.HasValue ? DataContainerFacade.GetByID<Portfolio>(_report.PortfolioID) : null;

            OfficeTools.FindAndReplace(WordApp, "{INN}", _report.INN);
            OfficeTools.FindAndReplace(WordApp, "{OGRN}", _report.OGRN);
            OfficeTools.FindAndReplace(WordApp, "{POSITION}", _person.Position);
            OfficeTools.FindAndReplace(WordApp, "{FIO}", _person.FormattedFullName);

            var dt = _report.Date.Subtract(TimeSpan.FromDays(1));
            OfficeTools.FindAndReplace(WordApp, "{DATE_DAY}", dt.Day.ToString("00"));
            OfficeTools.FindAndReplace(WordApp, "{DATE_MONTH}", DateTools.MonthesGenitive[dt.Month - 1]);
            OfficeTools.FindAndReplace(WordApp, "{DATE_YEAR}", (dt.Year-2000).ToString());

            OfficeTools.FindAndReplace(WordApp, "{TOTAL1}", GetVal(_report.Data.Total1));
            OfficeTools.FindAndReplace(WordApp, "{TOTAL2}", GetVal(_report.Data.Total2));
            OfficeTools.FindAndReplace(WordApp, "{TOTAL3}", GetVal(_report.Data.Total3));
            OfficeTools.FindAndReplace(WordApp, "{TOTAL4}", GetVal(_report.Data.Total4));
            OfficeTools.FindAndReplace(WordApp, "{TOTAL5}", GetVal(_report.Data.Total5));
            OfficeTools.FindAndReplace(WordApp, "{TOTAL6}", GetVal(_report.Data.Total6));
            OfficeTools.FindAndReplace(WordApp, "{TOTAL7}", GetVal(_report.Data.Total7));
            OfficeTools.FindAndReplace(WordApp, "{TOTAL8}", GetVal(_report.Data.Total8));
            OfficeTools.FindAndReplace(WordApp, "{TOTAL9}", GetVal(_report.Data.Total9));
            OfficeTools.FindAndReplace(WordApp, "{TOTAL10}", GetVal(_report.Data.Total10));
            OfficeTools.FindAndReplace(WordApp, "{TOTAL11}", GetVal(_report.Data.Total11));
            OfficeTools.FindAndReplace(WordApp, "{TOTAL12}", GetVal(_report.Data.Total12));
            OfficeTools.FindAndReplace(WordApp, "{TOTAL13}", GetVal(_report.Data.Total13));
            OfficeTools.FindAndReplace(WordApp, "{TOTAL14}", GetVal(_report.Data.Total14));
            OfficeTools.FindAndReplace(WordApp, "{TOTAL15}", GetVal(_report.Data.Total15));
            OfficeTools.FindAndReplace(WordApp, "{TOTAL16}", GetVal(_report.Data.Total16));
            OfficeTools.FindAndReplace(WordApp, "{TOTAL17}", GetVal(_report.Data.Total17));
            OfficeTools.FindAndReplace(WordApp, "{TOTAL18}", GetVal(_report.Data.Total18));

            OfficeTools.FindAndReplace(WordApp, "{QUARTER1}", GetVal(_report.Data.Quartal1));
            OfficeTools.FindAndReplace(WordApp, "{QUARTER2}", GetVal(_report.Data.Quartal2));
            OfficeTools.FindAndReplace(WordApp, "{QUARTER3}", GetVal(_report.Data.Quartal3));
            OfficeTools.FindAndReplace(WordApp, "{QUARTER4}", GetVal(_report.Data.Quartal4));
            OfficeTools.FindAndReplace(WordApp, "{QUARTER5}", GetVal(_report.Data.Quartal5));
            OfficeTools.FindAndReplace(WordApp, "{QUARTER6}", GetVal(_report.Data.Quartal6));
            OfficeTools.FindAndReplace(WordApp, "{QUARTER7}", GetVal(_report.Data.Quartal7));
            OfficeTools.FindAndReplace(WordApp, "{QUARTER8}", GetVal(_report.Data.Quartal8));
            OfficeTools.FindAndReplace(WordApp, "{QUARTER9}", GetVal(_report.Data.Quartal9));
            OfficeTools.FindAndReplace(WordApp, "{QUARTER10}", GetVal(_report.Data.Quartal10));
            OfficeTools.FindAndReplace(WordApp, "{QUARTER11}", GetVal(_report.Data.Quartal11));
            OfficeTools.FindAndReplace(WordApp, "{QUARTER12}", GetVal(_report.Data.Quartal12));
            OfficeTools.FindAndReplace(WordApp, "{QUARTER13}", GetVal(_report.Data.Quartal13));
            OfficeTools.FindAndReplace(WordApp, "{QUARTER14}", GetVal(_report.Data.Quartal14));
            OfficeTools.FindAndReplace(WordApp, "{QUARTER15}", GetVal(_report.Data.Quartal15));
            OfficeTools.FindAndReplace(WordApp, "{QUARTER16}", GetVal(_report.Data.Quartal16));
            OfficeTools.FindAndReplace(WordApp, "{QUARTER17}", GetVal(_report.Data.Quartal17));
            OfficeTools.FindAndReplace(WordApp, "{TEXT19}", $@"по инвестиционному портфелю {(pf == null ? "отчетного квартала отчетного года" : pf.Name)}");
            OfficeTools.FindAndReplace(WordApp, "{QUARTER19}", GetVal(_report.Data.Quartal19));

            

            FillRows();
        }

        private void FillRows()
        {
            if (_report.SumList.Count == 0)
            {
                return;
            }

            var sortedList = _report.SumList.OrderBy(a => a.ID).ToList();

            _myTable = _mWApp.ActiveDocument.Tables[2];
            var secRowNum = 20;

            for (int i = 0; i < sortedList.Count; i++)
                _myTable.Rows.Add(_oMissing);

            var cell = _myTable.Cell(1, 1);

            while (cell.Range.Text !="\r\a")
            {
                cell = cell.Next;
            }

            int counter = 0;
            int sumCount = 0;
            while (cell != null || sumCount < sortedList.Count)
            {
                if (sortedList.Count <= sumCount)
                {
                    cell = null;
                    continue;
                }
                var curSum = sortedList[sumCount];

                switch (counter)
                {
                    case 0:
                        cell.Range.Text= $@"по инвестиционному портфелю {curSum.PortfolioName}";
                        break;
                    case 1:
                        cell.Range.Text = secRowNum++.ToString();
                        break;
                    case 2:
                        cell.Range.Text = "x";
                        break;
                    case 3:
                        cell.Range.Text = GetVal(curSum.Sum);
                        break;
                }
                counter++;
                if (counter > 3)
                {
                    counter = 0;
                    sumCount++;
                }
                cell = cell?.Next;            
            }
        }

        private string GetVal(decimal? value)
        {
            return value == null ? "0" : Convert.ToInt64(Math.Round(value.Value)).ToString("n0", _culture);
        }

        public void GenerateReport()
        {
            var extractedFilePath = TemplatesManager.ExtractTemplate(TEMPLATE_NAME);
            if (!File.Exists(extractedFilePath))
            {
                RaiseErrorLoadingTemplate(TEMPLATE_NAME);
                return;
            }
            try
            {
                WordApp.Visible = false;
                WordApp.Documents.Open(extractedFilePath);
                GenerateDoc();
                WordApp.Visible = true;
            }
            finally
            {
                if (_myTable != null)
                    Marshal.FinalReleaseComObject(_myTable);
                Marshal.FinalReleaseComObject(_mWApp);
            }
        }
    }
}
