﻿
using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Journal;

namespace PFR_INVEST.BusinessLogic.SIReport
{
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class SiRegisterTransferData
    {
        public static String IMPORT_FLAG = "импорт";

        public class SITransferData
        {
            public SITransfer siTransfer = new SITransfer();
            public List<ReqTransfer> reqTransferList = new List<ReqTransfer>();

        }

        public SIRegister siRegister;

        public List<SITransferData> siTransferList;

        public SiRegisterTransferData()
        {
            siRegister = new SIRegister();
            siTransferList = new List<SITransferData>();
        }

        public SiRegisterTransferData(long companyMonthID, long companyYearID, long  directionID,long operationID)
        {
            siRegister = new SIRegister
            {
                                  CompanyMonthID = companyMonthID,
                                  CompanyYearID = companyYearID,
                                  DirectionID = directionID,
                                  OperationID = operationID,
                                  RegisterDate = DateTime.Now.Date,
                                  Comment = IMPORT_FLAG
                             };

            siTransferList = new List<SITransferData>();

        }
        public SiRegisterTransferData(SIRegister siRegister)
        {
            this.siRegister = siRegister;

            siTransferList = new List<SITransferData>();

            var transfers = BLServiceSystem.Client.GetSITransfersByRegister(this.siRegister.ID);

            foreach (var siTransfer in transfers)
            {
                siTransferList.Add(new SITransferData
                    {
                                           siTransfer = siTransfer,
                                           reqTransferList = BLServiceSystem.Client.GetTransfersListBySITR(siTransfer.ID)
                                       }

                    );
            }

        }


        /// <summary>
        /// Сохранить переданный экземпляр. Если пустой возврат 0, если нет № реестра
        /// </summary>
        /// <param name="siRegisterTransferData"></param>
        /// <returns>Если пустой возврат 0, если нет № реестра</returns>
        public long  ExecuteSave(SiRegisterTransferData siRegisterTransferData)
        {

            if (siRegisterTransferData.siTransferList.Count == 0) return 0;

            SIRegister siRegister = siRegisterTransferData.siRegister;
            long id = DataContainerFacade.Save<SIRegister, long>(siRegister);

            foreach (var siTransferData in siRegisterTransferData.siTransferList)
            {
                SITransfer siTransfer = siTransferData.siTransfer;
                siTransfer.TransferRegisterID = id;
                long idSITransfer = DataContainerFacade.Save<SITransfer, long>(siTransfer);

                foreach (var reqTransfer in siTransferData.reqTransferList)
                {
                    reqTransfer.TransferListID = idSITransfer;
                    reqTransfer.ID = DataContainerFacade.Save<ReqTransfer, long>(reqTransfer);
                }

            }

            return id;

        }
        /// <summary>
        /// Сохранить свой экземпляр. Если пустой возврат 0, если нет № реестра
        /// </summary>
        /// <returns>Если пустой возврат 0, если нет № реестра</returns>
        public long  ExecuteSave()
        {

            if (siTransferList.Count == 0) return 0;

            SIRegister siRegister = this.siRegister;
            long id = DataContainerFacade.Save<SIRegister, long>(siRegister);
            JournalLogger.LogEvent("Создание реестра перечислений", JournalEventType.INSERT, null, "SiRegisterTransferData", "Номер: " + id);
            foreach (var siTransferData in siTransferList)
            {
                SITransfer siTransfer = siTransferData.siTransfer;
                siTransfer.TransferRegisterID = id;
                long idSITransfer = DataContainerFacade.Save<SITransfer, long>(siTransfer);

                foreach (var reqTransfer in siTransferData.reqTransferList)
                {
                    reqTransfer.TransferListID = idSITransfer;
                    reqTransfer.ID = DataContainerFacade.Save<ReqTransfer, long>(reqTransfer);
                }

            }

            return id;

        }

        static public void ExecuteDelete(List<SIRegister> SIRegisters,List<long> listDeleteSiRegisterID)
        {

            foreach (var id in listDeleteSiRegisterID)
            {
                var TransferRegister = SIRegisters.FirstOrDefault(sr => sr.ID == id);
                if (TransferRegister == null) return;

                var list = BLServiceSystem.Client.GetSITransfersByRegister(TransferRegister.ID);

                foreach (var item in list)
                {
                    var reqs = BLServiceSystem.Client.GetTransfersListBySITR(item.ID);
                    foreach (var rq in reqs)
                    {
                        //get SIUKPLANs and modify FK
                        var requkpl = BLServiceSystem.Client.GetSIUKPlansByReqID(rq.ID);
                        foreach (var ukpl in requkpl)
                        {
                            ukpl.UnsetTransferId(rq.ID);
                            DataContainerFacade.Save<SIUKPlan, long>(ukpl);
                        }
                        //delete req
                        DataContainerFacade.Delete(rq);
                    }

                    //delete SIUKPLANs
                    var siukpl = BLServiceSystem.Client.GetSIUKPlansByReqListID(item.ID);
                    foreach (var sipl in siukpl)
                        DataContainerFacade.Delete(sipl);
                    //delete SItransfer
                    DataContainerFacade.Delete(item);
                }

                //delete register
                DataContainerFacade.Delete(TransferRegister);

            }
        }

    }
}

