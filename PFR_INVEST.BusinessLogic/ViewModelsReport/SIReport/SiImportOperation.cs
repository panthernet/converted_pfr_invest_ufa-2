﻿
using System.Linq;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.SIReport
{
    public class SiImportOperation
    {
        public string OperationName { get; set; }
        public long OperationId { get; set; }
        public long DirectionId { get; set; }



        public SiImportOperation(string OperationName)
        {
            var spnOperation = DataContainerFacade.GetListByProperty<SPNOperation>("Name", OperationName).ToList().FirstOrDefault();
            var dir = spnOperation.GetDirections();
            this.OperationName = spnOperation != null ? spnOperation.Name : null;
            OperationId = spnOperation != null ? spnOperation.ID : 0;
            DirectionId = dir != null ? (dir[0] as SPNDirection).ID : 0;

        }
    }
}
