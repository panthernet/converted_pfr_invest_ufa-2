﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.ListExtensions;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataObjects.Reports;

namespace PFR_INVEST.BusinessLogic.ViewModelsReport
{
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OKIP_manager)]
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OKIP_manager)]
    public class ReportErrorTypesViewModel : ReportNpfErrorBaseViewModel
	{
	    private const string TEMPLATE_NAME = "Типология ошибок.docx";

		protected override void ExecuteSave()
		{
            var filePath = string.Empty;
            OnRequestTopMost(true);
		    OnRequestLoadingIndicator("Создание отчета...");
		    try
		    {
		        var serviceData = BLServiceSystem.Client.GetReportErrorTypeData(new ReportErrorTypesPrompt {DateFrom = DateFrom, DateTo = DateTo}).ToList();

		        if (serviceData.Count == 0)
		        {
                    OnRequestLoadingIndicator();
                    OnRequestTopMost(false);
                    DialogHelper.ShowWarning("Данные для отчета за указанный период отсутствуют!", "Внимание");
		            return;
		        }

                /*Random rnd = new Random();
                for (int i = 0; i < 20; i++)
                {
                    serviceData.Add(new ReportErrorTypesQueryItem(new object[]
                    {
                        "Npf test name ",
                        rnd.Next(0,2) > 0 ? "11" : "4",
                        rnd.Next(0,15).ToString(),
                        "Name   dsfff  fsff?",
                        rnd.Next(1, 100)
                    }));
                }*/


                const int maxDataColumns = 8;
                //const int maxDataRows = 13;
                //подготовка данных
		        var columns = new List<WordReportDataColumn>();

		        serviceData.ForEach(a =>
		        {
		            if (!string.IsNullOrEmpty(a.ContractCode) && columns.FirstOrDefault(b => b.Value == a.ContractCode) == null && columns.Count < maxDataColumns)
		                columns.Add(new WordReportDataColumn(a.ContractName, a.ContractCode));
		        });
		        columns.Add(new WordReportDataColumn("Итоговая сумма", "-1"));

		        var blankList = new List<object>(columns.Count + 1);
		        for (int i = 0; i < columns.Count; i++)
		            blankList.Add(0);
		        var rows = new Dictionary<string, List<object>>();
		        var chartData = new Dictionary<string, object>();


		        foreach (var item in serviceData)
		        {
		            var index = columns.FindIndex(a => a.Value == item.ContractCode);
		            //индекс < 0, не нашли колонку: возможно отсеяли колонки, которые не влезли на страницу
		            if (index < 0) continue;
		            if (rows.ContainsKey(item.Name))
		            {
		                rows[item.Name][index] = item.Sum;
		            } else
		            {
		                var list = blankList.ToList();
		                list[index] = item.Sum;
		                rows.Add(item.Name, list);
		            }
		        }
		        //итоговая сумма по строкам
		        rows.ForEach(a => a.Value[columns.Count - 1] = a.Value.Sum(b => (int) b));
		        //сортировка строк - самые крупные значения сверху
		        rows = rows.OrderByDescending(a => a.Value[columns.Count - 1]).ToDictionary(x => x.Key, x => x.Value);



		        var sumList = blankList.ToList();
		        rows.ForEach(row =>
		        {
		            //данные для графика
		            chartData.Add(row.Key, row.Value[columns.Count - 1]);
		            //итоговая сумма по колонкам
		            for (int i = 0; i < row.Value.Count; i++)
		                sumList[i] = (int) sumList[i] + (int) row.Value[i];
		        });
		        rows.Add("Общий итог", sumList);

		        //вычисление леблов для скрытия
		        var total = (int)sumList.Last();
		        var counter = rows.Select(row => (int) row.Value.Last() * 100 / total).TakeWhile(perc => perc >= 3).Count();


		        //подготовка шаблона
		        filePath = TemplatesManager.ExtractTemplate(TEMPLATE_NAME);
		        using (var word = new WordReportProxy(filePath))
		        {
		            word.ReportManager.PopulatePieChart(chartData, 1, counter);
		            word.ReportManager.FindAndReplace("{DateFrom}", DateFrom.HasValue ? DateFrom.Value.ToShortDateString() : "");
		            word.ReportManager.FindAndReplace("{DateTo}", DateTo.HasValue ? DateTo.Value.ToShortDateString() : "");
		            var options = new PopulateTableOptions { AutoFitTableWidth = true };
		            options.RowAppearances.Add(rows.Count + 1, new RowAppearance {CustomBackgroundColor = 15853276, TextBold = true});
		            options.ColumnAppearances.Add(1, new ColumnAppearance {PreferredWidth = 150});
		            word.ReportManager.PopulateTable(columns, rows, options);
		            word.ReportManager.SaveAndClose();
		        }
		    }
            finally
            {
                OnRequestLoadingIndicator();
                OnRequestTopMost(false);
            }
            Process.Start(filePath);
            Close();
		}
	}
}
