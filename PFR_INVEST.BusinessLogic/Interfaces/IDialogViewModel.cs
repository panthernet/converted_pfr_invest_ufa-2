﻿using System;
using PFR_INVEST.BusinessLogic.Misc;

namespace PFR_INVEST.BusinessLogic.Interfaces
{
	public interface IDialogViewModel
	{
		event EventHandler<EventArgs<bool?>> OnCloseRequested; 
	}
}
