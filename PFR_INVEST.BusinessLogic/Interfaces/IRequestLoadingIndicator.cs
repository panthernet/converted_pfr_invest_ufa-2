﻿using System;

namespace PFR_INVEST.BusinessLogic.Interfaces
{
    /// <summary>
    /// Интерфейс для представления на показ индикатора загрузки
    /// </summary>
    public interface IRequestLoadingIndicator
    {
        /// <summary>
        /// Запрос для представления на показ индикатора загрузки
        /// </summary>
        event EventHandler RequestLoadingIndicator;
    }
}
