﻿using System;
using System.Collections.Generic;

namespace PFR_INVEST.BusinessLogic.Interfaces
{
	/// <summary>
	/// Интерфейс для моделей, которые генерируют события изменения данных
	/// </summary>
	public interface IUpdateRaisingModel
	{
		IList<KeyValuePair<Type, long>> GetUpdatedList();
	}
}
