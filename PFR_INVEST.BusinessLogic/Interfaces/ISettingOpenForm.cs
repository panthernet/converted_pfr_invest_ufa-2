﻿
namespace PFR_INVEST.BusinessLogic.Interfaces
{
    using PFR_INVEST.BusinessLogic.Misc;

    public interface ISettingOpenForm
    {       
        OpenSettingsProvider SettingsProvider { get; set; }
        
    }
}
