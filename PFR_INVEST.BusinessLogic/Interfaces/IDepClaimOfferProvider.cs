﻿using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.Interfaces
{
    public interface IDepClaimOfferProvider:IDepClaimAuctionInfoProvider
    {        
        long? OfferID { get; }
    }

    public interface IDepClaimOfferProviderEx : IDepClaimOfferProvider 
    {
        DepClaimOffer Offer { get; }
    }
}
