﻿namespace PFR_INVEST.BusinessLogic.Interfaces
{
    /// <summary>
    ///  Интерфейс, указывающий, что низкоуровневое логирование в модели будет отключено (операции save/delete)
    /// </summary>
    public interface IVmNoDefaultLogging
    {       
    }
}