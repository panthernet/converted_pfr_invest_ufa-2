﻿namespace PFR_INVEST.BusinessLogic.Interfaces
{
    public interface IWizardExModel
    {
        /// <summary>
        /// Указывает что визард должен быть закрыт или оставлен по результату диалогового окна с подтверждением закрытия
        /// </summary>
        bool IsClosed { get; set; }
        /// <summary>
        /// Текущий шаг визарда
        /// </summary>
        int Step { get; set; }
        /// <summary>
        /// Финальное действие. Если возвращает False, визард не закрывается
        /// </summary>
        /// <returns></returns>
        bool FinalAction();
        bool OnMoveNext();
        bool OnMovePrev();
        void OnShow();
        void OnInitialization();
        bool CanExecuteNext();
        bool CanExecutePrev();
    }
}
