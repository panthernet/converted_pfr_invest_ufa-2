﻿using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    public class LinkedDocumentItem
    {
        public Attach Attach { get; private set; }

        public LinkedDocumentItem(Attach instance)
        {
            Attach = instance ?? new Attach();
        }

        public string Classification => Attach.AttachClassificID == null ? null : Attach.GetAttachClassific().Name;

        public long ID => Attach.ID;

        public string AdditionalOfContent => Attach.Additional;

        public string Executor => Attach.ExecutorID == null ? Attach.ExecutorName : Attach.GetExecutor().FIOShort;

        public string ExecuteDate => Attach.ExecutionDate?.ToString("dd.MM.yyyy");
    }
}
