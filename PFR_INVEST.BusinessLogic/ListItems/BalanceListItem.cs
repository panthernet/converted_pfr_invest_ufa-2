﻿using System;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Proxy.DataContracts;

namespace PFR_INVEST.BusinessLogic
{
	public class BalanceListItem : DB2BalanceListItem
	{
		public BalanceListItem(DB2BalanceListItem it)
		{
			ACCOUNT = it.ACCOUNT;
			ACCOUNT_TYPE = it.ACCOUNT_TYPE;
			CURRENCY = it.CURRENCY;
			DATE = it.DATE;
			ID = it.ID;
			KIND = it.KIND;
			PORTFOLIO = it.PORTFOLIO;
			SUMM = it.SUMM;
			CURRENCYSUMM = it.CURRENCYSUMM;
			PORTFOLIO_TYPE = it.PORTFOLIO_TYPE;
			PORTFOLIO_YEAR = it.PORTFOLIO_YEAR;
			Ord = it.Ord;
			SubQuery = it.SubQuery;
			Content = it.Content;

			ForeignKey = it.ForeignKey;
		}

		public BalanceListItem(DB2BalanceListItem it, PFRAccountsListItem acc)
			: this(it)
		{
			this.AccountItem = acc;
		}

		/// <summary>
		/// Дополнительная иформация о счёте. Заполняется отдельно.
		/// </summary>
		public PFRAccountsListItem AccountItem { get; private set; }

		public string OperationType => SUMM > 0 ? "Зачисление" : "Списание";

	    public bool IsRur => CURRENCY.Equals("рубли", StringComparison.OrdinalIgnoreCase);

	    public decimal SummInCurrOnly => IsRur ? 0 : CURRENCYSUMM;

	    public decimal SummInRurOnly => IsRur ? SUMM : 0;

	    public string Quarter => DateTools.GetQarterYearInWordsForDate(this.DATE);

	    public string Month => DateTools.GetMonthInWordsForDate(DATE);

	    public string Operation => this.KIND;

	    public DateTime Date => this.DATE;

	    private decimal start;
		public decimal Start
		{
			get
			{
				return start;
			}
			set
			{
				start = value;
			}
		}

		private decimal finish;
		public decimal Finish
		{
			get
			{
				return finish;
			}
			set
			{
				finish = value;
			}
		}

		public decimal Plus
		{
			get
			{

				var d_sum = this.IsRur ? SUMM : CURRENCYSUMM;

				if (d_sum >= 0)
				{
					return Math.Round(d_sum, 2, MidpointRounding.AwayFromZero);
				}

				return 0;
			}
		}

		public decimal Minus
		{
			get
			{

				var d_sum = this.IsRur ? SUMM : CURRENCYSUMM;
				if (d_sum < 0)
				{
					return Math.Round(d_sum, 2, MidpointRounding.AwayFromZero);
				}

				return 0;
			}
		}

		public bool PaintCell => true;
	}
}
