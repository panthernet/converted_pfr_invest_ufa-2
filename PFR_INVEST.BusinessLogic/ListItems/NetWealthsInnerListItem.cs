﻿using PFR_INVEST.Proxy;

namespace PFR_INVEST.BusinessLogic
{
    public class NetWealthsInnerListItem
    {
        public NetWealthsInnerListItem(DBEntity db, string typeField, string currDateField, string startDateField) { }
        public NetWealthsInnerListItem(string typeField, decimal? currDateField, decimal? startDateField)
        {
            this.CurrDateValue = currDateField ?? 0.00m;
            this.NetWealthType = typeField;
            this.StartDateValue = startDateField ?? 0.00m;
        }

        public decimal? CurrDateValue { get; set; }

        public decimal? StartDateValue { get; set; }

        public string NetWealthType { get; set; }
    }
}
