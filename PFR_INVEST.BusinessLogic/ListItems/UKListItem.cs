﻿using PFR_INVEST.Core;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
	using System;

	using DataObjects.ListItems;

	public class UKListItem : ReqTransfer
	{
		public UKListItem(UKListItemHib uKListItemHib)
		{
			uKListItemHib.ReqTransfer.CopyTo(this);
			ContractNumber = uKListItemHib.ContractNumber;
			FormalizedName = uKListItemHib.FormalizedName;
			IsFromUKToPFR = uKListItemHib.IsFromUKToPFR;
			PfrBankAccount = uKListItemHib.PfrBankAccount;
			Portfolio = uKListItemHib.Portfolio;
			RegisterDate = uKListItemHib.RegisterDate;
			RegisterKind = uKListItemHib.RegisterKind;
			RegisterNumber = uKListItemHib.RegisterNumber;
			SPNDirectionName = uKListItemHib.SPNDirectionName;
			SPNOperationName = uKListItemHib.SPNOperationName;
			SIRegisterID = uKListItemHib.SIRegisterID;
			RegisterDate = uKListItemHib.RegisterDate;
			RegisterKind = uKListItemHib.RegisterKind;
			RegisterNumber = uKListItemHib.RegisterNumber;
			CommonPPSum = uKListItemHib.CommonPPSum ?? 0;
			CommonPPNum = uKListItemHib.CommonPPNumber;
			CommonPPDate = uKListItemHib.CommonPPDate;
			CommonPPKBK = uKListItemHib.CommonPPKBK;
		}

		public string Year => SPNDebitDate.HasValue ? SPNDebitDate.Value.Year.ToString() : null;

	    /// <summary>
	    /// Сумма СПН, зависит от направления СПН
	    /// </summary>
	    public decimal? SpnSum => IsFromUKToPFR == true && InvestmentIncome < 0 ? Sum : (Sum ?? 0) - (InvestmentIncome ?? 0);

	    public decimal? SpnFromUK => IsFromUKToPFR == true && InvestmentIncome < 0 ? SumFromUK : (SumFromUK ?? 0) - (InvestmentIncome ?? 0);

	    public string SPNDirectionName { get; private set; }

		public string SPNOperationName { get; private set; }

		public string FormalizedName { get; private set; }

		public string ContractNumber { get; private set; }

		public string Portfolio { get; private set; }

		public string PfrBankAccount { get; private set; }

		public ReqTransfer GetDerived()
		{
			return ((ReqTransfer)this).Copy();
		}

		public decimal? SumToUK => IsFromUKToPFR == false ? Sum : null;

	    public decimal? SumFromUK => IsFromUKToPFR == true ? Sum : null;

	    public bool? IsFromUKToPFR { get; private set; }

		public string RegisterKind { get; private set; }

		public string RegisterNumber { get; private set; }

		public DateTime? RegisterDate { get; private set; }

		public long? SIRegisterID { get; private set; }

		public decimal? CommonPPSum { get; set; }

		public string CommonPPNum { get; set; }

		public DateTime? CommonPPDate { get; set; }

		public long? CommonPPKBK { get; set; }

		public string RegisterData
		{

			get
			{
				const string dispText = "{0} \tдокумент: {1}     \r\n\t№ {2} от: {3}";

				return string.Format(dispText, SIRegisterID,
						RegisterKind,
						RegisterNumber,
						RegisterDate.HasValue ? RegisterDate.Value.ToShortDateString() : string.Empty
					); ;

			}


		}
	}
}
