﻿using System;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
	public class DueListItemClient
	{
		public bool IsDSV { get; private set; } 
		public DueListItemClient(AddSPN due, bool isDsv)
		{
			this.SourceObject = due;
			this.IsDSV = isDsv;
			this.ID = due.ID;
			this.Portfolio = due.GetPortfolio();
			this.SourcePortfolio = null;
			this.Operation = this.GetOperation(due);
			this.RegNum = due.RegNum;
			this.FK_ID = due.ID;
			this.Date = due.NewDate;
			this.Sum = due.Summ ?? 0;
			this.MSumm = this.Sum;
			this.ChSumm = this.Sum;
			this.Precision = string.Empty;// this.GetPrecesion(due);

			this.FakeDate = due.MonthID.HasValue ? new DateTime(1, (int)(due.MonthID.Value), 1) : this.Date.HasValue ? this.Date : null;
			this.YearID = due.YearID;
			if (!this.YearID.HasValue && this.Date.HasValue)
				this.YearID = this.Date.Value.Year - 2000;

			this.IsClosedByKDoc = due.ClosingKDocID.HasValue;
			//this.MonthIndex = (int?)due.MonthID ?? (this.Date.HasValue ? (int?)this.Date.Value.Month : null);
		}

		public DueListItemClient(DopSPN due, bool isDsv)
		{
			this.SourceObject = due;
			this.IsDSV = isDsv;
			this.ID = due.ID;
			this.Portfolio = due.GetPortfolio();
			this.SourcePortfolio = null;
			this.Operation = this.GetOperation(due);
			this.RegNum = due.RegNum;
			this.FK_ID = due.AddSpnID;
			this.Date = due.Date;
			this.Sum = 0;
			this.MSumm = due.MSumm ?? 0;
			this.ChSumm = due.ChSumm ?? 0;
			this.Precision = this.GetPrecesion(due);

			this.FakeDate = new DateTime(1, (int)(due.PeriodID ?? 1), 1);

			if (due.Quarter.HasValue)
				this.FakeDate = new DateTime(1, (int)(due.Quarter ?? 1) * 3, 1);

			this.YearID = due.YearID ?? due.KDOCYearID;
		    if (!this.YearID.HasValue)
		    {
		        if (due.QuarterYear.HasValue)
		            QuarterYear = YearID = due.QuarterYear;
		        else if(this.Date.HasValue)
                    this.YearID = this.Date.Value.Year - 2000;
		    }

		    this.IsClosedByKDoc = due.ClosingKDocID.HasValue;
			//this.MonthIndex = (int?)due.PeriodID;// this.Date.HasValue ? (int?)this.Date.Value.Month : null;
		}

		//public DueListItemClient(Aps due, PortfolioIdentifier.PortfolioTypes type)
		//    : this(due, type, false) { }

		public DueListItemClient(Aps due, bool isDsv, bool isNegative=false)
		{
			this.SourceObject = due;			
			this.ID = due.ID;
			this.Portfolio = due.GetPortfolio();
			this.SourcePortfolio = due.GetSPortfolio();
			this.IsDSV = isDsv;
			this.Operation = this.GetOperation(due);
			this.RegNum = due.RegNum;
			this.FK_ID = due.ID;
			this.Date = due.DocDate;
			this.Sum = due.Summ ?? 0;
			if (isNegative)
			{
				this.Sum = -this.Sum;
				this.NegativePortfolioName = this.Source;
			}
			this.MSumm = this.Sum;
			this.ChSumm = this.Sum;
			this.Precision = string.Empty;// this.GetPrecesion(due);

			this.FakeDate = this.Date;
			if (this.Date.HasValue)
				this.YearID = this.Date.Value.Year - 2000;
			this.IsClosedByKDoc = null;			
		}

		//public DueListItemClient(DopAps due, PortfolioIdentifier.PortfolioTypes type)
		//    : this(due, type, false) { }

		public DueListItemClient(DopAps due, bool isDsv, bool isNegative = false)
		{
			this.SourceObject = due;			
			this.ID = due.ID;
			this.Portfolio = due.GetPortfolio();
			this.SourcePortfolio = due.GetSPortfolio();
			this.IsDSV = isDsv;
			this.Operation = this.GetOperation(due);
			this.RegNum = due.RegNum;
			this.FK_ID = due.ApsID;
			this.Date = due.Date;
			this.Sum = due.Summ ?? 0;
			this.ChSumm = due.ChSumm ?? 0;
			if (isNegative)
			{
				this.Sum = -this.Sum;
				this.ChSumm = -this.ChSumm;
				this.NegativePortfolioName = this.Source;
			}
			this.MSumm = this.Sum;

			this.Precision = this.GetPrecesion(due);

			this.FakeDate = this.Date;
			if (this.Date.HasValue)
				this.YearID = this.Date.Value.Year - 2000;
			this.IsClosedByKDoc = null;			
		}

		public void UpdateOperation()
		{
			this.Operation = this.GetOperation(this.SourceObject);
		}

		private string GetOperation(object source)
		{
			String opName = "";
			Type type = source.GetType();
			if (type == typeof(AddSPN))
			{
				switch (((AddSPN)source).Kind)
				{
					case 1: opName = this.IsDSV ? DueDocKindIdentifier.DueDSV : DueDocKindIdentifier.Due; break;
					case 2: opName = DueDocKindIdentifier.Penalty; break;
				}
			}
			else if (type == typeof(DopSPN))
			{
				switch (((DopSPN)source).DocKind)
				{
					case 1: opName = this.IsDSV ? DueDocKindIdentifier.DueDSV : DueDocKindIdentifier.Due; break;
					case 2: opName = DueDocKindIdentifier.Penalty; break;
					case 3: opName = this.IsDSV ? DueDocKindIdentifier.DueDSV : DueDocKindIdentifier.Due; break;
					case 4: opName = DueDocKindIdentifier.Penalty; break;
				}
			}
			else if (type == typeof(Aps))
			{
				Aps aps = source as Aps;
				switch (aps.KindID)
				{
					case 1: opName = DueDocKindIdentifier.Prepayment; break;
					case 2: opName = this.IsDSV ? DueDocKindIdentifier.DueDeadDSV : DueDocKindIdentifier.DueDead; break;
					case 3: opName = this.IsDSV ? DueDocKindIdentifier.DueExcessDSV : DueDocKindIdentifier.DueExcess; break;
					case 4: opName = this.IsDSV ? DueDocKindIdentifier.DueUndistributedDSV : DueDocKindIdentifier.DueUndistributed; break;
				}
			}
			else if (type == typeof(DopAps))
			{
				switch (((DopAps)source).DocKind)
				{
					case 1: opName = DueDocKindIdentifier.Prepayment; break;
					case 2: opName = this.IsDSV ? DueDocKindIdentifier.DueDeadDSV : DueDocKindIdentifier.DueDead; break;
					case 3: opName = this.IsDSV ? DueDocKindIdentifier.DueExcessDSV : DueDocKindIdentifier.DueExcess; break;
					case 4: opName = this.IsDSV ? DueDocKindIdentifier.DueUndistributedDSV : DueDocKindIdentifier.DueUndistributed; break;
				}
			}
			else if (type == typeof(KDoc))
			{
				if (this.IsPenalty)
					opName = DueDocKindIdentifier.Penalty;
				else
					opName = this.IsDSV ? DueDocKindIdentifier.DueDSV : DueDocKindIdentifier.Due;
			}
			return opName;

		}

		private string GetPrecesion(DopSPN source)
		{
			string retVal = string.Empty;
			switch (source.DocKind)
			{
				case 1: retVal = this.IsDSV ? DueDocKindIdentifier.DueDSVAccurate : DueDocKindIdentifier.DueAccurate; break;
				case 2: retVal = DueDocKindIdentifier.PenaltyAccurate; break;
				case 3: retVal = this.IsDSV ? DueDocKindIdentifier.DueDSVAccurateYear : DueDocKindIdentifier.DueAccurateQuarter; break;
				case 4: retVal = DueDocKindIdentifier.PenaltyAccurateQuarter; break;
			}

			return retVal;
		}

		private string GetPrecesion(DopAps source)
		{
			string retVal = string.Empty;
			switch (source.DocKind)
			{
				case 1: retVal = DueDocKindIdentifier.PrepaymentAccurate; break;
				case 2: retVal = this.IsDSV ? DueDocKindIdentifier.DueDeadAccurateDSV : DueDocKindIdentifier.DueDeadAccurate; break;
				case 3: retVal = this.IsDSV ? DueDocKindIdentifier.DueExcessAccurateDSV : DueDocKindIdentifier.DueExcessAccurate; break;
				case 4: retVal = this.IsDSV ? DueDocKindIdentifier.DueUndistributedAccurateDSV : DueDocKindIdentifier.DueUndistributedAccurate; break;
			}
			return retVal;
		}

        public long? QuarterYear { get; private set; }

		private string GetPrecesion(KDoc source)
		{
			string retVal = string.Empty;
			retVal = DueDocKindIdentifier.Treasurity;
			return retVal;
		}

		public bool IsPenalty { get; set; }

		public int? MonthIndex => this.FakeDate.HasValue ? (int?)this.FakeDate.Value.Month : null;

	    public long? YearID { get; set; }

		public string Year => ((YearID ?? -1999) + 2000).ToString();

	    public object SourceObject { get; private set; }

		public Portfolio Portfolio { get; private set; }

		public long PortfolioID => Portfolio == null ? 0 : Portfolio.ID;

	    public Portfolio SourcePortfolio { get; private set; }

		public string NegativePortfolioName { get; set; }

		public long ID { get; private set; }

		public string PortfolioName
		{
			get
			{
				if (!string.IsNullOrEmpty(this.NegativePortfolioName))
					return this.NegativePortfolioName;
				else
					return this.Portfolio != null ? this.Portfolio.Year : string.Empty;
			}
		}

		public string Quarter => DateTools.GetQarterYearInWordsForDate(this.MonthIndex);

	    public string Month
		{
			get
			{
				if (this.SourceObject is DopSPN && (this.SourceObject as DopSPN).Quarter.HasValue)
				{
					return "Уточнение за квартал";
				}
				else
					return DateTools.GetMonthInWordsForDate(this.MonthIndex);

			}
		}

		public string Operation { get; set; }

		public string RegNum { get; private set; }

		public long? FK_ID { get; set; }

		public int Order { get; set; }

		public string Precision { get; private set; }

		public string Source => this.SourcePortfolio != null ? this.SourcePortfolio.Year : string.Empty;

	    public string GoalPortfolio => this.Portfolio != null ? this.Portfolio.Year : string.Empty;

	    public DateTime? Date { get; private set; }
		public DateTime? FakeDate { get; set; }

		public decimal Supply { get; set; }

		public decimal MSumm { get; set; }

		public Decimal ChSumm { get; set; }

		public decimal Sum { get; set; }

		public bool? IsClosedByKDoc { get; set; }
	}
}
