namespace PFR_INVEST.BusinessLogic
{
    public enum ViewModelState
    {
        /// <summary>
        /// ��������� - ������, �� ��������� �������� � �������������� ������
        /// </summary>
        Read = 0,
        /// <summary>
        /// ��������� ������ - ��������� �������� � �������������� ������
        /// </summary>
        Edit = 1,
        /// <summary>
        /// ��������� - �����, ��������������� ��� �������� ��������� �� ������� ��� ���������� ����������
        /// </summary>
        Create = 2,
        /// <summary>
        /// ��������� - �������
        /// </summary>
        Export = 3
    }
}