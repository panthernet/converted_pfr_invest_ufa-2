﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic.ViewModelsWizards
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    public class DepositCountWizardViewModel: ViewModelCardDialog, IWizardExModel, IRequestCustomAction
    {
        public int Step { get; set; }
        public bool IsClosed { get; set; }

        public DepositCountWizardViewModel()
        {
            IsOnlyOneInstanceAllowed = true;
            IsNotSavable = true;
        }

        public bool FinalAction()
        {
            switch (SelectedAction)
            {
                case 0:
                    return DepClaim2OfferAcceptedListViewModel.ExecuteAction(CreateOffersList);
                case 1:
                    DepositReturnListViewModel.ExecuteAction(ReturnOffersList);
                    ViewModelManager.UpdateDataInAllModels(ReturnOffersList.Select(dop => new KeyValuePair<Type, long>(typeof(Deposit), dop.DepositID)).ToList());
                    DialogHelper.ShowAlert("Депозиты возвращены успешно!");
                    return true;
            }

            return false;
        }

        public bool OnMoveNext()
        {
            if (Step == 2 && SelectedAction == 0)
                return DialogHelper.ShowConfirmation("Будет произведено размещение выбранных депозитов. Продолжить?");
            if (Step == 2 && SelectedAction == 1)
                return DialogHelper.ShowConfirmation("Будет произведен возврат выбранных депозитов. Продолжить?");
            return true;
        }

        public bool OnMovePrev()
        {
            return true;
        }

        public void OnShow()
        {
            switch (Step)
            {
                case 2:
                    if (SelectedAction == 0)
                        OnShowCreateOffers();
                    else OnShowReturnOffers();
                    return;
            }
        }

        public void OnInitialization()
        {
            switch (Step)
            {
                case 2:
                    switch (SelectedAction)
                    {
                        case 1:
                            OnInitializeReturn();
                            return;
                    }
                    return;
            }
        }

        public bool CanExecuteNext()
        {
            switch (Step)
            {
                case 1:
                    return SelectedAction != -1;
                case 2:
                    if (SelectedAction == 0)
                        return CreateOffersList != null && CreateOffersList.Any(item => item.IsSelected);
                    return ReturnOffersList != null && ReturnOffersList.Any(item => item.IsSelected);
            }
            return false;
        }

        public bool CanExecutePrev()
        {
            return true;
        }

        #region Select
        public List<string> ActionList { get; set; } = new List<string> { "Формирование депозитов", "Возврат депозитов" };
        public int SelectedAction { get; set; }
        #endregion

        #region Create

        private List<OfferPfrBankAccountSumListItem> _createOffersList;
        public List<OfferPfrBankAccountSumListItem> CreateOffersList
        {
            get { return _createOffersList; }
            set { _createOffersList = value; OnPropertyChanged("CreateOffersList"); }
        }

        private void OnShowCreateOffers()
        {
            var list = BLServiceSystem.Client.GetAcceptedDepClaimOffers();
            var failAucIdList = BLServiceSystem.Client.GetFailedDepClaimOffersAucIds();
            var idList = list.Where(a => !failAucIdList.Contains(a.Auction.ID)).Select(a=> a.Offer.ID).Distinct().ToArray();

            var sumOffers = BLServiceSystem.Client.GetOfferPfrBankAccountSumForOffers(idList).ToList();
            CreateOffersList = sumOffers;
            //группируем по аукциону
            var gL = from x in CreateOffersList group x by new { x.AuctionID } into p select new { Items = p.ToList() };

            foreach (var g in gL)
            {
                if (g.Items.Count < 1)// Группа нужна если больше 1 элемента
                    continue;
                foreach (var x in g.Items)
                    x.Group = g.Items;
            }
        }

        #endregion

        #region Return

        public ICommand SetPercentCommand { get; set; }
        public event EventHandler RequestCustomAction;
        private void OnRequestCustomAction()
        {
            RequestCustomAction?.Invoke(null, null);
        }

        private void SetPercent(long? offerNumber)
        {
            var list = ReturnOffersList.Where(p => p.OfferNumber == offerNumber && p.IsPercent).ToList();
            if (list.Count == 0) return;

            DialogHelper.ShowEditDepositSetPercent(list);
            list.ForEach(d => { d.AmountActual = d.Amount; d.OnPropertyChanged("AmountActual"); });
            OnRequestCustomAction();
        }

        private List<DepositReturnListItem> _returnOffersList;
        public List<DepositReturnListItem> ReturnOffersList
        {
            get { return _returnOffersList; }
            set { _returnOffersList = value; OnPropertyChanged("ReturnOffersList"); }
        }

        private void OnInitializeReturn()
        {
            SetPercentCommand = new DelegateCommand<long?>(SetPercent);
        }

        private void OnShowReturnOffers()
        {
            var list = BLServiceSystem.Client.GetDepositReturnList(null).Where(d => d.Amount != 0).ToList();
            foreach (var x in list)
            {
                x.IsSelected = x.IsPercent == false || x.IsPercent && x.Amount > 0;
                x.GroupItems = null;
            }
            list = list.Where(p => p.IsSelected).ToList();
            list.ForEach(a=> a.IsSelected = false);
            ReturnOffersList = list;
            //ReturnOffersList.ForEach(a=> a.IsSelected = false);
            //x.DepositID, x.IsPercent
            var gL = from x in ReturnOffersList group x by new { x.AuctionNum } into p select new { Items = p.ToList() };

            foreach (var g in gL)
            {
                if (g.Items.Count <= 1)// Группа нужна если больше 1 элемента
                    continue;

                foreach (var x in g.Items)
                    x.GroupItems = g.Items;
            }
        }

        #endregion

    }
}
