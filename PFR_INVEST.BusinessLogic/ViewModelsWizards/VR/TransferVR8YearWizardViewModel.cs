﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsWizards.VR
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class TransferVR8YearWizardViewModel : TransferWizardViewModel
    {
        /// <summary>
        /// Доступно ли поле выбора Типа операции
        /// </summary>
        public override bool IsOperationTypeEnabled => true;
        public override bool IsImportAvailable { get; } = false;

        private const long OP_VYP_PRAVO_ID = 8L;

        //ID поддерживаемых типов операций
        //private readonly long[] _availableOpIdList = { OP_VYP_PRAVO_ID, 10L, 11L };
        //Список ID наименований контрактов для годового плана ВР
        private readonly int[] _availableContractNames = { 4, 5 };

        public TransferVR8YearWizardViewModel(bool forApsi)
            : base(Document.Types.VR, forApsi)
        {
        }

        protected override void SetVkTranfderType()
        {
            //transferTypes_UKtoPFR = BLServiceSystem.Client.GetSPNOperationsByTypeListHib(3).Where(op => _availableOpIdList.Contains(op.ID)).ToList();
            TransferTypesUKtoPFR = TransferWizardHelper.GetOperationTypesListForYearPlanMasked(1);

            if (TransferTypes_UKtoPFR.Count == 0)
                throw new Exception("Не найдено доступных операций для направления ID = 3");

            SelectedTransferType = TransferTypes_UKtoPFR[0];
        }


        public override void EnsureAvailableList()
        {          
            AvailableContractsList = GetOperationsContracts();
        }

        protected override List<ModelContractRecord> GetOperationsContracts()
        {
            //TODO нужен ли отсев по наименованиям контрактов для новых операций?
            if (SelectedTransferType.ID == OP_VYP_PRAVO_ID)
                return BLServiceSystem.Client.GetContractsForUKsByTypeAndName(_contractType, _availableContractNames).ConvertAll(
                    c => new ModelContractRecord(c)).Where(a=> a.IsActive).ToList();
            return BLServiceSystem.Client.GetContractsForUKsByType(_contractType).ConvertAll(
                c => new ModelContractRecord(c)).Where(a=> a.IsActive).ToList();
        }

        public override string AssignePaymentTitle => SelectedTransferType.Name;
    }
}
