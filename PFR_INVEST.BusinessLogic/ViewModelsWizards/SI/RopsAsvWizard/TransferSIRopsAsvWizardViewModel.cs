﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsWizards.SI.RopsAsvWizard
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class TransferSIRopsAsvWizardViewModel : TransferWizardViewModel
    {
        private DateTime? _startDate = DateTime.Parse($"01.01.{DateTime.Now.Year - 1}");
        private DateTime? _endDate = DateTime.Parse($"31.12.{DateTime.Now.Year - 1}");

        public DateTime? StartDate
        {
            get { return _startDate; }
            set { SetPropertyValue(nameof(StartDate), ref _startDate, value); }
        }

        public DateTime? EndDate
        {
            get { return _endDate; }
            set { SetPropertyValue(nameof(EndDate), ref _endDate, value); }
        }

        private string _RateLabel;
        public string RateLabel
        {
            get { return _RateLabel; }
            set { SetPropertyValue(nameof(RateLabel), ref _RateLabel, value); }
        }
        decimal? _RatioEval;
        decimal? _RatioASV;
        decimal? _RatioROPS;
        public decimal? RatioEval
        {
            get { return _RatioEval; }
            set { SetPropertyValue(nameof(RatioEval), ref _RatioEval, value); }
        }
        public decimal? RatioASV
        {
            get { return _RatioASV; }
            set { SetPropertyValue(nameof(RatioASV), ref _RatioASV, value); }
        }
        public decimal? RatioROPS
        {
            get { return _RatioROPS; }
            set { SetPropertyValue(nameof(RatioROPS), ref _RatioROPS, value); }
        }

        private bool _IsShowRatio;
        //Флаг переключения типа отчисления, true - АСВ, false - РОПС
        public bool IsShowRatio
        {
            get { return _IsShowRatio; }
            set { SetPropertyValue(nameof(IsShowRatio), ref _IsShowRatio, value); }
        }

        public TransferSIRopsAsvWizardViewModel(bool forAPSI = false)
            : base(Document.Types.SI, forAPSI)
        {
            #region ш1 данные
            RatioEval = 0.975M;
            RatioASV = 0.0250M;
            RatioROPS = 0.250M;
            UpdatePage1Fields();
            #endregion

            PropertyChanged += TransferSIRopsAsvWizardViewModel_PropertyChanged;

        }

        private void TransferSIRopsAsvWizardViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(SelectedTransferType))
            {
                UpdatePage1Fields();
            }

        }
        //Управление отображением поля с вводом коэф.расчета, обновления подписи поля ш1
        void UpdatePage1Fields()
        {
            IsShowRatio = SelectedTransferType != null &&
                RopsAsvStringListForMaster[0] == SelectedTransferType.Name;
            RateLabel = IsShowRatio ? "Ставка отчисления в АСВ, %:" : "Ставка отчисления в РОПС, %:";
        }

        //Переопределение фильтрации договоров ш2
        public override void EnsureAvailableList()
        {
            AvailableContractsList = new List<ModelContractRecord>();
            if (SelectedTransferType == null)
                return;
            AvailableContractsList.AddRange(BLServiceSystem.Client.GetSIContractsForPeriod(StartDate.Value, EndDate.Value).ConvertAll(
                c => new ModelContractRecord(c)));
            //base.EnsureAvailableList();
        }

        public decimal GetSCHAByContract(long contrId, DateTime start, DateTime end)
        {
            return BLServiceSystem.Client.GetSCHAForContractPeriod(contrId, start, end);

        }
    }
}
