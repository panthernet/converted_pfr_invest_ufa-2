﻿using System;
using System.Windows.Data;

namespace PFR_INVEST.BusinessLogic.Converters
{
    public class BankRatingConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string data = value as string;
            if (data != null)
                data = data.TrimEnd();
            switch (data)
            {
                case "":
                case null: return "Нет рейтинга";
                case "Отозв": return "Отозван";
                default: return data;
            }

        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is string)
            {
                switch ((string)value)
                {
                    case "Нет рейтинга": return null;
                    case "Отозван": return "Отозв";
                    default: return value;
                }
            }
            return value;
        }
    }
}
