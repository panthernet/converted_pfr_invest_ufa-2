﻿using System;
using System.Windows.Data;

namespace PFR_INVEST.BusinessLogic.Converters
{
    /// <summary>
    /// Returns true if state is create and false if not
    /// </summary>
    public class ViewModelStateToBoolStrictConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool result = true;
            result = !(value is ViewModelState
                     && ((ViewModelState)value == ViewModelState.Create));

            return result;
        }


        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            ViewModelState result;
            if (value is bool && (bool)value)
                result = ViewModelState.Create;
            else
                result = ViewModelState.Read;

            return result;
        }
    }
}
