﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;

namespace PFR_INVEST.BusinessLogic
{
    /// <summary>
    /// Заготовка модели диалога, не использующего стандартные методы ExecuteSave/ExecuteDelete по умолчанию
    /// Автоматическое логирование операций при вызове данных методов отключено
    /// </summary>
    public abstract class ViewModelCardDialog : ViewModelCard, IVmNoDefaultLogging, IRequestCloseViewModel, ICloseViewModelAfterSave
    {
        /// <summary>
        /// Команда для валидации данных
        /// </summary>
        private ICommand ValidationTrigger { get; set; }

        protected ViewModelCardDialog(bool enableValidation = false)
        {
            ConstructorActions(enableValidation);
        }

        protected ViewModelCardDialog(Type pListViewModel, bool enableValidation = false)
            : base(pListViewModel)
        {
            ConstructorActions(enableValidation);
        }

        private void ConstructorActions(bool enableValidation)
        {
            if (enableValidation)
                ValidationTrigger = new DelegateCommand(a=> CanExecuteSave(), a=> {});
            IsValidationEnabled = enableValidation;
        }

        /// <summary>
        /// Запросить закрытие окна представления
        /// </summary>
        /// <param name="isOkResult">Возврашать DialogResult = true</param>
        /// <param name="useModelCloseRequest">Использовать событие RequestClose модели (для диалогов или карточек) или авто закрытие карточки по событию SaveCard (для карточек)</param>
        public void RequestCloseView(bool isOkResult, bool useModelCloseRequest = true)
        {
            if (useModelCloseRequest)
            {
                OnRequestClose(isOkResult);
                return;                
            }
            IsDataChanged = false;

            SaveCard.Execute(null);
        }

        /// <summary>
        /// Включена ли валидация полей модели
        /// </summary>
        public bool IsValidationEnabled { get; private set; }

        protected override void ExecuteSave()
        {
        }

        /// <summary>
        /// Внутренний флаг, указывающий на возможность выполнения команды Save. Использовать вместо метода CanExecuteSave().
        /// </summary>
        protected bool CanExecuteSaveInternal;

        public override bool CanExecuteSave()
        {
            return CanExecuteSaveInternal;
        }

        public override string this[string columnName] => string.Empty;

        public void ExecuteSaveCommand()
        {
            //CanExecuteSaveInternal выставлен в False по умолчанию для скрытия сообщения о сохранении при закрытии формы
            //при принудительном же запуске команды Save, он временно выставляется в True
            var tmp = CanExecuteSaveInternal;
            CanExecuteSaveInternal = true;
            SaveCard.Execute(null);
            CanExecuteSaveInternal = tmp;
        }

        public event EventHandler RequestClose;

        protected void OnRequestClose(bool isOkResult)
        {
            RequestClose?.Invoke(isOkResult, null);
        }
    }
}
