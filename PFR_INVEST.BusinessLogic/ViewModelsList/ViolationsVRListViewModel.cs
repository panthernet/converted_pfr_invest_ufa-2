﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class ViolationsVRListViewModel : ViolationsListViewModel
    {
        protected override void ExecuteRefreshList(object param)
        {
            List = BLServiceSystem.Client.GetReportViolationsByType((int)Document.Types.VR);
        }
    }
}
