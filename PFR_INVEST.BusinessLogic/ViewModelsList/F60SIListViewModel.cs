﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class F60SIListViewModel : F60ListViewModel
    {
        protected override void ExecuteRefreshList(object param)
        {
            CalculateDocNumbers(BLServiceSystem.Client.GetF60ListByTypeContractHib((int)Document.Types.SI));
        }
    }
}
