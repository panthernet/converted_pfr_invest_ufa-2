﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class TDateListViewModel : ViewModelList
    {
        private List<TDate> m_TDateList;
        public List<TDate> TDateList
        {
            get
            {
                RaiseDataRefreshing();
                return m_TDateList;
            }
            set
            {
                m_TDateList = value;
                OnPropertyChanged("TDateList");
            }
        }

        public TDateListViewModel()
        {
            DataObjectTypeForJournal = typeof(TDate);
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }

        protected override void ExecuteRefreshList(object param)
        {
            TDateList = DataContainerFacade.GetList<TDate>();
        }
    }
}
