﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class TransfersArchListViewModel : ViewModelList
    {
        private List<TransferListItem> m_SITransferArchiveList;

        public List<TransferListItem> SITransferArchiveList
        {
            get
            {
                RaiseDataRefreshing();
                return m_SITransferArchiveList;
            }
            set
            {
                m_SITransferArchiveList = value;
                OnPropertyChanged("SITransferArchiveList");
            }
        }

        public virtual Document.Types DocumentType => Document.Types.SI;


        public TransfersArchListViewModel()
        {
            DataObjectTypeForJournal = typeof(Transfer);
        }

        protected override void ExecuteRefreshList(object param)
        {
            var list = BLServiceSystem.Client.GetTransfersListBYContractTypeForUKArchive(this.DocumentType, true, false, false);
            SITransferArchiveList = list.GroupBy(t => t.RegisterID).Select(g =>
            {
                var master = g.First();
                master.Details = g.ToList();
                master.TotalToUK = master.TotalToUK ?? master.Details.Sum(d => d.SumFromPFRToUK);
                master.TotalFromUK = master.TotalFromUK ?? master.Details.Sum(d => d.SumFromUKtoPFR);
                master.TotalInvestdohod = master.TotalInvestdohod ?? master.Details.Sum(d => d.InvestmentIncome);
                return master;
            }).ToList();
           // SITransferArchiveList = BLServiceSystem.Client.GetTransfersListBYContractTypeForUKArchive(this.DocumentType, true, false, false);
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}
