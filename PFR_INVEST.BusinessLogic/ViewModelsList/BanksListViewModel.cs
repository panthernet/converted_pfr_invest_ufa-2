﻿using System;
using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class BanksListViewModel : ViewModelList
    {
        private IList<BankListItem> _mBanksList;
        public IList<BankListItem> BanksList
        {
            get
            {
                RaiseDataRefreshing();
                return _mBanksList;
            }
            set
            {
                _mBanksList = value;
                OnPropertyChanged("BanksList");
            }
        }

        public override Type DataObjectTypeForJournal => typeof (LegalEntity);

        protected override void ExecuteRefreshList(object param)
        {
            BanksList = BLServiceSystem.Client.GetCommonBankListItems();
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}
