﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OUFV_manager)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class VRAssignPaymentsArchiveListViewModel : AssignPaymentsArchiveListViewModel
    {
        //public readonly long OperationId = 8;
        private readonly long[] _availableOpIdList = { 8L, 10L, 11L, 13L };
        protected override List<TransferListItem> GetTransfers()
        {
            return BLServiceSystem.Client.GetTransfersListBYContractType(Document.Types.VR, true, true,false)
               .Where(tr =>( tr.RegisterOperationID == null || _availableOpIdList.Contains(tr.RegisterOperationID.Value)) && tr.MonthID > 0).ToList();
        }


        public VRAssignPaymentsArchiveListViewModel()
        {
            DataObjectTypeForJournal = typeof(SIRegister);
        }
    }
}
