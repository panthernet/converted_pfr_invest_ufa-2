﻿using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    public class SchilsCostsListViewModel : ViewModelList<SchilsCostsListItem>
    {
        //private List<SchilsCostsListItem> m_List = new List<SchilsCostsListItem>();
        //public List<SchilsCostsListItem> List
        //{
        //    get
        //    {
        //        RaiseDataRefreshing();
        //        return m_List;
        //    }
        //    set
        //    {
        //        m_List = value;
        //        OnPropertyChanged("List");
        //    }
        //}

        public SchilsCostsListViewModel()
        {
            DataObjectTypeForJournal = typeof(Budget);
            DataObjectDescForJournal = "Композитная таблица на основании информации о Бюджете";
        }

        protected override void ExecuteRefreshList(object param)
        {
            var list = BLServiceSystem.Client.GetSchilsCostsList();

            //отделяем трансферы
            var transfers = list.Where(l => l.Operation3Type == SchilsCostsListItem.SchilsCostsListItemOpertion2Type.Transfer);
            //отделяем создания
            var budgets = list.Where(l => l.Operation3Type == SchilsCostsListItem.SchilsCostsListItemOpertion2Type.BudgetCreation);

            list = list
                .Except(transfers)
                //.Except(budgets)
                .OrderBy(l => l.BudgetID).ThenBy(l => l.Date).ToList();
            
            //Создание бюджетов ставим перед любыми их проводками
            budgets.ToList().ForEach(b => {
                var first = list.FirstOrDefault(l => l.BudgetID == b.BudgetID && l.Operation2Type != SchilsCostsListItem.SchilsCostsListItemOpertion2Type.BudgetCreation);
                if (first != null)
                {
                    list.Remove(b);
                    list.Insert(list.IndexOf(first), b);
                }
            });

            //Трансферы вставляем после их распоряжений
            list.Where(l => l.Operation3Type == SchilsCostsListItem.SchilsCostsListItemOpertion2Type.Direction).ToList().ForEach(l =>
            {
                var dirTrans = transfers.Where(t => t.DirectionID == l.DirectionID).OrderBy(t => t.Date).ToList();
                list.InsertRange(list.IndexOf(l) + 1, dirTrans);
            });

            this.List = list;

        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}
