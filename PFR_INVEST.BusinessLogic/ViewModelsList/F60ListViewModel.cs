﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class F60ListViewModel : ViewModelList
    {
        private List<F060DetailsListItem> m_F60List = new List<F060DetailsListItem>();
        public List<F060DetailsListItem> F60List
        {
            get
            {
                return m_F60List;
            }
            set
            {
                m_F60List = value;
                OnPropertyChanged("F60List");
            }
        }

        public F60ListViewModel()
        {
            DataObjectTypeForJournal = typeof(EdoOdkF060);
        }

        protected override void ExecuteRefreshList(object param)
        {
            CalculateDocNumbers(BLServiceSystem.Client.GetF60ListHib());
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
        protected void CalculateDocNumbers(List<F060DetailsListItem> newList)
        {
            this.F60List=newList;

            const string sFormat = "{0}-{1}";
            var dict = new Dictionary<string, List<F060DetailsListItem>>();
            this.F60List.ForEach(i =>
            {
                string key = i.F060 != null ? string.Format(sFormat, i.F060.YearId.HasValue ? i.F060.YearId.Value.ToString() : string.Empty, i.F060.Quartal.HasValue ? i.F060.Quartal.Value.ToString() : string.Empty) : string.Format(sFormat, "0", "0");
                if (!dict.ContainsKey(key))
                    dict[key] = new List<F060DetailsListItem>();
                dict[key].Add(i);
            });
            foreach (var l in dict)
            {
                int cnt = l.Value.Count;
                l.Value.ForEach(i =>
                {
                    i.lContractNumbers = cnt;
                });
            }
            OnPropertyChanged("F60List");
        }
    }
}
