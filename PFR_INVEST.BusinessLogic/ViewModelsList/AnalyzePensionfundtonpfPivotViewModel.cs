﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Collections;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects.Analyze;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.User)]
    public class AnalyzePensionfundtonpfPivotViewModel : ViewModelList<AnalyzePensionfundtonpfReport>
    {
        public string FormTitle
            => IsWithSubtraction
                ? "Форма 3. Средства пенсионных накоплений, переданные в управляющие компании и НПФ за вычетом возврата в ПФР "
                : "Форма 2. Средства пенсионных накоплений, переданные в управляющие компании и НПФ без учета возврата в ПФР "
            ;

        public string FormTitleWithMeasure => $"{FormTitle}, (млн.руб)";
        private List<AnalyzePensionfundtonpfData> _ReportDataCache;
        public RangeObservableCollection<AnalyzePensionfundtonpfData> ReportData { get; private set; }

        private bool _isWithSubtraction;

        public bool IsWithSubtraction
        {
            get { return _isWithSubtraction; }
            set { SetPropertyValue(nameof(IsWithSubtraction), ref _isWithSubtraction, value); }
        }

        private int _subtParam;

        public AnalyzePensionfundtonpfPivotViewModel(bool withSubt = false)
        {
            _isWithSubtraction = withSubt;
            _subtParam = withSubt ? 1 : 0;
            ReportData = new RangeObservableCollection<AnalyzePensionfundtonpfData>();
            ExecuteRefreshList(null);
        }

        protected override void ExecuteRefreshList(object param)
        {
            if (ReportData != null)
            {
                var data = DataContainerFacade.GetClient().GetPensionfundDataByYearKvartal(subtraction: _subtParam);
                //List<AnalyzePensionfundtonpfData> data = new List<AnalyzePensionfundtonpfData>();
                //foreach (var rp in reports)
                //{
                //    data.AddRange(DataContainerFacade.GetClient().GetPensionfundDataByReportId(rp.Id));
                //}

                data.ForEach(d => d.QuarkText = QuarkDisplayHelper.GetQuarkDisplayValue(d.Kvartal));
                ReportData.Fill(data);
                RaiseDataRefreshing();
            }
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}