﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Common.Logger;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_directory_editor, DOKIP_ROLE_TYPE.OARRS_directory_editor, DOKIP_ROLE_TYPE.OFPR_directory_editor, DOKIP_ROLE_TYPE.OKIP_worker)]
    public class BankAccountsListViewModel : ViewModelList
    {
        private IList<BankAccount> m_BankAccountsList;
        public IList<BankAccount> BankAccountsList
        {
            get
            {
                RaiseDataRefreshing();
                return m_BankAccountsList;
            }
            set
            {
                m_BankAccountsList = value;
                OnPropertyChanged("BankAccountsList");
            }
        }

        public BankAccountsListViewModel()
        {
            DataObjectTypeForJournal = typeof (BankAccount);
        }

        protected override void ExecuteRefreshList(object param)
        {
            Logger.WriteLine($"Npf list refresh: {BLServiceSystem.Client != null}");
            BankAccountsList = BLServiceSystem.Client.GetBankAccountListByTypeHib(new string[] { "НПФ" });
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}
