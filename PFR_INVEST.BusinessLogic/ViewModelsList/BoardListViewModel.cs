﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Analyze;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class BoardListViewModel : ViewModelListObservable<Board>
    {
        public BoardListViewModel()
        {
            IsSaveOnCurrentChanged = true;
            DataObjectTypeForJournal = typeof(Board);
        }

        protected override void ExecuteRefreshList(object param)
        {
            List.Fill(DataContainerFacade.GetList<Board>());
            SetFirstAsCurrent();
        }

    }
}
