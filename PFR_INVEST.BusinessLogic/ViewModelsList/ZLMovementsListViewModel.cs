﻿using System.Collections.Generic;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    //[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    //[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class ZLMovementsListViewModel : ViewModelList
    {
        public class ZLMovementItemComparer : IComparer<ZLMovementListItem>
        {
        	#region Implementation of IComparer<in ZLMovementListItem>

        	public int Compare(ZLMovementListItem x, ZLMovementListItem y)
        	{
        		if (x.OperationDate == null && y.OperationDate == null)
        			return 0;

				if (x.OperationDate == null)
					return -1;

				if (y.OperationDate == null)
					return 1;

        		return x.OperationDate.Value.CompareTo(y.OperationDate.Value);
        	}

        	#endregion
        }
		
		private List<ZLMovementListItem> m_ZLMovements;
        public List<ZLMovementListItem> ZLMovements
        {
            get
            {
                RaiseDataRefreshing();
                return m_ZLMovements;
            }
            set
            {
                m_ZLMovements = value;
                OnPropertyChanged("ZLMovements");
            }
        }

        public ZLMovementsListViewModel()
        {
            DataObjectTypeForJournal = typeof(SITransfer);
        }

        protected override void ExecuteRefreshList(object param)
        {
            var movements = BLServiceSystem.Client.GetZLMovementsListHib();

        	var groupedMovements = GroupByContractNumber(movements);
        	ZLMovements = Join(groupedMovements);
        }

        protected Dictionary<string, List<ZLMovementListItem>> GroupByContractNumber(IEnumerable<ZLMovementListItem> movements)
		{
			var groupedMovements = new Dictionary<string, List<ZLMovementListItem>>();
			foreach (var movement in movements)
			{
				List<ZLMovementListItem> contractMovements;
				if (!groupedMovements.TryGetValue(movement.ContractNumber, out contractMovements))
				{
					contractMovements = new List<ZLMovementListItem>();
					groupedMovements.Add(movement.ContractNumber, contractMovements);
				}
				contractMovements.Add(movement);
			}
			return groupedMovements;
		}


        protected List<ZLMovementListItem> Join(Dictionary<string, List<ZLMovementListItem>> groupedMovements)
		{
			var movements = new List<ZLMovementListItem>();
			foreach (var contractNumber in groupedMovements.Keys)
			{
				movements.AddRange(groupedMovements[contractNumber]);
			}
			return movements;
		}

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}
