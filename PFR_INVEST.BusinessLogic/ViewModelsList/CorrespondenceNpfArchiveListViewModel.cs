﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OKIP_worker)]
    public class CorrespondenceNpfArchiveListViewModel : CorrespondenceNpfBaseListViewModel
    {
        protected override void ExecuteRefreshList(object param)
        {
            base.ExecuteRefreshList(Document.Statuses.Executed);
        }

    }
}
