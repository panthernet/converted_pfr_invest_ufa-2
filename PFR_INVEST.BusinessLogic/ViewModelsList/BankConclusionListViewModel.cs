﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class BankConclusionListViewModel : ViewModelList
    {
        private IList<BankConclusion> m_BankConclusionList;
        public IList<BankConclusion> BankConclusionList
        {
            get
            {
                RaiseDataRefreshing();
                return m_BankConclusionList;
            }
            set
            {
                m_BankConclusionList = value;
                OnPropertyChanged("BankConclusionList");
            }
        }

        public BankConclusionListViewModel()
        {
            DataObjectTypeForJournal = typeof(BankConclusion);
        }

        protected override void ExecuteRefreshList(object param)
        {
            this.BankConclusionList = BLServiceSystem.Client.GetBankConclusionList();
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}
