﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    public class RegistersArchiveListViewModel : PagedLoadObservableListModelBase<RegistersListItem>, ISettingOpenForm, IUpdateListenerModel, IRequestCustomAction
    {
		//private List<RegistersListItem> m_RegistersList;
		//public List<RegistersListItem> RegistersList
		//{
		//    get
		//    {
		//        return m_RegistersList;
		//    }
		//    set
		//    {
		//        m_RegistersList = value;
		//        OnPropertyChanged("RegistersList");
		//    }
		//}
        public event EventHandler RequestCustomAction;

        protected void OnRequestSelectId(long id, bool isFinregister)
        {
            if (RequestCustomAction != null)
                RequestCustomAction(new object[] { isFinregister, id }, null);
        }
		public OpenSettingsProvider SettingsProvider { get; set; }

        public RegistersArchiveListViewModel()
        {
            DataObjectTypeForJournal = typeof(Register);
        }

		protected override void BeforeListRefresh()
		{
			if (SettingsProvider == null)
				SettingsProvider = new OpenSettingsProvider("RegistersArchiveListViewModel", "Работа с НПФ - Передача СПН - Архив реестров", (s) => ExecuteRefreshList(null));
		}

		protected override long GetListCount()
		{
			return BLServiceSystem.Client.GetRegistersListCountPeriod(true, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
		}

		protected override List<RegistersListItem> GetListPart(long index)
		{
			return BLServiceSystem.Client.GetRegistersListPeriodByPage(true, (int)index, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
		}

		Type[] IUpdateListenerModel.UpdateListenTypes => new[] { typeof(Finregister), typeof(Register) };

        void IUpdateListenerModel.OnDataUpdate(Type type, long id)
		{
			if (type == typeof(Finregister))
			{
				var items = BLServiceSystem.Client.GetRegistersListPeriodByPage(true, 0, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd, id);
				var old = List.Where(r => r.ID == id).ToList();

				UpdateRecord(old, items);
                if (items.Any())
                    OnRequestSelectId(items.First().ID, true);
                return;
			}
		    if (type == typeof (Register))
		    {
                var items = BLServiceSystem.Client.GetRegistersListPeriodByPageForRegister(true, 0, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd, id);
                var old = List.Where(r => r.RegisterID == id).ToList();
                UpdateRecord(old, items);
                if (items.Any())
                    OnRequestSelectId(items.First().ID, false);
                return;
		    }
		}
	
		
    }
}
