﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator)]
	public class TemplatesListViewModel : ViewModelList<TemplateListItem>
	{
		private List<Department> DepList { get; set; }

		public TemplatesListViewModel()
		{
			DataObjectTypeForJournal = typeof(Template);

			UploadTemplate = new DelegateCommand(o => CanExecuteUploadTemplate(),
				o => ExecuteUploadTemplate());
		}

		protected override void ExecuteRefreshList(object param)
		{
			DepList = DataContainerFacade.GetList<Department>();
			List = BLServiceSystem.Client.GetTemplatesList();
		}

		protected override bool CanExecuteRefreshList()
		{
			return true;
		}

		public ICommand UploadTemplate { get; protected set; }
		private void ExecuteUploadTemplate()
		{
			if (DialogHelper.UploadTemplate())
			{
				RefreshList.Execute(null);
			}
		}

		private bool CanExecuteUploadTemplate()
		{
			return true;
		}

		private CommandBinding _DeleteCommandBinding;
		public CommandBinding DeleteCommandBinding
		{
			get
			{
				if (_DeleteCommandBinding == null)
					_DeleteCommandBinding = new CommandBinding(DeleteTemplate, DeleteTemplate_Executed, DeleteTemplate_CanExecute);
				return _DeleteCommandBinding;
			}
		}

		private RoutedUICommand _DeleteTemplate;
		public RoutedUICommand DeleteTemplate
		{
			get
			{
				if (_DeleteTemplate == null)
					_DeleteTemplate = new RoutedUICommand("Delete an existing template", "DeleteTemplate", this.GetType());
				return _DeleteTemplate;
			}
		}

		public void DeleteTemplate_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			if (e.Parameter != null)
			{
				object[] param = (object[])e.Parameter;
				if (param == null) return;

				List<long> ID = (List<long>)param[0];

				if (!DialogHelper.ConfirmDeletingTemplates())
					return;

				foreach (int id in ID)
				{
					var tmpl = DataContainerFacade.GetByID<Template>(id);
					if (tmpl != null)
					{
						DataContainerFacade.Delete(tmpl);
					}
				}

				RefreshList.Execute(null);
			}
		}

		public void DeleteTemplate_CanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			object[] param = (object[])e.Parameter;

			if (param == null || (bool)param[1] == false)
				e.CanExecute = false;
			else e.CanExecute = true;
		}

		private CommandBinding _OpenCommandBinding;
		public CommandBinding OpenCommandBinding => _OpenCommandBinding ?? (_OpenCommandBinding = new CommandBinding(OpenTemplate, OpenTemplate_Executed, OpenTemplate_CanExecute));

	    private RoutedUICommand _OpenTemplate;
		public RoutedUICommand OpenTemplate => _OpenTemplate ?? (_OpenTemplate = new RoutedUICommand("Open an existing template", "OpenTemplate", this.GetType()));

	    public void OpenTemplate_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			if (e.Parameter == null) return;
			long ID = (long)e.Parameter;

			var template = List.FirstOrDefault(item => item.ID == ID);
			if (template != null)
			{
				string file = TemplatesManager.ExtractTemplate(template.FileName);
				if (!string.IsNullOrEmpty(file))
				{
					try
					{
						Process.Start(file);
					}
					catch
					{
						DialogHelper.ShowError("При открытии файла шаблона '{0}' возникла ошибка.\nВозможно файл занят другим процессом.", file);
					}
				}
			}
		}

		public void OpenTemplate_CanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			e.CanExecute = e.Parameter != null;
		}
	}
}
