﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_directory_editor, DOKIP_ROLE_TYPE.OARRS_directory_editor)]
    public class MissedRatesListViewModel : ViewModelList
    {
        private List<RatesListItem> m_RatesList;
        public List<RatesListItem> RatesList
        {
            get
            {
                return m_RatesList;
            }
            set
            {
                m_RatesList = value;
                OnPropertyChanged("RatesList");
            }
        }

        public MissedRatesListViewModel()
        {
            DataObjectTypeForJournal = typeof(object);            
            DataObjectDescForJournal = "Сводка из таблиц CURRENCY и CURS";
        }

        protected override void ExecuteRefreshList(object param)
        {
            RatesList = BLServiceSystem.Client.GetMissedRatesListHib();
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}
