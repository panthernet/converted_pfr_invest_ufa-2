﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_directory_editor, DOKIP_ROLE_TYPE.OARRS_directory_editor)]
    public class DepClaimSelectParamsListViewModel : ViewModelList,
                                                                    IDepClaimOfferProvider,
                                                                    IDepClaimMaxExportProvider,
                                                                    IDepClaimMaxImportProvider,
                                                                    IDepClaimOfferCreateProvider,
                                                                    IDepClaim2ImportProvider,
                                                                    IDepClaim2ConfirmImportProvider,
                                                                    IDepClaimAuctionExportProvider,
                                                                    IDepClaim2ExportProvider,
                                                                    IDepClaimAuctionSourceProvider,
                                                                    IDepClaim2ConfirmExportProvider,
                                                                    IDepClaimStockProvider, IUpdateListenerModel
    {

        private List<DepClaimSelectParams> _depClaimSelectParamsList;
        public List<DepClaimSelectParams> DepClaimSelectParamsList
        {
            get
            {
                RaiseDataRefreshing();
                return _depClaimSelectParamsList;
            }
            set
            {
                _depClaimSelectParamsList = value;
                OnPropertyChanged("DepClaimSelectParamsList");
            }
        }

        public DepClaimSelectParamsListViewModel()
        {
            DataObjectTypeForJournal = typeof(DepClaimSelectParams);
        }

        public override bool CanExecuteDelete()
        {
            return CheckDeleteAccess(typeof(DepClaimSelectParamsViewModel));
        }

        protected override void ExecuteRefreshList(object param)
        {
            DepClaimSelectParamsList = BLServiceSystem.Client.GetDepClaimSelectParamsList();
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }

        public bool CanGenerateOfferts
        {
            get
            {
                if (ActiveAuction == null)
                    return false;

                if (ActiveAuction.AuctionStatus == DepClaimSelectParams.Statuses.DepClaimConfirmImported)
                    return true;

                if (ActiveAuction.AuctionStatus == DepClaimSelectParams.Statuses.OfferCreated && _activeAuctionOfferts.All(item => item.Offer.Status == DepClaimOffer.Statuses.NotSigned))
                    return true;

                return false;
            }
        }

        DepClaimSelectParams _activeAuction;
        List<DepClaimOfferListItem> _activeAuctionOfferts;

        public DepClaimSelectParams ActiveAuction
        {
            get
            {
                return _activeAuction;
            }
            set
            {
                _activeAuction = value;
                _activeAuctionOfferts = value == null ? new List<DepClaimOfferListItem>() : BLServiceSystem.Client.GetDepClaimOfferListByAuctionId(_activeAuction.ID);
            }
        }

        public long? AuctionID => ActiveAuction?.ID;


        long? IDepClaimAuctionProvider.AuctionID => AuctionID;

        long? IDepClaimOfferProvider.OfferID => null;

        DepClaimSelectParams.Statuses? IDepClaimAuctionInfoProvider.AuctionStatus => ActiveAuction?.AuctionStatus;

        bool IDepClaimMaxExportProvider.IsMaxClaim => true;

        bool IDepClaimMaxExportProvider.IsCommonClaim => true;

        int? IDepClaimAuctionFullInfoProvider.DepClaimMaxCount => ActiveAuction?.DepClaimMaxCount;

        int? IDepClaimAuctionFullInfoProvider.DepClaimCommonCount => ActiveAuction?.DepClaimCommonCount;

        bool IDepClaimStockProvider.IsMoscowStockSelected => ActiveAuction != null && ActiveAuction.StockId == (long)Stock.StockID.MoscowStock;

        bool IDepClaimStockProvider.IsSPVBStockSelected => ActiveAuction != null && ActiveAuction.StockId == (long)Stock.StockID.SPVB;

        public Type[] UpdateListenTypes => new[] {typeof(DepClaimSelectParams)};
        public void OnDataUpdate(Type type, long id)
        {
            var old = DepClaimSelectParamsList.FirstOrDefault(a => a.ID == id);
            var n = DataContainerFacade.GetByID<DepClaimSelectParams>(id);
            if (old == null && n != null)
            {
                DepClaimSelectParamsList.Add(n);
            }
            else if(old != null && n != null)
            {
                var index = DepClaimSelectParamsList.IndexOf(old);
                DepClaimSelectParamsList.Remove(old);
                DepClaimSelectParamsList.Insert(index, n);
            } else if (old != null)
                DepClaimSelectParamsList.Remove(old);
            OnPropertyChanged(nameof(DepClaimSelectParamsList));
        }
    }
}
