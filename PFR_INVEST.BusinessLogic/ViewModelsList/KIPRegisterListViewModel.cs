﻿using System;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Attributes;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    [ModelEntity(Type = typeof(KipRegister), JournalDescription = "Журнал реестров ПТК КИП")]
	public class KIPRegisterListViewModel : ViewModelList<KIPRegisterListItem>
	{

		private DateTime? dateFrom = DateTime.Now.AddYears(-1);
		public DateTime? DateFrom
		{
			get { return dateFrom; }
			set
			{
				if (dateFrom != value)
				{
					dateFrom = value;
					OnPropertyChanged("DateFrom");
				}
			}
		}

		private DateTime? _dateTo = DateTime.Now;
		public DateTime? DateTo
		{
			get { return _dateTo; }
			set
			{
				if (_dateTo != value)
				{
					_dateTo = value;
					OnPropertyChanged("DateTo");
				}
			}
		}



		public bool UserIsAdministrator => AP.Provider.CurrentUserSecurity.CheckUserInRole(DOKIP_ROLE_TYPE.Administrator);

	    protected override void ExecuteRefreshList(object param)
		{
		List = BLServiceSystem.Client.GetKIPRegisterList(DateFrom ?? DateTime.MinValue, DateTo ?? DateTime.MaxValue);

		}
	}
}
