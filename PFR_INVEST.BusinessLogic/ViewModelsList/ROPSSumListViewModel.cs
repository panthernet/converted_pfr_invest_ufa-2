﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	public class ROPSSumListViewModel : ViewModelListObservable<ROPSSum>, IUpdateListenerModel
	{
		public ROPSSumListViewModel()
		{
			DataObjectDescForJournal = "Распоряжения РОПС";
		}

		protected override void ExecuteRefreshList(object param)
		{
			List.Fill(DataContainerFacade.GetList<ROPSSum>());
		}

		public Type[] UpdateListenTypes => new[] { typeof(ROPSSum)					
		};

	    public void OnDataUpdate(Type type, long id)
		{
			if (type == typeof(ROPSSum))
			{
				var oldItem = List.FirstOrDefault(r => r.ID == id);
				var newItem = DataContainerFacade.GetByID<ROPSSum>(id);

				base.UpdateRecord(oldItem, newItem);
			}
		}
	}
}

