﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class KBKListViewModel : ViewModelList
    {
        private List<KBK> _list;
        public List<KBK> List
        {
            get
            {
                RaiseDataRefreshing();
                return _list;
            }
            set
            {
                _list = value;
                OnPropertyChanged("List");
            }
        }

        private KBK _selectedKbk;

        public KBK SelectedKBK
        {
            get { return _selectedKbk; }
            set
            {
                _selectedKbk = value;
                OnPropertyChanged("SelectedKBK");
            }
        }

        public KBKListViewModel()
        {
            DataObjectTypeForJournal = typeof(KBK);           
        }

        protected override void ExecuteRefreshList(object param)
        {
            List = DataContainerFacade.GetList<KBK>().ToList();
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}
