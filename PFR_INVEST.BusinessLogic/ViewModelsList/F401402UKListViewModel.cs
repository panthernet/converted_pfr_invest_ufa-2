﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.ListExtensions;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OUFV_manager)]
    public class F401402UKListViewModel : PagedLoadListModelBase<F401402UKListItem>, IUpdateListenerModel
    {
        public virtual bool ShowFilter => false;

        public F401402UKListViewModel(Document.Types contractType)
            : base(contractType)
        {
            DataObjectTypeForJournal = typeof(EdoOdkF401402);
        }


        protected override long GetListCount()
        {
            return BLServiceSystem.Client.GetCountF401402((Document.Types)RefreshParameter);
        }

        protected override List<F401402UKListItem> GetListPart(long index)
        {
            return BLServiceSystem.Client.GetF401402ListByPage((Document.Types)RefreshParameter, (int)index);
        }

        protected override void BeforeListRefresh()
        {
            OnDataRefreshed += (sender, e) => Update(TmpList);
        }

        protected void Update(List<F401402UKListItem> part)
        {
            var w = Stopwatch.StartNew();
            var filteredItems = new List<F401402UKListItem>();

            part.GroupBy(x => x.EdoID).ForEach(x => filteredItems.AddRange(x.GroupBy(y => y.DogSDNum).First()));
            /*var groupedItems = part.GroupBy(x => x.EdoID).Select(x => x.GroupBy(y => y.DogSDNum)).ToList();
            // внутри группы по EdoId выбирается каждая первая запись во внутренней группе по DogSDNum
            groupedItems.ForEach(x => filteredItems.AddRange(x.Select(byDogSDNum => byDogSDNum.First())));
            */
            // заполнение счетчиков по годам и месяцам
            var groupedByDate = filteredItems.GroupBy(x => x.Month + x.Year);
            groupedByDate.ForEach(x => x.ForEach(y => y.MonthWithSubitemsCount = $"{y.Month}  ({x.Count()})"));
            /*
            var groupedByDate = filteredItems.GroupBy(x => x.Month + x.Year).ToList();
            groupedByDate.ForEach(x => x.ToList().ForEach(y => y.MonthWithSubitemsCount = string.Format("{0}  ({1})", y.Month, x.Count())));*/
            Debug.WriteLine("UPD: "+w.Elapsed);
            w.Stop();
        }

        public Type[] UpdateListenTypes => new[] { typeof(F401402UKListItem) };

        public void OnDataUpdate(Type type, long id)
        {
            if (type != typeof(F401402UKListItem))
                return;
            var data = BLServiceSystem.Client.GetF401402ListByID((Document.Types)RefreshParameter, id);
            foreach (var i in data)
            {
                var inList = List.FirstOrDefault(x => x.ID == i.ID);
                if (inList == null)
                {
                    List.Add(i);
                }
                else
                {
                    // надо заменить значения полей, но при этом оставить элемент в том же месте последовательности
                    int index = List.IndexOf(inList);
                    List[index] = i;
                }
            }
            Update(TmpList);
            OnPropertyChanged("List");
        }
    }
}
