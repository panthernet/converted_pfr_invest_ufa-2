﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Collections;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Analyze;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.User)]
    public class AnalyzeReportPivotFacade<T> : ViewModelBase where T : AnalyzeReportDataBase
    {
        private int? _SelectedYear;

        public int? SelectedYear
        {
            get { return _SelectedYear; }
            set
            {
                if (SetPropertyValue(nameof(SelectedYear), ref _SelectedYear, value))
                    refreshCacheData();
            }
        }

        private int? _SelectedKvartal;

        public int? SelectedKvartal
        {
            get { return _SelectedKvartal; }
            set
            {
                if (SetPropertyValue(nameof(SelectedKvartal), ref _SelectedKvartal, value))
                    refreshCacheData();
            }
        }

        public RangeObservableCollection<T> ReportData { get; private set; }

        private List<T> _ReportDataCache;

        public RangeObservableCollection<int> Years { get; set; }
        public List<Tuple<int, string>> Kvartals { get; set; }

        private Action _afterAfterRefreshCache;
        private Func<List<T>> _refreshDataDB;

        public AnalyzeReportPivotFacade(Action afterRefreshCache, Func<List<T>> refreshDataDB)
        {
            if (afterRefreshCache == null)
                throw new NullReferenceException("refreshDataCache is null");
            if (refreshDataDB == null)
                throw new NullReferenceException("refreshDataDB is null");

            ReportData = new RangeObservableCollection<T>();
            _ReportDataCache = new List<T>();

            _afterAfterRefreshCache = afterRefreshCache;
            _refreshDataDB = refreshDataDB;

            RefreshDBData();

            Years =
                new RangeObservableCollection<int>(
                    _ReportDataCache.GroupBy(c => c.Year).Where(c => c.Key > 0).Select(g => g.Key).OrderBy(x => x));
            Kvartals = new List<Tuple<int, string>>();
            foreach (var qr in DataContainerFacade.GetList<Quark>())
            {
                Kvartals.Add(new Tuple<int, string>(qr.Index, qr.Name));
            }
        }

        void refreshCacheData()
        {
            if (SelectedYear.HasValue && SelectedKvartal.HasValue)
            {
                ReportData.Fill(_ReportDataCache.Where(r => (r.Year == SelectedYear && r.Kvartal == SelectedKvartal)));
                _afterAfterRefreshCache?.Invoke();
            }
        }

        public void RefreshDBData()
        {
            var data = _refreshDataDB();
            data.ForEach(d => d.QuarkText = QuarkDisplayHelper.GetQuarkDisplayValue(d.Kvartal));
            _ReportDataCache.Clear();
            _ReportDataCache.AddRange(data);
            ReportData.Clear();
            refreshCacheData();
            if (Years != null) //Обновление списка годов
            {
                Years.Clear();
                Years.AddRange(
                    _ReportDataCache.GroupBy(c => c.Year).Where(c => c.Key > 0).Select(g => g.Key).OrderBy(x => x));
            }
        }
    }
}