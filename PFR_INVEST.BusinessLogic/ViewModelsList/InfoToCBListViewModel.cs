﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OARRS_manager)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OARRS_manager)]
    public class InfoToCBListViewModel: ViewModelList<BOReportForm1>, IUpdateListenerModel
    {
        private BOReportForm1 _selectedItem;

        public BOReportForm1 SelectedItem
        {
            get { return _selectedItem; }
            set { _selectedItem = value; OnPropertyChanged(nameof(SelectedItem)); }
        }

        protected override void ExecuteRefreshList(object param)
        {
            List = new ObservableCollection<BOReportForm1>(BLServiceSystem.Client.GetGeneralBOReportForm1List(BOReportForm1.ReportTypeEnum.All, true));
        }

        public Type[] UpdateListenTypes => new[] {typeof(BOReportForm1)};
        public void OnDataUpdate(Type type, long id)
        {
            var old = List.FirstOrDefault(a => a.ID == id);
            var n = DataContainerFacade.GetByID<BOReportForm1>(id);
            var index = -1;
            if (old != null)
            {
                index = List.IndexOf(old);
                List.Remove(old);
            }
            if (n != null)
            {
                if (index == -1) List.Add(n);
                else List.Insert(index, n);
            }
        }
    }
}
