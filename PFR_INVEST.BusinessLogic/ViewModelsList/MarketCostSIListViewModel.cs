﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class MarketCostSIListViewModel : MarketCostListViewModel, ISettingOpenForm
    {
		protected override Document.Types DocType => Document.Types.SI;

        //public OpenSettingsProvider SettingsProvider { get; set; }

		//protected override long GetListCount()
		//{
		//    F020ListCount = BLServiceSystem.Client.GetCountMarketCostF020Filtered((int)DocType, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
		//    F025ListCount = BLServiceSystem.Client.GetCountMarketCostF025Filtered((int)DocType, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
		//    return F020ListCount + F025ListCount;

		//}

		//protected override List<MarketCostListItem> GetListPart(long index)
		//{
		//    return index < F020ListCount ?
		//        BLServiceSystem.Client.GetMarketCostF020ListFilteredByPage((int)DocType, (int)index, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd).ToList() :
		//        BLServiceSystem.Client.GetMarketCostF025ListFilteredByPage((int)DocType, (int)(index - F020ListCount), SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd).ToList();

		//}

		//protected override void BeforeListRefresh()
		//{
		//    base.BeforeListRefresh();
		//    if (this.SettingsProvider == null)
		//        this.SettingsProvider = new OpenSettingsProvider("MarketCostSIListViewModel", "Работа с СИ - Отчеты УК - Рыночная стоимость активов (РСА)", (s) => this.ExecuteRefreshList(null));

		//}
    }
}
