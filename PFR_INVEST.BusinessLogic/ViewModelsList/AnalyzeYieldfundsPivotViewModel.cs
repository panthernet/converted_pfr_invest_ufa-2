﻿using System;
using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Collections;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects.Analyze;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OFPR_worker,
        DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.User)]
    public class AnalyzeYieldfundsPivotViewModel : ViewModelList<AnalyzeYieldfundsReport>
    {
        private List<AnalyzeYieldfundsData> _ReportDataCache;

        public virtual string FormTitle
            =>
                _actId == 107
                    ? "Форма 6.1 Доходность инвестирования средств пенсионных накоплений, рассчитанная в соответствии с приказом Минфина России от 22.08.2005 № 107н «Об утверждении стандартов раскрытия информации об инвестировании средств пенсионных накоплений»"
                    : _actId == 140
                        ? "Форма 6.2 Доходность инвестирования средств пенсионных накоплений, рассчитанная в соответствии с приказом Минфина России от 18.11.2005 № 140н «Об утверждении порядка расчета результатов инвестирования средств пенсионных накоплений для их отражения в специальной части индивидуальных лицевых счетов»"
                        : string.Empty;

        public string FormTitleWithMeasure => $"{FormTitle}, (% годовых)";

        public int ActID => _actId;
        private int _actId = 107;
        public RangeObservableCollection<AnalyzeYieldfundsData> ReportData { get; private set; }

        public List<Tuple<int, string>> Kvartals { get; set; }


        public AnalyzeYieldfundsPivotViewModel(int actId)
        {
            _actId = actId;

            ReportData = new RangeObservableCollection<AnalyzeYieldfundsData>();

            ExecuteRefreshList(null);
        }

        protected override void ExecuteRefreshList(object param)
        {
            if (ReportData != null)
            {
                //var reports = DataContainerFacade.GetClient().GetYieldfundsReportsByYearKvartal(actId: _actId).Where(r => r.Status == 0);
                List<AnalyzeYieldfundsData> data = new List<AnalyzeYieldfundsData>();
                data.AddRange(DataContainerFacade.GetClient().GetYieldfundsDataByYearKvartal(actId: _actId));
                //foreach (var rp in reports)
                //{
                //    data.AddRange(DataContainerFacade.GetClient().GetYieldfundsDataByReportId(rp.Id));
                //}

                data.ForEach(d => d.QuarkText = QuarkDisplayHelper.GetQuarkDisplayValue(d.Kvartal));
                ReportData.Fill(data);
                RaiseDataRefreshing();
            }
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}