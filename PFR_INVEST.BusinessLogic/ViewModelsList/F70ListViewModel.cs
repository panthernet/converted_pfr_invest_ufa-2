﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class F70ListViewModel : PagedLoadListModelBase<F070DetailsListItem>
    {

        public F70ListViewModel()
        {
            DataObjectTypeForJournal = typeof(EdoOdkF070);
        }

        protected void UpdateDocNumbers(List<F070DetailsListItem> newList)
        {
            const string sFormat = "{0}-{1}";
            var dict = new Dictionary<string, List<F070DetailsListItem>>();
            newList.ForEach(i =>
            {
                string key = i.F070 != null ? string.Format(sFormat, i.F070.YearId.HasValue ? i.F070.YearId.Value.ToString() : string.Empty, i.F070.Quartal.HasValue ? i.F070.Quartal.Value.ToString() : string.Empty) : string.Format(sFormat, "0", "0");
                if (!dict.ContainsKey(key))
                    dict[key] = new List<F070DetailsListItem>();
                dict[key].Add(i);
            });
            foreach (var l in dict)
            {
                int cnt = l.Value.Count;
                l.Value.ForEach(i =>
                {
                    i.lContractNumbers = cnt;
                });
            }
            this.List = newList;
            OnPropertyChanged("List");
        }

        protected override void BeforeListRefresh()
        {

            OnDataRefreshed += (sender, e) => this.UpdateDocNumbers(this.TmpList);

        }

        protected override long GetListCount()
        {
            return BLServiceSystem.Client.GetCountF70ListByTypeContractHib((int)Document.Types.All);
        }

        protected override List<F070DetailsListItem> GetListPart(long index)
        {
            return BLServiceSystem.Client.GetF70ListByTypeContractPageHib((int)Document.Types.All, (int)index); ;
        }


        #region
        //private List<F070DetailsListItem> m_F70List = new List<F070DetailsListItem>();
        //      public List<F070DetailsListItem> F70List
        //      {
        //          get
        //          {
        //              return m_F70List;
        //          }
        //          set
        //          {
        //              m_F70List = value;
        //              OnPropertyChanged("F70List");
        //          }
        //      }

        //      protected override void ExecuteRefreshList(object param)
        //      {
        //          CalculateDocNumbers(BLServiceSystem.Client.GetF70ListHib());
        //      }

        //      protected override bool CanExecuteRefreshList()
        //      {
        //          return true;
        //      }


        #endregion


    }
}
