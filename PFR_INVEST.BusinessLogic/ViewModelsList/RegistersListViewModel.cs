﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{

    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    public class RegistersListViewModel : PagedLoadObservableListModelBase<RegistersListItem>, ISettingOpenForm, IUpdateListenerModel, IRequestCustomAction
    {
        public delegate void SelectGridRowDelegate(long rId, int type);
        public event SelectGridRowDelegate OnSelectGridRowDelegate;

        public void OnSelectGridRow(long rId, int type)
        {
            OnSelectGridRowDelegate?.Invoke(rId, type);
        }

        public RegistersListViewModel()
        {
            DataObjectTypeForJournal = typeof(Register);

            //if (m_SelectedYear.Key == null)
            //{
            //    SetOptionYearsList();
            //}

        }

        public OpenSettingsProvider SettingsProvider { get; set; }

        //protected List<FinregisterCorr> CorrList { get; set; }

        //protected void UpdateRegister(List<RegistersListItem> list){
        //        foreach (var registersListItem in list)
        //        {
        //            var dop = CorrList.Where(finregisterCorr => finregisterCorr.FinregisterID == registersListItem.ID).ToList();
        //            if (dop.Count <= 0) continue;
        //            registersListItem.Count = (decimal)dop[dop.Count - 1].CurrentSum;
        //            registersListItem.FinregRegNum = dop[dop.Count - 1].FinregisterNum;
        //            registersListItem.FinregDate = dop[dop.Count - 1].FinregisterDate;
        //        }
        //}

		//protected  void Update(List<RegistersListItem> part)
		//{
		//    // поля корректно заполняются по последней по дате (http://jira.vs.it.ru/browse/PFRPTK-1751) корректировке в сервисе при выполнении запроса данных 
		//    //foreach (var registersListItem in part)
		//    //{
		//    //    var dop = CorrList.Where(finregisterCorr => finregisterCorr.FinregisterID == registersListItem.ID).ToList();
		//    //    if (dop.Count <= 0) continue;
		//    //    registersListItem.Count = (decimal)dop[dop.Count - 1].CurrentSum;
		//    //    registersListItem.FinregRegNum = dop[dop.Count - 1].FinregisterNum;
		//    //    registersListItem.FinregDate = dop[dop.Count - 1].FinregisterDate;
		//    //}
		//    this.List = part;
		//}

        public bool IsEventsRegisteredOnDataRefreshed = false;
        public const string EMPTY_REGISTER_TEXT = "(Финреестры отсутствуют)";

        protected override void BeforeListRefresh()
        {
            //this.CorrList = DataContainerFacade.GetList<FinregisterCorr>();
            if (!IsEventsRegisteredOnDataRefreshed)
            {
                //OnDataRefreshed += new EventHandler((object sender, EventArgs e) => Update(this.t_List));
                IsEventsRegisteredOnDataRefreshed = true;
            }

            if (this.SettingsProvider == null)
                this.SettingsProvider = new OpenSettingsProvider("RegistersListViewModel", "Работа с НПФ - Передача СПН - Перечень реестров", (s) => this.ExecuteRefreshList(null));
            if (!IsOnDataRefreshed)
            {
                //OnDataRefreshed += new EventHandler((object sender, EventArgs e) => Update(this.t_List));
            }
        }

       /* protected override void AfterListRefreshFinished()
        {
            if (_toSelectId != null && _toSelectType != null)
            {
                OnSelectGridRow((long)_toSelectId, (int)_toSelectType);
                _toSelectId = null;
                _toSelectType = null;
            }
        }*/

        protected override long GetListCount()
        {
            //if (m_SelectedYear.Key == null)
            //{
            //    SetOptionYearsList();
            //}
            return BLServiceSystem.Client.GetRegistersListCountPeriod(false, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
        }

        protected override List<RegistersListItem> GetListPart(long index)
        {
            return BLServiceSystem.Client.GetRegistersListPeriodByPage(false, (int)index, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
        }




		Type[] IUpdateListenerModel.UpdateListenTypes => new[] { typeof(Finregister), typeof(Register) };

        void IUpdateListenerModel.OnDataUpdate(Type type, long id)
		{
            //
            // т.к. таблица с группировкой и составными записями, требуется спец. логика при еденичном обновлении
            // чтобы правильно отображались данные при вставке/удалении записей
            //
			if (type == typeof(Finregister)) 
			{
				var items = BLServiceSystem.Client.GetRegistersListPeriodByPage(false, 0, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd, id);
				var old = this.List.Where(r => r.ID == id).ToList();
                //вычисляем, если добавляем новую запись (первый финреестр для реестра) и в списке уже была запись реестра без финреестра, то ее нужно удалить
                if(!old.Any() && items.Any())
                {
                    var singleEntry = List.FirstOrDefault(a => a.RegisterID == items.First().RegisterID);
                    if(singleEntry != null && singleEntry.ContragentID < 1)
                        old.Add(singleEntry);
                }
                //вычисляем, если удалена последняя запись (финреестр) для реестра из списка, то добавляем пустую запись реестра, 
                //чтобы при еденичном обновлении запись реестра не исчезла из списка
			    var sItem = old.FirstOrDefault();
			    if (!items.Any() && old.Any() && List.Count(a => a.RegisterID == sItem.RegisterID) == 1)
			    {
                    //если у реестра нет финреестров - добавляем фейковую запись
                    //если же у реестра имеются другие финреестры и они не отображены в списке, предполагаем, что реестр также нужно исключить из списка (отправить в архив)
			        if(DataContainerFacade.GetListByPropertyCount<Finregister>("RegisterID", sItem.RegisterID, true) == 0)
			            items.Add(new RegistersListItem
			            {
			                RegisterID = sItem.RegisterID,
			                RegisterKind = sItem.RegisterKind,
			                Content = sItem.Content,
			                RegDate = sItem.RegDate
			            });
			    }

                //проверяем не нужно ли скрыть запись финреестра в статусе Не передан, 
                //для которого все дочерние финреестры находятся в статусе Передан
                if(old.Any())
                {
                    var tmpList = new List<RegistersListItem>();
                    old.ForEach(a =>
                    {
                        //если финреестр идет на замену или пустой, пропускаем
                        if (items.Contains(a) || a.DraftSum == 0) return;
                        //получаем финреестр
                        var fr = DataContainerFacade.GetByID<Finregister>(id);
                        var parentIdList = new List<long>();
                        //если финреестр удаляется
                        if (fr == null)
                        {
                            //пока пропускаем
                            return;
                            //проверяем все финреестры в статусе Не передан под реестром и формируем список идентификаторов
                            parentIdList = DataContainerFacade.GetListByPropertyConditions<Finregister>(new List<ListPropertyCondition>()
                            {
                                ListPropertyCondition.Equal("RegisterID", a.RegisterID),
                                ListPropertyCondition.Equal("Status", RegisterIdentifier.FinregisterStatuses.NotTransferred),
                                ListPropertyCondition.NotEqual("StatusID", -1L)                                
                            }).Select(regx => regx.ID).ToList();
                        } else
                        {
                            //если у финреестра нет родителя, пропускаем
                            if (fr.ParentFinregisterID == null) return;//если есть, сохраняем его ID
                            parentIdList.Add((long)fr.ParentFinregisterID);
                        }
                        //пробегаем по всем ID родителей
                        parentIdList.ForEach(pId => {
                            //получаем список всех финреестров по ID родителя
                            var frList = DataContainerFacade.GetListByProperty<Finregister>("ParentFinregisterID", pId, true);
                            //если у родительского финреестра все финреестры находятся в статусе Передан, нужно его скрыть
                            if (frList.All(fri => RegisterIdentifier.FinregisterStatuses.IsTransferred(fri.Status)))
                            {
                                var item = List.FirstOrDefault(reg => reg.ID == pId);
                                if (!tmpList.Contains(item) && !old.Contains(item))
                                    tmpList.Add(item);
                            }
                        });
                        
                    });
                    old.AddRange(tmpList);
                }
			    UpdateRecord(old, items);
                if (items.Any())
                    OnRequestSelectId(items.First().ID, true);
                /*  _toSelectId = id;
                  _toSelectType = 0;*/
			    return;
			}
            if(type == typeof(Register))
            {
                var items = BLServiceSystem.Client.GetRegistersListPeriodByPageForRegister(false, 0, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd, id);
                var old = List.Where(r => r.RegisterID == id).ToList();
                UpdateRecord(old, items);
                if (items.Any())
                    OnRequestSelectId(items.First().RegisterID, false); 
                return;
            }
		}

       /* private int? _toSelectType;
        private long? _toSelectId;*/
        public event EventHandler RequestCustomAction;

        protected void OnRequestSelectId(long id, bool isFinregister)
        {
            if (RequestCustomAction != null)
                RequestCustomAction(new object[] {isFinregister, id}, null);
        }
    }
}
