﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class NetWealthsListViewModel : PagedLoadListModelBase<NetWealthListItem>
    {
        public OpenSettingsProvider SettingsProvider { get; set; }

        protected Dictionary<string, List<NetWealthListItem>> dict;

        protected long F010ListCount { get; set; }

        protected long F015ListCount { get; set; }

        protected virtual Document.Types DocumentType => Document.Types.All;

        public NetWealthsListViewModel()
        {
            DataObjectTypeForJournal = typeof (object);
            DataObjectDescForJournal = "Составной список EDO_ODKF010 и EDO_ODKF015";
        }

        protected override List<NetWealthListItem> AfterListLoaded(List<NetWealthListItem> list)
        {
            var docs = base.AfterListLoaded(list);
            docs.ForEach(n => n.NetWealthSum = Math.Round(n.NetWealthSum ?? 0, 2, MidpointRounding.AwayFromZero));
            
            return docs;
        }

        protected override long GetListCount()
        {
            if (SettingsProvider == null)
            {
                F010ListCount = BLServiceSystem.Client.GetCountNetWealthsF010ListByTypeContract((int)DocumentType);
                F015ListCount = BLServiceSystem.Client.GetCountNetWealthsF015ListByTypeContract((int)DocumentType);
                return F010ListCount + F015ListCount;
            }
            else
            {
                if (SettingsProvider == null) return -1;
                F010ListCount = BLServiceSystem.Client.GetCountNetWealthsF010ListByTypeContract((int)DocumentType, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
                F015ListCount = BLServiceSystem.Client.GetCountNetWealthsF015ListByTypeContract((int)DocumentType, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
                return F010ListCount + F015ListCount;
            }
        }

        protected override List<NetWealthListItem> GetListPart(long index)
        {
            if (SettingsProvider == null)
            {
                return index < F010ListCount ?
                    BLServiceSystem.Client.GetNetWealthsF010ListByPage((int)DocumentType, (int)index).ToList() :
                    BLServiceSystem.Client.GetNetWealthsF015ListByPage((int)DocumentType, (int)(index - F010ListCount)).ToList();
            }
            else
            {
                return index < F010ListCount ?
                   BLServiceSystem.Client.GetNetWealthsF010ListByPage((int)DocumentType, (int)index, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd).ToList() :
                   BLServiceSystem.Client.GetNetWealthsF015ListByPage((int)DocumentType, (int)(index - F010ListCount), SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd).ToList();

            }
        }


        protected void UpdateDocNumbers(List<NetWealthListItem> newList)
        {
            string sFormat = "{0}-{1}";

            newList.ForEach(i =>
            {
                string key = i.ReportOnDate.HasValue ? string.Format(sFormat, i.ReportOnDate.Value.Year.ToString(), i.ReportOnDate.Value.Date.ToShortDateString()) : string.Format(sFormat, string.Empty, string.Empty);
                if (!dict.ContainsKey(key))
                    dict[key] = new List<NetWealthListItem>();
                dict[key].Add(i);
            });
            foreach (var l in dict)
            {
                int cnt = l.Value.Count;
                l.Value.ForEach(i =>
                {
                    i.lDocumentNumbers = cnt;
                });
            }
            this.List = newList;
            //OnPropertyChanged("List");
        }


        protected void CalculateDocNumbers(List<NetWealthListItem> newList)
        {
            UpdateDocNumbers(newList);
            this.List = newList;
        }

    }
}
