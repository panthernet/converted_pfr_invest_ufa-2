﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OUFV_manager)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class VRAssignPaymentsListViewModel : AssignPaymentsListViewModel
    {
        public long InitialSelectedRID { get; set; }

       // public readonly long OperationId = 8;
        private readonly long[] _availableOpIdList = { 8L, 10L, 11L, 13L };
        protected override List<TransferListItem> GetTransfers()
        {
            return BLServiceSystem.Client.GetTransfersListBYContractType(Document.Types.VR, false, true, false)
                .Where(tr => (tr.RegisterOperationID == null || _availableOpIdList.Contains(tr.RegisterOperationID.Value)) && tr.MonthID > 0).ToList();             
        }

        public VRAssignPaymentsListViewModel()
        {
            DataObjectTypeForJournal = typeof(SIRegister);
        }
    }
}
