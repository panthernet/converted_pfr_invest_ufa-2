﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class SIListViewModel : ViewModelList
    {
        private List<SIListItem> m_SIList;
        public List<SIListItem> SIList
        {
            get
            {
                RaiseDataRefreshing();
                return m_SIList;
            }
            set
            {
                m_SIList = value;
                OnPropertyChanged("SIList");
            }
        }

        public SIListViewModel(bool withContacts)
            : base(withContacts)
        {
            DataObjectTypeForJournal = typeof(LegalEntity);
        }

        protected override void ExecuteRefreshList(object param)
        {
            bool withContacts = (bool)param;
            SIList = BLServiceSystem.Client.GetSIListHib(withContacts);
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }


    }
}
