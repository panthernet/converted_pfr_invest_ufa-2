﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    public abstract class AssignPaymentsListViewModel : ViewModelList
    {
        public delegate void SelectGridRowDelegate(long RID);

        private List<TransferListItem> _APTransfers = new List<TransferListItem>();

        protected AssignPaymentsListViewModel()
        {
        }

        public List<TransferListItem> APTransfers
        {
            get
            {
                RaiseDataRefreshing();
                return _APTransfers;
            }
            set
            {
                _APTransfers = value;
                OnPropertyChanged("APTransfers");
            }
        }

        public event SelectGridRowDelegate OnSelectGridRowDelegate;

        public void OnSelectGridRow(long RID)
        {
            if (OnSelectGridRowDelegate != null)
            {
                OnSelectGridRowDelegate(RID);
            }
        }

        protected override void ExecuteRefreshList(object param)
        {
            APTransfers = GetTransfers();
            var tst = APTransfers.Where(a => string.IsNullOrEmpty(a.Operation));}

        protected abstract List<TransferListItem> GetTransfers();

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}
