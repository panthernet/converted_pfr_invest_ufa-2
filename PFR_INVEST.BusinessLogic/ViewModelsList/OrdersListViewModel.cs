﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class OrdersListViewModel : PagedLoadListModelBase<OrdersListItem>
    {

        public OrdersListViewModel()
        {
            DataObjectTypeForJournal = typeof (CbOrder);
        }

        protected override long GetListCount()
        {
            return BLServiceSystem.Client.GetCountOrdersListHib();
        }

        protected override List<OrdersListItem> GetListPart(long index)
        {
            return BLServiceSystem.Client.GetOrdersListByPageHib((int)index);
        }



        #region old
        //private List<OrdersListItem> m_OrdersList;
        //public List<OrdersListItem> OrdersList
        //{
        //    get
        //    {
        //        RaiseDataRefreshing();
        //        return m_OrdersList;
        //    }
        //    set
        //    {
        //        m_OrdersList = value;
        //        OnPropertyChanged("OrdersList");
        //    }
        //}

        //protected override void ExecuteRefreshList(object param)
        //{
        //    OrdersList = BLServiceSystem.Client.GetOrdersListHib();
        //}

        //protected override bool CanExecuteRefreshList()
        //{
        //    return true;
        //}
        #endregion


    }
}
