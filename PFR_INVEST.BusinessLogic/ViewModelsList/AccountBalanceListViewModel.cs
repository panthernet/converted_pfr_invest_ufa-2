﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Attributes;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [ModelEntity(JournalDescription = "Сводная таблица")]
    public class AccountBalanceListViewModel://ViewModelList<BalanceListItem>
		PagedLoadListModelBase<BalanceListItem>, ISettingOpenForm, IUpdateListenerModel
    {
        public OpenSettingsProvider SettingsProvider { get; set; }

        private List<PFRAccountsListItem> _pfrAccountsList;

		private readonly List<SaldoParts> _targetParts = new List<SaldoParts>
        {
            SaldoParts.Accoperation,
            SaldoParts.Apps,
            SaldoParts.AddSpn,
            SaldoParts.DoApps,
            SaldoParts.DopSpn_M,
			//Убрано согласно http://jira.vs.it.ru/browse/PFRPTK-922?focusedCommentId=203656&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-203656
            //SaldoParts.K_Doc,
            SaldoParts.OrdReport,
            SaldoParts.Cost,
            SaldoParts.IncomeSec,
            SaldoParts.Asg_Fin_Tr,
            SaldoParts.Req_Transfer,
            SaldoParts.Transfer,
            SaldoParts.Return,
            SaldoParts.Deposit,
            SaldoParts.BalanceExtra,
        };
		private readonly Dictionary<SaldoParts, long> _countParts = new Dictionary<SaldoParts, long>();
		private readonly Dictionary<SaldoParts, long> _loadedIndex = new Dictionary<SaldoParts, long>();

		protected override void BeforeListRefresh()
		{
		    if (SettingsProvider != null) return;
		    //if (this.RefreshParameter is List<string>)
		    //{
		    //var list = (this.RefreshParameter as List<string>);
		    //var formName = list.FirstOrDefault();
		    //var comment = list.Skip(1).FirstOrDefault();
		    //OnDataRefreshed += new EventHandler((object sender, EventArgs e) => this.RefreshList(this.t_List));
		    _pfrAccountsList = BLServiceSystem.Client.GetPFRAccountsList();
		    SettingsProvider = new OpenSettingsProvider("AccountBalanceListView", "Бэк-офис - Перечисления - Счета",
		        (s) => ExecuteRefreshList(null)) {IsPeriodVisible = true};
		    //}
		}

		protected override long GetListCount()
		{
			if (SettingsProvider == null) return -1;

			foreach (var part in _targetParts)
			{
				_countParts[part] = BLServiceSystem.Client.GetCountBalanceList(new List<SaldoParts> { part }, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
				_loadedIndex[part] = 0;
			}

			return _countParts.Sum(x => x.Value);
		}


		protected override List<BalanceListItem> GetListPart(long index)
		{
			var nextPart = _targetParts.FirstOrDefault(x => _countParts[x] - _loadedIndex[x] > 0);
            if ((int)nextPart == 0) return new List<BalanceListItem>();
            var part = BLServiceSystem.Client.GetBalanceListByPage(
                    new List<SaldoParts> { nextPart },
					(int)(_loadedIndex[nextPart]), SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
		    //part = part.Where(a => a.PORTFOLIO.StartsWith(@"ДСВ 2010\1") && a.DATE >= new DateTime(2010, 1, 1) && a.DATE < new DateTime(2010, 4, 1)).ToList();
			_loadedIndex[nextPart] += part.Count;
			return part.Select(it => new BalanceListItem(it, _pfrAccountsList.FirstOrDefault(a => a.AccountNumber == it.ACCOUNT))).ToList();
		}
		
		

        //// полностью грид обновляется только целиком нажатием кнопки: http://jira.vs.it.ru/browse/DOKIPIV-125?focusedCommentId=222876&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-222876
		// автоматическое обновление только для изменений из раздела Бэк-офис - Счета: http://jira.vs.it.ru/browse/DOKIPIV-67?focusedCommentId=225505&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-225505

		public Type[] UpdateListenTypes => new[] { typeof(AccOperation)
		    //, typeof(Aps), typeof(AddSPN), typeof(DopAps), typeof(DopSPN), typeof(OrdReport), typeof(Cost), typeof(Incomesec)
		    //, typeof(AsgFinTr), typeof(ReqTransfer), typeof(Transfer), typeof(Return), typeof(Deposit), typeof(BalanceExtra)
		};

        public void OnDataUpdate(Type type, long id)
		{
			var saldoParts = new List<SaldoParts>();
			if (type == typeof(AccOperation))
				saldoParts.Add(SaldoParts.Accoperation);
			//else if (type == typeof(Aps))
			//    saldoParts.Add(SaldoParts.Apps);
			//else if (type == typeof(AddSPN))
			//    saldoParts.Add(SaldoParts.AddSpn);
			//else if (type == typeof(DopAps))
			//    saldoParts.Add(SaldoParts.DoApps);
			//else if (type == typeof(DopSPN))
			//    saldoParts.Add(SaldoParts.DopSpn_M);
			//else if (type == typeof(OrdReport))
			//    saldoParts.Add(SaldoParts.OrdReport);
			//else if (type == typeof(Cost))
			//    saldoParts.Add(SaldoParts.Cost);
			//else if (type == typeof(Incomesec))
			//    saldoParts.Add(SaldoParts.IncomeSec);
			//else if (type == typeof(AsgFinTr))
			//{
			//    // новые ПП одинаковы дял УК и НПФ
			//    saldoParts.Add(SaldoParts.Asg_Fin_Tr);
			//    saldoParts.Add(SaldoParts.Req_Transfer);
			//}
			//else if (type == typeof(ReqTransfer))
			//    saldoParts.Add(SaldoParts.Req_Transfer);
			//else if (type == typeof(Transfer))
			//    saldoParts.Add(SaldoParts.Transfer);
			//else if (type == typeof(Return))
			//    saldoParts.Add(SaldoParts.Return);
			//else if (type == typeof(Deposit))
			//    saldoParts.Add(SaldoParts.Deposit);
			//else
			//    saldoParts.Add(SaldoParts.BalanceExtra);

			var data = BLServiceSystem.Client.GetBalanceListByID(saldoParts.ToList(), id, null, null);

			if (SettingsProvider != null && SettingsProvider.IsPeriodChecked)
			{
				data = data.Where(x => x.DATE >= SettingsProvider.Filter.PeriodStart && x.DATE <= SettingsProvider.Filter.PeriodEnd).ToList();
			}


			var ids = saldoParts.Select(p => (int)p).ToArray();
			//var toRemove = List.Where(x => x.ID == id && ids.Contains(x.Ord)).ToList();
			//toRemove.ForEach(x => List.Remove(x));

			//data.ForEach(x => List.Add(new BalanceListItem(x, PFRAccountsList.FirstOrDefault(a => a.AccountNumber == x.ACCOUNT))));

		    foreach (var i in data.Select(x => new BalanceListItem(x, _pfrAccountsList.FirstOrDefault(a => a.AccountNumber == x.ACCOUNT))))
			{
				var inList = List.SingleOrDefault(x => x.ID == i.ID && x.Ord == i.Ord && x.SubQuery == i.SubQuery);
				if (inList == null)
				{
					List.Add(i);
				}
				else
				{
					// надо заменить значения полей, но при этом оставить элемент в том же месте последовательности
					int index = List.IndexOf(inList);
					List[index] = i;
				}
			}

			var toRemove = List.Where(x => x.ID == id && ids.Contains(x.Ord) && !data.Any(y => y.ID == x.ID && y.Ord == x.Ord && y.SubQuery == x.SubQuery)).ToList();
			toRemove.ForEach(x => List.Remove(x));

			OnPropertyChanged("List");
		}
    }
}

