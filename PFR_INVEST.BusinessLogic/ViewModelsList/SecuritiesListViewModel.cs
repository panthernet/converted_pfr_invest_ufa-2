﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class SecuritiesListViewModel : ViewModelList
    {
        private IList<Security> m_SecuritiesList;
        public IList<Security> SecuritiesList
        {
            get
            {
                RaiseDataRefreshing();
                return m_SecuritiesList;
            }
            set
            {
                m_SecuritiesList = value;
                OnPropertyChanged("SecuritiesList");
            }
        }

        public SecuritiesListViewModel()
        {
            DataObjectTypeForJournal = typeof(Security);
        }

        protected override void ExecuteRefreshList(object param)
        {
            SecuritiesList = BLServiceSystem.Client.GetAllSecuritiesListHib();
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}
