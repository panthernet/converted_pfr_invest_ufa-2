﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator)]
    public class OnesImportErrorsListViewModel : PagedLoadObservableListModelBase<OnesErrorJournalListItem>, ISettingOpenForm
    {
        public override Type DataObjectTypeForJournal => typeof(OnesErrorJournalListItem);
        public OpenSettingsProvider SettingsProvider { get; set; }
        public bool IsEventsRegisteredOnDataRefreshed;
        public List<string> DocKinds { get; set; }
        public string SelectedDocKind { get; set; }

        protected override long GetListCount()
        {
            UpdateDocKinds();
            return BLServiceSystem.Client.OnesImportErrorsCount(SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd, (int)OnesJournal.Actions.ExportComplete, SelectedDocKind != DocKinds.First() ? SelectedDocKind : null);
        }

        private void UpdateDocKinds()
        {
            var kind = SelectedDocKind;
            DocKinds = BLServiceSystem.Client.GetOnesKindsList();
            DocKinds.Insert(0, "(Все)");
            if (kind != null && DocKinds.Contains(kind))
                SelectedDocKind = kind;
            else SelectedDocKind = DocKinds.FirstOrDefault();
            OnPropertyChanged("SelectedDocKind");
            OnPropertyChanged("DocKinds");
        }

        protected override List<OnesErrorJournalListItem> GetListPart(long index)
        {
            return BLServiceSystem.Client.GetOnesErrorJournal((int)index, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd, (int)OnesJournal.Actions.ExportComplete, SelectedDocKind != DocKinds.First() ? SelectedDocKind : null);
        }

        protected override void BeforeListRefresh()
        {
            if (!IsEventsRegisteredOnDataRefreshed)
                IsEventsRegisteredOnDataRefreshed = true;

            if (SettingsProvider == null)
            {
                SettingsProvider = new OpenSettingsProvider("OnesImportErrorsListViewModel", "Импорт 1С - Журнал ошибок", (s) => this.ExecuteRefreshList(null))
                {
                    PeriodEnd = DateTime.Today,
                    PeriodStart = DateTime.Today//.AddYears(-1).Date
                };
            }
        }
    }
}
