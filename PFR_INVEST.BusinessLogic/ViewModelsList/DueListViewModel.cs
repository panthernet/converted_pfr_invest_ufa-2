﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ServiceItems;

namespace PFR_INVEST.BusinessLogic
{

	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator)]
	public class DueListViewModel : PagedLoadListModelBase<DueListItemClient>, IUpdateListenerModel
	{
		public PortfolioTypeFilter Filter { get; private set; }

		private long _countAdd, _countDop, _countAps, _countDopAps;

		protected override long GetListCount()
		{

			_countAdd = BLServiceSystem.Client.GetCountAddSPNFullList(Filter);
			_countDop = BLServiceSystem.Client.GetCountDopSPNFullList(Filter);
			_countAps = BLServiceSystem.Client.GetCountApsFullList(Filter);
			_countDopAps = BLServiceSystem.Client.GetCountDopApsFullList(Filter);

			return _countAdd + _countDop + _countAps + _countDopAps;

		}

		protected override List<DueListItemClient> GetListPart(long index)
		{
			if (index < _countAdd)//ADDSPN
				return BLServiceSystem.Client.GetAddSPNFullListByPage(Filter, (int)index).Select(i => new DueListItemClient(i, IsDsv)).ToList();
			if (index < _countAdd + _countDop)//DOPSPN
				return BLServiceSystem.Client.GetDopSPNFullListByPage(Filter, (int)(index - (_countAdd))).Select(i => new DueListItemClient(i, IsDsv)).ToList();
			if (index < _countAdd + _countDop + _countAps) //APS
				return BLServiceSystem.Client.GetApsFullListByPage(Filter, (int)(index - (_countAdd + _countDop))).Select(i => new DueListItemClient(i, IsDsv)).ToList();
			return BLServiceSystem.Client.GetDopApsFullListByPage(Filter, (int)(index - (_countAdd + _countDop + _countAps))).Select(i => new DueListItemClient(i, IsDsv)).ToList();
		}


		protected override void BeforeListRefresh()
		{
			Filter = (PortfolioTypeFilter)RefreshParameter;
		}

		protected override List<DueListItemClient> AfterListLoaded(List<DueListItemClient> list)
		{
			var tempList = base.AfterListLoaded(list);
			return ProcessList(tempList);
		}

		//private PortfolioIdentifier.PortfolioTypes Type { get; set; }
		private IList<DueListItemClient> _mDuelist;
		public IList<DueListItemClient> DueList
		{
			get
			{
				return List;
			}
			set
			{
				_mDuelist = value;
				OnPropertyChanged("DueList");
			}
		}


		

		/*private static decimal GetPenny(KDoc due)
		{
			if (due == null)
				return -1;

			return (due.Penni1)
					+ (due.Pay1)
					+ (due.Proc1)
					+ (due.Penni2)
					+ (due.Pay2)
					+ (due.Proc2)
					+ (due.Penni3)
					+ (due.Pay3)
					+ (due.Proc3)
					+ (due.Penni4)
					+ (due.Pay4)
					+ (due.Proc4);
		}*/

		// add items with negative signs, if source porfolio exists
		public List<DueListItemClient> PopulateSourcePortfolioItems(IList<DueListItemClient> inListItems)
		{
			if (inListItems == null || !inListItems.Any())
				return null;

			var outListItems = new List<DueListItemClient>();

			foreach (var item in inListItems)
			{
				//Пропускаем двойные у которых портфель не нашего типа
				if (Filter.IsMatch(item.Portfolio.TypeEl))
					outListItems.Add(item);

				//Размножаем двойные у которых в источник нашего типа
				if (item.SourcePortfolio != null && Filter.IsMatch(item.SourcePortfolio.TypeEl))
				{
					//костыль, чтобы не просто не дублировать строки, а отсеивать дубликаты в одном и том же портфеле!
					if ((item.Operation == DueDocKindIdentifier.Prepayment || item.Operation == DueDocKindIdentifier.DueUndistributed
						|| item.Operation == DueDocKindIdentifier.DueDead || item.Operation == DueDocKindIdentifier.DueExcess)
						&& item.GoalPortfolio == item.SourcePortfolio.Year)
					{
						continue;
					}

					// add negative clone
					if (item.SourceObject is Aps)
					{
						var newListItem = new DueListItemClient((Aps)item.SourceObject, IsDsv, true);
						outListItems.Add(newListItem);
					}
					else if (item.SourceObject is DopAps)
					{
						var newListItem = new DueListItemClient((DopAps)item.SourceObject, IsDsv, true);
						outListItems.Add(newListItem);
					}

				}
			}

			return outListItems;
		}

		

		public List<DueListItemClient> CalculateSupply(IEnumerable<DueListItemClient> inListItems)
		{
			var outListItems = new List<DueListItemClient>();
			var clcStruct = new Dictionary<string, List<DueListItemClient>>();

			if (inListItems != null)
				foreach (var item in inListItems)
				{
					string key = string.Concat(item.PortfolioName, "_", item.Month, "_", item.Operation, item.FK_ID.HasValue ? item.FK_ID.Value.ToString() : string.Empty);

					if (!clcStruct.ContainsKey(key))
						clcStruct[key] = new List<DueListItemClient>();

					clcStruct[key].Add(item);
				}

			foreach (var branch in clcStruct)
			{
				var items = branch.Value;

				// sort by Date
				int n = items.Count;

				var tempFk = items.Where(i => i.FK_ID >= 0 && string.IsNullOrEmpty(i.Precision))
												.Select(i => i.FK_ID).ToList();

				long tempFkID = tempFk.Max() ?? -1;

				items.Where(i => (i.FK_ID ?? -1) < 0).ToList()
					.ForEach(i => i.FK_ID = tempFkID);


				// sort by Date
				items = items.OrderBy(i => i.FK_ID)
								.ThenBy(i => i.Precision == DueDocKindIdentifier.Treasurity ? DateTime.MaxValue : i.Date).ToList();

				// go while we meet 1st accurate

				for (int i = 0; i < n; i++)
				{
					var item = items[i];
					if (item.Precision == DueDocKindIdentifier.PenaltyAccurate)
					{
						item.Supply = item.ChSumm;
					}
					else if (item.Precision == DueDocKindIdentifier.DueAccurateQuarter || item.Precision == DueDocKindIdentifier.DueDSVAccurateYear)
					{
						item.Supply = item.ChSumm;
						//var quarterSum = inListItems.Where(d => d.PortfolioID == item.PortfolioID
						//                                        && d.Operation == item.Operation
						//                                        && d.Quarter == item.Quarter
						//                                        && d.MonthIndex.HasValue
						//                                        && d.ID != item.ID)
						//                .GroupBy(d => d.MonthIndex)
						//                .Select(g => g.OrderBy(d => d.FK_ID).ThenBy(d => d.Precision == DueDocKindIdentifier.Treasurity ? DateTime.MaxValue : d.Date)
						//                                .Last().MSumm);
						////.Sum();
						//item.Supply = item.MSumm - quarterSum.Sum();
					}
					else if (item.Precision != string.Empty) // если уточнение
					{
						item.Supply = item.ChSumm;

						//if (item.Precision == DueDocKindIdentifier.Treasurity)
						//    item.Order = 1;
						//decimal summ = item.MSumm;
						//item.Supply = summ - prevSumm; //item.ChSumm;
						//prevSumm = summ;

					}
					else // if doc
					{
						var currentDocItem = item;
						currentDocItem.Supply = currentDocItem.Sum;
					}
				}
				outListItems.AddRange(items);
			}
			return outListItems;
		}

		public List<DueListItemClient> ProcessList(IList<DueListItemClient> inListItems)
		{
			var listItems = inListItems.ToList();
			listItems = PopulateSourcePortfolioItems(listItems);			
			// makes the fakedates for placing accurates in one month with main document
			var list = listItems.Where(i => string.IsNullOrEmpty(i.Precision)).ToList();
			list.ForEach(i =>
			{
				var lst = listItems.FindAll(d =>
					d.FK_ID == i.ID &&
					!string.IsNullOrEmpty(d.Precision) &&
					d.Operation.Equals(i.Operation)
					);
				lst.ForEach(l => l.FakeDate = i.FakeDate);
			});

			listItems = CalculateSupply(listItems);

			return listItems;
		}

		//public List<DueListItemClient> processListPart(IList<DueListItemClient> inListItems, IList<DueListItemClient> fullListItems, PortfolioIdentifier.PortfolioTypes PortfolioType)
		//{

		//}

		private bool IsDsv { get; set; }

		public DueListViewModel(PortfolioTypeFilter filter)
			: base(filter)
		{
			this.Filter = filter;
			this.IsDsv = Filter.IsMatch(Portfolio.Types.DSV);

			if (Filter.IsMatch(Portfolio.Types.DSV))
			{
				DataObjectTypeForJournal = typeof(Aps);
			}
			else if (Filter.IsMatch(Portfolio.Types.SPN))
			{
				DataObjectTypeForJournal = typeof(AddSPN);
			}
		}

		public Type[] UpdateListenTypes => new[] { typeof(AddSPN), typeof(DopSPN), typeof(Aps), typeof(DopAps) };

	    public void OnDataUpdate(Type type, long id)
		{
			List<DueListItemClient> item = null;
			if (type == typeof(AddSPN))
				item = BLServiceSystem.Client.GetAddSPNFullListByID(Filter, id).Select(i => new DueListItemClient(i, IsDsv)).ToList();

			if (type == typeof(DopSPN))
				item = BLServiceSystem.Client.GetDopSPNFullListByID(Filter, id).Select(i => new DueListItemClient(i, IsDsv)).ToList();

			if (type == typeof(Aps))
				item = BLServiceSystem.Client.GetApsFullListByID(Filter, id).Select(i => new DueListItemClient(i, IsDsv)).ToList();

			if (type == typeof(DopAps))
				item = BLServiceSystem.Client.GetDopApsFullListByID(Filter, id).Select(i => new DueListItemClient(i, IsDsv)).ToList();

			if (item != null && item.Count > 0)
			{
				var pItem = ProcessList(item);

				// такое замещение нарушает раскрытие групп
				////Delete old
				////List.Where(r => r.ID == id).ToList().ForEach(r => List.Remove(r));
				//List.RemoveAll(x => x.ID == id);
				////Append new
				////data.ForEach(d => List.Add(d));
				//List.AddRange(data);

				//List.RemoveAll(x => x.ID == id && x.SourceObject.GetType() == type);
				//List.AddRange(pItem);

				// некоторые записи содержат одинаковые ID, но при этом различаются по типу и наличию значения в NegativePortfolioName внутри одного типа и ID

				foreach (var i in pItem)
				{
					var inList = List.FirstOrDefault(x => x.ID == i.ID && x.SourceObject.GetType() == type && (string.IsNullOrWhiteSpace(x.NegativePortfolioName) == string.IsNullOrWhiteSpace(i.NegativePortfolioName)));
					if (inList == null)
					{
						List.Add(i);
					}
					else
					{
						// надо заменить значения полей, но при этом оставить элемент в том же месте последовательности
						int index = List.IndexOf(inList);
						List[index] = i;
					}
				}

				// сам набор записей тоже может измениться, поэтому необходимо проконтролировать также отсутствие соответствия для существующей в гриде записи с тем же типом и ID
				List.RemoveAll(x => x.ID == id && x.SourceObject.GetType() == type && pItem.All(y => y.NegativePortfolioName != x.NegativePortfolioName));
			}
			else
			{
				//исходная запись была удалена
				List.RemoveAll(x => x.ID == id && x.SourceObject.GetType() == type);
			}

			OnPropertyChanged("List");
		}
	}
}
