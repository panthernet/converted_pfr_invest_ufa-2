﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    public class DeadZLListViewModel : ViewModelList
    {
        private List<DeadZLListItem> _mDeadZlList;
        public List<DeadZLListItem> DeadZLList
        {
            get
            {
                RaiseDataRefreshing();
                return _mDeadZlList;
            }
            set
            {
                _mDeadZlList = value;
                OnPropertyChanged("DeadZLList");
            }
        }

        public DeadZLListViewModel()
        {
            DataObjectTypeForJournal = typeof(ERZLNotify);
        }

        protected override void ExecuteRefreshList(object param)
        {
            this.DeadZLList = BLServiceSystem.Client.GetDeadZLListHib();
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}
