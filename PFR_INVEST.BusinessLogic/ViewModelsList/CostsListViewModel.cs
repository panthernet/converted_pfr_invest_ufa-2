﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    public sealed class CostsListViewModel : PagedLoadListModelBase<CostsListItem>, ISettingOpenForm
    {
        public OpenSettingsProvider SettingsProvider { get; set; }

        public CostsListViewModel()
        {
            DataObjectTypeForJournal = typeof(Cost);
        }

        protected override long GetListCount()
        {
            return BLServiceSystem.Client.GetCountCostsListFiltered(SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
        }

        protected override List<CostsListItem> GetListPart(long index)
        {            
            var tmpList = BLServiceSystem.Client.GetCostsListFilteredByPage((int)index, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
                       
            return tmpList.Select(x => new CostsListItem(x)).ToList();
        }

        protected override void BeforeListRefresh()
        {            
            if (SettingsProvider == null)
                SettingsProvider = new OpenSettingsProvider("CostsListViewModel", "Бэк-офис - Временное размещение - Расходы по сделкам с ЦБ", (s) => ExecuteRefreshList(null));
        }

    }
}
