﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects.Analyze;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.User)]
    public class AnalyzePensionplacementViewModel : ViewModelList<AnalyzePensionplacementReport>
    {
        public virtual string FormTitle
            => "Форма 5. Структура размещения средств пенсионных накоплений в финансовые инструменты";

        public string FormTitleWithMeasure
            =>
                $"{FormTitle} за {QuarkDisplayHelper.GetQuarkDisplayValue(Facade.SelectedKvartal)}, {Facade.SelectedYear}г., (млн.руб)"
            ;

        public AnalyzeReportPivotFacade<AnalyzePensionplacementData> Facade => _facade;
        private AnalyzeReportPivotFacade<AnalyzePensionplacementData> _facade;

        public AnalyzePensionplacementViewModel()
        {
            _facade = new AnalyzeReportPivotFacade<AnalyzePensionplacementData>(() => RaiseDataRefreshing(),
                () => DataContainerFacade.GetClient().GetPensionplacementDataByYearKvartal());
        }

        protected override void ExecuteRefreshList(object param)
        {
            _facade?.RefreshDBData();
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}