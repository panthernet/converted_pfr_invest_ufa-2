﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class LoadedODKLogListViewModel : ViewModelList
    {
        private IList<EDOLogListItem> m_List;
        public IList<EDOLogListItem> List
        {
            get
            {
                return m_List;
            }
            set
            {
                m_List = value;
                OnPropertyChanged("List");
            }
        }

        private DateTime? dateFrom = DateTime.Now.AddDays(-7);
        public DateTime? DateFrom
        {
            get { return dateFrom; }
            set
            {
                if (dateFrom != value)
                {
                    dateFrom = value;
                    OnPropertyChanged("DateFrom");
                }
            }
        }

        private DateTime? _dateTo = DateTime.Now;
        public DateTime? DateTo
        {
            get { return _dateTo; }
            set
            {
                if (_dateTo != value)
                {
                    _dateTo = value;
                    OnPropertyChanged("DateTo");
                }
            }
        }

        protected const string ALL_FORMS = "Все формы";
        private readonly List<string> _mForms = new List<string>(new[] 
        {
            ALL_FORMS,
            "EDO_ODKF010",
            "EDO_ODKF012",
            "EDO_ODKF014",
            "EDO_ODKF015",
            "EDO_ODKF016",
            "EDO_ODKF020",
            "EDO_ODKF022",
            "EDO_ODKF025",
            "EDO_ODKF024",
            "EDO_ODKF026",
            "EDO_ODKF040",
            "EDO_ODKF050",
            "EDO_ODKF060",
            "EDO_ODKF070",
            "EDO_ODKF080",
            "EDO_ODKF140",
            "EDO_ODKF401",
            "EDO_ODKF402",
        });

        public List<string> Forms => _mForms;

        private string m_SelectedForm = ALL_FORMS;
        public string SelectedForm
        {
            get
            {
                return m_SelectedForm;
            }
            set
            {
                m_SelectedForm = value;
                OnPropertyChanged("SelectedForm");
            }
        }

        public bool UserIsAdministrator => AP.Provider.CurrentUserSecurity.CheckUserInRole(DOKIP_ROLE_TYPE.Administrator);

        public ICommand DeleteSelectedItems { get; set; }
        public bool CanExecuteDelete(object param)
        {
            return (List != null && List.Any(item => item.Selected));
        }

        public void ExecuteDelete(object param)
        {
            var selectedItems = List.Where(item => item.Selected).ToList();

            if (selectedItems.Any())
            {
                if (!DialogHelper.ShowConfirmation("Удалить выбранные отчеты?"))
                {
                    return;
                }

            }

            foreach (var item in selectedItems)
            {
                var isDeleteSuccess = true;
                switch (item.DocForm)
                {
                    case "EDO_ODKF010":
                        DataContainerFacade.Delete<EdoOdkF010>(item.DocID.Value);
                        ViewModelManager.RefreshViewModels(typeof(NetWealthsListViewModel));
                        break;
                    case "EDO_ODKF012":
                        DataContainerFacade.Delete<EdoOdkF012>(item.DocID.Value);
                        ViewModelManager.RefreshViewModels(typeof(NetWealthsListViewModel));
                        break;
                    case "EDO_ODKF014":
                        DataContainerFacade.Delete<EdoOdkF014>(item.DocID.Value);
                        break;
                    case "EDO_ODKF015":
                        DataContainerFacade.Delete<EdoOdkF015>(item.DocID.Value);
                        ViewModelManager.RefreshViewModels(typeof(NetWealthsSIListViewModel), typeof(NetWealthsVRListViewModel));
                        break;
                    case "EDO_ODKF016":
                        DataContainerFacade.Delete<EdoOdkF016>(item.DocID.Value);
                        ViewModelManager.RefreshViewModels(typeof(NetWealthsSumListViewModel));
                        break;

                    case "EDO_ODKF020":
                        DataContainerFacade.Delete<EdoOdkF020>(item.DocID.Value);
                        ViewModelManager.RefreshViewModels(typeof(MarketCostScopeListViewModel), typeof(MarketCostSIListViewModel), typeof(MarketCostVRListViewModel));
                        break;
                    case "EDO_ODKF022":
                        DataContainerFacade.Delete<EdoOdkF022>(item.DocID.Value);
                        ViewModelManager.RefreshViewModels(typeof(MarketCostScopeListViewModel), typeof(MarketCostListViewModel));
                        break;
                    case "EDO_ODKF025":
                        DataContainerFacade.Delete<EdoOdkF025>(item.DocID.Value);
                        ViewModelManager.RefreshViewModels(typeof(MarketCostScopeListViewModel), typeof(MarketCostSIListViewModel), typeof(MarketCostVRListViewModel), typeof(MarketCostVRListViewModel));
                        break;
                    case "EDO_ODKF024":
                        DataContainerFacade.Delete<EdoOdkF024>(item.DocID.Value);
                        ViewModelManager.RefreshViewModels(typeof(MarketCostScopeListViewModel), typeof(MarketCostSIListViewModel), typeof(MarketCostVRListViewModel));
                        break;
                    case "EDO_ODKF026":
                        DataContainerFacade.Delete<EdoOdkF026>(item.DocID.Value);
                        ViewModelManager.RefreshViewModels(typeof(MarketCostScopeListViewModel));
                        break;
                    case "EDO_ODKF040":
                        DataContainerFacade.Delete<EdoOdkF040>(item.DocID.Value);
                        ViewModelManager.RefreshViewModels(typeof(F40SIListViewModel), typeof(F40VRListViewModel));
                        break;
                    case "EDO_ODKF050":
                        DataContainerFacade.Delete<EdoOdkF050>(item.DocID.Value);
                        ViewModelManager.RefreshViewModels(typeof(F50SIListViewModel), typeof(F50VRListViewModel));
                        break;
                    case "EDO_ODKF060":
                        DataContainerFacade.Delete<EdoOdkF060>(item.DocID.Value);
                        ViewModelManager.RefreshViewModels(typeof(F60SIListViewModel), typeof(F60VRListViewModel));
                        break;
                    case "EDO_ODKF070":
                        DataContainerFacade.Delete<EdoOdkF070>(item.DocID.Value);
                        ViewModelManager.RefreshViewModels(typeof(F70SIListViewModel), typeof(F70VRListViewModel));
                        break;
                    case "EDO_ODKF080":
                        DataContainerFacade.Delete<EdoOdkF080>(item.DocID.Value);
                        ViewModelManager.RefreshViewModels(typeof(F80SIListViewModel), typeof(F80VRListViewModel));
                        break;
                    case "EDO_ODKF140":
                        if (isDeleteSuccess = DeleteF140(item))
                        {
                            //DataContainerFacade.Delete<EdoOdkF140>(item.DocID.Value);
                            ViewModelManager.RefreshViewModels(typeof(ViolationsSIListViewModel), typeof(ViolationsVRListViewModel));
                        }
                        break;
                    case "EDO_ODKF401":
                        if (isDeleteSuccess = DeleteF401(item))
                        {
                            DataContainerFacade.Delete<EdoOdkF401402>(item.DocID.Value);
                            ViewModelManager.RefreshViewModels(typeof(F401402UKListViewModel));
                        }
                        break;
                    case "EDO_ODKF402":
                        if (isDeleteSuccess = DeleteF402(item))
                        {
                            //DataContainerFacade.Delete<EdoOdkF401402>(item.DocID.Value);
                            ViewModelManager.RefreshViewModels(typeof(F401402UKListViewModel));
                        }
                        break;
                    default:
                        break;
                }
                if (isDeleteSuccess)
                {
                    DataContainerFacade.Delete<EDOLog>(item.ID);
                    DataContainerFacade.Delete<EdoXmlBody>(item.DocBodyId.Value);
                }
            }
            JournalLogger.LogModelEvent(this, JournalEventType.DELETE, null, "Удалены документы с номерами регистрации: " + String.Join(",", selectedItems.Select(item => item.DocRegNum).ToArray()));
            RefreshList.Execute(RefreshParameter);
        }

        private bool DeleteF140(EDOLogListItem logItem)
        {
            var f140 = DataContainerFacade.GetByID<EdoOdkF140>(logItem.DocID.Value);
            if (f140 == null)
            {
                return false;
            }

            var f140logList = DataContainerFacade.GetListByProperty<EDOLog>("DocId", logItem.DocID.Value);
            if (f140logList.Count > 1)
            {
                if (!DialogHelper.ShowConfirmation(string.Format(@"Запись о нарушении для договора {0} относится к нескольким записям отчетов о нарушении: 
{1}
Удалить нарушение и все относящиеся к нему отчеты?", f140.ContractNumber, f140logList.Select(x => x.DocRegNum).Aggregate((x, y) => x + ", " + y))))
                {
                    return false;
                }
            }

            foreach (var f140log in f140logList)
            {
                DataContainerFacade.Delete(f140log);
            }

            DataContainerFacade.Delete(f140);

            return true;
        }

        private bool DeleteF401(EDOLogListItem logItem)
        {
            // получение связанных 401-402 сущностей
            var f401 = DataContainerFacade.GetByID<EdoOdkF401402>(logItem.DocID.Value);

            if (f401 == null)
            {
                return false;
            }

            var ukList = DataContainerFacade.GetListByProperty<F401402UK>("EdoID", f401.ID);

            var f402List = new List<EDOLog>();

            foreach (var num in ukList.Select(x => x.OutgoingDocNumber).Distinct())
            {
                // импортированные Log записи формы 402 ссылаются на изначальную запись, созданную в 401 форме
                f402List.AddRange(DataContainerFacade.GetListByProperty<EDOLog>("DocRegNumberOut", num).Where(x => x.DocId == logItem.DocID.Value && x.ID != logItem.ID));
            }

            if (f402List.Select(x=>x.ID).Distinct().Any())
            {
                //if (!
                DialogHelper.ShowError(
                string.Format(@"Для удаляемой записи F401({0}) существуют импортированные записи F402({1}).
Необходимо удалить все связанные записи формы F402.", logItem.DocRegNum, f402List.Select(x => x.DocRegNum).Distinct().Aggregate((x, y) => x + ", " + y)));
                //)
                {
                    return false;
                }

                //foreach (var f402 in f402List.Select(x=>x.ID).Distinct())
                //{
                //    DataContainerFacade.Delete<EDOLog>(f402);
                //}
            }

            foreach (var uk in ukList)
            {
                DataContainerFacade.Delete(uk);
            }

            return true;
        }

        private bool DeleteF402(EDOLogListItem logItem)
        {
            // согласно http://jira.vs.it.ru/browse/PFRPTK-1566 необходимо вначале выполнить поиск связанных записей 401 формы, 
            // а затем уже выполнять удаление, если среди текущих удаляемых записей нет связанной 401 формы

            //var f402 = DataContainerFacade.GetByID<EdoOdkF401402>(logItem.DocID.Value);

            //if (f402 == null)
            //{
            //    return false;
            //}

            var ukList = DataContainerFacade.GetListByProperty<F401402UK>("OutgoingDocNumber", logItem.DocRegNumberOut);

            var f401List = new List<EdoOdkF401402>();
            var f401LogList = new List<EDOLog>();
            //var f402List = new List<EDOLog>();

            foreach (var uk in ukList)
            {
                if (f401List.Any(x => x.ID == uk.EdoID)) continue;
                var f401 = DataContainerFacade.GetByID<EdoOdkF401402>(uk.EdoID);
                f401List.Add(f401);
                f401LogList.AddRange(DataContainerFacade.GetListByProperty<EDOLog>("DocId", f401.ID).Where(x => x.DocForm == "EDO_ODKF401"));

                //    if (!f402List.Any(x => x.DocRegNumberOut == uk.OutgoingDocNumber))
                //    {
                //        f402List.AddRange(DataContainerFacade.GetListByProperty<EDOLog>("DocRegNumberOut", uk.OutgoingDocNumber).Where(x => x.ID != logItem.ID));
                //    }
            }

            var itemsToDelete = List.Where(x => x.Selected);
            if (f401LogList.Any(x => itemsToDelete.Any(y => y.ID == x.ID)))
            {
                // не выполняем удаление, всплывающее окно с ошибкой будет выведено при попытке удаления 401 формы
                return false;
            }

            // восстанавливаем начальное состояние сущностей, измененных при импорте
            foreach (var uk in ukList)
            {
                if (uk.InitStatusID != null)
                {// если восстановление не было выполнено предыдущим удалением
                    uk.ControlFactDate = uk.InitControlFactDate;
                    uk.InitControlFactDate = null;
                    uk.OutgoingDocDate = uk.InitOutgoingDocDate;
                    uk.InitOutgoingDocDate = null;
                    uk.OutgoingDocNumber = uk.InitOutgoingDocNumber;
                    uk.InitOutgoingDocNumber = null;
                    uk.StatusID = uk.InitStatusID;
                    uk.InitStatusID = null;
                }

                DataContainerFacade.Save(uk);
            }


//            var f402Alert = f402List.Count > 0 ? "F402(" + f402List.Select(x => x.DocRegNum).Aggregate((x, y) => x + ", " + y) + ")" : string.Empty;
//            var f401Alert = f401List.Count > 0 ? "F401("+f401List.Select(x => x.RegNum).Distinct().Aggregate((x, y) => x + ", " + y)+")" : string.Empty;

//            if (!string.IsNullOrWhiteSpace(f401Alert) || !string.IsNullOrWhiteSpace(f402Alert))
//            {
//                if (!DialogHelper.Confirm(
//                    string.Format(@"Для удаляемой записи F402({0}) существуют импортированные записи {1}{3}{2}.
//Удалить все связанные записи?", logItem.DocRegNum, f401Alert, f402Alert, !string.IsNullOrWhiteSpace(f401Alert) && !string.IsNullOrWhiteSpace(f402Alert) ? " и " : string.Empty)))
//                {
//                    return false;
//                }

//                foreach (var f401 in f401List)
//                {
//                    DataContainerFacade.Delete(f401);
//                }

//                foreach (var log in f401LogList)
//                {
//                    DataContainerFacade.Delete(log);
//                }
//            }

//            foreach (var uk in ukList)
//            {
//                DataContainerFacade.Delete(uk);
//            }

            return true;
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }

        protected override void ExecuteRefreshList(object param)
        {
            List = BLServiceSystem.Client.GetLoadedODKLogList((bool)param, dateFrom, DateTo, SelectedForm == ALL_FORMS ? null : SelectedForm);
            if (List != null)
                List.ToList().ForEach(a => { if (string.IsNullOrEmpty(a.Filename)) a.Filename = "xml"; });         
        }

        public LoadedODKLogListViewModel(bool success)
            : base(success)
        {
            DataObjectTypeForJournal = typeof (object);
            DataObjectDescForJournal = "Составная таблица из данных EDO_ODK";
            DeleteSelectedItems = new DelegateCommand(CanExecuteDelete, ExecuteDelete);
        }
    }
}
