﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OUFV_manager)]
    public class CorrespondenceVRBaseListViewModel : CorrespondenceBaseListViewModel
    {
        public CorrespondenceVRBaseListViewModel()
        {
            DataObjectTypeForJournal = typeof(Document);
        }

		protected override List<CorrespondenceListItemNew> GetDocuments(Document.Statuses status, long? documentID = null, long? attachID = null)
        {
            return BLServiceSystem.Client.GetVRDocumentsListHib(status,documentID, attachID);
        }

	
	}
}
