﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OKIP_worker)]
    public class PFRBranchesListViewModel : ViewModelList
    {
        readonly List<FEDO> _mFedoList = DataContainerFacade.GetList<FEDO>();

        private List<PFRBranchListItem> m_List;
        public List<PFRBranchListItem> List
        {
            get
            {
                RaiseDataRefreshing();
                return m_List;
            }
            set
            {
                m_List = value;
                OnPropertyChanged("List");
            }
        }

        public PFRBranchesListViewModel()
        {
            DataObjectTypeForJournal = typeof (PFRBranch);
        }

        protected override void ExecuteRefreshList(object param)
        {
            var sw = Stopwatch.StartNew();          
            List = DataContainerFacade.GetList<PFRBranch>().ConvertAll<PFRBranchListItem>(a => new PFRBranchListItem(a, _mFedoList.First<FEDO>(f => f.ID == a.FEDO_ID)));
            sw.Stop();
            Debug.WriteLine("Загрузка списка отделений - {0} c", sw.Elapsed.Seconds);
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }

    public class PFRBranchListItem
    {
        private readonly string _fedo;
        private PFRBranch Item { get; set; }
        public PFRBranchListItem(PFRBranch item, FEDO fedo)
        {
            Item = item;
            //fedo = DataContainerFacade.GetByID<FEDO>(item.FEDO_ID).Name;
            this._fedo = fedo != null && !string.IsNullOrWhiteSpace(fedo.Name) ? fedo.Name : string.Empty;
        }

        public long ID { get { return Item.ID; } set { Item.ID = value; } }
        public string Name { get { return Item.Name; } set { Item.Name = value; } }
        public string Address { get { return Item.Address; } set { Item.Address = value; } }
        public int? RegionNumber { get { return Item.RegionNumber ?? 0; } set { Item.RegionNumber = value; } }
        
        public string RegionNumberDisplay { 
            get {
                int code = this.RegionNumber ?? 0;

                return code.ToString("D3");
            } 
        }
        public string Fedo => _fedo;
    }
}
