﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.ListItems;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OSRP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OSRP_directory_editor)]
    public class PfrBranchReportNpfNoticeListViewModel : ViewModelList
    {
        private List<PfrBranchReportNpfNoticeListItem> _items;
        public List<PfrBranchReportNpfNoticeListItem> Items
        {
            get
            {
                RaiseDataRefreshing();
                return _items;
            }
            set
            {
                _items = value;
                OnPropertyChanged("Items");
            }
        }

        public PfrBranchReportNpfNoticeListViewModel()
        {
            DataObjectTypeForJournal = typeof(PfrBranchReportNpfNoticeListItem);
        }

        protected override void ExecuteRefreshList(object param)
        {
            Items = BLServiceSystem.Client.GetPfrReportNpfNoticeList();
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}
