﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class F40VRListViewModel : F40ListViewModel
    {
        public F40VRListViewModel()
             : base(){}

         protected override long GetListCount()
         {
             return BLServiceSystem.Client.GetF040DetailsCount((int)Document.Types.VR);
         }
         protected override List<F040DetailsListItem> GetListPart(long index)
         {
             return BLServiceSystem.Client.GetF040DetailsByPage((int)Document.Types.VR, (int)index).ToList();
         } 

        //protected override void UpdatePart(List<F040DetailsListItem> part)
        // {
        //     this.CalculateDocNumbers(part);
        // }
        //protected override void ExecuteRefreshList(object param)
        //{
        //    CalculateDocNumbers(BLServiceSystem.Client.GetF040ListByTypeContract((int)Document.Types.VR));
        //}

    }
}
