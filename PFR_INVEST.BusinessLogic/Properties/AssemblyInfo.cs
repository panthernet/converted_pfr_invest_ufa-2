﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows.Markup;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ПТК ДОКИП. Модуль бизнес-логики приложения.")]

[assembly: InternalsVisibleTo("PFR_INVEST.Tests")]

[assembly: XmlnsDefinition("http://www.it.ru/pfrinvest/businesslogic", "PFR_INVEST.BusinessLogic")]
[assembly: XmlnsDefinition("http://www.it.ru/pfrinvest/businesslogic/commands", "PFR_INVEST.BusinessLogic.Commands")]
[assembly: XmlnsDefinition("http://www.it.ru/pfrinvest/businesslogic/converters", "PFR_INVEST.BusinessLogic.Converters")]