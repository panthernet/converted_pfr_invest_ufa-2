﻿using PFR_INVEST.Core.BusinessLogic;

namespace PFR_INVEST.Tests.Shared.Mocks
{
    public class BusyHelperMock:IBusyHelper
    {
        public void Start()
        {
           
        }

        public void Start(string message)
        {
           
        }

        public void Stop()
        {
           
        }
    }
}
