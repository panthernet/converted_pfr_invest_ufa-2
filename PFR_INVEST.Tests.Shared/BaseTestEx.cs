﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Common.Logger;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Tests.Shared.Mocks;

namespace PFR_INVEST.Tests.Shared
{
    public class BaseTestEx
    {
        private static readonly bool MbInited;
        protected static Random Random = new Random();

        static BaseTestEx()
        {
            ViewModelBase.DoNotThrowExceptionOnError = false;
            ViewModelBase.BusyHelper = new BusyHelperMock();
            ViewModelBase.ViewModelManager = new ViewModelManagerMock();
            ViewModelBase.Logger = new Log(null);
            App.ShowMessageBoxOnError = false;
            lock (typeof(BaseTest))
            {
                if (MbInited)
                    return;

                App.SetupEnvironment();

                for (var i = 0; i < 5; i++)
                {
                    try
                    {
                        CreateClient();
                    }
                    catch 
                    {
                        continue;
                    }
                    break;
                }


                ProcessAuthorization();
                MbInited = true;
            }
        }

        private static void  ProcessAuthorization()
        {
            WCFClient.Client.OpenDBConnection();
        }

        private static void CreateClient()
        {
            WCFClient.UseAutoRepair = false;
            WCFClient.ClientCredentials = new System.ServiceModel.Description.ClientCredentials();

            WCFClient.ClientCredentials.Windows.ClientCredential.Domain = AppSettings.DebugLoginDomain;
            WCFClient.ClientCredentials.Windows.ClientCredential.UserName = AppSettings.DebugLoginName;
            WCFClient.ClientCredentials.Windows.ClientCredential.Password = AppSettings.DebugLoginPassword;

            WCFClient.CreateClientByCredentials();
            WCFClient.UseAutoRepair = true;
        }

        protected T LoadFirstItem<T>() where T:BaseDataObject
        {
            var list = DataContainerFacade.GetList<T>();
            if (!list.Any())
                Assert.Inconclusive("List of '{0}' is empty", typeof(T).Name);
            return list.First();
        }

        protected void TestPublicProperty(ViewModelBase model, params string[] excludedProperties)
        {
            var card = model as ViewModelCard;
            if (card != null) 
            {
                if (card.State != ViewModelState.Create && !card.IsIDChanged && card.IsNotSavable == false) 
                {
                    //Assert.Fail("ID property not initialised");
                }
                    
            }


            var properties = model.GetType().GetProperties().Where(p => p.CanRead && p.GetIndexParameters().Length == 0);
            foreach (var pi in properties)
            {
                if (excludedProperties.Contains(pi.Name))
                    continue;
                var tmp = pi.GetValue(model, null);
                if (pi.CanWrite)
                    pi.SetValue(model, tmp, null);
            }
        }


        public XmlReader GetInputXMLFile(string filename, XmlReaderSettings settings)
        {
            var thisAssembly = Assembly.GetExecutingAssembly();
            const string path = "PFR_INVEST.Tests.TestFiles";
            return XmlReader.Create(thisAssembly.GetManifestResourceStream(path + "." + filename), settings);
        }

        public TextReader GetInputFile(string filename)
        {
            var thisAssembly = Assembly.GetExecutingAssembly();
            const string path = "PFR_INVEST.Tests.TestFiles";
            return new StreamReader(thisAssembly.GetManifestResourceStream(path + "." + filename));
        }

        public TextReader GetInputFile(string filename,string encoding)
        {
            var thisAssembly = Assembly.GetExecutingAssembly();
            const string path = "PFR_INVEST.Tests.TestFiles";
            return new StreamReader(thisAssembly.GetManifestResourceStream(path + "." + filename), System.Text.Encoding.GetEncoding(encoding));
        }
    }
}
