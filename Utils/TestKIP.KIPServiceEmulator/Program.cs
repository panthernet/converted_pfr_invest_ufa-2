﻿using System;
using System.Linq;
using System.ServiceModel;

namespace TestKIP.KIPServiceEmulator
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("!ВНИМАНИЕ! Для старта эмуляции сервиса необходимо запускать утилиту с правами админа.");
			Console.WriteLine();
			try
			{
				using (ServiceHost host = new ServiceHost(typeof(KIPServiceEmulator.KIPSeviceEmulator)))
				{
					var wsdl = new ServiceHost(typeof(KIPServiceEmulator.WSDLService));
					wsdl.AddServiceEndpoint(typeof(KIPServiceEmulator.IWSDLservice), new WebHttpBinding(), "RegisterService.wsdl");
					wsdl.Open();
					host.Open();

					Console.WriteLine("The service is ready at {0}", host.BaseAddresses.FirstOrDefault());
					Console.WriteLine("Press <Enter> to stop the service.");
					Console.WriteLine();
					Console.ReadLine();

					// Close the ServiceHost.
					host.Close();
					wsdl.Close();
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine();
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine(ex.Message);
			}
			Console.Write("");
			Console.ReadKey();
		}
	}
}
