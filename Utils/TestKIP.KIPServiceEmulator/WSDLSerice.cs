﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;


namespace TestKIP.KIPServiceEmulator
{
	[ServiceContract]
	public interface IWSDLservice 
	{

		[OperationContract]
		[WebGet(UriTemplate = "RegisterService.wsdl", BodyStyle = WebMessageBodyStyle.Bare)]
		Stream GetWsdl();
	}
	
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "KIPSeviceEmulator" in both code and config file together.
	//[DispatchByBodyElementServiceBehaviorAttribute]
	public class WSDLService : IWSDLservice
	{
		public Stream GetWsdl()
		{
			var baseFolder = Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]);
			var file = Path.Combine(baseFolder, "RegisterService.wsdl");
			var body = File.ReadAllBytes(file);

			return new MemoryStream(body);
			
		}
	}
}
