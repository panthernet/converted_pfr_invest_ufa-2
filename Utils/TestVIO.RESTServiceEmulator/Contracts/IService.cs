﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace TestVIO.RESTServiceEmulator.Contracts
{
    [ServiceContract]
    public interface IService
    {
        [OperationContract]
        [WebGet(UriTemplate = "contents/{contentNumber}/stream?documentId={documentId}&mimeType=application%2Fxml", ResponseFormat = WebMessageFormat.Xml)]
        string GetStream(string contentNumber, string documentId);
    }
}
