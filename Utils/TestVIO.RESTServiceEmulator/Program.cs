﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using System.Text;
using TestVIO.RESTServiceEmulator.Contracts;

namespace TestVIO.RESTServiceEmulator
{
    class Program
    {
        static void Main(string[] args)
        {
            string addr = $"localhost:{Config.RestPort}";

            WebServiceHost host = new WebServiceHost(typeof(Service), new Uri($"http://{addr}"));
            ServiceEndpoint ep = host.AddServiceEndpoint(typeof(IService), new WebHttpBinding(), Config.RestBaseUri);
            ServiceDebugBehavior stp = host.Description.Behaviors.Find<ServiceDebugBehavior>();
            stp.HttpHelpPageEnabled = false;
            host.Open();
            Console.WriteLine($"Service is up and running on {addr}");
            Console.WriteLine("Press enter to quit ");
            Console.ReadLine();
            host.Close();
        }
    }
}
