﻿using System;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace TestECASA.ServiceEmulator
{
	[ServiceContract]
	public interface IWSDLService 
	{

		[OperationContract]
        [WebGet(UriTemplate = "VerifyAuthenticationWSService.wsdl", BodyStyle = WebMessageBodyStyle.Bare)]
		Stream GetWsdl();
    }
	
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "KIPSeviceEmulator" in both code and config file together.
	//[DispatchByBodyElementServiceBehaviorAttribute]
	public class WSDLService : IWSDLService
	{
		public Stream GetWsdl()
		{
			var baseFolder = Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]);
            var file = Path.Combine(baseFolder, "VerifyAuthenticationWSService.wsdl");
			var body = File.ReadAllBytes(file);

			return new MemoryStream(body);
			
		}
    }

}
