﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TestECASA.ServiceEmulator
{
	[ServiceContract]
	public interface IMAPService 
	{
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "info?userGuid={userGuid}&appId={appId}&paramName={paramName}&otherParamName={otherParamName}",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        List<Ret1> Info(string userGuid, string appId, string paramName = null, string otherParamName = null);

    }

	public class MAPService : IMAPService
	{
	    public List<Ret1> Info(string userGuid, string appId, string paramName = null, string otherParamName = null)
	    {
	        var list = new List<Ret1>
	        {
	            new Ret1{name = "PAYMENT_DICTIONARIES.D"},
	            new Ret1{name = "PAYMENT_DICTIONARIES.U"},
	            new Ret1{name = "PAYMENT_DICTIONARIES.E"},
	            new Ret1{obligation = "checkOwner", attributeList = new [] { "FALSE" }},
	            new Ret1{obligation = "territory", attributeList = new [] { "opfr:005;to:001", "opfr:067;to:001", "opfr:008" }},
	        };
            return list;
	    }

    }

    [DataContract]
    public class Ret1
    {
        [DataMember(IsRequired = false, EmitDefaultValue = false)]
        public string name;
        [DataMember(IsRequired = false, EmitDefaultValue = false)]
        public string obligation;
        [DataMember(IsRequired = false, EmitDefaultValue = false)]
        public string[] attributeList;
    }
}
