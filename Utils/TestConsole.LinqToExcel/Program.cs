﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinqToExcel;
using System.IO;

namespace TestConsole.LinqToExcel
{
	class Program
	{
		static void Main(string[] args)
		{
			NSI();
		}

		static void NSI()
		{
			var folder = @"D:\WORK\PFR\PFR_PTK_DOKIP\PFR_INVEST\Анализ и проектирование\Интеграция\НСИ\2015-11-26 прислали";
			var excel = new ExcelQueryFactory();
			{
				excel.FileName = Path.Combine(folder, @"25. Негосударственные ПФ.xlsx");


				var data = excel.WorksheetRange("A2", "I100", "Перечень НПФ").Where(r => r["NPF_ID"] != null )
					.Select(r => new
				{
					NPF_ID = r["NPF_ID"].Value,
					NPF_Code = r["NPF_Code"].Value,
					NPF_FormName = r["NPF_FormName"].Value,
					NPF_FullName = r["NPF_FullName"].Value,
					NPF_ShortName = r["NPF_ShortName"].Value,
					NPF_INN = r["NPF_INN"].Value,
					NPF_Uaddress = r["NPF_Uaddress"].Value.ToString().Replace("\n"," "),
					NPF_Faddress = r["NPF_Faddress"].Value.ToString().Replace("\n", " "),
					NPF_Status = r["NPF_Status"].Value,
				}).ToList();

				var values = data.Select(r => string.Format("('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}')",
												r.NPF_ID, r.NPF_Code, r.NPF_FormName, r.NPF_FullName, r.NPF_ShortName, r.NPF_INN, r.NPF_Uaddress, r.NPF_Faddress, r.NPF_Status)).ToList();

				var sql = string.Join("," + Environment.NewLine, values.ToArray());
			}

			{
				excel.FileName = Path.Combine(folder, @"26. Управляющие компании.xls");


				var data = excel.WorksheetRange("A2", "J100", "Перечень УК").Where(r => r["UK_ID"] != null)
					.Select(r => new
					{
						UK_ID = r["UK_ID"].Value,
						UK_Code = r["UK_Code"].Value,
						UK_FormName = r["UK_FormName"].Value,
						UK_FullName = r["UK_FullName"].Value,
						UK_ShortName = r["UK_ShortName"].Value,
						UK_INN = r["UK_INN"].Value,
						UK_Uaddress = r["UK_Uaddress"].Value.ToString().Replace("\n", " "),
						UK_Faddress = r["UK_Faddress"].Value.ToString().Replace("\n", " "),
						UK_Port = r["UK_Port"].Value,
						UK_Contract = r["UK_Contract"].Value,
						UK_Status = r["UK_Status"].Value,
						UK_DataContEnd = r["UK_DataContEnd"].Value,						
					}).ToList();

				var values = data.Select(r => string.Format("('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}')",
												r.UK_ID, r.UK_Code, r.UK_FormName, r.UK_FullName, r.UK_ShortName, r.UK_INN, r.UK_Uaddress, r.UK_Faddress, r.UK_Port, r.UK_Contract, r.UK_Status, r.UK_DataContEnd)).ToList();

				var sql = string.Join("," + Environment.NewLine, values.ToArray());
			}

		}
	}
}
