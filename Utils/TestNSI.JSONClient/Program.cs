﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace TestNSI.JSONClient
{
	class Program
	{
		static void Main(string[] args)
		{
			WCFClient.StartClient("test", "test");

			//string msg = File.ReadAllText(@"D:\temp\NSI_json\create.json.txt");
			//string msg = File.ReadAllText(@"D:\temp\NSI_json\edit.json.txt");
			//string msg = File.ReadAllText(@"D:\temp\NSI_json\disable.json.txt");
			string msg = File.ReadAllText(@"D:\temp\NSI_json\enable.json.txt");

			WCFClient.Current.Client.ParseNSIMessage(msg);
		}
	}
}
