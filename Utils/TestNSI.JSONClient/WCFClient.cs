﻿using System;
using db2connector;
using PFR_INVEST.DataAccess.Client;
using System.ServiceModel.Description;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Security;
using db2connector.Contract;
using PFR_INVEST.Common.MessageCompressor;

namespace TestNSI.JSONClient
{
	public class WCFClient
	{
		public static WCFClient Current
		{
			get;
			set;
		}


		public static bool StartClient(string username, string password)
		{
		    Current = new WCFClient {ClientCredentials = new ClientCredentials()};
		    Current.ClientCredentials.Windows.ClientCredential = new NetworkCredential(username, password, "pfrinvst-dev.local");

			try
			{
				Current.CreateClientByCredentials();			
				
			}
			catch (InvalidLoginOrPasswordException)
			{
				return false;
			}
			catch (AuthInitException e)
			{
				return false;
			}
			catch (NotUserException)
			{
				return false;
			}
			catch (EndpointNotFoundException e)
			{
				return false;
			}
			catch (SecurityNegotiationException e)
			{
				return false;
			}
			catch (Exception e)
			{
				return false;
			}

		
			return true;
		}


		private DataServiceClient _wcfClient; //ServiceSystem.CreateNewDataClient();


		//public  event Action ConnectionRestoredEvent;
		public ClientCredentials CurrentClientCredentials
		{
			get
			{
				return _wcfClient.ClientCredentials;
			}
		}

		public ClientCredentials ClientCredentials { get; set; }

	
		

		private bool RepairConnection()
		{
			//пропускаем восстановление связи если приложение уже закрывается, больше обращений к сервису не требуется
			//if (App.IsClosing) return false;
			bool bError = false;
			try
			{
				if (_wcfClient.State == CommunicationState.Faulted)
					bError = true;
				else
				{
					//Simple call to check connection state
					_wcfClient.GetChannel().GetServiceVersion();
				}

			}
			catch (Exception ex)
			{
				bError = true;
				//App.log.WriteException(ex, "Возникла ошибка при восстановлении соединения к WCF сервису.");
			}

			if (_wcfClient.State != CommunicationState.Opened || bError)
			{
				if (ClientCredentials != null)
				{
					CreateClientByCredentials(true);
				}
				else
					return false;
			}

			return true;
		}

		public void CreateClientByCredentials()
		{
			CreateClientByCredentials(false);
		}

		public void CreateClientByCredentials(bool p_bRestore)
		{
			
			//bool wasActive = false;


			_wcfClient = ServiceSystem.CreateNewDataClient(ClientAuthType.None);

			if (ClientCredentials != null)
			{

					_wcfClient.ClientCredentials.Windows.ClientCredential.Domain = ClientCredentials.Windows.ClientCredential.Domain;
					_wcfClient.ClientCredentials.Windows.ClientCredential.UserName = ClientCredentials.Windows.ClientCredential.UserName;
					_wcfClient.ClientCredentials.Windows.ClientCredential.Password = ClientCredentials.Windows.ClientCredential.Password;
					_wcfClient.ClientCredentials.Windows.AllowedImpersonationLevel = ClientCredentials.Windows.AllowedImpersonationLevel;

				var behavior = _wcfClient.Endpoint.Behaviors.Find<IDB2Connector>();
				if (behavior == null)
				{

					_wcfClient.Endpoint.Behaviors.Add(new MessageCompressionAttribute(Compress.Reply | Compress.Request));
				}

				// Скрытие системного окна аутентификации при неверном логине/пароле
				_wcfClient.ClientCredentials.SupportInteractive = false;
				_wcfClient.Open();
				ClientCredentials = CurrentClientCredentials;

				// устанавливаем некоторые параметры журналирования.
				Client.SetCurrentUserInformation(
					ClientCredentials.Windows.ClientCredential.UserName,
					Environment.UserName,
					Environment.MachineName);

				try
				{


					//if (!AppSettings.WorkWithoutAD)
					//{
					//    var iresult = Client.InitDirectoryService(ClientCredentials.Windows.ClientCredential.UserName,
					//        ClientCredentials.Windows.ClientCredential.Password,
					//        ClientCredentials.Windows.ClientCredential.Domain);
					//    if (!iresult) throw new InvalidLoginOrPasswordException();
					//}
										

				}
				catch (Exception e)
				{
					if (e is NotUserException) throw e;
					throw new AuthInitException() { Ex = e };
				}
				finally
				{
					//Loading.UnloadWindow();
				}
			}

		}



		public IDB2Connector Client
		{
			get
			{
				if (_wcfClient == null) return null;
				//if (!UseAutoRepair) return _wcfClient.GetChannel();
				if (ClientCredentials == null) return _wcfClient.GetChannel();
				//UseAutoRepair = false;
				try
				{
					RepairConnection();
				}
				finally
				{
					//UseAutoRepair = true;
				}
				return _wcfClient.GetChannel();
			}
		}

		

		public void CloseClient()
		{
			try
			{
				if (_wcfClient.State != CommunicationState.Opened) return;
				// WCFClient.Client.CloseDB2Connection();
				// WCFClient.wcfClient.Close();
				//сервис закроет канал и тут будет исключение, пропускаем его
				try
				{
					Client.CloseConnection();
				}
				catch { }
			}
			catch (Exception ex)
			{
				//App.log.WriteLine("Возникла ошибка при попытке закрыть канал к WCF сервису.");
				//App.log.WriteException(ex);
			}

		}


	}
}