﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Tests.Shared;

namespace PFR_INVEST.Tests.Cards.BusinessLogic
{
    [TestClass]
    public class TestDialogViewModelsCreation : BaseTest
    {
        [TestMethod]
        public void ManualImportSettingsDlgViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new OnesPkipPathSettingsDlgViewModel(0);
            TestPublicProperty(model);
            model.CanExecuteDelete();
            model.Validate();
            model = new OnesPkipPathSettingsDlgViewModel(1);
            TestPublicProperty(model);
            model.CanExecuteDelete();
            model.Validate();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestImportRejAppDlgViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new ImportRejAppDlgViewModel();
            TestPublicProperty(model);
            model.CanExecuteImport();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestPrecisePenaltyViewModel()
        {
            var lst = DataContainerFacade.GetList<F401402UK>();
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new CreateOrderToPayViewModel(lst.First().ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestOrderDeliveredViewModel()
        {
            var lst = DataContainerFacade.GetList<F401402UK>();
            var chk = new PerformanceChecker();
            if (!lst.Any())
                Assert.Inconclusive();
            var model = new OrderDeliveredViewModel(lst.First().ID);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestOrderSentViewModel()
        {
            var lst = DataContainerFacade.GetList<F401402UK>();
            var chk = new PerformanceChecker();
            if (!lst.Any())
                Assert.Inconclusive();
            var model = new OrderSentViewModel(lst.First().ID);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestOrderPaidViewModel()
        {
            var lst = DataContainerFacade.GetList<F401402UK>();
            var chk = new PerformanceChecker();
            if (!lst.Any())
                Assert.Inconclusive();
            var model = new OrderPaidViewModel(lst.First().ID);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestMFLettersViewModel1()
        {
            var chk = new PerformanceChecker();
            var model = new MFLettersViewModel(MFLettersViewModel.TYPE_MF19);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestMFLettersViewModel2()
        {
            var chk = new PerformanceChecker();
            var model = new MFLettersViewModel(MFLettersViewModel.TYPE_MF20);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestMFLettersViewModel3()
        {
            var chk = new PerformanceChecker();
            var model = new MFLettersViewModel(MFLettersViewModel.TYPE_MF21);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestMFLettersViewModel4()
        {
            var chk = new PerformanceChecker();
            var model = new MFLettersViewModel(MFLettersViewModel.TYPE_MF22);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestMFLettersViewModel5()
        {
            var chk = new PerformanceChecker();
            var model = new MFLettersViewModel(MFLettersViewModel.TYPE_MF23);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestBanksVerificationViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new BanksVerificationViewModel(true);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }
        [TestMethod]
        public void TestExportDepClaimMaxListViewModel()
        {
            var chk = new PerformanceChecker();
            var list = DataContainerFacade.GetList<DepClaimMaxListItem>().Where(x => x.DepclaimType == (int)DepClaimMaxListItem.Types.Max);
            if (!list.Any()) Assert.Inconclusive();
            var model = new ExportDepClaimMaxListViewModel(DepClaimMaxListItem.Types.Max, list.First().DepclaimselectparamsId);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }
        [TestMethod]
        public void TestExportDepClaimListViewModel()
        {
            var chk = new PerformanceChecker();
            var list = DataContainerFacade.GetList<DepClaimMaxListItem>().Where(x => x.DepclaimType == (int)DepClaimMaxListItem.Types.Common);
            if (!list.Any()) Assert.Inconclusive();
            var model = new ExportDepClaimMaxListViewModel(DepClaimMaxListItem.Types.Common, list.First().DepclaimselectparamsId);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestImportKOListViewModel()
        {
            var chk = new PerformanceChecker();
            var list = DataContainerFacade.GetList<BankConclusion>();
            if (!list.Any()) Assert.Inconclusive();
            var model = new ImportBankConclusionDlgViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestImportDepClaimListViewModel()
        {
            var chk = new PerformanceChecker();
            var list = DataContainerFacade.GetList<DepClaimMaxListItem>().Where(x => x.DepclaimType ==(int)DepClaimMaxListItem.Types.Common);
            if (!list.Any()) Assert.Inconclusive();
            var model = new ImportDepClaimMaxViewModel(list.First().ID, DepClaimMaxListItem.Types.Common);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }
        [TestMethod]
        public void TestImportDepClaimMaxListViewModel()
        {
            var chk = new PerformanceChecker();
            var list = DataContainerFacade.GetList<DepClaimMaxListItem>().Where(x => x.DepclaimType == (int)DepClaimMaxListItem.Types.Max);
            if (!list.Any()) Assert.Inconclusive();
            var model = new ImportDepClaimMaxViewModel(list.First().ID, DepClaimMaxListItem.Types.Max);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }
        [TestMethod]
        public void TestImportDepClaim2ListViewModel()
        {
            var list = DataContainerFacade.GetList<DepClaimSelectParams>();
            if (!list.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new ImportDepClaim2DlgViewModel(list.First().ID);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestImportDepClaim2ConfirmDlgViewModel()
        {
            var list = DataContainerFacade.GetList<DepClaimSelectParams>();
            if (!list.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new ImportDepClaim2ConfirmDlgViewModel(list.First().ID);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestLookupPersonGridViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new LookupPersonGridDlgViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestBankStockCodeDlgViewModel()
        {
            var list = DataContainerFacade.GetList<BankStockCode>();
            if (!list.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new BankStockCodeDlgViewModel(list.First(), false, new List<long>());
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestImportOwnCapitalDlgViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new ImportOwnCapitalDlgViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestLegalEntityCertificateDlgViewModel()
        {
            var list = DataContainerFacade.GetList<LegalEntityCourierCertificate>();
            if (!list.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new LegalEntityCertificateDlgViewModel(list.First());
            TestPublicProperty(model);
            chk.CheckPerformance();
        }
        
        [TestMethod]
        public void TestLegalEntityLetterOfAttorneyDlgViewModel()
        {
            var list = DataContainerFacade.GetList<LegalEntityCourierLetterOfAttorney>();
            if (!list.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new LegalEntityLetterOfAttorneyDlgViewModel(list.First());
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestGarantFondDialogViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new GarantFondDialogViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }
        
    }
}
