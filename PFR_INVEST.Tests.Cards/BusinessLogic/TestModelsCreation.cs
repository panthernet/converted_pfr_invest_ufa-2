﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.BusinessLogic.ViewModelsCard;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;
using PFR_INVEST.BusinessLogic.ViewModelsDialog.Analyze.AnalyzeConditionChecker;
using PFR_INVEST.BusinessLogic.ViewModelsPrint;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataAccess.Client.ObjectsExtensions;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Analyze;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Tests.Shared;
using PFR_INVEST.Tests.Shared.Mocks;

namespace PFR_INVEST.Tests.Cards.BusinessLogic
{
    [TestClass]
    public class TestModelsCreation : BaseTest
    {

        [TestMethod]
        public void TestGetCourceManuallyOrFromCBRSiteDialogViewModel()
        {
            var chk = new PerformanceChecker();
            var lst = DataContainerFacade.GetList<Currency>();
            if (!lst.Any())
                Assert.Inconclusive();
            var model =
                new GetCourceManuallyOrFromCBRSiteDialogViewModel(lst.First().ID, DateTime.Today.AddMonths(6));
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }


        [TestMethod]
        public void TestTreasurityViewModel()
        {
            var lst = DataContainerFacade.GetList<KDoc>();
            var chk = new PerformanceChecker();
            if (!lst.Any())
                Assert.Inconclusive();
            var model = new TreasurityViewModel(ViewModelState.Edit, lst.First().ID, null);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestRenameSIDialogViewModel()
        {
            var chk = new PerformanceChecker();
            //RenameSIDialogViewModel model = new RenameSIDialogViewModel(GetClient().Client);
            var model = new RenameSIDialogViewModel(new LegalEntity { ID = 0 });
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestSelectAccountDlgViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new SelectAccountDialogViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestViolationCategoryViewModel()
        {
            var lst = DataContainerFacade.GetList<CategoryF140>();
            var chk = new PerformanceChecker();
            if (!lst.Any())
                Assert.Inconclusive();
            var model = new ViolationCategoryViewModel(lst.First().ID, ViewModelState.Edit);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestPortfolioViewModel()
        {
            var lst = DataContainerFacade.GetList<Portfolio>();
            var chk = new PerformanceChecker();
            if (!lst.Any())
                Assert.Inconclusive();
            var model = new PortfolioViewModel(lst.First().ID, ViewModelState.Edit);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestReorganizationViewModel()
        {
            var lst = DataContainerFacade.GetList<Reorganization>();
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new ReorganizationViewModel(lst.First().ID, 0, ViewModelState.Edit);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestSecurityViewModel()
        {
            var lst = DataContainerFacade.GetList<Security>();
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new SecurityViewModel(lst.First().ID, ViewModelState.Edit);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestPrintTransferRequestViewModel()
        {
            var lst = DataContainerFacade.GetList<Register>();
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new PrintTransferRequestViewModel(lst.First().ID);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestPrepaymentViewModel()
        {
            long id = 0;
            var lst = DataContainerFacade.GetList<DopAps>();
            if (!lst.Any())
                Assert.Inconclusive();
            else
                id = lst.First().ID;
            var chk = new PerformanceChecker();
            var model = new PrepaymentViewModel(PortfolioIdentifier.PortfolioTypes.SPN);
            model.Load(id);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestRecalcUKPortfoliosViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new RecalcUKPortfoliosViewModel();
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }


        [TestMethod]
        public void TestZLRedistViewModel()
        {
            var lst = DataContainerFacade.GetList<ERZL>();
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new ZLRedistViewModel();
            model.LoadZLRedist(lst.First().ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestDueDeadViewModel()
        {
            var lst = DataContainerFacade.GetList<Aps>();
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new DueDeadViewModel(PortfolioIdentifier.PortfolioTypes.SPN);
            model.Load(lst.First().ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestPreferencesViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new PreferencesViewModel();
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestSendEmailsToUKDialogModel()
        {
            var chk = new PerformanceChecker();
            var model = new SendEmailsToUKDialogModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestStopSIDialogModel()
        {
            var lst = DataContainerFacade.GetList<LegalEntity>();
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new StopSIDialogModel(lst.First().ID, StopSIDialogModel.StopType.StopActivities);
            TestPublicProperty(model);
            chk.CheckPerformance();

            var lst2 = DataContainerFacade.GetList<Contract>();
            if (!lst.Any())
                Assert.Inconclusive();
            model = new StopSIDialogModel(lst2.First().ID, StopSIDialogModel.StopType.StopСontract);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestERZLDialogViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new ERZLDialogViewModel();
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestViolationViewModel()
        {
            var lst = DataContainerFacade.GetList<EdoOdkF140>();
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new ViolationViewModel(lst.First().ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestDueExcessViewModel()
        {
            var lst = DataContainerFacade.GetList<Aps>();
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new DueExcessViewModel(PortfolioIdentifier.PortfolioTypes.SPN);
            model.Load(lst.First().ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestSIViewModel()
        {
            var lst = GetClient().Client.GetSIListHib(false);
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new SIViewModel(lst.First().LegalEntityID, ViewModelState.Create);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestPFRContactViewModel()
        {
            var lst = DataContainerFacade.GetList<PFRContact>();
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new PFRContactViewModel(lst.First().ID, ViewModelState.Edit);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestYieldSIViewModel()
        {
            var list = DataContainerFacade.GetList<McProfitAbility>();
            var prof = list.FirstOrDefault(pr => pr.GetContract().TypeID == (int)Document.Types.SI);
            long id = 0;
            if (prof == null)
                Assert.Inconclusive();
            else
                id = prof.ID;
            var chk = new PerformanceChecker();
            YieldViewModel model = new YieldSIViewModel(ViewModelState.Edit, id);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestYieldVRViewModel()
        {
            var list = DataContainerFacade.GetList<McProfitAbility>();
            var prof = list.FirstOrDefault(pr => pr.GetContract().TypeID == (int)Document.Types.VR);
            long id = 0;
            if (prof == null)
                Assert.Inconclusive();
            else
                id = prof.ID;
            var chk = new PerformanceChecker();
            YieldViewModel model = new YieldVRViewModel(ViewModelState.Edit, id);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestSupAgreementViewModel()
        {
            var lst = DataContainerFacade.GetList<SupAgreement>();
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new SupAgreementViewModel(lst.First().ID, ViewModelState.Edit);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestSIContractViewModel()
        {
            var lst = GetClient().Client.GetSIContractsListHib();
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new SIContractViewModel(lst.First().ContractID, ViewModelState.Edit);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestRegisterViewModel()
        {
            var lst = DataContainerFacade.GetList<Register>();
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new RegisterViewModel(lst.First().ID, ViewModelState.Edit);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestPaymentViewModel()
        {
            var lst = DataContainerFacade.GetList<Security>();
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new PaymentViewModel(lst.First().ID, ViewModelState.Create);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestRateViewModel()
        {
            var lst = DataContainerFacade.GetList<Curs>();
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new RateViewModel(lst.First().ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestBankViewModel()
        {
            var lst = GetClient().Client.GetBanksListHib();
            Assert.Inconclusive(lst.Count.ToString());
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var firstID = lst.First().ID;
            var model = new BankViewModel(firstID);
            TestPublicProperty(model, "SelectedPfrAgreementStatus");
            model.CanExecuteSave();
            chk.CheckPerformance();

        }

        [TestMethod]
        public void TestADRoleViewModel()
        {
            var lst = AP.Provider.GetRoles();
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new ADRoleViewModel(lst.First());
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestADUserViewModel()
        {
            var lst = AP.Provider.GetAuthUsers();
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new ADUserViewModel(lst.First());
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestDepClaimSelectParamsViewModel()
        {
            var item = LoadFirstItem<DepClaimSelectParams>();
            var chk = new PerformanceChecker();
            var model = new DepClaimSelectParamsViewModel(item.ID, ViewModelState.Edit);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestImportDepClaim2DlgViewModel()
        {
            var chk = new PerformanceChecker();
            var lst = DataContainerFacade.GetList<DepClaimSelectParams>();
            if (!lst.Any())
                Assert.Inconclusive();
            var model = new ImportDepClaim2DlgViewModel(lst.First().ID);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestImportDepClaim2ConfirmDlgViewModel()
        {
            var chk = new PerformanceChecker();
            var lst = DataContainerFacade.GetList<DepClaimSelectParams>();
            if (!lst.Any())
                Assert.Inconclusive();
            var model = new ImportDepClaim2ConfirmDlgViewModel(lst.First().ID);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestDepClaim2PrintModel()
        {
            var chk = new PerformanceChecker();
            var lst = DataContainerFacade.GetList<DepClaimSelectParams>();
            if (!lst.Any())
                Assert.Inconclusive();
            var model = new PrintExportDepClaim2(lst.First(), DateTime.Now);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestDepClaim2ConfirmPrintModel()
        {
            var chk = new PerformanceChecker();
            var lst = DataContainerFacade.GetList<DepClaimSelectParams>();
            if (!lst.Any())
                Assert.Inconclusive();
            var model = new PrintExportDepClaim2Confirm(lst.First(), DateTime.Now);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestDepClaimOfferCreateViewModel()
        {
            var chk = new PerformanceChecker();
            var lst = DataContainerFacade.GetList<DepClaimSelectParams>();
            if (!lst.Any())
                Assert.Inconclusive();
            var model = new DepClaimOfferCreateViewModel(lst.First().ID);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestDepClaimOfferViewModel()
        {
            var chk = new PerformanceChecker();
            var lst = DataContainerFacade.GetList<DepClaimOffer>();
            if (!lst.Any())
                Assert.Inconclusive();
            var model = new DepClaimOfferViewModel(lst.First().ID, ViewModelState.Edit);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestPrintDepClaimOffer()
        {
            var chk = new PerformanceChecker();
            var lst = DataContainerFacade.GetList<DepClaimOffer>();
            if (!lst.Any())
                Assert.Inconclusive();
            var model = new PrintDepClaimOffer(lst.First().ID);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestPrintNotAcceptedDepClaimOffer()
        {
            var chk = new PerformanceChecker();
            var lst = DataContainerFacade.GetList<DepClaimOffer>();
            if (!lst.Any())
                Assert.Inconclusive();
            var persons = DataContainerFacade.GetList<Person>();
            if (!persons.Any())
                Assert.Inconclusive();
            var model = new PrintNotAcceptedDepClaimOffer(lst.First().ID, persons.First(), persons.First(),
                persons.First());
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        #region Analyze tests

        [TestMethod]
        public void TestAnalyzeIncomingstatementsEditViewModel_Create()
        {
            var chk = new PerformanceChecker();
            var vm = new AnalyzeIncomingstatementsEditViewModel(null, RibbonStateBL.CBState, ViewModelState.Create, true);
            TestPublicProperty(vm);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestAnalyzeInsuredpersonEditViewModel_Create()
        {
            var chk = new PerformanceChecker();
            var vm = new AnalyzeInsuredpersonEditViewModel(null, RibbonStateBL.CBState, ViewModelState.Create, true);
            TestPublicProperty(vm);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestAnalyzePaymentyearrateEditViewModel_Create()
        {
            var chk = new PerformanceChecker();
            var vm = new AnalyzePaymentyearrateEditViewModel(null, RibbonStateBL.CBState, ViewModelState.Create, true);
            TestPublicProperty(vm);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestAnalyzePensionfundtonpfEditViewModel_Create()
        {
            var chk = new PerformanceChecker();
            var vm = new AnalyzePensionfundtonpfEditViewModel(null, RibbonStateBL.CBState, ViewModelState.Create, true,
                true);
            TestPublicProperty(vm);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestAnalyzePensionPlacementEditViewModel_Create()
        {
            var chk = new PerformanceChecker();
            var vm = new PensionPlacementEditViewModel(null, RibbonStateBL.CBState, ViewModelState.Create, true);
            TestPublicProperty(vm);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestAnalyzeYieldfundsEditViewModel_Create()
        {
            var chk = new PerformanceChecker();
            var vm = new AnalyzeYieldfundsEditViewModel(null, RibbonStateBL.CBState, ViewModelState.Create, true);
            TestPublicProperty(vm);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestAnalyzeIncomingstatementsEditViewModel_OpenEdit()
        {
            var chk = new PerformanceChecker();
            var rp = LoadFirstItem<AnalyzeIncomingstatementsReport>();
            var vm = new AnalyzeIncomingstatementsEditViewModel(rp, RibbonStateBL.CBState, ViewModelState.Edit);
            TestPublicProperty(vm);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestAnalyzeInsuredpersonEditViewModel_OpenEdit()
        {
            var chk = new PerformanceChecker();
            var rp = LoadFirstItem<AnalyzeInsuredpersonReport>();
            var vm = new AnalyzeInsuredpersonEditViewModel(rp, RibbonStateBL.CBState, ViewModelState.Edit);
            TestPublicProperty(vm);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestAnalyzePaymentyearrateEditViewModel_OpenEdit()
        {
            var chk = new PerformanceChecker();
            var rp = LoadFirstItem<AnalyzePaymentyearrateReport>();
            var vm = new AnalyzePaymentyearrateEditViewModel(rp, RibbonStateBL.CBState, ViewModelState.Edit);
            TestPublicProperty(vm);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestAnalyzePensionfundtonpfEditViewModel_OpenEdit()
        {
            var chk = new PerformanceChecker();
            var rp = LoadFirstItem<AnalyzePensionfundtonpfReport>();
            var vm = new AnalyzePensionfundtonpfEditViewModel(rp, RibbonStateBL.CBState, ViewModelState.Edit);
            TestPublicProperty(vm);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestAnalyzePensionPlacementEditViewModel_OpenEdit()
        {
            var chk = new PerformanceChecker();
            var rp = LoadFirstItem<AnalyzePensionplacementReport>();
            var vm = new PensionPlacementEditViewModel(rp, RibbonStateBL.CBState, ViewModelState.Edit);
            TestPublicProperty(vm);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestAnalyzeYieldfundsEditViewModel_OpenEdit()
        {
            var chk = new PerformanceChecker();
            var rp = LoadFirstItem<AnalyzeYieldfundsReport>();
            var vm = new AnalyzeYieldfundsEditViewModel(rp, RibbonStateBL.CBState, ViewModelState.Edit);
            TestPublicProperty(vm);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestAnalyzeYieldfundsEditViewModel_OpenArch_CantSave()
        {
            var chk = new PerformanceChecker();
            var rp = LoadFirstItem<AnalyzeYieldfundsReport>();
            var vm = new AnalyzeYieldfundsEditViewModel(rp, RibbonStateBL.CBState, ViewModelState.Edit, false, true);
            TestPublicProperty(vm);
            if (vm.CanExecuteSave())
                Assert.Fail();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestAnalyzePensionPlacementEditViewModel_OpenArch_CantSave()
        {
            var chk = new PerformanceChecker();
            var rp = LoadFirstItem<AnalyzePensionplacementReport>();
            var vm = new PensionPlacementEditViewModel(rp, RibbonStateBL.CBState, ViewModelState.Edit, false, true);
            TestPublicProperty(vm);
            if (vm.CanExecuteSave())
                Assert.Fail();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestAnalyzePensionPlacementEditViewModel_CanSaveIfHaveChanges()
        {
            var chk = new PerformanceChecker();
            var rp = LoadFirstItem<AnalyzePensionplacementReport>();
            var vm = new PensionPlacementEditViewModel(rp, RibbonStateBL.CBState, ViewModelState.Edit);
            if (vm.CanExecuteSave())//не должно быть доступно сохранение
                Assert.Fail();
            vm.NPFData.First().Total = 1234;
            if (!vm.CanExecuteSave())//должно стать доступно сохранение
                Assert.Fail();
            chk.CheckPerformance();
        }
        [TestMethod]
        public void TestYielfundsEditViewModel_CanSaveIfHaveChanges()
        {
            var chk = new PerformanceChecker();
            var rp = LoadFirstItem<AnalyzeYieldfundsReport>();
            var vm = new AnalyzeYieldfundsEditViewModel(rp, RibbonStateBL.CBState, ViewModelState.Edit);
            if (vm.CanExecuteSave())//не должно быть доступно сохранение
                Assert.Fail();
            vm.EditData.First().Total = 346789;
            if (!vm.CanExecuteSave())//должно стать доступно сохранение
                Assert.Fail();
            chk.CheckPerformance();
        }
        [TestMethod]
        public void TestPensionfundToNpfEditViewModel_CanSaveIfHaveChanges()
        {
            var chk = new PerformanceChecker();
            var rp = LoadFirstItem<AnalyzePensionfundtonpfReport>();
            var vm = new AnalyzePensionfundtonpfEditViewModel(rp, RibbonStateBL.CBState, ViewModelState.Edit);
            if (vm.CanExecuteSave())//не должно быть доступно сохранение
                Assert.Fail();
            vm.EditData.First().Total = 346789;
            if (!vm.CanExecuteSave())//должно стать доступно сохранение
                Assert.Fail();
            chk.CheckPerformance();
        }
        [TestMethod]
        public void TestIncomingstatementsEditViewModel_CanSaveIfHaveChanges()
        {
            var chk = new PerformanceChecker();
            var rp = LoadFirstItem<AnalyzeIncomingstatementsReport>();
            var vm = new AnalyzeIncomingstatementsEditViewModel(rp, RibbonStateBL.CBState, ViewModelState.Edit);
            if (vm.CanExecuteSave())//не должно быть доступно сохранение
                Assert.Fail();
            vm.EPGUEditItems.First().SrochCount = 1111;
            if (!vm.CanExecuteSave())//должно стать доступно сохранение
                Assert.Fail();
            chk.CheckPerformance();
        }
        [TestMethod]
        public void TestInsuredPersonEditViewModel_CanSaveIfHaveChanges()
        {
            var chk = new PerformanceChecker();
            var rp = LoadFirstItem<AnalyzeInsuredpersonReport>();
            var vm = new AnalyzeInsuredpersonEditViewModel(rp, RibbonStateBL.CBState, ViewModelState.Edit);
            if (vm.CanExecuteSave())//не должно быть доступно сохранение
                Assert.Fail();
            vm.EditData.First().Total = 4541;
            if (!vm.CanExecuteSave())//должно стать доступно сохранение
                Assert.Fail();
            chk.CheckPerformance();
        }

        #endregion

        #region Alerts tests

        //Проверка логики определения наличия незаполненных отчетов 1.1 для всех причастных ролей
        [TestMethod]
        public void CheckHasAlertsReport1_1FilledDataTest()
        {
            //проверка по кварталу
            var year = DateTime.Now.Year;
            var actId = 140;

            var dbMock = new AnalyzeMockDB();

            //настройка репозитория для отчета 1.1
            dbMock.Set1_1_1_2Data(year, null, (short)actId, false);
           // dbMock.Set1_1_1_2Data(year, quarter, (short)actId, false);
            dbMock.Set1_1_1_2Data(year - 1, 4, (short)actId, false);
            
            //список правил тестирования
           
            
            //Массив ролей учавствующих в заполнении отчетов
            var reportRoles = new[]
            {DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OVSI_worker};

            //класс с логикой проверки заполненности отчета
            var conditionCheck = new YieldfundsN140Condition();

            //запрос настроек правила правила проверки отчета с БД
            var conditionSettings = dbMock.GetCondition(typeof(YieldfundsN140Condition).Name);
            
            //Переназначение DB2Connector на mock репозиторий
            ServiceSystem.GetDataServiceDelegate = () => dbMock.Connector;

            //Проверка каждой роли на заполненных отчетах
            foreach (var role in reportRoles)
            {
                conditionCheck.CurrentUserRoles = new List<DOKIP_ROLE_TYPE> { role };

                //список сообщений о незаполненных отчетах
                var result = new List<LoginMessageItem>();

                conditionCheck.CheckCondition(result, conditionSettings);

                //ожидается: все отчеты заполненны, сообщений нет
                Assert.IsTrue(result.Count == 0);
            }

            //Тестирование наличия незаполненных отчетов
            dbMock.IsReturnEmptyList = true;
            dbMock.Set1_1_1_2Data(year, null, (short)actId, false);
            // dbMock.Set1_1_1_2Data(year, quarter, (short)actId, false);
            dbMock.Set1_1_1_2Data(year - 1, 4, (short)actId, false);

            //Проверка каждой роли на незаполненных отчетах
            foreach (var role in reportRoles)
            {
                conditionCheck.CurrentUserRoles = new List<DOKIP_ROLE_TYPE> { role };

                //список сообщений о незаполненных отчетах
                var result = new List<LoginMessageItem>();

                conditionCheck.CheckCondition(result, conditionSettings);

                //ожидается: все отчеты не заполненны, сообщения есть
                Assert.IsTrue(result.Count != 0);
            }


            //foreach (var con in DataContainerFacade.GetList<AnalyzeCondition>().Where(c => c.Status == 1 && ((DOKIP_ROLE_TYPE)c.UserRoleFlags).HasFlag(role)))
            //{
            //    var checkClass = col;
            //    if (checkClass != null)
            //    {
            //        var conMessages = new List<LoginMessageItem>();
            //        if (checkClass.CheckCondition(conMessages, con))
            //            result.AddRange(conMessages);
            //    }
            //}

        }

        //Проверка логики определения наличия незаполненных отчетов 1.2 для всех причастных ролей
        [TestMethod]
        public void CheckHasAlertsReport1_2FilledDataTest()
        {
            //проверка по кварталу
            var year = DateTime.Now.Year;
            var actId = 107;

            var dbMock = new AnalyzeMockDB();

            //настройка репозитория для отчета 1.1
            dbMock.Set1_1_1_2Data(year, null, (short) actId, false);
            dbMock.Set1_1_1_2Data(year - 1, 4, (short) actId, false);

            //список правил тестирования


            //Массив ролей учавствующих в заполнении отчетов
            var reportRoles = new[]
            {DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OVSI_worker};

            //класс с логикой проверки заполненности отчета
            var conditionCheck = new YieldfundsN107Condition();

            //запрос настроек правила правила проверки отчета с БД
            var conditionSettings = dbMock.GetCondition(typeof (YieldfundsN107Condition).Name);

            //Переназначение DB2Connector на mock репозиторий
            ServiceSystem.GetDataServiceDelegate = () => dbMock.Connector;

            //Проверка каждой роли на заполненных отчетах
            foreach (var role in reportRoles)
            {
                conditionCheck.CurrentUserRoles = new List<DOKIP_ROLE_TYPE> {role};

                //список сообщений о незаполненных отчетах
                var result = new List<LoginMessageItem>();

                conditionCheck.CheckCondition(result, conditionSettings);

                //ожидается: все отчеты заполненны, сообщений нет
                Assert.IsTrue(result.Count == 0);
            }

            //Тестирование наличия незаполненных отчетов
            dbMock.IsReturnEmptyList = true;
            dbMock.Set1_1_1_2Data(year, null, (short)actId, false);
            // dbMock.Set1_1_1_2Data(year, quarter, (short)actId, false);
            dbMock.Set1_1_1_2Data(year - 1, 4, (short)actId, false);

            //Проверка каждой роли на незаполненных отчетах
            foreach (var role in reportRoles)
            {
                conditionCheck.CurrentUserRoles = new List<DOKIP_ROLE_TYPE> { role };

                //список сообщений о незаполненных отчетах
                var result = new List<LoginMessageItem>();

                conditionCheck.CheckCondition(result, conditionSettings);

                //ожидается: все отчеты не заполненны, сообщения есть
                Assert.IsTrue(result.Count != 0);
            }
        }

        //Проверка логики определения наличия незаполненных отчетов 2 для всех причастных ролей
        [TestMethod]
        public void CheckHasAlertsReport2FilledDataTest()
        {
          
            var year = DateTime.Now.Year;
            var withSub = 0;
            var dbMock = new AnalyzeMockDB();

            //заполнение репозитория
            dbMock.Set2_3Data(year, null, withSub);//заполнение отчетов за текущий год
            dbMock.Set2_3Data(year - 1, 4, withSub);//прошлый год 4й квартал

            //список правил тестирования


            //Массив ролей учавствующих в заполнении отчетов
            var reportRoles = new[]
            {DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OVSI_worker};

            //класс с логикой проверки заполненности отчета
            var conditionCheck = new PensionfundtonpfCondition();

            //запрос настроек правила проверки отчета с БД
            var conditionSettings = dbMock.GetCondition(typeof(PensionfundtonpfCondition).Name);

            //Переназначение DB2Connector на mock репозиторий
            ServiceSystem.GetDataServiceDelegate = () => dbMock.Connector;

            //Проверка каждой роли на заполненных отчетах
            foreach (var role in reportRoles)
            {
                conditionCheck.CurrentUserRoles = new List<DOKIP_ROLE_TYPE> { role };

                //список сообщений о незаполненных отчетах
                var result = new List<LoginMessageItem>();

                conditionCheck.CheckCondition(result, conditionSettings);

                //ожидается: все отчеты заполненны, сообщений нет
                Assert.IsTrue(result.Count == 0);
            }

            #region тестирование незаполненных отчетов:

            dbMock.IsReturnEmptyList = true;
            dbMock.Set2_3Data(year, null, withSub);
            dbMock.Set2_3Data(year - 1, 4, withSub);

            //Проверка каждой роли на незаполненных отчетах
            foreach (var role in reportRoles)
            {
                conditionCheck.CurrentUserRoles = new List<DOKIP_ROLE_TYPE> { role };

                //список сообщений о незаполненных отчетах
                var result = new List<LoginMessageItem>();

                conditionCheck.CheckCondition(result, conditionSettings);

                //ожидается: все отчеты не заполненны, сообщения есть
                Assert.IsTrue(result.Count != 0);
            }
            #endregion
        }

        //Проверка логики определения наличия незаполненных отчетов 3 для всех причастных ролей
        [TestMethod]
        public void CheckHasAlertsReport3FilledDataTest()
        {

            var year = DateTime.Now.Year;
            var withSub = 1;
            var dbMock = new AnalyzeMockDB();

            //заполнение репозитория
            dbMock.Set2_3Data(year, null, withSub);//заполнение отчетов за текущий год
            dbMock.Set2_3Data(year - 1, 4, withSub);//прошлый год 4й квартал

            //список правил тестирования


            //Массив ролей учавствующих в заполнении отчетов
            var reportRoles = new[]
            {DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OVSI_worker};

            //класс с логикой проверки заполненности отчета
            var conditionCheck = new PensionfundtonpfWithSubtractionCondition();

            //запрос настроек правила проверки отчета с БД
            var conditionSettings = dbMock.GetCondition(typeof(PensionfundtonpfWithSubtractionCondition).Name);

            //Переназначение DB2Connector на mock репозиторий
            ServiceSystem.GetDataServiceDelegate = () => dbMock.Connector;

            //Проверка каждой роли на заполненных отчетах
            foreach (var role in reportRoles)
            {
                conditionCheck.CurrentUserRoles = new List<DOKIP_ROLE_TYPE> { role };

                //список сообщений о незаполненных отчетах
                var result = new List<LoginMessageItem>();

                conditionCheck.CheckCondition(result, conditionSettings);

                //ожидается: все отчеты заполненны, сообщений нет
                Assert.IsTrue(result.Count == 0);
            }

            #region тестирование незаполненных отчетов:

            dbMock.IsReturnEmptyList = true;
            dbMock.Set2_3Data(year, null, withSub);
            dbMock.Set2_3Data(year - 1, 4, withSub);

            //Проверка каждой роли на незаполненных отчетах
            foreach (var role in reportRoles)
            {
                conditionCheck.CurrentUserRoles = new List<DOKIP_ROLE_TYPE> { role };

                //список сообщений о незаполненных отчетах
                var result = new List<LoginMessageItem>();

                conditionCheck.CheckCondition(result, conditionSettings);

                //ожидается: все отчеты не заполненны, сообщения есть
                Assert.IsTrue(result.Count != 0);
            }
            #endregion
        }
        //Проверка логики определения наличия незаполненных отчетов 4 для всех причастных ролей
        [TestMethod]
        public void CheckHasAlertsReport4FilledDataTest()
        {

            var year = DateTime.Now.Year;
           
            var dbMock = new AnalyzeMockDB();

            //заполнение репозитория
            dbMock.Set4Data(year, null);//заполнение отчетов за текущий год
            dbMock.Set4Data(year - 1, 4);//прошлый год 4й квартал

            //список правил тестирования


            //Массив ролей учавствующих в заполнении отчетов
            var reportRoles = new[]
            {DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OFPR_worker};

            //класс с логикой проверки заполненности отчета
            var conditionCheck = new InsuredpersonCondition();

            //запрос настроек правила проверки отчета с БД
            var conditionSettings = dbMock.GetCondition(typeof(InsuredpersonCondition).Name);

            //Переназначение DB2Connector на mock репозиторий
            ServiceSystem.GetDataServiceDelegate = () => dbMock.Connector;

            //Проверка каждой роли на заполненных отчетах
            foreach (var role in reportRoles)
            {
                conditionCheck.CurrentUserRoles = new List<DOKIP_ROLE_TYPE> { role };

                //список сообщений о незаполненных отчетах
                var result = new List<LoginMessageItem>();

                conditionCheck.CheckCondition(result, conditionSettings);

                //ожидается: все отчеты заполненны, сообщений нет
                Assert.IsTrue(result.Count == 0);
            }

            #region тестирование незаполненных отчетов:

            dbMock.IsReturnEmptyList = true;
            dbMock.Set4Data(year, null);
            dbMock.Set4Data(year - 1, 4);

            //Проверка каждой роли на незаполненных отчетах
            foreach (var role in reportRoles)
            {
                conditionCheck.CurrentUserRoles = new List<DOKIP_ROLE_TYPE> { role };

                //список сообщений о незаполненных отчетах
                var result = new List<LoginMessageItem>();

                conditionCheck.CheckCondition(result, conditionSettings);

                //ожидается: все отчеты не заполненны, сообщения есть
                Assert.IsTrue(result.Count != 0);
            }
            #endregion
        }

        //Проверка логики определения наличия незаполненных отчетов 5
        [TestMethod]
        public void CheckHasAlertsReport5FilledDataTest()
        {

            var year = DateTime.Now.Year;

            var dbMock = new AnalyzeMockDB();

            //заполнение репозитория
            dbMock.Set5Data(year, null);//заполнение отчетов за текущий год
            dbMock.Set5Data(year - 1, 4);//прошлый год 4й квартал

            //список правил тестирования

            
            //Массив ролей учавствующих в заполнении отчетов
            var reportRoles = new[]
            {DOKIP_ROLE_TYPE.OARRS_worker};

            //класс с логикой проверки заполненности отчета
            var conditionCheck = new PensionplacementCondition();

            //запрос настроек правила проверки отчета с БД
            var conditionSettings = dbMock.GetCondition(typeof(PensionplacementCondition).Name);

            //Переназначение DB2Connector на mock репозиторий
            ServiceSystem.GetDataServiceDelegate = () => dbMock.Connector;

            //Проверка каждой роли на заполненных отчетах
            foreach (var role in reportRoles)
            {
                conditionCheck.CurrentUserRoles = new List<DOKIP_ROLE_TYPE> { role };

                //список сообщений о незаполненных отчетах
                var result = new List<LoginMessageItem>();

                conditionCheck.CheckCondition(result, conditionSettings);

                //ожидается: все отчеты заполненны, сообщений нет
                Assert.IsTrue(result.Count == 0);
            }

            #region тестирование незаполненных отчетов:

            dbMock.IsReturnEmptyList = true;
            dbMock.Set5Data(year, null);
            dbMock.Set5Data(year - 1, 4);

            //Проверка каждой роли на незаполненных отчетах
            foreach (var role in reportRoles)
            {
                conditionCheck.CurrentUserRoles = new List<DOKIP_ROLE_TYPE> { role };

                //список сообщений о незаполненных отчетах
                var result = new List<LoginMessageItem>();

                conditionCheck.CheckCondition(result, conditionSettings);

                //ожидается: все отчеты не заполненны, сообщения есть
                Assert.IsTrue(result.Count != 0);
            }
            #endregion
        }

        //Проверка логики определения наличия незаполненных отчетов 6
        [TestMethod]
        public void CheckHasAlertsReport6FilledDataTest()
        {

            var year = DateTime.Now.Year;

            var dbMock = new AnalyzeMockDB();

            //заполнение репозитория
            dbMock.Set6Data(year, null);//заполнение отчетов за текущий год
            dbMock.Set6Data(year - 1, 4);//прошлый год 4й квартал

            //список правил тестирования


            //Массив ролей учавствующих в заполнении отчетов
            var reportRoles = new[]
            {DOKIP_ROLE_TYPE.OSRP_worker};

            //класс с логикой проверки заполненности отчета
            var conditionCheck = new IncomingstatementsCondition();

            //запрос настроек правила проверки отчета с БД
            var conditionSettings = dbMock.GetCondition(typeof(IncomingstatementsCondition).Name);

            //Переназначение DB2Connector на mock репозиторий
            ServiceSystem.GetDataServiceDelegate = () => dbMock.Connector;

            //Проверка каждой роли на заполненных отчетах
            foreach (var role in reportRoles)
            {
                conditionCheck.CurrentUserRoles = new List<DOKIP_ROLE_TYPE> { role };

                //список сообщений о незаполненных отчетах
                var result = new List<LoginMessageItem>();

                conditionCheck.CheckCondition(result, conditionSettings);

                //ожидается: все отчеты заполненны, сообщений нет
                Assert.IsTrue(result.Count == 0);
            }

            #region тестирование незаполненных отчетов:

            dbMock.IsReturnEmptyList = true;
            dbMock.Set6Data(year, null);
            dbMock.Set6Data(year - 1, 4);

            //Проверка каждой роли на незаполненных отчетах
            foreach (var role in reportRoles)
            {
                conditionCheck.CurrentUserRoles = new List<DOKIP_ROLE_TYPE> { role };

                //список сообщений о незаполненных отчетах
                var result = new List<LoginMessageItem>();

                conditionCheck.CheckCondition(result, conditionSettings);

                //ожидается: все отчеты не заполненны, сообщения есть
                Assert.IsTrue(result.Count != 0);
            }
            #endregion
        }

        //Проверка логики определения наличия незаполненных отчетов 7
        [TestMethod]
        public void CheckHasAlertsReport7FilledDataTest()
        {

            var year = DateTime.Now.Year;

            var dbMock = new AnalyzeMockDB();

            //заполнение репозитория
            dbMock.Set7Data(year, null);//заполнение отчетов за текущий год
            dbMock.Set7Data(year - 1, 4);//прошлый год 4й квартал

            //список правил тестирования


            //Массив ролей учавствующих в заполнении отчетов
            var reportRoles = new[]
            {DOKIP_ROLE_TYPE.OSRP_worker, DOKIP_ROLE_TYPE.OUFV_worker };

            //класс с логикой проверки заполненности отчета
            var conditionCheck = new PaymentyearrateCondition();

            //запрос настроек правила проверки отчета с БД
            var conditionSettings = dbMock.GetCondition(typeof(PaymentyearrateCondition).Name);

            //Переназначение DB2Connector на mock репозиторий
            ServiceSystem.GetDataServiceDelegate = () => dbMock.Connector;

            //Проверка каждой роли на заполненных отчетах
            foreach (var role in reportRoles)
            {
                conditionCheck.CurrentUserRoles = new List<DOKIP_ROLE_TYPE> { role };

                //список сообщений о незаполненных отчетах
                var result = new List<LoginMessageItem>();

                conditionCheck.CheckCondition(result, conditionSettings);

                //ожидается: все отчеты заполненны, сообщений нет
                Assert.IsTrue(result.Count == 0);
            }

            #region тестирование незаполненных отчетов:

            dbMock.IsReturnEmptyList = true;
            dbMock.Set7Data(year, null);
            dbMock.Set7Data(year - 1, 4);

            //Проверка каждой роли на незаполненных отчетах
            foreach (var role in reportRoles)
            {
                conditionCheck.CurrentUserRoles = new List<DOKIP_ROLE_TYPE> { role };

                //список сообщений о незаполненных отчетах
                var result = new List<LoginMessageItem>();

                conditionCheck.CheckCondition(result, conditionSettings);

                //ожидается: все отчеты не заполненны, сообщения есть
                Assert.IsTrue(result.Count != 0);
            }
            #endregion
        }

        //Проверка логики определения даты начала показа оповещения для годового отчета
        [TestMethod]
        public void AlertConditionStartDateYearTest()
        {
            var conditionCollection = new AnalyzeConditionCollection();
            var dbMock = new AnalyzeMockDB();

            //заполнение репозитория
            //запрос настроек правила проверки отчета с БД AnalyzeCondition
            var conditionSettings = dbMock.GetCondition(typeof(PaymentyearrateCondition).Name);
            
            conditionSettings.AlertSign = "+";
            conditionSettings.AlertForDays = 3;
            conditionSettings.RepeatInterval = "Q";
            var date = new DateTime(2017,1,1);
            
            Assert.IsTrue(conditionCollection[nameof(PaymentyearrateCondition)].CheckAlertYearDate(ref date, conditionSettings));
        }

        //Проверка логики определения даты начала показа оповещения для квартального отчета
        [TestMethod]
        public void AlertConditionStartDateQuarterTest()
        {
            var conditionCollection = new AnalyzeConditionCollection();
            var dbMock = new AnalyzeMockDB();

            //заполнение репозитория
            //запрос настроек правила проверки отчета с БД
            var conditionSettings = dbMock.GetCondition(typeof(PaymentyearrateCondition).Name);
            conditionSettings.AlertSign = "+";
            conditionSettings.AlertForDays = 3;
            conditionSettings.RepeatInterval = "Q";
        }

        #endregion

        #region don't use

        //[TestMethod]
        //public void TestDraftViewModel()
        //{
        //    List<Finregister> lst = DataContainerFacade.GetList<Finregister>();
        //    if (!lst.Any())
        //        Assert.Inconclusive();
        //    PerformanceChecker chk = new PerformanceChecker();
        //    DraftViewModel model = new DraftViewModel(lst.First().ID);
        //    TestPublicProperty(model);
        //    model.CanExecuteSave();
        //    chk.CheckPerformance();
        //}
        //[TestMethod]
        //public void TestF24ViewModel()
        //{
        //    List<EdoOdkF024> lst = DataContainerFacade.GetList<EdoOdkF024>();
        //    if (!lst.Any())
        //        Assert.Inconclusive();
        //    PerformanceChecker chk = new PerformanceChecker();
        //    F24ViewModel model = new F24ViewModel(lst.First().ID);
        //    TestPublicProperty(model);
        //    model.CanExecuteSave();
        //    chk.CheckPerformance();
        //}

        #endregion

    }

}
