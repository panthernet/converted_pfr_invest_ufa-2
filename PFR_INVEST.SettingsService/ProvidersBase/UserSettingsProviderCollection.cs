using System.Configuration.Provider;

namespace PFR_INVEST.SettingsService.ProvidersBase
{
    public class UserSettingsProviderCollection : ProviderCollection
    {
        public new UserSettingsProviderBase this[string name]
        {
            get { return base[name] as UserSettingsProviderBase; }
        }

        public override void Add(ProviderBase provider)
        {
            if (provider is UserSettingsProviderBase)
                base.Add(provider);
        }
    }
}