﻿using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class ContrInvestOBLHib : ContrInvestOBL
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual ContrInvestHib ContrInvest { get; set; }
    }
}
