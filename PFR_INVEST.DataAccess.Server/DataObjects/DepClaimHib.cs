﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataObjects;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class DepClaimHib : DepClaim
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual PortfolioHib Portfolio { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<DepositHib> Deposits { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual PfrBankAccountHib PfrBankAccount { get; set; }
    }
}
