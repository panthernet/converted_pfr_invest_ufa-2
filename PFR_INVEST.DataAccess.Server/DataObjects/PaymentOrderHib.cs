﻿using PFR_INVEST.DataObjects;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
	public class PaymentOrderHib : PaymentOrder
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual LegalEntityHib Bank { get; set; }
    }
}
