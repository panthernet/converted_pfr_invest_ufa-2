﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataObjects;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
	[HibData]
	public class KDocHib:KDoc
	{
		[IgnoreDataMember]
		[HibExtensionDataProperty]
		public virtual PortfolioHib Portfolio { get; set; }


		[IgnoreDataMember]
		[HibExtensionDataProperty]
		public virtual PortfolioPFRBankAccountHib PortfolioPFRBankAccount { get; set; }

		
		
	}
}
