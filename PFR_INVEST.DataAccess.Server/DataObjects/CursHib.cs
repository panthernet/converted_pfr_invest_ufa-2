﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class CursHib : Curs
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual CurrencyHib Currency { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<IncomesecHib> Incomesecs { get; set; }
    }
}
