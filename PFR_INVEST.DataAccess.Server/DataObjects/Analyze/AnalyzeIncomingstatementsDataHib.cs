﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using PFR_INVEST.DataObjects.Analyze;

namespace PFR_INVEST.DataAccess.Server.DataObjects.Analyze
{
    [HibData]
    public class AnalyzeIncomingstatementsDataHib : AnalyzeIncomingstatementsData
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual FilingType Filingtype { get; set; }
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual AnalyzeIncomingstatementsReport AnalyzeIncomingstatementsReport { get; set; }
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual StatementType Statementtype { get; set; }
    }
}
