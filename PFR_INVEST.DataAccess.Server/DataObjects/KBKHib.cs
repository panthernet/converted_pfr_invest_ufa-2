﻿using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;
using System.Collections.Generic;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class KBKHib : KBK
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<KBKHistory> KBKHistory { get; set; }
    }
}
