﻿using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class IncomesecHib : Incomesec
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual CursHib Curs { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual PortfolioHib Portfolio { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual PfrBankAccountHib PfrBankAccount { get; set; }
    }
}
