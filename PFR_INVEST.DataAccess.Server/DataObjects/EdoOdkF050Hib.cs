﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataObjects;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
	[HibData]
	public class EdoOdkF050Hib : EdoOdkF050
	{
        //[IgnoreDataMember]
		[HibExtensionDataProperty]
		public virtual IList<F050CBInfo> F050CBInfos { get; set; }


		[IgnoreDataMember]
		[HibExtensionDataProperty]
		public virtual ContractHib Contract { get; set; }
	}
}
