﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class LegalEntityHib : LegalEntity
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual ContragentHib Contragent { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<OldNPFNameHib> OldNPFNames { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<LicenseHib> Licensies { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<ContractHib> Contracts { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<ContactHib> Contacts { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<PfrBankAccountHib> PfrBankAccounts { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual LegalStatusHib LegalStatus { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<BankAccountHib> BankAccounts { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<AddSPNHib> AddSPNs { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<LegalEntityHeadHib> Heads { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<LegalEntityChiefHib> Chiefs { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<DepClaim2Hib> DepClaims2 { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<LegalEntityCourier> Couriers { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<LegalEntityRating> MultiplierRatings { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<LegalEntityIdentifier> Identifiers { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<BankStockCode> BankStockCodes { get; set; }
    }
}
