﻿using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class OrdReportHib : OrdReport
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual CbInOrderHib CbInOrder { get; set; }
    }
}
