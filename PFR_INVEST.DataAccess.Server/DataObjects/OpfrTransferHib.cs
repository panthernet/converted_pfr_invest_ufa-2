﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class OpfrTransferHib : OpfrTransfer
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual OpfrCost Cost { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual PFRBranch Branch { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual OpfrRegisterHib Register { get; set; }


    }
}
