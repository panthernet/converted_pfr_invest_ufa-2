﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class ContrInvestHib : ContrInvest
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual ContractHib Contract { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<ContrInvestAKC> ContrInvestAKCList { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<ContrInvestOBL> ContrInvestOBLList { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<ContrInvestPAY> ContrInvestPAYList { get; set; }
    }
}
