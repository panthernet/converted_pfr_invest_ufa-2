﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class OwnedFoundsHib : OwnedFounds
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual LegalEntity LegalEntity { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual Year Year { get; set; }
    }
}
