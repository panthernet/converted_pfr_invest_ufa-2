﻿using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class DepositHib : Deposit
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual DepClaimOfferHib DepClaimOffer { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual Element Comment { get; set; }


        /// <summary>
        /// Portfolio Name
        /// </summary>
        [IgnoreDataMember]
        public virtual string Year { get; set; }

    }
}
