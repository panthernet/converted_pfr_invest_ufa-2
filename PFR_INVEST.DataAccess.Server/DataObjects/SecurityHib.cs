﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataObjects;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
	[HibData]
	public class SecurityHib : Security
	{
		[IgnoreDataMember]
		[HibExtensionDataProperty]
		public virtual IList<Payment> Payments { get; set; }

		[IgnoreDataMember]
		[HibExtensionDataProperty]
		public virtual IList<MarketPrice> MarketPrices { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual CurrencyHib Currency { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual SecurityKind Kind { get; set; }
	}
}
