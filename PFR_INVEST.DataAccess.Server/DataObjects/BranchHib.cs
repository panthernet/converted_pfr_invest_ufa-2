﻿using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class BranchHib : Branch
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual LegalEntityHib LegalEntity { get; set; }
    }
}
