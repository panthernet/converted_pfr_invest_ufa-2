﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class EdoOdkF012Hib : EdoOdkF012
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual ContractHib Contract { get; set; }
    }
}
