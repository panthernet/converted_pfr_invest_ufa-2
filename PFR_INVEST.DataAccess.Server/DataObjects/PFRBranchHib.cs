﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class PFRBranchHib : PFRBranch
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual FEDO FEDO { get; set; }
    }
}
