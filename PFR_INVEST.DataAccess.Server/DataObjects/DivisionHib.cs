﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class DivisionHib : Division
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<PersonHib> Persons { get; set; }

        [IgnoreDataMember]
        public virtual StatusHib Status { get; set; }
    }
}
