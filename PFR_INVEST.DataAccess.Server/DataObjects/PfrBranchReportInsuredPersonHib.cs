﻿using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class PfrBranchReportInsuredPersonHib : PfrBranchReportInsuredPerson
    {
        [IgnoreDataMember]
        public virtual PfrBranchReportInsuredPersonTypeHib Type { get; set; }

        [IgnoreDataMember]
        public virtual PfrBranchReportHib Report { get; set; }
    }
}
