﻿using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class DocFileBodyHib : DocFileBody
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        DocumentHib Document { get; set; }
    }
}
