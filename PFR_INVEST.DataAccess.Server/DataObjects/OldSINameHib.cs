﻿using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class OldSINameHib : OldSIName
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual LegalEntityHib LegalEntity { get; set; }
    }
}
