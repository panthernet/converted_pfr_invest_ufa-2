﻿using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class F401402UKHib : F401402UK
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual EdoOdkF401402 Document { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual ContractHib Contract { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual F401402UKStatus FStatus { get; set; }

    }
}
