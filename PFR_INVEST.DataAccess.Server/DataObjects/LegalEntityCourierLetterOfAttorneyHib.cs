﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class LegalEntityCourierLetterOfAttorneyHib : LegalEntityCourierLetterOfAttorney
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<LegalEntityCourierHib> LegalEntityCouriers { get; set; }
    }
}
