using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Threading;

namespace PFR_INVEST.DataAccess.Server
{
    public class ThreadCacheManager
    {
        private const string ThreadCacheDataSlotName = "ThreadCacheManagerThreadCacheData";        

        private static ThreadCacheManager m_oInstance = null;        
        public static ThreadCacheManager Instance
        {
            get {
                lock (typeof(ThreadCacheManager))
                {
                    if (m_oInstance == null)
                        m_oInstance = new ThreadCacheManager();
                    return m_oInstance;
                }
            }
        }        

        public static int DefaultMaxCachesCapacity = 100;
        
        internal ThreadCacheManager()
        {
            Create();
        }

        private void Create()
        {
            
        }

        public ThreadCache GetCurrentThreadCache()
        {
            LocalDataStoreSlot oSlot = GetCurrentThreadDataSlot();
            ThreadCache oCache = (ThreadCache)(Thread.GetData(oSlot));
            if (oCache == null)
            {
                oCache = new ThreadCache();
                Thread.SetData(oSlot, oCache);
            }

            return oCache;
        }
        
        private LocalDataStoreSlot GetCurrentThreadDataSlot()
        {
            LocalDataStoreSlot oCache = Thread.GetNamedDataSlot(ThreadCacheDataSlotName);

            if (oCache != null)
                return oCache;

            oCache = Thread.AllocateNamedDataSlot(ThreadCacheDataSlotName);
            return oCache;
        }

        public void FreeCurrentThreadCache()
        {
            GetCurrentThreadCache().Clear ();
            LocalDataStoreSlot oSlot = GetCurrentThreadDataSlot();
            if(oSlot != null)
                Thread.FreeNamedDataSlot(ThreadCacheDataSlotName);
        }
    }

    public class ThreadCache
    {
        /// <summary>
        /// First level contains dictionarties by name.
        /// Second level contains ThreadCacheSlots by Key
        /// </summary>
        private ThreadCacheDicsConainer m_aNamedDics;

        class ThreadCacheDicsConainer : Dictionary<string, Dictionary<object, ThreadCacheSlot>>
        {            
            private int m_iRefsCount = 0;

            public void AddRef()
            {
                lock(this)
                    m_iRefsCount++;
            }

            public void RemoveRef()
            {
                lock (this)
                {
                    m_iRefsCount--;
                    //Free memory
                    if (m_iRefsCount == 0 )
                        if(this.Count > 0)
                            this.Clear();
                }

            }
                    
        }


        private const string ThreadCacheNamedDicsContainer = "ThreadCacheManagerNamedDicsContainer";

        private object m_oSyncRoot = new object();
        public object SyncRoot
        {
            get {
                return m_oSyncRoot;
            }
        }

        internal ThreadCache()
        {
            ReInit();
        }

        private void ReInit()
        {
            lock (SyncRoot)
            {
                if (m_aNamedDics != null)
                    m_aNamedDics.RemoveRef();
                m_aNamedDics = new ThreadCacheDicsConainer();
                m_aNamedDics.AddRef();
            }
        }

        private Dictionary<object, ThreadCacheSlot> GetNamedCacheDic(string p_sDicName)
        {
            Dictionary<object, ThreadCacheSlot> oDic = null;
            
            if (m_aNamedDics.ContainsKey(p_sDicName))
            {
                oDic = (m_aNamedDics[p_sDicName]);
            }

            if (oDic == null)
            {
                oDic = new Dictionary<object, ThreadCacheSlot>();
                m_aNamedDics[p_sDicName] = oDic;
            }

            return oDic;
        }

        public void ClearNamedCacheDic(string p_sDicName)
        {
            Dictionary<object, ThreadCacheSlot> oDic = GetNamedCacheDic(p_sDicName);
            oDic.Clear();
        }

        private static ThreadCacheSlot GetCacheSlot(Dictionary<object, ThreadCacheSlot> p_oDic, object p_sKey)
        {
            ThreadCacheSlot oSlot = null;
            if (p_oDic.ContainsKey(p_sKey))
            {
                oSlot = p_oDic[p_sKey];
            }

            if (oSlot == null)
            {
                oSlot = new ThreadCacheSlot(null);
                p_oDic[p_sKey] = oSlot;
            }


            return oSlot;
         }

        public ThreadCacheSlot GetCacheSlot(string p_sDicName, object p_sKey)
        {
            lock (SyncRoot)
            {
                Dictionary<object, ThreadCacheSlot> oDic = GetNamedCacheDic(p_sDicName);
                return GetCacheSlot(oDic, p_sKey);
            }
        }        

        public ThreadCacheSlot GetCacheSlot(object p_sKey)
        {
            return GetCacheSlot(ThreadCacheNamedDicsContainer, p_sKey);
        }

        public void Clear()
        {
            //create new dictionary instance
            //Do not touch old dictionary. It could be joined with another thread
            ReInit();
        }

        public void JoinCache(ThreadCache p_oAnotherThreadCache)
        {
            lock (SyncRoot)
            {
                m_aNamedDics = p_oAnotherThreadCache.m_aNamedDics;
                m_aNamedDics.AddRef();
            }
        }
    }

    [DataContract]
    public class ThreadCacheSlot
    {
        [DataMember]
        private object m_oValue = null;
        [DataMember]
        private bool m_bAlreadyRequested = false;

        public object Value
        {
            get {
                return m_oValue;    
            }

            set {
                m_oValue = value;
            }
        }

        public bool AlreadyRequested
        {
            get {
                return m_bAlreadyRequested;
            }
            set {
                m_bAlreadyRequested = value;
            }
        }

        [DataMember]
        private object m_oSyncRoot = new object();
        public object SyncRoot
        {
            get {
                return m_oSyncRoot;
            }
        }

        public ThreadCacheSlot(object p_oValue)
        {
            m_oValue = p_oValue;
        }
    }
}
