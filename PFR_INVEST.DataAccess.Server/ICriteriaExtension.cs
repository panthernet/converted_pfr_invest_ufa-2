﻿using System.Runtime.Serialization;
using NHibernate.Engine;
using NHibernate.Impl;
using NHibernate.Loader;
using NHibernate.Loader.Criteria;
using NHibernate.Persister.Entity;

namespace NHibernate
{
    [DataContract]
    public static class ICriteriaExtension
    {
        public static string ToSql(this ICriteria criteria)
        {
            //var c = (CriteriaImpl)((CriteriaImpl.Subcriteria)criteria).Parent;
            //var s = (SessionImpl)c.Session;
            //var factory = (ISessionFactoryImplementor)s.SessionFactory;
            //string[] implementors = factory.GetImplementors(c.EntityOrClassName);
            //var loader = new CriteriaLoader(
            //    (IOuterJoinLoadable)factory.GetEntityPersister(implementors[0]),
            //    factory,
            //    c,
            //    implementors[0],
            //    s.EnabledFilters);

            //return ((OuterJoinLoader)loader).SqlString.ToString();





            var c = ((NHibernate.Impl.CriteriaImpl)(criteria));

            var s = c.Session;
            var factory = (ISessionFactoryImplementor)(((NHibernate.Impl.SessionImpl)(s)).SessionFactory);
            string[] implementors = factory.GetImplementors(c.EntityOrClassName);
            var loader = new CriteriaLoader(
                (IOuterJoinLoadable)factory.GetEntityPersister(implementors[0]),
                factory,
                c,
                implementors[0],
                s.EnabledFilters);

            return ((OuterJoinLoader)loader).SqlString.ToString();

            
        }
    }
}
