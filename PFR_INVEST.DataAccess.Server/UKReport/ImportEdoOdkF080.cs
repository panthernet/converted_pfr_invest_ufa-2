﻿
namespace PFR_INVEST.DataAccess.Server.UKReport
{
    using PFR_INVEST.DataAccess.Server.DataObjects;
    using PFR_INVEST.DataObjects;

    public class ImportEdoOdkF080 : ImportEdoOdkBase
    {
        public EdoOdkF080Hib F080;
        public Contract contract;
    }
}
