﻿
namespace PFR_INVEST.DataAccess.Server.UKReport
{
    using PFR_INVEST.DataAccess.Server.DataObjects;
    using PFR_INVEST.DataObjects;
    using System;

    public abstract class ImportEdoOdkBase : IUKReportImportItem
    {
        public string xmlBody;
        public EDOLog edoLog;

        public string FileName { get; set; }

        public string PathName { get; set; }

        public string ReportOnDate { get; set; }

        public DateTime? ReportOnDateNative { get; set; }

    }
}
