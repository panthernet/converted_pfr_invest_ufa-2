﻿using DXCommonThemes;

namespace DevExpress.Xpf.Xmas.LogoProvider
{
    public class XmasLogoProvider: IDXLogoProvider
    {
        public string LogoUri => @"/DevExpress.Xpf.Themes.Xmas.v13.1;component/Images/snowflake2.png";
    }
}
