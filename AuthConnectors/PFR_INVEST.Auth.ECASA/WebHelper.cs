﻿using System.IO;
using System.Net;

namespace PFR_INVEST.Auth.ECASA
{
    public static class WebHelper
    {
        public static string HttpGet(string url, string userGuid)
        {
            string result;

            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Headers.Add("OAM_REMOTE_USER", userGuid);
            request.AutomaticDecompression = DecompressionMethods.GZip;

            using (var response = (HttpWebResponse)request.GetResponse())
            using (var stream = response.GetResponseStream())
            using (var reader = new StreamReader(stream))
            {
                result = reader.ReadToEnd();
            }
            return result;
        }
    }
}
