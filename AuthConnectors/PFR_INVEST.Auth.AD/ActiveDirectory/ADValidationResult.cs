using System.ComponentModel;

namespace PFR_INVEST.Auth.AD.ActiveDirectory
{
    public enum ADValidationResult
    {
        [Description("����������� ������ ��")]
        GeneralError = 0,
        [Description("�������")]
        Success = 1,
        [Description("������������ ���������� ������� ������")]
        UserMustChangePassword = 773,
        [Description("������������ �� ������")]
        UserNotFound = 525,
        [Description("�������� �����/������")]
        InvalidCredentials = 2,
        [Description("���� ��������")]
        NotPermittedLogonThisTime = 530,
        [Description("�������� ���� � ���� ������� �������")]
        NotPermittedLogonAtThisWorkstation = 531,
        [Description("������ �����")]
        PasswordExpired = 532,
        [Description("������� ������������ ���������������")]
        AccountDisabled = 533,
        [Description("���� �������� �������� �����")]
        AccountExpired = 701,
        [Description("������� ������������")]
        AccountLocked = 775,
    }
}