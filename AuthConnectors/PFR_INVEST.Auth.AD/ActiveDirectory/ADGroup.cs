﻿using System;
using System.DirectoryServices;
using System.Runtime.Serialization;
using PFR_INVEST.Auth.AD.ActiveDirectory.Extensions;

namespace PFR_INVEST.Auth.AD.ActiveDirectory
{
    [DataContract(Namespace = ActiveDirectorySettingsHelper.ServiceNamespace)]
    public class ADGroup
    {
        [DataMember]
        public Guid Guid { get; set; }
        [DataMember]
        public string SamName { get; set; }
        [DataMember]
        public string CN { get; set; }
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string EMail { get; set; }
        [DataMember]
        public string Scope { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public long PrimaryGroupToken { get; set; }
        [DataMember]
        public string DN { get; set; }

        public static DirectorySearcher FillDirectorySearcher(DirectorySearcher directorySearcher)
        {
            directorySearcher.PropertiesToLoad.Add("objectGUID");
            directorySearcher.PropertiesToLoad.Add("sAMAccountName");
            directorySearcher.PropertiesToLoad.Add("cn");
            directorySearcher.PropertiesToLoad.Add("name");

            directorySearcher.PropertiesToLoad.Add("mail");
            directorySearcher.PropertiesToLoad.Add("groupScope");
            directorySearcher.PropertiesToLoad.Add("groupType");
            directorySearcher.PropertiesToLoad.Add("description");
            directorySearcher.PropertiesToLoad.Add("primaryGroupToken");
            directorySearcher.PropertiesToLoad.Add("distinguishedName");

            return directorySearcher;
        }

        public static ADGroup CreateFromEntry(DirectoryEntry groupEntry)
        {
            return new ADGroup
                       {
                           Guid = new Guid((byte[]) groupEntry.Properties["objectGuid"].Value),
                           SamName = groupEntry.GetStringValue("sAMAccountName"),
                           CN = groupEntry.GetStringValue("cn"),
                           EMail = groupEntry.GetStringValue("mail"),
                           Scope = groupEntry.GetStringValue("groupScope"),
                           Type = groupEntry.GetStringValue("groupType"),
                           Description = groupEntry.GetStringValue("descriprion"),
                           PrimaryGroupToken = Convert.ToInt64(groupEntry.Properties["primaryGroupToken"].Value),
                           DN = groupEntry.GetStringValue("distinguishedName")
                       };
        }
    }
}