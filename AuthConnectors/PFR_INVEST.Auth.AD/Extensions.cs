﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.AD.ActiveDirectory;
using PFR_INVEST.Auth.SharedData.Auth;

namespace PFR_INVEST.Auth.AD
{
    public static class Extensions
    {

        public static User MapFromAdUser(ADUser user)
        {
            return new User
            {
                Name = string.Join(" ", new[] { user.LastName, user.FirstName, user.Initials }),
                Login = user.SamName,
            };
        }

        public static List<User> MapFromAdUsers(IEnumerable<ADUser> users)
        {
            return users.Select(MapFromAdUser).ToList();
        }


        public static List<Role> MapFromAdGroups(IEnumerable<ADGroup> groups)
        {
            return groups.Select(g => new Role
            {
                Name = g.SamName,
                Description = g.Description
            }).ToList();
        }
    }
}
