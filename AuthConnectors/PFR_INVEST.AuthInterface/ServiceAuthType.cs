﻿namespace PFR_INVEST.Auth.ServerData
{
    public enum ServiceAuthType
    {
        /// <summary>
        /// Подключение к АД
        /// </summary>
        AD,
        /// <summary>
        /// Подключение к ЕЦАСА в номральном режиме
        /// </summary>
        ECASA,
        /// <summary>
        /// Подключение к эмулятору ролей, для отладки
        /// </summary>
        MOCK,
        /// <summary>
        /// Подключение к ЕЦАСА в облегченном режиме (без авторизации в самой ЕЦАСА)
        /// </summary>
        QUICKECASA
    }
}
