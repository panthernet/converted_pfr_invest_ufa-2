﻿using System.ServiceModel;
using db2connector;

namespace PFR_INVEST.Auth.ClientData
{
    public class AuthServiceClient : ClientBase<IDB2Connector>
    {
        internal AuthServiceClient()
        {
        }

        internal AuthServiceClient(string endpointConfigurationName)
            : base(endpointConfigurationName)
        {
        }

        public IDB2Connector GetChannel()
        {
            return Channel;
        }
    }
}
