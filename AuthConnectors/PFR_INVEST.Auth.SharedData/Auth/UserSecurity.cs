﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace PFR_INVEST.Auth.SharedData.Auth
{
    /// <summary>
    /// Битовость НЕ актуальна, много ролей и все равно юзаем списки
    /// </summary>
    [DataContract]   
    // ReSharper disable once InconsistentNaming
    public enum DOKIP_ROLE_TYPE
    {
        OUFV_viewer = 14,
        OUFV_worker = 524288,
        OUFV_directory_editor = 262144,
        OUFV_manager = 131072,

        OSRP_viewer = 13,
        OSRP_worker = 65536,
        OSRP_directory_editor = 32768,
        OSRP_manager = 16384,

        OKIP_viewer = 9,
        OKIP_worker = 8192,
        OKIP_directory_editor = 4096,
        OKIP_manager = 2048,

        OFPR_viewer = 10,
        OFPR_worker = 1024,
        OFPR_directory_editor = 512,
        OFPR_manager = 256,

        OARRS_viewer = 11,
        OARRS_worker = 1 * 1048576,
        OARRS_directory_editor = 128,
        OARRS_manager = 64,

        OVSI_viewer = 12,
        OVSI_worker = 32,
        OVSI_directory_editor = 16,
        OVSI_manager = 8,

        Administrator = 4,
        User = 2,
        None = 1
    }

    public class UserSecurity
    {
        private readonly List<DOKIP_ROLE_TYPE> _roles = new List<DOKIP_ROLE_TYPE>();

        public static string GetRoleRussianName(DOKIP_ROLE_TYPE type)
        {
            switch (type)
            {
                case DOKIP_ROLE_TYPE.OSRP_viewer:
                    return "Обозреватель ОСРП";
                case DOKIP_ROLE_TYPE.OSRP_worker:
                    return "Сотрудник ОСРП";
                case DOKIP_ROLE_TYPE.OSRP_directory_editor:
                    return "Редактор справочников ОСРП";
                case DOKIP_ROLE_TYPE.OSRP_manager:
                    return "Руководитель ОСРП";

                case DOKIP_ROLE_TYPE.OKIP_viewer:
                    return "Обозреватель ОКИП";
                case DOKIP_ROLE_TYPE.OKIP_worker:
                    return "Сотрудник ОКИП";
                case DOKIP_ROLE_TYPE.OKIP_directory_editor:
                    return "Редактор справочников ОКИП";
                case DOKIP_ROLE_TYPE.OKIP_manager:
                    return "Руководитель ОКИП";

                case DOKIP_ROLE_TYPE.OFPR_viewer:
                    return "Обозреватель ОФПР";
                case DOKIP_ROLE_TYPE.OFPR_worker:
                    return "Сотрудник ОФПР";
                case DOKIP_ROLE_TYPE.OFPR_directory_editor:
                    return "Редактор справочников ОФПР";
                case DOKIP_ROLE_TYPE.OFPR_manager:
                    return "Руководитель ОФПР";

                case DOKIP_ROLE_TYPE.OARRS_viewer:
                    return "Обозреватель ОАРиРС";
                case DOKIP_ROLE_TYPE.OARRS_worker:
                    return "Сотрудник ОАРиРС";
                case DOKIP_ROLE_TYPE.OARRS_directory_editor:
                    return "Редактор справочников ОАРиРС";
                case DOKIP_ROLE_TYPE.OARRS_manager:
                    return "Руководитель ОАРиРС";

                case DOKIP_ROLE_TYPE.OVSI_viewer:
                    return "Обозреватель ОВСИ";
                case DOKIP_ROLE_TYPE.OVSI_worker:
                    return "Сотрудник ОВСИ";
                case DOKIP_ROLE_TYPE.OVSI_directory_editor:
                    return "Редактор справочников ОВСИ";
                case DOKIP_ROLE_TYPE.OVSI_manager:
                    return "Руководитель ОВСИ";

                case DOKIP_ROLE_TYPE.OUFV_viewer:
                    return "Обозреватель ОУиФВ";
                case DOKIP_ROLE_TYPE.OUFV_worker:
                    return "Сотрудник ОУиФВ";
                case DOKIP_ROLE_TYPE.OUFV_directory_editor:
                    return "Редактор справочников ОУиФВ";
                case DOKIP_ROLE_TYPE.OUFV_manager:
                    return "Руководитель ОУиФВ";

                case DOKIP_ROLE_TYPE.None:
                    return "Пользователь Active Directory";
                case DOKIP_ROLE_TYPE.User:
                    return "Пользователь ПТК ДОКИП";
                case DOKIP_ROLE_TYPE.Administrator:
                    return "Администратор ПТК ДОКИП";
                default:
                    return string.Empty;
            }
        }

        public static Role CreatePtkRole(DOKIP_ROLE_TYPE type)
        {
            return new Role(GetRoleADName(type));
        }

        public static string GetRoleADName(DOKIP_ROLE_TYPE type)
        {
            switch (type)
            {
                case DOKIP_ROLE_TYPE.OSRP_viewer:
                    return "PTK_DOKIP_OSRP_VIEWERS";
                case DOKIP_ROLE_TYPE.OSRP_worker:
                    return "PTK_DOKIP_OSRP_WORKERS";
                case DOKIP_ROLE_TYPE.OSRP_directory_editor:
                    return "PTK_DOKIP_OSRP_DIRECTORY_EDITORS";
                case DOKIP_ROLE_TYPE.OSRP_manager:
                    return "PTK_DOKIP_OSRP_MANAGERS";

                case DOKIP_ROLE_TYPE.OKIP_viewer:
                    return "PTK_DOKIP_OKIP_VIEWERS";
                case DOKIP_ROLE_TYPE.OKIP_worker:
                    return "PTK_DOKIP_OKIP_WORKERS";
                case DOKIP_ROLE_TYPE.OKIP_directory_editor:
                    return "PTK_DOKIP_OKIP_DIRECTORY_EDITORS";
                case DOKIP_ROLE_TYPE.OKIP_manager:
                    return "PTK_DOKIP_OKIP_MANAGERS";

                case DOKIP_ROLE_TYPE.OFPR_viewer:
                    return "PTK_DOKIP_OFPR_VIEWERS";
                case DOKIP_ROLE_TYPE.OFPR_worker:
                    return "PTK_DOKIP_OFPR_WORKERS";
                case DOKIP_ROLE_TYPE.OFPR_directory_editor:
                    return "PTK_DOKIP_OFPR_DIRECTORY_EDITORS";
                case DOKIP_ROLE_TYPE.OFPR_manager:
                    return "PTK_DOKIP_OFPR_MANAGERS";

                case DOKIP_ROLE_TYPE.OARRS_viewer:
                    return "PTK_DOKIP_OARRS_VIEWERS";
                case DOKIP_ROLE_TYPE.OARRS_worker:
                    return "PTK_DOKIP_OARRS_WORKERS";
                case DOKIP_ROLE_TYPE.OARRS_directory_editor:
                    return "PTK_DOKIP_OARRS_DIRECTORY_EDITORS";
                case DOKIP_ROLE_TYPE.OARRS_manager:
                    return "PTK_DOKIP_OARRS_MANAGERS";

                case DOKIP_ROLE_TYPE.OVSI_viewer:
                    return "PTK_DOKIP_OVSI_VIEWERS";
                case DOKIP_ROLE_TYPE.OVSI_worker:
                    return "PTK_DOKIP_OVSI_WORKERS";
                case DOKIP_ROLE_TYPE.OVSI_directory_editor:
                    return "PTK_DOKIP_OVSI_DIRECTORY_EDITORS";
                case DOKIP_ROLE_TYPE.OVSI_manager:
                    return "PTK_DOKIP_OVSI_MANAGERS";

                case DOKIP_ROLE_TYPE.OUFV_viewer:
                    return "PTK_DOKIP_OUFV_VIEWERS";
                case DOKIP_ROLE_TYPE.OUFV_worker:
                    return "PTK_DOKIP_OUFV_WORKERS";
                case DOKIP_ROLE_TYPE.OUFV_directory_editor:
                    return "PTK_DOKIP_OUFV_DIRECTORY_EDITORS";
                case DOKIP_ROLE_TYPE.OUFV_manager:
                    return "PTK_DOKIP_OUFV_MANAGERS";

                case DOKIP_ROLE_TYPE.None:
                    return "NONE";
                case DOKIP_ROLE_TYPE.User:
                    return "PTK_DOKIP_USERS";
                case DOKIP_ROLE_TYPE.Administrator:
                    return "PTK_DOKIP_ADMINISTRATORS";
                default:
                    return string.Empty;
            }
        }

        public static DOKIP_ROLE_TYPE GetRoleByADName(string name)
        {
            switch (name.ToUpper())
            {
                case "PTK_DOKIP_OSRP_VIEWERS":
                    return DOKIP_ROLE_TYPE.OSRP_viewer;
                case "PTK_DOKIP_OSRP_WORKERS":
                    return DOKIP_ROLE_TYPE.OSRP_worker;
                case "PTK_DOKIP_OSRP_DIRECTORY_EDITORS":
                    return DOKIP_ROLE_TYPE.OSRP_directory_editor;
                case "PTK_DOKIP_OSRP_MANAGERS":
                    return DOKIP_ROLE_TYPE.OSRP_manager;

                case "PTK_DOKIP_OKIP_VIEWERS":
                    return DOKIP_ROLE_TYPE.OKIP_viewer;
                case "PTK_DOKIP_OKIP_WORKERS":
                    return DOKIP_ROLE_TYPE.OKIP_worker;
                case "PTK_DOKIP_OKIP_DIRECTORY_EDITORS":
                    return DOKIP_ROLE_TYPE.OKIP_directory_editor;
                case "PTK_DOKIP_OKIP_MANAGERS":
                    return DOKIP_ROLE_TYPE.OKIP_manager;

                case "PTK_DOKIP_OFPR_VIEWERS":
                    return DOKIP_ROLE_TYPE.OFPR_viewer;
                case "PTK_DOKIP_OFPR_WORKERS":
                    return DOKIP_ROLE_TYPE.OFPR_worker;
                case "PTK_DOKIP_OFPR_DIRECTORY_EDITORS":
                    return DOKIP_ROLE_TYPE.OFPR_directory_editor;
                case "PTK_DOKIP_OFPR_MANAGERS":
                    return DOKIP_ROLE_TYPE.OFPR_manager;

                case "PTK_DOKIP_OARRS_VIEWERS":
                    return DOKIP_ROLE_TYPE.OARRS_viewer;
                case "PTK_DOKIP_OARRS_WORKERS":
                    return DOKIP_ROLE_TYPE.OARRS_worker;
                case "PTK_DOKIP_OARRS_DIRECTORY_EDITORS":
                    return DOKIP_ROLE_TYPE.OARRS_directory_editor;
                case "PTK_DOKIP_OARRS_MANAGERS":
                    return DOKIP_ROLE_TYPE.OARRS_manager;

                case "PTK_DOKIP_OVSI_VIEWERS":
                    return DOKIP_ROLE_TYPE.OVSI_viewer;
                case "PTK_DOKIP_OVSI_WORKERS":
                    return DOKIP_ROLE_TYPE.OVSI_worker;
                case "PTK_DOKIP_OVSI_DIRECTORY_EDITORS":
                    return DOKIP_ROLE_TYPE.OVSI_directory_editor;
                case "PTK_DOKIP_OVSI_MANAGERS":
                    return DOKIP_ROLE_TYPE.OVSI_manager;

                case "PTK_DOKIP_OUFV_VIEWERS":
                    return DOKIP_ROLE_TYPE.OUFV_viewer;
                case "PTK_DOKIP_OUFV_WORKERS":
                    return DOKIP_ROLE_TYPE.OUFV_worker;
                case "PTK_DOKIP_OUFV_DIRECTORY_EDITORS":
                    return DOKIP_ROLE_TYPE.OUFV_directory_editor;
                case "PTK_DOKIP_OUFV_MANAGERS":
                    return DOKIP_ROLE_TYPE.OUFV_manager;

                case "PTK_DOKIP_USERS":
                    return DOKIP_ROLE_TYPE.User;
                case "PTK_DOKIP_ADMINISTRATORS":
                    return DOKIP_ROLE_TYPE.Administrator;
                default:
                    return DOKIP_ROLE_TYPE.None;
            }
        }

        /// <summary>
        /// Имеет ли юзер с ролью VIEWER доступ к объекту с указанным набором прав (roles)
        /// </summary>
        /// <param name="roles">набор прав объекта</param>
        public bool CheckViewerHasAccess(List<DOKIP_ROLE_TYPE> roles)
        {
            var userRoles =_roles;
            var vRoles = userRoles.Where(a => a.ToString().ToLower().EndsWith("viewer")).ToList();
            if (vRoles.Any())
            {
                foreach (var vRole in vRoles)
                {
                    var vRoleDep = vRole.ToString().Split('_')[0];
                    foreach (var fRole in roles)
                    {
                        var fRoleDep = fRole.ToString().Split('_')[0];
                        if (vRoleDep == fRoleDep)
                            return true;
                    }
                }
            }
            return false;
        }

		public bool CheckUserInAnyRole(params DOKIP_ROLE_TYPE[] roles)
		{
		    return roles.Any(CheckUserInRole);
		}

        public bool CheckUserInRole(DOKIP_ROLE_TYPE role)
        {
            return _roles.Any(r => r == role);
        }

        public List<DOKIP_ROLE_TYPE> GetRoles()
        {
            return _roles;
        }

        public void AddRole(DOKIP_ROLE_TYPE role)
        {
            if (!CheckUserInRole(role))
                _roles.Add(role);
        }

        public void DeleteRole(DOKIP_ROLE_TYPE role)
        {
            _roles.Remove(role);
        }

        public void ClearRoles()
        {
            _roles.Clear();
        }

        public bool IsPureViewer()
        {
            return GetRoles()
                .Where(a => a != DOKIP_ROLE_TYPE.None && a != DOKIP_ROLE_TYPE.User).All(a => a.ToString().ToLower().EndsWith("viewer"));
        }
    }
}