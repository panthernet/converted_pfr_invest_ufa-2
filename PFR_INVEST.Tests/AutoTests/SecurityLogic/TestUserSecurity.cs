﻿using PFR_INVEST.SecurityLogic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PFR_INVEST.Tests.AutoTests.SecurityLogic
{


    [TestClass()]
    public class TestUserSecurity : BaseTest
    {


        [TestMethod()]
        public void TestUserSecurityConstructor()
        {
            UserSecurity target = new UserSecurity();
        }

        [TestMethod()]
        public void TestAddRole()
        {
            UserSecurity target = new UserSecurity(); 
            DOKIP_ROLE_TYPE role = DOKIP_ROLE_TYPE.OVSI_worker; 
            target.AddRole(role);
        }

        [TestMethod()]
        public void TestCheckUserInRole()
        {
            UserSecurity target = new UserSecurity(); 
            DOKIP_ROLE_TYPE role = DOKIP_ROLE_TYPE.OVSI_worker; 
            bool expected = false; 
            bool actual = target.CheckUserInRole(role);
            
        }

        [TestMethod()]
        public void TestClearRoles()
        {
            UserSecurity target = new UserSecurity(); 
            target.ClearRoles();
        }

        [TestMethod()]
        public void TestDeleteRole()
        {
            UserSecurity target = new UserSecurity(); 
            DOKIP_ROLE_TYPE role = new DOKIP_ROLE_TYPE(); 
            target.DeleteRole(role);
        }

        [TestMethod()]
        public void TestGetRoleADName()
        {
            DOKIP_ROLE_TYPE type = DOKIP_ROLE_TYPE.OFPR_worker; 
            string expected = "PTK_DOKIP_OFPR_WORKERS";
            string actual = UserSecurity.GetRoleADName(type);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestGetRoleByADName()
        {
            string name = "PTK_DOKIP_OFPR_WORKERS"; 
            DOKIP_ROLE_TYPE expected = DOKIP_ROLE_TYPE.OFPR_worker; 
            DOKIP_ROLE_TYPE actual;
            actual = UserSecurity.GetRoleByADName(name);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestGetRoleRussianName()
        {
            DOKIP_ROLE_TYPE type = new DOKIP_ROLE_TYPE(); 
            string expected = string.Empty; 
            string actual;
            actual = UserSecurity.GetRoleRussianName(type);
           // Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestWorkWithoutAD()
        {
            bool actual;
            actual = UserSecurity.WorkWithoutAD;
        }
    }
}
