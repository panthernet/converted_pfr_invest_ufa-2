﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestF40InfoListItem : BaseTest
    {


        [TestMethod()]
        public void TestF40InfoListItemConstructor()
        {
            string name = string.Empty; 
            string broker = string.Empty; 
            string bir = string.Empty; 
            int buyCount = default(int); 
            string buySum = string.Empty;
			int sellCount = default(int); 
            string sellSum = string.Empty; 
            PFR_INVEST.ViewModels.ListItems.F40InfoListItem target = new PFR_INVEST.ViewModels.ListItems.F40InfoListItem(name, broker, bir, buyCount, buySum, sellCount, sellSum);
        }

        [TestMethod()]
        public void TestBir()
        {
            string name = string.Empty; 
            string broker = string.Empty; 
            string bir = string.Empty;
			int buyCount = default(int); 
            string buySum = string.Empty;
			int sellCount = default(int); 
            string sellSum = string.Empty; 
            PFR_INVEST.ViewModels.ListItems.F40InfoListItem target = new PFR_INVEST.ViewModels.ListItems.F40InfoListItem(name, broker, bir, buyCount, buySum, sellCount, sellSum); 
            string expected = string.Empty; 
            string actual;
            target.Bir = expected;
            actual = target.Bir;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestBroker()
        {
            string name = string.Empty; 
            string broker = string.Empty; 
            string bir = string.Empty;
			int buyCount = default(int); 
            string buySum = string.Empty;
			int sellCount = default(int); 
            string sellSum = string.Empty; 
            PFR_INVEST.ViewModels.ListItems.F40InfoListItem target = new PFR_INVEST.ViewModels.ListItems.F40InfoListItem(name, broker, bir, buyCount, buySum, sellCount, sellSum); 
            string expected = string.Empty; 
            string actual;
            target.Broker = expected;
            actual = target.Broker;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestBuyCount()
        {
            string name = string.Empty; 
            string broker = string.Empty; 
            string bir = string.Empty;
			int buyCount = default(int); 
            string buySum = string.Empty;
			int sellCount = default(int); 
            string sellSum = string.Empty; 
            PFR_INVEST.ViewModels.ListItems.F40InfoListItem target = new PFR_INVEST.ViewModels.ListItems.F40InfoListItem(name, broker, bir, buyCount, buySum, sellCount, sellSum);
			int expected = default(int); 
            int actual;
            target.BuyCount = expected;
            actual = target.BuyCount;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestBuySum()
        {
            string name = string.Empty; 
            string broker = string.Empty; 
            string bir = string.Empty;
			int buyCount = default(int); 
            string buySum = string.Empty;
			int sellCount = default(int); 
            string sellSum = string.Empty; 
            PFR_INVEST.ViewModels.ListItems.F40InfoListItem target = new PFR_INVEST.ViewModels.ListItems.F40InfoListItem(name, broker, bir, buyCount, buySum, sellCount, sellSum); 
            string expected = string.Empty; 
            string actual;
            target.BuySum = expected;
            actual = target.BuySum;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestName()
        {
            string name = string.Empty; 
            string broker = string.Empty; 
            string bir = string.Empty;
			int buyCount = default(int); 
            string buySum = string.Empty;
			int sellCount = default(int); 
            string sellSum = string.Empty; 
            PFR_INVEST.ViewModels.ListItems.F40InfoListItem target = new PFR_INVEST.ViewModels.ListItems.F40InfoListItem(name, broker, bir, buyCount, buySum, sellCount, sellSum); 
            string expected = string.Empty; 
            string actual;
            target.Name = expected;
            actual = target.Name;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSellCount()
        {
            string name = string.Empty; 
            string broker = string.Empty; 
            string bir = string.Empty;
			int buyCount = default(int); 
            string buySum = string.Empty;
			int sellCount = default(int); 
            string sellSum = string.Empty; 
            PFR_INVEST.ViewModels.ListItems.F40InfoListItem target = new PFR_INVEST.ViewModels.ListItems.F40InfoListItem(name, broker, bir, buyCount, buySum, sellCount, sellSum);
			int expected = default(int); 
            int actual;
            target.SellCount = expected;
            actual = target.SellCount;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSellSum()
        {
            string name = string.Empty; 
            string broker = string.Empty; 
            string bir = string.Empty;
			int buyCount = default(int); 
            string buySum = string.Empty;
			int sellCount = default(int); 
            string sellSum = string.Empty; 
            PFR_INVEST.ViewModels.ListItems.F40InfoListItem target = new PFR_INVEST.ViewModels.ListItems.F40InfoListItem(name, broker, bir, buyCount, buySum, sellCount, sellSum); 
            string expected = string.Empty; 
            string actual;
            target.SellSum = expected;
            actual = target.SellSum;
            Assert.AreEqual(expected, actual);
        }
    }
}
