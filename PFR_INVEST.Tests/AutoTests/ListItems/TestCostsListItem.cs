﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestCostsListItem : BaseTest
    {

        [TestMethod()]
        public void TestCostsListItemConstructor()
        {
            DBEntity ent = null;
            Decimal rate = new Decimal();
            CostsListItem target = new CostsListItem(ent, rate);
        }

        [TestMethod()]
        public void TestCostsListItemConstructor1()
        {
            DBEntity ent = null;
            CostsListItem target = new CostsListItem(ent);
        }

        [TestMethod()]
        public void TestCurrSumm()
        {
            DBEntity ent = null;
            CostsListItem target = new CostsListItem(ent);
            string actual;
            actual = target.CurrSumm;
        }

        [TestMethod()]
        public void TestCurrency()
        {
            DBEntity ent = null;
            CostsListItem target = new CostsListItem(ent);
            string actual;
            actual = target.Currency;
        }

        [TestMethod()]
        public void TestDate()
        {
            DBEntity ent = null;
            CostsListItem target = new CostsListItem(ent);
            string actual;
            actual = target.Date;
        }

        [TestMethod()]
        public void TestID()
        {
            DBEntity ent = null;
            CostsListItem target = new CostsListItem(ent);
            long actual;
            actual = target.ID;
        }

        [TestMethod()]
        public void TestKind()
        {
            DBEntity ent = null;
            CostsListItem target = new CostsListItem(ent);
            string actual;
            actual = target.Kind;
        }

        [TestMethod()]
        public void TestMonth()
        {
            DBEntity ent = null;
            CostsListItem target = new CostsListItem(ent);
            string actual;
            actual = target.Month;
        }

        [TestMethod()]
        public void TestPortfolio()
        {
            DBEntity ent = null;
            CostsListItem target = new CostsListItem(ent);
            string actual;
            actual = target.Portfolio;
        }

        [TestMethod()]
        public void TestQuarter()
        {
            DBEntity ent = null;
            CostsListItem target = new CostsListItem(ent);
            string actual;
            actual = target.Quarter;
        }

        [TestMethod()]
        public void TestRate()
        {
            DBEntity ent = null;
            CostsListItem target = new CostsListItem(ent);
            Decimal expected = new Decimal();
            Decimal actual;
            target.Rate = expected;
            actual = target.Rate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRegNum()
        {
            DBEntity ent = null;
            CostsListItem target = new CostsListItem(ent);
            string actual;
            actual = target.RegNum;
        }

        [TestMethod()]
        public void TestRubSumm()
        {
            DBEntity ent = null;
            CostsListItem target = new CostsListItem(ent);
            string actual;
            actual = target.RubSumm;
        }
    }
}
