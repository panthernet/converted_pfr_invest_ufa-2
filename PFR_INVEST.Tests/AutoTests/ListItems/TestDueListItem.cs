﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestDueListItem : BaseTest
    {
        [TestMethod()]
        public void TestDueListItemConstructor()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.DUE_TYPE type = new PFR_INVEST.ViewModels.DUE_TYPE();
            PFR_INVEST.ViewModels.ListItems.DueListItem target = new PFR_INVEST.ViewModels.ListItems.DueListItem(ent, type);
        }

        [TestMethod()]
        public void TestDate()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.DUE_TYPE type = new PFR_INVEST.ViewModels.DUE_TYPE();
            PFR_INVEST.ViewModels.ListItems.DueListItem target = new PFR_INVEST.ViewModels.ListItems.DueListItem(ent, type);
            string actual;
            actual = target.Date;
        }

        [TestMethod()]
        public void TestGoalPortfolio()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.DUE_TYPE type = new PFR_INVEST.ViewModels.DUE_TYPE();
            PFR_INVEST.ViewModels.ListItems.DueListItem target = new PFR_INVEST.ViewModels.ListItems.DueListItem(ent, type);
            string actual;
            actual = target.GoalPortfolio;
        }

        [TestMethod()]
        public void TestID()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.DUE_TYPE type = new PFR_INVEST.ViewModels.DUE_TYPE();
            PFR_INVEST.ViewModels.ListItems.DueListItem target = new PFR_INVEST.ViewModels.ListItems.DueListItem(ent, type);
            long actual;
            actual = target.ID;
        }

        [TestMethod()]
        public void TestMSumm()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.DUE_TYPE type = new PFR_INVEST.ViewModels.DUE_TYPE();
            PFR_INVEST.ViewModels.ListItems.DueListItem target = new PFR_INVEST.ViewModels.ListItems.DueListItem(ent, type);
            decimal actual;
            actual = target.MSumm;
        }

        [TestMethod()]
        public void TestMonth()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.DUE_TYPE type = new PFR_INVEST.ViewModels.DUE_TYPE();
            PFR_INVEST.ViewModels.ListItems.DueListItem target = new PFR_INVEST.ViewModels.ListItems.DueListItem(ent, type);
            string actual;
            actual = target.Month;
        }

        [TestMethod()]
        public void TestNegativePortfolioName()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.DUE_TYPE type = new PFR_INVEST.ViewModels.DUE_TYPE();
            PFR_INVEST.ViewModels.ListItems.DueListItem target = new PFR_INVEST.ViewModels.ListItems.DueListItem(ent, type);
            string expected = string.Empty;
            string actual;
            target.NegativePortfolioName = expected;
            actual = target.NegativePortfolioName;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestOperation()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.DUE_TYPE type = new PFR_INVEST.ViewModels.DUE_TYPE();
            PFR_INVEST.ViewModels.ListItems.DueListItem target = new PFR_INVEST.ViewModels.ListItems.DueListItem(ent, type);
            string actual;
            actual = target.Operation;
        }

        [TestMethod()]
        public void TestPortfolio()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.DUE_TYPE type = new PFR_INVEST.ViewModels.DUE_TYPE();
            PFR_INVEST.ViewModels.ListItems.DueListItem target = new PFR_INVEST.ViewModels.ListItems.DueListItem(ent, type);
            string actual;
            actual = target.Portfolio;
        }

        [TestMethod()]
        public void TestPrecision()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.DUE_TYPE type = new PFR_INVEST.ViewModels.DUE_TYPE();
            PFR_INVEST.ViewModels.ListItems.DueListItem target = new PFR_INVEST.ViewModels.ListItems.DueListItem(ent, type);
            string actual;
            actual = target.Precision;
        }

        [TestMethod()]
        public void TestQuarter()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.DUE_TYPE type = new PFR_INVEST.ViewModels.DUE_TYPE();
            PFR_INVEST.ViewModels.ListItems.DueListItem target = new PFR_INVEST.ViewModels.ListItems.DueListItem(ent, type);
            string actual;
            actual = target.Quarter;
        }

        [TestMethod()]
        public void TestSource()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.DUE_TYPE type = new PFR_INVEST.ViewModels.DUE_TYPE();
            PFR_INVEST.ViewModels.ListItems.DueListItem target = new PFR_INVEST.ViewModels.ListItems.DueListItem(ent, type);
            string actual;
            actual = target.Source;
        }

        [TestMethod()]
        public void TestSum()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.DUE_TYPE type = new PFR_INVEST.ViewModels.DUE_TYPE();
            PFR_INVEST.ViewModels.ListItems.DueListItem target = new PFR_INVEST.ViewModels.ListItems.DueListItem(ent, type);
            string actual;
            actual = target.Sum;
        }

        [TestMethod()]
        public void TestSupply()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.DUE_TYPE type = new PFR_INVEST.ViewModels.DUE_TYPE();
            PFR_INVEST.ViewModels.ListItems.DueListItem target = new PFR_INVEST.ViewModels.ListItems.DueListItem(ent, type);
            string expected = string.Empty;
            string actual;
            target.Supply = expected;
            actual = target.Supply;
            Assert.AreEqual(expected, actual);
        }
    }
}
