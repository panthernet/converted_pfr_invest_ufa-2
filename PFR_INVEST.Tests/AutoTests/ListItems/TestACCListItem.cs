﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestACCListItem : BaseTest
    {



        [TestMethod()]
        public void TestACCListItemConstructor()
        {
            DBEntity ent = null; // TODO: Initialize to an appropriate value
            Decimal rate = new Decimal(); // TODO: Initialize to an appropriate value
            ACCListItem target = new ACCListItem(ent, rate);
        }

        [TestMethod()]
        public void TestACCListItemConstructor1()
        {
            DBEntity ent = null; // TODO: Initialize to an appropriate value
            ACCListItem target = new ACCListItem(ent);
        }

        [TestMethod()]
        public void TestAccountNum()
        {
            DBEntity ent = null; // TODO: Initialize to an appropriate value
            ACCListItem target = new ACCListItem(ent); // TODO: Initialize to an appropriate value
            string actual;
            actual = target.AccountNum;
        }

        [TestMethod()]
        public void TestAccountType()
        {
            DBEntity ent = null; // TODO: Initialize to an appropriate value
            ACCListItem target = new ACCListItem(ent); // TODO: Initialize to an appropriate value
            string actual;
            actual = target.AccountType;
        }

        [TestMethod()]
        public void TestCurrSumm()
        {
            DBEntity ent = null; // TODO: Initialize to an appropriate value
            ACCListItem target = new ACCListItem(ent); // TODO: Initialize to an appropriate value
            string actual;
            actual = target.CurrSumm;
        }

        [TestMethod()]
        public void TestCurrency()
        {
            DBEntity ent = null; // TODO: Initialize to an appropriate value
            ACCListItem target = new ACCListItem(ent); // TODO: Initialize to an appropriate value
            string actual;
            actual = target.Currency;
        }

        [TestMethod()]
        public void TestDOCKIND()
        {
            DBEntity ent = null; // TODO: Initialize to an appropriate value
            ACCListItem target = new ACCListItem(ent); // TODO: Initialize to an appropriate value
            long actual;
            actual = target.DOCKIND;
        }

        [TestMethod()]
        public void TestDate()
        {
            DBEntity ent = null; // TODO: Initialize to an appropriate value
            ACCListItem target = new ACCListItem(ent); // TODO: Initialize to an appropriate value
            string actual;
            actual = target.Date;
        }

        [TestMethod()]
        public void TestID()
        {
            DBEntity ent = null; // TODO: Initialize to an appropriate value
            ACCListItem target = new ACCListItem(ent); // TODO: Initialize to an appropriate value
            long actual;
            actual = target.ID;
        }

        [TestMethod()]
        public void TestMonth()
        {
            DBEntity ent = null; // TODO: Initialize to an appropriate value
            ACCListItem target = new ACCListItem(ent); // TODO: Initialize to an appropriate value
            string actual;
            actual = target.Month;
        }

        [TestMethod()]
        public void TestOperation()
        {
            DBEntity ent = null; // TODO: Initialize to an appropriate value
            ACCListItem target = new ACCListItem(ent); // TODO: Initialize to an appropriate value
            string actual;
            actual = target.Operation;
        }

        [TestMethod()]
        public void TestPortfolio()
        {
            DBEntity ent = null; // TODO: Initialize to an appropriate value
            ACCListItem target = new ACCListItem(ent); // TODO: Initialize to an appropriate value
            string actual;
            actual = target.Portfolio;
        }

        [TestMethod()]
        public void TestQuarter()
        {
            DBEntity ent = null; // TODO: Initialize to an appropriate value
            ACCListItem target = new ACCListItem(ent); // TODO: Initialize to an appropriate value
            string actual;
            actual = target.Quarter;
        }

        [TestMethod()]
        public void TestRate()
        {
            DBEntity ent = null; // TODO: Initialize to an appropriate value
            ACCListItem target = new ACCListItem(ent); // TODO: Initialize to an appropriate value
            Decimal expected = new Decimal(); // TODO: Initialize to an appropriate value
            Decimal actual;
            target.Rate = expected;
            actual = target.Rate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRubSumm()
        {
            DBEntity ent = null; // TODO: Initialize to an appropriate value
            ACCListItem target = new ACCListItem(ent); // TODO: Initialize to an appropriate value
            string actual;
            actual = target.RubSumm;
        }

        [TestMethod()]
        public void TestSource()
        {
            DBEntity ent = null; // TODO: Initialize to an appropriate value
            ACCListItem target = new ACCListItem(ent); // TODO: Initialize to an appropriate value
            string actual;
            actual = target.Source;
        }
    }
}
