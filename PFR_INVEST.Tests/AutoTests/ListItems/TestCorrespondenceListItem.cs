﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestCorrespondenceListItem : BaseTest
    {
        [TestMethod()]
        public void TestCorrespondenceListItemConstructor()
        {
            DBEntity ent = null;
            CorrespondenceListItem target = new CorrespondenceListItem(ent);
        }

        [TestMethod()]
        public void TestContrDate()
        {
            DBEntity ent = null;
            CorrespondenceListItem target = new CorrespondenceListItem(ent);
            string actual;
            actual = target.ContrDate;
        }

        [TestMethod()]
        public void TestDocClassif()
        {
            DBEntity ent = null;
            CorrespondenceListItem target = new CorrespondenceListItem(ent);
            string actual;
            actual = target.DocClassif;
        }

        [TestMethod()]
        public void TestExecDate()
        {
            DBEntity ent = null;
            CorrespondenceListItem target = new CorrespondenceListItem(ent);
            string actual;
            actual = target.ExecDate;
        }

        [TestMethod()]
        public void TestExecutor()
        {
            DBEntity ent = null;
            CorrespondenceListItem target = new CorrespondenceListItem(ent);
            string actual;
            actual = target.Executor;
        }

        [TestMethod()]
        public void TestExtra()
        {
            DBEntity ent = null;
            CorrespondenceListItem target = new CorrespondenceListItem(ent);
            string actual;
            actual = target.Extra;
        }

        [TestMethod()]
        public void TestID()
        {
            DBEntity ent = null;
            CorrespondenceListItem target = new CorrespondenceListItem(ent);
            long actual;
            actual = target.ID;
        }

        [TestMethod()]
        public void TestInDate()
        {
            DBEntity ent = null;
            CorrespondenceListItem target = new CorrespondenceListItem(ent);
            string actual;
            actual = target.InDate;
        }

        [TestMethod()]
        public void TestInNum()
        {
            DBEntity ent = null;
            CorrespondenceListItem target = new CorrespondenceListItem(ent);
            string actual;
            actual = target.InNum;
        }

        [TestMethod()]
        public void TestMonth()
        {
            DBEntity ent = null;
            CorrespondenceListItem target = new CorrespondenceListItem(ent);
            string actual;
            actual = target.Month;
        }

        [TestMethod()]
        public void TestOutNum()
        {
            DBEntity ent = null;
            CorrespondenceListItem target = new CorrespondenceListItem(ent);
            string actual;
            actual = target.OutNum;
        }

        [TestMethod()]
        public void TestSendDate()
        {
            DBEntity ent = null;
            CorrespondenceListItem target = new CorrespondenceListItem(ent);
            string actual;
            actual = target.SendDate;
        }

        [TestMethod()]
        public void TestSender()
        {
            DBEntity ent = null;
            CorrespondenceListItem target = new CorrespondenceListItem(ent);
            string actual;
            actual = target.Sender;
        }

        [TestMethod()]
        public void TestYear()
        {
            DBEntity ent = null;
            CorrespondenceListItem target = new CorrespondenceListItem(ent);
            string actual;
            actual = target.Year;
        }
    }
}
