﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestViolationListItem : BaseTest
    {



        [TestMethod()]
        public void TestViolationListItemConstructor()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.ViolationListItem target = new PFR_INVEST.ViewModels.ListItems.ViolationListItem(ent);
        }

        [TestMethod()]
        public void TestBaseDocDate()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.ViolationListItem target = new PFR_INVEST.ViewModels.ListItems.ViolationListItem(ent); 
            string actual;
            actual = target.BaseDocDate;
        }

        [TestMethod()]
        public void TestContractNum()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.ViolationListItem target = new PFR_INVEST.ViewModels.ListItems.ViolationListItem(ent); 
            string actual;
            actual = target.ContractNum;
        }

        [TestMethod()]
        public void TestID()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.ViolationListItem target = new PFR_INVEST.ViewModels.ListItems.ViolationListItem(ent); 
            long actual;
            actual = target.ID;
        }

        [TestMethod()]
        public void TestInFirm1()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.ViolationListItem target = new PFR_INVEST.ViewModels.ListItems.ViolationListItem(ent); 
            string actual;
            actual = target.InFirm1;
        }

        [TestMethod()]
        public void TestInFirm2()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.ViolationListItem target = new PFR_INVEST.ViewModels.ListItems.ViolationListItem(ent); 
            string actual;
            actual = target.InFirm2;
        }

        [TestMethod()]
        public void TestRealStopDate()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.ViolationListItem target = new PFR_INVEST.ViewModels.ListItems.ViolationListItem(ent); 
            string actual;
            actual = target.RealStopDate;
        }

        [TestMethod()]
        public void TestState()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.ViolationListItem target = new PFR_INVEST.ViewModels.ListItems.ViolationListItem(ent); 
            string actual;
            actual = target.State;
        }

        [TestMethod()]
        public void TestStopDate()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.ViolationListItem target = new PFR_INVEST.ViewModels.ListItems.ViolationListItem(ent); 
            string actual;
            actual = target.StopDate;
        }

        [TestMethod()]
        public void TestStopDocNumber()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.ViolationListItem target = new PFR_INVEST.ViewModels.ListItems.ViolationListItem(ent); 
            string actual;
            actual = target.StopDocNumber;
        }

        [TestMethod()]
        public void TestUK()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.ViolationListItem target = new PFR_INVEST.ViewModels.ListItems.ViolationListItem(ent); 
            string actual;
            actual = target.UK;
        }
    }
}
