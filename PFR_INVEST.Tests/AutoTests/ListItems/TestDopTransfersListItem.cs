﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestDopTransfersListItem : BaseTest
    {


        [TestMethod()]
        public void TestDopTransfersListItemConstructor()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.DopTransfersListItem target = new PFR_INVEST.ViewModels.ListItems.DopTransfersListItem(dbEn);
        }

        [TestMethod()]
        public void TestDopTransfersListItemConstructor1()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            long num = 1;
            PFR_INVEST.ViewModels.ListItems.DopTransfersListItem target = new PFR_INVEST.ViewModels.ListItems.DopTransfersListItem(dbEn, num);
        }

        [TestMethod()]
        public void TestDopTransfersListItemConstructor2()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            long num = 0;
            decimal baseSum = new decimal();
            PFR_INVEST.ViewModels.ListItems.DopTransfersListItem target = new PFR_INVEST.ViewModels.ListItems.DopTransfersListItem(dbEn, num, baseSum);
        }

        [TestMethod()]
        public void TestBaseSum()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.DopTransfersListItem target = new PFR_INVEST.ViewModels.ListItems.DopTransfersListItem(dbEn);
            decimal expected = new decimal();
            decimal actual;
            target.BaseSum = expected;
            actual = target.BaseSum;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDate()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.DopTransfersListItem target = new PFR_INVEST.ViewModels.ListItems.DopTransfersListItem(dbEn);
            object actual;
            actual = target.Date;
        }

        [TestMethod()]
        public void TestDiff()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.DopTransfersListItem target = new PFR_INVEST.ViewModels.ListItems.DopTransfersListItem(dbEn);
            decimal actual;
            actual = target.Diff;
        }

        [TestMethod()]
        public void TestID()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.DopTransfersListItem target = new PFR_INVEST.ViewModels.ListItems.DopTransfersListItem(dbEn);
            long actual;
            actual = target.ID;
        }

        [TestMethod()]
        public void TestNUM()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.DopTransfersListItem target = new PFR_INVEST.ViewModels.ListItems.DopTransfersListItem(dbEn);
            long expected = 0;
            long actual;
            target.NUM = expected;
            actual = target.NUM;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSumm()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.DopTransfersListItem target = new PFR_INVEST.ViewModels.ListItems.DopTransfersListItem(dbEn);
            decimal actual;
            actual = target.Summ;
        }
    }
}
