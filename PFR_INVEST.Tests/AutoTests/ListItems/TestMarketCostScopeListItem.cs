﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestMarketCostScopeListItem : BaseTest
    {



        [TestMethod()]
        public void TestMarketCostScopeListItemConstructor()
        {
            PFR_INVEST.ViewModels.ListItems.MarketCostScopeListItem target = new PFR_INVEST.ViewModels.ListItems.MarketCostScopeListItem();
        }

        [TestMethod()]
        public void TestDate()
        {
            PFR_INVEST.ViewModels.ListItems.MarketCostScopeListItem target = new PFR_INVEST.ViewModels.ListItems.MarketCostScopeListItem();
            System.DateTime expected = new System.DateTime();
            System.DateTime actual;
            target.Date = expected;
            actual = target.Date;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestFormType()
        {
            PFR_INVEST.ViewModels.ListItems.MarketCostScopeListItem target = new PFR_INVEST.ViewModels.ListItems.MarketCostScopeListItem();
            int expected = 0;
            int actual;
            target.FormType = expected;
            actual = target.FormType;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestID()
        {
            PFR_INVEST.ViewModels.ListItems.MarketCostScopeListItem target = new PFR_INVEST.ViewModels.ListItems.MarketCostScopeListItem();
            long expected = 0;
            long actual;
            target.ID = expected;
            actual = target.ID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestMarketCost()
        {
            PFR_INVEST.ViewModels.ListItems.MarketCostScopeListItem target = new PFR_INVEST.ViewModels.ListItems.MarketCostScopeListItem();
            decimal expected = new decimal();
            decimal actual;
            target.MarketCost = expected;
            actual = target.MarketCost;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestYear()
        {
            PFR_INVEST.ViewModels.ListItems.MarketCostScopeListItem target = new PFR_INVEST.ViewModels.ListItems.MarketCostScopeListItem();
            int expected = 0;
            int actual;
            target.Year = expected;
            actual = target.Year;
            Assert.AreEqual(expected, actual);
        }
    }
}
