﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestBalanceListItem : BaseTest
    {



        [TestMethod()]
        public void TestBalanceListItemConstructor()
        {
            DBEntity ent = null; // TODO: Initialize to an appropriate value
            Decimal rate = new Decimal(); // TODO: Initialize to an appropriate value
            BalanceListItem target = new BalanceListItem(null);
        }

        [TestMethod()]
        public void TestBalanceListItemConstructor1()
        {
            DBEntity ent = null; // TODO: Initialize to an appropriate value
            BalanceListItem target = new BalanceListItem(null);
        }

        [TestMethod()]
        public void TestAccountNum()
        {
            DBEntity ent = null; // TODO: Initialize to an appropriate value
            BalanceListItem target = new BalanceListItem(null); // TODO: Initialize to an appropriate value
            string actual;
            actual = target.AccountNum;
        }

        [TestMethod()]
        public void TestAccountType()
        {
            DBEntity ent = null; // TODO: Initialize to null appropriate value
            BalanceListItem target = new BalanceListItem(null); // TODO: Initialize to an appropriate value
            string actual;
            actual = target.AccountType;
        }

        [TestMethod()]
        public void TestCurrency()
        {
            DBEntity ent = null; // TODO: Initialize to an appropriate value
            BalanceListItem target = new BalanceListItem(null); // TODO: Initialize to an appropriate value
            string actual;
            actual = target.Currency;
        }

        [TestMethod()]
        public void TestDate()
        {
            DBEntity ent = null; // TODO: Initialize to an appropriate value
            BalanceListItem target = new BalanceListItem(null); // TODO: Initialize to an appropriate value
            string actual;
            actual = target.Date;
        }

        [TestMethod()]
        public void TestFinish()
        {
            DBEntity ent = null; // TODO: Initialize to an appropriate value
            BalanceListItem target = new BalanceListItem(null); // TODO: Initialize to an appropriate value
            string actual;
            actual = target.Finish;
        }

        [TestMethod()]
        public void TestID()
        {
            DBEntity ent = null; // TODO: Initialize to an appropriate value
            BalanceListItem target = new BalanceListItem(null); // TODO: Initialize to an appropriate value
            long actual;
            actual = target.ID;
        }

        [TestMethod()]
        public void TestMinus()
        {
            DBEntity ent = null; // TODO: Initialize to an appropriate value
            BalanceListItem target = new BalanceListItem(null); // TODO: Initialize to an appropriate value
            string actual;
            actual = target.Minus;
        }

        [TestMethod()]
        public void TestMonth()
        {
            DBEntity ent = null; // TODO: Initialize to an appropriate value
            BalanceListItem target = new BalanceListItem(null); // TODO: Initialize to an appropriate value
            string actual;
            actual = target.Month;
        }

        [TestMethod()]
        public void TestOperation()
        {
            DBEntity ent = null; // TODO: Initialize to an appropriate value
            BalanceListItem target = new BalanceListItem(null); // TODO: Initialize to an appropriate value
            string actual;
            actual = target.Operation;
        }

        [TestMethod()]
        public void TestPlus()
        {
            DBEntity ent = null; // TODO: Initialize to an appropriate value
            BalanceListItem target = new BalanceListItem(null); // TODO: Initialize to an appropriate value
            string actual;
            actual = target.Plus;
        }

        [TestMethod()]
        public void TestPortfolio()
        {
            DBEntity ent = null; // TODO: Initialize to an appropriate value
            BalanceListItem target = new BalanceListItem(null); // TODO: Initialize to an appropriate value
            string actual;
            actual = target.Portfolio;
        }

        [TestMethod()]
        public void TestQuarter()
        {
            DBEntity ent = null; // TODO: Initialize to an appropriate value
            BalanceListItem target = new BalanceListItem(null); // TODO: Initialize to an appropriate value
            string actual;
            actual = target.Quarter;
        }


        [TestMethod()]
        public void TestStart()
        {
            DBEntity ent = null; // TODO: Initialize to an appropriate value
            BalanceListItem target = new BalanceListItem(null); // TODO: Initialize to an appropriate value
            string actual;
            actual = target.Start;
        }
    }
}
