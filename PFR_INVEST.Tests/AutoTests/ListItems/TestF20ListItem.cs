﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestF20ListItem : BaseTest
    {

        [TestMethod()]
        public void TestF20ListItemConstructor()
        {
            PFR_INVEST.Proxy.DBEntity entity = null; 
            string field = string.Empty; 
            string current = string.Empty; 
            string pie = string.Empty; 
            string start = string.Empty; 
            PFR_INVEST.ViewModels.ListItems.F20ListItem target = new PFR_INVEST.ViewModels.ListItems.F20ListItem(entity, field, current, pie, start);
        }

        [TestMethod()]
        public void TestCurrent()
        {
            PFR_INVEST.Proxy.DBEntity entity = null; 
            string field = string.Empty; 
            string current = string.Empty; 
            string pie = string.Empty; 
            string start = string.Empty; 
            PFR_INVEST.ViewModels.ListItems.F20ListItem target = new PFR_INVEST.ViewModels.ListItems.F20ListItem(entity, field, current, pie, start); 
            string expected = string.Empty; 
            string actual;
            target.Current = expected;
            actual = target.Current;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestField()
        {
            PFR_INVEST.Proxy.DBEntity entity = null; 
            string field = string.Empty; 
            string current = string.Empty; 
            string pie = string.Empty; 
            string start = string.Empty; 
            PFR_INVEST.ViewModels.ListItems.F20ListItem target = new PFR_INVEST.ViewModels.ListItems.F20ListItem(entity, field, current, pie, start); 
            string expected = string.Empty; 
            string actual;
            target.Field = expected;
            actual = target.Field;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPie()
        {
            PFR_INVEST.Proxy.DBEntity entity = null; 
            string field = string.Empty; 
            string current = string.Empty; 
            string pie = string.Empty; 
            string start = string.Empty; 
            PFR_INVEST.ViewModels.ListItems.F20ListItem target = new PFR_INVEST.ViewModels.ListItems.F20ListItem(entity, field, current, pie, start); 
            string expected = string.Empty; 
            string actual;
            target.Pie = expected;
            actual = target.Pie;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestStart()
        {
            PFR_INVEST.Proxy.DBEntity entity = null; 
            string field = string.Empty; 
            string current = string.Empty; 
            string pie = string.Empty; 
            string start = string.Empty; 
            PFR_INVEST.ViewModels.ListItems.F20ListItem target = new PFR_INVEST.ViewModels.ListItems.F20ListItem(entity, field, current, pie, start); 
            string expected = string.Empty; 
            string actual;
            target.Start = expected;
            actual = target.Start;
            Assert.AreEqual(expected, actual);
        }
    }
}
