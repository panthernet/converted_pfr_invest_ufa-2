﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ViewModels.ListItems;
using PFR_INVEST.Proxy;
using System.Collections.Generic;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestDopSPN_MViewModel : BaseTest
    {


        [TestMethod()]
        public void TestDopSPN_MViewModelConstructor()
        {
            DueViewModel dueVM = new DueViewModel();
            dueVM.Load(1);
            DopSPN_MViewModel target = new DopSPN_MViewModel(dueVM);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            DueViewModel dueVM = new DueViewModel();
            dueVM.Load(1);
            DopSPN_MViewModel target = new DopSPN_MViewModel(dueVM);
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteSelectAccount()
        {
            DueViewModel dueVM = new DueViewModel();
            dueVM.Load(1);
            DopSPN_MViewModel target = new DopSPN_MViewModel(dueVM);
            bool expected = true;
            bool actual;
            actual = target.CanExecuteSelectAccount();
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteSave()
        //{
        //    PrivateObject param0 = null;
        //    DopSPN_MViewModel_Accessor target = new DopSPN_MViewModel_Accessor(param0);
        //    target.ExecuteSave();
        //}

        
        [TestMethod()]
        public void TestLoad()
        {
            DueViewModel dueVM = new DueViewModel();
            dueVM.Load(1);
            DopSPN_MViewModel target = new DopSPN_MViewModel(dueVM);
            long lId = 1;
            target.Load(lId);
        }

        [TestMethod()]
        public void TestAccount()
        {
            DueViewModel dueVM = new DueViewModel();
            dueVM.Load(1);
            DopSPN_MViewModel target = new DopSPN_MViewModel(dueVM);
            PortfolioListItemFull expected = null;
            PortfolioListItemFull actual;
            target.Account = expected;
            actual = target.Account;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestChSumm()
        {
            DueViewModel dueVM = new DueViewModel();
            dueVM.Load(1);
            DopSPN_MViewModel target = new DopSPN_MViewModel(dueVM);
            decimal expected = 12;
            decimal actual;
            target.ChSumm = expected.ToString();
            actual = Convert.ToDecimal(target.ChSumm);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestItem()
        {
            DueViewModel dueVM = new DueViewModel();
            dueVM.Load(1);
            DopSPN_MViewModel target = new DopSPN_MViewModel(dueVM);
            string columnName = string.Empty;
            string actual;
            actual = target[columnName];
        }

        [TestMethod()]
        public void TestMSumm()
        {
            DueViewModel dueVM = new DueViewModel();
            dueVM.Load(1);
            DopSPN_MViewModel target = new DopSPN_MViewModel(dueVM);
            decimal expected = 0;
            decimal actual;
            target.MSumm = expected.ToString();
            actual = Convert.ToDecimal(target.MSumm);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestMonthsList()
        {
            DueViewModel dueVM = new DueViewModel();
            dueVM.Load(1);
            DopSPN_MViewModel target = new DopSPN_MViewModel(dueVM);
            List<DBEntity> expected = null;
            List<DBEntity> actual;
            target.MonthsList = expected;
            actual = target.MonthsList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestSelectAccount()
        {
            DueViewModel dueVM = new DueViewModel();
            dueVM.Load(1);
            DopSPN_MViewModel target = new DopSPN_MViewModel(dueVM);
            
            ICommand actual;
            actual = target.SelectAccount;
            
        }

        [TestMethod()]
        public void TestSelectedMonth()
        {
            DueViewModel dueVM = new DueViewModel();
            dueVM.Load(1);
            DopSPN_MViewModel target = new DopSPN_MViewModel(dueVM);
            DBEntity expected = null;
            DBEntity actual;
            target.SelectedMonth = expected;
            actual = target.SelectedMonth;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestType()
        {
            DueViewModel dueVM = new DueViewModel();
            dueVM.Load(1);
            DopSPN_MViewModel target = new DopSPN_MViewModel(dueVM);
            DUE_TYPE expected = new DUE_TYPE();
            DUE_TYPE actual;
            target.Type = expected;
            actual = target.Type;
            Assert.AreEqual(expected, actual);
        }
    }
}
