﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ViewModels.ListItems;
using System.Collections.Generic;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestViolationCategoriesViewModel : BaseTest
    {


        [TestMethod()]
        public void TestViolationCategoriesViewModelConstructor()
        {
            ViolationCategoriesViewModel target = new ViolationCategoriesViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefresh()
        {
            ViolationCategoriesViewModel_Accessor target = new ViolationCategoriesViewModel_Accessor();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteRefresh();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshList()
        {
            ViolationCategoriesViewModel_Accessor target = new ViolationCategoriesViewModel_Accessor();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteRefreshList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefresh()
        {
            ViolationCategoriesViewModel_Accessor target = new ViolationCategoriesViewModel_Accessor(); 
            target.ExecuteRefresh();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshList()
        {
            ViolationCategoriesViewModel_Accessor target = new ViolationCategoriesViewModel_Accessor(); 
            target.ExecuteRefreshList();
        }

        [TestMethod()]
        public void TestList()
        {
            ViolationCategoriesViewModel target = new ViolationCategoriesViewModel(); 
            List<ViolationCategoryListItem> expected = null; 
            List<ViolationCategoryListItem> actual;
            target.List = expected;
            actual = target.List;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefresh()
        {
            ViolationCategoriesViewModel_Accessor target = new ViolationCategoriesViewModel_Accessor(); 
            ICommand expected = null; 
            ICommand actual;
            target.Refresh = expected;
            actual = target.Refresh;
            Assert.AreEqual(expected, actual);
        }
    }
}
