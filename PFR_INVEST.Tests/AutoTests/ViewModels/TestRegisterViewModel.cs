﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;
using System.Collections.Generic;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestRegisterViewModel : BaseTest
    {


        [TestMethod()]
        public void TestRegisterViewModelConstructor()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            RegisterViewModel target = new RegisterViewModel(recordID, action);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            RegisterViewModel target = new RegisterViewModel(recordID, action);
            bool expected = true;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteSelectReg()
        {
            bool expected = true;
            bool actual;
            actual = RegisterViewModel_Accessor.CanExecuteSelectReg();
            Assert.AreEqual(expected, actual);
        }

       
        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestSafeConvertToDecimal()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            RegisterViewModel target = new RegisterViewModel(recordID, action);
            string value = string.Empty;
            Decimal expected = new Decimal();
            Decimal actual;
            actual = target.SafeConvertToDecimal(value);
            Assert.AreEqual(expected, actual);
        }

      

        [TestMethod()]
        public void TestAddedFinRegisters()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            RegisterViewModel target = new RegisterViewModel(recordID, action);
            List<DB2FinRegister> expected = null;
            List<DB2FinRegister> actual;
            target.AddedFinRegisters = expected;
            actual = target.AddedFinRegisters;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestContent()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            RegisterViewModel target = new RegisterViewModel(recordID, action);
            string expected = string.Empty;
            string actual;
            target.Content = expected;
            actual = target.Content;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestContents()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            RegisterViewModel target = new RegisterViewModel(recordID, action);
            List<string> expected = null;
            List<string> actual;
            target.Contents = expected;
            actual = target.Contents;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestERZL()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            RegisterViewModel target = new RegisterViewModel(recordID, action);
            string expected = string.Empty;
            string actual;
            target.ERZL = expected;
            actual = target.ERZL;
            Assert.AreEqual(expected, actual);
        }

      

        [TestMethod()]
        public void TestKind()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            RegisterViewModel target = new RegisterViewModel(recordID, action);
            string expected = string.Empty;
            string actual;
            target.Kind = expected;
            actual = target.Kind;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestKinds()
        {
            List<string> actual;
            actual = RegisterViewModel.Kinds;
        }

        [TestMethod()]
        public void TestPortfoliosList()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            RegisterViewModel target = new RegisterViewModel(recordID, action);
            List<DBEntity> expected = null;
            List<DBEntity> actual;
            target.PortfoliosList = expected;
            actual = target.PortfoliosList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestSelectReg()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;

            RegisterViewModel target = new RegisterViewModel(recordID, action);

            
            ICommand actual;
            
            actual = target.SelectReg;
            
        }

        [TestMethod()]
        public void TestSelectedPortfolio()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            
            RegisterViewModel target = new RegisterViewModel(recordID, action);
            DBEntity expected = null;
            DBEntity actual;
            target.SelectedPortfolio = expected;
            actual = target.SelectedPortfolio;
            //Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedSummSPN()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = new VIEWMODEL_STATES();
            RegisterViewModel target = new RegisterViewModel(recordID, action);
            DBEntity expected = null;
            DBEntity actual;
            target.SelectedSummSPN = expected;
            actual = target.SelectedSummSPN;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSummsSPN()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = new VIEWMODEL_STATES();
            RegisterViewModel target = new RegisterViewModel(recordID, action);
            List<DBEntity> expected = null;
            List<DBEntity> actual;
            target.SummsSPN = expected;
            actual = target.SummsSPN;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestTotalSPN()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = new VIEWMODEL_STATES();
            RegisterViewModel target = new RegisterViewModel(recordID, action);
            decimal expected = 12;
            decimal actual;
            target.TotalSPN = expected.ToString();
            actual = Convert.ToDecimal(target.TotalSPN);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestTotalZL()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = new VIEWMODEL_STATES();
            RegisterViewModel target = new RegisterViewModel(recordID, action);
            long expected = 12;
            long actual;
            target.TotalZL = expected;
            actual = target.TotalZL;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestTranches123()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = new VIEWMODEL_STATES();
            RegisterViewModel target = new RegisterViewModel(recordID, action);
            List<Tranche123> expected = null;
            List<Tranche123> actual;
            target.Tranches123 = expected;
            actual = target.Tranches123;
            Assert.AreEqual(expected, actual);
        }
    }
}
