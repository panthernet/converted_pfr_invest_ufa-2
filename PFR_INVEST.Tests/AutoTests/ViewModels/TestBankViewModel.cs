﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestBankViewModel : BaseTest
    {



        [TestMethod()]
        public void TestBankViewModelConstructor()
        {
            BankViewModel target = new BankViewModel();
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            BankViewModel target = new BankViewModel();
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            BankViewModel_Accessor target = new BankViewModel_Accessor();
            target.ExecuteSave();
        }

        [TestMethod()]
        public void TestLoad()
        {
            BankViewModel target = new BankViewModel();
            long lid = 1;
            target.Load(lid);

            
            target.Load(0);
        }

       

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestcalculateD()
        {
            BankViewModel_Accessor target = new BankViewModel_Accessor();
            double expected = 0F;
            double actual;
            actual = target.calculateD();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestcalculateR()
        {
            BankViewModel_Accessor target = new BankViewModel_Accessor();
            double expected = 0F;
            double actual;
            actual = target.calculateR();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestcalculateV()
        {
            BankViewModel_Accessor target = new BankViewModel_Accessor();
            double expected = 0F;
            double actual;
            actual = target.calculateV();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDepositList()
        {
            BankViewModel target = new BankViewModel();
            List<BankItem> expected = null;
            List<BankItem> actual;
            target.DepositList = expected;
            actual = target.DepositList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestFitchRatings()
        {
            BankViewModel target = new BankViewModel();
            List<string> expected = null;
            List<string> actual;
            target.FitchRatings = expected;
            actual = target.FitchRatings;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestFitchRatingsDate()
        {
            BankViewModel target = new BankViewModel();
            DateTime expected = DateTime.Today;
            DateTime actual;
            target.FitchRatingsDate = expected;
            actual = target.FitchRatingsDate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestFormalizedName()
        {
            BankViewModel target = new BankViewModel();
            string expected = "test string";
            string actual;
            target.FormalizedName = expected;
            actual = target.FormalizedName;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestFullName()
        {
            BankViewModel target = new BankViewModel();
            string expected = "test string";
            string actual;
            target.FullName = expected;
            actual = target.FullName;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestINN()
        {
            BankViewModel target = new BankViewModel();
            string expected = "test string";
            string actual;
            target.INN = expected;
            actual = target.INN;
            Assert.AreEqual(expected, actual);
        }

       

        [TestMethod()]
        public void TestLegalAddress()
        {
            BankViewModel target = new BankViewModel();
            string expected = "test string";
            string actual;
            target.LegalAddress = expected;
            actual = target.LegalAddress;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestLimit4Money()
        {
            BankViewModel target = new BankViewModel();
            decimal expected = 0;
            decimal actual;
            target.Limit4Money = expected.ToString();
            actual = Convert.ToDecimal(target.Limit4Money);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestLimit4Requests()
        {
            BankViewModel target = new BankViewModel();
            int expected = 1;
            int actual;
            target.Limit4Requests = expected;
            actual = target.Limit4Requests;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestMoodyRatings()
        {
            BankViewModel target = new BankViewModel();
            List<string> expected = null;
            List<string> actual;
            target.MoodyRatings = expected;
            actual = target.MoodyRatings;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestMoodyRatingsDate()
        {
            BankViewModel target = new BankViewModel();
            DateTime expected = DateTime.Today;
            DateTime actual;
            target.MoodyRatingsDate = expected;
            actual = target.MoodyRatingsDate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestOwnCapital()
        {
            BankViewModel target = new BankViewModel();
            Decimal expected = 1;
            Decimal actual;
            target.OwnCapital = expected;
            actual = target.OwnCapital;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestOwnCapitalDate()
        {
            BankViewModel target = new BankViewModel();
            DateTime expected = DateTime.Today;
            DateTime actual;
            target.OwnCapitalDate = expected;
            actual = target.OwnCapitalDate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPfrAgreementStatuses()
        {
            BankViewModel target = new BankViewModel();
            List<string> expected = null;
            List<string> actual;
            target.PfrAgreementStatuses = expected;
            actual = target.PfrAgreementStatuses;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSPRatings()
        {
            BankViewModel target = new BankViewModel();
            List<string> expected = null;
            List<string> actual;
            target.SPRatings = expected;
            actual = target.SPRatings;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSPRatingsDate()
        {
            BankViewModel target = new BankViewModel();
            DateTime expected = DateTime.Today;
            DateTime actual;
            target.SPRatingsDate = expected;
            actual = target.SPRatingsDate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedFitchRating()
        {
            BankViewModel target = new BankViewModel();
            string expected = "test string";
            string actual;
            target.SelectedFitchRating = expected;
            actual = target.SelectedFitchRating;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedMoodyRating()
        {
            BankViewModel target = new BankViewModel();
            string expected = "test string";
            string actual;
            target.SelectedMoodyRating = expected;
            actual = target.SelectedMoodyRating;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedPfrAgreementStatus()
        {
            BankViewModel target = new BankViewModel();
            string expected = "test string";
            string actual;
            target.SelectedPfrAgreementStatus = expected;
            actual = target.SelectedPfrAgreementStatus;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedSPRating()
        {
            BankViewModel target = new BankViewModel();
            string expected = "test string";
            string actual;
            target.SelectedSPRating = expected;
            actual = target.SelectedSPRating;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestShortName()
        {
            BankViewModel target = new BankViewModel();
            string expected = "test string";
            string actual;
            target.ShortName = expected;
            actual = target.ShortName;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestTotalVolume()
        {
            BankViewModel target = new BankViewModel();
            string expected = "test string";
            string actual;
            target.TotalVolume = expected;
            actual = target.TotalVolume;
            Assert.AreEqual(expected, actual);
        }
    }
}
