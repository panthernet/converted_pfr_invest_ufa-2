﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ViewModels.ListItems;
using System.Collections.Generic;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestF50ListViewModel : BaseTest
    {


        [TestMethod()]
        public void TestF50ListViewModelConstructor()
        {
            F50ListViewModel target = new F50ListViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefresh()
        {
            F50ListViewModel_Accessor target = new F50ListViewModel_Accessor();
            bool expected = false;
            bool actual;
            actual = target.CanExecuteRefresh();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshList()
        {
            F50ListViewModel_Accessor target = new F50ListViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefreshList();
            Assert.AreEqual(expected, actual);
        }

       

       

        [TestMethod()]
        public void TestF50List()
        {
            F50ListViewModel target = new F50ListViewModel();
            List<F50ListItem> expected = null;
            List<F50ListItem> actual;
            target.F50List = expected;
            actual = target.F50List;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefresh()
        {
            F50ListViewModel target = new F50ListViewModel();
            ICommand expected = null;
            ICommand actual;
            
            actual = target.Refresh;
            
        }
    }
}
