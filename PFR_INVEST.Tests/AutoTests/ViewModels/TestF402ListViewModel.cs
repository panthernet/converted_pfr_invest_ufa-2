﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestF402ListViewModel : BaseTest
    {


        [TestMethod()]
        public void TestF402ListViewModelConstructor()
        {
            F402ListViewModel target = new F402ListViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefresh()
        {
            F402ListViewModel_Accessor target = new F402ListViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefresh();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshList()
        {
            F402ListViewModel_Accessor target = new F402ListViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefreshList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefresh()
        {
            F402ListViewModel_Accessor target = new F402ListViewModel_Accessor();
            target.ExecuteRefresh();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshList()
        {
            F402ListViewModel_Accessor target = new F402ListViewModel_Accessor();
            target.ExecuteRefreshList();
        }

       

        [TestMethod()]
        public void TestF402List()
        {
            F402ListViewModel target = new F402ListViewModel();
            List<F402ListItem> expected = null;
            List<F402ListItem> actual;
            target.F402List = expected;
            actual = target.F402List;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefresh()
        {
            F402ListViewModel_Accessor target = new F402ListViewModel_Accessor();
            ICommand expected = null;
            ICommand actual;
            target.Refresh = expected;
            actual = target.Refresh;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedItem()
        {
            F402ListViewModel target = new F402ListViewModel();
            F402ListItem expected = null;
            F402ListItem actual;
            target.SelectedItem = expected;
            actual = target.SelectedItem;
            Assert.AreEqual(expected, actual);
        }
    }
}
