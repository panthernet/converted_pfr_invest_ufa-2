﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestF60ListItem : BaseTest
    {

        [TestMethod()]
        public void TestF60ListItemConstructor()
        {
            DBEntity ent = null;
            F60ListItem target = new F60ListItem(ent);
        }

        [TestMethod()]
        public void TestDetained1()
        {
            DBEntity ent = null;
            F60ListItem target = new F60ListItem(ent);
            string actual;
            actual = target.Detained1;
        }

        [TestMethod()]
        public void TestID()
        {
            DBEntity ent = null;
            F60ListItem target = new F60ListItem(ent);
            long actual;
            actual = target.ID;
        }

        [TestMethod()]
        public void TestName()
        {
            DBEntity ent = null;
            F60ListItem target = new F60ListItem(ent);
            string actual;
            actual = target.Name;
        }

        [TestMethod()]
        public void TestPaymentReceived1()
        {
            DBEntity ent = null;
            F60ListItem target = new F60ListItem(ent);
            string actual;
            actual = target.PaymentReceived1;
        }

        [TestMethod()]
        public void TestPaymentTransfer1()
        {
            DBEntity ent = null;
            F60ListItem target = new F60ListItem(ent);
            string actual;
            actual = target.PaymentTransfer1;
        }

        [TestMethod()]
        public void TestProfit1()
        {
            DBEntity ent = null;
            F60ListItem target = new F60ListItem(ent);
            string actual;
            actual = target.Profit1;
        }

        [TestMethod()]
        public void TestQuarter()
        {
            DBEntity ent = null;
            F60ListItem target = new F60ListItem(ent);
            long actual;
            actual = target.Quarter;
        }

        [TestMethod()]
        public void TestRegNum()
        {
            DBEntity ent = null;
            F60ListItem target = new F60ListItem(ent);
            string actual;
            actual = target.RegNum;
        }

        [TestMethod()]
        public void TestSCHAEnd1()
        {
            DBEntity ent = null;
            F60ListItem target = new F60ListItem(ent);
            string actual;
            actual = target.SCHAEnd1;
        }

        [TestMethod()]
        public void TestSCHAStart1()
        {
            DBEntity ent = null;
            F60ListItem target = new F60ListItem(ent);
            string actual;
            actual = target.SCHAStart1;
        }

        [TestMethod()]
        public void TestYear()
        {
            DBEntity ent = null;
            F60ListItem target = new F60ListItem(ent);
            string actual;
            actual = target.Year;
        }
    }
}
