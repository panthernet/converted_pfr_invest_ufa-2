﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ViewModels.ListItems;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestDueDeadAccurateViewModel : BaseTest
    {


        [TestMethod()]
        public void TestDueDeadAccurateViewModelConstructor()
        {
            DueDeadViewModel parentVM = new DueDeadViewModel();
            parentVM.Load(1);
            DueDeadAccurateViewModel target = new DueDeadAccurateViewModel(parentVM);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            DueDeadViewModel parentVM = new DueDeadViewModel();
            parentVM.Load(1);
            DueDeadAccurateViewModel target = new DueDeadAccurateViewModel(parentVM);
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteSave()
        //{
        //    PrivateObject param0 = null;
        //    DueDeadAccurateViewModel_Accessor target = new DueDeadAccurateViewModel_Accessor(param0);
        //    target.ExecuteSave();
        //}

        [TestMethod()]
        public void TestLoad()
        {
            DueDeadViewModel parentVM = new DueDeadViewModel();
            parentVM.Load(1);
            DueDeadAccurateViewModel target = new DueDeadAccurateViewModel(parentVM);
            long lId = 1;
            target.Load(lId);
        }

       

        [TestMethod()]
        public void TestChSumm()
        {
            DueDeadViewModel parentVM = new DueDeadViewModel();
            parentVM.Load(1);
            DueDeadAccurateViewModel target = new DueDeadAccurateViewModel(parentVM);
            string expected = "0,00";
            string actual;
            target.ChSumm = expected;
            actual = target.ChSumm;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDstAccount()
        {
            DueDeadViewModel parentVM = new DueDeadViewModel();
            parentVM.Load(1);
            DueDeadAccurateViewModel target = new DueDeadAccurateViewModel(parentVM);
            PortfolioListItemFull expected = null;
            PortfolioListItemFull actual;
            target.DstAccount = expected;
            actual = target.DstAccount;
            Assert.AreEqual(expected, actual);
        }

       
        [TestMethod()]
        public void TestSrcAccount()
        {
            DueDeadViewModel parentVM = new DueDeadViewModel();
            parentVM.Load(1);
            DueDeadAccurateViewModel target = new DueDeadAccurateViewModel(parentVM);
            PortfolioListItemFull expected = null;
            PortfolioListItemFull actual;
            target.SrcAccount = expected;
            actual = target.SrcAccount;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSumm()
        {
            DueDeadViewModel parentVM = new DueDeadViewModel();
            parentVM.Load(1);
            DueDeadAccurateViewModel target = new DueDeadAccurateViewModel(parentVM);
            decimal expected = 12;
            decimal actual;
            target.Summ = expected.ToString();
            actual = Convert.ToDecimal(target.Summ);
            Assert.AreEqual(expected, actual);
        }
    }
}
