﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestClientThemesViewModel : BaseTest
    {

        [TestMethod()]
        public void TestClientThemesViewModelConstructor()
        {
            ClientThemesViewModel target = new ClientThemesViewModel();
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            ClientThemesViewModel target = new ClientThemesViewModel();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            ClientThemesViewModel_Accessor target = new ClientThemesViewModel_Accessor();
            target.ExecuteSave();
        }

        [TestMethod()]
        public void TestAccountKindsList()
        {
            ClientThemesViewModel target = new ClientThemesViewModel();
            List<string> expected = null;
            List<string> actual;
            target.AccountKindsList = expected;
            actual = target.AccountKindsList;
            Assert.AreEqual(expected, actual);
        }


        [TestMethod()]
        public void TestName()
        {
            ClientThemesViewModel target = new ClientThemesViewModel();
            string expected = string.Empty;
            string actual;
            target.Name = expected;
            actual = target.Name;
            Assert.AreEqual(expected, actual);
        }
    }
}
