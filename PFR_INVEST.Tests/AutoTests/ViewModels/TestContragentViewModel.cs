﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestContragentViewModel : BaseTest
    {



        [TestMethod()]
        public void TestContragentViewModelConstructor()
        {
            ContragentViewModel target = new ContragentViewModel();
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            ContragentViewModel target = new ContragentViewModel();
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            ContragentViewModel_Accessor target = new ContragentViewModel_Accessor();
            target.ExecuteSave();
        }

        [TestMethod()]
        public void TestLoadContragent()
        {
            ContragentViewModel target = new ContragentViewModel();
            long id = 1;
            target.LoadContragent(id);
            target.LoadContragent(0);
        }

        [TestMethod()]
        public void TestRefreshStatusesList()
        {
            ContragentViewModel target = new ContragentViewModel();
            target.RefreshStatusesList();
        }

        [TestMethod()]
        public void TestContragentName()
        {
            ContragentViewModel target = new ContragentViewModel();
            string expected = string.Empty;
            string actual;
            target.ContragentName = expected;
            actual = target.ContragentName;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestContragentType()
        {
            ContragentViewModel target = new ContragentViewModel();
            string expected = string.Empty;
            string actual;
            target.ContragentType = expected;
            actual = target.ContragentType;
            Assert.AreEqual(expected, actual);
        }


        [TestMethod()]
        public void TestSelectedStatus()
        {
            ContragentViewModel target = new ContragentViewModel();
            DB2Status expected = null;
            DB2Status actual;
            target.SelectedStatus = expected;
            actual = target.SelectedStatus;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestStatusID()
        {
            ContragentViewModel target = new ContragentViewModel();
            long expected = 1;
            long actual;
            target.StatusID = expected;
            actual = target.StatusID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestStatusesList()
        {
            ContragentViewModel target = new ContragentViewModel();
            List<DB2Status> expected = null;
            List<DB2Status> actual;
            target.StatusesList = expected;
            actual = target.StatusesList;
            Assert.AreEqual(expected, actual);
        }
    }
}
