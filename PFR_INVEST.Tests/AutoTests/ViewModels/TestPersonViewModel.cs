﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Input;
using System.Collections;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestPersonViewModel : BaseTest
    {




        [TestMethod()]
        public void TestPersonViewModelConstructor()
        {
            PersonViewModel target = new PersonViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteAddPerson()
        {
            PersonViewModel_Accessor target = new PersonViewModel_Accessor();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteAddPerson();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteGetTestString()
        {
            PersonViewModel_Accessor target = new PersonViewModel_Accessor();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteGetTestString();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            PersonViewModel target = new PersonViewModel();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteGetTestString()
        {
            PersonViewModel_Accessor target = new PersonViewModel_Accessor(); 
            target.ExecuteGetTestString();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            PersonViewModel_Accessor target = new PersonViewModel_Accessor(); 
            target.ExecuteSave();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefreshPostList()
        {
            PersonViewModel_Accessor target = new PersonViewModel_Accessor(); 
            target.RefreshPostList();
        }

        [TestMethod()]
        public void TestAddPersonCommand()
        {
            PersonViewModel target = new PersonViewModel(); 
            ICommand actual;
            actual = target.AddPersonCommand;
        }

        [TestMethod()]
        public void TestBirthDate()
        {
            PersonViewModel target = new PersonViewModel(); 
            DateTime expected = new DateTime(); 
            DateTime actual;
            target.BirthDate = expected;
            actual = target.BirthDate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestFirstName()
        {
            PersonViewModel target = new PersonViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.FirstName = expected;
            actual = target.FirstName;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestGetTestStringCommand()
        {
            PersonViewModel target = new PersonViewModel(); 
            ICommand actual;
            actual = target.GetTestStringCommand;
        }


        [TestMethod()]
        public void TestLastName()
        {
            PersonViewModel target = new PersonViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.LastName = expected;
            actual = target.LastName;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestMiddleName()
        {
            PersonViewModel target = new PersonViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.MiddleName = expected;
            actual = target.MiddleName;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPost()
        {
            PersonViewModel target = new PersonViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.Post = expected;
            actual = target.Post;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPostsList()
        {
            PersonViewModel target = new PersonViewModel(); 
            ArrayList expected = null; 
            ArrayList actual;
            target.PostsList = expected;
            actual = target.PostsList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestTestString()
        {
            PersonViewModel target = new PersonViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.TestString = expected;
            actual = target.TestString;
            Assert.AreEqual(expected, actual);
        }
    }
}
