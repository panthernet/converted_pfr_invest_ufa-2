﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestDB2DeadZL_NPFName : BaseTest
    {


        [TestMethod()]
        public void TestDB2DeadZL_NPFNameConstructor()
        {
            long erzlID = 1;
            DateTime date = DateTime.Today;
            long count = 1;
            long creditAccountID = 1;
            long debetAccountID = 1;
            DB2DeadZL_NPFName target = new DB2DeadZL_NPFName(erzlID, date, count, creditAccountID, debetAccountID);
        }

        [TestMethod()]
        public void TestDB2DeadZL_NPFNameConstructor1()
        {
            long id = 0;
            long erzlID = 0;
            DateTime date = new DateTime();
            long count = 0;
            long creditAccountID = 0;
            long debetAccountID = 0;
            DB2DeadZL_NPFName target = new DB2DeadZL_NPFName(id, erzlID, date, count, creditAccountID, debetAccountID);
        }

        [TestMethod()]
        public void TestMonth()
        {
            long erzlID = 1;
            DateTime date = DateTime.Today;
            long count = 1;
            long creditAccountID = 1;
            long debetAccountID = 1;
            DB2DeadZL_NPFName target = new DB2DeadZL_NPFName(erzlID, date, count, creditAccountID, debetAccountID);
            string actual;
            actual = target.Month;
        }

        [TestMethod()]
        public void TestNPFName()
        {
            long erzlID = 1;
            DateTime date = DateTime.Today;
            long count = 1;
            long creditAccountID = 1;
            long debetAccountID = 1;
            DB2DeadZL_NPFName target = new DB2DeadZL_NPFName(erzlID, date, count, creditAccountID, debetAccountID);
            string expected = string.Empty;
            string actual;
            target.NPFName = expected;
            actual = target.NPFName;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestYear()
        {
            long erzlID = 1;
            DateTime date = DateTime.Today;
            long count = 1;
            long creditAccountID = 1;
            long debetAccountID = 1;
            DB2DeadZL_NPFName target = new DB2DeadZL_NPFName(erzlID, date, count, creditAccountID, debetAccountID);
            string actual;
            actual = target.Year;

        }
    }
}
