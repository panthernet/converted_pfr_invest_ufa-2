﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestRateViewModel : BaseTest
    {

        [TestMethod()]
        public void TestRateViewModelConstructor()
        {
            long _id = 1;
            RateViewModel target = new RateViewModel(_id);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            long _id = 1;
            RateViewModel target = new RateViewModel(_id);
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteSave()
        //{
        //    PrivateObject param0 = null;
        //    RateViewModel_Accessor target = new RateViewModel_Accessor(param0);
        //    target.ExecuteSave();
        //}

      

        [TestMethod()]
        public void TestCurrencyList()
        {
            long _id = 1;
            RateViewModel target = new RateViewModel(_id);
            List<DBEntity> expected = null;
            List<DBEntity> actual;
            target.CurrencyList = expected;
            actual = target.CurrencyList;
            Assert.AreEqual(expected, actual);
        }

      
        [TestMethod()]
        public void TestSelectedCurrency()
        {
            long _id = 1;
            RateViewModel target = new RateViewModel(_id);
            DBEntity expected = null;
            DBEntity actual;
            target.SelectedCurrency = expected;
            actual = target.SelectedCurrency;
            //Assert.AreEqual(expected, actual);
        }
    }
}
