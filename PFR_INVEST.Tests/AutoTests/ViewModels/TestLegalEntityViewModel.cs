﻿using System.Collections.Generic;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Proxy;
using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestLegalEntityViewModel : BaseTest
    {

        [TestMethod()]
        public void TestLegalEntityViewModelConstructor()
        {
            LegalEntityViewModel target = new LegalEntityViewModel();
        }

       
        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteAddBranch()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel_Accessor target = new PFR_INVEST.ViewModels.LegalEntityViewModel_Accessor(); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteAddBranch();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteDeleteBranch()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel_Accessor target = new PFR_INVEST.ViewModels.LegalEntityViewModel_Accessor(); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteDeleteBranch();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteEditBranch()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel_Accessor target = new PFR_INVEST.ViewModels.LegalEntityViewModel_Accessor(); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteEditBranch();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCheckActivity()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel_Accessor target = new PFR_INVEST.ViewModels.LegalEntityViewModel_Accessor(); 
            System.DateTime start = new DateTime(2018,10,10); 
            System.DateTime end = new System.DateTime();
            string expected = "Деятельность осуществляется"; 
            string actual;
            actual = target.CheckActivity(start, end);
            Assert.AreEqual(expected, actual);
        }

       

     
        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteDeleteBranch()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel_Accessor target = new PFR_INVEST.ViewModels.LegalEntityViewModel_Accessor(); 
            target.ExecuteDeleteBranch();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteEditBranch()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel_Accessor target = new PFR_INVEST.ViewModels.LegalEntityViewModel_Accessor(); 
            target.ExecuteEditBranch();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel_Accessor target = new PFR_INVEST.ViewModels.LegalEntityViewModel_Accessor(); 
            target.ExecuteSave();
            
        }

        [TestMethod()]
        public void TestLoadLegalEntity()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            long id = 1; 
            target.LoadLegalEntity(id);
            
        }

        [TestMethod()]
        public void TestRefreshContragentsList()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            target.RefreshContragentsList();
            
        }

        [TestMethod()]
        public void TestRefreshNPFPauseList()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            target.RefreshNPFPauseList();
            
        }

        [TestMethod()]
        public void TestRefreshReorganizationsList()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            target.RefreshReorganizationsList();
            
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestisEmail()
        {
            string inputEmail = string.Empty; 
            bool expected = false; 
            bool actual;
            actual = PFR_INVEST.ViewModels.LegalEntityViewModel_Accessor.isEmail(inputEmail);
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestAddBranch()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            System.Windows.Input.ICommand actual;
            actual = target.AddBranch;
            
        }

        [TestMethod()]
        public void TestBankAccountsList()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel();
            List<DB2BankAccount> expected = null;
            List<DB2BankAccount> actual;
            target.BankAccountsList = expected;
            actual = target.BankAccountsList;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestBranchesList()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            System.Collections.Generic.List<PFR_INVEST.ViewModels.BranchItem> expected = null; 
            System.Collections.Generic.List<PFR_INVEST.ViewModels.BranchItem> actual;
            target.BranchesList = expected;
            actual = target.BranchesList;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestCloseDate()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            System.DateTime? expected = new System.DateTime(); 
            System.DateTime? actual;
            target.CloseDate = expected;
            actual = target.CloseDate;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestComment()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.Comment = expected;
            actual = target.Comment;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestContragentID()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            long expected = 0;
            target.ContragentID = expected;
            long? actual = target.ContragentID;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestDeclName()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.DeclName = expected;
            actual = target.DeclName;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestDeleteBranch()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            System.Windows.Input.ICommand actual;
            actual = target.DeleteBranch;
            
        }

        [TestMethod()]
        public void TestEMail()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.EMail = expected;
            actual = target.EMail;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestEditBranch()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            System.Windows.Input.ICommand actual;
            actual = target.EditBranch;
            
        }

        [TestMethod()]
        public void TestFax()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.Fax = expected;
            actual = target.Fax;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestFormalizedName()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.FormalizedName = expected;
            actual = target.FormalizedName;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestFullName()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.FullName = expected;
            actual = target.FullName;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestHeadFullName()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.HeadFullName = expected;
            actual = target.HeadFullName;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestHeadPosition()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.HeadPosition = expected;
            actual = target.HeadPosition;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestINN()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.INN = expected;
            actual = target.INN;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestInfoSource()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.InfoSource = expected;
            actual = target.InfoSource;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestInputName()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.InputName = expected;
            actual = target.InputName;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestIsDataChanged()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            bool expected = true; 
            bool actual;
            target.IsDataChanged = expected;
            actual = target.IsDataChanged;
            Assert.AreEqual(expected, actual);
            
        }


        [TestMethod()]
        public void TestLS_ID()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            long expected = 0; 
            long actual;
            target.LS_ID = expected;
            actual = target.LS_ID;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestLegalAddress()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.LegalAddress = expected;
            actual = target.LegalAddress;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestLetterWho()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.LetterWho = expected;
            actual = target.LetterWho;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestLicCloseDate()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            DateTime? expected = null;
            DateTime? actual;
            target.LicCloseDate = expected;
            actual = target.LicCloseDate;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestLicDate()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel();
            DateTime? expected = null;
            DateTime? actual;
            target.LicDate = expected;
            actual = target.LicDate;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestLicNumber()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.LicNumber = expected;
            actual = target.LicNumber;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestLicRegDate()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel();
            DateTime? expected = null;
            DateTime? actual;
            target.LicRegDate = expected;
            actual = target.LicRegDate;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestLicRegistrator()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.LicRegistrator = expected;
            actual = target.LicRegistrator;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestLicSuspEndDate()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel();
            DateTime? expected = null;
            DateTime? actual;
            target.LicSuspEndDate = expected;
            actual = target.LicSuspEndDate;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestLicSuspStartDate()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel();
            DateTime? expected = null;
            DateTime? actual;
            target.LicSuspStartDate = expected;
            actual = target.LicSuspStartDate;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestNPFPauseList()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            System.Collections.Generic.List<PFR_INVEST.ViewModels.NPFPauseListItem> expected = null; 
            System.Collections.Generic.List<PFR_INVEST.ViewModels.NPFPauseListItem> actual;
            target.NPFPauseList = expected;
            actual = target.NPFPauseList;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestNPFStatus()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.NPFStatus = expected;
            actual = target.NPFStatus;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestNPFStatusIsEditable()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            bool actual;
            actual = target.NPFStatusIsEditable;
            
        }

        [TestMethod()]
        public void TestOKPP()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.OKPP = expected;
            actual = target.OKPP;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestOldNamesList()
        {
            LegalEntityViewModel target = new LegalEntityViewModel();
            List<OldNPFName> expected = null;
            List<OldNPFName> actual;
            target.OldNamesList = expected;
            actual = target.OldNamesList;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestPhone()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.Phone = expected;
            actual = target.Phone;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestPostalAddress()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.PostalAddress = expected;
            actual = target.PostalAddress;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestRealAddress()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.RealAddress = expected;
            actual = target.RealAddress;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestRegistrationDate()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            System.DateTime? expected = new System.DateTime(); 
            System.DateTime? actual;
            target.RegistrationDate = expected;
            actual = target.RegistrationDate;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestRegistrationNumber()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.RegistrationNumber = expected;
            actual = target.RegistrationNumber;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestRegistrator()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.Registrator = expected;
            actual = target.Registrator;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestReorganizationsList()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            System.Collections.Generic.List<PFR_INVEST.Proxy.DB2Reorganization> expected = null; 
            System.Collections.Generic.List<PFR_INVEST.Proxy.DB2Reorganization> actual;
            target.ReorganizationsList = expected;
            actual = target.ReorganizationsList;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestSelectedItem()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            PFR_INVEST.ViewModels.BranchItem expected = null; 
            PFR_INVEST.ViewModels.BranchItem actual;
            target.SelectedItem = expected;
            actual = target.SelectedItem;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestShortName()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.ShortName = expected;
            actual = target.ShortName;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestSite()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.Site = expected;
            actual = target.Site;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestSubsidiaries()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.Subsidiaries = expected;
            actual = target.Subsidiaries;
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        public void TestTransfDocKind()
        {
            PFR_INVEST.ViewModels.LegalEntityViewModel target = new PFR_INVEST.ViewModels.LegalEntityViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.TransfDocKind = expected;
            actual = target.TransfDocKind;
            Assert.AreEqual(expected, actual);
            
        }
    }
}
