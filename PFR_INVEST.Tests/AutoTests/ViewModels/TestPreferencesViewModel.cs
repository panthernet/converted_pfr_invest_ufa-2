﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestPreferencesViewModel : BaseTest
    {

        [TestMethod()]
        public void TestPreferencesViewModelConstructor()
        {
            PreferencesViewModel target = new PreferencesViewModel();
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            PreferencesViewModel target = new PreferencesViewModel();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteSave()
        //{
        //    PreferencesViewModel_Accessor target = new PreferencesViewModel_Accessor(); 
        //    target.ExecuteSave();
        //}

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefreshServerSettings()
        {
            PreferencesViewModel_Accessor target = new PreferencesViewModel_Accessor(); 
            target.RefreshServerSettings();
        }

        [TestMethod()]
        public void TestClientVersion()
        {
            PreferencesViewModel target = new PreferencesViewModel(); 
            string actual;
            actual = target.ClientVersion;
        }

        [TestMethod()]
        public void TestCognosServerPath()
        {
            PreferencesViewModel target = new PreferencesViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.CognosServerPath = expected;
            actual = target.CognosServerPath;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDBFPath()
        {
            PreferencesViewModel target = new PreferencesViewModel(); 
            string expected = "string.Empty"; 
            string actual;
            target.DBFPath = expected;
            actual = target.DBFPath;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestIISServicePath()
        {
            PreferencesViewModel target = new PreferencesViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.IISServicePath = expected;
            actual = target.IISServicePath;
            Assert.AreEqual(expected, actual);
        }


        [TestMethod()]
        public void TestMQManagerName()
        {
            PreferencesViewModel target = new PreferencesViewModel(); 
            string expected = "Mgr"; 
            string actual;
            target.MQManagerName = expected;
            actual = target.MQManagerName;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestMQQueueName()
        {
            PreferencesViewModel target = new PreferencesViewModel(); 
            string expected = "string.Empty"; 
            string actual;
            target.MQQueueName = expected;
            actual = target.MQQueueName;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestMQXMLPath()
        {
            PreferencesViewModel target = new PreferencesViewModel(); 
            string expected = "C:/somename"; 
            string actual;
            target.MQXMLPath = expected;
            actual = target.MQXMLPath;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestServerVersion()
        {
            PreferencesViewModel target = new PreferencesViewModel(); 
            string actual;
            actual = target.ServerVersion;
        }
    }
}
