﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestIssue : BaseTest
    {

        [TestMethod()]
        public void TestIssueConstructor()
        {
            PFR_INVEST.ViewModels.Issue target = new PFR_INVEST.ViewModels.Issue();
            
        }

        [TestMethod()]
        public void TestID()
        {
            PFR_INVEST.ViewModels.Issue target = new PFR_INVEST.ViewModels.Issue(); 
            long expected = 1; 
            long actual;
            target.ID = expected;
            actual = target.ID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestName()
        {
            PFR_INVEST.ViewModels.Issue target = new PFR_INVEST.ViewModels.Issue(); 
            string expected = string.Empty; 
            string actual;
            target.Name = expected;
            actual = target.Name;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestNomValue()
        {
            PFR_INVEST.ViewModels.Issue target = new PFR_INVEST.ViewModels.Issue(); 
            decimal expected = new decimal(); 
            decimal actual;
            target.NomValue = expected;
            actual = target.NomValue;
            Assert.AreEqual(expected, actual);
        }
    }
}
