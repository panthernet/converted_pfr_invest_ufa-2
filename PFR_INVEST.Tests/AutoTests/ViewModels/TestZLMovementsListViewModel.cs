﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using PFR_INVEST.ViewModels.ListItems;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.ViewModels;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestZLMovementsListViewModel : BaseTest
    {


        [TestMethod()]
        public void TestZLMovementsListViewModelConstructor()
        {
            ZLMovementsListViewModel target = new ZLMovementsListViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefresh()
        {
            ZLMovementsListViewModel_Accessor target = new ZLMovementsListViewModel_Accessor();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteRefresh();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshList()
        {
            ZLMovementsListViewModel_Accessor target = new ZLMovementsListViewModel_Accessor();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteRefreshList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefresh()
        {
            ZLMovementsListViewModel_Accessor target = new ZLMovementsListViewModel_Accessor(); 
            target.ExecuteRefresh();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshList()
        {
            ZLMovementsListViewModel_Accessor target = new ZLMovementsListViewModel_Accessor(); 
            target.ExecuteRefreshList();
        }

        [TestMethod()]
        public void TestList()
        {
            ZLMovementsListViewModel target = new ZLMovementsListViewModel(); 
            List<ZLMovement> expected = null;
            List<ZLMovement> actual;
            target.ZLMovements = expected;
            actual = target.ZLMovements;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefresh()
        {
            ZLMovementsListViewModel_Accessor target = new ZLMovementsListViewModel_Accessor(); 
            ICommand expected = null; 
            ICommand actual;
            target.Refresh = expected;
            actual = target.Refresh;
            Assert.AreEqual(expected, actual);
        }
    }
}
