﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestBankAccountsListViewModel : BaseTest
    {



        [TestMethod()]
        public void TestBankAccountsListViewModelConstructor()
        {
            BankAccountsListViewModel target = new BankAccountsListViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshBankAccountsList()
        {
            BankAccountsListViewModel_Accessor target = new BankAccountsListViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefreshBankAccountsList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshList()
        {
            BankAccountsListViewModel_Accessor target = new BankAccountsListViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefreshList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshBankAccountsList()
        {
            BankAccountsListViewModel_Accessor target = new BankAccountsListViewModel_Accessor();
            target.ExecuteRefreshBankAccountsList();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshList()
        {
            BankAccountsListViewModel_Accessor target = new BankAccountsListViewModel_Accessor();
            target.ExecuteRefreshList();
        }

        [TestMethod()]
        public void TestBankAccountsList()
        {
            BankAccountsListViewModel target = new BankAccountsListViewModel();
            DB2BankAccount[] expected = null;
            DB2BankAccount[] actual;
            target.BankAccountsList = expected;
            actual = target.BankAccountsList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRefreshBankAccountsList()
        {
            BankAccountsListViewModel target = new BankAccountsListViewModel();
            ICommand actual;
            actual = target.RefreshBankAccountsList;
        }
    }
}
