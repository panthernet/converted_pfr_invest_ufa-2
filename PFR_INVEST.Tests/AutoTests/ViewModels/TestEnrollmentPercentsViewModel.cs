﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ViewModels.ListItems;
using PFR_INVEST.Proxy;
using System.Collections.Generic;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestEnrollmentPercentsViewModel : BaseTest
    {



        [TestMethod()]
        public void TestEnrollmentPercentsViewModelConstructor()
        {
            EnrollmentPercentsViewModel target = new EnrollmentPercentsViewModel();
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            EnrollmentPercentsViewModel target = new EnrollmentPercentsViewModel();
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteSelectAccount()
        {
            EnrollmentPercentsViewModel_Accessor target = new EnrollmentPercentsViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteSelectAccount();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            EnrollmentPercentsViewModel_Accessor target = new EnrollmentPercentsViewModel_Accessor();
            target.ExecuteSave();
        }



        [TestMethod()]
        public void TestLoad()
        {
            EnrollmentPercentsViewModel target = new EnrollmentPercentsViewModel();
            long lId = 1;
            target.Load(lId);
        }

     

        [TestMethod()]
        public void TestAccount()
        {
            EnrollmentPercentsViewModel target = new EnrollmentPercentsViewModel();
            PortfolioListItemFull expected = null;
            PortfolioListItemFull actual;
            target.Account = expected;
            actual = target.Account;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestCurrencyList()
        {
            EnrollmentPercentsViewModel target = new EnrollmentPercentsViewModel();
            List<DBEntity> expected = null;
            List<DBEntity> actual;
            target.CurrencyList = expected;
            actual = target.CurrencyList;
            Assert.AreEqual(expected, actual);
        }

      

        [TestMethod()]
        public void TestRate()
        {
            EnrollmentPercentsViewModel target = new EnrollmentPercentsViewModel();
            Decimal expected = 12;
            Decimal actual;
            target.Rate = expected;
            actual = target.Rate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectAccount()
        {
            EnrollmentPercentsViewModel target = new EnrollmentPercentsViewModel();
            ICommand actual;
            actual = target.SelectAccount;
        }

        [TestMethod()]
        public void TestSelectedCurrency()
        {
            EnrollmentPercentsViewModel target = new EnrollmentPercentsViewModel();
            DBEntity expected = null;
            DBEntity actual;
            target.SelectedCurrency = expected;
            actual = target.SelectedCurrency;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSumm()
        {
            EnrollmentPercentsViewModel target = new EnrollmentPercentsViewModel();
            Decimal expected = 12;
            Decimal actual;
            target.Summ = expected;
            actual = target.Summ;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestTotal()
        {
            EnrollmentPercentsViewModel target = new EnrollmentPercentsViewModel();
            Decimal actual;
            actual = target.Total;
        }
    }
}
