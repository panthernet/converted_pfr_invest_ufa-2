﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using PFR_INVEST.Proxy;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestSIRegisterViewModel : BaseTest
    {

        [TestMethod()]
        public void TestSIRegisterViewModelConstructor()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SIRegisterViewModel target = new SIRegisterViewModel(id, action);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            long id = 1; 
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SIRegisterViewModel target = new SIRegisterViewModel(id, action); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteSave()
        //{
        //    PrivateObject param0 = null; 
        //    SIRegisterViewModel_Accessor target = new SIRegisterViewModel_Accessor(param0); 
        //    target.ExecuteSave();
        //}


        [TestMethod()]
        public void TestDirections()
        {
            long id = 1; 
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SIRegisterViewModel target = new SIRegisterViewModel(id, action); 
            List<SPNDirection> expected = null;
            List<SPNDirection> actual;
            target.Directions = expected;
            actual = target.Directions;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDocTypes()
        {
            long id = 1; 
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SIRegisterViewModel target = new SIRegisterViewModel(id, action); 
            List<string> expected = null; 
            List<string> actual;
            target.DocTypes = expected;
            actual = target.DocTypes;
            Assert.AreEqual(expected, actual);
        }

      

        [TestMethod()]
        public void TestMonthsList()
        {
            long id = 1; 
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SIRegisterViewModel target = new SIRegisterViewModel(id, action); 
            List<Month> expected = null;
            List<Month> actual;
            target.MonthsList = expected;
            actual = target.MonthsList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestOpTypes()
        {
            long id = 1; 
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SIRegisterViewModel target = new SIRegisterViewModel(id, action);
            List<SPNOperation> expected = null;
            List<SPNOperation> actual;
            target.Operations = expected;
            actual = target.Operations;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRequests()
        {
            long id = 1; 
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SIRegisterViewModel target = new SIRegisterViewModel(id, action); 
            List<DBEntity> expected = null; 
            List<DBEntity> actual;
            target.Requests = expected;
            actual = target.Requests;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRequestsGrid()
        {
            long id = 1; 
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SIRegisterViewModel target = new SIRegisterViewModel(id, action); 
            List<SIRegisterItem> expected = null; 
            List<SIRegisterItem> actual;
            target.RequestsGrid = expected;
            actual = target.RequestsGrid;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedDirection()
        {
            long id = 1; 
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SIRegisterViewModel target = new SIRegisterViewModel(id, action);
            SPNDirection expected = new SPNDirection();
            SPNDirection actual;
            target.SelectedDirection = expected;
            actual = target.SelectedDirection;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedDocType()
        {
            long id = 1; 
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SIRegisterViewModel target = new SIRegisterViewModel(id, action); 
            string expected = string.Empty; 
            string actual;
            target.SelectedDocType = expected;
            actual = target.SelectedDocType;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedMonth()
        {
            long id = 1; 
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SIRegisterViewModel target = new SIRegisterViewModel(id, action); 
            Month expected = null;
            Month actual;
            target.SelectedMonth = expected;
            actual = target.SelectedMonth;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedOpType()
        {
            long id = 1; 
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SIRegisterViewModel target = new SIRegisterViewModel(id, action);
            SPNOperation expected = new SPNOperation();
            SPNOperation actual;
            target.SelectedOperation = expected;
            actual = target.SelectedOperation;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedYear()
        {
            long id = 1; 
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SIRegisterViewModel target = new SIRegisterViewModel(id, action); 
            Year expected = null;
            Year actual;
            target.SelectedYear = expected;
            actual = target.SelectedYear;
            //Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestTotalInvestDohod()
        {
            long id = 1; 
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SIRegisterViewModel target = new SIRegisterViewModel(id, action); 
            Decimal expected = new Decimal(); 
            Decimal actual;
            target.TotalInvestDohod = expected;
            actual = target.TotalInvestDohod;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestTotalZLSumm()
        {
            long id = 1; 
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SIRegisterViewModel target = new SIRegisterViewModel(id, action); 
            Decimal expected = new Decimal(); 
            Decimal actual;
            target.TotalZLSumm = expected;
            actual = target.TotalZLSumm;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestUKTotalSumm()
        {
            long id = 1; 
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SIRegisterViewModel target = new SIRegisterViewModel(id, action); 
            Decimal expected = new Decimal(); 
            Decimal actual;
            target.UKTotalSumm = expected;
            actual = target.UKTotalSumm;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestYearsList()
        {
            long id = 1; 
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SIRegisterViewModel target = new SIRegisterViewModel(id, action);
            List<Year> expected = null;
            List<Year> actual;
            target.YearsList = expected;
            actual = target.YearsList;
            Assert.AreEqual(expected, actual);
        }
    }
}
