﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestFinRegisterCorrViewModel : BaseTest
    {

        [TestMethod()]
        public void TestFinRegisterCorrViewModelConstructor()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            FinRegisterCorrViewModel target = new FinRegisterCorrViewModel(recordID, action);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            FinRegisterCorrViewModel target = new FinRegisterCorrViewModel(recordID, action);
            bool expected = true;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteSave()
        //{
        //    PrivateObject param0 = null;
        //    FinRegisterCorrViewModel_Accessor target = new FinRegisterCorrViewModel_Accessor(param0);
        //    target.ExecuteSave();
        //}

      

        [TestMethod()]
        public void TestCurrSumm()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            FinRegisterCorrViewModel target = new FinRegisterCorrViewModel(recordID, action);
            Decimal expected = new Decimal();
            Decimal actual;
            target.CurrSumm = expected;
            actual = target.CurrSumm;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestFinRegDate()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            FinRegisterCorrViewModel target = new FinRegisterCorrViewModel(recordID, action);
            DateTime expected = new DateTime();
            DateTime actual;
            target.FinRegDate = expected;
            actual = target.FinRegDate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestFinRegID()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            FinRegisterCorrViewModel target = new FinRegisterCorrViewModel(recordID, action);
            long expected = 0;
            long actual;
            target.FinRegID = expected;
            actual = target.FinRegID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestFinRegNum()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            FinRegisterCorrViewModel target = new FinRegisterCorrViewModel(recordID, action);
            long expected = 0;
            long actual;
            target.FinRegNum = expected;
            actual = target.FinRegNum;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestIsDataChanged()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            FinRegisterCorrViewModel target = new FinRegisterCorrViewModel(recordID, action);
            bool expected = true;
            bool actual;
            target.IsDataChanged = expected;
            actual = target.IsDataChanged;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestItem()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            FinRegisterCorrViewModel target = new FinRegisterCorrViewModel(recordID, action);
            string columnName = string.Empty;
            string actual;
            actual = target[columnName];
        }

        [TestMethod()]
        public void TestNotSaved()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            FinRegisterCorrViewModel target = new FinRegisterCorrViewModel(recordID, action);
            bool expected = false;
            bool actual;
            target.NotSaved = expected;
            actual = target.NotSaved;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestNum()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            FinRegisterCorrViewModel target = new FinRegisterCorrViewModel(recordID, action);
            long expected = 0;
            long actual;
            target.Num = expected;
            actual = target.Num;
            Assert.AreEqual(expected, actual);
        }
    }
}
