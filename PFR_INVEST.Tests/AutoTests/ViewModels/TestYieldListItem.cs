﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestYieldListItem : BaseTest
    {
        [TestMethod()]
        public void TestYieldListItemConstructor()
        {
            DBEntity ent = null; 
            YieldListItem target = new YieldListItem(ent);
        }

        [TestMethod()]
        public void TestID()
        {
            DBEntity ent = null; 
            YieldListItem target = new YieldListItem(ent); 
            long actual;
            actual = target.ID;
        }

        [TestMethod()]
        public void TestName()
        {
            DBEntity ent = null; 
            YieldListItem target = new YieldListItem(ent); 
            string actual;
            actual = target.Name;
        }

        [TestMethod()]
        public void TestQuarter()
        {
            DBEntity ent = null; 
            YieldListItem target = new YieldListItem(ent);
            string actual;
            actual = target.Quarter;
        }

        [TestMethod()]
        public void TestRegNum()
        {
            DBEntity ent = null; 
            YieldListItem target = new YieldListItem(ent); 
            string actual;
            actual = target.RegNum;
        }

        [TestMethod()]
        public void TestYear()
        {
            DBEntity ent = null; 
            YieldListItem target = new YieldListItem(ent); 
            string actual;
            actual = target.Year;
        }

        [TestMethod()]
        public void TestYield12()
        {
            DBEntity ent = null; 
            YieldListItem target = new YieldListItem(ent); 
            string actual;
            actual = target.Yield12;
        }

        [TestMethod()]
        public void TestYield3()
        {
            DBEntity ent = null; 
            YieldListItem target = new YieldListItem(ent); 
            string actual;
            actual = target.Yield3;
        }

        [TestMethod()]
        public void TestYieldYear()
        {
            DBEntity ent = null; 
            YieldListItem target = new YieldListItem(ent); 
            string actual;
            actual = target.YieldYear;
        }
    }
}
