﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestContragentsListViewModel : BaseTest
    {

        [TestMethod()]
        public void TestContragentsListViewModelConstructor()
        {
            ContragentsListViewModel target = new ContragentsListViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshList()
        {
            ContragentsListViewModel_Accessor target = new ContragentsListViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefreshList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshNPFList()
        {
            ContragentsListViewModel_Accessor target = new ContragentsListViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefreshNPFList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshList()
        {
            ContragentsListViewModel_Accessor target = new ContragentsListViewModel_Accessor();
            target.ExecuteRefreshList();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshNPFList()
        {
            ContragentsListViewModel_Accessor target = new ContragentsListViewModel_Accessor();
            target.ExecuteRefreshNPFList();
        }

        [TestMethod()]
        public void TestNPFList()
        {
            ContragentsListViewModel target = new ContragentsListViewModel();
            DB2NPFListItem[] expected = null;
            DB2NPFListItem[] actual;
            target.NPFList = expected;
            actual = target.NPFList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRefreshNPFList()
        {
            ContragentsListViewModel target = new ContragentsListViewModel();
            ICommand actual;
            actual = target.RefreshNPFList;
        }
    }
}
