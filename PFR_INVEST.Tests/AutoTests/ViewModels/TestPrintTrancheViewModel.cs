﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestPrintTrancheViewModel : BaseTest
    {

        [TestMethod()]
        public void TestPrintTrancheViewModelConstructor()
        {
            long registerID = 1; 
            int trancheNum = 1; 
            PrintTrancheViewModel target = new PrintTrancheViewModel(registerID, trancheNum);
        }

        [TestMethod()]
        public void TestDate()
        {
            long registerID = 1;
            int trancheNum = 1; 
            PrintTrancheViewModel target = new PrintTrancheViewModel(registerID, trancheNum); 
            DateTime actual;
            actual = target.Date;
        }

        [TestMethod()]
        public void TestItems()
        {
            long registerID = 1;
            int trancheNum = 1; 
            PrintTrancheViewModel target = new PrintTrancheViewModel(registerID, trancheNum); 
            List<PrintTrancheItem> actual;
            actual = target.Items;
        }
    }
}
