﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestTreasurityViewModel : BaseTest
    {



        [TestMethod()]
        public void TestTreasurityViewModelConstructor()
        {
            TreasurityViewModel target = new TreasurityViewModel();
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            TreasurityViewModel target = new TreasurityViewModel(); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            TreasurityViewModel_Accessor target = new TreasurityViewModel_Accessor(); 
            target.ExecuteSave();
        }

        [TestMethod()]
        public void TestLoad()
        {
            TreasurityViewModel target = new TreasurityViewModel(); 
            long lId = 1; 
            target.Load(lId);
        }

      
        [TestMethod()]
        public void TestgetPenny()
        {
            DBEntity entity = null; 
            Decimal expected = -1; 
            Decimal actual;
            actual = TreasurityViewModel.getPenny(entity);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestBudgetPFR()
        {
            TreasurityViewModel target = new TreasurityViewModel(); 
            Decimal expected = new Decimal(); 
            Decimal actual;
            target.BudgetPFR = expected;
            actual = target.BudgetPFR;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestKBK1()
        {
            TreasurityViewModel target = new TreasurityViewModel(); 
            Decimal expected = new Decimal(); 
            Decimal actual;
            target.KBK1 = expected;
            actual = target.KBK1;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestKBK2()
        {
            TreasurityViewModel target = new TreasurityViewModel(); 
            Decimal expected = new Decimal(); 
            Decimal actual;
            target.KBK2 = expected;
            actual = target.KBK2;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestMonthsList()
        {
            TreasurityViewModel target = new TreasurityViewModel(); 
            List<DBEntity> expected = null; 
            List<DBEntity> actual;
            target.MonthsList = expected;
            actual = target.MonthsList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPortfoliosDSV()
        {
            TreasurityViewModel target = new TreasurityViewModel(); 
            List<DBEntity> expected = null; 
            List<DBEntity> actual;
            target.PortfoliosDSV = expected;
            actual = target.PortfoliosDSV;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPortfoliosSPN()
        {
            TreasurityViewModel target = new TreasurityViewModel(); 
            List<DBEntity> expected = null; 
            List<DBEntity> actual;
            target.PortfoliosSPN = expected;
            actual = target.PortfoliosSPN;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedMonth()
        {
            TreasurityViewModel target = new TreasurityViewModel(); 
            DBEntity expected = null; 
            DBEntity actual;
            target.SelectedMonth = expected;
            actual = target.SelectedMonth;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedPortfolio1()
        {
            TreasurityViewModel target = new TreasurityViewModel(); 
            DBEntity expected = null; 
            DBEntity actual;
            target.SelectedPortfolio1 = expected;
            actual = target.SelectedPortfolio1;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedPortfolio2()
        {
            TreasurityViewModel target = new TreasurityViewModel(); 
            DBEntity expected = null; 
            DBEntity actual;
            target.SelectedPortfolio2 = expected;
            actual = target.SelectedPortfolio2;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestTotalPayment()
        {
            TreasurityViewModel target = new TreasurityViewModel(); 
            Decimal expected = new Decimal(); 
            Decimal actual;
            target.TotalPayment = expected;
            actual = target.TotalPayment;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestTotalPenny()
        {
            TreasurityViewModel target = new TreasurityViewModel(); 
            Decimal expected = new Decimal(); 
            Decimal actual;
            target.TotalPenny = expected;
            actual = target.TotalPenny;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestWholeYear()
        {
            TreasurityViewModel target = new TreasurityViewModel(); 
            Decimal expected = new Decimal(); 
            Decimal actual;
            target.WholeYear = expected;
            actual = target.WholeYear;
            Assert.AreEqual(expected, actual);
        }
    }
}
