﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Input;
using PFR_INVEST.ViewModels.ListItems;
using System.Collections.Generic;
using PFR_INVEST.Proxy;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestACCListViewModel : BaseTest
    {


        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefresh()
        {
            ACCListViewModel_Accessor target = new ACCListViewModel_Accessor(); 
            ICommand expected = null; 
            ICommand actual;
            target.Refresh = expected;
            actual = target.Refresh;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestList()
        {
            ACCListViewModel target = new ACCListViewModel(); 
            List<ACCListItem> expected = new List<ACCListItem>(); 
            List<ACCListItem> actual;
            target.List = expected;
            actual = target.List;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestCurrencyList()
        {
            ACCListViewModel target = new ACCListViewModel(); 
            List<DBEntity> expected = new List<DBEntity>();
            List<DBEntity> actual;
            target.CurrencyList = expected;
            actual = target.CurrencyList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshList()
        {
            ACCListViewModel_Accessor target = new ACCListViewModel_Accessor(); 
            target.ExecuteRefreshList();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefresh()
        {
            ACCListViewModel_Accessor target = new ACCListViewModel_Accessor(); 
            target.ExecuteRefresh();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshList()
        {
            ACCListViewModel_Accessor target = new ACCListViewModel_Accessor(); 
            bool actual;
            actual = target.CanExecuteRefreshList();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefresh()
        {
            ACCListViewModel_Accessor target = new ACCListViewModel_Accessor(); 
            bool actual;
            actual = target.CanExecuteRefresh();
        }

        [TestMethod()]
        public void TestACCListViewModelConstructor()
        {
            ACCListViewModel target = new ACCListViewModel();
        }
    }
}
