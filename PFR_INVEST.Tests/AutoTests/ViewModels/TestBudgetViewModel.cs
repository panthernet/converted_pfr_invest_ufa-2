﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;
using System.Collections.Generic;
using PFR_INVEST.ViewModels.ListItems;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestBudgetViewModel : BaseTest
    {



        [TestMethod()]
        public void TestBudgetViewModelConstructor()
        {
            BudgetViewModel target = new BudgetViewModel();
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            BudgetViewModel target = new BudgetViewModel();
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            BudgetViewModel_Accessor target = new BudgetViewModel_Accessor();
            target.ExecuteSave();
        }

        [TestMethod()]
        public void TestLoad()
        {
            BudgetViewModel target = new BudgetViewModel();
            long lId = 1;
            target.Load(lId);
            target.Load(0);
        }

        [TestMethod()]
        public void TestDopBudgets()
        {
            BudgetViewModel target = new BudgetViewModel();
            List<DBEntity> expected = null;
            List<DBEntity> actual;
            target.DopBudgets = expected;
            actual = target.DopBudgets;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDopBudgetsGrid()
        {
            BudgetViewModel target = new BudgetViewModel();
            List<BudgetAccurateListItem> expected = null;
            List<BudgetAccurateListItem> actual;
            target.DopBudgetsGrid = expected;
            actual = target.DopBudgetsGrid;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedYear()
        {
            BudgetViewModel target = new BudgetViewModel();
            DBEntity expected = null;
            DBEntity actual;
            target.SelectedYear = expected;
            actual = target.SelectedYear;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestYearsList()
        {
            BudgetViewModel target = new BudgetViewModel();
            List<DBEntity> expected = null;
            List<DBEntity> actual;
            target.YearsList = expected;
            actual = target.YearsList;
            Assert.AreEqual(expected, actual);
        }
    }
}
