﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ViewModels.ListItems;
using System.Windows.Input;
using PFR_INVEST.Proxy;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestPenaltyViewModel : BaseTest
    {


        [TestMethod()]
        public void TestPenaltyViewModelConstructor()
        {
            PenaltyViewModel target = new PenaltyViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteAddDopSPN()
        {
            PenaltyViewModel_Accessor target = new PenaltyViewModel_Accessor(); 
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteAddDopSPN();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            PenaltyViewModel target = new PenaltyViewModel(); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }


        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            PenaltyViewModel_Accessor target = new PenaltyViewModel_Accessor(); 
            target.ExecuteSave();
        }

       
        [TestMethod()]
        public void TestLoad()
        {
            PenaltyViewModel target = new PenaltyViewModel(); 
            long lId = 1; 
            target.Load(lId);
        }

      

        [TestMethod()]
        public void TestAccount()
        {
            PenaltyViewModel target = new PenaltyViewModel(); 
            PortfolioListItemFull expected = null; 
            PortfolioListItemFull actual;
            target.Account = expected;
            actual = target.Account;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestAddDopSPN()
        {
            PenaltyViewModel target = new PenaltyViewModel(); 
            ICommand actual;
            actual = target.AddDopSPN;
        }

        [TestMethod()]
        public void TestDopSPNs()
        {
            PenaltyViewModel target = new PenaltyViewModel(); 
            List<DBEntity> expected = null; 
            List<DBEntity> actual;
            target.DopSPNs = expected;
            actual = target.DopSPNs;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDopSPNsGrid()
        {
            PenaltyViewModel target = new PenaltyViewModel(); 
            List<DopSPNListItem> expected = null; 
            List<DopSPNListItem> actual;
            target.DopSPNsGrid = expected;
            actual = target.DopSPNsGrid;
            Assert.AreEqual(expected, actual);
        }

      
        [TestMethod()]
        public void TestMonthsList()
        {
            PenaltyViewModel target = new PenaltyViewModel(); 
            List<DBEntity> expected = null; 
            List<DBEntity> actual;
            target.MonthsList = expected;
            actual = target.MonthsList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectAccount()
        {
            PenaltyViewModel target = new PenaltyViewModel(); 
            ICommand actual;
            actual = target.SelectAccount;
        }

        [TestMethod()]
        public void TestSelectedMonth()
        {
            PenaltyViewModel target = new PenaltyViewModel(); 
            DBEntity expected = null; 
            DBEntity actual;
            target.SelectedMonth = expected;
            actual = target.SelectedMonth;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestType()
        {
            PenaltyViewModel target = new PenaltyViewModel(); 
            DUE_TYPE expected = new DUE_TYPE(); 
            DUE_TYPE actual;
            target.Type = expected;
            actual = target.Type;
            Assert.AreEqual(expected, actual);
        }
    }
}
