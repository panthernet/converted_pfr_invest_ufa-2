﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Input;
using PFR_INVEST.Proxy;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestZLRedistListViewModel : BaseTest
    {


        [TestMethod()]
        public void TestZLRedistListViewModelConstructor()
        {
            ZLRedistListViewModel target = new ZLRedistListViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshList()
        {
            ZLRedistListViewModel_Accessor target = new ZLRedistListViewModel_Accessor();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteRefreshList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshZLRedistList()
        {
            ZLRedistListViewModel_Accessor target = new ZLRedistListViewModel_Accessor();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteRefreshZLRedistList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshList()
        {
            ZLRedistListViewModel_Accessor target = new ZLRedistListViewModel_Accessor(); 
            target.ExecuteRefreshList();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshZLRedistList()
        {
            ZLRedistListViewModel_Accessor target = new ZLRedistListViewModel_Accessor(); 
            target.ExecuteRefreshZLRedistList();
        }

        [TestMethod()]
        public void TestRefreshZLRedistList()
        {
            ZLRedistListViewModel target = new ZLRedistListViewModel(); 
            ICommand actual;
            actual = target.RefreshZLRedistList;
        }

        [TestMethod()]
        public void TestZLRedistList()
        {
            ZLRedistListViewModel target = new ZLRedistListViewModel(); 
            DB2ZLCard[] expected = null; 
            DB2ZLCard[] actual;
            target.ZLRedistList = expected;
            actual = target.ZLRedistList;
            Assert.AreEqual(expected, actual);
        }
    }
}
