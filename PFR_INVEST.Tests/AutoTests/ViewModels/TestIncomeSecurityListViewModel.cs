﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ViewModels.ListItems;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestIncomeSecurityListViewModel : BaseTest
    {

        [TestMethod()]
        public void TestIncomeSecurityListViewModelConstructor()
        {
            IncomeSecurityListViewModel target = new IncomeSecurityListViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshList()
        {
            IncomeSecurityListViewModel_Accessor target = new IncomeSecurityListViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefreshList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshList()
        {
            IncomeSecurityListViewModel_Accessor target = new IncomeSecurityListViewModel_Accessor();
            target.ExecuteRefreshList();
        }

        [TestMethod()]
        public void TestIncomeSecList()
        {
            IncomeSecurityListViewModel target = new IncomeSecurityListViewModel();
            List<IncomeSecurityListItem> expected = null;
            List<IncomeSecurityListItem> actual;
            target.IncomeSecList = expected;
            actual = target.IncomeSecList;
            Assert.AreEqual(expected, actual);
        }
    }
}
