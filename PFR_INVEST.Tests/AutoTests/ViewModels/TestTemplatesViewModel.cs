﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestTemplatesViewModel : BaseTest
    {

        [TestMethod()]
        public void TestTemplatesViewModelConstructor()
        {
            TemplatesViewModel target = new TemplatesViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteBrowseTemplate()
        {
            TemplatesViewModel_Accessor target = new TemplatesViewModel_Accessor();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteBrowseTemplate();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteUploadTemplate()
        {
            TemplatesViewModel_Accessor target = new TemplatesViewModel_Accessor(); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteUploadTemplate();
            Assert.AreEqual(expected, actual);
        }


        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestBrowseTemplate()
        {
            TemplatesViewModel_Accessor target = new TemplatesViewModel_Accessor(); 
            ICommand expected = null; 
            ICommand actual;
            target.BrowseTemplate = expected;
            actual = target.BrowseTemplate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestGeneratedQuery()
        {
            TemplatesViewModel target = new TemplatesViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.GeneratedQuery = expected;
            actual = target.GeneratedQuery;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedFilePath()
        {
            TemplatesViewModel target = new TemplatesViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.SelectedFilePath = expected;
            actual = target.SelectedFilePath;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestUploadTemplate()
        {
            TemplatesViewModel_Accessor target = new TemplatesViewModel_Accessor(); 
            ICommand expected = null; 
            ICommand actual;
            target.UploadTemplate = expected;
            actual = target.UploadTemplate;
            Assert.AreEqual(expected, actual);
        }
    }
}
