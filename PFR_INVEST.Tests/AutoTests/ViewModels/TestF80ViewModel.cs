﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ViewModels.ListItems;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestF80ViewModel : BaseTest
    {


        [TestMethod()]
        public void TestF80ViewModelConstructor()
        {
            long id = 1; 
            F80ViewModel target = new F80ViewModel(id);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            long id = 1; 
            F80ViewModel target = new F80ViewModel(id); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteSave()
        //{
        //    PrivateObject param0 = null; 
        //    F80ViewModel_Accessor target = new F80ViewModel_Accessor(param0); 
        //    target.ExecuteSave();
        //}

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefreshLists()
        {
            long id = 1;
            F80ViewModel target = new F80ViewModel(id); 
            target.RefreshLists();
        }

       

        [TestMethod()]
        public void TestInfoList()
        {
            long id = 1; 
            F80ViewModel target = new F80ViewModel(id); 
            List<F80InfoListItem> expected = null; 
            List<F80InfoListItem> actual;
            target.InfoList = expected;
            actual = target.InfoList;
            Assert.AreEqual(expected, actual);
        }

       
    }
}
