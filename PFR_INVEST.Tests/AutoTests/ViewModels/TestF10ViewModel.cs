﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ViewModels.ListItems;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestF10ViewModel : BaseTest
    {


        [TestMethod()]
        public void TestF10ViewModelConstructor()
        {
            long id = 1;
            F10ViewModel target = new F10ViewModel(id);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            long id = 1;
            F10ViewModel target = new F10ViewModel(id);
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteSave()
        //{
        //    PrivateObject param0 = null;
        //    F10ViewModel_Accessor target = new F10ViewModel_Accessor(param0);
        //    target.ExecuteSave();
        //}

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefreshNetWealthsList()
        {
            long id = 1;
            F10ViewModel target = new F10ViewModel(id);
            target.RefreshNetWealthsList();
        }

        [TestMethod()]
        public void TestNetWealthsList()
        {
            long id = 1;
            F10ViewModel target = new F10ViewModel(id);
            List<NetWealthsInnerListItem> expected = null;
            List<NetWealthsInnerListItem> actual;
            target.NetWealthsList = expected;
            actual = target.NetWealthsList;
            Assert.AreEqual(expected, actual);
        }
    }
}
