﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ViewModels.ListItems;
using PFR_INVEST.Proxy;
using System.Collections.Generic;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestTransferViewModel : BaseTest
    {


        [TestMethod()]
        public void TestTransferViewModelConstructor()
        {
            TransferViewModel target = new TransferViewModel();
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            TransferViewModel target = new TransferViewModel(); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteSelectAccount()
        {
            TransferViewModel_Accessor target = new TransferViewModel_Accessor();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteSelectAccount();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            TransferViewModel_Accessor target = new TransferViewModel_Accessor(); 
            target.ExecuteSave();
        }

     
        [TestMethod()]
        public void TestLoad()
        {
            TransferViewModel target = new TransferViewModel(); 
            long lId = 1; 
            target.Load(lId);
        }

       

        [TestMethod()]
        public void TestAccount()
        {
            TransferViewModel target = new TransferViewModel(); 
            PortfolioListItemFull expected = null; 
            PortfolioListItemFull actual;
            target.Account = expected;
            actual = target.Account;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestCurrencyList()
        {
            TransferViewModel target = new TransferViewModel(); 
            List<DBEntity> expected = null; 
            List<DBEntity> actual;
            target.CurrencyList = expected;
            actual = target.CurrencyList;
            Assert.AreEqual(expected, actual);
        }

        
        [TestMethod()]
        public void TestRate()
        {
            TransferViewModel target = new TransferViewModel(); 
            Decimal expected = new Decimal(); 
            Decimal actual;
            target.Rate = expected;
            actual = target.Rate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectAccount()
        {
            TransferViewModel target = new TransferViewModel(); 
            ICommand actual;
            actual = target.SelectAccount;
        }

        [TestMethod()]
        public void TestSelectedCurrency()
        {
            TransferViewModel target = new TransferViewModel(); 
            DBEntity expected = null; 
            DBEntity actual;
            target.SelectedCurrency = expected;
            actual = target.SelectedCurrency;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSumm()
        {
            TransferViewModel target = new TransferViewModel(); 
            Decimal expected = new Decimal(); 
            Decimal actual;
            target.Summ = expected;
            actual = target.Summ;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestTotal()
        {
            TransferViewModel target = new TransferViewModel(); 
            string actual;
            actual = target.Total;
        }
    }
}
