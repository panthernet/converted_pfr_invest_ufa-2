﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestBanksListViewModel : BaseTest
    {



        [TestMethod()]
        public void TestBanksListViewModelConstructor()
        {
            BanksListViewModel target = new BanksListViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefresh()
        {
            BanksListViewModel_Accessor target = new BanksListViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefresh();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshList()
        {
            BanksListViewModel_Accessor target = new BanksListViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefreshList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefresh()
        {
            BanksListViewModel_Accessor target = new BanksListViewModel_Accessor();
            target.ExecuteRefresh();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshList()
        {
            BanksListViewModel_Accessor target = new BanksListViewModel_Accessor();
            target.ExecuteRefreshList();
        }

        [TestMethod()]
        public void TestBanksList()
        {
            BanksListViewModel target = new BanksListViewModel();
            List<BankListItem> expected = null;
            List<BankListItem> actual;
            target.BanksList = expected;
            actual = target.BanksList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefresh()
        {
            BanksListViewModel_Accessor target = new BanksListViewModel_Accessor();
            ICommand expected = null;
            ICommand actual;
            target.Refresh = expected;
            actual = target.Refresh;
            Assert.AreEqual(expected, actual);
        }
    }
}
