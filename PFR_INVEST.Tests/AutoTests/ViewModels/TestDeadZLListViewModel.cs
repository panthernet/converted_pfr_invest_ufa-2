﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestDeadZLListViewModel : BaseTest
    {




        [TestMethod()]
        public void TestDeadZLListViewModelConstructor()
        {
            DeadZLListViewModel target = new DeadZLListViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshDeadZLList()
        {
            DeadZLListViewModel_Accessor target = new DeadZLListViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefreshDeadZLList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshList()
        {
            DeadZLListViewModel_Accessor target = new DeadZLListViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefreshList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshDeadZLList()
        {
            DeadZLListViewModel_Accessor target = new DeadZLListViewModel_Accessor();
            target.ExecuteRefreshDeadZLList();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshList()
        {
            DeadZLListViewModel_Accessor target = new DeadZLListViewModel_Accessor();
            target.ExecuteRefreshList();
        }

        [TestMethod()]
        public void TestDeadZLList_NPF()
        {
            DeadZLListViewModel target = new DeadZLListViewModel();
            DB2DeadZL_NPFName[] expected = null;
            DB2DeadZL_NPFName[] actual;
            target.DeadZLList_NPF = expected;
            actual = target.DeadZLList_NPF;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefreshDeadZLList()
        {
            DeadZLListViewModel_Accessor target = new DeadZLListViewModel_Accessor();
            ICommand expected = null;
            ICommand actual;
            target.RefreshDeadZLList = expected;
            actual = target.RefreshDeadZLList;
            Assert.AreEqual(expected, actual);
        }
    }
}
