﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestBuyReportsListViewModel : BaseTest
    {



        [TestMethod()]
        public void TestBuyReportsListViewModelConstructor()
        {
            BuyReportsListViewModel target = new BuyReportsListViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefresh()
        {
            BuyReportsListViewModel_Accessor target = new BuyReportsListViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefresh();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshList()
        {
            BuyReportsListViewModel_Accessor target = new BuyReportsListViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefreshList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefresh()
        {
            BuyReportsListViewModel_Accessor target = new BuyReportsListViewModel_Accessor();
            target.ExecuteRefresh();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshList()
        {
            BuyReportsListViewModel_Accessor target = new BuyReportsListViewModel_Accessor();
            target.ExecuteRefreshList();
        }

        [TestMethod()]
        public void TestList()
        {
            BuyReportsListViewModel target = new BuyReportsListViewModel();
            List<SellReportItem> expected = null;
            List<SellReportItem> actual;
            target.List = expected;
            actual = target.List;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRefresh()
        {
            BuyReportsListViewModel target = new BuyReportsListViewModel();
            ICommand actual;
            actual = target.Refresh;
        }
    }
}
