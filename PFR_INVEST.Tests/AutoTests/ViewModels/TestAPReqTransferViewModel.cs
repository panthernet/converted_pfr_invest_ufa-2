﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestAPReqTransferViewModel : BaseTest
    {


        [TestMethod()]
        public void TestShowPP()
        {
            long id = 1; 
            VIEWMODEL_STATES state = VIEWMODEL_STATES.Edit; 
            APReqTransferViewModel target = new APReqTransferViewModel(id, state); 
            Visibility actual;
            actual = target.ShowPP;
        }

        [TestMethod()]
        public void TestShowControl()
        {
            long id = 1;
            VIEWMODEL_STATES state = VIEWMODEL_STATES.Edit; 
            APReqTransferViewModel target = new APReqTransferViewModel(id, state); 
            Visibility actual;
            actual = target.ShowControl;
        }

        [TestMethod()]
        public void TestRegisterInfo()
        {
            long id = 1;
            VIEWMODEL_STATES state = VIEWMODEL_STATES.Edit; 
            APReqTransferViewModel target = new APReqTransferViewModel(id, state); 
            string actual;
            actual = target.RegisterInfo;
        }

      
        [TestMethod()]
        public void TestIsReadOnly()
        {
            long id = 1;
            VIEWMODEL_STATES state = VIEWMODEL_STATES.Edit; 
            APReqTransferViewModel target = new APReqTransferViewModel(id, state); 
            bool actual;
            actual = target.IsReadOnly;
        }

        [TestMethod()]
        public void TestControlDate()
        {
            long id = 1;
            VIEWMODEL_STATES state = VIEWMODEL_STATES.Edit; 
            APReqTransferViewModel target = new APReqTransferViewModel(id, state); 
            DateTime expected = new DateTime(2010,1,13); 
            DateTime actual;
            target.ControlDate = expected;
            actual = target.ControlDate;
            Assert.AreEqual(expected, actual);
        }

       

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefreshParentViewModel()
        {
            Type viewModelList = null;
            APReqTransferViewModel.RefreshParentViewModel(viewModelList);
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteSave()
        //{
        //    PrivateObject param0 = null; 
        //    APReqTransferViewModel_Accessor target = new APReqTransferViewModel_Accessor(param0); 
        //    target.ExecuteSave();
        //}

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            long id = 1;
            VIEWMODEL_STATES state = VIEWMODEL_STATES.Edit; 
            APReqTransferViewModel target = new APReqTransferViewModel(id, state); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

    }
}
