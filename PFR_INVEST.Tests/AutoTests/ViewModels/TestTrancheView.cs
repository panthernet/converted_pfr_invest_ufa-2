﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestTrancheView : BaseTest
    {

        [TestMethod()]
        public void TestTrancheViewConstructor()
        {
            TrancheView target = new TrancheView();
        }

        [TestMethod()]
        public void TestCount()
        {
            TrancheView target = new TrancheView(); 
            decimal expected = 12; 
            decimal actual;
            target.Count = expected.ToString();
            actual = Convert.ToDecimal(target.Count);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDate()
        {
            TrancheView target = new TrancheView(); 
            string expected = string.Empty; 
            string actual;
            target.Date = expected;
            actual = target.Date;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestFinDate()
        {
            TrancheView target = new TrancheView(); 
            string expected = string.Empty; 
            string actual;
            target.FinDate = expected;
            actual = target.FinDate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestFinNum()
        {
            TrancheView target = new TrancheView(); 
            string expected = string.Empty; 
            string actual;
            target.FinNum = expected;
            actual = target.FinNum;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestNum()
        {
            TrancheView target = new TrancheView(); 
            int expected = 0; 
            int actual;
            target.Num = expected;
            actual = target.Num;
            Assert.AreEqual(expected, actual);
        }
    }
}
