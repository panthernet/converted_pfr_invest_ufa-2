﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;
using System.Collections.Generic;
using System.Windows.Input;
using System.Windows;
using PFR_INVEST.ViewModels.ListItems;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestOrderViewModel : BaseTest
    {

        [TestMethod()]
        public void TestOrderViewModelConstructor()
        {
            OrderViewModel target = new OrderViewModel();
        }


        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteAddSecurity()
        {
            bool expected = true; 
            bool actual;
            actual = OrderViewModel_Accessor.CanExecuteAddSecurity();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRemoveSecurity()
        {
            OrderViewModel_Accessor target = new OrderViewModel_Accessor(); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteRemoveSecurity();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            OrderViewModel target = new OrderViewModel(); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRemoveSecurity()
        {
            OrderViewModel_Accessor target = new OrderViewModel_Accessor(); 
            target.ExecuteRemoveSecurity();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            OrderViewModel_Accessor target = new OrderViewModel_Accessor(); 
            target.ExecuteSave();
        }

        [TestMethod()]
        public void TestGetType()
        {
            OrderViewModel target = new OrderViewModel(); 
            Dictionary<string, DBField> sec = null;
            SECURITY_IN_ORDER_TYPE expected = SECURITY_IN_ORDER_TYPE.Unknown;
            SECURITY_IN_ORDER_TYPE actual;
            actual = target.GetType(sec);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestLoad()
        {
            OrderViewModel target = new OrderViewModel(); 
            long lId = 1; 
            target.Load(lId);
        }

       

        [TestMethod()]
        public void TestfillAccountNums()
        {
            OrderViewModel target = new OrderViewModel(); 
            target.fillAccountNums();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestgetOrderNum()
        {
            OrderViewModel target = new OrderViewModel(); 
            string expected = string.Empty; 
            string actual;
            actual = target.getOrderNum();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestAccountDepo()
        {
            OrderViewModel target = new OrderViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.AccountDepo = expected;
            actual = target.AccountDepo;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestAccountTorg()
        {
            OrderViewModel target = new OrderViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.AccountTorg = expected;
            actual = target.AccountTorg;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestAddSecurity()
        {
            OrderViewModel_Accessor target = new OrderViewModel_Accessor(); 
            ICommand expected = null; 
            ICommand actual;
            target.AddSecurity = expected;
            actual = target.AddSecurity;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestBankAccounts()
        {
            OrderViewModel target = new OrderViewModel(); 
            List<DB2LegalEntityCard> expected = null; 
            List<DB2LegalEntityCard> actual;
            target.BankAccounts = expected;
            actual = target.BankAccounts;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestBuyCurrencyVisible()
        {
            OrderViewModel target = new OrderViewModel(); 
            Visibility actual;
            actual = target.BuyCurrencyVisible;
        }

        [TestMethod()]
        public void TestBuyRubAuctionVisible()
        {
            OrderViewModel target = new OrderViewModel(); 
            Visibility actual;
            actual = target.BuyRubAuctionVisible;
        }

        [TestMethod()]
        public void TestBuyRubSecondMarketVisible()
        {
            OrderViewModel target = new OrderViewModel(); 
            Visibility actual;
            actual = target.BuyRubSecondMarketVisible;
        }

        [TestMethod()]
        public void TestCommissioners()
        {
            OrderViewModel target = new OrderViewModel(); 
            List<string> expected = null; 
            List<string> actual;
            target.Commissioners = expected;
            actual = target.Commissioners;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDepoCurrencyID()
        {
            OrderViewModel target = new OrderViewModel(); 
            long expected = 0; 
            long actual;
            target.DepoCurrencyID = expected;
            actual = target.DepoCurrencyID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestFioList()
        {
            OrderViewModel target = new OrderViewModel(); 
            List<DBEntity> expected = null; 
            List<DBEntity> actual;
            target.FioList = expected;
            actual = target.FioList;
            Assert.AreEqual(expected, actual);
        }

      
        [TestMethod()]
        public void TestLastSecurityName()
        {
            OrderViewModel target = new OrderViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.LastSecurityName = expected;
            actual = target.LastSecurityName;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestOrderNum()
        {
            OrderViewModel target = new OrderViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.OrderNum = expected;
            actual = target.OrderNum;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPlaces()
        {
            OrderViewModel target = new OrderViewModel(); 
            List<string> expected = null; 
            List<string> actual;
            target.Places = expected;
            actual = target.Places;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPortfolios()
        {
            OrderViewModel target = new OrderViewModel(); 
            List<DB2Portfolio> expected = null; 
            List<DB2Portfolio> actual;
            target.Portfolios = expected;
            actual = target.Portfolios;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPostList()
        {
            OrderViewModel target = new OrderViewModel(); 
            List<DBEntity> expected = null; 
            List<DBEntity> actual;
            target.PostList = expected;
            actual = target.PostList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRemoveSecurity()
        {
            OrderViewModel_Accessor target = new OrderViewModel_Accessor(); 
            ICommand expected = null; 
            ICommand actual;
            target.RemoveSecurity = expected;
            actual = target.RemoveSecurity;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSecurities()
        {
            OrderViewModel target = new OrderViewModel(); 
            List<DBEntity> expected = new List<DBEntity>(); 
            List<DBEntity> actual;
            target.Securities = expected;
            actual = target.Securities;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSecuritiesGrid()
        {
            OrderViewModel target = new OrderViewModel(); 
            List<SecurityInOrderListItem> expected = null; 
            List<SecurityInOrderListItem> actual;
            target.SecuritiesGrid = expected;
            actual = target.SecuritiesGrid;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSecuritiesNotExist()
        {
            OrderViewModel target = new OrderViewModel(); 
            bool expected = false; 
            bool actual;
            target.SecuritiesNotExist = expected;
            actual = target.SecuritiesNotExist;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedBankAccount()
        {
            OrderViewModel target = new OrderViewModel(); 
            DB2LegalEntityCard expected = null; 
            DB2LegalEntityCard actual;
            target.SelectedBankAccount = expected;
            actual = target.SelectedBankAccount;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedCommissioner()
        {
            OrderViewModel target = new OrderViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.SelectedCommissioner = expected;
            actual = target.SelectedCommissioner;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedFio()
        {
            OrderViewModel target = new OrderViewModel(); 
            DBEntity expected = null; 
            DBEntity actual;
            target.SelectedFio = expected;
            actual = target.SelectedFio;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedItem()
        {
            OrderViewModel target = new OrderViewModel(); 
            SecurityInOrderListItem expected = null; 
            SecurityInOrderListItem actual;
            target.SelectedItem = expected;
            actual = target.SelectedItem;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedPlace()
        {
            OrderViewModel target = new OrderViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.SelectedPlace = expected;
            actual = target.SelectedPlace;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedPortfolio()
        {
            OrderViewModel target = new OrderViewModel(); 
            DB2Portfolio expected = null; 
            DB2Portfolio actual;
            target.SelectedPortfolio = expected;
            actual = target.SelectedPortfolio;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedPost()
        {
            OrderViewModel target = new OrderViewModel(); 
            DBEntity expected = null; 
            DBEntity actual;
            target.SelectedPost = expected;
            actual = target.SelectedPost;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedStatus()
        {
            OrderViewModel target = new OrderViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.SelectedStatus = expected;
            actual = target.SelectedStatus;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedType()
        {
            OrderViewModel target = new OrderViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.SelectedType = expected;
            actual = target.SelectedType;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSellCurrencyVisible()
        {
            OrderViewModel target = new OrderViewModel(); 
            Visibility actual;
            actual = target.SellCurrencyVisible;
        }

        [TestMethod()]
        public void TestSellRubVisible()
        {
            OrderViewModel target = new OrderViewModel(); 
            Visibility actual;
            actual = target.SellRubVisible;
        }

        [TestMethod()]
        public void TestStartDate()
        {
            OrderViewModel target = new OrderViewModel(); 
            DateTime expected = new DateTime(); 
            DateTime actual;
            target.StartDate = expected;
            actual = target.StartDate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestStartTime()
        {
            OrderViewModel target = new OrderViewModel(); 
            TimeSpan expected = new TimeSpan(DateTime.Now.Ticks); 
            TimeSpan actual;
            target.StartTime = expected;
            actual = target.StartTime;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestStatuses()
        {
            OrderViewModel target = new OrderViewModel(); 
            List<string> expected = null; 
            List<string> actual;
            target.Statuses = expected;
            actual = target.Statuses;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestType()
        {
            OrderViewModel target = new OrderViewModel(); 
            SECURITY_IN_ORDER_TYPE expected = new SECURITY_IN_ORDER_TYPE(); 
            SECURITY_IN_ORDER_TYPE actual;
            target.Type = expected;
            actual = target.Type;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestTypes()
        {
            OrderViewModel target = new OrderViewModel(); 
            List<string> expected = null; 
            List<string> actual;
            target.Types = expected;
            actual = target.Types;
            Assert.AreEqual(expected, actual);
        }
    }
}
