﻿using System.Runtime.Serialization;
using db2connector.Contract.ActiveDirectory.MessageBase;

namespace db2connector.Contract.ActiveDirectory.Messages
{
    [DataContract(Namespace = ActiveDirectorySettingsHelper.ServiceNamespace)]
    public class UserRequest : RequestBase
    {
        [DataMember]
        public ADUser User { get; set; }
    }
}