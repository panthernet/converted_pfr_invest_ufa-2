﻿using System.Runtime.Serialization;
using db2connector.Contract.ActiveDirectory.MessageBase;

namespace db2connector.Contract.ActiveDirectory.Messages
{
    [DataContract(Namespace = ActiveDirectorySettingsHelper.ServiceNamespace)]
    public class TokenResponse : ResponseBase
    {
        [DataMember]
        public string AccessToken;
    }
}
