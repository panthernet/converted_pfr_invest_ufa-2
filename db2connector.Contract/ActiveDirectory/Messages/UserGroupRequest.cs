﻿using System.Runtime.Serialization;
using db2connector.Contract.ActiveDirectory.MessageBase;

namespace db2connector.Contract.ActiveDirectory.Messages
{
    [DataContract(Namespace = ActiveDirectorySettingsHelper.ServiceNamespace)]
    public class UserGroupRequest : RequestBase
    {
        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string GroupName { get; set; }
    }
}