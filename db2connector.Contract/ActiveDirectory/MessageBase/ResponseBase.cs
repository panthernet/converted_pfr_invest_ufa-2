﻿using System;
using System.Reflection;
using System.Runtime.Serialization;

namespace db2connector.Contract.ActiveDirectory.MessageBase
{
    [DataContract(Namespace = ActiveDirectorySettingsHelper.ServiceNamespace)]
    public abstract class ResponseBase
    {
        [DataMember]
        public AcknowledgeType Acknowledge = AcknowledgeType.Success;

        [DataMember]
        public string CorrelationId;

        [DataMember]
        public string Message;

        [DataMember]
        public string Version =
            String.Format("{0}.{1}", Assembly.GetExecutingAssembly().GetName().Version.Major, Assembly.GetExecutingAssembly().GetName().Version.Minor);

        [DataMember]
        public string Build = Assembly.GetExecutingAssembly().GetName().Version.Build.ToString();

        [DataMember]
        public int RowsAffected;
    }
}
