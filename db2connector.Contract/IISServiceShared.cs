﻿using System.Collections.Generic;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataObjects;

namespace db2connector
{
    public interface IISServiceShared
    {
        PFR_INVEST.DataObjects.Contract GetContractByDogovorNum(string dogovorNum);

        ActionResult ImportUKF401(EdoOdkF401402 requirement, List<F401402UK> details, EDOLog edoLog, string xmlBody);

        ActionResult ImportUKF402(List<F401402UK> details, EDOLog edoLog, string xmlBody);
    }
}
