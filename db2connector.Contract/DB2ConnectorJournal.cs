﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using PFR_INVEST.Common.Logger;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Journal;

namespace db2connector
{
    //[ServiceKnownType(typeof(JournalEventType))]
    //[ServiceKnownType(typeof(JournalFilter))]
    /// <summary>
    /// Часть интерфейса, отвечающая за работу с журналированием
    /// </summary>
    public partial interface IDB2Connector
    {
        string DomainUser { get; set; }
        string OsUser { get; set; }
        string HostName { get; set; }

        [OperationContract]
        string GetCurrentUserLogin();

        [OperationContract]
        void SaveJornalLog(JournalEventType code, string name, long? documentId, string otype);        

        [OperationContract]
        void SaveJornalLogWithAdditionalInfo(JournalEventType code, string name, long? documentId, string otype, string additionalInfo);

        /// <summary>
        /// Сохранение детального события по действию с карточкой объекта
        /// </summary>
        /// <param name="type">Тип события</param>
        /// <param name="message">Сообщение</param>
        /// <param name="documentId">ID документа</param>
        /// <param name="otype">Наименование типа модели</param>
        /// <param name="additionalInfo">Дополнительная текстовая информация</param>
        /// <param name="entityType">Тип объекта карточки для получения доп. информации на стороне сервиса</param>
        /// <param name="entityDesc">Кастомное описание для модели, например, когда невозможно определить тип объекта данных или если используется композитна таблица</param>
        [OperationContract]
        void SaveDetailedJornalLog(JournalEventType type, string message, long? documentId, string otype, string additionalInfo, string entityType, string entityDesc = null);

        [OperationContract]
        IList<Audit> GetJournalLog(JournalFilter filter);

        [OperationContract]
        IList<string> GetJournalEventObjectList();

        [OperationContract]
        bool SetCurrentUserInformation(string domainUser, string osUser, string hostName);

        [OperationContract]
        void SysLog(string userName, string eventName, string resultName, string detailMessage, string hostName, LogSeverity severity = LogSeverity.Info);
    }
}
