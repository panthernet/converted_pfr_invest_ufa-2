﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using db2connector.Contract;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.DataObjects.Proxy;
using PFR_INVEST.Proxy;
#if !WEBCLIENT
using db2connector.Contract;
using PFR_INVEST.Proxy.DataContracts;
#endif

namespace db2connector
{
    /// <summary>
    /// Основная часть интерфейса
    /// </summary>
    [ServiceContract(SessionMode = SessionMode.Required)] //, CallbackContract = typeof(IClientCallback)
	public partial interface IDB2Connector
    {
        [OperationContract]
		int GetMetricActiveUser();

		[OperationContract]
		bool GetMetricDecimalFix();

        /// <summary>
        /// Финальное закрытие соединения с сервисом
        /// </summary>
        [OperationContract]
        void CloseConnection();

		[OperationContract]
        ServiceData GetServiceVersion();

		[OperationContract]
		string GetServiceType();

		[OperationContract]
		void DeleteAPS(long id);

		[OperationContract]
		void DeleteDopAPSList(List<long> ids);

		[OperationContract]
		ServerSettings OpenDBConnection();

        [OperationContract]
        ServerSettings GetServerSettings();

        [OperationContract]
		Dictionary<string, DBField> GetF40(long id);

		[OperationContract]
		Dictionary<string, DBField> GetF50(long id);

		[OperationContract]
		Dictionary<string, DBField> GetF60(long id);

        [OperationContract]
        long AddRate(Curs rate);

        [OperationContract]
		List<ERZL> GetERZLList();

		[OperationContract]
		Dictionary<string, DBField> GetF70(long id);

		[OperationContract]
		List<TransferFromOrToNPFListItem> GetRegistersListPPNotEntered(bool pfRtoNPF, long? finregisterID = null);

		[OperationContract]
		List<Portfolio> GetSPNAllocatedPortfolios();

		[OperationContract]
		List<FinregisterListItem> GetSPNAllocatedFinregisters(long portfolioID, string content = null);

		[OperationContract]
		List<FinregisterListItem> GetSPNReturnedFinregisters(long portfolioID, string content = null);

		[OperationContract]
		List<PFRAccountsListItem> GetPFRAccountsList();

		[OperationContract]
		List<FZ> GetFZList();

		[OperationContract]
		long GetCurrencyID(string name);

		[OperationContract]
		long GetCountCostsListFiltered(DateTime? from, DateTime? to);

		[OperationContract]
		IList<Cost> GetCostsListFilteredByPage(int startIndex, DateTime? from, DateTime? to);

		[OperationContract]
		void RecalcUKPortfoliosForContract(long contractID);

		[OperationContract]
		decimal GetTotalInvestDohodForSIRegister(long id);

		[OperationContract]
		bool IsUseAccountInBalanceList(long accountID);

		[OperationContract]
		decimal GetROPSBalance(DateTime? onDate = null);

		[OperationContract]
		long GetCountBalanceList(List<SaldoParts> parts, DateTime? periodStart, DateTime? periodEnd);

		[OperationContract]
		List<PFR_INVEST.Proxy.DataContracts.DB2BalanceListItem> GetBalanceListByPage(List<SaldoParts> parts, int startIndex, DateTime? periodStart, DateTime? periodEnd);

		[OperationContract]
		List<PFR_INVEST.Proxy.DataContracts.DB2BalanceListItem> GetBalanceListByID(List<SaldoParts> parts, long id, DateTime? periodStart, DateTime? periodEnd);

		[OperationContract]
		int GetMaxAuctionNumDepClaimSelectParams(int year);

		[OperationContract]
		DepClaimSelectParams SelectLastDepClaimSelectParams(long stockID);

		[OperationContract]
		int GetCountSelectDateDepClaimSelectParams(DateTime date);

		[OperationContract]
		bool IncludedToThe761List(string bankCode, DateTime date);

		[OperationContract]
		List<OwnFundsListItem> GetOwnedFoundsListHibPrevious();

		[OperationContract]
		bool DepositReturn(List<DepositReturnListItem> xL);

		[OperationContract]
		EdoOdkF140 GetEdoOdkF140ByReportData(long contractId, long categoryId, DateTime? theDate);

		[OperationContract]
		List<LegalEntity> GetBankListByStockCode(string stockCode);

		[OperationContract]
		PaymentHistoryItem GetPaymentHistoryItem(long id);

		[OperationContract]
		List<ModelContactRecord> GetModelContactRecords();

		[OperationContract]
		List<OrdReport> GetReportByOrderType(string orderType, int? limit = null);

		[OperationContract]
		bool UpdateTransferByActSigned(
			PFR_INVEST.DataObjects.Contract contract,
			ReqTransfer transfer, bool isTransferFromPFRToUK,
			TransferStatusIdentifier.ActionTransfer action);

		[OperationContract]
		List<F401402UKListItem> GetF401402ListByID(Document.Types types, long id);

		[OperationContract]
		long GetCommonPPCount(DateTime? periodStart, DateTime? periodEnd, bool isOpfr);

		[OperationContract]
		List<PPListItem> GetCommonPPByPage(long startIndex, DateTime? periodStart, DateTime? periodEnd, bool isOpfr);

		[OperationContract]
		PPListItem GetCommonPPByID(long id, DateTime? periodStart, DateTime? periodEnd);

		[OperationContract]
		long GetPaymentOrderCount(DateTime? periodStart, DateTime? periodEnd);

		[OperationContract]
		List<PaymentOrderListItem> GetPaymentOrderByPage(long startIndex, DateTime? periodStart, DateTime? periodEnd);

		[OperationContract]
		PaymentOrderListItem GetPaymentOrderByID(long id, DateTime? periodStart, DateTime? periodEnd);

        /*Настройка включения/отключения оповещений контроля сроков*/

        [OperationContract]
        bool IsInfoAlertsEnabled();
    }
}
