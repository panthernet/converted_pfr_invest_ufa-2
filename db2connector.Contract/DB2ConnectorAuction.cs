﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.DataObjects.XMLModel;
using PFR_INVEST.DataObjects.XMLModel.Auction;

namespace db2connector
{
	
	/// <summary>
	/// Часть интерфейса, отвечающая за работу с Аукционами
	/// </summary>	
	public partial interface IDB2Connector
	{
		
		/// <summary>
		/// Экспорт уведомления о параметрах отбора заявок
		/// </summary>		
		[OperationContract]
		XMLFileResult ExportAuctionInfo(long auctionID);

		/// <summary>
		/// Лимиты для банков на дату аукциона
		/// </summary>
		/// <param name="auctionID">ID аукциона</param>
		[OperationContract]
		List<BankLimitListItem> GetBanksLimit(long auctionID);


		/// <summary>
		/// Экспорт лимитов для банков
		/// </summary>		
		[OperationContract]
		XMLFileResult ExportBankLimitsInfo(long auctionID);

		/// <summary>
		/// Экспорт ставки отсечения
		/// </summary>		
		[OperationContract]
		XMLFileResult ExportAuctionCutoffRate(long auctionID);
	}
}
