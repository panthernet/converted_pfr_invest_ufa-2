﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using PFR_INVEST.DataObjects.XMLModel;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;
using db2connector.Contract;
using PFR_INVEST.Core.BusinessLogic;

namespace db2connector
{
	public partial interface IDB2Connector
	{
		#region ForModels

		[OperationContract]
		List<KipLog> GetKIPLogList(DateTime from, DateTime to);

		[OperationContract]
		List<KIPRegisterListItem> GetKIPRegisterList(DateTime from, DateTime to);

        #endregion ForModels

        ////[OperationContract]
        ////void TestKIP(byte[] file);


        [OperationContract]
		ActionResult<string[]> GetKIPFilesForImport();

		[OperationContract]
		ActionResult<string> LoadKIPFileFromServer(string fileName);


		[OperationContract]
		ActionResult<string> LoadKIPZipFile(byte[] file);

		[OperationContract]
		ActionResult<string> LoadKIPFile(KIPSettings settings, byte[] file, bool gzip = false);

		[OperationContract]
		List<string> GetKIPExportReadyList();

		[OperationContract]
		byte[] GetKIPExportZipFile(string DocNumber);

		[OperationContract]
		XMLFileResult GetKIPExportFile(string DocNumber, bool onlyServerExport = false);

		[OperationContract]
		void SetKIPRegisterExportStatus(string docNumber, KipRegister.ExportStatuses status);

        [OperationContract]
        KIPSettings GetKIPSettings();
    }
}
