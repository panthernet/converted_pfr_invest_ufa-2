﻿using System;

namespace db2connector.Contract
{
    public class NotUserException : Exception
    {
    }

    public class AuthInitException : Exception
    {
        public Exception Ex { get; set; }
    }

    public class InvalidLoginOrPasswordException : Exception
    {
        public InvalidLoginOrPasswordException(string message = null) : base(message) { }
    }

    public class UserMustChangePasswordException : Exception
    {
        public UserMustChangePasswordException(string message = null) : base(message) { }
    }
}
