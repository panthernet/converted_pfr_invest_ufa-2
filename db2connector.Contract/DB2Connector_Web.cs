﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace db2connector
{
    public partial interface IDB2Connector
    {
#if WEBCLIENT
        [OperationContract]
        System.Linq.IQueryable WebLazyGetSpnRegistersArchiveList();
#endif
    }
}
