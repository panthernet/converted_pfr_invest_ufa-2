﻿

namespace db2connector.Contract.DataContracts
{
    using System;

    using PFR_INVEST.Core.BusinessLogic;
    using PFR_INVEST.DataAccess.Server.DataObjects;

    public class ImportF026UKAsyncResult : AsyncResult
    {
        private EdoOdkF026Hib input;
        private ActionResult result;

        public ImportF026UKAsyncResult(EdoOdkF026Hib input, AsyncCallback callback, object state)
            : base(callback, state)
        {
            this.input = input;
        }

        public EdoOdkF026Hib Input
        {
            get { return input; }
            set { input = value; }
        }
        public Exception Exception { get; set; }
        public ActionResult Result
        {
            get { return result; }
            set { result = value; }
        }
    }
}
