﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace PFR_INVEST.Proxy
{
    [DataContract]
    public class DB2Status
    {
        #region Fields
        private long id = 0;
        private string status = "";

        #endregion

        #region Properties

        [DataMember]
        public long ID
        {
            get { return id; }
            set { id = value; }
        }

        [DataMember]
        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        #endregion
    }
}
