﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace PFR_INVEST.Proxy.DataContracts
{
	[DataContract]
	public class DB2AccOperationListItem
	{
		[DataMember]
		public long ID { get; set; }

		[DataMember]
		public string PORTFOLIO { get; set; }

		[DataMember]
		public string ACCOUNT_TYPE { get; set; }

		[DataMember]
		public string LEGALENTITYNAME { get; set; }

		[DataMember]
		public string ACCOUNT { get; set; }

		[DataMember]
		public DateTime DATE { get; set; }

		[DataMember]
		public string KIND { get; set; }

		[DataMember]
		public string CURRENCY { get; set; }

		[DataMember]
		public decimal SUMM { get; set; }

		[DataMember]
		public decimal SUMMRUBLE { get; set; }

		[DataMember]
		public long DOCKIND { get; set; }

        [DataMember]
        public string CONTENT { get; set; }

	}
}
