﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using PFR_INVEST.DataObjects;
using System.Collections;
using PFR_INVEST.Auth.SharedData;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects.BranchReport;
using PFR_INVEST.DataObjects.Interfaces;
using PFR_INVEST.DataObjects.Proxy;
using PFR_INVEST.DataObjects.ServiceItems;

namespace db2connector
{
    public static class KnownTypeHelper
    {
        public static IEnumerable<Type> GetDataObjectKnownTypes(ICustomAttributeProvider provider)
        {
            // все наследники BaseDataObject и другие сериализируемые типы
            var types = Assembly.GetAssembly(typeof(BaseDataObject)).GetTypes().Where(t => t.IsSubclassOf(typeof(BaseDataObject)) || t.IsSerializable).ToList();

            types.AddRange(Assembly.GetAssembly(typeof(BaseDataObject)).GetTypes().Where(a=> typeof(IKnownType).IsAssignableFrom(a)));

            // здесь добавляем штучные спецефические 
            types.AddRange(Assembly.GetAssembly(typeof(BaseDataObject)).GetTypes().Where(t => t.IsSubclassOf(typeof(PfrBranchReportImportContentBase)) || t.IsSerializable).ToList());
            
            types.Add(typeof(ArrayList));
            types.Add(typeof(long));
            types.Add(typeof(long[]));
            types.Add(typeof(WebServiceDataError));
            types.Add(typeof(ListPropertyCondition));

            types.Add(typeof(List<AsgFinTr>));
            types.Add(typeof(AuthRequest));
            types.Add(typeof(AuthResponse));
            types.Add(typeof(AcknowledgeType));
            types.Add(typeof(ServiceAuthMsg));
            types.Add(typeof(Role));
            types.Add(typeof(User));
            types.Add(typeof(List<Role>));
            types.Add(typeof(List<User>));
            types.Add(typeof(ServiceData));
            types.Add(typeof(DeletedDataItem));
            types.Add(typeof(DeletedData));
            
            return types;
        }
    }
}
