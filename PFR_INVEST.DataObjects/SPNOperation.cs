﻿using System;
using System.Runtime.Serialization;
using System.Diagnostics;

namespace PFR_INVEST.DataObjects
{
	[DebuggerDisplay("[{ID}] {Name}")]
	public class SPNOperation : BaseDataObject, IIdentifiable
	{
		[DataMember]
		Int16 m_ShowInsuredPersonsCount;

		[DataMember]
		Int16 m_ShowMonth;

		[DataMember]
		Int16 m_ShowPeriod;

		[DataMember]
		public virtual long ID { get; set; }
		
		[DataMember] 
		public virtual string Name { get; set; }

		[DataMember]
		public virtual bool? ShowInsuredPersonsCount 
		{ 
			get
			{
				return m_ShowInsuredPersonsCount > 0;
			}
			set 
			{
				if (value.HasValue && value.Value)
					m_ShowInsuredPersonsCount = 1;
				else
					m_ShowInsuredPersonsCount = 0;
			} 
		}

		[DataMember]
		public virtual bool? ShowMonth 
		{ 
			get
			{
				return m_ShowMonth > 0;
			}
			set 
			{
				if(value.HasValue && value.Value)
					m_ShowMonth = 1;
				else
					m_ShowMonth = 0;
			}
		}

		[DataMember]
		public virtual bool? ShowPeriod
		{
			get
			{
				return m_ShowPeriod > 0;
			}
			set
			{
				if (value.HasValue && value.Value)
					m_ShowPeriod = 1;
				else
					m_ShowPeriod = 0;
			}
		}
	}
}
