﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace PFR_INVEST.DataObjects.Analyze
{
    public class AnalyzeInsuredpersonData : AnalyzeReportDataBase//, IDataErrorInfo
    {
        public new virtual int? Total { get; set; }
        public virtual int SubjectId { get; set; }
        
        #region Поля только для чтения из БД
        public virtual string SubjectName { get; set; }
        public virtual string SubjectGroup { get; set; }
        public virtual SubjectSPN.FondType SubjectTypeFond { get; set; }
        public virtual int SubjOrderId { get; set; }

        #endregion

        public AnalyzeInsuredpersonData(AnalyzeInsuredpersonData d)
        {
            Id = d.Id;
            Total = d.Total;
            ReportId = d.ReportId;
            SubjectId = d.SubjectId;
            SubjectName = d.SubjectName;
            SubjectGroup = d.SubjectGroup;
            SubjOrderId = d.SubjOrderId;
            IsReadOnly = d.IsReadOnly;
        }
        public AnalyzeInsuredpersonData()
        {
            
        }
        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "Total":
                        if (Total.HasValue && Total.Value > int.MaxValue)
                            return "Значение превысило допустимый максимум (2 147 483 647)";
                        break;
                }
                return null;
            }
        }

    }
}
