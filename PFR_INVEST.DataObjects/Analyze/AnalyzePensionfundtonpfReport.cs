﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PFR_INVEST.DataObjects.Analyze
{
    public class AnalyzePensionfundtonpfReport : QuarkReportBase
    {
        public virtual int Subtraction { get; set; }
        public virtual string SubString => sbstr[Subtraction];
        static string[] sbstr = new[] { "Без вычета возврата в ПФР", "C учетом возврата в ПФР" };
        
    }
}
