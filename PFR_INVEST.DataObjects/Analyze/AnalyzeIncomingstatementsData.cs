﻿namespace PFR_INVEST.DataObjects.Analyze
{
    public class AnalyzeIncomingstatementsData : AnalyzeReportDataBase
    {
        public new virtual int? Total { get; set; }
        public virtual int Changetype { get; set; }
        public virtual string ChangeTypeName => _vidsChange[Changetype];
        public virtual int StatementId { get; set; }
        public virtual int FilingId { get; set; }
       
        public virtual string FilSubtypeName => FilingSubtype.HasValue ? ((FilingSubtypes)FilingSubtype).ToString() : string.Empty;
        public virtual int? FilingSubtype { get; set; }

        #region Вычисляемые поля
        //перечень полей с тремя полями ввода
       

        #endregion

        #region Поля только для чтения из БД
        public virtual string FilingName { get; set; }
        public virtual string StatementName { get; set; }
        public virtual int StatementOrderId { get; set; }
        public virtual int FilingOrderId { get; set; }
        #endregion

        static string[] _vidsChange = new[] { "Cрочные", "Уведомления о замене страховщика", "Досрочные" };
        public AnalyzeIncomingstatementsData()
        {
            
        }
        public AnalyzeIncomingstatementsData(AnalyzeIncomingstatementsData d)
        {
            Id = d.Id;
            ReportId = d.ReportId;
            Total = d.Total;
            StatementId = d.StatementId;
            StatementName = d.StatementName;
            Changetype = d.Changetype;
            FilingId = d.FilingId;
            FilingName = d.FilingName;
            FilingSubtype = d.FilingSubtype;
            FilingOrderId = d.FilingOrderId;
            StatementOrderId = d.StatementOrderId;
            IsReadOnly = d.IsReadOnly;
        }

    }

    public enum ChangeFundKind
    {
        Срочные,
        Уведомление,
        Досрочные
    }

    public enum FilingSubtypes
    {
        Лично,
        Почтой,
        Курьером
    }

}
