﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using PFR_INVEST.DataObjects.Helpers;

namespace PFR_INVEST.DataObjects.Analyze
{
    public abstract class QuarkReportBase : YearReportBase
    {
        public virtual int? Kvartal { get; set; }

        [IgnoreDataMember]
        public virtual string QuarkDisplay { get; set; }

        public new static T InitNewReport<T>(T emptyClass) where T : QuarkReportBase
        {
            emptyClass.Year = DateTime.Now.Year;
            emptyClass.Kvartal = DateHelper.GetAddSPNQuarter();
            emptyClass.CreateDate = DateTime.Now;
            emptyClass.UpdateDate = DateTime.Now;
            return emptyClass;
        }
    }

    public  abstract class YearReportBase : BaseDataObject
    {
        public virtual long Id { get; set; }

        public virtual int Year { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual DateTime? CreateDate { get; set; }
        [IgnoreDataMember]
        public virtual string Title => $"Отчет №:{Id}";

        public virtual int Status { get; set; }
        public static T InitNewReport<T>(T emptyClass) where T : YearReportBase
        {
            emptyClass.Year = DateTime.Now.Year;
            emptyClass.CreateDate = DateTime.Now;
            emptyClass.UpdateDate = DateTime.Now;
            emptyClass.Status = 0;
            return emptyClass;
        }
    }
}
