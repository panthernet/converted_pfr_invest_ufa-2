﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.InteropServices.ComTypes;

namespace PFR_INVEST.DataObjects.Analyze
{
    public class AnalyzePaymentyearrateData : AnalyzeReportDataBase
    {

        [Required]
        public virtual decimal? Total { get; set; }
        // public virtual int? Count { get; set; }
        public virtual int PaymentkindId { get; set; }

        #region Поля только для чтения из БД

        public virtual string PaymentkindName { get; set; }
        public virtual int PaymentkindGroup { get; set; }
        public virtual string PaymentkindGroupName => PaymentKind.GetGroupName(PaymentkindGroup);
        public virtual int PaymentkindEditType { get; set; }
        #endregion

        public AnalyzePaymentyearrateData(AnalyzePaymentyearrateData d)
        {
            Id = d.Id;
            ReportId = d.ReportId;
            Total = d.Total;
            //  Count = d.Count;
            PaymentkindId = d.PaymentkindId;
            PaymentkindName = d.PaymentkindName;
            PaymentkindGroup = d.PaymentkindGroup;
            PaymentkindEditType = d.PaymentkindEditType;
            IsReadOnly = d.IsReadOnly;


        }

        public AnalyzePaymentyearrateData() { }
    }
}
