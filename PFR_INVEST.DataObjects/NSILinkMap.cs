﻿namespace PFR_INVEST.DataObjects
{
	public class NSILinkMap : BaseDataObject
	{
		public virtual long ID { get; set; }

		public virtual string Table { get; set; }
		public virtual long TableID { get; set; }
		public virtual string Dictionary { get; set; }
		public virtual string NSIID { get; set; }
		public virtual long NSIDictID { get; set; }
	}
}
