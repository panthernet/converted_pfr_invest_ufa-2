﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    [Obsolete("Устаревший, использовать EdoOdkF025", false)]
    public class EdoOdkF022 : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual DateTime? ReportOnDate { get; set; }
        [DataMember]
        public virtual string Name { get; set; }
        [DataMember]
        public virtual string INN { get; set; }
        [DataMember]
        public virtual string KPP { get; set; }
        [DataMember]
        public virtual DateTime? ReportDate { get; set; }
        [DataMember]
        public virtual TimeSpan? ReportTime { get; set; }
        [DataMember]
        public virtual decimal? TotalAmount { get; set; }
        [DataMember]
        public virtual string Post { get; set; }
        [DataMember]
        public virtual string ManageName { get; set; }
        [DataMember]
        public virtual decimal? Group1 { get; set; }
        [DataMember]
        public virtual decimal? Group2 { get; set; }
        [DataMember]
        public virtual decimal? Group3 { get; set; }
        [DataMember]
        public virtual decimal? Group4 { get; set; }
        [DataMember]
        public virtual decimal? Group5 { get; set; }
        [DataMember]
        public virtual decimal? Group6 { get; set; }
        [DataMember]
        public virtual decimal? Group7 { get; set; }
        [DataMember]
        public virtual decimal? Group8 { get; set; }
        [DataMember]
        public virtual decimal? Group9 { get; set; }
        [DataMember]
        public virtual decimal? Group10 { get; set; }
        [DataMember]
        public virtual decimal? Group11 { get; set; }
        [DataMember]
        public virtual decimal? Group12 { get; set; }
        [DataMember]
        public virtual decimal? Group13 { get; set; }
        [DataMember]
        public virtual decimal? Subgroup1 { get; set; }
        [DataMember]
        public virtual decimal? Subgroup2 { get; set; }
        [DataMember]
        public virtual decimal? Subgroup3 { get; set; }
        [DataMember]
        public virtual long? ContractId { get; set; }
        [DataMember]
        public virtual string RegNum { get; set; }
        [DataMember]
        public virtual DateTime? RegDate { get; set; }


    }
}
