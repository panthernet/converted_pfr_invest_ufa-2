﻿using System;
using System.Globalization;

namespace PFR_INVEST.DataObjects
{
    public class RepositoryNoAucImpExpFile: RepositoryImpExpFile
    {
        public RepositoryNoAucImpExpFile()
        {
            AuctionLocalNum = 999;
            AuctionDate = DateTime.ParseExact("01.01.2099", "dd.MM.yyyy", CultureInfo.InvariantCulture);
            Operacia = "";
        }
    }
}
