﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class PfrBranchReportType : BaseDataObject
    {
		/// <summary>
		/// Режим источника отчета
		/// Загрузка из excel- или xml-файла
		/// </summary>
	    public enum SourceMode
	    {
			Excel = 1,
			Xml = 2
	    }

	    public enum BranchReportType
        {
            Empty = 0,

            InsuredPerson = 1,
            NpfNotice = 2,
            CertificationAgreement = 3,
            AssigneePayment = 4,

            SignatureContract = 5,

            CashExpenditures = 6,
            Applications741 = 7,
            
            DeliveryCost = 9,

			XmlInsuredPerson = 10,
			XmlNpfNotice = 20,
			XmlAssigneePayment = 40,
			XmlCashExpenditures = 60,
			XmlApplications741 = 70,
			XmlDeliveryCost = 90
        }


        public PfrBranchReportType()
            : base()
        {
        }

        [DataMember]
        public virtual long ID { get; set; }

		[DataMember]
		public virtual int Source { get; set; }

        [DataMember]
        public virtual string ShortName { get; set; }

        [DataMember]
        public virtual string FullName { get; set; }

        [DataMember]
        public virtual string Description { get; set; }

        [IgnoreDataMember]
        public virtual BranchReportType ReportType
        {
            get { return (BranchReportType)ID; }
        }
    }
}
