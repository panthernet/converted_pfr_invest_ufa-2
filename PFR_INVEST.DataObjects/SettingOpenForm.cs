﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class SettingOpenForm : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual string UserName { get; set; }
       
        [DataMember]
        public virtual string FormName { get; set; }

        [DataMember]
        public virtual DateTime Date { get; set; }

        [DataMember]
        public virtual DateTime? PeriodStart { get; set; }
        
        [DataMember]
        public virtual DateTime? PeriodEnd { get; set; }

        [DataMember]
        public virtual string Comment { get; set; }

        [DataMember]
        public virtual int ShowForm { get; set; }
    }
}
