﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class McProfitAbility : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }
        [DataMember]
        private decimal? _profitAbility;
        public virtual decimal? ProfitAbility
        {
            get { return _profitAbility; }
            set { if (_profitAbility != value) { _profitAbility = value; OnPropertyChanged("ProfitAbility"); } }
        }

        [DataMember]
        private decimal? _profitAbility3;
        public virtual decimal? ProfitAbility3
        {
            get { return _profitAbility3; }
            set { if (_profitAbility3 != value) { _profitAbility3 = value; OnPropertyChanged("ProfitAbility3"); } }
        }

        [DataMember]
        private decimal? _profitAbility12;
        public virtual decimal? ProfitAbility12
        {
            get { return _profitAbility12; }
            set { if (_profitAbility12 != value) { _profitAbility12 = value; OnPropertyChanged("ProfitAbility12"); } }
        }

        [DataMember]
        private decimal? _profitAbilityContract;
        public virtual decimal? ProfitAbilityContract
        {
			get { return _profitAbilityContract ?? 0; }
            set { if (_profitAbilityContract != value) { _profitAbilityContract = value; OnPropertyChanged("ProfitAbilityContract"); } }
        }

        [DataMember]
        private long? _contractId;
        public virtual long? ContractId
        {
            get { return _contractId; }
            set { if (_contractId != value) { _contractId = value; OnPropertyChanged("ContractId"); } }
        }

        [DataMember]
        private long? _yearId;
        public virtual long? YearId
        {
            get { return _yearId; }
            set { if (_yearId != value) { _yearId = value; OnPropertyChanged("YearId"); } }
        }

        [DataMember]
        private DateTime? _date;
        public virtual DateTime? Date
        {
            get { return _date; }
            set { if (_date != value) { _date = value; OnPropertyChanged("Date"); } }
        }

        [DataMember]
        private long? _quarter;
        public virtual long? Quarter
        {
            get { return _quarter; }
            set { if (_quarter != value) { _quarter = value; OnPropertyChanged("Quarter"); } }
        }

        [DataMember]
        private string _comment;
        public virtual string Comment
        {
            get { return _comment; }
            set { if (_comment != value) { _comment = value; OnPropertyChanged("Comment"); } }
        }

        [DataMember]
        public virtual string ContractNumber { get; set; }
        [DataMember]
        public virtual string UKName { get; set; }
        [DataMember]
        public virtual string YearName { get; set; }

    
        public virtual DateTime? GetControlDateTime()
        {
            if (!YearId.HasValue || !Quarter.HasValue) return null;
            var year = 2000 + (int)YearId.Value;
            var month = (int)Quarter.Value * 3 - 2;
            return new DateTime(year, month, 1);
        }
    }
}
