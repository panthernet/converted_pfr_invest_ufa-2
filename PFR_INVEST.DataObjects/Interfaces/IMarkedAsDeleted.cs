﻿namespace PFR_INVEST.DataObjects
{
    /// <summary>
    /// Интерфейс обязывает изменять статус сущности на архивный (-1) при удалении (отменяет фактическое удаление из БД)
    /// Необходим для реализации восстановления записей после удаления (архивирования).
    /// </summary>
    public interface IMarkedAsDeleted
    {
        /// <summary>
        /// Статус сущности
        /// </summary>
        long StatusID { get; set; }
    }

	
}
