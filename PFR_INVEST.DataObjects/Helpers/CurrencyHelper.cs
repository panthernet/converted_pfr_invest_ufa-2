﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.Constants.Identifiers;

namespace PFR_INVEST.DataObjects.Helpers
{
    public class CurrencyHelper
    {
        public static bool IsRUB(Currency currency)
        {
            return currency != null && CurrencyIdentifier.IsRUB(currency.Name);
        }
    }
}
