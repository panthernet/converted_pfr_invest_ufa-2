﻿using System;
using System.Collections.Generic;
using PFR_INVEST.Proxy;

namespace PFR_INVEST.DataObjects.Helpers
{
    public static class DBFieldHelper
    {
        #region GetValue() implementations
        public static object GetValue(string key, Dictionary<string, DBField>  fields)
        {
            object retVal = null;
            if (!fields.ContainsKey(key)) return null;
            var dbVal = fields[key];
            if (dbVal != null && dbVal.Value != null)
                retVal = dbVal.Value;
            return retVal;
        }

        /// <summary>
        /// Возвращает null для даты DateTime.MinValue
        /// </summary>		
        public static DateTime? GetDate(string key, Dictionary<string, DBField> fields)
        {
            var retVal = GetValue(key, fields) as DateTime?;
            if (retVal == DateTime.MinValue)
                retVal = null;
            return retVal;
        }

        public static decimal? GetDecimal(string key, Dictionary<string, DBField> fields)
        {
            return GetValue(key, fields) as decimal?;
        }

        public static void SetValue(string key, object value, Dictionary<string, DBField> fields)
        {
            if (!fields.ContainsKey(key)) return;
            var dbVal = fields[key];
            if (dbVal != null)
                dbVal.Value = value;
        }
        #endregion
    }
}
