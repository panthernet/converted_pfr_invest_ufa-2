﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    
    public class Attach : BaseDataObject,IMarkedAsDeleted
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long? DocumentID { get; set; }

        [DataMember]
        public virtual long? AttachClassificID { get; set; }

        [DataMember]
        public virtual string Additional { get; set; }

        [DataMember]
        public virtual long? ExecutorID { get; set; }

        [DataMember]
        public virtual string ExecutorName { get; set; }

        [DataMember]
        public virtual DateTime? ControlDate { get; set; }

        [DataMember]
        public virtual DateTime? ExecutionDate { get; set; }

        [DataMember]
        public virtual string AdditionalToExecution { get; set; }

        private long? _OriginalPlaceID;
        [DataMember]
        public virtual long? OriginalPlaceID
        {
            get { return _OriginalPlaceID; }
            set { if (_OriginalPlaceID != value) { _OriginalPlaceID = value; OnPropertyChanged("OriginalPlaceID"); } }
        }

        /// <summary>
        /// Поле только для вычитки из базы
        /// </summary>
        [DataMember]
        public virtual string OriginalPlace { get; set; }

        [DataMember]
        public virtual string Comment { get; set; }

        
        [DataMember]
        public virtual long? FileContentId { get; set; }

        [DataMember]
        public virtual int TypeID { get; set; }

        [IgnoreDataMember]
        public virtual Document.Types Type
        {
            get { return (Document.Types)this.TypeID; }
            set { this.TypeID = (int)value; }
        }

        public virtual long StatusID { get; set; }
    }

    
}
