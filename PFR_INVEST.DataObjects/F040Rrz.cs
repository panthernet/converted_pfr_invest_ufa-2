﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
	public class F040Rrz:BaseDataObject
	{
		[DataMember]
		public virtual long ID { get; set; }

		[DataMember]
		public virtual long? EdoID { get; set; }

		[DataMember]
		public virtual long? RrzID { get; set; }

		[DataMember]
		public virtual string RrzText { get; set; }

		[DataMember]
		public virtual decimal? BuyCount { get; set; }

		[DataMember]
		public virtual decimal? SellCount { get; set; }

		[DataMember]
		public virtual decimal? BuyAmount { get; set; }

		[DataMember]
		public virtual decimal? SellAmount { get; set; }
	}
}
