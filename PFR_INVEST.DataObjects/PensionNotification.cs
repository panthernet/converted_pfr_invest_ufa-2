﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;


namespace PFR_INVEST.DataObjects
{
    public class PensionNotification : BaseDataObject
    {
        public PensionNotification()
            : base()
        {
            StatusID = 1;
        }

        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        private long _SenderID;
        public virtual long SenderID
        {
            get { return _SenderID; }
            set { if (_SenderID != value) { _SenderID = value; OnPropertyChanged("SenderID"); } }
        }

        [DataMember]
        private DateTime? _DeliveryDate;
        public virtual DateTime? DeliveryDate
        {
            get { return _DeliveryDate; }
            set { if (_DeliveryDate != value) { _DeliveryDate = value; OnPropertyChanged("DeliveryDate"); } }
        }

        /// <summary>
        /// Ссылка на тип уведомления (ELEMENT) 
        /// </summary>
        [DataMember]
        public virtual long? NotifTypeID { get; set; }

        [DataMember]
        private string _NotifNum;
        public virtual string NotifNum
        {
            get { return _NotifNum; }
            set { if (_NotifNum != value) { _NotifNum = value; OnPropertyChanged("NotifNum"); } }
        }

        [DataMember]
        private DateTime? _NotifDate;
        public virtual DateTime? NotifDate
        {
            get { return _NotifDate; }
            set { if (_NotifDate != value) { _NotifDate = value; OnPropertyChanged("NotifDate"); } }
        }

        [DataMember]
        private string _RegisterNum;
        public virtual string RegisterNum
        {
            get { return _RegisterNum; }
            set { if (_RegisterNum != value) { _RegisterNum = value; OnPropertyChanged("RegisterNum"); } }
        }

        [DataMember]
        private DateTime? _RegisterDate;
        public virtual DateTime? RegisterDate
        {
            get { return _RegisterDate; }
            set { if (_RegisterDate != value) { _RegisterDate = value; OnPropertyChanged("RegisterDate"); } }
        }

        [DataMember]
        private long _StatusID;
        public virtual long StatusID
        {
            get { return _StatusID; }
            set { if (_StatusID != value) { _StatusID = value; OnPropertyChanged("StatusID"); } }
        }

        [DataMember]
        private string _LegalEntityName;
        public virtual string LegalEntityName
        {
            get { return _LegalEntityName; }
            set { if (_LegalEntityName != value) { _LegalEntityName = value; OnPropertyChanged("LegalEntityName"); } }
        }

        [DataMember]
        private string _NotifTypeName;
        public virtual string NotifTypeName
        {
            get { return _NotifTypeName; }
            set { if (_NotifTypeName != value) { _NotifTypeName = value; OnPropertyChanged("NotifTypeName"); } }
        }

        /*
        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "NotifNum":
                        if (string.IsNullOrEmpty(NotifNum))
                            return "Поле обязательное для заполнения";
                        if (IsInvalidLength(NotifNum, 100))
                            return "Максимальная длина 100 символов";
                        break;
                    case "SenderID":
                        if (SenderID == null || SenderID <= 0)
                            return "Выберите отправителя";
                        break;
                    case "NotifTypeID":
                        if (NotifTypeID == null || !NotifTypeID.HasValue || NotifTypeID.Value <= 0)
                            return "Выберите тип уведомления";
                        break;
                    case "DeliveryDate":
                        if (DeliveryDate == null)
                            return "Заполните дату поступления";
                        break;
                    case "NotifDate":
                        if (DeliveryDate == null)
                            return "Заполните дату уведомления";
                        break;
                }
                return null;
            }
        }
        */

        public virtual bool IsValid()
        {
            string[] fieldNames = { "NotifNum", "SenderID", "NotifTypeID", "DeliveryDate", "NotifDate" };
            foreach (string fieldName in fieldNames)
            {
                if (!String.IsNullOrEmpty(this[fieldName]))
                {
                    return false;
                }
            }

            return true;
        }

        private bool IsInvalidLength(string value, int maxLength)
        {
            return string.IsNullOrEmpty(value) || value.Length > maxLength;
        }

        private bool IsInvalidLengthNullable(string value, int maxLength)
        {
            return !string.IsNullOrEmpty(value) && value.Length > maxLength;
        }

        [IgnoreDataMember]
        public virtual bool IsDeleted
        {
            get { return StatusID == -1; }
        }

        public virtual void MarkDeleted()
        {
            StatusID = -1;
        }

    }
}
