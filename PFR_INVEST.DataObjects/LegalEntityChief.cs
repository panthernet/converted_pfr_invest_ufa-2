﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class LegalEntityChief : BaseDataObject
    {

        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        private long _LegalEntityID;
        public virtual long LegalEntityID
        {
            get { return _LegalEntityID; }
            set { if (_LegalEntityID != value) { _LegalEntityID = value; OnPropertyChanged("LegalEntityID"); } }
        }

        [DataMember]
        private string _FirstName;
        public virtual string FirstName
        {
            get { return _FirstName; }
            set { if (_FirstName != value) { _FirstName = value; OnPropertyChanged("FirstName"); } }
        }

        [DataMember]
        private string _LastName;
        public virtual string LastName
        {
            get { return _LastName; }
            set { if (_LastName != value) { _LastName = value; OnPropertyChanged("LastName"); } }
        }

        [DataMember]
        private string _PatronymicName;
        public virtual string PatronymicName
        {
            get { return _PatronymicName; }
            set { if (_PatronymicName != value) { _PatronymicName = value; OnPropertyChanged("PatronymicName"); } }
        }


        [DataMember]
        private string _Position;
        public virtual string Position
        {
            get { return _Position; }
            set { if (_Position != value) { _Position = value; OnPropertyChanged("Position"); } }
        }

        [DataMember]
        private string _AccordingTo;
        public virtual string AccordingTo
        {
            get { return _AccordingTo; }
            set { if (_AccordingTo != value) { _AccordingTo = value; OnPropertyChanged("AccordingTo"); } }
        }


        [DataMember]
        private string _WorkPhone;
        public virtual string WorkPhone
        {
            get { return _WorkPhone; }
            set { if (_WorkPhone != value) { _WorkPhone = value; OnPropertyChanged("WorkPhone"); } }
        }

        [DataMember]
        private string _WorkPhoneExtension;
        public virtual string WorkPhoneExtension
        {
            get { return _WorkPhoneExtension; }
            set { if (_WorkPhoneExtension != value) { _WorkPhoneExtension = value; OnPropertyChanged("WorkPhoneExtension"); } }
        }

        [DataMember]
        private string _MobilePhone;
        public virtual string MobilePhone
        {
            get { return _MobilePhone; }
            set { if (_MobilePhone != value) { _MobilePhone = value; OnPropertyChanged("MobilePhone"); } }
        }

        [DataMember]
        private string _Email;
        public virtual string Email
        {
            get { return _Email; }
            set { if (_Email != value) { _Email = value; OnPropertyChanged("Email"); } }
        }

        [DataMember]
        private DateTime? _InaugurationDate;
        public virtual DateTime? InaugurationDate
        {
            get { return _InaugurationDate; }
            set { if (_InaugurationDate != value) { _InaugurationDate = value; OnPropertyChanged("InaugurationDate"); } }
        }

        [DataMember]
        private DateTime? _RetireDate;
        public virtual DateTime? RetireDate
        {
            get { return _RetireDate; }
            set { if (_RetireDate != value) { _RetireDate = value; OnPropertyChanged("RetireDate"); } }
        }

        [DataMember]
        private string _Comment;
        public virtual string Comment
        {
            get { return _Comment; }
            set { if (_Comment != value) { _Comment = value; OnPropertyChanged("Comment"); } }
        }

        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "FirstName":
                        if (string.IsNullOrEmpty(this.FirstName))
                            return "Поле обязательное для заполнения";
                        break;
                    case "LastName":
                        if (string.IsNullOrEmpty(this.LastName))
                            return "Поле обязательное для заполнения";
                        break;
                    case "Position":
                        if (string.IsNullOrEmpty(this.Position))
                            return "Поле обязательное для заполнения";
                        break;
                    case "AccordingTo":
                        if (string.IsNullOrEmpty(this.AccordingTo))
                            return "Поле обязательное для заполнения";
                        break;
                }
                return null;
            }
        }

        public virtual string GetFullName()
        {
            return string.Format("{0} {1} {2}", LastName, FirstName, PatronymicName);
        }


        public virtual void ApplyValues(LegalEntityChief x)
        {
            ID = x.ID;
            LegalEntityID = x.LegalEntityID;
            FirstName = x.FirstName;
            LastName = x.LastName;
            PatronymicName = x.PatronymicName;

            Position = x.Position;
            AccordingTo = x.AccordingTo;
            WorkPhone = x.WorkPhone;
            WorkPhoneExtension = x.WorkPhoneExtension;
            MobilePhone = x.MobilePhone;
            Email = x.Email;
            InaugurationDate = x.InaugurationDate;
            RetireDate = x.RetireDate;
            Comment = x.Comment;
        }
    }

}
