﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class OnesExportStatus: BaseDataObject
    {
        [DataMember]
        public int Count { get; set; }

        [DataMember]
        public int MaxCount { get; set; }

        [DataMember]
        public bool Completed { get; set; }
    }
}
