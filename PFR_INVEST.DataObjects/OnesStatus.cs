﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class OnesStatus : BaseDataObject
    {
        public enum Statuses: long
        {
            Received = 1L,
            Validated = 2L,
            ValidatedWithError = 3L,
            Processing = 4L,
            Processed = 5L,
            ProcessedWithError = 6L
        }

        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual string NameEng { get; set; }

        [DataMember]
        public virtual string NameRus { get; set; }

        [DataMember]
        public virtual string Description { get; set; }
    }
}
