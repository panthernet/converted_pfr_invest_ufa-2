﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{

	public class EdoOdkF016 : BaseDataObject
	{
		[DataMember]
		public virtual long ID { get; set; }

		[DataMember]
		public virtual DateTime? ReportDate { get; set; }

		[IgnoreDataMember]
		public virtual decimal? NetWealthSum { get { return this.A090; } set { this.A090 = value; } }

		[DataMember]
		public virtual TimeSpan? ReportTime { get; set; }

		[IgnoreDataMember]
		public virtual DateTime? ReportDateTime
		{
			get
			{
                DateTime? dt = null;
                if (this.ReportDate.HasValue && this.ReportTime.HasValue)
                    dt = this.ReportDate.Value.Date.Add(this.ReportTime.Value);
                else if (this.ReportDate.HasValue)
                    dt = this.ReportDate.Value;
                return dt;
			}
		}
		[DataMember]
		public virtual string RegNumberOut { get; set; }
		[DataMember]
		public virtual DateTime? ReportOnDate { get; set; }
		[DataMember]
		public virtual string Name { get; set; }
		[DataMember]
		public virtual decimal? INN { get; set; }
		[DataMember]
		public virtual decimal? KPP { get; set; }
		[DataMember]
		public virtual decimal? A010 { get; set; }
		[DataMember]
		public virtual decimal? A020 { get; set; }
		[DataMember]
		public virtual decimal? A030 { get; set; }
		[DataMember]
		public virtual decimal? A031 { get; set; }
		[DataMember]
		public virtual decimal? A032 { get; set; }
		[DataMember]
		public virtual decimal? A033 { get; set; }
		[DataMember]
		public virtual decimal? A034 { get; set; }
		[DataMember]
		public virtual decimal? A035 { get; set; }
		[DataMember]
		public virtual decimal? A036 { get; set; }
		[DataMember]
		public virtual decimal? A037 { get; set; }
		[DataMember]
		public virtual decimal? A038 { get; set; }
		[DataMember]
		public virtual decimal? A039 { get; set; }
		[DataMember]
		public virtual decimal? A040 { get; set; }
		[DataMember]
		public virtual decimal? A041 { get; set; }
		[DataMember]
		public virtual decimal? A042 { get; set; }
		[DataMember]
		public virtual decimal? A043 { get; set; }
		[DataMember]
		public virtual decimal? A050 { get; set; }
		[DataMember]
		public virtual decimal? A060 { get; set; }
		[DataMember]
		public virtual decimal? A070 { get; set; }
		[DataMember]
		public virtual decimal? A071 { get; set; }
		[DataMember]
		public virtual decimal? A072 { get; set; }
		[DataMember]
		public virtual decimal? A073 { get; set; }
		[DataMember]
		public virtual decimal? A074 { get; set; }
		[DataMember]
		public virtual decimal? A075 { get; set; }
		[DataMember]
		public virtual decimal? A080 { get; set; }
		[DataMember]
		public virtual decimal? A090 { get; set; }
		[DataMember]
		public virtual string ApName { get; set; }
		[DataMember]
		public virtual string ApPost { get; set; }
		[DataMember]
		public virtual string RegNum { get; set; }
		[DataMember]
		public virtual DateTime? RegDate { get; set; }


	}
}
