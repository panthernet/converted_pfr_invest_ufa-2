﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    /// <summary>
    /// Вынесеные доп. поля из Депозита
    /// </summary>
	public class Deposit2 : Deposit
	{
        /// <summary>
        /// Дублирование Depclaim2.Ammount
        /// </summary>
        [Obsolete("dc2.Amount (местами s.Sum) Не заполняется, нужно избавляться")]
        public virtual decimal? LocationVolume { get; set; }

        public virtual string Year { get; set; }

        [Obsolete("dc2.SettleDate, нужно избавляться")]
        public virtual DateTime? LocationDate { get; set; }

        [Obsolete("dc2.ReturnDate, нужно избавляться")]
        public virtual DateTime? ReturnDate { get; set; }

        /// <summary>
        /// Дублирование Depclaim_Offer.Number
        /// </summary>
        [Obsolete("надо избавляться")]
        public virtual string RegNum { get; set; }
    }
}
