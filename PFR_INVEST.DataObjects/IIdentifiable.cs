﻿namespace PFR_INVEST.DataObjects
{
    public interface IIdentifiable
    {
        long ID { get; set; }
    }
}