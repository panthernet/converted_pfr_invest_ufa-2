﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class LegalEntityHead : BaseDataObject
    {

        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        private long _LegalEntityID;
        public virtual long LegalEntityID
        {
            get { return _LegalEntityID; }
            set { if (_LegalEntityID != value) { _LegalEntityID = value; OnPropertyChanged("LegalEntityID"); } }
        }

        [DataMember]
        private string _FullName;
        public virtual string FullName
        {
            get { return _FullName; }
            set { if (_FullName != value) { _FullName = value; OnPropertyChanged("FullName"); } }
        }


        [DataMember]
        private string _Position;
        public virtual string Position
        {
            get { return _Position; }
            set { if (_Position != value) { _Position = value; OnPropertyChanged("Position"); } }
        }

        [DataMember]
        private string _Phone;
        public virtual string Phone
        {
            get { return _Phone; }
            set { if (_Phone != value) { _Phone = value; OnPropertyChanged("Phone"); } }
        }

        [DataMember]
        private string _Email;
        public virtual string Email
        {
            get { return _Email; }
            set { if (_Email != value) { _Email = value; OnPropertyChanged("Email"); } }
        }




        //Костыль, что-бы забиндить колонку грида на обьект
        //[IgnoreDataMember]
        //public virtual LegalEntityHead self { get { return this; } }

        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {

                    case "FullName":
                        if (string.IsNullOrEmpty(this.FullName))
                            return "Поле обязательное для заполнения";
                        if (FullName.Length > 100)
                            return "Максимальная длина поля 100 символов";
                        break;

                    case "Position":
                        if (string.IsNullOrEmpty(this.Position))
                            return "Поле обязательное для заполнения";
                        if (Position.Length > 100)
                            return "Максимальная длина поля 100 символов";
                        break;
                }
                return null;
            }
        }
        //public LegalEntityHead()
        //{
        //    this.Validate += (sender, e) =>
        //    {
        //        switch (e.PropertyName)
        //        {
        //            case "FullName":
        //                if (string.IsNullOrEmpty(this.FullName))
        //                    e.Error = "Поле обязательное для заполнения";
        //                break;
        //            case "Position":
        //                if (string.IsNullOrEmpty(this.Position))
        //                    e.Error = "Поле обязательное для заполнения";
        //                break;

        //        }
        //    };
        //}




    }
}
