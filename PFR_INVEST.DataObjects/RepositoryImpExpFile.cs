﻿using System.Runtime.Serialization;
using System;

namespace PFR_INVEST.DataObjects
{
    public class RepositoryImpExpFile : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

		[DataMember]
		public virtual DateTime DDate { get; set; }

		[DataMember]
		public virtual TimeSpan TTime { get; set; }

		[DataMember]
		public virtual string UserName { get; set; }

		[DataMember]
		public virtual DateTime? AuctionDate { get; set; }

		[DataMember]
		public virtual int? AuctionLocalNum { get; set; }

		[DataMember]
		public virtual string Operacia { get; set; }

		/// <summary>
		/// 1 - Импорт
		/// 0 - Экспорт
		/// </summary>
		[DataMember]
		public virtual int ImpExp { get; set; }

		[DataMember]
		public virtual int Key { get; set; }

		[DataMember]
		public virtual byte[] Repository { get; set; }

		[DataMember]
		public virtual string Comment { get; set; }

		[DataMember]
		public virtual int Status { get; set; }

        public enum Keys
        {

            /// <summary>
            /// Сводный реестр заявок
            /// </summary>
            DepClaimImport=1,
            /// <summary>
            /// Сводный реестр заявок, с учетом максимальной суммы размещения
            /// </summary>
            DepClaimImportMax = 2,
            /// <summary>
            /// Импорт сводного реестра заявок от СПВБ
            /// </summary>
            DepClaimImportSPVB = 3,
            /// <summary>
            /// 
            /// </summary>
            DepClaimImportZ = 4,
            /// <summary>
            /// Импорт пакета XML от ММВБ
            /// </summary>
            AucImportPacket = 5,
            /// <summary>
            /// Заключение о результатах проверки
            /// </summary>
            BankVerResolution = 6,
            /// <summary>
            /// Сформировать лимиты
            /// </summary>
            BankVerLimits = 7,
            /// <summary>
            /// Экспорт 1С
            /// </summary>
            OnesExportXml = 199,
            /// <summary>
            /// Импорт 1С
            /// </summary>
            OnesImportXml = 200,
            /// <summary>
            /// Экспорт отчета в ЦБ
            /// </summary>
            CBReportExportXml = 201,
            /// <summary>
            /// Импорт ОПФР
            /// </summary>
            OpfrImport = 202
        }
    }
}
