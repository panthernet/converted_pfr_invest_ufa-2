﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class OnesSession : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual DateTime SessionDate { get; set; }

        [DataMember]
        public virtual int IsImport { get; set; }

        /*[DataMember]
        public virtual int State { get; set; }*/


        /*public enum OnesSessionState
        {
            Started = 0,
            Finished = 1
        }*/

        [IgnoreDataMember]
        public virtual bool IsImportSession => IsImport == 1;
    }
}
