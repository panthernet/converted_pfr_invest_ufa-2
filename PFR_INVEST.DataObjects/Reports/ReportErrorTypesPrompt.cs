﻿using System;

namespace PFR_INVEST.DataObjects.Reports
{
    public class ReportErrorTypesPrompt
    {
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public int Year { get; set; }
    }
}
