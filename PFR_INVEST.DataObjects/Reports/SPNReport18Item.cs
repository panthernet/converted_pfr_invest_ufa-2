﻿using System;

namespace PFR_INVEST.DataObjects.Reports
{
    public class SPNReport18Item
    {
        public string FName { get; set; }
        public string RegNum { get; set; }
        public DateTime? RegDate { get; set; }
        public decimal SaldoStart { get; set; }
        public decimal SendedDUTotal { get; set; }
        public decimal SendedDUDsv { get; set; }
        public decimal SendedDUMsk { get; set; }
        public decimal ReturnDUTotalTotal { get; set; }
        public decimal ReturnDUTotalYield { get; set; }
        public decimal ReturnDUPravTotal { get; set; }
        public decimal ReturnDUPravYield { get; set; }
        public decimal ReturnDUDsvTotal { get; set; }
        public decimal ReturnDUDsvYield { get; set; }
        public decimal PeretokTotal { get; set; }
        public decimal PeretokYield { get; set; }
        public decimal ReturnDUMskTotal { get; set; }
        public decimal ReturnDUMskYield { get; set; }
        public decimal SendedDUSum { get; set; }
        public decimal SaldoEnd { get; set; }


    }
}
