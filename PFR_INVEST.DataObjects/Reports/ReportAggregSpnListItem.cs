﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.Reports
{
    [DataContract]
    public class ReportAggregSpnListItem : BaseDataObject
    {
        public ReportAggregSpnListItem() { }

        public ReportAggregSpnListItem(decimal sum, string name, string op, long opid, int mod)
        {
            FormalizedName = name;
            OperationName = op;
            OperationID = opid;
            Mod = mod;
            if (Mod < 0)
                ToNpf = sum;
            else FromNpf = sum;
        }

        public int Mod { get; set; }

        public long OperationID { get; set; }

        public string FormalizedName { get; set; }

        public string OperationName { get; set; }

        public decimal IncomeSpnLeft { get; set; }

        public decimal ToNpf { get; set; }

        public decimal FromNpf { get; set; }

        public decimal OutcomeSpnLeft { get; set; }

    }
}