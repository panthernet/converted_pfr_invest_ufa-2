﻿

namespace PFR_INVEST.DataObjects.Reports
{
    public class SPNReport19Item :SPNReportItemBase
    {
        public decimal SPNBegin { get; set; }
        public decimal SPNBeginSum { get; set; }
        public decimal SPNEnd { get; set; }
        public decimal SPNEndSum { get; set; }
        public decimal IncreaseRub { get; set; }
        public decimal IncreaseRubSum { get; set; }
        public decimal IncreasePerc { get; set; }
        public decimal IncreasePercSum { get; set; }

      
    }
}
