﻿using System;
using System.Runtime.Serialization;
using PFR_INVEST.Constants.Identifiers;

namespace PFR_INVEST.DataObjects.Reports
{
	[DataContract]
	public class ReportIPUKListItem : ReqTransfer
	{
		public ReportIPUKListItem() { }

		public ReportIPUKListItem(ReqTransfer transfer, long? directionID, string operationName, string comment, AsgFinTr asg)
		{
			transfer.CopyTo(this);
			DirectionID = directionID;
			OperationName = operationName;
			Comment = comment;
			CommonPPID = asg?.ID ?? 0;
			CommonPPOrderDate = asg?.PayDate ?? transfer.PaymentOrderDate;
            CommonPPDraftPayDate = asg?.DraftPayDate;
			CommonPPOrderNum = asg?.PayNumber ?? transfer.PaymentOrderNumber;
			//Sum = Sum ?? 0.00m;
		}

	    public ReportIPUKListItem(ReqTransfer transfer, long? directionID, string operationName, string comment, AsgFinTr asg, string ukName, string contractName, DateTime? contractDate)
            : this(transfer, directionID, operationName, comment, asg)
	    {
	        ContractDate = contractDate;
	        ContractNumber = contractName;
	        UKName = ukName;
	    }

        public DateTime? ContractDate { get; set; }

        public string ContractNumber { get; set; }

        public string UKName { get; set; }

	    public DateTime? CommonPPDraftPayDate { get; set; }


	    public long? DirectionID { get; set; }

		public string OperationName { get; set; }
		public new string Comment { get; set; }


		public long CommonPPID { get; set; }
		public string CommonPPOrderNum { get; set; }

		public DateTime? CommonPPOrderDate { get; set; }

		public decimal? SumToUK => TransferDirectionIdentifier.IsFromPFRToUK(DirectionID) ? Sum : null;

	    public decimal? SumFromUK => TransferDirectionIdentifier.IsFromUKToPFR(DirectionID) ? Sum : null;
	}
}
