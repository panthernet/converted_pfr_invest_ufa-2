﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.Reports
{
	[DataContract]
	public class ReportDepositListItem
	{
		public ReportDepositListItem(){}

		public ReportDepositListItem(DepClaimSelectParams auction, DepClaimOffer offer) :this()
		{
		
		}

		public DateTime? AuctionDate { get; set; }
		public int AuctionNumber { get; set; }
		public string BankName { get; set; }
		public long OfferNumber { get; set; }
		public decimal Percent { get; set; }
		public decimal SumBody { get; set; }
		public decimal SumPercent { get; set; }
		public int Period { get; set; }

		public DateTime? SettleDate { get; set; }
		public DateTime? ReturnDate { get; set; }

		public string Portfolio { get; set; }
	    public string StockName { get; set; }
	}
}
