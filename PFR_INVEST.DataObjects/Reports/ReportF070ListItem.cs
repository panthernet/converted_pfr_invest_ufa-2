﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PFR_INVEST.DataObjects.Reports
{
	public class ReportF070ListItem : EdoOdkF070
	{
		public ReportF070ListItem() { }

		public ReportF070ListItem(EdoOdkF070 edo, Contract contract, string ukName, string contragentType)
			: this() 
		{
			edo.CopyToWithoutNull(this);
			UKName = ukName;
			ContractNumber = contract.ContractNumber;
			Portfolio = contract.PortfolioName;
			ContragentType = contragentType;
		}

		public string UKName { get; set; }
		public string Portfolio { get; set; }
		public string ContractNumber { get; set; }
		public string ContragentType { get; set; }
	}
}
