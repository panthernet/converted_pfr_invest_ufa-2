﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PFR_INVEST.DataObjects.Reports
{
	public class ReportRSASCAOldListItem
	{

		public ReportRSASCAOldListItem() 
		{		
		}

		public string UKName { get; set; }
		public string Portfolio { get; set; }
		public string ContractNumber { get; set; }
		public DateTime? ContractDate { get; set; }


		public decimal? A010 { get; set; }
		public decimal? A010P { get; set; }
		public decimal? A011 { get; set; }
		public decimal? A011P { get; set; }
		public decimal? A020 { get; set; }
		public decimal? A020P { get; set; }
		public decimal? A030 { get; set; }
		public decimal? A030P { get; set; }
		public decimal? A031 { get; set; }
		public decimal? A031P { get; set; }
		public decimal? A040 { get; set; }
		public decimal? A040P { get; set; }
		public decimal? A041 { get; set; }
		public decimal? A041P { get; set; }
		public decimal? A050 { get; set; }
		public decimal? A050P { get; set; }
		public decimal? A060 { get; set; }
		public decimal? A060P { get; set; }
		public decimal? A061 { get; set; }
		public decimal? A061P { get; set; }
		public decimal? A070 { get; set; }
		public decimal? A070P { get; set; }
		public decimal? A080 { get; set; }
		public decimal? A080P { get; set; }
		public decimal? A090 { get; set; }
		public decimal? A090P { get; set; }
		public decimal? A091 { get; set; }
		public decimal? A091P { get; set; }
		public decimal? A100 { get; set; }
		public decimal? A100P { get; set; }
		public decimal? A101 { get; set; }
		public decimal? A101P { get; set; }
		public decimal? A102 { get; set; }
		public decimal? A102P { get; set; }
		public decimal? A103 { get; set; }
		public decimal? A103P { get; set; }

		public decimal? A110 { get; set; }		
		public decimal? A120 { get; set; }		
		public decimal? A121 { get; set; }
		public decimal? A122 { get; set; }
		public decimal? A123 { get; set; }

		public decimal? TotalRSA { get { return A010 + A020 + A030 + A040 + A050 + A060 + A070 + A080 + A090 + A100 + A110; } }
		public decimal? TotalSCA { get { return TotalRSA - A120; } }

		
	}
}
