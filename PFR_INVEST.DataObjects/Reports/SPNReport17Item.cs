﻿using System;

namespace PFR_INVEST.DataObjects.Reports
{
    public class SPNReport17Item : SPNReport18Item
    {
        public decimal ReturnSPNTotal { get; set; }
        public decimal ReturnSPNTotalYield { get; set; }
        public decimal ReturnSPNNchtp { get; set; }
        public decimal ReturnSPNNchtpYield { get; set; }
        public decimal ReturnSPNSPV { get; set; }
        public decimal ReturnSPNSPVYield { get; set; }
        public decimal ReturnSPNEV { get; set; }
        public decimal ReturnSPNEVYield { get; set; }
        public string UKType { get; set; }
        public int IsGUK { get; set; }
        public int IsUK { get; set; }
    }
}
