﻿using System;

namespace PFR_INVEST.DataObjects.Reports
{
	public class ReportRSASCAPrompt
	{
		public DateTime Date { get; set; }

		public Document.Types Type { get; set; }
        public Person Person { get; set; }
    }
}
