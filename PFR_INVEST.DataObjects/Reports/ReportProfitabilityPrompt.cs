﻿namespace PFR_INVEST.DataObjects.Reports
{
	public class ReportProfitabilityPrompt
	{
		public long YearID { get; set; }
		public int Quarter { get; set; }
        public Document.Types Type { get; set; }
        public Person Person { get; set; }
    }
}
