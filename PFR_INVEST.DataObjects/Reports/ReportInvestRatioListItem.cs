﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PFR_INVEST.DataObjects.Reports
{
	public class ReportInvestRatioListItem
	{
		public long ContractID { get; set; }
		public string ContractNumber { get; set; }
		public string UKName { get; set; }
		public string Portfolio { get; set; }

		/// <summary>
		/// СЧА начальная
		/// </summary>
		public decimal S0 { get; set; }
		/// <summary>
		/// Возвращено из ДУ
		/// </summary>
		public decimal Sm { get; set; }
		/// <summary>
		/// Перечислено в ДУ 
		/// </summary>
		public decimal Sn { get; set; }
		/// <summary>
		/// СЧА средняя
		/// </summary>
		public decimal Sc { get; set; }
		/// <summary>
		/// Множитель
		/// </summary>
		public decimal K { get; set; }

		/// <summary>
		/// СЧА конечная
		/// </summary>
		public decimal Sk { get; set; }
		public decimal R { get; set; }
		public decimal V { get; set; }

		private decimal denominator { get { return (S0 + Sn - Sm); } }

		public decimal D { get { return Sc * K; } }

		/// <summary>
		/// Предельные расходы УК
		/// </summary>
		public decimal F070SumMax { get; set; }

		/// <summary>
		/// Фактические расходы УК
		/// </summary>
		public decimal F070SumFact { get; set; }

		/// <summary>
		/// Предельные расходы УК предидущий год
		/// </summary>
		public decimal F070SumMaxOld { get; set; }

		/// <summary>
		/// Фактические расходы УК предидущий год
		/// </summary>
		public decimal F070SumFactOld { get; set; }

		/// <summary>
		/// Вознаграждение УК предидущий год
		/// </summary>
		public decimal F070RewardOld { get; set; }
		

		public decimal RatioPlus
		{
			get { return denominator != 0 ? ((Sk - D) / denominator) : 0; }
		}

		public decimal RatioMinus
		{
			get { return denominator != 0 ? ((R + V) / denominator) : 0; }
		}

	}
}
