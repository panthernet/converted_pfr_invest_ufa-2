﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PFR_INVEST.DataObjects.Reports
{
	public class ReportDepositsPrompt
	{

		public DateTime? DateFrom { get; set; }
		public DateTime? DateTo { get; set; }

		public bool UseSettleDate { get; set; }
		public bool UseReturnDate { get; set; }
	}
}
