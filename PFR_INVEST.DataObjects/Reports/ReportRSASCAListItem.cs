﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PFR_INVEST.DataObjects.Reports
{
	public class ReportRSASCAListItem
	{

		public ReportRSASCAListItem()
		{
		}

		public string UKName { get; set; }
		public string Portfolio { get; set; }
		public string ContractNumber { get; set; }
		public DateTime? ContractDate { get; set; }

		public decimal? R010 { get; set; }
		public decimal? R020 { get; set; }
		public decimal? R030 { get; set; }
		public decimal? R040 { get; set; }
		public decimal? R050 { get; set; }
		public decimal? R060 { get; set; }
		public decimal? R070 { get; set; }
		public decimal? R080 { get; set; }
		public decimal? R090 { get; set; }
		public decimal? R100 { get; set; }
		public decimal? R110 { get; set; }
		public decimal? R120 { get; set; }
		public decimal? R081 { get; set; }
		public decimal? R130 { get; set; }
		public decimal? R131 { get; set; }
		public decimal? R132 { get; set; }
		public decimal? R133 { get; set; }
		public decimal? R140 { get; set; }


		public decimal? S010 { get; set; }
		public decimal? S020 { get; set; }
		public decimal? S030 { get; set; }
		public decimal? S032 { get; set; }
		public decimal? S033 { get; set; }
		public decimal? S034 { get; set; }
		public decimal? S035 { get; set; }
		public decimal? S037 { get; set; }
		public decimal? S038 { get; set; }
		public decimal? S036 { get; set; }
		public decimal? S039 { get; set; }
		public decimal? S040 { get; set; }
		public decimal? S041 { get; set; }
		public decimal? S042 { get; set; }
		public decimal? S043 { get; set; }
		public decimal? S050 { get; set; }
		public decimal? S070 { get; set; }
		public decimal? S071 { get; set; }
		public decimal? S072 { get; set; }
		public decimal? S073 { get; set; }
		public decimal? S074 { get; set; }
		public decimal? S075 { get; set; }
		public decimal? S080 { get; set; }
		public decimal? S090 { get; set; }


		public decimal? A010_010 { get { return (R010 ?? 0) == 0 ? (S010 ?? 0) : R010; } }
		public decimal? A020_020 { get { return (R020 ?? 0) == 0 ? (S020 ?? 0) : R020; } }
		public decimal? A000_030 { get { return S030 ?? 0; } }
		public decimal? A030_000 { get { return R030 ?? 0; } }
		public decimal? A040_000 { get { return R040 ?? 0; } }
		public decimal? A050_000 { get { return R050 ?? 0; } }
		public decimal? A060_032 { get { return (R060 ?? 0) == 0 ? (S032 ?? 0) : R060; } }
		public decimal? A070_033 { get { return (R070 ?? 0) == 0 ? (S033 ?? 0) : R070; } }
		public decimal? A080_034 { get { return (R080 ?? 0) == 0 ? (S034 ?? 0) : R080; } }
		public decimal? A090_035 { get { return (R090 ?? 0) == 0 ? (S035 ?? 0) : R090; } }
		public decimal? A100_037 { get { return (R100 ?? 0) == 0 ? (S037 ?? 0) : R100; } }
		public decimal? A110_038 { get { return (R110 ?? 0) == 0 ? (S038 ?? 0) : R110; } }
		public decimal? A120_036 { get { return (R120 ?? 0) == 0 ? (S036 ?? 0) : R120; } }
		public decimal? A081_039 { get { return (R081 ?? 0) == 0 ? (S039 ?? 0) : R081; } }
		public decimal? A130_040 { get { return (R130 ?? 0) == 0 ? (S040 ?? 0) : R130; } }
		public decimal? A131_041 { get { return (R131 ?? 0) == 0 ? (S041 ?? 0) : R131; } }
		public decimal? A132_042 { get { return (R132 ?? 0) == 0 ? (S042 ?? 0) : R132; } }
		public decimal? A133_043 { get { return (R133 ?? 0) == 0 ? (S043 ?? 0) : R133; } }
		public decimal? A000_050 { get { return S050 ?? 0; } }
		public decimal? A000_070 { get { return S070 ?? 0; } }
		public decimal? A000_071 { get { return S071 ?? 0; } }
		public decimal? A000_072 { get { return S072 ?? 0; } }
		public decimal? A000_073 { get { return S073 ?? 0; } }
		public decimal? A000_074 { get { return S074 ?? 0; } }
		public decimal? A000_075 { get { return S075 ?? 0; } }
		public decimal? A000_080 { get { return S080 ?? 0; } }

		public decimal? A140_000
		{
			get
			{return A010_010 + A020_020 + A030_000 + A040_000 + A050_000 + A060_032 + A070_033 + A080_034 + A090_035 + A100_037 + A110_038 + A120_036 + A130_040
					   + A000_050;
			}
		}
		public decimal? A000_090
		{
			get
			{
                //на момент импорта документов чистых активов строка 81 еще просто не была добавлена - я помню, что ее добавляли допом, 
                //и скорее всего в этих значениях, т.е. в строке 30(СЧА) она просто не учтена
                //http://jira.vs.it.ru/browse/DOKIPIV-389
				return A140_000 //A010_010 + A020_020 + A000_030 + A130_040 + A000_050
						- A000_080 + A081_039;
			}
		}


	}
}
