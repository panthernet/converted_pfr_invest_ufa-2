﻿using System;

namespace PFR_INVEST.DataObjects.Reports
{
    public class UKListReportItem
    {
        public int IdUK { get; set; }
        public int IsOldName { get; set; }
        public string FName { get; set; }
        public string SName { get; set; }
        public string INN { get; set; }
        public string SupagrNum { get; set; }
        public string PName { get; set; }
        public string ContrNum { get; set; }
        public DateTime? ContrEnd { get; set; }
        public DateTime? ContrStart { get; set; }
        public string PfName { get; set; }
        
    }
}
