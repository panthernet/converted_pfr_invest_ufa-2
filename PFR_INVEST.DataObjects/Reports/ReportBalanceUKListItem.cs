﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PFR_INVEST.DataObjects.Reports
{
	public class ReportBalanceUKListItem
	{
		public string UKName { get; set; }
		public string UKType { get; set; }
		public string ContractNumber { get; set; }
		public DateTime ContractDate { get; set; }
		public long ContractID { get; set; }


		public decimal ToUKTotal { get; set; }
		public decimal ToUKDSV { get; set; }
		public decimal ToUKMSK { get; set; }

		public decimal ReturnTotal { get; set; }
		public decimal ReturnTotalInvest { get; set; }

		public decimal ReturnMart { get; set; }
		public decimal ReturnMartInvest { get; set; }

		public decimal ReturnMSK { get; set; }
		public decimal ReturnMSKInvest { get; set; }

		public decimal ReturnPravopr { get; set; }
		public decimal ReturnPravoprInvest { get; set; }

		public decimal ReturnDSV { get; set; }
		public decimal ReturnDSVInvest { get; set; }

		public decimal ReturnSPNTotal { get { return ReturnSPN_NCTP + ReturnSPN_SPV + ReturnSPN_EV; } }
		public decimal ReturnSPNTotalInvest { get { return ReturnSPN_NCTPInvest + ReturnSPN_SPVInvest + ReturnSPN_EVInvest; } }
		public decimal ReturnSPN_NCTP { get; set; }
		public decimal ReturnSPN_NCTPInvest { get; set; }
		public decimal ReturnSPN_SPV { get; set; }
		public decimal ReturnSPN_SPVInvest { get; set; }
		public decimal ReturnSPN_EV { get; set; }
		public decimal ReturnSPN_EVInvest { get; set; }

		public decimal Saldo { get; set; }


		public ReportBalanceUKListItem() { }

	}
}
