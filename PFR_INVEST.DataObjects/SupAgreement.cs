﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class SupAgreement : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual string Number { get; set; }

        [DataMember]
        public virtual long? ContractID { get; set; }

        [DataMember]
        public virtual string Kind { get; set; }

        [DataMember]
        public virtual DateTime? RegDate { get; set; }

        [DataMember]
        public virtual long? Extention { get; set; }

        [DataMember]
        public virtual DateTime? SupendDate { get; set; }

    }
}
