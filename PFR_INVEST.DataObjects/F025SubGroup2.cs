﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    using PFR_INVEST.DataObjects.Interfaces;

    public class F025SubGroup2 : BaseDataObject, IF025Part
	{
		[DataMember]
		public virtual long ID { get; set; }
		[DataMember]
		public virtual long? EdoID { get; set; }
		[DataMember]
		public virtual string IssuerName { get; set; }
		[DataMember]
		public virtual decimal? Amount { get; set; }
		[DataMember]
		public virtual string StateRegNum { get; set; }

	}
}
