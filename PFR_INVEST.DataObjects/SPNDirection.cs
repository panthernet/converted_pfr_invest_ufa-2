﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class SPNDirection : BaseDataObject, IIdentifiable
    {
        [DataMember]
        public virtual long ID { get; set; }
        
        [DataMember]
        public virtual string Name { get; set; }

        
        public virtual bool? isFromUKToPFR
        {
            get
            {
                if (string.IsNullOrEmpty(Name))
                    return null;
                return Name.ToLower().Trim().Contains("в пфр");
            }
        }
    }
}
