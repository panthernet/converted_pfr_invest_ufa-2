﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class Curs : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long? CurrencyID { get; set; }

        [DataMember]
        public virtual decimal? Value { get; set; }

        [DataMember]
        public virtual DateTime? Date { get; set; }
    }
}
