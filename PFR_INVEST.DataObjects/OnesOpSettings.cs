﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class OnesOpSettings : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long SetDefID { get; set; }

        [DataMember]
        public virtual long OperationContentID { get; set; }

        [DataMember]
        public virtual long PortfolioTypeID { get; set; }

        [DataMember]
        public virtual string Comment { get; set; }
    }
}
