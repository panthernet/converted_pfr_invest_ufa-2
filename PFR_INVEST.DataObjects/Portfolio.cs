﻿using System.Runtime.Serialization;
using System.Diagnostics;
using System;

namespace PFR_INVEST.DataObjects
{
	[DebuggerDisplay("'{Year}' - '{Type}'")]
	public class Portfolio : BaseDataObject, IMarkedAsDeleted, IIdentifiable
	{
		/// <summary>
		/// Тип портфеля (ELEMENT 108)
		/// </summary>
		public enum Types: long
		{
			[Obsolete("Не использовать, техническое значение для сериализации", true)]
			Unknown = 0,

			NoType = 108000,
			SPN = 108001,
			DSV = 108002,
			ROPS = 108003
		}


		[DataMember]
		public virtual long ID { get; set; }

		/// <summary>
		/// Наименование портфеля, год тут ни при чем
		/// Нам просто повезло со схемой БД:)
		/// </summary>
		[DataMember]
		public virtual string Year { get; set; }

		/// <summary>
		/// Используется только для вычитки значения из базы
		/// </summary>
		[DataMember]		
		public virtual string Type { get; set; }

		[DataMember]
		public virtual long StatusID { get; set; }

		/// <summary>
		/// Реальный ID года из таблицы YEARS 
		/// Нам просто повезло со схемой БД:)
		/// </summary>
		[DataMember]
		public virtual long? YearID { get; set; }

		[IgnoreDataMember]
		public virtual Types TypeEl
		{
			get { return (Types)this.TypeID; }
			set { this.TypeID = (long)value; }
		}

		[DataMember]
		public virtual long TypeID { get; set; }

		[IgnoreDataMember]
		public virtual string Name
		{
			get { return Year; }
			set { Year = value; }
		}
	}
}
