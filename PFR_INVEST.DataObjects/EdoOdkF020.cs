﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
	public class EdoOdkF020 : BaseDataObject
	{
		[DataMember]
		public virtual long ID { get; set; }		

		[DataMember]
		public virtual DateTime? ReportDate { get; set; }
		[DataMember]
		public virtual DateTime? ReportOnDate { get; set; }
		[DataMember]
		public virtual long? ReportType { get; set; }
		[DataMember]
		public virtual string Status { get; set; }
		[DataMember]
		public virtual string Portfolio { get; set; }
		[DataMember]
		public virtual decimal? Funds1 { get; set; }
		[DataMember]
		public virtual decimal? Funds2 { get; set; }
		[DataMember]
		public virtual decimal? Funds3 { get; set; }
		[DataMember]
		public virtual decimal? Fundscur1 { get; set; }
		[DataMember]
		public virtual decimal? Fundscur2 { get; set; }
		[DataMember]
		public virtual decimal? Fundscur3 { get; set; }
		[DataMember]
		public virtual decimal? Deprur1 { get; set; }
		[DataMember]
		public virtual decimal? Deprur2 { get; set; }
		[DataMember]
		public virtual decimal? Deprur3 { get; set; }
		[DataMember]
		public virtual decimal? Gcbrf1 { get; set; }
		[DataMember]
		public virtual decimal? Gcbrf2 { get; set; }
		[DataMember]
		public virtual decimal? Gcbrf3 { get; set; }
		[DataMember]
		public virtual decimal? Gcbrfincur1 { get; set; }
		[DataMember]
		public virtual decimal? Gcbrfincur2 { get; set; }
		[DataMember]
		public virtual decimal? Gcbrfincur3 { get; set; }
		[DataMember]
		public virtual decimal? Gcbsub1 { get; set; }
		[DataMember]
		public virtual decimal? Gcbsub2 { get; set; }
		[DataMember]
		public virtual decimal? Gcbsub3 { get; set; }
		[DataMember]
		public virtual decimal? Gcbsubcur1 { get; set; }
		[DataMember]
		public virtual decimal? Gcbsubcur2 { get; set; }
		[DataMember]
		public virtual decimal? Gcbsubcur3 { get; set; }
		[DataMember]
		public virtual decimal? Stockmo1 { get; set; }
		[DataMember]
		public virtual decimal? Stockmo2 { get; set; }
		[DataMember]
		public virtual decimal? Stockmo3 { get; set; }
		[DataMember]
		public virtual decimal? Stockmoho1 { get; set; }
		[DataMember]
		public virtual decimal? Stockmoho2 { get; set; }
		[DataMember]
		public virtual decimal? Stockmoho3 { get; set; }
		[DataMember]
		public virtual decimal? Stockmohoincur1 { get; set; }
		[DataMember]
		public virtual decimal? Stockmohoincur2 { get; set; }
		[DataMember]
		public virtual decimal? Stockmohoincur3 { get; set; }
		[DataMember]
		public virtual decimal? Akciire1 { get; set; }
		[DataMember]
		public virtual decimal? Akciire2 { get; set; }
		[DataMember]
		public virtual decimal? Akciire3 { get; set; }
		[DataMember]
		public virtual decimal? Pai1 { get; set; }
		[DataMember]
		public virtual decimal? Pai2 { get; set; }
		[DataMember]
		public virtual decimal? Pai3 { get; set; }
		[DataMember]
		public virtual decimal? Profi1 { get; set; }
		[DataMember]
		public virtual decimal? Profi2 { get; set; }
		[DataMember]
		public virtual decimal? Profi3 { get; set; }
		[DataMember]
		public virtual decimal? Procdolg1 { get; set; }
		[DataMember]
		public virtual decimal? Procdolg2 { get; set; }
		[DataMember]
		public virtual decimal? Procdolg3 { get; set; }
		[DataMember]
		public virtual decimal? Otherdolg1 { get; set; }
		[DataMember]
		public virtual decimal? Otherdolg2 { get; set; }
		[DataMember]
		public virtual decimal? Otherdolg3 { get; set; }
		[DataMember]
		public virtual decimal? Otheractiv1 { get; set; }
		[DataMember]
		public virtual decimal? Otheractiv2 { get; set; }
		[DataMember]
		public virtual decimal? Otheractiv3 { get; set; }
		[DataMember]
		public virtual decimal? Sumactiv1 { get; set; }
		[DataMember]
		public virtual decimal? Sumactiv3 { get; set; }
		[DataMember]
		public virtual string PersonUK { get; set; }
		[DataMember]
		public virtual string PersonSpecDep { get; set; }
		[DataMember]
		public virtual string RegNumberOut { get; set; }
		[DataMember]
		public virtual long? ContractId { get; set; }
		[DataMember]
		public virtual decimal? Cbipoteka1 { get; set; }
		[DataMember]
		public virtual decimal? Cbipoteka2 { get; set; }
		[DataMember]
		public virtual decimal? Cbipoteka3 { get; set; }
		[DataMember]
		public virtual decimal? Cbipotekagrf1 { get; set; }
		[DataMember]
		public virtual decimal? Cbipotekagrf2 { get; set; }
		[DataMember]
		public virtual decimal? Cbipotekagrf3 { get; set; }
		[DataMember]
		public virtual decimal? DebDolgItogo1 { get; set; }
		[DataMember]
		public virtual decimal? DebDolgItogo2 { get; set; }
		[DataMember]
		public virtual decimal? DebDolgItogo3 { get; set; }
		[DataMember]
		public virtual string RegNum { get; set; }
		[DataMember]
		public virtual DateTime? RegDate { get; set; }
	}
}
