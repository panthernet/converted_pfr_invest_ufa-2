﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
	[DataContract]
    public class Finregister : BaseDataObject, IMarkedAsDeleted
    {
        [DataMember]
        public virtual int IsTransfered { get; set; }

        [DataMember]
        public virtual long ID { get; set; }
        
        [DataMember]
        public virtual long? RegisterID { get; set; }
        
        [DataMember]
        public virtual DateTime? Date { get; set; }

        [DataMember]
        public virtual decimal? Count { get; set; }

		[DataMember]
		public virtual decimal? CFRCount { get; set; }

        [DataMember]
        public virtual long CrAccID { get; set; }

        [DataMember]
        public virtual long DbtAccID { get; set; }

        [DataMember]
        public virtual string Status { get; set; }

        [DataMember]
        public virtual DateTime? LetterDate { get; set; }

        [DataMember]
        public virtual string LetterNum { get; set; }

        [DataMember]
        public virtual long? ZLCount { get; set; }

        [DataMember]
        public virtual DateTime? FinDate { get; set; }

        [DataMember]
        public virtual string FinNum { get; set; }

        [DataMember]
        public virtual string FinMoveType { get; set; }

        [DataMember]
        public virtual string RegNum { get; set; }

        [DataMember]
        public virtual long? ParentFinregisterID { get; set; }

        [DataMember]
        public virtual long? SelectedContentID { get; set; }
        
        [DataMember]
        public virtual long? SelectedRegisterID { get; set; }

		[DataMember]
		public virtual string Comment { get; set; }

        [DataMember]
	    public virtual long StatusID { get; set; }

	    public Finregister()
	    {
	        Count = 0;
	    }
    }
}
