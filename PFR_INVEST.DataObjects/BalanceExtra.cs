﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    [DataContract]
    public class BalanceExtra
    {
        [DataMember]
        public virtual long ID{ get; set; }

        [DataMember]
        public virtual long PortfolioID { get; set; }

        [DataMember]
        public virtual DateTime OperationDate { get; set; }
        
        [DataMember]
        public virtual DateTime DiDate { get; set; }

        [DataMember]
        public virtual DateTime ReturnDate { get; set; }

        [DataMember]
        public virtual string OperationName { get; set; }


        [DataMember]
        public virtual decimal? Amount { get; set; }

        [DataMember]
        public virtual decimal? VAmount { get; set; }

        [DataMember]
        public virtual long CurrencyID { get; set; }

        [DataMember]
        public virtual long PfrBankAccountID { get; set; }

        [DataMember]
        public virtual string Comment { get; set; }

        [DataMember]
        public virtual string RegNumber { get; set; }

    }
}
