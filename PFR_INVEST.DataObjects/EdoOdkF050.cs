﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class EdoOdkF050 : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long? ContractId { get; set; }

        [DataMember]
        public virtual DateTime? ReportDate { get; set; }

        [DataMember]
        public virtual decimal? GCB1 { get; set; }

        [DataMember]
        public virtual decimal? GCB2 { get; set; }

        [DataMember]
        public virtual decimal? GCBSubRF1 { get; set; }

        [DataMember]
        public virtual decimal? GCBSubRF2 { get; set; }

        [DataMember]
        public virtual decimal? ObligaciiMO1 { get; set; }

        [DataMember]
        public virtual decimal? ObligaciiMO2 { get; set; }

        [DataMember]
        public virtual decimal? ObligaciiRHO1 { get; set; }

        [DataMember]
        public virtual decimal? ObligaciiRHO2 { get; set; }

        [DataMember]
        public virtual decimal? AkciiRAO1 { get; set; }

        [DataMember]
        public virtual decimal? AkciiRAO2 { get; set; }

        [DataMember]
        public virtual decimal? Pai1 { get; set; }

        [DataMember]
        public virtual decimal? Pai2 { get; set; }

        [DataMember]
        public virtual decimal? Ipotech1 { get; set; }

        [DataMember]
        public virtual decimal? Ipotech2 { get; set; }

        [DataMember]
        public virtual decimal? ObligMFO1 { get; set; }

        [DataMember]
        public virtual decimal? ObligMFO2 { get; set; }

        [DataMember]
        public virtual decimal? ItogoCB1 { get; set; }

        [DataMember]
        public virtual decimal? ItogoCB2 { get; set; }

        [DataMember]
        public virtual string Chief { get; set; }

        [DataMember]
        public virtual string ResPerson { get; set; }

        [DataMember]
        public virtual string Post { get; set; }

        [DataMember]
        public virtual string RegNum { get; set; }

        [DataMember]
        public virtual DateTime? RegDate { get; set; }

        [DataMember]
        public virtual string RegNumOut { get; set; }

        [DataMember]
        public virtual string Portfolio { get; set; }
    }
}
