﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using PFR_INVEST.DataObjects.Interfaces;


namespace PFR_INVEST.DataObjects
{
    public class Board : BaseDataObject, IIdentifiable
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual string BoardId { get; set; }

        [DataMember]
        public virtual string BoardName { get; set; }

       /* public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "BoardId": return BoardId.ValidateNonEmpty() ?? BoardId.ValidateMaxLength(32);
                    case "BoardName": return BoardName.ValidateNonEmpty() ?? BoardName.ValidateMaxLength(64);
                    default:
                        return base[columnName];
                }

            }
        }
        */
    }
}