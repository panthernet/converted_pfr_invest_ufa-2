﻿using System;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;

namespace PFR_INVEST.DataObjects
{
    public class KBK : BaseDataObject,IMarkedAsDeleted, IIdentifiable
    {
        public const string EMPTY = "(не указан)";

        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        private string _code;
        public virtual string Code
        {
            get { return _code; }
            set { if (_code != value) { 
                _code = value; 
                OnPropertyChanged("Code"); 
            } }
        }

        [DataMember]
        private string _name;
        public virtual string Name
        {
            get { return _name; }
            set { if (_name != value) { _name = value; OnPropertyChanged("Name"); } }
        }

        [DataMember]
        private string _comment;
        public virtual string Comment
        {
            get { return _comment; }
            set { if (_comment != value) { _comment = value; OnPropertyChanged("Comment"); } }
        }

        [IgnoreDataMember]
        public virtual string MaskedCode
        {
            get
            {
                if (_code == null || _code == EMPTY || !Regex.IsMatch(_code, "\\d{20}")) return _code;
                var p1 = Convert.ToInt64(_code.Substring(0, 10));
                var p2 = Convert.ToInt64(_code.Substring(10, 10));

                return $"{p1:000 0 00 0000}{p2:0 00 0000 000}";
            }
            set
            {
                string v = null;
                if (value != null)
                {
                    v = (value == EMPTY ? value : value.Replace(" ", ""));
                }

                if (_code != v)
                {
                    _code = v;
                    OnPropertyChanged("Code");
                    OnPropertyChanged("MaskedCode");
                }
            }
        }

		[DataMember]
		public virtual long StatusID { get; set; }
	}
}
