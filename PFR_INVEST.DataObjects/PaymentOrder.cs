﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
	/// <summary>
	/// Платежное поручение по депозиту
	/// </summary>
	public class PaymentOrder : BaseDataObject, IMarkedAsDeleted
	{
		// Element (Key = 101)
		public enum Directions
		{
			/// <summary>
			/// Приход
			/// </summary>
			ToPFR = 101001,
			/// <summary>
			/// Расход
			/// </summary>
			FromPFR = 101002
		}

		/// <summary>
		/// Element (Key = 111)
		/// </summary>
		public enum DocumentType
		{
			/// <summary>
			/// Размещение
			/// </summary>
			Allocation = 111001,

			/// <summary>
			/// Возврат
			/// </summary>
			Refund = 111002,

			/// <summary>
			/// Процент по депозитам
			/// </summary>
			Percent = 111003
		}

		public PaymentOrder()
		{
			DIDate = DateTime.Today;
		}

		/// <summary>
		/// Идентификатор
		/// </summary>
		[DataMember]
		public virtual long ID { get; set; }

		/// <summary>
		/// Дата импорта
		/// </summary>
		[DataMember]
		public virtual DateTime? DIDate { get; set; }

		/// <summary>
		/// Дата зачисления/списания
		/// </summary>
		[DataMember]
		public virtual DateTime? PayerIssueDate { get; set; }

		/// <summary>
		/// Ссылка на направление передачи (ELEMENT)
		/// </summary>
		[DataMember]
		public virtual long? DirectionElID { get; set; }

		/// <summary>
		/// Номер ПП
		/// </summary>
		[DataMember]
		public virtual string DraftRegNum { get; set; }

		/// <summary>
		/// Дата ПП
		/// </summary>
		[DataMember]
		public virtual DateTime? DraftDate { get; set; }

		/// <summary>
		/// Сумма ПП
		/// </summary>
		[DataMember]
		public virtual decimal? DraftAmount { get; set; }

		/// <summary>
		/// Комментарий
		/// </summary>
		[DataMember]
		public virtual string Comment { get; set; }

		/// <summary>
		/// Ссылка на банк (LEGALENTITY)
		/// </summary>
		[DataMember]
		public virtual long? BankID { get; set; }

		/// <summary>
		/// Ссылка на тип документа (ELEMENT)
		/// </summary>
		[DataMember]
		public virtual long? DocumentTypeElID { get; set; }

		/// <summary>
		/// Статус записи
		/// </summary>
		[DataMember]
		public virtual long StatusID { get; set; }
	}
}
