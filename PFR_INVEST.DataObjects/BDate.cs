﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class BDate : BaseDataObject, ILoadedFromDBF
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual DateTime? TradeDate { get; set; }

        [DataMember]
        public virtual long? BoardID { get; set; }

        [DataMember]
        public virtual int? Duration { get; set; }

        [DataMember]
        public virtual decimal? Open { get; set; }

        [DataMember]
        public virtual long? SecurityID { get; set; }

        [DataMember]
        public virtual decimal? Low { get; set; }

        [DataMember]
        public virtual decimal? High { get; set; }

        [DataMember]
        public virtual decimal? Close { get; set; }

        [DataMember]
        public virtual decimal? WAPrice { get; set; }

        [DataMember]
        public virtual decimal? TrendClose { get; set; }

        [DataMember]
        public virtual decimal? TrendWAP { get; set; }

        [DataMember]
        public virtual decimal? HighBID { get; set; }

        [DataMember]
        public virtual decimal? LowOffer { get; set; }

		[DataMember]
        
		public virtual decimal? BID { get; set; }

        [DataMember]
        public virtual decimal? Offer { get; set; }

        [DataMember]
        public virtual decimal? YieldClose { get; set; }

        [DataMember]
        public virtual decimal? YieldAtWAP { get; set; }

        [DataMember]
        public virtual decimal? Volume { get; set; }

        [DataMember]
        public virtual decimal? Value { get; set; }

        [DataMember]
        public virtual decimal? NumberOfTrades { get; set; }

        [DataMember]
        public virtual decimal? MarketPrice1 { get; set; }

        [DataMember]
        public virtual decimal? MarketPrice2 { get; set; }

        #region Поля для отображения без сохранения
        [DataMember]
        public virtual string BoardIdText { get; set; }
        public virtual string BoardName { get; set; }
        public virtual string SecurityIdText { get; set; }
        public virtual string SecRegNum { get; set; }
        public virtual DateTime? SecRepayDate { get; set; }
        public virtual string SecName { get; set; }

        #endregion
    }
}
