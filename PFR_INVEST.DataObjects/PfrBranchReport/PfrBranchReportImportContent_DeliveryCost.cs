﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataObjects;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.BranchReport
{
    public class PfrBranchReportImportContent_DeliveryCost : PfrBranchReportImportContentBase
    {
        [DataMember]
        public PfrBranchReportDeliveryCost DeliveryCost;
    }
}
