﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    using System;

    using PFR_INVEST.DataObjects.Interfaces;

    public class F026Group7 : BaseDataObject, IF026Part
	{
		[DataMember]
		public virtual long ID { get; set; }
		[DataMember]
		public virtual long? EdoID { get; set; }
		[DataMember]
		public virtual string MarketPriceSource { get; set; }
		[DataMember]
		public virtual string StateRegNum { get; set; }
		[DataMember]
		public virtual decimal? MarketPrice { get; set; }
		[DataMember]
		public virtual decimal? Quantity { get; set; }
		[DataMember]
		public virtual decimal? Amount { get; set; }
		[DataMember]
		public virtual string MunicipalName { get; set; }
	}
}
