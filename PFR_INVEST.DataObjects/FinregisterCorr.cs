﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    
    public class FinregisterCorr : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual string Num { get; set; }

        [DataMember]
        public virtual string FinregisterNum { get; set; }

        [DataMember]
        public virtual DateTime? FinregisterDate { get; set; }

        [DataMember]
        public virtual decimal? CurrentSum { get; set; }

        [DataMember]
        public virtual long? FinregisterID { get; set; }
    }
}
