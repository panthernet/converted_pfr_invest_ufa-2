﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class PfrBranchReportCertificationAgreementType : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual string Name { get; set; }

        [IgnoreDataMember]
        public virtual enRowType Type
        {
            get
            {
                switch (Name.ToUpper())
                {
                    case "NPF_SUM": return enRowType.NpfSum;
                    case "NPF": return enRowType.Npf;
                    case "CREDIT": return enRowType.Credit;
                    case "EMPLOYER": return enRowType.Employer;
                    case "COMMUNICATION": return enRowType.Communication;
                    case "SUM_BY_OPFR": return enRowType.SumByOPFR;
                }

                return enRowType.Empty;
            }
        }

        public enum enRowType
        {
            Empty = 0,
            NpfSum = 1,
            Npf = 2,
            Credit = 3,
            Employer = 4,
            Communication = 5,
            SumByOPFR = 6
        }
    }
}
