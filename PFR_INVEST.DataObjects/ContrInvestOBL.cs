﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class ContrInvestOBL : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual string Year { get; set; }

        private long? m_Max;
        [DataMember]
        public virtual long? Max
        {
            get
            {
                return m_Max;
            }
            set
            {
                m_Max = value;
                OnPropertyChanged("Max");
            }
        }

        private long? m_Min;
        [DataMember]
        public virtual long? Min
        {
            get
            {
                return m_Min;
            }
            set
            {
                m_Min = value;
                OnPropertyChanged("Min");
            }
        }

        [DataMember]
        public virtual long? ContrInvestID { get; set; }
    }
}
