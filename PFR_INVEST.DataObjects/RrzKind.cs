﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
	public class RrzKind:BaseDataObject, IIdentifiable
	{
		[DataMember]
		public virtual long ID { get; set; }

		[DataMember]
		public virtual string Name { get; set; }
	}
}
