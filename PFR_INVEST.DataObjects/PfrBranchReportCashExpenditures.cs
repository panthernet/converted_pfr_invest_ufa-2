﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class PfrBranchReportCashExpenditures : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long BranchReportID { get; set; }

        [DataMember]
        public virtual decimal Sum_KBK_392_10_06_505_23_01_244_226 { get; set; }

        [DataMember]
        public virtual decimal Sum_KBK_392_10_06_505_54_02_244_226_by400 { get; set; }

        [DataMember]
        public virtual decimal Sum_KBK_392_10_06_505_54_02_244_226_by472 { get; set; }
    }
}
