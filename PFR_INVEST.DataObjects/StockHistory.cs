﻿using System;
using System.Runtime.Serialization;
using PFR_INVEST.DataObjects.Interfaces;

namespace PFR_INVEST.DataObjects
{
    [DataContract(IsReference = true)]
    public class StockHistory: BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long StockID { get; set; }

        [DataMember]
        public virtual string Name { get; set; }

        [DataMember]
        public virtual string FullName { get; set; }

        [DataMember]
        public virtual DateTime? UpdateDate { get; set; }
    }
}
