﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class RatingAgency : BaseDataObject, IMarkedAsDeleted, IIdentifiable
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember] 
        private string _name;
        public virtual string Name
        {
            get { return _name; }
            set { _name = value; OnPropertyChanged("Name"); }
        }

        [DataMember] 
        private string _agencyType;
        public virtual string AgencyType
        {
            get { return _agencyType; }
            set { _agencyType = value; OnPropertyChanged("AgencyType"); }

        }
        [DataMember]
        private long _statusID = 1;
        public virtual long StatusID
        {
            get { return _statusID; }
            set
            {
                if (_statusID != value)
                {
                    _statusID = value;
                    OnPropertyChanged("StatusID");
                }
            }
        }
    }
}
