﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class LegalEntityCourier : BaseDataObject, IEquatable<LegalEntityCourier>
    {
        public enum Types
        {
            Courier = 1,
            Contact = 2,
            Person = 3,
        }

        public enum AlertTypes
        {
            NotAlerted = 0,
            Alert2Weeks = 1,
            Alert1Week = 2,
        }

        public LegalEntityCourier()
        {
            StatusID = 1;
        }

        public LegalEntityCourier(LegalEntityCourier x)
            : this()
        {
            ApplyValues(x);
        }


        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        private long _legalEntityID;
        public virtual long LegalEntityID
        {
            get { return _legalEntityID; }
            set { if (_legalEntityID != value) { _legalEntityID = value; OnPropertyChanged("LegalEntityID"); } }
        }

        [DataMember]
        private string _legalEntityName;
        public virtual string LegalEntityName
        {
            get { return _legalEntityName; }
            set { if (_legalEntityName != value) { _legalEntityName = value; OnPropertyChanged("LegalEntityName"); } }
        }

        [DataMember]
        private long _statusID;
        public virtual long StatusID
        {
            get { return _statusID; }
            set { if (_statusID != value) { _statusID = value; OnPropertyChanged("StatusID"); } }
        }

        [DataMember]
        private string _firstName;
        public virtual string FirstName
        {
            get { return _firstName; }
            set { if (_firstName != value) { _firstName = value; OnPropertyChanged("FirstName"); } }
        }
        [IgnoreDataMember]
        public virtual string FirstNameTrim
        {
            get { return XTrim(FirstName); }
            set { FirstName = XTrim(value); }
        }

        [DataMember]
        private string _lastName;
        public virtual string LastName
        {
            get { return _lastName; }
            set { if (_lastName != value) { _lastName = value; OnPropertyChanged("LastName"); } }
        }
        [IgnoreDataMember]
        public virtual string LastNameTrim
        {
            get { return XTrim(LastName); }
            set { LastName = XTrim(value); }
        }

        [DataMember]
        private string _patronymicName;
        /// <summary>
        /// Отчество
        /// </summary>
        public virtual string PatronymicName
        {
            get { return _patronymicName; }
            set { if (_patronymicName != value) { _patronymicName = value; OnPropertyChanged("PatronymicName"); } }
        }
        [IgnoreDataMember]
        public virtual string PatronymicNameTrim
        {
            get { return XTrim(PatronymicName); }
            set { PatronymicName = XTrim(value); }
        }

        [DataMember]
        private string _passportSer;
        public virtual string PassportSer
        {
            get { return _passportSer; }
            set { if (_passportSer != value) { _passportSer = value; OnPropertyChanged("PassportSer"); } }
        }

        [DataMember]
        private string _passportNum;
        public virtual string PassportNum
        {
            get { return _passportNum; }
            set { if (_passportNum != value) { _passportNum = value; OnPropertyChanged("PassportNum"); } }
        }

        [DataMember]
        private string _passportIssuedBy;
        public virtual string PassportIssuedBy
        {
            get { return _passportIssuedBy; }
            set { if (_passportIssuedBy != value) { _passportIssuedBy = value; OnPropertyChanged("PassportIssuedBy"); } }
        }
        [IgnoreDataMember]
        public virtual string PassportIssuedByTrim
        {
            get { return XTrim(PassportIssuedBy); }
            set { PassportIssuedBy = XTrim(value); }
        }

        [DataMember]
        private DateTime? _passportIssueDate;
        public virtual DateTime? PassportIssueDate
        {
            get { return _passportIssueDate; }
            set { if (_passportIssueDate != value) { _passportIssueDate = value; OnPropertyChanged("PassportIssueDate"); } }
        }


        [DataMember]
        private string _address;
        public virtual string Address
        {
            get { return _address; }
            set { if (_address != value) { _address = value; OnPropertyChanged("Address"); } }
        }
        [IgnoreDataMember]
        public virtual string AddressTrim
        {
            get { return XTrim(Address); }
            set { Address = XTrim(value); }
        }

        [DataMember]
        private string _workPhone;
        public virtual string WorkPhone
        {
            get { return _workPhone; }
            set { if (_workPhone != value) { _workPhone = value; OnPropertyChanged("WorkPhone"); } }
        }

        [DataMember]
        private string _workPhoneExtension;
        public virtual string WorkPhoneExtension
        {
            get { return _workPhoneExtension; }
            set { if (_workPhoneExtension != value) { _workPhoneExtension = value; OnPropertyChanged("WorkPhoneExtension"); } }
        }

        [DataMember]
        private string _mobilePhone;
        public virtual string MobilePhone
        {
            get { return _mobilePhone; }
            set { if (_mobilePhone != value) { _mobilePhone = value; OnPropertyChanged("MobilePhone"); } }
        }

        [DataMember]
        private string _email;
        public virtual string Email
        {
            get { return _email; }
            set { if (_email != value) { _email = value; OnPropertyChanged("Email"); } }
        }


        private string _position;
        [DataMember]
        public virtual string Position
        {
            get { return _position; }
            set { if (_position != value) { _position = value; OnPropertyChanged("Position"); } }
        }


        private string _comment;
        [DataMember]
        public virtual string Comment
        {
            get { return _comment; }
            set { if (_comment != value) { _comment = value; OnPropertyChanged("Comment"); } }
        }

        private string _signature;
        [DataMember]
        public virtual string Signature
        {
            get { return _signature; }
            set { if (_signature != value) { _signature = value; OnPropertyChanged("Signature"); } }
        }

        private DateTime? _signatureExpireDate;
        [DataMember]
        public virtual DateTime? SignatureExpireDate
        {
            get { return _signatureExpireDate; }
            set { if (_signatureExpireDate != value) { _signatureExpireDate = value; OnPropertyChanged("SignatureExpireDate"); } }
        }

        private int _signatureExpireAlert;
        [DataMember]
        public virtual int SignatureExpireAlert
        {
            get { return _signatureExpireAlert; }
            set { if (_signatureExpireAlert != value) { _signatureExpireAlert = value; OnPropertyChanged("SignatureExpireAlert"); } }
        }

        [IgnoreDataMember]
        public virtual Types Type
        {
            get { return (Types)TypeID; }
            set { TypeID = (int)value; }
        }


        private int _typeID;
        [DataMember]
        public virtual int TypeID
        {
            get { return _typeID; }
            set { if (_typeID != value) { _typeID = value; OnPropertyChanged("TypeID"); } }
        }



        [DataMember]
        private string _letterOfAttorneyNumber;
        public virtual string LetterOfAttorneyNumber
        {
            get { return _letterOfAttorneyNumber; }
            set
            {
                if (_letterOfAttorneyNumber != value)
                {
                    _letterOfAttorneyNumber = value;
                    OnPropertyChanged("LetterOfAttorneyNumber"); OnPropertyChanged("LetterOfAttorneyIssueDate"); OnPropertyChanged("LetterOfAttorneyExpireDate");
                }
            }
        }

        [DataMember]
        private DateTime? _letterOfAttorneyRegisterDate;
        public virtual DateTime? LetterOfAttorneyRegisterDate
        {
            get { return _letterOfAttorneyRegisterDate; }
            set 
            { 
                if (_letterOfAttorneyRegisterDate != value) {
                _letterOfAttorneyRegisterDate = value;
                OnPropertyChanged("LetterOfAttorneyRegisterDate"); OnPropertyChanged("LetterOfAttorneyIssueDate"); OnPropertyChanged("LetterOfAttorneyExpireDate");
            }
            }
        }

        [DataMember]
        private DateTime? _letterOfAttorneyIssueDate;
        public virtual DateTime? LetterOfAttorneyIssueDate
        {
            get { return _letterOfAttorneyIssueDate; }
            set
            {
                if (_letterOfAttorneyIssueDate != value)
                {
                    _letterOfAttorneyIssueDate = value;
                    OnPropertyChanged("LetterOfAttorneyNumber"); OnPropertyChanged("LetterOfAttorneyIssueDate"); OnPropertyChanged("LetterOfAttorneyExpireDate");
                }
            }
        }

        [DataMember]
        private DateTime? _letterOfAttorneyExpireDate;
        public virtual DateTime? LetterOfAttorneyExpireDate
        {
            get { return _letterOfAttorneyExpireDate; }
            set
            {
                if (_letterOfAttorneyExpireDate != value)
                {
                    _letterOfAttorneyExpireDate = value;
                    OnPropertyChanged("LetterOfAttorneyNumber"); OnPropertyChanged("LetterOfAttorneyIssueDate"); OnPropertyChanged("LetterOfAttorneyExpireDate");
                }
            }
        }

        [DataMember]
        private int _letterOfAttorneyExpireAlert;
        public virtual int LetterOfAttorneyExpireAlert
        {
            get { return _letterOfAttorneyExpireAlert; }
            set
            {
                _letterOfAttorneyExpireAlert = value;
            }
        }

        [IgnoreDataMember]
        public virtual ILoaListProvider LoaListProvider { get; set; }

        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "FirstName":
                    case "FirstNameTrim":
                        if (string.IsNullOrEmpty(FirstName))
                            return "Поле обязательное для заполнения";
                        if (IsInvalidLength(FirstName, 100))
                            return "Максимальная длина 100 символов";
                        break;
                    case "LastName":
                    case "LastNameTrim":
                        if (string.IsNullOrEmpty(LastName))
                            return "Поле обязательное для заполнения";
                        if (IsInvalidLength(LastName, 100))
                            return "Максимальная длина 100 символов";
                        break;
                    case "Position":
                        if (Type == Types.Contact && string.IsNullOrEmpty(Position))
                            return "Поле обязательное для заполнения";
                        if (IsInvalidLength(Position, 100))
                            return "Максимальная длина 100 символов";
                        break;
                    case "Comment":
                        if (IsInvalidLengthNullable(Comment, 100))
                            return "Максимальная длина 100 символов";
                        break;
                    case "LetterOfAttorneyRegisterDate":
                        if (LetterOfAttorneyIssueDate != null && LetterOfAttorneyRegisterDate != null && LetterOfAttorneyRegisterDate < LetterOfAttorneyIssueDate)
                            return "Дата регистрации доверенности в ПФР не должна быть меньше, чем дата выдачи";
                        break;
                    case "LetterOfAttorneyIssueDate":
                    case "LetterOfAttorneyExpireDate":

                        if (LetterOfAttorneyIssueDate != null && LetterOfAttorneyExpireDate != null && LetterOfAttorneyExpireDate < LetterOfAttorneyIssueDate)
                            return "Дата прекращения действия доверенности должна быть больше даты выдачи доверенности";
                        if ((LetterOfAttorneyIssueDate != null && LetterOfAttorneyExpireDate == null) || (LetterOfAttorneyIssueDate == null && LetterOfAttorneyExpireDate != null))
                            return "Поля дата прекращения действия доверенности и дата выдачи доверенности должны быть заполнены";
                        if (LetterOfAttorneyIssueDate == null && LetterOfAttorneyRegisterDate != null)
                            return "Заполните дату выдачи";

                        if (LoaListProvider != null)
                        {
                            return LoaListProvider.ValidateNewLoaDate(this);
                        }

                        break;

                    case "PatronymicName":
                    case "PatronymicNameTrim":
                        if (IsInvalidLengthNullable(PatronymicName, 100))
                            return "Максимальная длина 100 символов";

                        break;
                    case "PassportSer":
                        if (PassportSer != null && PassportSer.Length != 4)
                            return "Допустимая длина 4 символа";
                        break;
                    case "PassportNum":
                        if (PassportNum != null && PassportNum.Length != 6)
                            return "Допустимая длина 6 символов";
                        break;
                    case "PassportIssuedBy":
                    case "PassportIssuedByTrim":
                        if (IsInvalidLengthNullable(PassportIssuedBy, 200))
                            return "Максимальная длина 200 символов";
                        break;

                    case "Address":
                    case "AddressTrim":
                        if (IsInvalidLengthNullable(Address, 200))
                            return "Максимальная длина 200 символов";
                        break;
                    case "WorkPhone":
                        if (IsInvalidLengthNullable(WorkPhone, 20))
                            return "Максимальная длина 20 символов";
                        break;
                    case "WorkPhoneExtension":
                        if (IsInvalidLengthNullable(WorkPhoneExtension, 5))
                            return "Максимальная длина 5 символов";
                        break;

                    case "MobilePhone":
                        if (IsInvalidLengthNullable(MobilePhone, 20))
                            return "Максимальная длина 20 символов";
                        break;
                    case "Email":
                        if (IsInvalidLengthNullable(Email, 100))
                            return "Максимальная длина 100 символов";
                        break;
                    case "LetterOfAttorneyNumber":
                        if (IsInvalidLengthNullable(LetterOfAttorneyNumber, 50))
                            return "Максимальная длина 50 символов";
                        if (!string.IsNullOrEmpty(LetterOfAttorneyNumber) && LetterOfAttorneyNumber.Trim().Length == 0)
                            return "№ доверенности не может быть пробелом";

                        if (LoaListProvider != null)
                        {
                            return LoaListProvider.ValidateNewLoaNumber(this);
                        }

                        break;
                }
                return null;
            }
        }

        public virtual bool IsValid()
        {
            string[] fieldNames = { "FirstName", "LastName", "LetterOfAttorneyExpireDate", "PatronymicName", "PassportSer", "PassportNum", "PassportIssuedBy", "Address", 
                                      "WorkPhone", "WorkPhoneExtension", "MobilePhone", "Email", "LetterOfAttorneyNumber", "LetterOfAttorneyRegisterDate" };
            return fieldNames.All(fieldName => string.IsNullOrEmpty(this[fieldName]));
        }


        public virtual LegalEntityCourierLetterOfAttorney GetItemLECLA()
        {
            LegalEntityCourierLetterOfAttorney res = new LegalEntityCourierLetterOfAttorney()
            {
                ID = 0,
                Number = LetterOfAttorneyNumber,
                ExpireDate = LetterOfAttorneyExpireDate,
                IssueDate = LetterOfAttorneyIssueDate,
                RegisterDate = LetterOfAttorneyRegisterDate,
                LegalEntityCourierID = LegalEntityID
            };
            return res;
        }


        private bool IsInvalidLength(string value, int maxLength)
        {
            return string.IsNullOrEmpty(value) || value.Length > maxLength;
        }

        private bool IsInvalidLengthNullable(string value, int maxLength)
        {
            return !string.IsNullOrEmpty(value) && value.Length > maxLength;
        }

        [IgnoreDataMember]
        public virtual bool IsDeleted
        {
            get { return StatusID == -1; }
        }


        public virtual void MarkDeleted()
        {
            StatusID = -1;
        }

        public virtual void ApplyValues(LegalEntityCourier x)
        {
            ID = x.ID;
            LegalEntityID = x.LegalEntityID;
            StatusID = x.StatusID;
            TypeID = x.TypeID;
            FirstName = x.FirstName;
            LastName = x.LastName;
            PatronymicName = x.PatronymicName;
            PassportSer = x.PassportSer;
            PassportNum = x.PassportNum;
            PassportIssuedBy = x.PassportIssuedBy;
            PassportIssueDate = x.PassportIssueDate;
            Address = x.Address;

            WorkPhone = x.WorkPhone;
            WorkPhoneExtension = x.WorkPhoneExtension;
            MobilePhone = x.MobilePhone;
            Email = x.Email;

            LetterOfAttorneyNumber = x.LetterOfAttorneyNumber;
            LetterOfAttorneyRegisterDate = x.LetterOfAttorneyRegisterDate;
            LetterOfAttorneyIssueDate = x.LetterOfAttorneyIssueDate;
            LetterOfAttorneyExpireDate = x.LetterOfAttorneyExpireDate;

            LetterOfAttorneyList = x.LetterOfAttorneyList == null ? null : x.LetterOfAttorneyList.Select(item => (LegalEntityCourierLetterOfAttorney)item.Clone()).ToList();
            LetterOfAttorneyListChanged = x.LetterOfAttorneyListChanged == null ? null : x.LetterOfAttorneyListChanged.Select(item => (LegalEntityCourierLetterOfAttorney)item.Clone()).ToList();
            LetterOfAttorneyListDeleted = x.LetterOfAttorneyListDeleted == null ? null : x.LetterOfAttorneyListDeleted.Select(item => (LegalEntityCourierLetterOfAttorney)item.Clone()).ToList();

            CertificatesList = x.CertificatesList == null ? null : x.CertificatesList.Select(item => (LegalEntityCourierCertificate)item.Clone()).ToList();
            CertificatesListChanged = x.CertificatesListChanged == null ? null : x.CertificatesListChanged.Select(item => (LegalEntityCourierCertificate)item.Clone()).ToList();
            CertificatesListDeleted = x.CertificatesListDeleted == null ? null : x.CertificatesListDeleted.Select(item => (LegalEntityCourierCertificate)item.Clone()).ToList(); 

            Signature = x.Signature;
            SignatureExpireDate = x.SignatureExpireDate;
        }

        private static string XTrim(string value)
        {
            return value == null ? null : value.Trim();
        }

        [IgnoreDataMember]
        private List<LegalEntityCourierCertificate> _certificatesList;
		[DataMember]
        public virtual List<LegalEntityCourierCertificate> CertificatesList
        {
            get
            {
                return _certificatesList;
            }
            set { _certificatesList = value; }
        }

        [IgnoreDataMember]
        private List<LegalEntityCourierCertificate> _certificatesListChanged;
        [DataMember]
        public virtual List<LegalEntityCourierCertificate> CertificatesListChanged
        {
            get
            {
                return _certificatesListChanged;
            }
            set { _certificatesListChanged = value; }
        }

        [IgnoreDataMember]
        private List<LegalEntityCourierCertificate> _certificatesListDeleted;
		[DataMember]
        public virtual List<LegalEntityCourierCertificate> CertificatesListDeleted
        {
            get
            {
                return _certificatesListDeleted;
            }
            set { _certificatesListDeleted = value; }
        }

        public virtual bool IsLetterOfAttorneyNeedSave(string oldNumber, DateTime? oldIssueDate, DateTime? oldExpireDate, DateTime? oldRegisterDate)
        {
            if (oldNumber == null && oldIssueDate == null && oldExpireDate == null && oldRegisterDate == null)
                return false;

            return LetterOfAttorneyNumber != oldNumber || LetterOfAttorneyIssueDate != oldIssueDate || LetterOfAttorneyExpireDate != oldExpireDate || LetterOfAttorneyRegisterDate != oldRegisterDate;
        }

        [IgnoreDataMember]
        private List<LegalEntityCourierLetterOfAttorney> _letterOfAttorneyList;
		[DataMember]
        public virtual List<LegalEntityCourierLetterOfAttorney> LetterOfAttorneyList
        {
            get
            {
                return _letterOfAttorneyList;
            }
            set { _letterOfAttorneyList = value; }
        }

        [IgnoreDataMember]
        private List<LegalEntityCourierLetterOfAttorney> _letterOfAttorneyListChanged;
		[DataMember]
        public virtual List<LegalEntityCourierLetterOfAttorney> LetterOfAttorneyListChanged
        {
            get
            {
                return _letterOfAttorneyListChanged;
            }
            set { _letterOfAttorneyListChanged = value; }
        }

        [IgnoreDataMember]
        private List<LegalEntityCourierLetterOfAttorney> _letterOfAttorneyListDeleted;
		[DataMember]
        public virtual List<LegalEntityCourierLetterOfAttorney> LetterOfAttorneyListDeleted
        {
            get
            {
                return _letterOfAttorneyListDeleted;
            }
            set { _letterOfAttorneyListDeleted = value; }
        }

        public virtual bool Equals(LegalEntityCourier x)
        {
            if (x == null)
                return false;
            if (x == this)
                return true;

            if (x.Type == Types.Person)
            {
                if ((CertificatesListChanged != null && CertificatesListChanged.Count > 0) ||
                    (CertificatesListDeleted != null && CertificatesListDeleted.Count > 0) ||
                    (LetterOfAttorneyListChanged != null && LetterOfAttorneyListChanged.Count > 0) ||
                    (LetterOfAttorneyListDeleted != null && LetterOfAttorneyListDeleted.Count > 0)) return false;
            }


            return
                x.ID == ID &&
                x.LegalEntityID == LegalEntityID &&
                x.StatusID == StatusID &&
                x.FirstName == FirstName &&
                x.LastName == LastName &&
                x.PatronymicName == PatronymicName &&
                x.PassportSer == PassportSer &&
                x.PassportNum == PassportNum &&
                x.PassportIssueDate == PassportIssueDate &&
                x.PassportIssuedBy == PassportIssuedBy &&
                x.Address == Address &&
                x.WorkPhone == WorkPhone &&
                x.WorkPhoneExtension == WorkPhoneExtension &&
                x.MobilePhone == MobilePhone &&
                x.Email == Email &&
                x.LetterOfAttorneyNumber == LetterOfAttorneyNumber &&
                x.LetterOfAttorneyRegisterDate == LetterOfAttorneyRegisterDate &&
                x.LetterOfAttorneyIssueDate == LetterOfAttorneyIssueDate &&
                x.LetterOfAttorneyExpireDate == LetterOfAttorneyExpireDate &&
                x.Signature == Signature &&
                x.SignatureExpireDate == SignatureExpireDate;
        }


        public virtual bool IsValidLetterOfAttorneyNumber(List<LegalEntityCourierLetterOfAttorney> temp)
        {
            if (string.IsNullOrEmpty(LetterOfAttorneyNumber)) return true;

            return temp.All(l => l.Number != LetterOfAttorneyNumber);
        }

        public virtual bool IsValidLetterOfAttorneyDate(List<LegalEntityCourierLetterOfAttorney> temp)
        {
            if (LetterOfAttorneyIssueDate == null && LetterOfAttorneyIssueDate == null)
                return true;

            if (!temp.Any(l => l.IssueDate.HasValue && l.ExpireDate.HasValue))
                return true;

            if (temp.Any(l => IsPeriodIntersect(LetterOfAttorneyIssueDate, LetterOfAttorneyExpireDate, l.IssueDate, l.ExpireDate)))
                return false;

            return true;
        }

        /// <summary>
        /// Copy of method from PFR_INVEST.Core.Tools.DateTools
        /// </summary>
        /// <param name="firstStart"></param>
        /// <param name="firstEnd"></param>
        /// <param name="secondStart"></param>
        /// <param name="secondEnd"></param>
        /// <returns></returns>
        public static bool IsPeriodIntersect(DateTime? firstStart, DateTime? firstEnd, DateTime? secondStart, DateTime? secondEnd)
        {
            firstStart = firstStart ?? DateTime.MinValue;
            firstEnd = firstEnd ?? DateTime.MaxValue;
            secondStart = secondStart ?? DateTime.MinValue;
            secondEnd = secondEnd ?? DateTime.MaxValue;

            if (firstStart > firstEnd)
                throw new ArgumentException("First period");
            if (secondStart > secondEnd)
                throw new ArgumentException("Second period");

            if (firstEnd <= secondStart) return false;
            if (secondEnd <= firstStart) return false;

            return true;
        }

    }

    public interface ILoaListProvider
    {
        string ValidateNewLoa(LegalEntityCourier x);
        string ValidateNewLoaDate(LegalEntityCourier x);
        string ValidateNewLoaNumber(LegalEntityCourier x);
    }
}
