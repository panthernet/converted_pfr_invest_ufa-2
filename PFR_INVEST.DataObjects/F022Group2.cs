﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    [Obsolete("Устаревший, использовать F025Group2", true)]
	public class F022Group2 : BaseDataObject, IF022Part
	{
		[DataMember]
		public virtual long ID { get; set; }
		[DataMember]
		public virtual long? EdoID { get; set; }
		[DataMember]
		public virtual string BankName { get; set; }
		[DataMember]
		public virtual DateTime? DocDate { get; set; }
		[DataMember]
		public virtual string DocNum { get; set; }
		[DataMember]
		public virtual string Account { get; set; }
		[DataMember]
		public virtual decimal? Amount { get; set; }
		[DataMember]
		public virtual decimal? Interest { get; set; }
	}
}
