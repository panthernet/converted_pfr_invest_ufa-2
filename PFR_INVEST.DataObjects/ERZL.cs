﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    
    public class ERZL : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }
        
        [DataMember] 
        public virtual string Content { get; set; }
        
        [DataMember]
        public virtual long? FZ_ID { get; set; }

        [DataMember]
        public virtual string RegNum { get; set; }

        [DataMember]
        public virtual DateTime? Date { get; set; }

        [DataMember]
        public virtual string Company { get; set; }
    }
}
