﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class OnesJournal : BaseDataObject
    {
        public enum Actions
        {
            General = 0,
            Load = 1,
            Validate = 2,
            Parse = 3,
            ExportComplete = 4,
            Export = 5
        }

        public enum LogTypes: int
        {
            Info = 0,
            Error = 1
        }

        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual int Action { get; set; }

        [DataMember]
        public virtual long SessionID { get; set; }

        [DataMember]
        public virtual long? ImportID { get; set; }

        [DataMember]
        public virtual DateTime LogDate { get; set; }

        [DataMember]
        public virtual int LogType { get; set; }

        [DataMember]
        public virtual string Message { get; set; }

        [IgnoreDataMember]
        public virtual string LogTypeName
        {
            get
            {
                switch (LogType)
                {
                    case (int)LogTypes.Info:
                        return "Информация";
                    case (int)LogTypes.Error:
                        return "Ошибка";
                    default:
                        return "";
                }
            }
        }
    }
}
