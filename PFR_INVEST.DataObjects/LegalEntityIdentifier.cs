﻿using System;
using System.Linq;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class LegalEntityIdentifier : BaseDataObject, IEquatable<LegalEntityIdentifier>
    {
        public LegalEntityIdentifier()
            : base()
        {
            StatusID = 1;
        }

        public LegalEntityIdentifier(LegalEntityIdentifier x)
            : this()
        {
            ApplyValues(x);
        }

        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        private long _legalEntityID;
        public virtual long LegalEntityID
        {
            get { return _legalEntityID; }
            set { if (_legalEntityID != value) { _legalEntityID = value; OnPropertyChanged("LegalEntityID"); } }
        }

        [DataMember]
        private string _identifier;
        public virtual string Identifier
        {
            get { return _identifier; }
            set { if (_identifier != value) { _identifier = value; OnPropertyChanged("Identifier"); } }
        }

        [DataMember]
        private string _reason;
        public virtual string Reason
        {
            get { return _reason; }
            set { if (_reason != value) { _reason = value; OnPropertyChanged("Reason"); } }
        }

        [DataMember]
        private DateTime? _setDate;
        public virtual DateTime? SetDate
        {
            get { return _setDate; }
            set { if (_setDate != value) { _setDate = value; OnPropertyChanged("SetDate"); } }
        }

        [DataMember]
        private string _comment;
        public virtual string Comment
        {
            get { return _comment; }
            set { if (_comment != value) { _comment = value; OnPropertyChanged("Comment"); } }
        }

        [DataMember]
        private long _statusID;
        public virtual long StatusID
        {
            get { return _statusID; }
            set { if (_statusID != value) { _statusID = value; OnPropertyChanged("StatusID"); } }
        }

        [DataMember]
        private string _legalEntityName;
        public virtual string LegalEntityName
        {
            get { return _legalEntityName; }
            set { if (_legalEntityName != value) { _legalEntityName = value; OnPropertyChanged("LegalEntityName"); } }
        }
     
        [IgnoreDataMember]
        public virtual ILEIdentifierListProvider LEIdentifierListProvider { get; set; }

        [IgnoreDataMember]
        private Guid _additionInfo;
        public virtual Guid AdditionInfo
        {
            get { return _additionInfo; }
            set { if (_additionInfo != value) { _additionInfo = value; OnPropertyChanged("AdditionInfo"); } }
        }

        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "Identifier":
                        if (string.IsNullOrEmpty(this.Identifier))
                            return "Поле обязательное для заполнения";
                        if (IsInvalidLength(Identifier, 4))
                            return "Максимальная длина 4 символа";

                        if (LEIdentifierListProvider != null)
                        {
                            return LEIdentifierListProvider.ValidateNewIdentifier(this);
                        }

                        break;
                    case "Reason":
                        //if (string.IsNullOrEmpty(this.Reason))
                         //   return "Поле обязательное для заполнения";
                        //if (IsInvalidLength(Reason, 250))
                        //    return "Максимальная длина 250 символов";
                        break;
                    case "Comment":
                        //if (string.IsNullOrEmpty(this.Comment))
                        //    return "Поле обязательное для заполнения";
                        //if (IsInvalidLength(Comment, 1000))
                        //    return "Максимальная длина 1000 символов";
                        break;
                    case "SetDate":
                        if (SetDate == null)
                            return "Поле обязательное для заполнения";

                        break;
                }
                return null;
            }
        }

        public virtual bool IsValid()
        {
            string[] fieldNames = { "Identifier", "Reason", "SetDate" };
            return fieldNames.All(fieldName => String.IsNullOrEmpty(this[fieldName]));
        }

        private static bool IsInvalidLength(string value, int maxLength)
        {
            return string.IsNullOrEmpty(value) || value.Length > maxLength;
        }

        private bool IsInvalidLengthNullable(string value, int maxLength)
        {
            return !string.IsNullOrEmpty(value) && value.Length > maxLength;
        }

        [IgnoreDataMember]
        public virtual bool IsDeleted
        {
            get { return StatusID == -1; }
        }


        public virtual void MarkDeleted()
        {
            StatusID = -1;
        }

        public virtual void ApplyValues(LegalEntityIdentifier x)
        {
            ID = x.ID;
            LegalEntityID = x.LegalEntityID;
            Identifier = x.Identifier;
            StatusID = x.StatusID;
            Reason = x.Reason;
            Comment = x.Reason;
            if (AdditionInfo != Guid.Empty)
                AdditionInfo = Guid.NewGuid();
        }

        private string xTrim(string value)
        {
            return value == null ? value : value.Trim();
        }

        public virtual bool Equals(LegalEntityIdentifier x)
        {
            if (x == null)
                return false;
            if (x == this)
                return true;

            return
                x.ID == ID &&
                x.LegalEntityID == LegalEntityID &&
                x.StatusID == StatusID &&
                x.Comment == Comment &&
                x.Reason == Reason &&
                x.SetDate == SetDate;
        }

    }


    public interface ILEIdentifierListProvider
    {
        string ValidateNewIdentifier(LegalEntityIdentifier x);
    }

}
