﻿
namespace PFR_INVEST.DataObjects
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class EdoOdkF025 : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual DateTime? ReportOnDate { get; set; }
        [DataMember]
        public virtual string Name { get; set; }
        [DataMember]
        public virtual string INN { get; set; }
        [DataMember]
        public virtual string KPP { get; set; }
        [DataMember]
        public virtual DateTime? ReportDate { get; set; }
        [DataMember]
        public virtual TimeSpan? ReportTime { get; set; }
        [IgnoreDataMember]
        public virtual DateTime? ReportDateTime
        {
            get
            {
                DateTime? dt = null;
                if (this.ReportDate.HasValue && this.ReportTime.HasValue)
                    dt = this.ReportDate.Value.Date.Add(this.ReportTime.Value);
                else if (this.ReportDate.HasValue)
                    dt = this.ReportDate.Value;
                return dt;
            }
        }
        [DataMember]
        public virtual decimal? TotalAmount { get; set; }
        [DataMember]
        public virtual string Post { get; set; }
        [DataMember]
        public virtual string ManageName { get; set; }
        [DataMember]
        public virtual decimal? Group1 { get; set; }
        [DataMember]
        public virtual decimal? Group2 { get; set; }
        [DataMember]
        public virtual decimal? Group3 { get; set; }
        [DataMember]
        public virtual decimal? Group4 { get; set; }
        [DataMember]
        public virtual decimal? Group5 { get; set; }
        [DataMember]
        public virtual decimal? Group6 { get; set; }
        [DataMember]
        public virtual decimal? Group7 { get; set; }
        [DataMember]
        public virtual decimal? Group8 { get; set; }
        [DataMember]
        public virtual decimal? Group81 { get; set; }
        [DataMember]
        public virtual decimal? Group9 { get; set; }
        [DataMember]
        public virtual decimal? Group10 { get; set; }
        [DataMember]
        public virtual decimal? Group11 { get; set; }
        [DataMember]
        public virtual decimal? Group12 { get; set; }
        [DataMember]
        public virtual decimal? Group13 { get; set; }
        [DataMember]
        public virtual decimal? Subgroup1 { get; set; }
        [DataMember]
        public virtual decimal? Subgroup2 { get; set; }
        [DataMember]
        public virtual decimal? Subgroup3 { get; set; }
        [DataMember]
        public virtual long? ContractId { get; set; }
        [DataMember]
        public virtual string RegNum { get; set; }
        [DataMember]
        public virtual DateTime? RegDate { get; set; }

    }
}
