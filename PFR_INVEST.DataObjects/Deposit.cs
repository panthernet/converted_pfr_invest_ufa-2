﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    [DataContract]
	public class Deposit : BaseDataObject
	{
		public enum Statuses 
		{
			/// <summary>
			/// 1 Размещен
			/// </summary>
			Placed = 1,
			/// <summary>
			/// 2 Возвращен
			/// </summary>
			Returned = 2,
			/// <summary>
			/// 3 Возвращен частично
			/// </summary>
			PartiallyReturned = 3,
			/// <summary>
			/// 4 Возвращен с нарушениями
			/// </summary>
			[Obsolete(@"Статус ""Возвращен с нарушениями"" больше не используется", true)]
			ReturnedWithViolation = 4
		}
		
		//private string comment;

        [DataMember]
		public virtual long ID { get; set; }

		[DataMember]
		public virtual long SummID { get; set; }

        [DataMember]
        public virtual long? DepClaimOfferId { get; set; }


        //Placed = "Размещен";//ID=1
        //Returned = "Возвращен";//ID=2
        //PartiallyReturned = "Возвращен частично";//ID=3
        //ReturnedWithViolation = "Возвращен с нарушениями";//ID=4

		[DataMember]
		public virtual long? Status { get; set; }


        [DataMember]
        public virtual long? CommentID { get; set; }

        [DataMember]
        public virtual DateTime? ReturnDateActual { get; set; }

        /// <summary>
        /// Дата создания карточки
        /// </summary>
        [DataMember]
        public virtual DateTime? DateDI { get; set; }

        //#region не используется

        //[Obsolete("Не используется")]
        //[DataMember]
        //public virtual long? DepClaimID { get; set; }

        //[Obsolete("Не используется")]
        //[DataMember]
        //public virtual string DepKind { get; set; }

        //[Obsolete("Не заполняется, надо избавляться")]
        //[DataMember]
        //public virtual long? ContragentID { get; set; }


        //#endregion
    }
}
