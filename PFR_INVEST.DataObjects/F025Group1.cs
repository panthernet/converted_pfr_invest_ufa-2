﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    using PFR_INVEST.DataObjects.Interfaces;

    public class F025Group1 : BaseDataObject, IF025Part
	{
		[DataMember]
		public virtual long ID { get; set; }
		[DataMember]
		public virtual long? EdoID { get; set; }
		[DataMember]
		public virtual string BankName { get; set; }
		[DataMember]
		public virtual DateTime? DocDate { get; set; }
		[DataMember]
		public virtual string DocNum { get; set; }
		[DataMember]
		public virtual string Account { get; set; }
		[DataMember]
		public virtual decimal? Amount { get; set; }
	}
}
