﻿using System.Runtime.Serialization;
namespace PFR_INVEST.DataObjects
{
    public class Rating : BaseDataObject, IMarkedAsDeleted, IIdentifiable
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        private string _ratingName;
        public virtual string RatingName
        {
            get { return _ratingName; }
            set
            {
                if (_ratingName != value)
                {
                    _ratingName = value;
                    OnPropertyChanged("RatingName");
                }
            }
        }

        [DataMember]
        private long _statusID = 1;
        public virtual long StatusID
        {
            get { return _statusID; }
            set
            {
                if (_statusID != value)
                {
                    _statusID = value;
                    OnPropertyChanged("StatusID");
                }
            }
        }

        //[DataMember]
        //private long _number = 1;
        //public virtual long Number
        //{
        //    get { return _number; }
        //    set
        //    {
        //        if (_number != value)
        //        {
        //            _number = value;
        //            OnPropertyChanged("Number");
        //        }
        //    }
        //}

        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "RatingName":
                        if (this.RatingName == null || string.IsNullOrEmpty(this.RatingName.Trim()))
                            return "Поле обязательное для заполнения";
                        if (this.RatingName.Length > 127)
                            return "Превышение допустимой длины поля";
                        break;
                }
                return null;
            }
        }
    }
}
