﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class OrdReport : BaseDataObject
    {
        /// <summary>
        /// ID
        /// </summary>
        [DataMember]
        public virtual long ID { get; set; }

        /// <summary>
        /// ID CbInOrder
        /// </summary>
        [DataMember]
        public virtual long? CbInOrderID { get; set; }

        /// <summary>
        /// Дата сделки
        /// </summary>
        [DataMember]
        public virtual DateTime? DateOrder { get; set; }

        /// <summary>
        /// Флаг того, что отчёт не связан и не должен отображатся в сальдо (ЛОТУС) 
        /// </summary>
        [DataMember]
        public virtual int IsUnlinked { get; set; }

        /// <summary>
        /// Дата операции по счету ДЕПО
        /// </summary>
        [DataMember]
        public virtual DateTime? DateDEPO { get; set; }


        /// <summary>
        /// Дата создания отчёта
        /// </summary>
        [DataMember]
        public virtual DateTime? ReportDate { get; set; }
        

        /// <summary>
        /// Количество
        /// </summary>
        [DataMember]
        public virtual long? Count { get; set; }

        /// <summary>
        /// Цена % от номинала
        /// </summary>
        [DataMember]
        public virtual decimal? Price { get; set; }

        /// <summary>
        /// НКД всего по сделке
        /// </summary>
        [DataMember]
        public virtual decimal? NKD { get; set; }

        /// <summary>
        /// Доходность
        /// </summary>
        [DataMember]
        public virtual decimal? Yield { get; set; }

        /// <summary>
        /// Статус контрагента
        /// </summary>
        [DataMember]
        public virtual string ContragentStatus { get; set; }

        /// <summary>
        /// Сумма
        /// </summary>
        [DataMember]
        public virtual decimal? Sum { get; set; }

        /// <summary>
        /// НКД на одну облигацию
        /// </summary>
        [DataMember]
        public virtual decimal? OneNKD { get; set; }

        /// <summary>
        /// Номинальная стоимость
        /// </summary>
        [DataMember]
        public virtual decimal? OneNomValue { get; set; }

        /// <summary>
        /// Предопределённый курс валют
        /// </summary>
        [DataMember]
        public virtual decimal? CustomCurs { get; set; }
        /// <summary>
        /// Ссылка на PortfolioPFRBankAccount (таблица PF_BACC),  связь счет-портфель.
        /// Суммарные сведения о покупке ценных бумаг.
        /// Заполняется если деньги берутся НЕ с торгового счета, указанного в поручении на покупку ЦБ (ORDER)
        /// </summary>
        [DataMember]
		public virtual long? TotalAccountPfBaccID { get; set; }

		[DataMember]
		public virtual string OrderRegNum { get; set; }
		[DataMember]
		public virtual string OrderType { get; set; }
		[DataMember]
		public virtual string CurrencyName { get; set; }
		[DataMember]
		public virtual long? CurrencyID { get; set; }

		[DataMember]
		public virtual string Portfolio { get; set; }

        [DataMember]
        public virtual decimal? NkdBuy { get; set; }

        /// <summary>
        /// Сумма сделки, по цене покупки
        /// </summary>
        [DataMember]
        public virtual decimal? SumBuy { get; set; }

        /// <summary>
        /// Сумма сделки, по цене покупки, руб
        /// </summary>
        [DataMember]
        public virtual decimal? SumBuyRub { get; set; }

        /// <summary>
        /// НКД, всего по сделке. Для переопределения вычисляемого значения.
        /// </summary>
        [DataMember]
        public virtual decimal? CustomNKD { get; set; }

        /// <summary>
        /// НКД, всего по сделке(руб). Для переопределения вычисляемого значения.
        /// </summary>
        [DataMember]
        public virtual decimal? CustomNKDRUR { get; set; }

        /// <summary>
        /// Сумма без НКД. Для переопределения вычисляемого значения.
        /// </summary>
        [DataMember]
        public virtual decimal? CustomSumWithoutNKD { get; set; }

        /// <summary>
        /// Сумма без НКД (руб). Для переопределения вычисляемого значения.
        /// </summary>
        [DataMember]
        public virtual decimal? CustomSumWithoutNKDRUR { get; set; }

        /// <summary>
        /// Сумма сделки (с НКД). Для переопределения вычисляемого значения.
        /// </summary>
        [DataMember]
        public virtual decimal? CustomSumm { get; set; }

        /// <summary>
        /// Сумма сделки (с НКД) (руб). Для переопределения вычисляемого значения.
        /// </summary>
        [DataMember]
        public virtual decimal? CustomSummRUR { get; set; }

		[DataMember]
		public virtual string LotusTable { get; set; }

		[IgnoreDataMember]
		public virtual bool IsReadOnly
		{
			get { return !string.IsNullOrEmpty(LotusTable); }
		}
       
    }
}
