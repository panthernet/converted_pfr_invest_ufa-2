﻿using System.Runtime.Serialization;
using System;

namespace PFR_INVEST.DataObjects
{
    public class PfrBranchReportSignatureContract : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long ReportID { get; set; }

        [DataMember]
        public virtual long TypeID { get; set; }

        [DataMember]
        public virtual long? LegalEntityID { get; set; }
       

        [DataMember]
        public virtual string Name { get; set; }

        [DataMember]
        public virtual string ContractNumber { get; set; }

        [DataMember]
        public virtual DateTime ContractDate { get; set; }

        [DataMember]
        public virtual string PostAddress { get; set; }

        [DataMember]
        public virtual string Phone { get; set; }

        [DataMember]
        public virtual string ContactName { get; set; }

        public enum enType
        {
            Npf = 1,
            Credit = 2,
            Communication = 3
        }

        [IgnoreDataMember]
        public virtual enType DataType
        {
            get { return (enType)TypeID; }
            set { TypeID = (long)value; }
        }
    }
}
