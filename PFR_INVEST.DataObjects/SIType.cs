﻿using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace PFR_INVEST.DataObjects
{
    public struct SIType
    {

        private static SIType ManagementCompany = new SIType("Управляющая компания", "УК");
        public static SIType УК
        {
            get
            {
                return ManagementCompany;
            }
        }

        public static SIType GeneralManagementCompany = new SIType("Государственная управляющая компания", "ГУК");
        public static SIType ГУК
        {
            get
            {
                return GeneralManagementCompany;
            }
        }
        
        public static SIType SpecializedDepositary = new SIType("Специализированный депозитарий", "СД");
        public static SIType СД
        {
            get
            {
                return SpecializedDepositary;
            }
        }

        private static ReadOnlyCollection<SIType> _allTypes = new ReadOnlyCollection<SIType>(new SIType[] { 
            УК,
            ГУК,
            СД
        });

        public static ReadOnlyCollection<SIType> AllTypes
        {
            get
            {
                return _allTypes;
            }
        }

        private SIType(string title, string shorTitle){
            _title = title;
            _shortTitle = shorTitle;
        }

        private string _title;
        public string Title
        { 
            get 
            {
                return _title;
            }
        }

        private string _shortTitle;
        public string ShortTitle
        { 
            get 
            {
                return _shortTitle;
            }
        }

        public static implicit operator SIType(string title)
        {
            title = title.Trim();
            foreach (var t in AllTypes.Where(t => t.ShortTitle == title || t.Title == title))
                return t;
            throw new InvalidCastException(string.Format("Can't convert '{0}' to SIType", title));
        }

        public static bool operator ==(SIType a, SIType b)
        {
            return (a.Title == b.Title && a.ShortTitle == b.ShortTitle);
        }

        public static bool operator !=(SIType a, SIType b)
        {
            return !(a == b);
        }

        public override bool Equals(object o)
        {
            return o is SIType && this == (SIType)o;
        }

        public override int GetHashCode()
        {
            return (Title ?? string.Empty).GetHashCode() + (ShortTitle ?? string.Empty).GetHashCode();
        }
    }
}
