﻿using System;

namespace PFR_INVEST.DataObjects.Attributes
{
    /// <summary>
    /// Аттрибут добавляющий авто логику восстановления зависимых сущностей для помечаемой сущности через Hibernate
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
    public class UseRestoreChildAttribute: Attribute
    {
        /// <summary>
        /// Тип зависимой сущности
        /// </summary>
        public Type ChildType { get; set; }

        /// <summary>
        /// Наименование поля зависимой сущности для связи по ID
        /// </summary>
        public string ChildIdFieldName { get; set; }
        /// <summary>
        /// Наименование поля статуса зависимой сущности (обычно StatusID)
        /// </summary>
        public string ChildStatusField { get; set; }
        /// <summary>
        /// Значение для активного статуса записи зависимой сущности (обычно 1)
        /// </summary>
        public long ChildOkStatus { get; set; }

        /// <summary>
        /// Аттрибут добавляющий авто логику восстановления зависимых сущностей для помечаемой сущности через Hibernate
        /// </summary>
        /// <param name="childType">Тип зависимой сущности</param>
        /// <param name="childIdFieldName">Наименование поля зависимой сущности для связи по ID</param>
        /// <param name="childStatusField"></param>
        /// <param name="childOkStatus"></param>
        public UseRestoreChildAttribute(Type childType, string childIdFieldName, string childStatusField, long childOkStatus)
        {
            ChildType = childType;
            if(!typeof(IMarkedAsDeleted).IsAssignableFrom(ChildType))
                throw new Exception("Зависимый тип должен реализовывать интерфейс IMarkedAsDeleted!");
            ChildIdFieldName = childIdFieldName;
            if(string.IsNullOrEmpty(childIdFieldName))
                throw new Exception("Необходимо указать наименование поля!");
            ChildStatusField = childStatusField;
            if (string.IsNullOrEmpty(childStatusField))
                throw new Exception("Необходимо указать наименование поля статуса!");

            ChildOkStatus = childOkStatus;

        }

    }

    /// <summary>
    /// Аттрибут указывающий на использование новой логики восстановления записей через аттрибуты
    /// Вызывает IUpdateRaisingModel для обновления записей в списках
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class UseRestoreAttribute : Attribute
    {
        /// <summary>
        /// Тип сущности
        /// </summary>
        public Type Type { get; set; }

        public UseRestoreAttribute(Type type)
        {
            Type = type;
            if(!typeof(IMarkedAsDeleted).IsAssignableFrom(type))
                throw new Exception("Тип должен реализовывать интерфейс IMarkedAsDeleted!");

        }
    }

    /// <summary>
    /// Атрибут текста заголовка для записи удаления сущности
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class DeleteLogCaptionAttribute : Attribute
    {
        public string Text { get; set; }

        public DeleteLogCaptionAttribute(string text)
        {
            Text = text;
        }
    }
}
