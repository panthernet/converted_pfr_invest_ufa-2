﻿using System;
using System.Text.RegularExpressions;

namespace PFR_INVEST.DataObjects
{
	public static class Validator
	{
		public static bool ValidateEmail(this string value)
		{
			value = value ?? "";
			const string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
									@"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
									@".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
			var re = new Regex(strRegex);
			return re.IsMatch(value);

		}

		public static string ValidateMaxLength(this string value, int maxLength)
		{
			return (value ?? string.Empty).Length > maxLength ? $"Максимальная длина поля {maxLength} символов" : null;
		}

		public static string ValidateRequired<T>(this T value)
		{
			return value == null || value.Equals(default(T)) ? "Поле обязательное для заполнения" : null;
		}

		public static string ValidateNonEmpty(this string value)
		{
			return string.IsNullOrEmpty((value ?? string.Empty).Trim()) ? "Поле не может быть пустым" : null;
		}

		public static string ValidateFixedLength(this string value, int length)
		{
			return (value ?? string.Empty).Length != length ? $"Поле должно быть длинной {length} символов" : null;
		}

		public static string ValidateOnlyNumeric(this string value)
		{
			for (int i = 0; i < value.Length; i++)
			{
				if (!"1234567890".Contains(value[i].ToString()))
				{
					return "Поле должно содержать только цифры";
				}
			}
			return null;			
		}

		/// <summary>
		/// Validate non zero (greater or less than)
		/// </summary>
		public static string ValidateNonZero(this decimal value)
		{
			return value == decimal.Zero ? "Значение не должно равняться нулю" : null;
		}

		/// <summary>
		/// Validate non zero (greater or less than)
		/// </summary>
		public static string ValidateNonZero(this decimal? value)
		{
			return value == null || value.Value == decimal.Zero ? "Значение не должно равняться нулю" : null;
		}


		public static string ValidateNonNegative(this decimal value)
		{
			return value < 0 ? "Значение не может быть отрицательным" : null;
		}

		/// <summary>
		/// Проверка, что значение является не отрицательным
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static string ValidateNonNegative(this decimal? value)
		{
			return value.HasValue ? value.Value.ValidateNonNegative() : null;
		}


		/// <summary>
		/// Validate range
		/// </summary>
		/// <param name="value"></param>
		/// <param name="min">Min value. Null for MinValue</param>
		/// <param name="max">Max value. Null for MaxValue</param>
		/// <returns></returns>
		public static string ValidateRange(this decimal value, decimal? min, decimal? max, bool includeBorder = true)
		{
			return (value as decimal?).ValidateRange(min, max, includeBorder);
		}

		/// <summary>
		/// Validate range
		/// </summary>
		/// <param name="value"></param>
		/// <param name="min">Min value. Null for MinValue</param>
		/// <param name="max">Max value. Null for MaxValue</param>
		/// <returns></returns>
		public static string ValidateRange(this decimal? value, decimal? min, decimal? max, bool includeBorder = true)
		{
			min = min ?? decimal.MinValue;
			max = max ?? decimal.MaxValue;

			if (!value.HasValue)
				return null;

			if (includeBorder)
			{
			    if (value < min)
					return $"Значение должно быть не меньше {min}";
			    if (value > max)
			        return $"Значение должно быть не больше {max}";
			}
			else
			{
			    if (value <= min)
					return $"Значение должно быть больше {min}";
			    if (value >= max)
			        return $"Значение должно быть меньше {max}";
			}
		    return null;
		}


		/// <summary>
		/// Validate max Format DECIMAL(19.4)
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static string ValidateMaxFormat(this decimal value)
		{
			return value.ValidateMaxFormat(19 - 4);
		}

		/// <summary>
		/// Validate max Format
		/// </summary>
		/// <param name="value"></param>
		/// <param name="maxDigits">Decimal digits before poin (Example Decimal(19.4) = 15 digits)</param>
		/// <returns></returns>
		public static string ValidateMaxFormat(this decimal value, int maxDigits)
		{
			var max = (decimal)Math.Pow(10, maxDigits);
			return Math.Abs(value) >= max ? $"Значение не может иметь больше {maxDigits} знаков перед запятой " : null;
		}

		/// <summary>
		/// Validate max Format DECIMAL(19.4)
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static string ValidateMaxFormat(this decimal? value)
		{
			return value.ValidateMaxFormat(19 - 4);
		}

		/// <summary>
		/// Validate max Format
		/// </summary>
		/// <param name="value"></param>
		/// <param name="maxDigits">Decimal digits before poin (Example Decimal(19.4) = 15 digits)</param>
		/// <returns></returns>
		public static string ValidateMaxFormat(this decimal? value, int maxDigits)
		{
			return value.HasValue ? value.Value.ValidateMaxFormat(maxDigits) : null;
		}

		public static string ValidateNonNegative(this int value)
		{
			return value < 0 ? "Значение не может быть отрицательным" : null;
		}

		public static string ValidatePeriodStart(this DateTime? value, DateTime? periodEnd)
		{
			return value > periodEnd ? "Дата начала периода не может быть больше даты конца" : null;
		}

		public static string ValidatePeriodEnd(this DateTime? value, DateTime? periodStart)
		{
			return value < periodStart ? "Дата конца периода не может быть меньше даты начала" : null;
		}

		public static bool TrimmedIsNullOrEmpty(this string value)
		{
			return string.IsNullOrEmpty(value == null ? null : value.Trim());
		}

	}
}
