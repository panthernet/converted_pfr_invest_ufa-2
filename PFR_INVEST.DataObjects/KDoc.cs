﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class KDoc : BaseDataObject
    {
        /// <summary>
        /// Версия документа казначейства
        /// </summary>
        public enum DocVersion
        {
            Migrated = 1,
            Version1 = 2,
            Version2 = 3,
            Version3 = 4,
            Latest = 5
        }

		public enum Types 
		{
			Month = 80001,
			Year = 80002
		}

		public KDoc() 
		{
			ReportPeriodID = (long)KDoc.Types.Month;
			OperationDate = DateTime.Today;
			DocDate = DateTime.Today;
			DocType = (int)DocVersion.Latest;//Новый формат документа
		}

		[IgnoreDataMember]
		public virtual bool IsMonthDocument { get { return this.ReportPeriodID == (long)Types.Month; } }

		[IgnoreDataMember]
		public virtual bool IsYearDocument { get { return this.ReportPeriodID == (long)Types.Year; } }

        [DataMember]
        public virtual decimal Penni1Proc { get; set; }

        [DataMember]
        public virtual decimal Penni2Proc { get; set; }

        [DataMember]
        public virtual decimal Penni3Proc { get; set; }

        [DataMember]
        public virtual decimal Penni4Proc { get; set; }

        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long? AddSpnID { get; set; }
		        
        [DataMember]
		public virtual long? PortfolioID { get; set; }
       

        [DataMember]
        public virtual DateTime? OperationDate { get; set; }

        [DataMember]
        public virtual DateTime? DocDate { get; set; }

        [DataMember]
        public virtual DateTime? CheckDate { get; set; }

        [DataMember]
        public virtual string RegNum { get; set; }

        [DataMember]
        public virtual decimal Summ1 { get; set; }

        [DataMember]
        public virtual decimal Penni1 { get; set; }

        [DataMember]
        public virtual decimal Pay1 { get; set; }

        [DataMember]
        public virtual decimal Other1 { get; set; }

        [DataMember]
        public virtual decimal Proc1 { get; set; }

        [DataMember]
        public virtual decimal Summ2 { get; set; }

        [DataMember]
        public virtual decimal Penni2 { get; set; }

        [DataMember]
        public virtual decimal Pay2 { get; set; }

        [DataMember]
        public virtual decimal Other2 { get; set; }

        [DataMember]
        public virtual decimal Proc2 { get; set; }

        [DataMember]
        public virtual decimal Fix { get; set; }

        [DataMember]
        public virtual decimal Summ3 { get; set; }

        [DataMember]
        public virtual decimal Penni3 { get; set; }

        [DataMember]
        public virtual decimal Pay3 { get; set; }

        [DataMember]
        public virtual decimal Other3 { get; set; }

        [DataMember]
        public virtual decimal Proc3 { get; set; }

        [DataMember]
        public virtual decimal Summ4 { get; set; }

        [DataMember]
        public virtual decimal Penni4 { get; set; }

        [DataMember]
        public virtual decimal Other4 { get; set; }

        [DataMember]
        public virtual decimal Proc4 { get; set; }
		
        [DataMember]
        public virtual decimal Pay4 { get; set; }

		[DataMember]
		public virtual decimal Summ5 { get; set; }

		[DataMember]
		public virtual decimal Penni5 { get; set; }

		[DataMember]
		public virtual decimal Other5 { get; set; }

		[DataMember]
		public virtual decimal Proc5 { get; set; }

		[DataMember]
		public virtual decimal Pay5 { get; set; }

        [DataMember]
		public virtual long? ReportPeriodID { get; set; }


		[DataMember]
		public virtual int DocType{ get; set; }

		[DataMember]
		public virtual decimal TotalSumm { get; set; }

		[DataMember]
		public virtual decimal TotalPenny { get; set; }

		[DataMember]
		public virtual long? PortfolioPFRBankAccountID { get; set; }
    }
}
