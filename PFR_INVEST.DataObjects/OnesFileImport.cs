﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class OnesDocKinds: BaseDataObject
    {
        public const string VYP = "ВЫП";
        public const string POST = "ПОСТ";
        public const string K_NPF = "НПФ";
        public const string K_NPFV = "НПФВ";
        public const string K_UK = "УК";
        public const string K_UKV = "УКВ";
        public const string K_DEP = "ДЕП";
        public const string K_DEPV = "ДЕПВ";
        public const string K_DEPVP = "ДЕПВП";
        public const string K_OPFR = "ОПФР";
        public const string K_OPFRV = "ОПФРВ";
        public const string K_GSO = "ГСО";
        public const string K_ZZL = "ЗЗЛ";
        public const string K_ZZLV = "ЗЗЛВ";
        public const string K_SOF = "СОФ";
        public const string K_MSK = "МСК";
        public const string K_DOST = "ДОСТ";
        public const string K_DOSTV = "ДОСТВ";
    }

    public class OnesFileImport : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long SessionID { get; set; }

        [DataMember]
        public virtual DateTime? LoadDate { get; set; }

        [DataMember]
        public virtual string DocNumber { get; set; }

        [DataMember]
        public virtual DateTime? DocDate { get; set; }

        [DataMember]
        public virtual string DocKind { get; set; }

        [DataMember]
        public virtual long ImportStatusID { get; set; }

        [DataMember]
        public virtual string Filename { get; set; }

        [DataMember]
        public virtual long FileID { get; set; }

        [DataMember]
        public virtual string StatusName { get; set; }
    }
}
