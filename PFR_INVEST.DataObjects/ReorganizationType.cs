﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class ReorganizationType : BaseDataObject, IIdentifiable
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual string Type { get; set; }
    }
}
