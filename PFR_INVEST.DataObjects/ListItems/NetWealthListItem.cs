﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PFR_INVEST.DataObjects.ListItems
{
	public class NetWealthListItem : BaseDataObject
	{
		public NetWealthListItem() { }
		public NetWealthListItem(long id, DateTime? reportOnDate, decimal? netWealthSum)
			: this(id, reportOnDate, netWealthSum, null, null, null, null) { }
		public NetWealthListItem(long id, DateTime? reportOnDate, decimal? netWealthSum, long? contractId, string contractNumber, string UKName, string UKOldName)
		{
			this.ID = id;
			this.ReportOnDate = reportOnDate;
			this.NetWealthSum = netWealthSum;
			this.ContractNumber = contractNumber;
			this.ContractId = contractId;
			this.UKName = LegalEntity.GetFormalizedNameFull(UKName, UKOldName);
		} 

		[DataMember]
		public virtual long ID { get; set; }

		[DataMember]
		public virtual string Table { get; set; }

		[DataMember]
		public virtual long? ContractId { get; set; }

		[DataMember]
		public virtual DateTime? ReportOnDate { get; set; }

		[IgnoreDataMember]
		public virtual int? ReportOnDateYear { get { return this.ReportOnDate.HasValue ? (int?)this.ReportOnDate.Value.Year : null; } }

		[IgnoreDataMember]
        public virtual string ReportOnDateDate { get { return this.ReportOnDate.HasValue ? string.Format("{0}\t{1}", this.ReportOnDate.Value.Date.ToShortDateString(), this.lDocumentNumbers.HasValue ? string.Concat("(", this.lDocumentNumbers.Value.ToString(), ")") : string.Empty) : null; } }

        [IgnoreDataMember]
        public virtual long? lDocumentNumbers { get; set; }

		[DataMember]
		public virtual decimal? NetWealthSum { get; set; }

		[DataMember]
		public virtual string UKName { get; set; }

		[DataMember]
		public virtual string ContractNumber { get; set; }
	}
}
