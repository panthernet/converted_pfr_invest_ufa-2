﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class PfrBranchReportDeliveryCostListItem
    {
        public PfrBranchReportDeliveryCostListItem(PfrBranchReportDeliveryCost dc, PfrBranchReport r, PFRBranch b, FEDO f)
        {
            ID = dc.ID;
            ReportId = dc.BranchReportID;

            Sum_221 = dc.Sum_221;
            Sum_226 = dc.Sum_226;
            Total = dc.Total;
            
            ReportRegionName = b.Name;
            ReportYear = r.PeriodYear;
            ReportMonth = "";
            if (r.PeriodMonth != null)
            {
                ReportMonth = DateTools.GetMonthInWordsForDate(r.PeriodMonth);
            }

            ReportFedoName = f.Name;
        }

        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public long ReportId { get; set; }

        [DataMember]
        public virtual decimal Sum_221 { get; set; }

        [DataMember]
        public virtual decimal Sum_226 { get; set; }

        [DataMember]
        public virtual decimal Total { get; set; }

        [DataMember]
        public string ReportMonth { get; set; }

        [DataMember]
        public int ReportYear { get; set; }

        [DataMember]
        public string ReportRegionName { get; set; }

        [DataMember]
        public string ReportFedoName { get; set; }
    }
}
