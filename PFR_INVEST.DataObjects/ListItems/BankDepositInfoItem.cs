﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class BankDepositInfoItem
    {
        [IgnoreDataMember]
		[XmlIgnore]
        public virtual long ID
        {
            get { return DepositID; }
            set { DepositID = value; }
        }

        /// <summary>
        /// Номер договора банковского депозита
        /// DEPOSIT.ID
        /// </summary>
        [DataMember]
		[XmlElement("DEPOSIT_ID")]
		public virtual long DepositID { get; set; }


        /// <summary>
        /// Номер договора банковского депозита
        /// DEPCLAIM_OFFER.ID
        /// </summary>
        [DataMember]
		[XmlElement("DEPCLAIM_OFFER_ID")]
		public virtual long OfferID { get; set; }

        /// <summary>
        /// Номер договора банковского депозита
        /// DEPCLAIM_OFFER.NUMBER
        /// </summary>
        [DataMember]
		[XmlElement("DEPCLAIM_OFFER_NUMBER")]
		public virtual long Number { get; set; }

        /// <summary>
        /// DEPCLAIM_OFFER.DATE
        /// </summary>
        [DataMember]
		[XmlElement("DEPCLAIM_OFFER_DATE")]
		public virtual DateTime Date { get; set; }

        /// <summary>
        /// DEPCLAIM2.SETTLEDATE
        /// </summary>
        [DataMember]
		[XmlElement("DEPCLAIM2_SETTLEDATE")]
		public virtual DateTime SettleDate { get; set; }

        /// <summary>
        /// Сумма депозита
        /// DEPCLAIM2.AMOUNT
        /// </summary>
        [DataMember]
		[XmlElement("DEPCLAIM2_AMOUNT")]
		public virtual decimal Amount { get; set; }

        /// <summary>
        /// DEPCLAIM2.RATE
        /// </summary>
        [DataMember]
		[XmlElement("DEPCLAIM2_RATE")]
		public virtual decimal Rate { get; set; }

        /// <summary>
        /// DEPCLAIM2.RETURNDATE
        /// </summary>
        [DataMember]
		[XmlElement("DEPCLAIM2_RETURNDATE")]
		public virtual DateTime ReturnDate { get; set; }

        /// <summary>
        /// DEPOSIT. STATUS
        /// </summary>
        [DataMember]
		[XmlElement("DEPOSIT_STATUS")]
		public virtual long? Status { get; set; }

        [IgnoreDataMember]
		[XmlIgnore]
		public virtual string StatusText
        {
            get
            {
                switch (Status)// Тут: нас интересуют только 2 статуса
                {
                    case 1: return "Размещен";
                    case 3: return "Возвращен частично";
                    default: return null;
                }
            }
        }

        /// <summary>
        /// Сумма возврата (текущая)
        /// </summary>
        [DataMember]
		[XmlElement("PAYMENT_HISTORY_SUM")]
		public decimal? ReturnVolume { get; set; }

        /// <summary>
        /// Идентификатор банка депозита
        /// LEGALENTITY.ID
        /// </summary>
        [DataMember]
		[XmlElement("LEGALENTITY_ID")]
		public long BankID { get; set; }


        /// <summary>
        /// Сумма, которую еще подлежить вернуть
        /// </summary>
        [IgnoreDataMember]
		[XmlIgnore]
		public decimal? PendingVolume
        {
            get
            {
                return Amount - (ReturnVolume ?? 0);
            }
        }

    }
}
