﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using PFR_INVEST.Constants.Identifiers;

namespace PFR_INVEST.DataObjects.ListItems
{
	[DataContract]
	public class DepositReturnListItem : BaseDataObject
	{
		[DataMember]
		public virtual bool IsSelected { get; set; }

		[DataMember]
		public virtual bool IsPercent { get; set; }

		[IgnoreDataMember]
		public virtual string IsPercentText
		{
			get
			{
				return IsPercent ? "Проценты по депозиту" : "Депозит";
			}
		}

		[IgnoreDataMember]
		public virtual string TotalText
		{
			get
			{
				return "Итого:";
			}
		}


		[DataMember]
		public virtual long DepositID { get; set; }

		[DataMember]
		public virtual long OfferNumber { get; set; }


		/// <summary>
		/// Deposit status
		/// </summary>
		[DataMember]
		public virtual long Status { get; set; }

		[DataMember]
		public virtual string Comment { get; set; }



		[DataMember]
		public virtual string BankName { get; set; }

		[DataMember]
		public virtual DateTime SettleDate { get; set; }

		[DataMember]
		public virtual DateTime ReturnDate { get; set; }

		[DataMember]
		public virtual string PortfolioName { get; set; }

		[DataMember]
		public virtual string PortfolioType { get; set; }

		[DataMember]
		public virtual string AccountNum { get; set; }


		/// <summary>
		/// Сумма вклада DepClaim2 (из импорта)
		/// </summary>
		[DataMember]
		public virtual decimal AmountTotal { get; set; }

		/// <summary>
		/// Сумма процентов DepClaim2 (из импорта)
		/// </summary>
		[DataMember]
		public virtual decimal AmountPercent { get; set; }

		/// <summary>
		/// Сумма, уже уплаченная по данной записи (сумма выплат из истории)
		/// </summary>
		[DataMember]
		public virtual decimal AmountPaid { get; set; }


		///// <summary>
		///// Сумма к оплате для текущей записи
		///// </summary>
		//[IgnoreDataMember]
		//public virtual decimal Amount
		//{
		//    get
		//    {
		//        //return IsPercent ? AmountPercent - AmountPaid : AmountTotal - AmountPaid;
		//        return IsPercent ? AmountSumPercent - AmountPaid : AmountSum - AmountPaid;
		//    }
		//}

		public void SetAmmount()
		{
			if (!IsPercent)
				Amount = AmountSum - AmountPaid;

		}

		private decimal m_Amount;
		/// <summary>
		/// Сумма к оплате для текущей записи (сколько осталось после выплат)
		/// </summary>
		[DataMember]
		public virtual decimal Amount
		{
			get { return m_Amount; }
			set { m_Amount = value; OnPropertyChanged("Amount"); }
		}


		/// <summary>
		/// Сколько платим (фактическая сумма)
		/// </summary>
		[DataMember]
		public virtual decimal AmountActual { get; set; }

		[IgnoreDataMember]
		public virtual string StatusText
		{
			get
			{
				return DepositIdentifier.GetDepositStatusText(Status);
			}
		}

		[DataMember]
		public virtual long SumID { get; set; }



		[IgnoreDataMember]
		public string DepositStatusText
		{
			get
			{
				if (Status == 3)//PartiallyReturned
					return " (Возвращен частично)";

				return null;
			}
		}

		[DataMember]
		public DateTime PaymentDate { get; set; }


		/// <summary>
		/// Returns change if overpaid.
		/// </summary>
		/// <param name="paymentDate"></param>
		/// <param name="amount"></param>
		/// <returns></returns>
		public virtual decimal SetPayment(DateTime paymentDate, decimal amount)
		{
			PaymentDate = paymentDate;

			if (amount < 0)
			{
				AmountActual = 0;
				return amount;
			}

			decimal res = 0;

			AmountActual = amount;
			if (AmountActual > Amount)// need to return change
			{
				res = AmountActual - Amount;
				AmountActual = Amount;
			}

			OnPropertyChanged("PaymentDate");
			OnPropertyChanged("AmountActual");

			return res;
		}

		/// <summary>
		/// true если заплатили все
		/// </summary>
		[IgnoreDataMember]
		public virtual bool IsPaidAll
		{
			get
			{
				if (Amount <= 0)
					return true;

				if (IsSelected && AmountActual >= Amount)
					return true;

				return false;
			}
		}


		/// <summary>
		/// true если была хоть какая-то проплата
		/// </summary>
		[IgnoreDataMember]
		public virtual bool IsPaidPartial
		{
			get
			{
				if (IsPaidAll)
					return true;

				if (AmountPaid > 0)
					return true;

				if (IsSelected && AmountActual > 0)
					return true;

				return false;
			}
		}


		[IgnoreDataMember]
		public virtual List<DepositReturnListItem> GroupItems
		{
			get;
			set;
		}


		/// <summary>
		/// Для выделения сразу всех платежей по данному депозиту
		/// </summary>
		[IgnoreDataMember]
		public virtual bool IsSelectedGroup
		{
			get
			{
				return IsSelected;
			}
			set
			{
				if (GroupItems == null)
				{
					IsSelected = value;
					return;
				}

				foreach (var x in GroupItems)
					x.IsSelected = value;
			}
		}


		/// <summary>
		/// Сумма по портфелю Offer_PfrBbankAccount_Sum (берется из бд)
		/// </summary>
		[DataMember]
		public virtual decimal AmountSum { get; set; }


		private decimal m_AmountSumPercent;
		/// <summary>
		/// Сумма процентов по портфелю Offer_PfrBbankAccount_Sum (вычисляется с учетом плановой)
		/// </summary>
		[DataMember]
		public virtual decimal AmountSumPercent
		{
			get { return m_AmountSumPercent; }
			set { m_AmountSumPercent = value; OnPropertyChanged("AmountSumPercent"); OnPropertyChanged("Amount"); }
		}

        /// <summary>
        /// Плановая сумма процентов (появляется после Возврата депозитов)
        /// </summary>
        [DataMember]
        public virtual decimal? AmountPercPlanned { get; set; }

        [DataMember]
	    public virtual int AuctionNum { get; set; }

	    /// <summary>
		/// Returns change if overpaid.
		/// </summary>
		/// <param name="paymentDate"></param>
		/// <param name="amount"></param>
		/// <returns></returns>
		public virtual decimal SetAmountSumPercent(decimal amount)
		{
			if (amount < 0)
			{
				AmountSumPercent = 0;
				Amount = 0;
				return amount;
			}

			decimal res = 0;
			Amount = amount;


			AmountSumPercent = amount;
            //TODO a
            //добавляем учет плановой суммы по процентам http://jira.vs.it.ru/browse/DOKIPIV-890
	        var perc = AmountPercPlanned ?? AmountPercent;
			if (AmountSumPercent > perc)// need to return change
			{
				res = AmountSumPercent - perc;
				AmountSumPercent = perc;
			}

			OnPropertyChanged("PaymentDate");
			OnPropertyChanged("AmountActual");

			return res;
		}
	}
}
