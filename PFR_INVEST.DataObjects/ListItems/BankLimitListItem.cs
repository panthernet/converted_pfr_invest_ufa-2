﻿namespace PFR_INVEST.DataObjects.ListItems
{
	public class BankLimitListItem : BaseDataObject
	{
		/// <summary>
		/// Имя банка
		/// </summary>
		public string BankName { get; set; }
		
		/// <summary>
		/// Биржевой код банка
		/// </summary>
		public string StockCode { get; set; }

		/// <summary>
		/// ID биржи
		/// </summary>
		public long StockID { get; set; }

		/// <summary>
		/// Суммарный лимит на средства, рассчитанный для данной кредитной организации, млн.руб;
		/// </summary>
		public decimal LimitTotal { get; set; }

		/// <summary>
		/// Лимит на заявки, рассчитанный для данной кредитной организации, млн.руб.
		/// </summary>
		public decimal LimitClaim { get; set; }
	}
}
