﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class BankItem
    {
        [DataMember]
        public virtual LegalEntity Entity { get; set; }

        [DataMember]
        public virtual string RegistrationNum { get; set; }

        [IgnoreDataMember]
        public virtual long? RegistrationNumInt
        {
            get
            {
                long v;
                if (long.TryParse(RegistrationNum, out v))
                    return v;
                return null;
            }
        }

        [IgnoreDataMember]
        public virtual long ID { get { return Entity.ID; } }

        [IgnoreDataMember]
        public virtual string ShortName { get { return Entity.ShortName; } }

        [IgnoreDataMember]
        public virtual string LegalAddress { get { return Entity.LegalAddress; } }

        [IgnoreDataMember]
        public virtual string Phone { get { return Entity.Phone; } }
    }
}
