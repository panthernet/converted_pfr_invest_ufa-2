﻿using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataObjects.ListItems
{
    public class NT_NpfListItem
    {
        public string Name { get; set; }
        public decimal? Summ { get { return _finRegister.Count; } }
        public long? ZlCount { get { return _finRegister.ZLCount; } }
        public bool IsCreatedState { get; private set; }
        public long Id { get; private set; }
        private readonly Finregister _finRegister;
        public string Status { get; set; }


        public NT_NpfListItem(Finregister fr)
        {
            _finRegister = fr;
            IsCreatedState = RegisterIdentifier.FinregisterStatuses.IsCreated(fr.Status);
            Id = fr.ID;
            Status = fr.Status;
        }

        public Finregister GetFinregister()
        {
            return _finRegister;
        }
    }
}
