﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class ReqTransferRecalcListItem: BaseDataObject
    {
        [DataMember]
        public long ID { get; set; }

        [DataMember]
        public decimal? Sum { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public decimal? StartSum { get; set; }

        [DataMember]
        public decimal? InvestDohod { get; set; }

        [DataMember]
        public string DirSpnName { get; set; }

        public ReqTransferRecalcListItem(object[] a)
        {
            if (a == null) return;
            ID = Convert.ToInt64(a[0]);
            Sum = Convert.ToDecimal(a[1]);
            Status = a[2].ToString();
            StartSum = Convert.ToDecimal(a[3]);
            InvestDohod = Convert.ToDecimal(a[4]);
            DirSpnName = a[5].ToString();
        }
    }
}
