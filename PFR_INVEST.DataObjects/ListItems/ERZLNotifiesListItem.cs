﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class ERZLNotifiesListItem
    {
        [DataMember]
        public string NotifyNum { get; set; }

        /// <summary>
        /// ID ERZL
        /// </summary>
        [DataMember]
        public long? ERZL_ID { get; set; }

        [DataMember]
        public long ID { get; set; }

        /// <summary>
        /// ID контрагента
        /// </summary>
        [DataMember]
        public long ContragentID { get; set; }

        /// <summary>
        /// ID счета НПФ
        /// </summary>
        [DataMember]
        public long? NPFAccountID { get; set; }

        /// <summary>
        /// Наименование контрагента
        /// </summary>
        [DataMember]
        public string NPFName { get; set; }

        /// <summary>
        /// Дата
        /// </summary>
        [DataMember]
        public DateTime? Date { get; set; }

        [DataMember]
        public long? NPF2PFRCount { get; set; }

        [DataMember]
        public long? PFR2NPFCount { get; set; }

        [DataMember]
        public long? NPF2OTHERCount { get; set; }

        [DataMember]
        public long? OTHER2NPFCount { get; set; }

        [DataMember]
        public bool IsOutNotEqualInput { get; set; }

        [DataMember]
        public bool IsInputNotEqualOut { get; set; }
    }
}
