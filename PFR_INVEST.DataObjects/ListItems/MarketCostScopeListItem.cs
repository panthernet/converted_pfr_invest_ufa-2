﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class MarketCostScopeListItem
    {
        [DataMember]
        public long ID { get; set; }
        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public decimal MarketCost { get; set; }

        [DataMember]
        public int Year { get; set; }

        [DataMember]
        public int FormType { get; set; }

    }
}
