﻿using System;
using System.Runtime.Serialization;
using PFR_INVEST.Constants.Identifiers;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class RegistersListItem
    {
        /// <summary>
        /// ID данного финреестра
        /// </summary>
        [DataMember]
        public long ID { get; set; }

        /// <summary>
        /// ID реестра
        /// </summary>
        [DataMember]
        public long RegisterID { get; set; }

        /// <summary>
        /// ID контрагента
        /// </summary>
        [DataMember]
        public long ContragentID { get; set; }

        private string _registerKind;

        /// <summary>
        /// Вид реестра
        /// </summary>
        [DataMember]
        public string RegisterKind 
        {
			get
			{
				return RegisterIdentifier.IsFromNPF(this._registerKind) ? RegisterIdentifier.NPFtoPFR :
					RegisterIdentifier.IsToNPF(this._registerKind) ? RegisterIdentifier.PFRtoNPF :
					RegisterIdentifier.IsTempReturn(this._registerKind) ? RegisterIdentifier.TempReturn :
					RegisterIdentifier.IsTempAllocation(this._registerKind) ? RegisterIdentifier.TempAllocation :
					null;
			}
            set { _registerKind = value; }
        }

        /// <summary>
        /// Содержание финреестра
        /// </summary>        
        [DataMember]
        public string Content { get; set; }

        /// <summary>
        /// Наименование контрагента
        /// </summary>
        [DataMember]
        public string ContragentName { get; set; }

   /*     [DataMember]
        public string ContragentName1 { get; set; }

        [DataMember]
        public string ContragentName2 { get; set; }*/


        /// <summary>
        /// Сумма в финреестре
        /// </summary>
        [DataMember]
        public decimal Count { get; set; }

		/// <summary>
		/// Сумма ЧФР финреестра
		/// </summary>
		[DataMember]
		public decimal? CFR { get; set; }

        /// <summary>
        /// Количество застрахованных лиц
        /// </summary>
        [DataMember]
        public long? ZLCount { get; set; }

        /// <summary>
        /// Утверждающий документ
        /// </summary>
        [DataMember]
        public string ApproveDocName { get; set; }

        /// <summary>
        /// Номер
        /// </summary>
        [DataMember]
        public string FinregRegNum { get; set; }

        /// <summary>
        /// Дата
        /// </summary>
        [DataMember]
        public DateTime? FinregDate { get; set; }

        /// <summary>
        /// Номер
        /// </summary>
        [DataMember]
        public string RegNum { get; set; }

        /// <summary>
        /// Дата
        /// </summary>
        [DataMember]
        public DateTime? RegDate { get; set; }

        /// <summary>
        /// Сумма в платежном поручении
        /// </summary>
        [DataMember]
        public decimal? DraftSum { get; set; }

        /// <summary>
        /// ID кредитового счета
        /// </summary>
        [DataMember]
        public long CreditAccID { get; set; }

        /// <summary>
        /// ID дебитового счета
        /// </summary>
        [DataMember]
        public long DebitAccID { get; set; }

        /// <summary>
        /// Дата заявки на перечисление
        /// </summary>
        [DataMember]
        public DateTime? TrancheDate { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public bool IsMarkedFinregister 
        {
            get
            {
                if (!string.IsNullOrEmpty(this.RegisterKind) && RegisterIdentifier.IsToNPF(this.RegisterKind.ToLower()))
                {
                    return this.TrancheDate != null && this.DraftSum != this.Count;
                }
                else
                {
                    return this.Status != null && RegisterIdentifier.FinregisterStatuses.IsIssued(this.Status.ToLower()) && (this.DraftSum == null || this.DraftSum != this.Count);
                }
            } 

            set
            {

            } 
        }

        [DataMember]
        public bool IsActiveNPF { get; set; }

		[DataMember]
		public string Portfolio { get; set; }

		[DataMember]
        public bool CanPrintLetter { get; set; }
		[DataMember]
        public bool CanCreateOrder { get; set; }

        //FOR WEB
        public bool IsCustomArchive { get; set; }

        public long? ParentFinregisterID { get; set; }
    }
}
