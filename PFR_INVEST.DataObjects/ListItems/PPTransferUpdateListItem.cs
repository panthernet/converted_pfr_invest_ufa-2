﻿namespace PFR_INVEST.DataObjects.ListItems
{
    public class PPTransferUpdateListItem : BaseDataObject
	{
        public long? FinregisterID { get; set; }
        public long? ReqTransferID { get; set; }
        public long ID { get; set; }
	}
}
