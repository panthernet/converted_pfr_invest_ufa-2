﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class UKPaymentListItem
    {
        [DataMember]
        public long ID { get; set; }

        [DataMember]
        public long ReqTransferID { get; set; }

        [DataMember]
        public long ReqTransfer2ID { get; set; }

        [DataMember]
        public string Month { get; set; }

        [DataMember]
        public long? MonthID { get; set; }

        [DataMember]
        public long? TransferListID { get; set; }

        [DataMember]
        public decimal PlanSum { get; set; }

        [DataMember]
        public decimal FactSum { get; set; }

        [DataMember]
        public decimal Diff { get; set; }

        [DataMember]
        public decimal InvestDohod { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public string ActNum { get; set; }

        [DataMember]
        public DateTime? ActDate { get; set; }

		[DataMember]
		public long ZLCount { get; set; }

        [IgnoreDataMember]
        public bool HasReqTransfer => ReqTransferID != 0 || ReqTransfer2ID != 0;

        public List<long> GetReqTransferIds()
        {
            return new List<long> { ReqTransferID, ReqTransfer2ID };
        }
    }
}
