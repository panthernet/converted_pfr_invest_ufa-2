﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class PortfolioPFRBankAccountListItem
    {
        /// <summary>
        /// ID счета
        /// </summary>
        [DataMember]
        public long ID { get; set; }

        /// <summary>
        /// Номер счета
        /// </summary>
        [DataMember]
        public string AccountNumber { get; set; }

        /// <summary>
        /// Банк 
        /// </summary>
        [DataMember]
        public string BankName { get; set; }

        /// <summary>
        /// Тип счета
        /// </summary>
        [DataMember]
        public string Type { get; set; }

        /// <summary>
        /// Тип связи
        /// </summary>
        [DataMember]
        public long AssignKindID { get; set; }

        /// <summary>
        /// Тип связи
        /// </summary>
        [DataMember]
        public string AssignKindName { get; set; }
    }
}
