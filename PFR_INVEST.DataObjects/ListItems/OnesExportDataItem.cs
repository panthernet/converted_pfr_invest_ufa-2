﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    public class OnesExportDataItem: BaseDataObject
    {
        [DataMember]
        public bool Selected { get; set; }

        [DataMember]
        public long ID { get; set; }

        [DataMember]
        public string Item1 { get; set; }
        
        [DataMember]
        public string Item2 { get; set; }

        [DataMember]
        public string Item3 { get; set; }

        [DataMember]
        public string Item4 { get; set; }

        [DataMember]
        public string Item5 { get; set; }

        [DataMember]
        public object Item6 { get; set; }

        [DataMember]
        public object Item7 { get; set; }

        [DataMember]
        public object Item8 { get; set; }

        [DataMember]
        public object Item9 { get; set; }

        [DataMember]
        public object Item10 { get; set; }

        [DataMember]
        public DateTime? Date { get; set; }

        public OnesExportDataItem()
        {
            Selected = true;
        }
    }
}
