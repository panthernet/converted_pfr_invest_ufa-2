﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class SITransferDeleteListItem
    {
        [DataMember]
        public long SITransferId { get; set; }
        [DataMember]
        public long? ContractId { get; set; }
    }
}
