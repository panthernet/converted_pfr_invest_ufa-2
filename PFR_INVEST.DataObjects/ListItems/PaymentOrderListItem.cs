﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.Constants.Identifiers;

namespace PFR_INVEST.DataObjects.ListItems
{
	/// <summary>
	/// Представляет одну строку таблицы интерфейса
	/// Бэк-офис - Временное размещение - Загруженные п/п по депозитам
	/// </summary>
	public class PaymentOrderListItem : BaseDataObject
	{
		public PaymentOrderListItem() { }

		public PaymentOrderListItem(PaymentOrder transfer,string paymentDetails)
		{
			this.ID = transfer.ID;
			this.Date = transfer.DraftDate;
			this.ImportDate = transfer.DIDate;
			this.Direction = (PaymentOrder.Directions?)transfer.DirectionElID;
			this.PPNumber = transfer.DraftRegNum;
			this.PPDate = transfer.DraftDate;
			this.Sum = transfer.DraftAmount;
            this.PaymentDetails = paymentDetails;
			this.DocumentType = (PaymentOrder.DocumentType) transfer.DocumentTypeElID;
			this.BankID = transfer.BankID;
		}

		public long ID { get; set; }
		public int? Year { get { return Date.HasValue ? (int?)Date.Value.Year : null; } }
		public DateTime? Date { get; set; }

		public PaymentOrder.Directions? Direction { get; set; }
		public string PPNumber { get; set; }
		public DateTime? PPDate { get; set; }
		public DateTime? ImportDate { get; set; }
		public decimal? Sum { get; set; }
		public string PaymentDetails { get; set; }

		public PaymentOrder.DocumentType DocumentType { get; set; }
		public long? BankID { get; set; }

		public long? DirectionID { get { return (long?)Direction; } }
		public long DocumentTypeID { get { return (long)DocumentType; } }

		public decimal? SumSaldo { get { return (Direction == PaymentOrder.Directions.ToPFR) ? Sum : -Sum; } }
	}
}
