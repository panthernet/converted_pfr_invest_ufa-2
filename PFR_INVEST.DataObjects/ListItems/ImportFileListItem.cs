﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class ImportFileListItem : BaseDataObject
    {
        [DataMember]
        public string FilePath { get; set; }

        [DataMember]
        public byte[] FileContent { get; set; }

        [DataMember]
        public bool IsCompressed { get; set; }
    }
}
