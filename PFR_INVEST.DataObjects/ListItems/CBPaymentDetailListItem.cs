﻿using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ListItems
{
    [DataContract]
    public class CBPaymentDetailListItem
    {   
        [DataMember]
        public CBPaymentDetail CBPaymentDetail { get; set; }

        public CBPaymentDetailListItem(CBPaymentDetail item, Portfolio p, PaymentDetail pd)
        {
            CBPaymentDetail = item;

            ID = item.ID;
            PortfolioId = item.PortfolioID;
            PaymentDetailId = item.PaymentDetailID;
            ReportPaymentDetail = item.ReportPaymentDetail;

            PortfolioName = p.Name;
            PaymentDetailName = pd.PaymentDetails;
        }

        [DataMember]
        public long ID { get; set; }

        [DataMember]
        public long PortfolioId { get; set; }

        [DataMember]
        public string PortfolioName { get; set; }

        [DataMember]
        public long PaymentDetailId { get; set; }

        [DataMember]
        public string PaymentDetailName { get; set; }

        [DataMember]
        public string ReportPaymentDetail { get; set; }
    }
}
