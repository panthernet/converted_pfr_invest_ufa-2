﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class YearPlanListItem
    {
        public YearPlanListItem() { }

        public YearPlanListItem(long p_UKPlanID, string p_UKFormalizedName, string p_ContractNumber, decimal p_Sum, long? p_InsuredPersonsCount, decimal p_InvesmentIncome)
        {
            UKPlanID = p_UKPlanID;
            UKFormalizedName = p_UKFormalizedName;
            ContractNumber = p_ContractNumber;
            Sum = p_Sum;
            InsuredPersonsCount = p_InsuredPersonsCount.GetValueOrDefault();
            InvesmentIncome = p_InvesmentIncome;
        }

        /// <summary>
        /// ID плана по УК
        /// </summary>
        [DataMember]
        public virtual long UKPlanID { get; set; }

        /// <summary>
        /// Формализованное наименование УК
        /// </summary>
        [DataMember]
        public virtual string UKFormalizedName { get; set; }

        /// <summary>
        /// Номер договора
        /// </summary>
        [DataMember]
        public virtual string ContractNumber { get; set; }

        /// <summary>
        /// Сумма фактических сумм всех перечислений по данному плану
        /// </summary>
        [DataMember]
        public virtual decimal Sum { get; set; }

        /// <summary>
        /// Количество ЗЛ
        /// </summary>
        [DataMember]
        public virtual long InsuredPersonsCount { get; set; }

        /// <summary>
        /// Сумма всех инвестдоходов по всем перечислениям по данному плану
        /// </summary>
        [DataMember]
        public virtual decimal InvesmentIncome { get; set; }
    }
}
