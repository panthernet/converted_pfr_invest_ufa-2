﻿using System.Linq;

namespace PFR_INVEST.DataObjects.ServiceItems
{
	public class PortfolioTypeFilter : BaseDataObject
	{
		/// <summary>
		/// Флаг для инверированого применения фильтра. Все типы кроме перечисленых.
		/// </summary>
		public bool Exclude { get; set; }

		/// <summary>
		/// Список Типов для фильтра.
		/// </summary>
		public Portfolio.Types[] Types { get; set; }

		/// <summary>
		/// Проверка, что тип попадает в фильтр
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public bool IsMatch(Portfolio.Types type)
		{
		    return Exclude ? !Types.Contains(type) : Types.Contains(type);
		}

	    public static PortfolioTypeFilter DSV
		{
			get { return new PortfolioTypeFilter { Types = new[] { Portfolio.Types.DSV } }; }
		}

		public static PortfolioTypeFilter NotDSV
		{
			get { return new PortfolioTypeFilter { Types = new[] { Portfolio.Types.DSV }, Exclude = true }; }
		}
	}
}
