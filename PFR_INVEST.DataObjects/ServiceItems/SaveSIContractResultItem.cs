﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ServiceItems
{
    [DataContract(IsReference = true)]
    public class SaveSIContractResultItem
    {
        [DataMember]
        public long BankAccountId { get; set; }
        [DataMember]
        public long ContractId { get; set; }
        [DataMember]
        public long RewardCostId { get; set; }
        [DataMember]
        public long ContrInvestId { get; set; }
    }
}
