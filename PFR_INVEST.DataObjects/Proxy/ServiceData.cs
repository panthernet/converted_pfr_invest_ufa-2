﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.Proxy
{
    [DataContract]
    public class ServiceData
    {
        [DataMember]
        public Version Version { get; set; }

        [DataMember]
        public string DBText { get; set; }

        [DataMember]
        public string AuthText { get; set; }

    }
}
