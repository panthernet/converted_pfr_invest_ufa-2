﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace PFR_INVEST.Proxy
{
    [DataContract]
    public class DBEntity
    {
        protected Dictionary<string, DBField> fields = new Dictionary<string, DBField>();
        [DataMember]
        public Dictionary<string, DBField> Fields
        {
            get { return fields; }
            set { fields = value; }
        }

        public DBEntity(Dictionary<string, DBField> flds)
        {
            foreach (string key in flds.Keys)
                fields.Add(key, new DBField(flds[key]));
        }

        public override bool Equals(object obj)
        {
            if (base.Equals(obj)) return true;
            else
            {
                var entity =  obj as DBEntity;
                if (entity == null) return false;

                if (entity.Fields.Count != Fields.Count)
                    return false;

                if (entity.Fields.Any(field => !Fields.ContainsKey(field.Key)))
                {
                    return false;
                }

                foreach (KeyValuePair<string, DBField> field in Fields)
                {
                    if(entity.Fields.ContainsKey(field.Key))
                    {
                        var entityField = entity.Fields[field.Key];
                        if (entityField.DataType != field.Value.DataType)
                            return false;
                        if (entityField.DbName != field.Value.DbName)
                            return false;
                        if (entityField.DbTable != field.Value.DbTable)
                            return false;
                        if (entityField.FieldType != field.Value.FieldType)
                            return false;
                        if (entityField.IsDataChanged != field.Value.IsDataChanged)
                            return false;
                        if (entityField.Name != field.Value.Name)
                            return false;
                        if (entityField.Scale != field.Value.Scale)
                            return false;
                        if (entityField.Scheme != field.Value.Scheme)
                            return false;
                        return true;
                    }
                    return false;
                }
                return false;
            }
        }
    }
}
