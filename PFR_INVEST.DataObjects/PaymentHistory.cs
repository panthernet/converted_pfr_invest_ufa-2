﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class PaymentHistory : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long OfferPfrBankAccountSumId { get; set; }

        [DataMember]
        public virtual DateTime Date { get; set; }

        [DataMember]
        public virtual decimal Sum { get; set; }

        [DataMember]
        public virtual byte ForPercent { get; set; }

        [IgnoreDataMember]
        public virtual bool IsForPercent
        {
            get
            {
                if (ForPercent == 0) return false;
                return true;
            }
            set
            {
                ForPercent = (byte)(value ? 1 : 0);
            }
        }

        [DataMember]
        public virtual DateTime? OrderDate { get; set; }

        [DataMember]
        public virtual string OrderNumber { get; set; }

        [DataMember]
        public virtual string Comment { get; set; }
    }
}
