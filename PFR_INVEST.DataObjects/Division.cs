﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class Division : BaseDataObject, IMarkedAsDeleted, IIdentifiable
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        private string _name;
        public virtual string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    OnPropertyChanged("Name");
                }
            }
        }

        [DataMember]
        private string _shortName;
        public virtual string ShortName
        {
            get { return _shortName; }
            set
            {
                if (_shortName != value)
                {
                    _shortName = value;
                    OnPropertyChanged("ShortName");
                }
            }
        }

        [DataMember]
        private long _statusID = 1;
        public virtual long StatusID
        {
            get { return _statusID; }
            set
            {
                if (_statusID != value)
                {
                    _statusID = value;
                    OnPropertyChanged("StatusID");
                }
            }
        }
    }
}
