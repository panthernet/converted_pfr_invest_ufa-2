﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    using PFR_INVEST.DataObjects.Interfaces;

    public class F026SubGroup3 : BaseDataObject, IF026Part
	{
		[DataMember]
		public virtual long ID { get; set; }
		[DataMember]
		public virtual long? EdoID { get; set; }
		[DataMember]
		public virtual string DebName { get; set; }
		[DataMember]
		public virtual decimal? Amount { get; set; }
	}
}
