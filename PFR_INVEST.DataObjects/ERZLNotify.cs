﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    
    public class ERZLNotify : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }
  
        [DataMember]
        public virtual long? ERZL_ID { get; set; }

        [DataMember]
        public virtual DateTime? Date { get; set; }

        [DataMember]
        public virtual long? Count { get; set; }

        [DataMember]
        public virtual long CrAccID { get; set; }

        [DataMember]
        public virtual long DbtAccID { get; set; }

        [DataMember]
        public virtual string NotifyNum { get; set; }

        [DataMember]
        public virtual string MainNPF { get; set; }
    }
}
