﻿using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PFR_INVEST.DataObjects
{
	public class Person : BaseDataObject, IMarkedAsDeleted
	{
		[DataMember]
		public virtual long ID { get; set; }

		[DataMember]
		private string _lastName;
		public virtual string LastName
		{
			get { return _lastName; }
			set
			{
				if (_lastName != value)
				{
					_lastName = value;
					OnPropertyChanged("LastName");
				}
			}
		}

		[DataMember]
		private string _firstName;
		public virtual string FirstName
		{
			get { return _firstName; }
			set
			{
				if (_firstName != value)
				{
					_firstName = value;
					OnPropertyChanged("FirstName");
				}
			}
		}

		[DataMember]
		private string _middleName;
		public virtual string MiddleName
		{
			get { return _middleName; }
			set
			{
				if (_middleName != value)
				{
					_middleName = value;
					OnPropertyChanged("MiddleName");
				}
			}
		}

		[DataMember]
		private string _position;
		public virtual string Position
		{
			get { return _position; }
			set
			{
				if (_position != value)
				{
					_position = value;
					OnPropertyChanged("Position");
				}
			}
		}

		[DataMember]
		private string _phone;
		public virtual string Phone
		{
			get { return _phone; }
			set
			{
				if (_phone != value)
				{
					_phone = value;
					OnPropertyChanged("Phone");
				}
			}
		}

		[DataMember]
		private string _email;
		public virtual string Email
		{
			get { return _email; }
			set
			{
				if (_email != value)
				{
					_email = value;
					OnPropertyChanged("Email");
				}
			}
		}

		[DataMember]
		private string _fax;
		public virtual string Fax
		{
			get { return _fax; }
			set
			{
				if (_fax != value)
				{
					_fax = value;
					OnPropertyChanged("Fax");
				}
			}
		}

		[DataMember]
		private long? _divisionId;
		public virtual long? DivisionId
		{
			get { return _divisionId; }
			set
			{
				if (_divisionId != value)
				{
					_divisionId = value;
					OnPropertyChanged("DivisionId");
				}
			}
		}

		[DataMember]
		private int? _isDisabledFlag;
		public virtual int? IsDisabledFlag
		{
			get { return _isDisabledFlag; }
			set
			{
				if (_isDisabledFlag != value)
				{
					_isDisabledFlag = value;
					OnPropertyChanged("IsDisabledFlag");
				}
			}
		}

		[IgnoreDataMember]
		public virtual bool IsDisabled
		{
			get { return IsDisabledFlag == 1; }
			set
			{
				if (IsDisabled != value)
				{
					IsDisabledFlag = value ? 1 : 0;
				}
			}
		}

		[DataMember]
		private long _statusID = 1;
		public virtual long StatusID
		{
			get { return _statusID; }
			set
			{
				if (_statusID != value)
				{
					_statusID = value;
					OnPropertyChanged("StatusID");
				}
			}
		}

		[IgnoreDataMember]
		public virtual string FIOShort
		{
			get
			{
				string fio = (!string.IsNullOrEmpty(LastName.Trim()) ? (LastName.Trim() + " ") : "") + (!string.IsNullOrEmpty(FirstName) ? (FirstName.ToUpper().First() + ".") : "");

				if (!string.IsNullOrEmpty(MiddleName))
					fio += (" " + MiddleName.ToUpper().First() + ".").Trim();
				return fio;
			}
		}

		[IgnoreDataMember]
		public virtual string FullName
		{
			get
			{
				var sb = new StringBuilder();

			    if (!string.IsNullOrEmpty(LastName))
			    {
			        sb.Append(LastName);
			        sb.Append(" ");
			    }
                if(!string.IsNullOrEmpty(FirstName))
			        sb.Append(FirstName);

				if (!string.IsNullOrEmpty(MiddleName))
				{
					sb.Append(" ");
					sb.Append(MiddleName);
				}

				return sb.ToString();
			}
		}

        [IgnoreDataMember]
        public virtual string FormattedFullName
        {
            get
            {                
                StringBuilder sb = new StringBuilder();

                sb.Append(string.IsNullOrEmpty(LastName) ? string.Empty : CultureInfo.CurrentCulture.TextInfo.ToTitleCase(LastName).Trim() + " ");

                sb.Append(string.IsNullOrEmpty(FirstName) ? string.Empty : CultureInfo.CurrentCulture.TextInfo.ToTitleCase(FirstName.Trim()).First() + ". ");

                sb.Append(string.IsNullOrEmpty(MiddleName) ? string.Empty : CultureInfo.CurrentCulture.TextInfo.ToTitleCase(MiddleName.Trim()).First() + ".");

                
                return sb.ToString();
            }
        }

    }
}
