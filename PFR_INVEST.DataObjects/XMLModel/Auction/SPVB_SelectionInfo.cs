﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Globalization;


namespace PFR_INVEST.DataObjects.XMLModel.Auction
{
	/// <summary>
	/// Класс для сериализации "Уведомления о параметрах отбора заявок" для биржи СПВБ
	/// </summary>	
	[XmlRoot("SPCEX_DOC")]
	public class SPVB_SelectionInfo : SPVB_BaseDocument
	{

		public class DocBody : IXmlSerializable
		{
			public DateTime SelectDate { get; set; }
			public string ExecutorPhone { get; set; }
			public string ExecutorName { get; set; }

			public List<DepClaimSelectParams> Auctions { get; set; }


			public DocBody()
			{
				this.Auctions = new List<DepClaimSelectParams>();
			}

			public System.Xml.Schema.XmlSchema GetSchema()
			{
				return null;
			}

			public void ReadXml(System.Xml.XmlReader reader)
			{
				throw new NotImplementedException();
			}

			public void WriteXml(System.Xml.XmlWriter writer)
			{
				writer.WriteStartElement("selection_info");
				writer.WriteAttributeString("select_date", DateTime.Now.Date.ToXmlString());
				writer.WriteAttributeString("executor_phone", this.ExecutorPhone);
				writer.WriteAttributeString("executor_name", this.ExecutorName);
				writer.WriteAttributeString("records", this.Auctions.Count.ToString());


				foreach (var a in this.Auctions)
				{
					writer.WriteStartElement("info");
					writer.WriteAttributeString("maker", "PFRF__0001");
					writer.WriteAttributeString("board", "SPCEX");
					writer.WriteAttributeString("selection_date", a.SelectDate.ToXmlString());
					writer.WriteAttributeString("total_sum", a.MaxInsuranceFee.ToXmlString());
					writer.WriteAttributeString("period", a.Period.ToXmlString());
					writer.WriteAttributeString("paying_date", a.PlacementDate.ToXmlString());
					writer.WriteAttributeString("repaym_date", a.ReturnDate.ToXmlString());
					writer.WriteAttributeString("min_ord_rate", a.MinRate.ToXmlString());
					writer.WriteAttributeString("deposit_type", "S");
					writer.WriteAttributeString("min_ord_vol", a.MinDepClaimVolume.ToXmlString());
					writer.WriteAttributeString("max_order_cnt", a.MaxDepClaimAmount.ToXmlString());
					writer.WriteAttributeString("selection_type", "O");

					writer.WriteAttributeString("booking_time_start", a.DepClaimAcceptStart.ToXmlString());
					writer.WriteAttributeString("booking_time_finish", a.DepClaimAcceptEnd.ToXmlString());
					writer.WriteAttributeString("preliminary_mode_order_time_start", a.DepClaimListStart.ToXmlString());
					writer.WriteAttributeString("preliminary_mode_order_time_finish", a.DepClaimListEnd.ToXmlString());
					writer.WriteAttributeString("compete_mode_order_time_start", string.Empty);
					writer.WriteAttributeString("compete_mode_order_time_finish", string.Empty);
					writer.WriteAttributeString("order_reestr_time_start", string.Empty);
					writer.WriteAttributeString("order_reestr_time_finish", string.Empty);
					writer.WriteAttributeString("command_time_start", a.FilterStart.ToXmlString());
					writer.WriteAttributeString("command_time_finish", a.FilterEnd.ToXmlString());
					writer.WriteAttributeString("offer_time_start", a.OfferSendStart.ToXmlString());
					writer.WriteAttributeString("offer_time_finish", a.OfferSendEnd.ToXmlString());
					writer.WriteAttributeString("accept_time_start", a.OfferReceiveStart.ToXmlString());
					writer.WriteAttributeString("accept_time_finish", a.OfferReceiveStart.ToXmlString());
					writer.WriteAttributeString("comment", string.Empty);

					writer.WriteEndElement();
				}
				writer.WriteEndElement();
			}
		}

		[Obsolete("Only for XML Serializer", true)]	
		public SPVB_SelectionInfo() : this(new DepClaimSelectParams()) { }

		public SPVB_SelectionInfo(DepClaimSelectParams value)
			: base(value)
		{
			this.Sender = AddreseInfoWraper.PFR;
			this.Reciver = AddreseInfoWraper.SPVB;
			this.Document = new DocBody() { ExecutorPhone = "+74959820604", ExecutorName = "Пенсионный Фонд Российской Федерации" };
			this.Document.Auctions.Add(value);		
		}

		public override string FileType
		{
			get { return "SelInfo"; }
		}

		public override string DocType
		{
			get { return "selection_info"; }
		}

		[XmlElement("document")]
		public DocBody Document { get; set; }

	}
}
