﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace PFR_INVEST.DataObjects.XMLModel.KIP
{
	[XmlRoot("ФайлПФР")]
	public class KIP_Export : KIP_DocumentBase
	{
		public const string RECALL_MSK = "V_SVED_PD_RECALL_MSK_UK";
		public const string RECALL_MSK_TEXT = "СВЕДЕНИЯ_О_ПЛАТЕЖНОМ_ДОКУМЕНТЕ_МСК_ОТЗЫВ_ИЗ_УК";

		public const string TRANS_MSK = "V_SVED_PD_TRANS_MSK";
		public const string TRANS_MSK_TEXT = "СВЕДЕНИЯ_О_ПЛАТЕЖНОМ_ДОКУМЕНТЕ_МСК_ПЕРЕДАЧА";

		[XmlType("ExportDocumentInfo")]
		public class ExportDocumentInfo : DocumentInfo
		{
			[XmlElement("КоличествоОрганизаций")]
			public CountInfo CountData { get; set; }

			[XmlElement("ОснованиеПодачиРеестра")]
			public SourceDocumentInfo SourceDocument { get; set; }

			public ExportDocumentInfo()
			{
				SourceDocument = new SourceDocumentInfo();
				DocumentType = new DocType();
			}
		}

		public class RegisterItem
		{
			public class OrganizationInfo
			{
				[XmlElement("КодОрганизации")]
				public string Code { get; set; }
				[XmlElement("КодПортфеля")]
				public string PortfolioCode { get; set; }
				[XmlElement("ТипОрганизации")]
				public string OrgType { get; set; }
			}

			public class PPInfo
			{
				[XmlElement("НомерПлатежногоДокумента")]
				public string Number { get; set; }

				[XmlIgnore]
				public DateTime Date { get; set; }

				[XmlElement("ДатаПлатежногоДокумента")]
				public string DataText
				{
					get { return Date.ToXmlString(XMLTools.KIP_DATE_FORMAT); }
					set { Date = value.ParseAsDate(XMLTools.KIP_DATE_FORMAT); }
				}
			}

			public class ReciverInfo
			{
				[XmlElement("Банк")]
				public string BankName { get; set; }

				[XmlElement("БИК")]
				public string BIK { get; set; }

				[XmlElement("ИНН")]
				public string INN { get; set; }

				[XmlElement("КПП")]
				public string KPP { get; set; }

				[XmlElement("КоррСчет")]
				public string CorrAccount { get; set; }

				[XmlElement("РасчетныйСчет")]
				public string OperatingAccount { get; set; }
			}

			public class SummInfo
			{
				[XmlElement("ВзносыОПС")]
				public decimal OPS { get; set; }
				[XmlElement("ИДпоОПС")]
				public decimal OPSInvest { get; set; }
				[XmlElement("ВзносыДСВ")]
				public decimal DSV { get; set; }
				[XmlElement("ИДпоДСВ")]
				public decimal DSVInvest { get; set; }
				[XmlElement("ВзносыСофин")]
				public decimal SoFin { get; set; }
				[XmlElement("ИДпоСофин")]
				public decimal SoFinInvest { get; set; }
				[XmlElement("СредстваМСК")]
				public decimal MSK { get; set; }
				[XmlElement("ИДпоМСК")]
				public decimal MSKInvest { get; set; }
				[XmlElement("ВсегоСПН")]
				public decimal TotalSPN { get; set; }

				public void RecalcTotal()
				{
					TotalSPN = OPS + OPSInvest + DSV + DSVInvest + SoFin + SoFinInvest + MSK + MSKInvest;
				}

				public SummInfo()
				{
					OPS = OPSInvest = DSV = DSVInvest = SoFin = SoFinInvest = MSK = MSKInvest = TotalSPN = 0.00m;
				}
			}

			[XmlElement("НомерВпачке")]
			public int OrderNumber { get; set; }

			[XmlElement("Организация")]
			public OrganizationInfo Organization { get; set; }

			[XmlElement("ПлатежныйДокумент")]
			public List<PPInfo> PPList { get; set; }

			[XmlElement("СведенияОПолучателе")]
			public ReciverInfo Reciver { get; set; }

			[XmlElement("Сумма")]
			public SummInfo Summ { get; set; }

			public RegisterItem()
			{
				PPList = new List<PPInfo>();

			}
		}

		[XmlElement("ОПИСЬ")]
		public ExportDocumentInfo Info { get; set; }

		[XmlElement("СведенияОПлатеже")]
		public List<RegisterItem> Items { get; set; }


		public KIP_Export()
		{
			Info = new ExportDocumentInfo();
			Items = new List<RegisterItem>();
		}

		/// <summary>
		/// Вычисление зависимых полей, вызывать после заполнения документа
		/// </summary>
		public void RecalcFields()
		{
			var si = Items.Where(n => n.Organization.OrgType == "УК").ToList();
			var npf = Items.Where(n => n.Organization.OrgType == "НПФ").ToList();

			Info.CountData = new CountInfo()
			{
				CountUK = si.Count,
				TotalSumUK = Math.Round(si.Sum(s => s.Summ.TotalSPN), 2, MidpointRounding.AwayFromZero),
				CountNPF = npf.Count,
				TotalSumNPF = Math.Round(npf.Sum(s => s.Summ.TotalSPN), 2, MidpointRounding.AwayFromZero), 
			};

			if (Info.CountData.TotalSumNPF == 0) Info.CountData.TotalSumNPF = 0.00m;
			if (Info.CountData.TotalSumUK == 0) Info.CountData.TotalSumUK = 0.00m;


			for (var i = 1; i <= Items.Count; i++)
			{
				Items[i - 1].OrderNumber = i;
			}
		}
	}
}
