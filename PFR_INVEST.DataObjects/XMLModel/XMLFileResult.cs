﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.XMLModel
{

	public class XMLFileResult
	{
		[DataMember]
		public string FileName { get; set; }
		[DataMember]
		public string FileBody { get; set; }
		[DataMember]
		public byte[] FileBodyBinary { get; set; }

		[DataMember]
		public string Error { get; set; }

		[IgnoreDataMember]
		public bool IsSuccesss { get { return string.IsNullOrEmpty(this.Error); } }

		[IgnoreDataMember]
		public bool IsText { get { return !string.IsNullOrEmpty(this.FileBody); } }

		[IgnoreDataMember]
		public bool IsBinary { get { return this.FileBodyBinary != null; } }
	}
}
