﻿using System.Linq;
using System.ServiceModel;
using PFR_INVEST.Common.Logger;
using PFR_INVEST.DataAccess.Server;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Integration.KIPClient;
using PFR_INVEST.Proxy.Tools;

namespace db2connector.AsyncServices
{
    public class KIPNotifyTask : IAsyncTask
    {
        public void Process()
        {
            var files = IISService.LocalInstance.GetKIPExportReadyList();
            if (files.Count > 0)
            {
                var KIPFacade = new KIPClientFacade();

                using (var session = DataAccessSystem.OpenSession())
                {
                    foreach (var file in files)
                    {
                        var zip = IISService.LocalInstance.GetKIPExportZipFile(file);
                        try
                        {
                            LogManager.Log2KIP($"SEND[STEP1]: Sending file '{file}'...");
                            var result = KIPFacade.SendDocument(zip);


                            var KIPReg = session.CreateQuery("select r FROM KipRegister r where r.DocNum = :docnum")
                                                .SetParameter("docnum", file).SetMaxResults(1).List<KipRegister>().FirstOrDefault();


                            if (KIPIdentifiers.IsOKResult(result))
                            {
                                //файл ушёл нормально
                                //var Log = new KipLog()
                                //{
                                //    ErrorLevel = KipLog.ErrorLevels.NoError,
                                //    Meassage = string.Format("Документ успешно передан сервису КИП", result.outputData)
                                //};
                                //session.SaveOrUpdate(Log);
                                //session.Flush();

                                IISService.LocalInstance.SetKIPRegisterExportStatus(file, KipRegister.ExportStatuses.Exported);
                                LogManager.Log2KIP($"SEND[STEP3]: Transfer complete for '{file}'. STATUS=1");

                            }
                            else
                            {
                                //файл не принят
                                var log = new KipLog
                                {
                                    FileRepositoryID = KIPReg != null ? KIPReg.ExportFileID : null,
                                    ErrorLevel = KipLog.ErrorLevels.Communication,
                                    Meassage = $"Ошибка при передаче файла сервису КИП - '{result}'"

                                };
                                session.SaveOrUpdate(log);
                                session.Flush();
                                LogManager.Log2KIP(
                                    $"SEND[STEP3]: Transfer FAILED for '{file}'. ERR: {result}. STATUS=0", LogSeverity.Error);
                            }
                        }
                        catch (CommunicationException ex)
                        {
                            var log = new KipLog
                            {
                                ErrorLevel = KipLog.ErrorLevels.Communication,
                                Meassage = $"Ошибка связи с сервисом КИП - '{ex.Message}'"
                            };
                            session.SaveOrUpdate(log);
                            session.Flush();

                            LogManager.LogException(ex, $"SEND[STEP2]: SERVICE COMMS FAILED for '{file}'. STATUS=0", LogManager.KIPLog);
                            return;
                        }
                    }
                }
            }
        }
    }
}
