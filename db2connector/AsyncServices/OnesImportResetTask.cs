﻿using System;
using System.Linq;
using PFR_INVEST.DataAccess.Server;
using PFR_INVEST.DataObjects;

namespace db2connector.AsyncServices
{
	public class OnesImportResetTask : IAsyncTask
    {
		public void Process()
		{
            if(IISServiceHib.IsOnesImportInProgress) return;
			using (var session = DataAccessSystem.OpenSession())
			{
			    var list = session.CreateQuery(@"from OnesFileImport where ImportStatusID=4 and LoadDate >=:date ")
			        .SetParameter("date", new DateTime(2016, 8, 3))
			        .List<OnesFileImport>().ToList();
			    if (list.Count == 0) return;

			    session.CreateQuery(@"update OnesFileImport set ImportStatusID=6 where ID in (:list)")
                    .SetParameterList("list", list.Select(a=> a.ID).ToList())
                    .ExecuteUpdate();
			    using (var transaction = session.BeginTransaction())
			    {
			        try
			        {
			            list.ForEach(item =>
			            {
                            session.SaveOrUpdate(new OnesJournal
                            {
                                Action = (int)OnesJournal.Actions.General,
                                ImportID = item.ID,
                                LogDate = DateTime.Today,
                                LogType = (int)OnesJournal.LogTypes.Error,
                                SessionID = item.SessionID,
                                Message = "Импорт завершен вследствие внутренней ошибки или остановки сервиса"
                            });
			            });
			        }
			        catch
			        {
			            // ignored
			        }
			        transaction.Commit();
			    }
			}
		}
	}
}
