﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using PFR_INVEST.Proxy.Tools;

namespace db2connector.AsyncServices
{
	public class GenericScheduleService : IDisposable
	{
		public Thread ProcessThread;
		private bool _disposed;

		private readonly List<Action> _mCallDelegates = new List<Action>();
        public int SleepPeriodMs { get; private set; }

	    public GenericScheduleService()
	    {
	        SleepPeriodMs = ConfigurationManager.AppSettings.AllKeys.Contains("SchedulerTimeInterval") ? Convert.ToInt32(ConfigurationManager.AppSettings["SchedulerTimeInterval"]) : 60000;
	    }

	    public void AddDelegate(Action p_callDelegate)
		{
			_mCallDelegates.Add(p_callDelegate);
		}

		public void StartService()
		{
		    if (ProcessThread != null)
		    {
                try
                {
                    ProcessThread.Abort();
                }
                catch
                {
                    // ignored
                }
            }
			ProcessThread = new Thread(ThreadProc);
			ProcessThread.Start();
		}


		private void ThreadProc()
		{
			while (_disposed == false)
			{
				try
				{
                    LogManager.ASSLogWrap("Cycle started");
				    int counter = 1;
                    foreach (var del in _mCallDelegates.Where(d => d != null))
					{
					    try
					    {
					        del();
					        LogManager.ASSLogWrap($"Part {counter} ({del.Target}) completed");

                        }
					    catch (Exception ex)
					    {
					        LogManager.LogException(ex);
					        LogManager.ASSLogWrap($"Part {counter} ({del.Target}) failed");
                        }
                        finally
					    {
                            counter++;
                        }
                    }

				}
				catch(Exception ex)
				{
                    // ignored
                    LogManager.ASSLogWrap("fuuuuu....   " + ex);
                }

                LogManager.ASSLogWrap($"Cycle completed. Sleep for {SleepPeriodMs}");
                Thread.Sleep(SleepPeriodMs);
			}

			ProcessThread = null;
		}

		void IDisposable.Dispose()
		{
			if (ProcessThread == null)
				return;
			try
			{
				ProcessThread.Abort();
			}
			catch
			{
			    // ignored
			}
			finally
			{
				ProcessThread = null;
				_disposed = true;
			}
		}
	}
}
