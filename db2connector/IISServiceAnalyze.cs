﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.DataAccess.Server;
using PFR_INVEST.DataObjects.Analyze;
using PFR_INVEST.Proxy;

namespace db2connector
{
    /// <summary>
    /// Частичный класс сервиса загрузки данных для блока "Аналитические показатели ДОКИП"
    /// Часть методов возвращает Report, часть Data 
    /// Report - сущность отчета
    /// Data - сущность с данными отчетов
    /// </summary>
    public partial class IISServiceHib : IISServiceBase
    {
        //Report1-2
        public List<AnalyzePensionfundtonpfReport> GetPensionfundReportsByYearKvartal(int? year = null,
            int? kvartal = null, int? subtraction = null, bool archiveData = false)
        {
            var subCond = subtraction.HasValue ? $" and r.Subtraction = {subtraction}" : " and 1 = 1";
            using (var session = DataAccessSystem.OpenSession())
            {
                var list =
                    session.CreateQuery($@"{getSelectReportQuery(typeof(AnalyzePensionfundtonpfReport), year, kvartal, archiveData)} {subCond}")
                        .List<AnalyzePensionfundtonpfReport>();
                return list.ToList();
            }
        }

        public List<AnalyzePensionfundtonpfData> GetPensionfundDataByYearKvartal(int? year = null, int? kvartal = null,
            int? subtraction = null, bool archiveData = false)
        {
            var subtr = subtraction.HasValue ? $" and r.Subtraction = {subtraction}" : " and 1 = 1";
            var query = $@"{getDataReportQuery(typeof(AnalyzePensionfundtonpfData), typeof(AnalyzePensionfundtonpfReport), year, kvartal, archiveData)} {subtr}";

            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery(query).List<AnalyzePensionfundtonpfData>();
                return list.ToList();
            }
        }

        public List<AnalyzePensionfundtonpfData> GetPensionfundDataByReportId(long reportId)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list =
                    session.CreateQuery($@"select kl from {nameof(AnalyzePensionfundtonpfData)} kl where kl.ReportId = :rid")
                        .SetParameter("rid", reportId)
                        .List<AnalyzePensionfundtonpfData>();
                return list.ToList();
            }
        }

        #region Отчет 3
        public List<AnalyzePensionplacementData> GetPensionplacementDataByReportId(long reportId)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list =
                    session.CreateQuery($@"select kl from {nameof(AnalyzePensionplacementData)} kl where kl.ReportId = :rid")
                        .SetParameter("rid", reportId)
                        .List<AnalyzePensionplacementData>();
                return list.ToList();
            }

        }

        public List<AnalyzePensionplacementData> GetPensionplacementDataByYearKvartal(int? year, int? kvartal, bool archiveData = false)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery(getDataReportQuery(typeof(AnalyzePensionplacementData), typeof(AnalyzePensionplacementReport), year, kvartal, archiveData))
                    .List<AnalyzePensionplacementData>();
                return list.ToList();
            }

        }

        public List<AnalyzePensionplacementReport> GetPensionplacementReportByYearKvartal(int? year, int? kvartal, bool archiveData = false)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list =
                    session.CreateQuery(getSelectReportQuery(typeof(AnalyzePensionplacementReport), year, kvartal, archiveData))
                        .List<AnalyzePensionplacementReport>();
                return list.ToList();
            }
        }
        #endregion

        //Report4
        public List<AnalyzeIncomingstatementsReport> GetIncomingReportsByYearKvartal(int? year = null,
            int? kvartal = null, bool archiveData = false)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list =
                    session.CreateQuery(getSelectReportQuery(typeof(AnalyzeIncomingstatementsReport), year, kvartal, archiveData))
                        .List<AnalyzeIncomingstatementsReport>();
                return list.ToList();
            }
        }
        public List<AnalyzeIncomingstatementsData> GetIncomingstatementsDataByYearKvartal(int? year = null, int? kvartal = null, bool archiveData = false)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery(getDataReportQuery(typeof(AnalyzeIncomingstatementsData), typeof(AnalyzeIncomingstatementsReport), year, kvartal, archiveData))
                    .List<AnalyzeIncomingstatementsData>();
                return list.ToList();
            }
        }

        public List<AnalyzeIncomingstatementsData> GetIncomingstatementsDataByReportId(long rId)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list =
                    session.CreateQuery($@"select kl from {nameof(AnalyzeIncomingstatementsData)} kl where kl.ReportId = :rid")
                        .SetParameter("rid", rId)
                        .List<AnalyzeIncomingstatementsData>();
                return list.ToList();
            }
        }

        //Report5
        public List<AnalyzeInsuredpersonReport> GetInsuredpersonReportsByYearKvartal(int? year = null,
            int? kvartal = null , bool archiveData = false)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list =
                    session.CreateQuery(getSelectReportQuery(typeof(AnalyzeInsuredpersonReport), year, kvartal, archiveData))
                        .List<AnalyzeInsuredpersonReport>();
                return list.ToList();
            }
        }
        public List<AnalyzeInsuredpersonData> GetInsuredpersonDataByYearKvartal(int? year = null, int? kvartal = null, bool archiveData = false)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery(getDataReportQuery(typeof(AnalyzeInsuredpersonData), typeof(AnalyzeInsuredpersonReport), year, kvartal, archiveData))
                    .List<AnalyzeInsuredpersonData>();
                return list.ToList();
            }
        }

        public List<AnalyzeInsuredpersonData> GetInsuredpersonDataByReportId(long rId)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list =
                    session.CreateQuery($@"select kl from {nameof(AnalyzeInsuredpersonData)} kl where kl.ReportId = :rid")
                        .SetParameter("rid", rId)
                        .List<AnalyzeInsuredpersonData>();
                return list.ToList();
            }
        }

        //Report6
        public List<AnalyzeYieldfundsReport> GetYieldfundsReportsByYearKvartal(int? year = null, int? kvartal = null, int? actId = null, bool archiveData = false)
        {
            var actCond = actId.HasValue ? $@" and r.Act = {actId}" : "and 1 = 1";
            var query = $@"{getSelectReportQuery(typeof(AnalyzeYieldfundsReport), year, kvartal, archiveData)} {actCond}";
            using (var session = DataAccessSystem.OpenSession())
            {
                var list =
                    session.CreateQuery(query)
                        .List<AnalyzeYieldfundsReport>();
                return list.ToList();
            }
        }
        public List<AnalyzeYieldfundsData> GetYieldfundsDataByYearKvartal(int? year = null, int? kvartal = null, int? actId = null, bool archiveData = false)
        {
            var actCond = actId.HasValue ? $@" and r.Act = {actId}" : "and 1 = 1";
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery($"{getDataReportQuery(typeof(AnalyzeYieldfundsData), typeof(AnalyzeYieldfundsReport), year, kvartal, archiveData)} {actCond}")
                    .List<AnalyzeYieldfundsData>();
                return list.ToList();
            }
        }

        public List<AnalyzeYieldfundsData> GetYieldfundsDataByReportId(long rId)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list =
                    session.CreateQuery($@"select kl from {nameof(AnalyzeYieldfundsData)} kl where kl.ReportId = :rid")
                        .SetParameter("rid", rId)
                        .List<AnalyzeYieldfundsData>();
                return list.ToList();
            }
        }
        //Report7
        public List<AnalyzePaymentyearrateReport> GetPaymentyearrateReportByYearKvartal(int? year = null, int? kvartal = null, bool archiveData = false)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list =
                    session.CreateQuery(getSelectReportQuery(typeof(AnalyzePaymentyearrateReport), year, kvartal, archiveData))
                        .List<AnalyzePaymentyearrateReport>();
                return list.ToList();
            }
        }
        public List<AnalyzePaymentyearrateData> GetPaymentyearrateDataByYearKvartal(int? year = null, int? kvartal = null, bool archiveData = false)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery(getDataReportQuery(typeof(AnalyzePaymentyearrateData), typeof(AnalyzePaymentyearrateReport), year, kvartal, archiveData))
                    .List<AnalyzePaymentyearrateData>();
                return list.ToList();
            }
        }

        public List<AnalyzePaymentyearrateData> GetPaymentyearrateDataByReportId(long rId)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list =
                    session.CreateQuery($@"select kl from {nameof(AnalyzePaymentyearrateData)} kl where kl.ReportId = :rid")
                        .SetParameter("rid", rId)
                        .List<AnalyzePaymentyearrateData>();
                return list.ToList();
            }
        }

        //Helpers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity">Report Entity</param>
        /// <param name="year"></param>
        /// <param name="kvartal"></param>
        /// <param name="archiveData"></param>
        /// <returns></returns>
        private string getSelectReportQuery(Type entity, int? year = null,
           int? kvartal = null, bool archiveData = false)
        {
            var yearCond = year.HasValue ? $@" r.Year = {year}" : "1 = 1";
            var statusCond = archiveData ? $@" and r.Status = -1" : " and r.Status = 0";
            var kvartalCond = kvartal.HasValue ? $" and r.Kvartal = {kvartal}" : " and 1 = 1";
            return $@"select r from {entity.Name} r where {yearCond} {statusCond} {kvartalCond}";
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dEntity">Data Entity</param>
        /// <param name="rEntity">Report Entity</param>
        /// <param name="year"></param>
        /// <param name="kvartal"></param>
        /// <returns></returns>
        private string getDataReportQuery(Type dEntity, Type rEntity, int? year = null,
           int? kvartal = null, bool archiveData = false)
        {
            var yearCond = year.HasValue ? $@" r.Year = {year}" : "1 = 1";
            var statusCond = archiveData ? $@" and r.Status = -1" : " and r.Status = 0";
            var kvartalCond = kvartal.HasValue ? $" and r.Kvartal = {kvartal}" : " and 1 = 1";

            return
                $@"select kl from {dEntity.Name} kl
                                                    join kl.{rEntity.Name}  r
                                                    where {yearCond} 
                                                    {statusCond}
                                                    {kvartalCond}";


        }



    }
}