﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.Odbc;
using System.Linq;
using Npgsql;
using PFR_INVEST.Proxy;
using PFR_INVEST.Proxy.Tools;

namespace db2connector
{
    public class DBCommandLogging : IDisposable
    {
        private readonly IDbCommand _command;

        public DBCommandLogging(string query)
        {
            if (IISServiceHib.IsMockDb)
                _command = null;//new MockDbCommand();
            else _command = IISServiceBase.DBType == ServiceDBType.DB2 ? (DbCommand)new OdbcCommand(query, (OdbcConnection)CreateDBConnection()) : new NpgsqlCommand(query, (NpgsqlConnection)CreateDBConnection()) { CommandTimeout = 0 };
        }

        private static IDbConnection CreateDBConnection()
        {
            if (IISServiceHib.IsMockDb)
                return null;//new MockDbConnection();
            var conn = IISServiceBase.DBType == ServiceDBType.DB2
                ? (DbConnection)new OdbcConnection(IISServiceBase.DB_CONNECTION_STRING) : new NpgsqlConnection(IISServiceBase.DB_CONNECTION_STRING);
            conn.Open();
            return conn;
        }

        public void Prepare()
        {
            _command.Prepare();
        }

        public string CommandText
        {
            get { return _command.CommandText; }
            set { _command.CommandText = value; }
        }

        public int CommandTimeout
        {
            get { return _command.CommandTimeout; }
            set { _command.CommandTimeout = value; }
        }

        public CommandType CommandType
        {
            get { return _command.CommandType; }
            set { _command.CommandType = value; }
        }

        public UpdateRowSource UpdatedRowSource
        {
            get { return _command.UpdatedRowSource; }
            set { _command.UpdatedRowSource = value; }
        }

        public DbConnection Connection
        {
            get { return (DbConnection)_command.Connection; }
            set { _command.Connection = value; }
        }

        public DbParameterCollection Parameters
        {
            get { return (DbParameterCollection)_command.Parameters; }
        }

        public DbTransaction Transaction
        {
            get { return (DbTransaction)_command.Transaction; }
            set { _command.Transaction = value; }
        }

        public void Cancel()
        {
            _command.Cancel();
        }

        public DbParameter CreateParameter()
        {
            return (DbParameter)_command.CreateParameter();
        }

        public DbDataReader ExecuteReader(CommandBehavior behavior)
        {
            LogSqlQuery();
            return (DbDataReader)_command.ExecuteReader(behavior);
        }

        private void LogSqlQuery()
        {
            string sql = $"ODBC: {_command.CommandText}{(_command.CommandText.Last().ToString() == ";" ? string.Empty : ";")}";

            for (int i = 0; i < _command.Parameters.Count; i++)
            {
                var parameter = (DbParameter)_command.Parameters[i];
                sql += $" p{i}={parameter.Value}, DBType={parameter.DbType}, ValueType={parameter.Value.GetType().Name};";
            }

            if(LogManager.Instance.IsSqlTraceEnabled && LogManager.PlainSQLLog != null)
                LogManager.PlainSQLLog.Info(sql);
        }

        public DbDataReader ExecuteReader()
        {
            LogSqlQuery();
            return (DbDataReader)_command.ExecuteReader();
        }

        public int ExecuteNonQuery()
        {
            LogSqlQuery();
            return _command.ExecuteNonQuery();
        }

        public object ExecuteScalar()
        {
            LogSqlQuery();
            return _command.ExecuteScalar();
        }

        public void Dispose()
        {
            if (_command.Connection != null)
            {
                _command.Connection.Close();
                _command.Connection.Dispose();
            }
            _command.Cancel();
            _command.Dispose();
        }
    }
}
