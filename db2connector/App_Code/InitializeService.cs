﻿using System;
using System.Configuration;
using System.IO;
using log4net.Config;
using PFR_INVEST.Common.Logger;

namespace PFR_INVEST.Proxy
{
    public class InitializeService
    {
        public static void AppInitialize()
        {

            InitializeLogger();
        }

        private static void InitializeLogger()
        {
            XmlConfigurator.Configure();

            //LogManager.getSingleton().Log("получаем уровень вывода ошибок в лог");
            // получаем уровень вывода ошибок в лог.
            string severity = ConfigurationManager.AppSettings.Get("LogSeverity");
            LogSeverity severityValue;
            if (string.IsNullOrEmpty(severity))
                severityValue = LogSeverity.Error;
            else
                severityValue = (LogSeverity)Enum.Parse(typeof(LogSeverity), severity, true);

            // инициализируем экземпляр логгера полченным значением.
            Logger.Instance.Severity = severityValue;

            // получаем имя файла для вывода лога в файл.
            string logFileName = ConfigurationManager.AppSettings.Get("LogFile");
            if (string.IsNullOrEmpty(logFileName))
                logFileName = "PfrProxy.Log";

            logFileName = Path.Combine(
                Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "logs"),
                logFileName);

            // инициализируем вывод логов в файл.
            ObserverLogToFile fileLogger = new ObserverLogToFile(logFileName);
            Logger.Instance.Attach(fileLogger);

            //// инициализируем вывод лого в пользовательский журнал событий
            //var journalLogger = new ObserverLogToJournalDatabase("DSN=PTK_PROXY;");
            //Logger.Instance.Attach(journalLogger);

        }
    }
}
