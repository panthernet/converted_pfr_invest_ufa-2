﻿using PFR_INVEST.Integration.VIO;

namespace db2connector
{
	public partial class IISServiceHib
	{
		public void ParseVIOMessage(string message)
		{
			new MessageProcessor().ParseMessage(message);
		}
	}
}