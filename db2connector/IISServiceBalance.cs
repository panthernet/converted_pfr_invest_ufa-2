﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Proxy.DataContracts;
using PFR_INVEST.Proxy.Tools;

namespace db2connector
{
	public partial class IISService
	{
		private void FillBalanceSubquery(StringBuilder sb, List<SaldoParts> parts = null, DateTime? periodStart = null, DateTime? periodEnd = null)
		{
			int unions = 0;
			if (parts == null || parts.Contains(SaldoParts.Accoperation))
			{
				if (unions > 0) sb.Append("\r\nUNION ALL\r\n");
				SelectAccoperation(sb, periodStart, periodEnd);
				unions++;

			}
			if (parts == null || parts.Contains(SaldoParts.Apps))
			{
				if (unions > 0) sb.Append("\r\nUNION ALL\r\n");
				SelectApps(sb, periodStart, periodEnd);
				unions++;
			}
			if (parts == null || parts.Contains(SaldoParts.DoApps))
			{
				if (unions > 0) sb.Append("\r\nUNION ALL\r\n");
				SelectDoApps(sb, periodStart, periodEnd);
				unions++;
			}
			if (parts == null || parts.Contains(SaldoParts.AddSpn))
			{
				if (unions > 0) sb.Append("\r\nUNION ALL\r\n");
				SelectAddSpn(sb, periodStart, periodEnd);
				unions++;
			}
			if (parts == null || parts.Contains(SaldoParts.DopSpn_M))
			{
				if (unions > 0) sb.Append("\r\nUNION ALL\r\n");
				SelectDopSpn_M(sb, periodStart, periodEnd);
				unions++;
			}
			
			if (parts == null || parts.Contains(SaldoParts.OrdReport))
			{
				if (unions > 0) sb.Append("\r\nUNION ALL\r\n");
				SelectOrdReport(sb, periodStart, periodEnd);
				unions++;
			}
			if (parts == null || parts.Contains(SaldoParts.Cost))
			{
				if (unions > 0) sb.Append("\r\nUNION ALL\r\n");
				SelectCost(sb, periodStart, periodEnd);
				unions++;
			}
			if (parts == null || parts.Contains(SaldoParts.IncomeSec))
			{
				if (unions > 0) sb.Append("\r\nUNION ALL\r\n");
				SelectIncomeSec(sb, periodStart, periodEnd);
				unions++;
			}
			if (parts == null || parts.Contains(SaldoParts.Asg_Fin_Tr))
			{
				if (unions > 0) sb.Append("\r\nUNION ALL\r\n");
				SelectAsg_Fin_Tr(sb, periodStart, periodEnd);
				unions++;
			}
			if (parts == null || parts.Contains(SaldoParts.Req_Transfer))
			{
				if (unions > 0) sb.Append("\r\nUNION ALL\r\n");
				SelectReq_Transfer(sb, periodStart, periodEnd);
				unions++;
			}
			if (parts == null || parts.Contains(SaldoParts.Transfer))
			{
				if (unions > 0) sb.Append("\r\nUNION ALL\r\n");
				SelectTransfer(sb, periodStart, periodEnd);
				unions++;
			}
			if (parts == null || parts.Contains(SaldoParts.Return))
			{
				if (unions > 0) sb.Append("\r\nUNION ALL\r\n");
				SelectReturn(sb, periodStart, periodEnd);
				unions++;
			}
			if (parts == null || parts.Contains(SaldoParts.Deposit))
			{
				if (unions > 0) sb.Append("\r\nUNION ALL\r\n");
				SelectDeposit(sb, periodStart, periodEnd);
				unions++;
			}

			if (parts == null || parts.Contains(SaldoParts.BalanceExtra))
			{
				if (unions > 0) sb.Append("\r\nUNION ALL\r\n");
				SelectBalanceExtra(sb, periodStart, periodEnd);
				//unions++;
			}
		}

		private static List<DB2BalanceListItem> ReadData(DbDataReader r)
		{
			var result = new List<DB2BalanceListItem>();
			while (r.Read())
			{
			    var x = new DB2BalanceListItem
			    {
			        ID = ReadInt64Value(r, 0),
			        PORTFOLIO = ReadStringValue(r, 1),
			        PORTFOLIO_YEAR = ReadStringValue(r, 2),
			        DATE = ReadDateTimeValue(r, 3).Date,
			        KIND = ReadStringValue(r, 4),
			        SUMM = ReadDecimalValue(r, 5, 4),
			        CURRENCYSUMM = ReadDecimalValue(r, 6, 4),
			        ACCOUNT_TYPE = ReadStringValue(r, 7),
			        ACCOUNT = ReadStringValue(r, 8),
			        CURRENCY = ReadStringValue(r, 9),
			        Ord = ReadInt32Value(r, 10),
			        ForeignKey = ReadInt64Value(r, 11),
			        PORTFOLIO_TYPE = ReadInt64Value(r, 12),
			        SubQuery = ReadInt32Value(r, 13),
			        Content = (ReadStringValue(r, 14) ?? string.Empty).Trim()
			    };

			    result.Add(x);
			}

			return result;
		}

		public decimal GetROPSBalance(DateTime? onDate = null)
		{
			var sb = new StringBuilder();

			sb.Append(@"
				SELECT 
					SUM(CAST(a.SUMM as DECIMAL(30,4)))				
				FROM PFR_BASIC.CURRENCY c
				INNER JOIN
					(");

			FillBalanceSubquery(sb);


			sb.Append(@"
				) a ON a.CURR_ID = c.ID
				INNER JOIN PFR_BASIC.ACC_BANK_TYPE t ON t.ID = a.TYPE_ID ");

			sb.AppendLine();
		    sb.Append($" WHERE a.PORTFOLIOTYPE = {(long) Portfolio.Types.ROPS} ");
			sb.AppendLine();
			if (onDate.HasValue)
			    sb.Append($" AND a.ODATE <= '{onDate.Value.ToString("yyyy-MM-dd")}'");
			using (var cmd = new DBCommandLogging(sb.ToString()))
			{
				cmd.Prepare();
				var r = cmd.ExecuteReader();
				var balance = 0.00m;
                while (r.Read())
                {
					balance = ReadDecimalValue(r, 0, 4);
				}

			    var result = balance + GetDepositsSumDiffForROPS();
				return Math.Round(result, 2, MidpointRounding.AwayFromZero);
			}
		}

		

		public long GetCountBalanceList(List<SaldoParts> parts, DateTime? periodStart, DateTime? periodEnd)
		{
			var sb = new StringBuilder();

			sb.Append(@"
                SELECT count (*)
                FROM PFR_BASIC.CURRENCY c
                INNER JOIN (");

			FillBalanceSubquery(sb, parts, periodStart, periodEnd);

			sb.Append(@") a ON a.CURR_ID = c.ID
                        INNER JOIN PFR_BASIC.ACC_BANK_TYPE t ON t.ID = a.TYPE_ID ");

			var query = sb.ToString();
			LogManager.Log($" +++ query +++ {query}", LogManager.PlainSQLLog);
			using (var cmd = new DBCommandLogging(query))
			{
				long res;
				return long.TryParse(cmd.ExecuteScalar().ToString(), out res) ? res : -1;
			}
		}

		public List<DB2BalanceListItem> GetBalanceListByPage(List<SaldoParts> parts, int startIndex, DateTime? periodStart, DateTime? periodEnd)
		{
			var sb = new StringBuilder();

			sb.Append(
			    $@"
                SELECT * FROM (  SELECT 
                a.ID,
                a.PORTFOLIO,
                a.PORTFOLIO_YEAR,
                a.ODATE,
                a.DOCKIND,
                CAST(a.SUMM as DECIMAL(30,4)) as SUMM,
                CAST(a.VSUMM as DECIMAL(30,4)) as VSUMM,

                t.NAME,
                a.ACCOUNT,
                c.NAME,
                a.ORD,
                a.FK,
                a.PORTFOLIOTYPE,
                a.SUB_Q,
                TRIM(a.CONTENT),
                {(IsDB2
			        ? "ROWNUMBER()"
			        : "row_number()")} OVER () as ROWNUMBER
                FROM PFR_BASIC.CURRENCY c
                INNER JOIN
                  (");

			FillBalanceSubquery(sb, parts, periodStart, periodEnd);

			sb.Append($@"
                ) a ON a.CURR_ID = c.ID
                INNER JOIN PFR_BASIC.ACC_BANK_TYPE t ON t.ID = a.TYPE_ID 
                ORDER BY 2 ASC, 6 ASC, 10 ASC, 3 ASC, 9 ASC ) as msel
                WHERE ROWNUMBER BETWEEN {startIndex + 1} AND {startIndex + PageSize} ");

			string query = sb.ToString();
			LogManager.Log("+++" + query, LogManager.PlainSQLLog);

			using (var cmd = new DBCommandLogging(query))
			{
				cmd.Prepare();
				var r = cmd.ExecuteReader();
				var result = ReadData(r);
				return result;

			}

		}

		public List<DB2BalanceListItem> GetBalanceListByID(List<SaldoParts> parts, long id, DateTime? periodStart, DateTime? periodEnd)
		{
			var sb = new StringBuilder();

			sb.Append(
			    $@"
					SELECT b.* FROM (  SELECT 
					a.ID,
					a.PORTFOLIO,
					a.PORTFOLIO_YEAR,
					a.ODATE,
					a.DOCKIND,
					CAST(a.SUMM as DECIMAL(30,4)) as SUMM,
					CAST(a.VSUMM as DECIMAL(30,4)) as VSUMM,

					t.NAME,
					a.ACCOUNT,
					c.NAME,
					a.ORD,
					a.FK,
					a.PORTFOLIOTYPE,
					a.SUB_Q,
					TRIM(a.CONTENT),
					{(IsDB2
			        ? "ROWNUMBER()"
			        : "row_number()")} OVER () as ROWNUMBER
					FROM PFR_BASIC.CURRENCY c
					INNER JOIN
					  (");

			FillBalanceSubquery(sb, parts, periodStart, periodEnd);

			sb.Append(
			    $@"
					) a ON a.CURR_ID = c.ID
					INNER JOIN PFR_BASIC.ACC_BANK_TYPE t ON t.ID = a.TYPE_ID 
					WHERE a.ID = {id} 
					ORDER BY 2 ASC, 6 ASC, 10 ASC, 3 ASC, 9 ASC ) b
					");

			var query = sb.ToString();
			LogManager.Log("+++" + query, LogManager.PlainSQLLog);

			using (var cmd = new DBCommandLogging(query))
			{
				cmd.Prepare();
				var r = cmd.ExecuteReader();
				var result = ReadData(r);
				return result;
			}

		}

		#region Part queries


		private void SelectAccoperation(StringBuilder sb, DateTime? periodStart, DateTime? periodEnd)
		{
			//ACCOPERATION
			//если перечисление с валютного на рублевый или с валютного на валютный, умножаем на курс
			//перепутаны местами источник и цель
			sb.Append($@"
                   SELECT DISTINCT a.ID,
                   p.YEAR AS PORTFOLIO,
                   y.NAME AS PORTFOLIO_YEAR,
                   p.TYPE_ID AS PORTFOLIOTYPE,
                   a.NEWDATE AS ODATE,
                   k.NAME AS DOCKIND,
                   a.SUMM * CASE
                                WHEN ((b.CURR_ID IS NOT NULL
                                       AND b2.CURR_ID IS NOT NULL
                                       AND b.CURR_ID = 1
                                       AND b2.CURR_ID <> 1)
                                      OR (b.CURR_ID IS NOT NULL
                                          AND b2.CURR_ID IS NOT NULL
                                          AND b.CURR_ID <> 1
                                          AND b2.CURR_ID <> 1)) THEN COALESCE(c.VALUE,
                                                 						CASE
                                                                         WHEN
                                                                                (SELECT r.ID
                                                                                 FROM PFR_BASIC.CURS r
                                                                                 WHERE a.CURR_ID = r.CURR_ID
                                                                                   AND a.NEWDATE>=r.DATE
                                                                                 ORDER BY r.DATE DESC FETCH FIRST 1 ROWS ONLY) IS NULL THEN 1
                                                                         ELSE
                                                                                (SELECT r.VALUE
                                                                                 FROM PFR_BASIC.CURS r
                                                                                 WHERE a.CURR_ID = r.CURR_ID
                                                                                   AND a.NEWDATE>=r.DATE
                                                                                 ORDER BY r.DATE DESC FETCH FIRST 1 ROWS ONLY)
                                                                     END)
                                ELSE 1
                            END * CASE a.DOCKIND
                                      WHEN 4 THEN -1
                                      ELSE 1
                                  END AS SUMM,
                                  a.SUMM * CASE a.DOCKIND
                                               WHEN 4 THEN -1
                                               ELSE 1
                                           END AS VSUMM,
                                  b.ACCOUNTNUM AS ACCOUNT,
                                  b.CURR_ID AS CURR_ID,
                                  {(int)SaldoParts.Accoperation} AS ORD,
                                  1 AS SUB_Q,
                                  TRIM(a.CONTENT){(IsDB2? "" : "::TEXT")} AS CONTENT,
                                  b.TYPE_ID AS TYPE_ID,
                                  0 AS FK
   FROM 
   		PFR_BASIC.ACCOPERATION a   		
        LEFT JOIN PFR_BASIC.PORTFOLIO p ON p.ID=a.PF_ID
        LEFT JOIN PFR_BASIC.YEARS y ON p.YEAR_ID=y.ID
        JOIN PFR_BASIC.PFRBANKACCOUNT b ON b.ID = a.PFR_BA_ID
        JOIN PFR_BASIC.PFRBANKACCOUNT b2 ON b2.ID=a.PFR_BA_S_ID
        JOIN PFR_BASIC.ACCOPERATION_KIND k ON k.ID=a.DOCKIND
        LEFT JOIN PFR_BASIC.CURS c ON c.ID = a.CURS_ID {GetFilterCondition(true, "a.NEWDATE", periodStart, periodEnd)}
");


			//ACCOPERATION
			//Отдельно для тех, у кого PFR_BA_S_ID = Null
			sb.Append($@"
   UNION ALL SELECT DISTINCT a.ID,
                         p.YEAR AS PORTFOLIO,
                         y.NAME AS PORTFOLIO_YEAR,
                         p.TYPE_ID AS PORTFOLIOTYPE,
                         a.NEWDATE AS ODATE,
                         k.NAME AS DOCKIND,
                         a.SUMM * CASE
                                      WHEN (b.CURR_ID IS NOT NULL
                                            AND b.CURR_ID <> 1) THEN COALESCE(c.VALUE,
                                                 						CASE
                                                                         WHEN
                                                                                (SELECT r.ID
                                                                                 FROM PFR_BASIC.CURS r
                                                                                 WHERE a.CURR_ID = r.CURR_ID
                                                                                   AND a.NEWDATE>=r.DATE
                                                                                 ORDER BY r.DATE DESC FETCH FIRST 1 ROWS ONLY) IS NULL THEN 1
                                                                         ELSE
                                                                                (SELECT r.VALUE
                                                                                 FROM PFR_BASIC.CURS r
                                                                                 WHERE a.CURR_ID = r.CURR_ID
                                                                                   AND a.NEWDATE>=r.DATE
                                                                                 ORDER BY r.DATE DESC FETCH FIRST 1 ROWS ONLY)
                                                                     END)
                                      ELSE 1
                                  END * CASE a.DOCKIND
                                            WHEN 4 THEN -1
                                            ELSE 1
                                        END AS SUMM,
                                        a.SUMM * CASE a.DOCKIND
                                                     WHEN 4 THEN -1
                                                     ELSE 1
                                                 END AS VSUMM,
                                        b.ACCOUNTNUM AS ACCOUNT,
                                        b.CURR_ID AS CURR_ID,
                                        {(int)SaldoParts.Accoperation} AS ORD,
                                        2 AS SUB_Q,
                                        TRIM(a.CONTENT){(IsDB2 ? "" : "::TEXT")} AS CONTENT,
                                        b.TYPE_ID AS TYPE_ID,
                                        0 AS FK
    FROM 
   		PFR_BASIC.ACCOPERATION a   		
        JOIN PFR_BASIC.PORTFOLIO p ON p.ID=a.PF_ID
        LEFT JOIN PFR_BASIC.YEARS y ON p.YEAR_ID=y.ID
        JOIN PFR_BASIC.PFRBANKACCOUNT b ON a.PFR_BA_ID = b.ID       
        JOIN PFR_BASIC.ACCOPERATION_KIND k ON k.ID=a.DOCKIND
        LEFT JOIN PFR_BASIC.CURS c ON c.ID = a.CURS_ID
    WHERE
        a.PFR_BA_S_ID IS NULL {GetFilterCondition(false, "a.NEWDATE", periodStart, periodEnd)}
");

			//ACCOPERATION
			//Минусовые версии для перевода между счетами
			sb.AppendFormat($@"
            UNION ALL 
            SELECT DISTINCT a.ID,
                            p.YEAR AS PORTFOLIO,
                            y.NAME AS PORTFOLIO_YEAR,
                            p.TYPE_ID AS PORTFOLIOTYPE,
                            a.NEWDATE AS ODATE,
                            k.NAME AS DOCKIND,
                            -1 * a.SUMM * CASE
                                           WHEN (b.CURR_ID IS NOT NULL
                                                 AND b.CURR_ID <> 1) THEN COALESCE(c.VALUE,
                                                 						CASE
                                                                              WHEN
                                                                                     (SELECT r.ID
                                                                                      FROM PFR_BASIC.CURS r
                                                                                      WHERE a.CURR_ID = r.CURR_ID
                                                                                        AND a.NEWDATE>=r.DATE
                                                                                      ORDER BY r.DATE DESC FETCH FIRST 1 ROWS ONLY) IS NULL THEN 1
                                                                              ELSE
                                                                                     (SELECT r.VALUE
                                                                                      FROM PFR_BASIC.CURS r
                                                                                      WHERE a.CURR_ID = r.CURR_ID
                                                                                        AND a.NEWDATE>=r.DATE
                                                                                      ORDER BY r.DATE DESC FETCH FIRST 1 ROWS ONLY)
                                                                          END)
                                           ELSE 1
                                       END  AS SUMM,
                                            -1 * a.SUMM AS VSUMM,
                                            b.ACCOUNTNUM AS ACCOUNT,
                                            b.CURR_ID AS CURR_ID,
                                            {(int)SaldoParts.Accoperation} AS ORD,
                                            3 AS SUB_Q,
                                            TRIM(a.CONTENT){(IsDB2? "" : "::TEXT")} AS CONTENT,
                                            b.TYPE_ID AS TYPE_ID,
                                            0 AS FK
   FROM 
   		PFR_BASIC.ACCOPERATION a   		
        JOIN PFR_BASIC.PORTFOLIO p ON p.ID=a.PF_S_ID
        LEFT JOIN PFR_BASIC.YEARS y ON p.YEAR_ID=y.ID
        JOIN PFR_BASIC.PFRBANKACCOUNT b ON a.PFR_BA_S_ID = b.ID     
        JOIN PFR_BASIC.ACCOPERATION_KIND k ON k.ID=a.DOCKIND
        LEFT JOIN PFR_BASIC.CURS c ON c.ID = a.CURS_ID
   WHERE --a.DOCKIND IN (3, 4)
        1=1  {GetFilterCondition(false, "a.NEWDATE", periodStart, periodEnd)}");
		}

		private void SelectApps(StringBuilder sb, DateTime? periodStart, DateTime? periodEnd)
		{
			//APS
			//приход
			sb.AppendFormat($@"
   SELECT 
a.ID,
p.YEAR AS PORTFOLIO,
y.NAME AS PORTFOLIO_YEAR,
p.TYPE_ID AS PORTFOLIOTYPE,
a.DATE AS ODATE,
CONCAT(CASE UPPER(TRIM(el.Name)) WHEN TRIM(' ДСВ') THEN CONCAT(k.NAME, ' ДСВ') ELSE k.NAME END, '/приход') AS DOCKIND,
a.SUMM AS SUMM,
a.SUMM AS VSUMM,
b.ACCOUNTNUM AS ACCOUNT,
b.CURR_ID AS CURR_ID,
{(int)SaldoParts.Apps} AS ORD,
1 AS SUB_Q,
''{(IsDB2? "" : "::TEXT")} AS CONTENT,
b.TYPE_ID AS TYPE_ID,
a.ID AS FK
   FROM PFR_BASIC.APS a
   INNER JOIN PFR_BASIC.PORTFOLIO p ON a.PF_ID=p.ID
   INNER JOIN PFR_BASIC.PFRBANKACCOUNT b ON a.PBA_ID = b.ID
   INNER JOIN PFR_BASIC.APS_KIND k ON a.DOCKIND=k.ID
   INNER JOIN PFR_BASIC.ELEMENT el ON el.ID = p.TYPE_ID
   LEFT JOIN PFR_BASIC.YEARS y ON p.YEAR_ID=y.ID {GetFilterCondition(true, "a.DATE", periodStart, periodEnd)}");

			//APS
			//расход
			sb.AppendFormat($@"
UNION ALL SELECT 
a.ID, 
p.YEAR AS PORTFOLIO, 
y.NAME AS PORTFOLIO_YEAR, 
p.TYPE_ID AS PORTFOLIOTYPE, 
a.DATE AS ODATE, 
   CONCAT(CASE UPPER(TRIM(el.NAME)) WHEN TRIM(' ДСВ') THEN CONCAT(k.NAME, ' ДСВ') ELSE k.NAME END, '/расход') AS DOCKIND, 
-a.SUMM AS SUMM, 
-a.SUMM AS VSUMM,
b.ACCOUNTNUM AS ACCOUNT,
b.CURR_ID AS CURR_ID,
{(int)SaldoParts.Apps} AS ORD,
2 AS SUB_Q,
''{(IsDB2 ? "" : "::TEXT")} AS CONTENT,
b.TYPE_ID AS TYPE_ID,
a.ID AS FK
   FROM PFR_BASIC.APS a
   INNER JOIN PFR_BASIC.PORTFOLIO p ON a.SPF_ID=p.ID
   INNER JOIN PFR_BASIC.PFRBANKACCOUNT b ON a.SPBA_ID = b.ID
   INNER JOIN PFR_BASIC.APS_KIND k ON a.DOCKIND=k.ID
   INNER JOIN PFR_BASIC.ELEMENT el ON el.ID = p.TYPE_ID
   LEFT JOIN PFR_BASIC.YEARS y ON p.YEAR_ID=y.ID {GetFilterCondition(true, "a.DATE", periodStart, periodEnd)}");
		}

		private void SelectDoApps(StringBuilder sb, DateTime? periodStart, DateTime? periodEnd)
		{
			//DOPAPS
			//приход
			sb.AppendFormat($@"
    SELECT 
a.ID,
p.YEAR AS PORTFOLIO,
y.NAME AS PORTFOLIO_YEAR,
p.TYPE_ID AS PORTFOLIOTYPE,
a.DATE AS ODATE,
CONCAT(CASE UPPER(TRIM(el.NAME)) WHEN TRIM(' ДСВ') THEN CONCAT(k.NAME, ' ДСВ') ELSE k.NAME END, '/приход') AS DOCKIND,
a.CHSUMM AS SUMM,
a.CHSUMM AS VSUMM,
b.ACCOUNTNUM AS ACCOUNT,
b.CURR_ID AS CURR_ID,
{(int)SaldoParts.DoApps} AS ORD,
1 AS SUB_Q,
''{(IsDB2 ? "" : "::TEXT")} AS CONTENT,
b.TYPE_ID AS TYPE_ID,
a.APS_ID AS FK
   FROM PFR_BASIC.DOPAPS a
   INNER JOIN PFR_BASIC.APS s ON a.APS_ID=s.ID
   INNER JOIN PFR_BASIC.PORTFOLIO p ON a.PF_ID=p.ID
   INNER JOIN PFR_BASIC.PFRBANKACCOUNT b ON a.PBA_ID = b.ID
   INNER JOIN PFR_BASIC.DOPAPS_KIND k ON k.ID=a.DOCKIND
INNER JOIN PFR_BASIC.ELEMENT el ON el.ID = p.TYPE_ID
   LEFT JOIN PFR_BASIC.YEARS y ON p.YEAR_ID=y.ID {GetFilterCondition(true, "a.DATE", periodStart, periodEnd)}");
			//DOPAPS
			//расход
			sb.AppendFormat($@"
   UNION ALL SELECT 
a.ID, 
p.YEAR AS PORTFOLIO, 
y.NAME AS PORTFOLIO_YEAR, 
p.TYPE_ID AS PORTFOLIOTYPE, 
a.DATE AS ODATE, 
CONCAT(CASE UPPER(TRIM(el.NAME)) WHEN TRIM(' ДСВ') THEN CONCAT(k.NAME, ' ДСВ') ELSE k.NAME END, '/расход') AS DOCKIND, 
-a.CHSUMM AS SUMM, 
-a.CHSUMM AS VSUMM,
b.ACCOUNTNUM AS ACCOUNT,
b.CURR_ID AS CURR_ID,
{(int)SaldoParts.DoApps} AS ORD,
2 AS SUB_Q,
''{(IsDB2 ? "" : "::TEXT")} AS CONTENT,
b.TYPE_ID AS TYPE_ID,
a.APS_ID AS FK
   FROM PFR_BASIC.DOPAPS a
   INNER JOIN PFR_BASIC.APS s ON a.APS_ID=s.ID
   INNER JOIN PFR_BASIC.PORTFOLIO p ON a.SPF_ID=p.ID
   INNER JOIN PFR_BASIC.PFRBANKACCOUNT b ON a.SPBA_ID = b.ID
   INNER JOIN PFR_BASIC.DOPAPS_KIND k ON k.ID=a.DOCKIND
INNER JOIN PFR_BASIC.ELEMENT el ON el.ID = p.TYPE_ID
   LEFT JOIN PFR_BASIC.YEARS y ON p.YEAR_ID=y.ID {GetFilterCondition(true, "a.DATE", periodStart, periodEnd)}");
		}

		private void SelectAddSpn(StringBuilder sb, DateTime? periodStart, DateTime? periodEnd)
		{
			//ADDSPN
			sb.AppendFormat($@"
   SELECT 
a.ID,
p.YEAR AS PORTFOLIO,
y.NAME AS PORTFOLIO_YEAR,
p.TYPE_ID AS PORTFOLIOTYPE,
a.NEWDATE AS ODATE,
CASE a.DOCKIND
    WHEN 1 THEN CASE UPPER(el.NAME)
                    WHEN 'ДСВ' THEN 'Страховые взносы ДСВ'
                    ELSE 'Страховые взносы'
                END
    WHEN 2 THEN 'Пени и штрафы'
    WHEN 3 THEN CASE UPPER(el.NAME)
        WHEN 'ДСВ' THEN 'Уточнение ДСВ за год'
        ELSE 'Уточнение за квартал'
    END
	WHEN 4 THEN 'Уточнение пени и штрафов за квартал'
END AS DOCKIND,
a.SUMM AS SUMM,
a.SUMM AS VSUMM,
b.ACCOUNTNUM AS ACCOUNT,
b.CURR_ID AS CURR_ID,
{(int)SaldoParts.AddSpn} AS ORD,
1 AS SUB_Q,
''{(IsDB2 ? "" : "::TEXT")} AS CONTENT,
b.TYPE_ID AS TYPE_ID,
a.ID AS FK
   FROM PFR_BASIC.ADDSPN a
   INNER JOIN PFR_BASIC.PORTFOLIO p ON a.PF_ID=p.ID
   INNER JOIN PFR_BASIC.PFRBANKACCOUNT b ON a.PBA_ID = b.ID
   INNER JOIN PFR_BASIC.ELEMENT el ON el.ID = p.TYPE_ID
   LEFT JOIN PFR_BASIC.YEARS y ON p.YEAR_ID=y.ID {GetFilterCondition(true, "a.NEWDATE", periodStart, periodEnd)}");
		}

		private void SelectDopSpn_M(StringBuilder sb, DateTime? periodStart, DateTime? periodEnd)
		{
			//DOPSPN_M
			sb.AppendFormat($@"
    SELECT a.ID,
                p.YEAR AS PORTFOLIO,
                y.NAME AS PORTFOLIO_YEAR,
                p.TYPE_ID AS PORTFOLIOTYPE,
                a.DATE AS ODATE,
                CASE a.DOCKIND
                    WHEN 1 THEN CASE UPPER(el.NAME)
                                    WHEN 'ДСВ' THEN 'Уточнение ДСВ'
                                    ELSE 'Уточнение страховых взносов'
                                END
                    WHEN 2 THEN 'Уточнение пени и штрафов'
                    WHEN 3 THEN CASE UPPER(el.NAME)
                        WHEN 'ДСВ' THEN 'Уточнение ДСВ за год'
                        ELSE 'Уточнение за квартал'
                    END
                    WHEN 4 THEN 'Уточнение пени и штрафов за квартал'
                END AS DOCKIND,
                CASE a.DOCKIND
                    WHEN 1 THEN a.CHSUMM
                    ELSE a.CHSUMM
                END AS SUMM,
                CASE a.DOCKIND
                    WHEN 1 THEN a.CHSUMM
                    ELSE a.CHSUMM
                END AS VSUMM,
                b.ACCOUNTNUM AS ACCOUNT,
                b.CURR_ID AS CURR_ID,
                {(int)SaldoParts.DopSpn_M} AS ORD,
                1 AS SUB_Q,
                ''{(IsDB2 ? "" : "::TEXT")} AS CONTENT,
                b.TYPE_ID AS TYPE_ID,
                a.ASPN_ID AS FK
   FROM PFR_BASIC.DOPSPN_M a
   INNER JOIN PFR_BASIC.PORTFOLIO p ON a.PF_ID=p.ID
   INNER JOIN PFR_BASIC.ADDSPN s ON s.ID=a.ASPN_ID
   INNER JOIN PFR_BASIC.PFRBANKACCOUNT b ON b.ID = coalesce(a.QUARTER_PBA_ID, s.PBA_ID)
INNER JOIN PFR_BASIC.ELEMENT el ON el.ID = p.TYPE_ID
   LEFT JOIN PFR_BASIC.YEARS y ON p.YEAR_ID=y.ID

    WHERE a.QUARTER is null {GetFilterCondition(false, "a.DATE", periodStart, periodEnd)}");

			//Квартальные Уточнения
			sb.AppendFormat($@"
   UNION ALL SELECT 
a.ID,
                p.YEAR AS PORTFOLIO,
                y.NAME AS PORTFOLIO_YEAR,
                p.TYPE_ID AS PORTFOLIOTYPE,
                a.DATE AS ODATE,
                CASE a.DOCKIND
                    WHEN 1 THEN 
                                CONCAT(CASE UPPER(el.NAME)
                                    WHEN 'ДСВ' THEN 'Уточнение ДСВ'
                                    ELSE 'Уточнение страховых взносов'
                                END 
                                ,
                                CASE a.QUARTER
                                	WHEN 1 THEN ' за период 3 месяца'
                                	WHEN 2 THEN ' за период 6 месяцев'
                                	WHEN 3 THEN ' за период 9 месяцев'
                                	WHEN 4 THEN ' за период 12 месяцев'
                                	ELSE ''
                                END)
                    WHEN 2 THEN 'Уточнение пени и штрафов'
                    WHEN 3 THEN CASE UPPER(el.NAME)
                        WHEN 'ДСВ' THEN 'Уточнение ДСВ за год'
                        ELSE 'Уточнение за квартал'
                    END
                    WHEN 4 THEN 'Уточнение пени и штрафов за квартал'
                END AS DOCKIND,
                CASE a.DOCKIND
                    WHEN 1 THEN a.CHSUMM
                    ELSE a.CHSUMM
                END AS SUMM,
                CASE a.DOCKIND
                    WHEN 1 THEN a.CHSUMM
                    ELSE a.CHSUMM
                END AS VSUMM,
                b.ACCOUNTNUM AS ACCOUNT,
                b.CURR_ID AS CURR_ID,
                {(int)SaldoParts.DopSpn_M} AS ORD,
                2 AS SUB_Q,
                ''{(IsDB2 ? "" : "::TEXT")} AS CONTENT,
                b.TYPE_ID AS TYPE_ID,
                a.ID AS FK
   FROM PFR_BASIC.DOPSPN_M a
   INNER JOIN PFR_BASIC.PORTFOLIO p ON a.PF_ID=p.ID
INNER JOIN PFR_BASIC.ELEMENT el ON el.ID = p.TYPE_ID
   LEFT JOIN PFR_BASIC.ADDSPN s ON s.ID=a.ASPN_ID
   INNER JOIN PFR_BASIC.PFRBANKACCOUNT b ON a.QUARTER_PBA_ID = b.ID
   LEFT JOIN PFR_BASIC.YEARS y ON p.YEAR_ID=y.ID
   
   WHERE a.QUARTER is not null {GetFilterCondition(false, "a.DATE", periodStart, periodEnd)}");
		}


		private void SelectOrdReport(StringBuilder sb, DateTime? periodStart, DateTime? periodEnd)
		{
            //ЛОГИКА ТАК ЖЕ ИСПОЛЬЗУЕТСЯ В GetOrdReportForBOReport()
			//ORD_REPORT
			//Считаем сумму для отображения сделок по ЦБ
			//Считаем непогашеный остаток номинала по последней выплате, если выплаты нет возвращаем номинал ЦБ
			//Условие: если дата сделки = дате последней выплаты, то возвращаем 0
			//Считаем непогашеный остаток номинала по последней выплате, если выплаты нет возвращаем номинал ЦБ
			sb.AppendFormat($@"
        SELECT a.ID,
                p.YEAR 		AS PORTFOLIO,
                y.NAME		AS PORTFOLIO_YEAR,
                p.TYPE_ID 		AS PORTFOLIOTYPE,
                a.DATEORDER AS ODATE,
                CASE o.TYPE
                    WHEN 'покупка' THEN 'покупка ЦБ'
                    WHEN 'продажа' THEN 'продажа ЦБ'
                END AS DOCKIND,                
				CAST((
                	coalesce(a.ONE_NOMVALUE, s.NOMVALUE) * a.PRICE / 100
                	+ CAST(CASE WHEN ONE_NKD IS NULL THEN coalesce(NKD,0)/a.Count ELSE coalesce(ONE_NKD,0) END  as DECIMAL(30,14)))
                	* a.Count as DECIMAL(30, 4))
                *
                CASE 
				WHEN s.CURR_ID = 1 
				THEN 1 
				ELSE COALESCE(
					(SELECT cr.VALUE AS V
					 FROM PFR_BASIC.CURS cr
					 WHERE s.CURR_ID = cr.CURR_ID
					   AND CASE s.CURR_ID WHEN 1 THEN a.DATEORDER ELSE a.DATEDEPO END >= cr.DATE
					 ORDER BY cr.DATE DESC FETCH FIRST 1 ROWS ONLY) , 1) 
				END 
				*
				CASE o.TYPE
                    WHEN 'покупка' THEN -1
                    WHEN 'продажа' THEN 1
                END
				AS SUMM, 
                CAST((
                	coalesce(a.ONE_NOMVALUE, s.NOMVALUE) * a.PRICE / 100
                	+ CAST(CASE WHEN ONE_NKD IS NULL THEN coalesce(NKD,0)/a.Count ELSE coalesce(ONE_NKD,0) END as DECIMAL(30,14)))
                	* a.Count as DECIMAL(30, 4))
				*
				CASE o.TYPE
                    WHEN 'покупка' THEN -1
                    WHEN 'продажа' THEN 1
                END
				AS VSUMM,
                b.ACCOUNTNUM 	AS ACCOUNT,
                b.CURR_ID 		AS CURR_ID,
                {(int)SaldoParts.OrdReport} AS ORD,
                1 AS SUB_Q,
                ''{(IsDB2 ? "" : "::TEXT")} AS CONTENT,
                b.TYPE_ID 		AS TYPE_ID,
                a.CBORD_ID 		AS FK
FROM 
   PFR_BASIC.ORD_REPORT a
   LEFT JOIN PFR_BASIC.PF_BACC  bp ON  bp.ID = a.PF_BACC_ID       
   INNER JOIN PFR_BASIC.CBINORDER i ON i.ID=a.CBORD_ID   
   INNER JOIN PFR_BASIC.SECURITY s ON s.ID = i.SEC_ID
   INNER JOIN PFR_BASIC.ORDER o ON o.ID=i.ORD_ID
   INNER JOIN PFR_BASIC.PORTFOLIO p ON p.ID = coalesce(bp.PF_ID, o.PF_ID)
   INNER JOIN PFR_BASIC.PFRBANKACCOUNT b ON b.ID = coalesce(bp.BACC_ID , o.TRD_ID, o.TRANSIT_ID, o.CURRENT_ID, o.DEPO_ID) 
   LEFT JOIN PFR_BASIC.YEARS y ON p.YEAR_ID=y.ID   

WHERE a.UNLINKED = 0 {GetFilterCondition(false, "a.DATEORDER", periodStart, periodEnd)}");
		}

		private void SelectCost(StringBuilder sb, DateTime? periodStart, DateTime? periodEnd)
		{
			//COST
			//!!! -a.SUMPP as SUMM возможны проблемы с рублевыми транзакциями Вознаграждение за сделки по ЦБ
			sb.AppendFormat(
				$@"
   SELECT 
a.ID, 
p.YEAR AS PORTFOLIO, 
y.NAME AS PORTFOLIO_YEAR, 
p.TYPE_ID AS PORTFOLIOTYPE, 
a.DATEPP AS ODATE, 
a.KIND AS DOCKIND, 
-a.SUMPP AS SUMM, 
-a.SUMPP AS VSUMM,
b.ACCOUNTNUM AS ACCOUNT,
b.CURR_ID AS CURR_ID,
{(int)SaldoParts.Cost} AS ORD,
1 AS SUB_Q,
''{(IsDB2 ? "" : "::TEXT")} AS CONTENT,
b.TYPE_ID AS TYPE_ID,
a.ID AS FK
   FROM PFR_BASIC.COST a
   INNER JOIN PFR_BASIC.PORTFOLIO p ON a.PF_ID=p.ID
   INNER JOIN PFR_BASIC.PFRBANKACCOUNT b ON a.ACC_ID = b.ID
   LEFT JOIN PFR_BASIC.YEARS y ON p.YEAR_ID=y.ID {GetFilterCondition(true, "a.DATEPP", periodStart, periodEnd)} ");
		}

		private void SelectIncomeSec(StringBuilder sb, DateTime? periodStart, DateTime? periodEnd)
		{
			//INCOMESEC
			//!!!!! i.ORDER_SUM * COALESCE(c.VALUE, 1) as SUMM возможны проблемы с рублевыми транзакциями Купонный доход
			sb.AppendFormat(
				$@"   
   SELECT i.ID,
            p.YEAR AS PORTFOLIO,
            y.NAME AS PORTFOLIO_YEAR,
            p.TYPE_ID AS PORTFOLIOTYPE,
            i.ORDER_DATE AS ODATE,
            i.INCOME_KIND AS DOCKIND,
            i.ORDER_SUM * COALESCE(c.VALUE, 1) AS SUMM,
            i.ORDER_SUM AS VSUMM,
            b.ACCOUNTNUM AS ACCOUNT,
            b.CURR_ID AS CURR_ID,
            {(int)SaldoParts.IncomeSec} AS ORD,
            1 AS SUB_Q,
            ''{(IsDB2 ? "" : "::TEXT")} AS CONTENT,
            b.TYPE_ID AS TYPE_ID,
            i.ID AS FK
   FROM PFR_BASIC.INCOMESEC i
   INNER JOIN PFR_BASIC.PORTFOLIO p ON i.PF_ID=p.ID
   INNER JOIN PFR_BASIC.PFRBANKACCOUNT b ON i.PFR_ACC_ID=b.ID
   LEFT  JOIN PFR_BASIC.CURS c ON c.ID=i.CURS_ID
   LEFT JOIN PFR_BASIC.YEARS y ON p.YEAR_ID=y.ID {GetFilterCondition(true, "i.ORDER_DATE", periodStart, periodEnd)} ");
		}

		private void SelectAsg_Fin_Tr(StringBuilder sb, DateTime? periodStart, DateTime? periodEnd)
		{
			//ASG_FIN_TR
			sb.AppendFormat(
				$@"
            SELECT a.ID,
            p.YEAR AS PORTFOLIO,
            y.NAME AS PORTFOLIO_YEAR,
            p.TYPE_ID AS PORTFOLIOTYPE,
            a.DRAFT_PAYDATE AS ODATE,
            r.KIND AS DOCKIND,
            a.DRAFT_AMOUNT * CASE
                             WHEN LOWER(r.KIND) < 'передача' THEN 1
                             ELSE -1
                         END AS SUMM,
            a.DRAFT_AMOUNT * CASE
                             WHEN LOWER(r.KIND) < 'передача' THEN 1
                             ELSE -1
                         END AS VSUMM,
            b.ACCOUNTNUM AS ACCOUNT,
            b.CURR_ID AS CURR_ID,
            {(int)SaldoParts.Asg_Fin_Tr} AS ORD,
            1 AS SUB_Q,
            TRIM(e.CONTENT){(IsDB2 ? "" : "::TEXT")} AS CONTENT,
            b.TYPE_ID AS TYPE_ID,
            a.ID AS FK
            FROM PFR_BASIC.ASG_FIN_TR a
            INNER JOIN PFR_BASIC.FINREGISTER f ON a.FR_ID=f.ID  AND a.UNLINKED = 0 --Исключаем перечисления лотуса
            INNER JOIN PFR_BASIC.REGISTER r ON f.REG_ID=r.ID
            LEFT  JOIN PFR_BASIC.ERZL e ON r.ERZL_ID = e.ID
            INNER JOIN PFR_BASIC.PORTFOLIO p ON a.PF_ID=p.ID
            INNER JOIN PFR_BASIC.PFRBANKACCOUNT b ON a.PBA_ID=b.ID
            LEFT JOIN PFR_BASIC.YEARS y ON p.YEAR_ID=y.ID 
	        WHERE 
	        a.STATUS_ID <> -1 {GetFilterCondition(false, "a.DRAFT_PAYDATE", periodStart, periodEnd)}");
			//  a.UNLINKED = 0 --Исключаем перечисления лотуса


			sb.Append("UNION ALL");
			//ASG_FIN_TR
			//ПП, которые пока никуда не привязаны
			sb.AppendFormat(
			   $@"
SELECT a.ID,
            p.YEAR AS PORTFOLIO,
            y.NAME AS PORTFOLIO_YEAR,
            p.TYPE_ID AS PORTFOLIOTYPE,
            a.DRAFT_PAYDATE AS ODATE,
            dir.NAME AS DOCKIND,
            a.DRAFT_AMOUNT * CASE
                             WHEN a.DIRECTION_EL_ID = {(long)AsgFinTr.Directions.ToPFR} THEN 1
                             ELSE -1
                         END AS SUMM,
            a.DRAFT_AMOUNT *  CASE
                             WHEN a.DIRECTION_EL_ID = {(long)AsgFinTr.Directions.ToPFR} THEN 1
                             ELSE -1
                         END AS VSUMM,
            b.ACCOUNTNUM AS ACCOUNT,
            b.CURR_ID AS CURR_ID,
            {(int)SaldoParts.Asg_Fin_Tr} AS ORD,
            1 AS SUB_Q,
            ''{(IsDB2 ? "" : "::TEXT")} AS CONTENT,
            b.TYPE_ID AS TYPE_ID,
            a.ID AS FK
   FROM PFR_BASIC.ASG_FIN_TR a
LEFT JOIN PFR_BASIC.ELEMENT dir ON dir.ID = a.DIRECTION_EL_ID   
   INNER JOIN PFR_BASIC.PORTFOLIO p ON a.PF_ID=p.ID
   INNER JOIN PFR_BASIC.PFRBANKACCOUNT b ON a.PBA_ID=b.ID
   LEFT JOIN PFR_BASIC.YEARS y ON p.YEAR_ID=y.ID 
WHERE
	a.FR_ID is null AND a.REQ_TR_ID is null
	AND a.STATUS_ID <> -1
{GetFilterCondition(false, "a.DRAFT_PAYDATE", periodStart, periodEnd)}");

		}

		private void SelectReq_Transfer(StringBuilder sb, DateTime? periodStart, DateTime? periodEnd)
		{
			//REQ_TRANSFER
			sb.AppendFormat(
				@"
    SELECT pp.ID,
            p.YEAR AS PORTFOLIO,
            y.NAME AS PORTFOLIO_YEAR,
            p.TYPE_ID AS PORTFOLIOTYPE,
            coalesce(pp.DRAFT_PAYDATE,a.ACT_DATE, a.CONTRL_SPN_DATE) AS ODATE,
            CASE r.DIR_SPN_ID
            WHEN 1 THEN 'Возврат из УК в ПФР'
            WHEN 2 THEN 'Передача из ПФР в УК'
            WHEN 3 THEN 'Возврат из ГУК ВР в ПФР'
            WHEN 4 THEN 'Передача из ПФР в ГУК ВР'
        END AS DOCKIND,
            CASE r.DIR_SPN_ID
            WHEN 1 THEN pp.DRAFT_AMOUNT
            WHEN 2 THEN -pp.DRAFT_AMOUNT
            WHEN 3 THEN pp.DRAFT_AMOUNT
            WHEN 4 THEN -pp.DRAFT_AMOUNT
        END AS SUMM,
            CASE r.DIR_SPN_ID
            WHEN 1 THEN pp.DRAFT_AMOUNT
            WHEN 2 THEN -pp.DRAFT_AMOUNT
            WHEN 3 THEN pp.DRAFT_AMOUNT
            WHEN 4 THEN -pp.DRAFT_AMOUNT
        END AS VSUMM,
            b.ACCOUNTNUM AS ACCOUNT,
            b.CURR_ID AS CURR_ID,
            " + (int)SaldoParts.Req_Transfer + @" AS ORD,
            1 AS SUB_Q,
            o.OPERATION AS CONTENT,
            b.TYPE_ID AS TYPE_ID,
            pp.ID AS FK
  
 FROM PFR_BASIC.ASG_FIN_TR pp	
	JOIN	PFR_BASIC.REQ_TRANSFER a ON a.ID = pp.REQ_TR_ID
	INNER JOIN PFR_BASIC.PORTFOLIO p ON p.ID=pp.PF_ID
	INNER JOIN PFR_BASIC.PFRBANKACCOUNT b ON pp.PBA_ID=b.ID
	INNER JOIN PFR_BASIC.SI_TRANSFER t ON t.ID=a.SI_TR_ID AND a.STATUS_ID <> -1 AND a.UNLINKED = 0 --Исключаем перечисления лотуса
	INNER JOIN PFR_BASIC.SI_REGISTER r ON r.ID=t.SI_REG_ID  AND r.STATUS_ID <> -1
	INNER JOIN PFR_BASIC.OPERATION o ON r.OPERATION_ID = o.ID
	LEFT JOIN PFR_BASIC.YEARS y ON p.YEAR_ID=y.ID
WHERE 1=1 AND a.STATUS_ID <> -1 AND pp.STATUS_ID <> -1  {0} ",
				GetFilterCondition(false, "coalesce(pp.DRAFT_PAYDATE,a.ACT_DATE, a.CONTRL_SPN_DATE)", periodStart, periodEnd));
			//a.UNLINKED = 0 --Исключаем перечисления лотуса
		}


		private void SelectTransfer(StringBuilder sb, DateTime? periodStart, DateTime? periodEnd)
		{
			//TRANSFER
			sb.AppendFormat($@"
SELECT 
    a.ID, 
    p.YEAR AS PORTFOLIO, 
    y.NAME AS PORTFOLIO_YEAR, 
    p.TYPE_ID AS PORTFOLIOTYPE, 
    a.DATE AS ODATE, 
    'Перечисление. Расходы на ведение СЧ ИЛС' AS DOCKIND, 
    -a.SUM AS SUMM, 
    -a.SUM AS VSUMM,
    b.ACCOUNTNUM AS ACCOUNT,
    b.CURR_ID AS CURR_ID,
    {(int)SaldoParts.Transfer} AS ORD,
    1 AS SUB_Q,
    ''{(IsDB2 ? "" : "::TEXT")} AS CONTENT,
    b.TYPE_ID AS TYPE_ID,
    a.ID AS FK
   FROM PFR_BASIC.TRANSFER a
   INNER JOIN PFR_BASIC.PFRBANKACCOUNT b ON a.ID_PFR_BA = b.ID
   INNER JOIN PFR_BASIC.PORTFOLIO p ON p.ID=a.PF_ID
   LEFT JOIN PFR_BASIC.YEARS y ON p.YEAR_ID=y.ID {GetFilterCondition(true, "a.DATE", periodStart, periodEnd)} ");
		}

		private void SelectReturn(StringBuilder sb, DateTime? periodStart, DateTime? periodEnd)
		{
			//RETURN
			sb.AppendFormat($@"
    SELECT a.ID,
            p.YEAR AS PORTFOLIO,
            y.NAME AS PORTFOLIO_YEAR,
            p.TYPE_ID AS PORTFOLIOTYPE,
            a.DATE AS ODATE,
            'Возврат. Расходы на ведение СЧ ИЛС' AS DOCKIND,
            a.SUM AS SUMM,
            a.SUM AS VSUMM,
            b.ACCOUNTNUM AS ACCOUNT,
            b.CURR_ID AS CURR_ID,
            {(int)SaldoParts.Return} AS ORD,
            1 AS SUB_Q,
            ''{(IsDB2 ? "" : "::TEXT")} AS CONTENT,
            b.TYPE_ID AS TYPE_ID,
            a.ID AS FK
   FROM PFR_BASIC.RETURN a
   INNER JOIN PFR_BASIC.PFRBANKACCOUNT b ON a.ID_PFR_BA = b.ID
   INNER JOIN PFR_BASIC.PORTFOLIO p ON p.ID=a.PF_ID
   LEFT JOIN PFR_BASIC.YEARS y ON p.YEAR_ID=y.ID {GetFilterCondition(true, "a.DATE", periodStart, periodEnd)} ");
		}

		private void SelectDeposit(StringBuilder sb, DateTime? periodStart, DateTime? periodEnd)
		{
			//Размещение депозита
			sb.AppendFormat(
				$@"   
   SELECT 
d.ID,
p.YEAR AS PORTFOLIO,
y.NAME AS PORTFOLIO_YEAR,
p.TYPE_ID AS PORTFOLIOTYPE,

dc2.SettleDate AS ODATE,
'Размещение депозита' AS DOCKIND,
-s.SUM * 
CASE
    WHEN (b.CURR_ID IS NULL OR b.CURR_ID = 1) THEN 1 ELSE
        COALESCE(
            (SELECT r.ID
            FROM PFR_BASIC.CURS r
            WHERE b.CURR_ID = r.CURR_ID
            AND dc2.SettleDate>=r.DATE
            ORDER BY r.DATE DESC FETCH FIRST 1 ROWS ONLY)
        ,1)
END AS SUMM,

-s.SUM AS VSUMM,

b.ACCOUNTNUM AS ACCOUNT,
b.CURR_ID AS CURR_ID,
{(int)SaldoParts.Deposit} AS ORD,
1 AS SUB_Q,
''{(IsDB2 ? "" : "::TEXT")} AS CONTENT,
b.TYPE_ID AS TYPE_ID,
d.ID AS FK

   FROM PFR_BASIC.DEPOSIT d
   inner join PFR_BASIC.DEPCLAIM_OFFER o ON o.ID = d.DEPCLAIM_OFFER_ID
   INNER JOIN PFR_BASIC.DEPCLAIM2 dc2 ON o.DEPCLAIM_ID=dc2.ID
   INNER JOIN PFR_BASIC.OFFER_PFRBANKACCOUNT_SUM s 
ON  s.OFFER_ID = o.ID
AND s.STATUS>0
   INNER JOIN PFR_BASIC.PORTFOLIO p ON s.PORTFOLIO_ID=p.ID
   LEFT JOIN PFR_BASIC.YEARS y ON p.YEAR_ID=y.ID
   LEFT JOIN PFR_BASIC.PFRBANKACCOUNT b ON s.PFRBANKACCOUNT_ID=b.ID {GetFilterCondition(true, "dc2.SettleDate", periodStart, periodEnd)} ");

			//Возврат депозита
			sb.Append("UNION ALL");
			sb.AppendFormat($@"
   SELECT 
d.ID,
p.YEAR AS PORTFOLIO,
y.NAME AS PORTFOLIO_YEAR,
p.TYPE_ID AS PORTFOLIOTYPE,

ph.Order_Date AS ODATE,
'Возврат депозита' AS DOCKIND,
ph.SUM * 
CASE
    WHEN (b.CURR_ID IS NULL OR b.CURR_ID = 1) THEN 1 ELSE
        COALESCE(
            (SELECT r.ID
            FROM PFR_BASIC.CURS r
            WHERE b.CURR_ID = r.CURR_ID
            AND dc2.SettleDate>=r.DATE
            ORDER BY r.DATE DESC FETCH FIRST 1 ROWS ONLY)
        ,1)
END AS SUMM,

ph.SUM AS VSUMM,

b.ACCOUNTNUM AS ACCOUNT,
b.CURR_ID AS CURR_ID,
{(int)SaldoParts.Deposit} AS ORD,
2 AS SUB_Q,
''{(IsDB2 ? "" : "::TEXT")} AS CONTENT,
b.TYPE_ID AS TYPE_ID,
d.ID AS FK 

   FROM PFR_BASIC.DEPOSIT d
   inner join PFR_BASIC.DEPCLAIM_OFFER o ON o.ID = d.DEPCLAIM_OFFER_ID
   INNER JOIN PFR_BASIC.DEPCLAIM2 dc2 ON o.DEPCLAIM_ID=dc2.ID
   INNER JOIN PFR_BASIC.OFFER_PFRBANKACCOUNT_SUM s 
ON  s.OFFER_ID = o.ID
AND s.STATUS>0
   INNER JOIN PFR_BASIC.PORTFOLIO p ON s.PORTFOLIO_ID=p.ID
   LEFT JOIN PFR_BASIC.YEARS y ON p.YEAR_ID=y.ID
   LEFT JOIN PFR_BASIC.PFRBANKACCOUNT b ON s.PFRBANKACCOUNT_ID=b.ID
   INNER JOIN PFR_BASIC.PAYMENT_HISTORY ph ON ph.OFFER_PFRBANKACCOUNT_SUM_ID = s.ID
   where
   ph.FOR_PERCENT = 0 {GetFilterCondition(false, "ph.Order_Date", periodStart, periodEnd)} ");


			//Проценты по депозитам
			sb.Append("UNION ALL");

			sb.AppendFormat($@"
   SELECT 
ph.ID,
p.YEAR AS PORTFOLIO,
y.NAME AS PORTFOLIO_YEAR,
p.TYPE_ID AS PORTFOLIOTYPE,

ph.Order_Date AS ODATE,
'Проценты по депозитам' AS DOCKIND,
ph.SUM * 
CASE
    WHEN (b.CURR_ID IS NULL OR b.CURR_ID = 1) THEN 1 ELSE
        COALESCE(
            (SELECT r.ID
            FROM PFR_BASIC.CURS r
            WHERE b.CURR_ID = r.CURR_ID
            AND dc2.SettleDate>=r.DATE
            ORDER BY r.DATE DESC FETCH FIRST 1 ROWS ONLY)
        ,1)
END AS SUMM,

ph.SUM AS VSUMM,

b.ACCOUNTNUM AS ACCOUNT,
b.CURR_ID AS CURR_ID,
{(int)SaldoParts.Deposit} AS ORD,
3 AS SUB_Q,
''{(IsDB2 ? "" : "::TEXT")} AS CONTENT,
b.TYPE_ID AS TYPE_ID,
0 AS FK 


   FROM PFR_BASIC.DEPOSIT d
   inner join PFR_BASIC.DEPCLAIM_OFFER o ON o.ID = d.DEPCLAIM_OFFER_ID
   INNER JOIN PFR_BASIC.DEPCLAIM2 dc2 ON o.DEPCLAIM_ID=dc2.ID
   INNER JOIN PFR_BASIC.OFFER_PFRBANKACCOUNT_SUM s 
ON  s.OFFER_ID = o.ID
AND s.STATUS>0
   INNER JOIN PFR_BASIC.PORTFOLIO p ON s.PORTFOLIO_ID=p.ID
   LEFT JOIN PFR_BASIC.YEARS y ON p.YEAR_ID=y.ID
   LEFT JOIN PFR_BASIC.PFRBANKACCOUNT b ON s.PFRBANKACCOUNT_ID=b.ID
   INNER JOIN PFR_BASIC.PAYMENT_HISTORY ph ON ph.OFFER_PFRBANKACCOUNT_SUM_ID = s.ID
   where
   ph.FOR_PERCENT = 1 {GetFilterCondition(false, "ph.Order_Date", periodStart, periodEnd)} ");
			//-1 AS FK -- для отличия от записей из Incomesec

		}


		private void SelectBalanceExtra(StringBuilder sb, DateTime? periodStart, DateTime? periodEnd)
		{
			//Размещение депозита
			sb.AppendFormat(
				$@"   
   SELECT 
t.ID,
p.YEAR AS PORTFOLIO,
y.NAME AS PORTFOLIO_YEAR,
p.TYPE_ID AS PORTFOLIOTYPE,

t.OPERATION_DATE as ODATE,
t.OPERATION_NAME AS DOCKIND,
-t.AMOUNT AS SUMM,
-t.VAMOUNT AS VSUMM,

b.ACCOUNTNUM AS ACCOUNT,
b.CURR_ID AS CURR_ID,
{(int)SaldoParts.BalanceExtra} AS ORD,
1 AS SUB_Q,
''{(IsDB2 ? "" : "::TEXT")} AS CONTENT,
b.TYPE_ID AS TYPE_ID,
-2 AS FK -- для отличия записей 

from PFR_BASIC.BALANCE_EXTRA t
INNER JOIN PFR_BASIC.PORTFOLIO p ON t.PORTFOLIO_ID=p.ID
LEFT JOIN PFR_BASIC.YEARS y ON p.YEAR_ID=y.ID
LEFT JOIN PFR_BASIC.PFRBANKACCOUNT b ON t.PFRBANKACCOUNT_ID=b.ID
where t.OPERATION_NAME='Размещение депозита' {GetFilterCondition(false, "t.OPERATION_DATE", periodStart, periodEnd)} ");

			//Временное размещение - ЦБ
			sb.Append("UNION ALL");
			sb.AppendFormat($@"
  
   SELECT 
t.ID,
p.YEAR AS PORTFOLIO,
y.NAME AS PORTFOLIO_YEAR,
p.TYPE_ID AS PORTFOLIOTYPE,

t.OPERATION_DATE as ODATE,
'Временное размещение - покупка ЦБ' AS DOCKIND,
-t.AMOUNT AS SUMM,
CASE b.CURR_ID
WHEN 1 THEN  t.AMOUNT
WHEN t.CURRENCY_ID THEN t.VAMOUNT
ELSE t.VAMOUNT
END * -1 AS VSUMM,

b.ACCOUNTNUM AS ACCOUNT,
b.CURR_ID AS CURR_ID,
{(int)SaldoParts.BalanceExtra} AS ORD,
2 AS SUB_Q,
''{(IsDB2 ? "" : "::TEXT")} AS CONTENT,
b.TYPE_ID AS TYPE_ID,
-5 AS FK -- для отличия записей 

from PFR_BASIC.BALANCE_EXTRA t
INNER JOIN PFR_BASIC.PORTFOLIO p ON t.PORTFOLIO_ID=p.ID
LEFT JOIN PFR_BASIC.YEARS y ON p.YEAR_ID=y.ID
LEFT JOIN PFR_BASIC.PFRBANKACCOUNT b ON t.PFRBANKACCOUNT_ID=b.ID
where t.OPERATION_NAME='Покупка ценных бумаг' {GetFilterCondition(false, "t.OPERATION_DATE", periodStart, periodEnd)} 
");

			//Возврат депозита, Реализация активов, Купонный доход
			sb.Append("UNION ALL");
			sb.AppendFormat(
				$@"
   SELECT 
t.ID,
p.YEAR AS PORTFOLIO,
y.NAME AS PORTFOLIO_YEAR,
p.TYPE_ID AS PORTFOLIOTYPE,

t.OPERATION_DATE as ODATE,
t.OPERATION_NAME AS DOCKIND,
t.AMOUNT AS SUMM,
CASE b.CURR_ID
    WHEN 1 THEN  t.AMOUNT
    WHEN t.CURRENCY_ID THEN t.VAMOUNT
    ELSE t.VAMOUNT
END AS VSUMM,

b.ACCOUNTNUM AS ACCOUNT,
b.CURR_ID AS CURR_ID,
{(int)SaldoParts.BalanceExtra} AS ORD,
3 AS SUB_Q,
''{(IsDB2 ? "" : "::TEXT")} AS CONTENT,
b.TYPE_ID AS TYPE_ID,
-6 AS FK -- для отличия записей

FROM 
    PFR_BASIC.BALANCE_EXTRA t
    INNER JOIN PFR_BASIC.PORTFOLIO p ON t.PORTFOLIO_ID=p.ID
    LEFT JOIN PFR_BASIC.YEARS y ON p.YEAR_ID=y.ID
    LEFT JOIN PFR_BASIC.PFRBANKACCOUNT b ON t.PFRBANKACCOUNT_ID=b.ID
WHERE 
    t.OPERATION_NAME IN ('возврат средств по депозитам', 'реализация активов','Купонный доход') {GetFilterCondition(false, "t.OPERATION_DATE", periodStart, periodEnd)} ");

		}

		#endregion Part queries

		private string GetFilterCondition(bool isWhere, string columnName, DateTime? periodStart, DateTime? periodEnd)
		{
			if (periodStart == null || periodEnd == null)
			{
				return string.Empty;
			}

			if (isWhere)
			{
				return string.Format(
					" WHERE ( {0} is not null AND {0}  BETWEEN '{1:yyyy-MM-dd}' AND '{2:yyyy-MM-dd}') ",
					columnName, periodStart.Value, periodEnd.Value);
			}
		    return string.Format(
		        " AND ( {0} is not null AND {0}  BETWEEN '{1:yyyy-MM-dd}' AND '{2:yyyy-MM-dd}') ",
		        columnName, periodStart.Value, periodEnd.Value);
		}


		public bool IsUseAccountInBalanceList(long accountID)
		{

			var queries = new List<string>
			{
			    $@"select count(a.id) from PFR_BASIC.ACCOPERATION a where  a.PFR_BA_ID = {accountID} or a.PFR_BA_S_ID = {accountID};",
			    $@"select count(a.id) from PFR_BASIC.APS a where  a.PBA_ID = {accountID} or a.SPBA_ID = {accountID};",
			    $@"select count(a.id) from PFR_BASIC.DOPAPS a where  a.PBA_ID = {accountID} or a.SPBA_ID = {accountID};",
			    $@"select count(a.id) from PFR_BASIC.ADDSPN a where  a.PBA_ID = {accountID} ;",
			    $@"select count(a.id) from PFR_BASIC.DOPSPN_M  a where a.QUARTER_PBA_ID = {accountID} ;",
			    $@"select count(a.id) from  PFR_BASIC.ORD_REPORT a
                    LEFT JOIN PFR_BASIC.PF_BACC  bp ON  bp.ID = a.PF_BACC_ID       
                    INNER JOIN PFR_BASIC.CBINORDER i ON i.ID=a.CBORD_ID
                    INNER JOIN PFR_BASIC.ORDER o ON o.ID=i.ORD_ID
                    INNER JOIN PFR_BASIC.PORTFOLIO p ON p.ID = coalesce(bp.PF_ID, o.PF_ID)
                    where coalesce(bp.BACC_ID , o.TRD_ID)  = {accountID} ;",
			    $@"select count(a.id)   FROM PFR_BASIC.COST a
                    INNER JOIN PFR_BASIC.PORTFOLIO p ON a.PF_ID=p.ID
                    where a.ACC_ID = {accountID};",
			    $@"select count(i.id) FROM PFR_BASIC.INCOMESEC i where i.PFR_ACC_ID={accountID};",
			    $@"select count(a.id) FROM PFR_BASIC.ASG_FIN_TR a  where a.PBA_ID={accountID};",
			    $@"select count(a.id) FROM PFR_BASIC.REQ_TRANSFER a 
                    where  a.STATUS_ID <> -1 AND a.UNLINKED = 0 AND a.PF_BA_ID={accountID};",
			    $@"select count(a.id) FROM PFR_BASIC.TRANSFER a  where  a.ID_PFR_BA={accountID};",
			    $@"select count(a.id)    FROM PFR_BASIC.RETURN a   where  a.ID_PFR_BA={accountID};",
			    $@"select count(d.id)    FROM PFR_BASIC.DEPOSIT d
                     inner join PFR_BASIC.DEPCLAIM_OFFER o ON o.ID = d.DEPCLAIM_OFFER_ID
                     INNER JOIN PFR_BASIC.DEPCLAIM2 dc2 ON o.DEPCLAIM_ID=dc2.ID
                     INNER JOIN PFR_BASIC.OFFER_PFRBANKACCOUNT_SUM s  ON  s.OFFER_ID = o.ID AND s.STATUS>0
                     where  s.PFRBANKACCOUNT_ID={accountID};",
			    $@"select count(t.id)   from PFR_BASIC.BALANCE_EXTRA t where  t.PFRBANKACCOUNT_ID={accountID};"
			};

			foreach (var query in queries)
			{
				using (var cmd = new DBCommandLogging(query))
				{
					cmd.Prepare();
					var r = cmd.ExecuteScalar();
					if (Convert.ToInt64(r) > 0)
					{
						return true;
					}
				}
			}
			return false;
		}
	}
}
