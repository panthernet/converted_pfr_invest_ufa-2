﻿using System;
using db2connector;
using PFR_INVEST.Proxy.Tools;

namespace PFR_INVEST.Proxy.Integration.kip
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "RegisterService" in code, svc and config file together.
	public class RegisterService : Iservice
	{
		internal enum ErrorCodes
		{
			/// <summary>
			/// "Пройден контроль"
			/// </summary>
			NoError = 0,
			/// <summary>
			/// "Отклонен на ФЛК"
			/// </summary>
			FLKError = 1,
			/// <summary>
			/// "Невозможно прочитать файл"
			/// </summary>
			ReadError = 2
		}

		private static string LoadDocument(byte[] file)
		{
			var res = IISService.LocalInstance.LoadKIPZipFile(file);
			return SetError((ErrorCodes)res.ErrorCode);
		}

		private static string SetError(ErrorCodes code)
		{
			switch (code)
			{
				case ErrorCodes.NoError: return "Пройден контроль"; // 0
				case ErrorCodes.FLKError: return "Отклонен на ФЛК"; // 1
				case ErrorCodes.ReadError: return "Невозможно прочитать файл"; // 2
				default: return null;
			}
		}

		public RegisterResponse1 Register(RegisterRequest1 request)
		{
			try
			{
				var res = LoadDocument(request.RegisterRequest.inputData);

				return new RegisterResponse1 { RegisterResponse = new RegisterResponse { outputData = res } };
			}
			catch(Exception ex)
			{
				//При нештатных ситуациях возвращаем ошибку чтения
                LogManager.Log2KIP($"Нештатная ситуация при обработке сообщения от КИП! {ex}");
				return new RegisterResponse1 { RegisterResponse = new RegisterResponse { outputData = SetError(ErrorCodes.ReadError) } };
			}
		}
	}
}
