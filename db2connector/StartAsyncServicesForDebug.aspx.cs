﻿using System;
using System.Web.UI;
using db2connector.AsyncServices;

namespace db2connector
{
    public partial class StartAsyncServices : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AsyncServicesSystem.StartAsyncServices();//Запускаем асинхронные обновления
            Response.Write("Асинхронные обновления запущены");
        }
    }
}