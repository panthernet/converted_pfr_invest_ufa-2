﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.Xml;
using PFR_INVEST.DataAccess.Server;

namespace PFR_INVEST.Proxy
{
    public class HibDataBehaviorAttribute : Attribute, 
       IServiceBehavior

    {
        void IServiceBehavior.AddBindingParameters(
              ServiceDescription serviceDescription,
              ServiceHostBase serviceHostBase,
              Collection<ServiceEndpoint> endpoints,
              BindingParameterCollection bindingParameters)
        {
            ReplaceDataContractSerializerOperationBehavior(serviceDescription);
        }

        void IServiceBehavior.ApplyDispatchBehavior(
                ServiceDescription serviceDescription,
                ServiceHostBase serviceHostBase)
        {
            ReplaceDataContractSerializerOperationBehavior(serviceDescription);
        }

        void IServiceBehavior.Validate(ServiceDescription serviceDescription,
                ServiceHostBase serviceHostBase)
        { }






        private static void ReplaceDataContractSerializerOperationBehavior(
            ServiceDescription description)
        {
            foreach (var endpoint in description.Endpoints)
            {
                ReplaceDataContractSerializerOperationBehavior(endpoint);
            }
        }

        private static void ReplaceDataContractSerializerOperationBehavior(
                ContractDescription description)
        {
            foreach (var operation in description.Operations)
            {
                ReplaceDataContractSerializerOperationBehavior(operation);
            }
        }

        private static void ReplaceDataContractSerializerOperationBehavior(
                ServiceEndpoint endpoint)
        {
            // игнорируем mex-интерфейс
            if (endpoint.Contract.ContractType == typeof(IMetadataExchange))
            {
                return;
            }
            ReplaceDataContractSerializerOperationBehavior(endpoint.Contract);
        }

        private static void ReplaceDataContractSerializerOperationBehavior(
                OperationDescription description)
        {
            var behavior =
             description.Behaviors.Find<DataContractSerializerOperationBehavior>();
            if (behavior != null)
            {
                description.Behaviors.Remove(behavior);
                description.Behaviors.Add(
                    
                    new HibDataContractSerializerOperationBehavior(description) 
                    );
            }

        }


        public class HibDataContractSerializerOperationBehavior
            : DataContractSerializerOperationBehavior
        {
            public HibDataContractSerializerOperationBehavior(
                    OperationDescription description)
                : base(description) { }

            public override XmlObjectSerializer CreateSerializer(Type type,
                    string name, string ns, IList<Type> knownTypes)
            {
                FillAdditionalKnownTypes(knownTypes);
                return new DataContractSerializer(type, name, ns, knownTypes, int.MaxValue, false, false, new HibDataContractSerializerSurrogate());
            }

            public override XmlObjectSerializer CreateSerializer(Type type,
                    XmlDictionaryString name, XmlDictionaryString ns,
                    IList<Type> knownTypes)
            {
                FillAdditionalKnownTypes(knownTypes);
                return new DataContractSerializer(type, name, ns, knownTypes, int.MaxValue,  false, false, new HibDataContractSerializerSurrogate());
            }

            public void FillAdditionalKnownTypes(IList<Type> p_types)
            {
                Type[] aAdditionalTypes = Assembly.GetAssembly(typeof(DataAccessSystem)).GetTypes();
                foreach (Type t in aAdditionalTypes)
                {
                    if(!p_types.Contains(t))
                        p_types.Add(t);
                }
            }
        }


        public class HibDataContractSerializerSurrogate : IDataContractSurrogate
        {
            public Type GetDataContractType(Type type)
            {
                //if (type.GetInterface("IDataObjectLite") != null) return type;

                Type t = HibObjectsToDtoConverter.GetDataContractType(type);
                return type;
            }

            public object GetObjectToSerialize(object obj, Type targetType)
            {
                object r = HibObjectsToDtoConverter.GetDtoObject(obj, targetType);
                return r;
            }

            public object GetDeserializedObject(object obj, Type targetType)
            {

                return obj;
            }
            public Type GetReferencedTypeOnImport(string typeName, string typeNamespace, object customData)
            {

                return null;
            }

            public CodeTypeDeclaration ProcessImportedType(CodeTypeDeclaration typeDeclaration, CodeCompileUnit compileUnit)
            {
                return typeDeclaration;
            }


            public object GetCustomDataToExport(Type clrType, Type dataContractType)
            {
                return null;
            }

            public object GetCustomDataToExport(MemberInfo memberInfo, Type dataContractType)
            {
                return null;
            }

            public void GetKnownCustomDataTypes(Collection<Type> customDataTypes)
            {
            }


        
        
        }



    }
}
