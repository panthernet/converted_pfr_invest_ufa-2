﻿using System;
using System.Linq;
using PFR_INVEST.Auth.AD;
using PFR_INVEST.Auth.ECASA;
using PFR_INVEST.Auth.Mock;
using PFR_INVEST.Auth.ServerData;
using PFR_INVEST.Auth.SharedData;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Common.Logger;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.Proxy.Tools;

namespace db2connector
{
    /// <summary>
    /// Часть сервиса - реализация работы с авторизацией и управлением доступа
    /// </summary>
    public partial class IISServiceHib
    {
        private static readonly ServiceAuthType AuthType;
        private IAuthConnector _authConnector;

        public ServiceAuthMsg InitService(object[] prms)
        {
            try
            {
                switch (AuthType)
                {
                    case ServiceAuthType.AD:
                        LogManager.LogAuth("InitService AD");
                        _authConnector = new AuthConnectorAD((string)prms[0], (string)prms[1]);
                        var result = _authConnector.Init();
                        switch (result.Response)
                        {
                            case AuthMsg.UserMustChangePassword:
                                SaveJornalLogWithAdditionalInfo(JournalEventType.AUTHORIZATION, "Авторизация в системе", null, string.Empty, "НЕ УСПЕШНО (NaN)");
                                LogManager.LogAuth($"{result.ResultValue}");
                                // syslog
                                LogManager.LogLeefSysLog((IISService)this, (string)prms[0], "Авторизация в системе", "Требуется изменить пароль", $"{result.ResultValue}");

                                return new ServiceAuthMsg(AuthMsg.UserMustChangePassword, result.ResultValue);
                            case AuthMsg.Success:
                                return new ServiceAuthMsg(AuthMsg.Success);
                            default:
                                SaveJornalLogWithAdditionalInfo(JournalEventType.AUTHORIZATION, "Авторизация в системе", null, string.Empty, "НЕ УСПЕШНО (NaN)");
                                LogManager.LogAuth((string)result.ResultValue);
                                // syslog
                                LogManager.LogLeefSysLog((IISService)this, (string)prms[0], "Авторизация в системе", "НЕ УСПЕШНО", $"{result.ResultValue}");

                                return new ServiceAuthMsg(AuthMsg.Denied, result.ResultValue);
                        }
                    case ServiceAuthType.ECASA:
                    case ServiceAuthType.QUICKECASA:
                        LogManager.LogAuth($@"InitService ECASA ({(AuthType == ServiceAuthType.ECASA ? "Normal" : "Quick")})");
                        _authConnector = new AuthConnectorECASA((string)prms[0], (string)prms[2], AuthType == ServiceAuthType.ECASA);
                        var result2 = _authConnector.Init();
                        switch (result2.Response)
                        {
                            case AuthMsg.UserMustChangePassword:
                                // syslog
                                LogManager.LogLeefSysLog((IISService)this, (string)prms[0], "Авторизация в системе", "Требуется изменить пароль", $"{result2.ResultValue}");

                                return new ServiceAuthMsg(AuthMsg.NotSupported);
                            case AuthMsg.Success:
                                return new ServiceAuthMsg(AuthMsg.Success);
                            default:
                                SaveJornalLogWithAdditionalInfo(JournalEventType.AUTHORIZATION, "Авторизация в системе", null, string.Empty, "НЕ УСПЕШНО (NaN)");
                                LogManager.LogAuth((string)result2.ResultValue);
                                // syslog
                                LogManager.LogLeefSysLog((IISService)this, (string)prms[0], "Авторизация в системе", "НЕ УСПЕШНО", $"{result2.ResultValue}");

                                return new ServiceAuthMsg(AuthMsg.Denied, result2.ResultValue);
                        }
                    case ServiceAuthType.MOCK:
                        LogManager.LogAuth("InitService MOCK");
                        _authConnector = new AuthConnectorMock((string)prms[0]);
                        _authConnector.Init();

                        return new ServiceAuthMsg(AuthMsg.Success);
                    default:
                        throw new Exception("Unknown AuthType!");
                }
            }
            catch (Exception e)
            {
                // syslog
                LogManager.LogLeefSysLog((IISService)this, (string)prms[0], "Авторизация в системе", "НЕ УСПЕШНО", $"{e.Message}");
                throw;
            }
        }


        public ServiceAuthMsg ChangeCurrentUserPassword(string userName, string oldPassword, string password)
        {
            var result = _authConnector.ChangeCurrentUserPassword(userName, oldPassword, password);
            switch (result.Response)
            {
                case AuthMsg.Success:
                    SaveJornalLogWithAdditionalInfo(JournalEventType.USERS_CHANGES, "Смена пароля пользователем", null, string.Empty, "УСПЕШНО");
                    return new ServiceAuthMsg(AuthMsg.Success);
                default:
                    SaveJornalLogWithAdditionalInfo(JournalEventType.USERS_CHANGES, "Смена пароля пользователем", null, string.Empty, "ОШИБКА");
                    
                    // syslog
                    LogManager.LogLeefSysLog((IISService)this, userName, "Смена пароля пользователем", "НЕ УСПЕШНО", (string)result.ResultValue);

                    return new ServiceAuthMsg(result.Response);
            }
        }

        public object GetToken()
        {
            return _authConnector == null ? "BAD_TOKEN" : _authConnector.GetToken();
        }


        public AuthResponse GetGroupsChildUser(AuthRequest request)
        {
            var response = new AuthResponse();

            if (!IsValidRequest(request, response))
                return response;

            try
            {
                response.Result = _authConnector.GetGroupChildUsers((string)request.Value1); //имя группы
            }
            catch (Exception e)
            {
                response.Acknowledge = AcknowledgeType.Failure;
                response.Message = e.Message;
            }
            return response;
        }

        /// <summary>
        /// Проверяет безопасность запроса
        /// </summary>
        /// <param name="request">Запрос клиента.</param>
        /// <param name="response">Ответ.</param>
        private bool IsValidRequest(AuthRequest request, AuthResponse response)
        {
            if (string.IsNullOrEmpty(request.AccessToken))
            {
                response.Acknowledge = AcknowledgeType.Failure;
                response.Message =
                    "Точка доступа не задана. Воспользуйтесь методом GetToken() для получения новой точки доступа.";
                LogManager.LogAuth("Validation: null access token", LogSeverity.Debug);
                return false;
            }

            if (_authConnector == null || !_authConnector.IsValidRequest(request.AccessToken))
            {
                LogManager.LogAuth(_authConnector == null ? "Validation: null connector" : "Validation: token not valid", LogSeverity.Debug);
                response.Acknowledge = AcknowledgeType.Failure;
                response.Message =
                    "Некорретная или истекшая точка доступа. Воспользуйтесь методом GetToken() для получения новой точки доступа.";
                return false;
            }
            return true;
        }

        public AuthResponse GetRoles(AuthRequest request)
        {
            LogManager.LogAuth($"GetRoles start. Token: {request.AccessToken} User:{request.Value1}");
            var response = new AuthResponse();
            if (!IsValidRequest(request, response))
            {
                LogManager.LogAuth("GetRoles not valid");
                return response;
            }
            LogManager.LogAuth("GetRoles valid");

            try
            {
                response.Result = _authConnector.GetRoles((string)request.Value1);
                response.Acknowledge = AcknowledgeType.Success;
                LogManager.LogAuth("GetRoles success");
            }
            catch (Exception ex)
            {
                LogManager.LogAuthEx(ex, "GetRoles ex");

                response.Acknowledge = AcknowledgeType.Failure;
                response.Message = ex.Message;
            }
            return response;
        }

        public AuthResponse AddUserToGroup(AuthRequest request)
        {
            if (!_authConnector.IsAdmin())
                throw new Exception("Данная операция доступна только администраторам ПТК ДОКИП");

            var response = new AuthResponse();
            if (!IsValidRequest(request, response))
                return response;

            try
            {
                response.Result = _authConnector.AddUserToGroup(request.Value1.ToString(), request.Value2.ToString());
            }
            catch (Exception e)
            {
                response.Acknowledge = AcknowledgeType.Failure;
                response.Message = e.Message;
            }
            return response;
        }

        public AuthResponse RemoveUserFromGroup(AuthRequest request)
        {
            if (!_authConnector.IsAdmin())
                throw new Exception("Данная операция доступна только администраторам ПТК ДОКИП");

            var response = new AuthResponse();
            if (!IsValidRequest(request, response))
                return response;

            try
            {
                response.Result = _authConnector.RemoveUserFromGroup(request.Value1.ToString(), request.Value2.ToString());
            }
            catch (Exception e)
            {
                response.Acknowledge = AcknowledgeType.Failure;
                response.Message = e.Message;
            }
            return response;
        }

        public AuthResponse GetUsersNotInGroup(AuthRequest request)
        {
            var response = new AuthResponse();
            if (!IsValidRequest(request, response))
                return response;
            try
            {
                response.Result = _authConnector.GetUsersNotInGroup(request.Value1.ToString());
            }
            catch (Exception e)
            {
                response.Acknowledge = AcknowledgeType.Failure;
                response.Message = e.Message;
            }
            return response;
        }

        public User IsUserExist(string dn)
        {
            return _authConnector.IsUserExist(dn);
        }

        public bool IsUserInRole(string userName, string role)
        {
            return _authConnector.IsUserInRole(userName, role);
        }

        public bool IsUserInAtLeastOneRole(string userName, string[] roles)
        {
            return roles.Aggregate(false, (current, role) => current || _authConnector.IsUserInRole(userName, role));
        }

        public AuthResponse GetUserInfo(AuthRequest request)
        {
            var response = new AuthResponse();
            if (!IsValidRequest(request, response))
                return response;

            try
            {
                response.Result = _authConnector.GetUserInfo(request.Value1.ToString());
            }
            catch (Exception e)
            {
                response.Acknowledge = AcknowledgeType.Failure;
                response.Message = e.Message;
            }
            return response;
        }

        #region ЭТИ МЕТОДЫ ПОХОЖЕ НЕ НУЖНЫ
        /* public AuthResponse AddUser(AuthRequest request)
         {
             if (!_authConnector.IsAdmin())
                 throw new Exception("Данная операция доступна только администраторам ПТК ДОКИП");

             var response = new AuthResponse();
             if (!IsValidRequest(request, response))
                 return FileWebResponsee;

             try
             {
                 _activeDirectory.AddUser(userRequest.User);
                 response.Result = _activeDirectory.GetUserInfo(userRequest.User.SamName);
             }
             catch (Exception e)
             {
                 response.Acknowledge = AcknowledgeType.Failure;
                 response.Message = e.Message;
             }
             return response;
         }

         public UserResponse RemoveUser(UserRequest userRequest)
         {
             if (!m_UserIsPtkDokipAdmin)
                 throw new Exception("Данная операция доступна только администраторам ПТК ДОКИП");

             var respone = new UserResponse
             {
                 CorrelationId = userRequest.RequestId
             };
             if (!ValidRequest(userRequest, respone, Validate.ClientTag | Validate.AccessToken))
                 return respone;

             try
             {
                 _activeDirectory.RemoveUser(userRequest.User);
             }
             catch (Exception e)
             {
                 respone.Acknowledge = AcknowledgeType.Failure;
                 respone.Message = e.Message;
             }
             return respone;
         }*/

        /* public UsersResponse GetUsers(UsersRequest usersRequest)
         {
             var lastCulture = System.Threading.Thread.CurrentThread.CurrentCulture;

             try
             {
                 var respone = new UsersResponse
                 {
                     CorrelationId = usersRequest.RequestId
                 };
                 if (!ValidRequest(usersRequest, respone, Validate.ClientTag | Validate.AccessToken))
                     return respone;

                 try
                 {
                     respone.Users = _activeDirectory.GetUsers();
                 }
                 catch (Exception e)
                 {
                     respone.Acknowledge = AcknowledgeType.Failure;
                     respone.Message = e.Message;
                 }

                 return respone;
             }
             finally
             {
                 System.Threading.Thread.CurrentThread.CurrentCulture = lastCulture;
             }
         }*/
        #endregion

        #region OLD

        /*
         public UsersResponse GetGroupsChildUser(GroupUsersRequest groupUsersRequest)
         {
             var respone = new UsersResponse
             {
                 CorrelationId = groupUsersRequest.RequestId
             };
             if (!ValidRequest(groupUsersRequest, respone, Validate.ClientTag | Validate.AccessToken))
                 return respone;

             try
             {
                 respone.Users = _activeDirectory.GetGroupChildUsers(groupUsersRequest.GroupName);
             }
             catch (Exception e)
             {
                 respone.Acknowledge = AcknowledgeType.Failure;
                 respone.Message = e.Message;
             }
             return respone;
         }

         */

        /* public ADValidationResult InitDirectoryService(string userName, string password, string domain)
         {
             var context = new ActiveDirectoryConnectionContext
             {
                 LdapServer = ConfigurationManager.AppSettings[AD_SERVER_NAME_KEY],
                 LdapDistinguishedName = domain,
                 UserName = userName,
                 Password = password
             };
             _activeDirectory = new ActiveDirectoryClass(context);
             var result = _activeDirectory.Validate();
             if (result == ADValidationResult.Success)
                 m_UserIsPtkDokipAdmin = IsUserInRole(userName, domain, PTK_DOKIP_ADMINISTRATORS_GROUP);
             else
             {
                 SaveJornalLogWithAdditionalInfo(JournalEventType.AUTHORIZATION, "Авторизация в системе", null, string.Empty, string.Format("НЕ УСПЕШНО ({0})", result.GetDescription()));
                 LogManager.getSingleton().LogAD(string.Format("Ошибка авторизации в AD! Login: {0} Domain: {1} Error: {2}", userName, domain, result.GetDescription()));
             }
             //LogManager.getSingleton().LogAD(string.Format("USER: {0} domain: {1} res: {2}", userName, domain, m_UserIsPtkDokipAdmin));

             return result;
         }*/

        /*  public ADPasswordSetResult ChangeCurrentUserPassword(string userName, string oldPassword, string password, string domain)
          {
              //создаем логин АД
              var context = new ActiveDirectoryConnectionContext
              {
                  LdapServer = ConfigurationManager.AppSettings[AD_SERVER_NAME_KEY],
                  LdapDistinguishedName = domain,
                  //UserName = "ptkdokiptechuser",
                  //Password = "p@ssw0rd"
                  UserName = userName,
                  Password = oldPassword
              };
              _activeDirectory = new ActiveDirectoryClass(context);

              //пытаемся сменить пароль юзеру
              var result = _activeDirectory.ChangeUserPassword(userName, oldPassword, password); 
              if (result == ADPasswordSetResult.Success)
              {
                  result = ADPasswordSetResult.SuccessNoLogin;
                  SaveJornalLogWithAdditionalInfo(JournalEventType.USERS_CHANGES, "Смена пароля пользователем", null, string.Empty, "УСПЕШНО");
                  //обновляем котекст АД на юзера, которому меняем пароль
                  context.UserName = userName;
                  context.Password = password;
                  //валидируем юзера
                  if (_activeDirectory.Validate() == ADValidationResult.Success)
                  {
                 //     LogManager.getSingleton().LogAD("AD pwd ch: user validated after pwd change");
                      result = ADPasswordSetResult.Success;
                  }
               //   else LogManager.getSingleton().LogAD("AD pwd ch: user validation failed after pwd change!!!");
              }
              else if (result == ADPasswordSetResult.FailPwdPolicy)
              {
                  return result;
              }
              else
              {
                  SaveJornalLogWithAdditionalInfo(JournalEventType.USERS_CHANGES, "Смена пароля пользователем", null, string.Empty, "ОШИБКА");
                //  LogManager.getSingleton().LogAD("AD pwd ch: pwd change FAILED");
              }
              return result;
          }*/

        /*  public string InitDirectoryServiceByCertificate()
          {
              LogManager.getSingleton().LogAD("Started init cert...");
              CheckAuthentication();
              LogManager.getSingleton().LogAD("After CheckAuthentication");
              var context = new ActiveDirectoryConnectionContext
              {
                  LdapServer = ConfigurationManager.AppSettings[AD_SERVER_NAME_KEY],
                  LdapDistinguishedName = ConfigurationManager.AppSettings[AD_SERVER_DOMAINNAME_KEY],
                  UserName = ConfigurationManager.AppSettings[AD_SERVER_USERNAME_KEY],
                  Password = ConfigurationManager.AppSettings[AD_SERVER_USERPASS_KEY]
              };
              LogManager.getSingleton().LogAD("After CreateContext");
              _activeDirectory = new ActiveDirectoryClass(context);
              if (_activeDirectory.Validate() != ADValidationResult.Success)
                  throw new AuthInitException();


  #if DEBUGETOKEN
              string subject = "CN=DOKIP Tech User,CN=Users,DC=centerpfr,DC=ru";
  #else
              string subject = ServiceSecurityContext.Current.PrimaryIdentity.Name.Split(new char[] { ';' })[0];
              LogManager.getSingleton().LogAD(ServiceSecurityContext.Current.PrimaryIdentity.Name);
  #endif
              LogManager.getSingleton().LogAD(CertificateTool.ExtractUserName(subject));
              LogManager.getSingleton().LogAD(CertificateTool.ExtractDomain(subject));
              var login = GetAccountNameByCert(subject);
              if (string.IsNullOrEmpty(login)) throw new NotUserException();
              m_UserIsPtkDokipAdmin = IsUserInRole(login, CertificateTool.ExtractDomain(subject), PTK_DOKIP_ADMINISTRATORS_GROUP);
              return login;
          }
          */
        /*   public string GetAccountNameByCert(string subject)
           {
               var username = CertificateTool.ExtractUserName(subject);
               foreach (var user in _activeDirectory.GetGroupChildUsers("PTK_DOKIP_USERS"))
                   if (user.Name == username) return user.SamName;
               return string.Empty;
           }*/

            /*
        /// <summary>
        /// Возвращает уникальный идентификатор сессии, который действителен в течении всего периода действия сессии.
        /// </summary>
        /// <param name="tokenRequest"></param>
        /// <returns></returns>
         public TokenResponse GetToken(TokenRequest tokenRequest)
         {
             _accessToken = Guid.NewGuid().ToString();
             return new TokenResponse
             {
                 CorrelationId = tokenRequest.RequestId,
                 AccessToken = _accessToken
             };
         }*/

        /*  public GroupsResponse GetGroups(GroupsRequest groupsRequest)
          {
              var respone = new GroupsResponse
              {
                  CorrelationId = groupsRequest.RequestId
              };
              if (!ValidRequest(groupsRequest, respone, Validate.ClientTag | Validate.AccessToken))
                  return respone;

              try
              {
                  respone.Groups = _activeDirectory.GetGroupInfos();
              }
              catch (Exception e)
              {
                  respone.Acknowledge = AcknowledgeType.Failure;
                  respone.Message = e.Message;
              }
              return respone;
          }*/

        /* public GroupsResponse GetUserGroups(UserGroupRequest userGroupsRequest)
         {
             var respone = new GroupsResponse
             {
                 CorrelationId = userGroupsRequest.RequestId
             };
             if (!ValidRequest(userGroupsRequest, respone, Validate.ClientTag | Validate.AccessToken))
                 return respone;

             try
             {
                 respone.Groups = _activeDirectory.GetUserGroups(userGroupsRequest.UserName);
             }
             catch (Exception e)
             {
                 respone.Acknowledge = AcknowledgeType.Failure;
                 respone.Message = e.Message;
             }
             return respone;
         }*/


        /* public UserResponse GetUserInfo(UserRequest userRequest)
         {
             var respone = new UserResponse
             {
                 CorrelationId = userRequest.RequestId
             };
             if (!ValidRequest(userRequest, respone, Validate.ClientTag | Validate.AccessToken))
                 return respone;

             try
             {
                 respone.User = _activeDirectory.GetUserInfo(userRequest.User.SamName);
             }
             catch (Exception e)
             {
                 respone.Acknowledge = AcknowledgeType.Failure;
                 respone.Message = e.Message;
             }
             return respone;
         }*/

        /*public UserResponse AddUser(UserRequest userRequest)
        {
            if (!m_UserIsPtkDokipAdmin)
                throw new Exception("Данная операция доступна только администраторам ПТК ДОКИП");

            var respone = new UserResponse
            {
                CorrelationId = userRequest.RequestId
            };
            if (!ValidRequest(userRequest, respone, Validate.ClientTag | Validate.AccessToken))
                return respone;

            try
            {
                _activeDirectory.AddUser(userRequest.User);
                respone.User = _activeDirectory.GetUserInfo(userRequest.User.SamName);
            }
            catch (Exception e)
            {
                respone.Acknowledge = AcknowledgeType.Failure;
                respone.Message = e.Message;
            }
            return respone;
        }

        public UserResponse RemoveUser(UserRequest userRequest)
        {
            if (!m_UserIsPtkDokipAdmin)
                throw new Exception("Данная операция доступна только администраторам ПТК ДОКИП");

            var respone = new UserResponse
            {
                CorrelationId = userRequest.RequestId
            };
            if (!ValidRequest(userRequest, respone, Validate.ClientTag | Validate.AccessToken))
                return respone;

            try
            {
                _activeDirectory.RemoveUser(userRequest.User);
            }
            catch (Exception e)
            {
                respone.Acknowledge = AcknowledgeType.Failure;
                respone.Message = e.Message;
            }
            return respone;
        }*/

        /*public UserResponse AddUserToGroup(UserGroupRequest userGroupRequest)
        {
            if (!m_UserIsPtkDokipAdmin)
                throw new Exception("Данная операция доступна только администраторам ПТК ДОКИП");

            var respone = new UserResponse
            {
                CorrelationId = userGroupRequest.RequestId
            };
            if (!ValidRequest(userGroupRequest, respone, Validate.ClientTag | Validate.AccessToken))
                return respone;

            try
            {
                _activeDirectory.AddUserToGroup(userGroupRequest.UserName, userGroupRequest.GroupName);
                respone.User = _activeDirectory.GetUserInfo(userGroupRequest.UserName);
            }
            catch (Exception e)
            {
                respone.Acknowledge = AcknowledgeType.Failure;
                respone.Message = e.Message;
            }
            return respone;
        }*/

        /*public UserResponse RemoveUserFromGroup(UserGroupRequest userGroupRequest)
        {
            if (!m_UserIsPtkDokipAdmin)
                throw new Exception("Данная операция доступна только администраторам ПТК ДОКИП");

            var respone = new UserResponse
            {
                CorrelationId = userGroupRequest.RequestId
            };
            if (!ValidRequest(userGroupRequest, respone, Validate.ClientTag | Validate.AccessToken))
                return respone;

            try
            {
                _activeDirectory.RemoveUserFromGroup(userGroupRequest.UserName, userGroupRequest.GroupName);
                respone.User = _activeDirectory.GetUserInfo(userGroupRequest.UserName);
            }
            catch (Exception e)
            {
                respone.Acknowledge = AcknowledgeType.Failure;
                respone.Message = e.Message;
            }
            return respone;
        }*/

        /*public UsersResponse GetUsers(UsersRequest usersRequest)
        {
            var lastCulture = System.Threading.Thread.CurrentThread.CurrentCulture;

            try
            {
                var respone = new UsersResponse
                {
                    CorrelationId = usersRequest.RequestId
                };
                if (!ValidRequest(usersRequest, respone, Validate.ClientTag | Validate.AccessToken))
                    return respone;

                try
                {
                    respone.Users = _activeDirectory.GetUsers();
                }
                catch (Exception e)
                {
                    respone.Acknowledge = AcknowledgeType.Failure;
                    respone.Message = e.Message;
                }

                return respone;
            }
            finally
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = lastCulture;
            }
        }*/

        /* public UsersResponse GetUsersNotInGroup(GroupUsersRequest groupUsersRequest)
         {
             var respone = new UsersResponse
             {
                 CorrelationId = groupUsersRequest.RequestId
             };
             if (!ValidRequest(groupUsersRequest, respone, Validate.ClientTag | Validate.AccessToken))
                 return respone;

             try
             {
                 respone.Users = _activeDirectory.GetUsersNotInGroup(groupUsersRequest.GroupName);
             }
             catch (Exception e)
             {
                 respone.Acknowledge = AcknowledgeType.Failure;
                 respone.Message = e.Message;
             }
             return respone;
         }*/

        /* public ADUser IsUserExist(string dn)
         {
             return _activeDirectory.IsUserExist(dn);
         }

         public bool IsUserInRole(string userName, string domain, string role)
         {
             return _activeDirectory.IsUserInRole(userName, role);
         }*/

        /*public bool IsUserInAtLeastOneRole(string userName, string domain, string[] roles)
        {
            bool result = false;
            foreach (string role in roles)
                result = result || _activeDirectory.IsUserInRole(userName, role);
            return result;
        }*/

        #region [ Вспомогательные методы ]
        /* /// <summary>
         /// Проверяет уровни безопасности запроса.
         /// </summary>
         /// <param name="request">Запрос клиента.</param>
         /// <param name="response">Ответ.</param>
         /// <param name="validate">Тип осуществляемой проверки.</param>
         /// <returns></returns>
         private bool ValidRequest(RequestBase request, ResponseBase response, Validate validate)
         {
             //// Проверка ClientTag
             //if ((Validate.ClientTag & validate) == Validate.ClientTag)
             //{
             //    if (request.ClientTag != "ABC123")
             //    {
             //        response.Acknowledge = AcknowledgeType.Failure;
             //        response.Message = "Неизвестный клиент.";
             //        return false;
             //    }
             //}
             //}

             // Проверка точки доступа
             if ((Validate.AccessToken & validate) == Validate.AccessToken)
             {
                 if (_accessToken == null)
                 {
                     response.Acknowledge = AcknowledgeType.Failure;
                     response.Message =
                         "Некорретная или истекшая точка доступа. Воспользуйтесь методом GetToken() для получения новой точки доступа.";
                     return false;
                 }
             }

             return true;
         }*/
        #endregion

        #endregion
    }

    public enum ServiceDBType
    {
        DB2,
        PostgreSQL
    }
}
