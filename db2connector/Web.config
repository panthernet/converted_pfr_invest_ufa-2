﻿<?xml version="1.0" encoding="utf-8"?> 
<configuration>
  <configSections>
    <section name="log4net" type="log4net.Config.Log4NetConfigurationSectionHandler,log4net"/>
    <sectionGroup name="applicationSettings"
      type="System.Configuration.ApplicationSettingsGroup, System, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089">
      <section name="PFR_INVEST.Auth.AD.Properties.Settings"
        type="System.Configuration.ClientSettingsSection, System, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" requirePermission="false"/>
      <section name="PFR_INVEST.Auth.ECASA.Properties.Settings"
        type="System.Configuration.ClientSettingsSection, System, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" requirePermission="false"/>
    </sectionGroup>
  </configSections>

  <system.diagnostics>
    <sources>
      <source name="System.ServiceModel" switchValue="Information, ActivityTracing" propagateActivity="true">
        <listeners>
          <add name="traceListener" type="System.Diagnostics.XmlWriterTraceListener" initializeData="..\logs\Traces.svclog"/>
        </listeners>
      </source>
    </sources>
  </system.diagnostics>
   
  <system.web>
    <globalization culture="en-US"/>
    <identity impersonate="true"/>
    <compilation debug="true" defaultLanguage="c#">
      <buildProviders>
        <add extension=".rdlc"
          type="Microsoft.Reporting.RdlBuildProvider, Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845DCD8080CC91"/>
      </buildProviders>
      <assemblies>
        <add assembly="Microsoft.ReportViewer.Common, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845DCD8080CC91"/>
        <add assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845DCD8080CC91"/>
      </assemblies>
    </compilation>
    <customErrors mode="Off"/>
        <authorization>
            <allow users="*"/>
        </authorization>
        <httpHandlers>
              <add path="Reserved.ReportViewerWebControl.axd" verb="*"
                    type="Microsoft.Reporting.WebForms.HttpHandler, Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845DCD8080CC91"
                    validate="false"/>
        </httpHandlers>
  </system.web>
  <!-- When deploying the service library project, the content of the config file must be added to the host's 
  app.config file. System.Configuration does not support config files for libraries. -->
  <system.serviceModel>
    <bindings>
      <netTcpBinding>
        <binding name="WSHttpBinding_IDB2Connector_bin" maxBufferPoolSize="2147483647" maxReceivedMessageSize="2147483647" closeTimeout="02:00:00"
          openTimeout="00:01:00" receiveTimeout="00:10:00" sendTimeout="00:01:00">
          <readerQuotas maxDepth="32" maxStringContentLength="2147483647" maxArrayLength="2147483647" maxBytesPerRead="2147483647"
            maxNameTableCharCount="2147483647"/>
          <reliableSession inactivityTimeout="23:59:59" enabled="True" ordered="True"/>
          <security mode="None">
            <message clientCredentialType="Windows"/>
          </security>
        </binding>
      </netTcpBinding>
      <ws2007HttpBinding>
        <binding name="WSHttpBinding_IDB2Connector" maxBufferPoolSize="2147483647" maxReceivedMessageSize="2147483647" closeTimeout="02:00:00"
          openTimeout="00:01:00" receiveTimeout="00:10:00" sendTimeout="00:01:00" messageEncoding="Mtom">
          <readerQuotas maxDepth="32" maxStringContentLength="2147483647" maxArrayLength="2147483647" maxBytesPerRead="2147483647"
            maxNameTableCharCount="2147483647"/>
          <reliableSession inactivityTimeout="23:59:59" enabled="True" ordered="True"/>
          <security mode="None"/>
          <!--
          <security mode="Message">
            <message clientCredentialType="Windows" negotiateServiceCredential="true" />
          </security>
          -->
        </binding>
      </ws2007HttpBinding>

      <customBinding>
        <binding name="serviceSoap12">
          <textMessageEncoding messageVersion="Soap12"/>
          <httpTransport/>
         
        </binding>
          <binding name="WSHttpsBinding_IDB2Connector">
            <reliableSession inactivityTimeout="23:59:59" ordered="True"/>
            <httpsTransport requireClientCertificate="false"/>
          </binding>
        </customBinding>
    
      <basicHttpBinding>
        <binding name="VerifyAuthenticationWSPortBinding1">
            <security mode="Transport"> 
              <transport clientCredentialType="None" proxyCredentialType="None" realm=""/> 
            </security> 
        </binding>
      </basicHttpBinding>
    </bindings>

    <behaviors>
      <serviceBehaviors>
        <behavior name="Behavior_IDB2Connector">
          <serviceMetadata httpGetEnabled="true"/>
          <dataContractSerializer maxItemsInObjectGraph="2147483647"/>
          <serviceDebug includeExceptionDetailInFaults="true"/>
          <serviceThrottling maxConcurrentCalls="100" maxConcurrentSessions="50" maxConcurrentInstances="50"/>
        </behavior>
        <behavior name="PFR_INVEST.Proxy.Integration.kip.RegisterServiceBehavior">
          <serviceMetadata httpGetEnabled="true" externalMetadataLocation="..\RegisterService.wsdl"/>
          <!-- <useRequestHeadersForMetadataAddress/> .NET 4.0!!! -->
          <serviceDebug includeExceptionDetailInFaults="false"/>          
        </behavior>
        
      </serviceBehaviors>
    </behaviors>

    <services>
      <service behaviorConfiguration="Behavior_IDB2Connector" name="db2connector.IISService">
        <endpoint address="" binding="customBinding" bindingConfiguration="WSHttpsBinding_IDB2Connector" contract="db2connector.IDB2Connector">
          <identity>
            <dns value="localhost"/>
          </identity>
        </endpoint>
        <endpoint address="" binding="ws2007HttpBinding" bindingConfiguration="WSHttpBinding_IDB2Connector" contract="db2connector.IDB2Connector">
          <identity>
            <dns value="localhost"/>
          </identity>
        </endpoint>
        <endpoint address="" binding="netTcpBinding" bindingConfiguration="WSHttpBinding_IDB2Connector_bin" name="TCP_EP" contract="db2connector.IDB2Connector">
          <identity>
            <dns value="localhost"/>
          </identity>
        </endpoint>
        <endpoint address="mex" binding="mexHttpBinding" name="MEX_EP" contract="IMetadataExchange"/>
        <host>
          <baseAddresses>
            <add baseAddress="http://localhost:8731/pheasant/IISService/"/>
          </baseAddresses>
        </host>
      </service>
      <service behaviorConfiguration="PFR_INVEST.Proxy.Integration.kip.RegisterServiceBehavior" name="PFR_INVEST.Proxy.Integration.kip.RegisterService">
        <endpoint address="" binding="customBinding" bindingConfiguration="serviceSoap12" name="SOAP_EP" contract="Iservice">
          <identity>
            <dns value="localhost"/>
          </identity>
        </endpoint>
        <endpoint address="mex" binding="mexHttpBinding" name="MEX_EP" contract="IMetadataExchange"/>
      </service>
    </services>

    <client>
      <endpoint address="http://localhost:8732/Design_Time_Addresses/TestKIP.KIPServiceEmulator/KIPSeviceEmulator/" binding="customBinding"
        bindingConfiguration="serviceSoap12" contract="KIPService.service" name="serviceSoap12"/>
      <endpoint address="https://sitepfr.fed.pfr.local:7893/auth-ws/VerifyAuthenticationWSPort" binding="basicHttpBinding"
        bindingConfiguration="VerifyAuthenticationWSPortBinding1" contract="VerifyAuthenticationWSService.VerifyAuthenticationWS"
        name="VerifyAuthenticationWSPort"/>
    </client>
  
    <serviceHostingEnvironment aspNetCompatibilityEnabled="true"/>
  </system.serviceModel>

  <appSettings>
    <add key="RemoveSuccessLoadedODKXMLReports" value="false"/>
    <!--<add key="DBConnectionString" value="DSN=PFR-INVEST;LONGDATACOMPAT=1;" />-->
    <add key="DBConnectionString" value="server=mercury.it.ru;port=9132;database=ptk_dev;user id=postgres;password=postgres;"/>
    
    <!-- Источники (local) PFR-INVEST  PFR-DEV  PFR-DEV3 />-->
    <!--<add key="DBConnectionString" value="DSN=PFR-INVEST;LONGDATACOMPAT=1;" />-->
    <!--<add key="DBConnectionString" value="server=localhost;port=9132;database=ptk_dev;user id=postgres;password=postgres;" />-->
    <!--<add key="DBConnectionString" value="server=mercury.it.ru;port=9132;database=ptk_test;user id=postgres;password=postgres;" />-->
    <add key="PageSize" value="1000"/>
    <add key="log4net.Config" value="log4net.config"/>
    <add key="log4net.Config.Watch" value="True"/>
    <add key="SchedulerTimeInterval" value="60000"/>
    <add key="KIPServiceEnabled" value="False"/>
    <add key="NSIServiceEnabled" value="False"/>
    <add key="VIOServiceEnabled" value="False"/>
    <add key="AuthType" value="MOCK"/> <!-- AuthType = MOCK AD ECASA QUICKECASA -->
    <add key="DBType" value="POSTGRES"/> <!-- DBType = POSTGRES DB2 -->
    <add key="SqlQueriesTraceEnabled" value="False"/>
    <add key="MockDBConnection" value="False"/>
    <add key="MQEnabled" value="false"/>
    <add key="IsOpfrTransfersEnabled" value="false"/>
    <add key="ShowClientNotificationSettings" value="true"/>
    <add key="InfoAlertsEnabled" value="true"/>
    <add key="IsAsyncOpLogEnabled" value="false"/> <!-- Отладка для шедулера сервиса -->
    
    
  </appSettings>

  <applicationSettings>
    <!-- Раздел настроек коннектора АД -->
    <PFR_INVEST.Auth.AD.Properties.Settings>
      <setting name="ADServerAddress" serializeAs="String">
        <value>192.168.2.124</value>
      </setting>
      <setting name="ADServerUserDomain" serializeAs="String">
        <value>PFRSERVER.TEST</value>
      </setting>
    </PFR_INVEST.Auth.AD.Properties.Settings>

    <PFR_INVEST.Auth.ECASA.Properties.Settings>
      <setting name="PDPDecideUrl" serializeAs="String">
        <value>https://sitepfr.fed.pfr.local:7893/eca-ws/pdp/decide?userGuid={userGuid}&amp;action={action}&amp;resourceString={resString}</value>
      </setting>
      <setting name="MAPRequestUrl" serializeAs="String">
        <value>https://sitepfr.fed.pfr.local:7893/eca-ws/map/info?userGuid={userGuid}&amp;appId={appId}</value>
      </setting>
      <setting name="PDPGetObligationsUrl" serializeAs="String">
        <value>https://sitepfr.fed.pfr.local:7893/eca-ws/pdp/getObligations?userGuid={userGuid}</value>
      </setting>
    </PFR_INVEST.Auth.ECASA.Properties.Settings>
  </applicationSettings>
  
  <system.webServer>
    <directoryBrowse enabled="true" showFlags="Date, Time, Size, Extension, LongDate"/>
    <validation validateIntegratedModeConfiguration="false"/>
    <modules runAllManagedModulesForAllRequests="true"/>
    <handlers>
      <add name="ReportViewerWebControlHandler" verb="*" path="Reserved.ReportViewerWebControl.axd" preCondition="integratedMode"
        type="Microsoft.Reporting.WebForms.HttpHandler, Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845DCD8080CC91"/>
    </handlers>
  </system.webServer>
</configuration>
