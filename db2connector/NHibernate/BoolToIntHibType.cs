﻿using System;
using System.Data;
using NHibernate;
using NHibernate.SqlTypes;
using NHibernate.UserTypes;

namespace db2connector.NHibernate
{

	public class BoolToIntHibType : IUserType
	{
		object IUserType.Assemble(object cached, object owner)
		{
			return cached;
		}

		object IUserType.DeepCopy(object value)
		{
			return value;
		}

		object IUserType.Disassemble(object value)
		{
			return value;
		}

		bool IUserType.Equals(object x, object y)
		{
			if (x == null)
			{
				if (y == null)
					return true;
				return false;
			}

			return x.Equals(y);
		}

		int IUserType.GetHashCode(object x)
		{
			if (x == null)
				return 0;
			return x.GetHashCode();
		}

		bool IUserType.IsMutable
		{
			get
			{
				return false;
			}
		}

		// represents conversion on load-from-db operations:   
		object IUserType.NullSafeGet(IDataReader rs, string[] names, object owner)
		{
			var obj = NHibernateUtil.Decimal.NullSafeGet(rs, names[0]);

			if (obj == null || obj == DBNull.Value)
				return null;

			return Convert.ToInt16(obj) == 0 ? false : true;
		}

		// represents conversion on save-to-db operations:   
		void IUserType.NullSafeSet(IDbCommand cmd, object value, int index)
		{
			if (value == null)
			{
				((IDataParameter)cmd.Parameters[index]).Value = DBNull.Value;
			}
			else
			{
				((IDataParameter)cmd.Parameters[index]).Value = (bool)value == false ? 0 : 1;
            }
		}

		object IUserType.Replace(object original, object target, object owner)
		{
			return original;
		}

		Type IUserType.ReturnedType
		{
			get { return typeof(bool); }
		}

		SqlType[] IUserType.SqlTypes
		{
			get { return new[] { SqlTypeFactory.Int16 }; }
		}
	}
}
