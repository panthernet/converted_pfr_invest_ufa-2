﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using NHibernate;
using NHibernate.SqlTypes;
using NHibernate.UserTypes;
using PFR_INVEST.DataObjects;

namespace db2connector.NHibernate
{
	public class DB2Decimal4HibType : DB2DecimalHibType
	{
		public DB2Decimal4HibType() : base(4) { }
	}

	public class DB2DecimalHibType : IUserType, IParameterizedType
	{
		public int Scale;

		public DB2DecimalHibType()
			: this(0)
		{

		}

		public DB2DecimalHibType(int p_iScale)
		{
			Scale = p_iScale;
		}

		object IUserType.Assemble(object cached, object owner)
		{
			return cached;
		}

		object IUserType.DeepCopy(object value)
		{
			return value;
		}

		object IUserType.Disassemble(object value)
		{
			return value;
		}

		bool IUserType.Equals(object x, object y)
		{
			if (x == null)
			{
				if (y == null)
					return true;
				return false;
			}

			return x.Equals(y);
		}

		int IUserType.GetHashCode(object x)
		{
			if (x == null)
				return 0;
			return x.GetHashCode();
		}

		bool IUserType.IsMutable
		{
			get
			{
				return false;
			}
		}

        public static object locker = new object();



		// represents conversion on load-from-db operations:   
		object IUserType.NullSafeGet(IDataReader rs, string[] names, object owner)
		{
			object obj = NHibernateUtil.Decimal.NullSafeGet(rs, names[0]);

			if (obj == null || obj == DBNull.Value)
				return null;

			decimal d = (decimal)obj;

#if DB2_DECIMAL_FIX
            //Log(string.Format("GET {0}: {1} - Scale: 0\n", names[0], d));
			return BaseDataObject.ConvertDecimalToNormal(d, 0); //Отключение сдвига для теста
#else
           // Log(string.Format("GET {0}: {1} - Scale: {2}\n", names[0], d, Scale));
            return BaseDataObject.ConvertDecimalToNormal(d, Scale);
#endif
		}

		// represents conversion on save-to-db operations:   
		void IUserType.NullSafeSet(IDbCommand cmd, object value, int index)
		{
			if (value == null)
			{
				((IDataParameter)cmd.Parameters[index]).Value = DBNull.Value;
			}
			else
			{
				decimal decimalValue = (decimal)value;
				((IDataParameter)cmd.Parameters[index]).Value = decimalValue;
               // Log(string.Format("SET [{0}][{2}]: {1}\n", index, value, ((IDataParameter)cmd.Parameters[index]).ParameterName));
            }
		}

	    private void Log(string message)
	    {
	        lock (locker)
	        {
	            try
	            {
	                const string filename = "C:/Logs/Log.txt";
	                var dir = Path.GetDirectoryName(filename);
	                if (!Directory.Exists(dir))
	                    Directory.CreateDirectory(dir);
	                using (var SW = File.AppendText(filename))
	                {
	                    SW.WriteLine(message);
	                }
	            }
	            catch (UnauthorizedAccessException)
	            {

	            }
	        }
	    }

		object IUserType.Replace(object original, object target, object owner)
		{
			return original;
		}

		Type IUserType.ReturnedType
		{
			get { return typeof(decimal); }
		}

		SqlType[] IUserType.SqlTypes
		{
			get { return new SqlType[] { SqlTypeFactory.Decimal }; }
		}


		void IParameterizedType.SetParameterValues(IDictionary<string, string> parameters)
		{
			if (parameters == null)
				return;

			if (parameters.ContainsKey("scale") == false)
				return;

			int iScale = Convert.ToInt32(parameters["scale"]);
			Scale = iScale;
		}

	}
}
