﻿namespace PFR_INVEST.Integration.KIPClient
{
	public class KIPIdentifiers
	{
		
		public const string RESULT_OK = "без ошибки";
		public const string RESULT_OK_1 = "пройден контроль";
		public const string RESULT_ERROR_AUTH = "ошибка аутентификации";
		public const string RESULT_ERROR_READ = "невозможно прочитать файл";
		public const string RESULT_ERROR_FLK = "ошибка флк";
		public const string RESULT_ERROR_FLK_1 = "отклонен на флк";


		public static bool IsOKResult(string result) 
		{
			switch (result.ToLower()) 
			{
				case RESULT_OK:
				case RESULT_OK_1:
					return true;
				default:
					return false;
			}
		}
	}
}
