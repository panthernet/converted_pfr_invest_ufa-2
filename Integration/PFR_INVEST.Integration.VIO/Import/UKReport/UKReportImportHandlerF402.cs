﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using PFR_INVEST.Common.Logger;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Server;
using PFR_INVEST.DataAccess.Server.UKReport;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Integration.VIO.BL;
using PFR_INVEST.Integration.VIO.MQ;
using PFR_INVEST.XmlSchemas;

namespace PFR_INVEST.Integration.VIO.Import.UKReport
{
    /// <summary>
    /// ВИО
    /// Импорт документа из УК F402
    /// </summary>
    public class UKReportImportHandlerF402 : DocumentParseHandlerBase<ImportEdoOdkF401402>
    {
        private List<F401402UKStatus> f401402UkStatuses;

        public UKReportImportHandlerF402()
        {
        }


        public override ImportEdoOdkF401402 ParseDocument(string documentBody)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(EDO_ODKF402));

            CultureInfo ci = Thread.CurrentThread.CurrentCulture;
            CultureInfo ciUi = Thread.CurrentThread.CurrentUICulture;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("ru-RU");
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("ru-RU");

            XmlReaderSettings settings = new XmlReaderSettings();
            settings.ValidationType = ValidationType.Schema;
            settings.Schemas.Add(DocumentBase.GetShemaWithIncludes("F402.xsd"));
            try
            {
                using (var ms = new MemoryStream())
                {
                    var contentBytes = Encoding.UTF8.GetBytes(documentBody);

                    ms.Write(contentBytes, 0, contentBytes.Length);
                    ms.Position = 0;
                    ms.Seek(0, SeekOrigin.Begin);

                    using (XmlReader reader = XmlReader.Create(ms, settings))
                    {
                        var doc = (EDO_ODKF402)serializer.Deserialize(reader);

                        doc.Details = doc.Details.Where(d => !string.IsNullOrEmpty(d.DogPFRNum)).ToList();

                        if (doc.Details.Count == 0)
                        {
                            throw new ReportImportException(
                                $"Документ с регистрационным номером {doc.RegNumberOut}\nне содержит информации к требованию на оплату.");
                        }

                        //if (Items.Any(i => string.Equals((i as ImportEdoOdkF401402).edoLog.DocRegNumberOut, doc.RegNumberOut, StringComparison.OrdinalIgnoreCase)))
                        //{
                        //    return ActionResult.Error(
                        //        $"Документ с регистрационным номером {doc.RegNumberOut} уже загружен для импорта");
                        //}


                        //if (Items.Any(i => string.Equals((i as ImportEdoOdkF401402).edoLog.DocRegNumberOut, doc.RegNumberOut, StringComparison.OrdinalIgnoreCase)))
                        //{
                        //    return ActionResult.Error(
                        //        $"Документ с регистрационным номером {doc.RegNumberOut} уже загружен для импорта");
                        //}

                        //if (
                        //    Items.Any(
                        //        item =>
                        //        (item as ImportEdoOdkF401402).F401402.FromDate == DateUtil.ParseImportDate(doc.FromDate) &&
                        //        (item as ImportEdoOdkF401402).F401402.ToDate == DateUtil.ParseImportDate(doc.ToDate) &&
                        //        (item as ImportEdoOdkF401402).F401402UK.Any(uk => doc.Details.Select(d => d.DogPFRNum).Contains(uk.ContractNumber)))
                        //    )
                        //{
                        //    return ActionResult.Error(
                        //        $"Данные из файла {fName}\nдля одного или некольких договоров\nс периодом оплаты с {DateUtil.ParseImportDate(doc.FromDate).Value.ToString("dd.MM.yyyy")} по {DateUtil.ParseImportDate(doc.ToDate).Value.ToString("dd.MM.yyyy")}\n уже загружены для импорта");
                        //}

                        using (var session = DataAccessSystem.OpenSession())
                        {
                            f401402UkStatuses =
                                (List<F401402UKStatus>)
                                    session.CreateCriteria(typeof (F401402UKStatus)).List<F401402UKStatus>();

                            var requirements = session.CreateCriteria(typeof (EdoOdkF401402)).List<EdoOdkF401402>();

                            requirements =
                                requirements.Where(
                                    r =>
                                        DateUtil.IsDateEquals(r.FromDate, DateUtil.ParseImportDate(doc.FromDate)) &&
                                        DateUtil.IsDateEquals(r.ToDate, DateUtil.ParseImportDate(doc.ToDate))).ToList();

                            if (requirements?.Any() != true)
                            {
                                throw new ReportImportException(
                                    $"Требования на оплату в период\nс {DateUtil.ParseImportDate(doc.FromDate).Value.ToString("dd.MM.yyyy")} по {DateUtil.ParseImportDate(doc.ToDate).Value.ToString("dd.MM.yyyy")}\nне найдены.");
                            }


                            XDocument xdoc = XDocument.Parse(documentBody);
                            var fName = "Получено от ВИО";
                            xdoc.Root.AddFirst(new XElement("FILENAME", fName));

                            // для 402 формы в таблицу данные не пишутся, проверку можно выполнить на основе данных таблицы EDO_LOG
                            //Requirements.Any(r => string.Equals(r.RegNumberOut, doc.RegNumberOut, StringComparison.OrdinalIgnoreCase)))
                            if (doc.GetEdoLogsByRegNumberOut(session).Any(x => x.DocForm == xdoc.Root.Name.ToString()))
                            {
                                throw new ReportImportException(
                                    $"Документ с регистрационным номером {doc.RegNumberOut} уже был импортирован");
                            }

                            ImportEdoOdkF401402 importEdoOdkF401402 = new ImportEdoOdkF401402();

                            importEdoOdkF401402.xmlBody = xdoc.ToString();
                            importEdoOdkF401402.F401402 = new EdoOdkF401402
                            {
                                ReportDate = DateUtil.ParseImportDate(doc.ReportDate),
                                RegDate = DateTime.Now.Date,
                                AuthorizedPerson = doc.AuthorizedPerson,
                                Executor = doc.Executor,
                                FromDate = DateUtil.ParseImportDate(doc.FromDate),
                                ToDate = DateUtil.ParseImportDate(doc.ToDate),
                                RegNumberOut = doc.RegNumberOut
                            };

                            var rewriteList = new List<string>();

                            foreach (var info in doc.Details)
                            {
                                Contract contract = MQConnector.Instance.Service.GetContractByDogovorNum(info.DogPFRNum);

                                if (contract == null)
                                {
                                    throw new ReportImportException(
                                        $"Договор с регистрационным номером {info.DogPFRNum} не найден");
                                }

                                var reqs =
                                    requirements.Where(
                                        r => r.GetDetailList(session).Any(det => det.ContractId == contract.ID))
                                        .ToList();

                                if (!reqs.Any())
                                {
                                    throw new ReportImportException(
                                        $"Требование на оплату в период\nс {DateUtil.ParseImportDate(doc.FromDate).Value.ToString("dd.MM.yyyy")} по {DateUtil.ParseImportDate(doc.ToDate).Value.ToString("dd.MM.yyyy")}\nдля  договора с регистрационным номером {info.DogPFRNum}\nне найден.");
                                }
                                else if (reqs.Count > 1)
                                {
                                    throw new ReportImportException(
                                        $"Требований на оплату в период\nс {DateUtil.ParseImportDate(doc.FromDate).Value.ToString("dd.MM.yyyy")} по {DateUtil.ParseImportDate(doc.ToDate).Value.ToString("dd.MM.yyyy")}\nдля  договора с регистрационным номером {info.DogPFRNum}\nбольше одного.");
                                }

                                var detail =
                                    reqs.SingleOrDefault()
                                        .GetDetailList(session)
                                        .FirstOrDefault(d => d.ContractId == contract.ID);

                                // определяем по полю статуса, был ли выполнен импорт ранее, если нет - сохраняем состояние
                                if (detail.InitStatusID == null)
                                {
                                    detail.InitControlFactDate = detail.ControlFactDate;
                                    detail.InitStatusID = detail.StatusID;
                                    detail.InitOutgoingDocNumber = detail.OutgoingDocNumber;
                                    detail.InitOutgoingDocDate = detail.OutgoingDocDate;
                                }
                                else
                                {
                                    rewriteList.Add(info.DogPFRNum);
                                }

                                detail.ControlFactDate = DateUtil.ParseImportDate(info.PayDate);
                                if (detail.ControlSetDate.HasValue && detail.ControlFactDate.HasValue)
                                {
                                    if (detail.ControlFactDate.Value <= detail.ControlSetDate.Value)
                                    {
                                        SetStatus(F402Status.PaidInTime, detail);
                                    }
                                    else
                                    {
                                        SetStatus(F402Status.PaidTimeExceeded, detail);
                                    }
                                }
                                detail.OutgoingDocNumber = doc.RegNumberOut;
                                detail.OutgoingDocDate = DateUtil.ParseImportDate(doc.ReportDate);
                                importEdoOdkF401402.F401402UK.Add(detail);
                            }

                            var rd = DateUtil.ParseImportDate(doc.ReportDate);
                            importEdoOdkF401402.edoLog = new EDOLog()
                            {
                                DocTable = xdoc.Root.Name.ToString(),
                                DocForm = xdoc.Root.Name.ToString(),
                                Filename = fName,
                                DocRegNumberOut = doc.RegNumberOut,
                                RegDate = DateTime.Now.Date,
                                DocId = requirements.First().ID
                            };

                            importEdoOdkF401402.edoLog = new EDOLog
                            {
                                DocTable = xdoc.Root.Name.ToString(),
                                DocForm = xdoc.Root.Name.ToString(),
                                Filename = fName,
                                DocRegNumberOut = doc.RegNumberOut,
                                RegDate = DateTime.Now.Date
                            };

                            importEdoOdkF401402.FileName = fName; //  Path.GetFileName(fName);
                            importEdoOdkF401402.PathName = "VIO"; // Path.GetDirectoryName(fileName);
                            importEdoOdkF401402.ReportOnDate =
                                importEdoOdkF401402.F401402.ReportDate?.ToString("dd.MM.yyyy") ?? string.Empty;
                            importEdoOdkF401402.ReportOnDateNative = importEdoOdkF401402.F401402.ReportDate;

                            return importEdoOdkF401402;
                        }
                    }
                }
            }
            catch (InvalidOperationException ex)
            {
                if (ex.InnerException is XmlSchemaValidationException)
                {
                    if (ex.InnerException.InnerException == null)
                    {
                        throw new ReportImportException($"Неверный формат документа: {ex.Message}\n\nПодробности - {ex.InnerException.Message}");
                    }
                    else
                    {
                        throw new ReportImportException($"Неверный формат документа: {ex.Message}\n\nОписание - {ex.InnerException.InnerException.Message}\n\nПодробности - {ex.InnerException.Message}");
                    }
                }
                else
                {
                    Logs.MainLog.Error("Неверный формат документа", ex);
                }
            }
            catch (ReportImportException ex)
            {
                Logs.MainLog.Error("Общая ошибка импорта отчета", ex);
            }
            catch (Exception ex)
            {
                Logs.MainLog.Error("Ошибка открытия документа", ex);
            }
            finally
            {
                Thread.CurrentThread.CurrentUICulture = ciUi;
                Thread.CurrentThread.CurrentCulture = ci;
            }

            return null;
        }

        private void SetStatus(F402Status s, F401402UK detail)
        {
            var status = this.f401402UkStatuses.FirstOrDefault(x => x.ID == (long)s);
            if (status != null)
            {
                detail.StatusID = status.ID;
                detail.StatusName = status.Name;
            }
        }
    }
}
