using Newtonsoft.Json;

namespace PFR_INVEST.Integration.VIO.JSON
{
    /// <summary>
    /// ���� ���������� ��������������� ������
    /// </summary>
    public class PeerBlock
    {
        /// <summary>
        /// ������������ ��� �������� ����������\��������� ���������� ���� ���������� ��������������� ������. 
        /// ������� ��������:
        ///     PERSO
        ///     BPI
        ///     UVKiP
        ///     VIO
        ///     KS
        /// �������� ����� ���������\����������� ��������� � ���������� �.2	
        /// </summary>
        [JsonProperty("identifier")]
        public string Identifier { get; set; }

        /// <summary>
        /// ������������ ��� �������� ��������� ����������
        /// </summary>
        [JsonProperty("peer-data")]
        public object PeerData { get; set; }
    }
}