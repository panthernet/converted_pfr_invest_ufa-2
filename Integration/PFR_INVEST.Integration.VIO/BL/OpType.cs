﻿namespace PFR_INVEST.Integration.VIO.BL
{
    internal enum OpType
    {
        Create,
        Update,
        Delete
    }
}
