﻿using System;
using System.Linq;
using PFR_INVEST.Integration.NSI.BL;

namespace PFR_INVEST.Integration.NSI.MQ
{
	public class MQConnector
	{
		private static MQConnector _instance;
		public static MQConnector Instance
		{
			get
			{
				_instance = _instance ?? new MQConnector();
				return _instance;
			}
		}

		public Core.MQConnector.MQCoreConnector CoreConnector { get; private set; }

		public void SubscribeToMQ(Action<string> action)
		{
		    if (CoreConnector?.IsOperating == true) return;

		    try
		    {
		        Logs.MainLog.Info("Загружаем настройки IBM MQ");
		        var settings = ServerEnvironment.LoadServerConfig();

		        if (!settings.MQConnection.IsDebugEnabled)
		        {
		            var managerName = settings.MQConnection.MQManagerName;
                    var queueName = settings.MQConnection.MQQueueName;
                    var topicName = settings.MQConnection.NotifyTopicName;
		            var topicPrefix = settings.MQConnection.NotifyTopicPrefix;
		            var topicInterval = settings.MQConnection.NotifyTopicInterval;
		            if (topicInterval == 0) topicInterval = 60*1000;
		            var conStr = string.Join("; ", settings.MQConnection.ConnectionSettings.Select(c => $"{c.Key}:{c.Value}").ToArray());
		            Logs.MainLog.InfoFormat($"Подключаемся к IBM MQ '{conStr}'");
		            CoreConnector = new Core.MQConnector.MQCoreConnector(managerName, topicPrefix, settings.MQConnection.GetConnectionSettingsAsHashTable()
		                , message => Logs.MainLog.InfoFormat(message), exception => Logs.MainLog.Error(exception.ToString()));


                    if (settings.MQConnection.IsQueueMode)
		            {
                        Logs.MainLog.InfoFormat($"Подписываемся на обновление очереди {queueName}");
                        CoreConnector.SubscribeToQueue(settings.MQConnection.MQQueueName, action, topicInterval, HandleMQException, settings.MQConnection.IsVerbose);
                    }
		            else
		            {
                        Logs.MainLog.InfoFormat($"Подписываемся на обновления топика {topicName}. Durable name: {topicPrefix}{topicName}");
                        CoreConnector.SubscribeTopic(topicName, action, topicInterval, HandleMQException, settings.MQConnection.IsVerbose);
                    }
                    Logs.MainLog.InfoFormat(" --> УСПЕХ!");
                }
                else
		        {
		            Logs.MainLog.InfoFormat("Запускаем отладочные сообщения!");
		            MessageProcessor.PushDebugData(settings).ForEach(action);
		        }
		    }
		    catch (Exception ex)
		    {
		        Logs.MainLog.Error("Ошибка подключения к IBM MQ", ex);
		    }
		}

	    private void HandleMQException(Exception ex)
		{
			Logs.MainLog.Error("Ошибка во время работы с MQ", ex);
            //Закрываем коннектор, что-бы потом в SubscribeToMQ открыть заново
            CoreConnector.Close();
			CoreConnector = null;
		}
	}
}
