﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Xml;
using System.Collections;

namespace PFR_INVEST.Integration.NSI.BL
{
	[XmlRoot("NSISettings")]
	public class NSISettings
	{
		public class Connection
		{
			public class ConSetting
			{
				[XmlAttribute("Key")]
				public string Key { get; set; }
				[XmlAttribute("Value")]
				public string Value { get; set; }
			}

			public string MQConnectionString { get; set; }

            [XmlElement("IsQueueMode")]
            public bool IsQueueMode { get; set; }

            [XmlElement("MQManagerName")]
			public string MQManagerName { get; set; }

			[XmlElement("MQQueueName")]
            public string MQQueueName { get; set; }

            [XmlElement("IsDebugEnabled")]
            public bool IsDebugEnabled { get; set; }

            [XmlElement("IsGeneralDicAllowed")]
            public bool IsGeneralDicAllowed { get; set; }         

			public string SubscriptionName { get; set; }

			[XmlElement("NotifyTopicName")]
			public string NotifyTopicName { get; set; }

            [XmlElement("NotifyTopicPrefix")]
            public string NotifyTopicPrefix { get; set; }

            [XmlElement("NotifyTopicInterval")]
			public int NotifyTopicInterval { get; set; }


			[XmlArray("ConnectionSettings")]
			[XmlArrayItem("Setting")]
			public List<ConSetting> ConnectionSettings { get; set; }

            [XmlElement("IsVerbose")]
            public bool IsVerbose { get; set; }


		    public Hashtable GetConnectionSettingsAsHashTable()
			{
				var ht = new Hashtable();
			    ConnectionSettings?.ForEach(s => ht.Add(s.Key, s.Value));
			    return ht;
			}
		}

		public class MessageSubTypesSettings
		{
			[XmlElement("Create")]
			public long Create { get; set; }
			[XmlElement("Update")]
			public long Update { get; set; }
			[XmlElement("Enable")]
			public long Enable { get; set; }
			[XmlElement("Disable")]
			public long Disable { get; set; }
		}

		public class Mapping
		{
			[XmlAttribute("NSITerm")]
			public string NSITerm { get; set; }

			[XmlAttribute("DBField")]
			public string DBField { get; set; }

			[XmlAttribute("DBValue")]
            public string DBValue { get; set; }

            [XmlAttribute("SpecialName")]
            public string SpecialName { get; set; }      
		}

		public class Dictionary
		{		
            [XmlAttribute("NSIDictionaryID")]
			public long NSIDictionaryID { get; set; }

			[XmlAttribute("DBTable")]
			public string DBTable { get; set; }

            [XmlAttribute("DBFieldStatus")]
            public string DBFieldStatus { get; set; }

            [XmlAttribute("DBFieldStatusType")]
            public string DBFieldStatusType { get; set; }

            [XmlAttribute("DBFieldStatusEnable")]
            public string DBFieldStatusEnable { get; set; }

            [XmlAttribute("DBFieldStatusDisable")]
            public string DBFieldStatusDisable { get; set; }

            [XmlAttribute("DBCheckForDupes")]
            public bool DBCheckForDupes { get; set; }

			[XmlAttribute("Name")]
			public string Name { get; set; }

			[XmlElement("Mapping")]
			public List<Mapping> Mappings { get; set; }

            [XmlIgnore]
            public long? DBFieldStatusEnableLong => string.IsNullOrEmpty(DBFieldStatusEnable) ? null : (long?)Convert.ToInt64(DBFieldStatusEnable);

		    [XmlIgnore]            
            public long? DBFieldStatusDisableLong => string.IsNullOrEmpty(DBFieldStatusDisable) ? null : (long?)Convert.ToInt64(DBFieldStatusDisable);

		    public Dictionary()
		    {
		        DBCheckForDupes = true;
		    }
		}

	    public class SpecialValue
	    {
            [XmlAttribute("Name")]
            public string Name { get; set; }

            [XmlAttribute("Value")]
            public string Value { get; set; }
	    }


        [XmlElement("MQConnection")]
		public Connection MQConnection { get; set; }

		[XmlElement("MessageSubTypes")]
		public MessageSubTypesSettings MessageSubTypes { get; set; }

        [XmlArray("SpecialValues")]
        [XmlArrayItem("SpecialValue")]
        public List<SpecialValue> SpecialValues { get; set; }

        [XmlElement("Dictionary")]
		public List<Dictionary> Dictionaries { get; set; }

	    [XmlElement("DecodeMessages")]
	    public bool DecodeMessages { get; set; } = false;

        [XmlElement("DecodeMode")]
        public int DecodeMode { get; set; }

        public static NSISettings Load(string fileName)
		{
			var serializer = new XmlSerializer(typeof(NSISettings));
			var settings = new XmlReaderSettings();
			//settings.ValidationType = ValidationType.Schema;
			//settings.Schemas.Add(string.Empty, KIP_Import.GetShema("I_TRANS_MSK_1.0.1.xsd"));
			using (var reader = XmlReader.Create(fileName, settings))
			{
				var res = (NSISettings)serializer.Deserialize(reader);
				return res;
			}
		}

	}
}

