﻿using System;

namespace PFR_INVEST.Integration.NSI.BL
{
    [Serializable]
	public class ActionResult
	{
		public bool IsSuccess { get; set; }
		public string Message { get; set; }
		public Exception Exception { get; set; }

        public string GetExceptionOrMessage()
        {
            return Exception?.ToString() ?? Message;
        }

		public static ActionResult Error(Exception ex)
		{
			return new ActionResult { IsSuccess = false, Message = ex.Message, Exception = ex };
		}

		public static ActionResult Error(string errorMesage = null)
		{
			return new ActionResult { IsSuccess = false, Message = errorMesage };
		}

		public static ActionResult Success(string errorMesage = null)
		{
			return new ActionResult { IsSuccess = true, Message = errorMesage };
		}
	}
}
