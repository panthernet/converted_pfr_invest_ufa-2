﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Integration.NSI.JSON;
using PFR_INVEST.DataAccess.Server;
using NHibernate;
using PFR_INVEST.DataObjects;
using System.Reflection;
using NHibernate.Persister.Entity;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Integration.NSI.BL.DBHelpers;


// ПРОВЕРИТЬ СОЗДАНИЕ И АПДЕЙТ НПФ СРАЗУ по очереди (переименование?)


namespace PFR_INVEST.Integration.NSI.BL
{
	public class DBUpdater
	{
		public class PropertyMapping
		{
			public string DBField { get; set; }
			public PropertyInfo PropertyInfo { get; set; }
		}

		private static readonly Dictionary<Type, List<PropertyMapping>> DBMetaData = new Dictionary<Type, List<PropertyMapping>>();
		public NSISettings.Dictionary Dictionary { get; }
		public IDBHelper DBHelper { get; }

        public bool IsGeneralDicAllowed { get; }

		public DBUpdater(NSISettings.Dictionary dictionary, NSISettings settings, Message msg)
		{
			Dictionary = dictionary;
		    IsGeneralDicAllowed = settings.MQConnection.IsGeneralDicAllowed;
			switch (Dictionary.Name)
			{
				case "NPFUK":
			        var field = settings.SpecialValues.FirstOrDefault(a => a.Name == "NPFUK_Type_Field");
			        if (field == null)
			        {
			            Logs.MainLog.Error("Спец значение NPFUK_Type_Field не задано в настройках! Пропускаем сообщение");
                        return;
			        }
                    var npfId = settings.SpecialValues.FirstOrDefault(a => a.Name == "NPFUK_Type_NPF");
                    var ukId = settings.SpecialValues.FirstOrDefault(a => a.Name == "NPFUK_Type_UK");
			        if (npfId == null || ukId == null)
			        {
                        Logs.MainLog.Error("В настройках отсутствуют определения спец значений NPFUK_Type_NPF или NPFUK_Type_UK! Пропускаем сообщение");
                        return;
                    }
			        long lNpfId;
			        long lUkId;
			        if (!long.TryParse(npfId.Value, out lNpfId) || !long.TryParse(ukId.Value, out lUkId))
			        {
                        Logs.MainLog.Error("Не удалось считать значения NPFUK_Type_NPF или NPFUK_Type_UK! Пропускаем сообщение");
                        return;
                    }

			        if (msg.dictionaryData.records == null || !msg.dictionaryData.records.Any())
			        {
                        Logs.MainLog.Error("В сообщении не найдено записей данных records! Пропускаем сообщение");
                        return;
                    }

                    var value = msg.dictionaryData.records.FirstOrDefault()?.terms.FirstOrDefault(a => a.termName == field.Value);
			        if (value == null)
			        {
                        Logs.MainLog.Error("В сообщении не найдено поле согласно спец значению NPFUK_Type_Field! Пропускаем сообщение");
                        return;
                    }
			        long lValue;
			        if (!long.TryParse(value.term, out lValue))
			        {
                        Logs.MainLog.Error("Не удалось считать значение типа УК/НПФ из сообщения! Пропускаем сообщение");
                        return;
                    }

			        if (lValue == lNpfId)
			        {
			            DBHelper = new LegalEntityDBHelper(ContragentIdentifier.NPF, dictionary.DBTable);
			            Logs.MainLog.Info("Создан класс обработки спец логики: NPF");
			        }else if (lValue == lUkId)
			        {
			            DBHelper = new LegalEntityDBHelper(ContragentIdentifier.UK, dictionary.DBTable);
			            Logs.MainLog.Info("Создан класс обработки спец логики: UK");
			        }
			        else
			        {
                        Logs.MainLog.Error("Тип сообщения не соответствует указанным в настройках значениям УК/НПФ! Пропускаем сообщение");
                    }
                    break;
				default:
			        if (IsGeneralDicAllowed)
			        {
			            //DBHelper = new GeneralDBHelper(Type.GetType(dictionary.DBTypeName), dictionary.DBTable);
			            var type = GetTypeByTableName(dictionary.DBTable, true);
			            if (type == null)
			                throw new NotSupportedException($"Для таблицы {dictionary.DBTable} не найден соответствующий класс маппинга хибернейта!");
			            DBHelper = new GeneralDBHelper(type, dictionary.DBTable, dictionary.DBFieldStatus, dictionary.Name)
			            {
                            DbFieldStatusType = dictionary.DBFieldStatusType,
                            DbFieldStatusEnable = dictionary.DBFieldStatusEnableLong,
                            DbFieldStatusDisable = dictionary.DBFieldStatusDisableLong,
                            DbCheckForDupes = dictionary.DBCheckForDupes
			            };
			            Logs.MainLog.InfoFormat($"Создан униф. класс обработки справочника. Поддержка изменения статуса записи: {(DBHelper.HasStatus ? "Да" : "Нет")}");
			            Logs.MainLog.InfoFormat($"Найден тип данных для справочника: {type.FullName}");
                    }
			        else
			            throw new NotSupportedException($"Неизвестный справочник - '{Dictionary.Name}'");
			        break;
			}
		}


	    private static Type GetTypeByTableName(string name, bool noHib = false)
	    {
	        var item = DataAccessSystem.GetClassMetadataByTable(name, noHib);
            return item == null ? null : GetType(item.EntityName);
	       // return null;
	    }

        private static Type GetType(string typeName)
        {
            var type = Type.GetType(typeName);
            if (type != null) return type;
            foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
            {
                type = a.GetType(typeName);
                if (type != null)
                    return type;
            }
            return null;
        }

		internal List<PropertyMapping> GetMetadata(Type type)
		{
		    if (DBMetaData.ContainsKey(type))
                return DBMetaData[type];
		    var meta = DataAccessSystem.GetClassMetadata(type) as SingleTableEntityPersister;
		    var mapping = meta.PropertyNames.Select(name => new { Name = name, DBField = meta.GetPropertyColumnNames(name).FirstOrDefault() })
		        .Where(m => !string.IsNullOrEmpty(m.DBField)).ToList();
		    var res = mapping.Select(m => new PropertyMapping()
		    {
		        DBField = m.DBField,
		        PropertyInfo = type.GetProperty(m.Name)
		    }).ToList();

		    DBMetaData.Add(type, res);
		    return DBMetaData[type];
		}

	    internal void UpdateFieldsGeneral(ISession session, object item, Type itemType, DictionaryRecord record, OpType opType)
	    {
            var metadata = GetMetadata(itemType);
            UpdateFieldsInternal(session, item, metadata, record, opType);
	    }

	    internal void UpdateFields<T>(T item, DictionaryRecord record, OpType opType) where T : BaseDataObject
	    {
            var metadata = GetMetadata(typeof(T));
            UpdateFieldsInternal(null, item, metadata, record, opType);
	    }


	    public string GetMappingTermForSpecialName(string specialName)
	    {
            var result = Dictionary.Mappings.FirstOrDefault(d => d.SpecialName == specialName);
	        return result?.NSITerm;
	    }

	    private void UpdateFieldsInternal(ISession session, object item, List<PropertyMapping> metadata , DictionaryRecord record, OpType opType)
		{
            //обработка маппинга прямых значений, только при создании
	        if (opType == OpType.Create)
	            Dictionary.Mappings.ForEach(map =>
	            {
	                if (!string.IsNullOrEmpty(map.DBValue))
	                {
	                    var md = metadata.FirstOrDefault(m => m.DBField.Equals(map.DBField, StringComparison.OrdinalIgnoreCase));
	                    SetReflectionValue(md, map.DBValue, item);
	                }
	            });

            //общая обработка маппингов
			foreach (var term in record.terms)
			{
				var dbField = Dictionary.Mappings.FirstOrDefault(d => d.NSITerm == term.termName);
			    if (dbField == null) continue;

                //обработка маппинга спец функций
                if (!string.IsNullOrEmpty(dbField.SpecialName))
			    {
			        SpecialsManager.ProcessSpecial(session, dbField.SpecialName, item, term.term, opType);
                    continue;
			    }

                if (string.IsNullOrEmpty(dbField.DBField))
					continue;
                //не выбираем хибовские расширения, из-за проблем с сохранением таких сущностей
				var md = metadata.FirstOrDefault(m => m.DBField.Equals(dbField.DBField, StringComparison.OrdinalIgnoreCase));
				if (md == null || !md.PropertyInfo.CanWrite)
				{
				    Logs.MainLog.ErrorFormat($"Некоректный мапинг для поля '{term.termName}' -> '{dbField.DBField}'. Поле пропущено.");
					continue;
				}

                SetReflectionValue(md, term.term, item, dbField.DBField);
			}
		}

	    internal void SetReflectionValue(PropertyMapping md, string value, object item, string field = null)
	    {
            //Конверируем в нужный тип и присваиваем значение
            var pi = md.PropertyInfo;
            if (pi.PropertyType == typeof(string))
            {
                var val = value;
                pi.SetValue(item, val, null);
            }
            else if (pi.PropertyType == typeof(decimal) || pi.PropertyType == typeof(decimal?))
            {
                var val = decimal.Parse(value);
                pi.SetValue(item, val, null);
            }
            else if (pi.PropertyType == typeof(int) || pi.PropertyType == typeof(int?))
            {
                var val = int.Parse(value);
                pi.SetValue(item, val, null);
            }
            else if (pi.PropertyType == typeof(long) || pi.PropertyType == typeof(long?))
            {
                var val = long.Parse(value);
                pi.SetValue(item, val, null);
            }
            else if (pi.PropertyType == typeof(DateTime) || pi.PropertyType == typeof(DateTime?))
            {
                var val = DateTime.Parse(value);
                pi.SetValue(item, val, null);
            }
            else
            {
                Logs.MainLog.ErrorFormat($"Неподдерживаимый тип поля '{null}' -> '{field}' '{pi.PropertyType}'. Поле пропущено.");
            }
        }


		private NSILinkMap GetMap(ISession session, DictionaryRecord record)
		{
			var map = session.CreateQuery("select map from NSILinkMap map where map.Table = :table and map.Dictionary = :dict and map.NSIDictID = :nsiDict and map.NSIID = :nsiID")
							.SetParameter("table", DBHelper.DBTable)
							.SetParameter("dict", DBHelper.Dictionary)
							.SetParameter("nsiDict", Dictionary.NSIDictionaryID)
							.SetParameter("nsiID", Convert.ToString(record.number))
							.SetMaxResults(1).List<NSILinkMap>().FirstOrDefault();

			return map;
		}

		private NSILinkMap CreateMap(ISession session, DictionaryRecord record, long itemID)
		{
			var map = new NSILinkMap
			{
				Table = DBHelper.DBTable,
				Dictionary = DBHelper.Dictionary,
				NSIDictID = Dictionary.NSIDictionaryID,
				NSIID = record.number.ToString(),
				TableID = itemID
			};

			session.SaveOrUpdate(map);
			session.Flush();

			return map;
		}

		public ActionResult Create(DictionaryRecord record)
		{
			using (var session = DataAccessSystem.OpenSession())
			{
				try
				{
					using (var transaction = session.BeginTransaction())
					{
						var map = GetMap(session, record);
						if (map == null)
						{
						    Logs.MainLog.InfoFormat($"Создаём новую запись nsi_id={record.number}");
							var id = DBHelper.Create(session, record, this);
							CreateMap(session, record, id);
						}
						else
						{
						    Logs.MainLog.InfoFormat($"Создание записи с nsi_id={record.number ?? 0} будет пропущено, т.к. запись уже есть в базе.");
							//this.DBHelper.Update(session, map.TableID, record, this);
						}

						transaction.Commit();
						return ActionResult.Success();
					}
				}
				catch (Exception ex)
				{
					return ActionResult.Error(ex);
				}
			}

		}

		public ActionResult Update(DictionaryRecord record)
		{
			using (var session = DataAccessSystem.OpenSession())
			{
				try
				{
					using (var transaction = session.BeginTransaction())
					{
						var map = GetMap(session, record);
						if (map == null)
						{
							return ActionResult.Error($"Записи с номером '{record.number}' нет в базе.");
						}

						DBHelper.Update(session, map.TableID, record, this);
						transaction.Commit();
						Logs.MainLog.Info("Запись обновлена");
						return ActionResult.Success();
					}
				}
				catch (Exception ex)
				{
					return ActionResult.Error(ex);
				}
			}
		}

		public ActionResult Enable(DictionaryRecord record)
		{
			using (var session = DataAccessSystem.OpenSession())
			{
				try
				{
					using (var transaction = session.BeginTransaction())
					{
						var map = GetMap(session, record);
						if (map == null)
						{
							return ActionResult.Error($"Записи с номером '{record.number}' нет в базе.");
						}
						if(!DBHelper.Enable(session, map.TableID))
                            throw new Exception("Попытка включения записи справочника для которого не найдено поле статуса!");

						transaction.Commit();
						Logs.MainLog.Info("Запись активирована в базе");
						return ActionResult.Success();
					}
				}
				catch (Exception ex)
				{
					return ActionResult.Error(ex);
				}
			}
		}

		public ActionResult Disable(DictionaryRecord record)
		{
			using (var session = DataAccessSystem.OpenSession())
			{
				try
				{
					using (var transaction = session.BeginTransaction())
					{

						var map = GetMap(session, record);
						if (map == null)
						{
							return ActionResult.Error($"Записи с номером '{record.number}' нет в базе.");
						}
						if(!DBHelper.Disable(session, map.TableID))
                            throw new Exception("Попытка выключения записи справочника для которого не найдено поле статуса!");

						transaction.Commit();
						Logs.MainLog.Info("Запись отключена в базе");
						return ActionResult.Success();
					}
				}
				catch (Exception ex)
				{
					return ActionResult.Error(ex);
				}
			}
		}

		private ActionResult ValidateProcedureName(string procedureName)
		{
			return new ActionResult();
		}

		/// <summary>
		/// Обновляем значения в базе для конкретной записи
		/// </summary>	
		private void UpdateItem(ISession session, long itemID, DictionaryRecord record)
		{

		}

		private long FindLinkedDBItem(ISession session, DictionaryRecord record)
		{
			var nsiID = record.number;
			var nsiDict = Dictionary.NSIDictionaryID;
			var table = Dictionary.DBTable;
			return 0;
		}

	}
}
