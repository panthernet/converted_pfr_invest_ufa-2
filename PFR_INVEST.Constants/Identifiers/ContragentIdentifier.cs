﻿namespace PFR_INVEST.Constants.Identifiers
{
    public static class ContragentIdentifier
    {
        public static string NPF = "НПФ";
        public static string UK = "УК";

        public static bool IsContragentDI(string contracgentName)
        {
            return contracgentName.ToLower().Trim() == "департамент инвестирования";
        }
    }
}
