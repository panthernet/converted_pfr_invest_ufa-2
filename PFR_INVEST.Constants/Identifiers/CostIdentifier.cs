﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PFR_INVEST.Constants.Identifiers
{
    public struct CostIdentifier
    {
        public const string s_CBFee = "Вознаграждение за сделки по ЦБ";
        public const string s_TradingCommission = "Комиссионное вознаграждение по торгам";
        public const string s_DepositServicesCommission = "Комиссионное вознаграждение за депозитарные услуги";

        public const string s_CurrencyBuySellCommission = "Комиссия за покупку/продажу иностранной валюты";
		public const string s_CurrencyBuySellCommission_ForSaldoBugfix = "Комиссия за покупку";
        public const string s_DirectDebiting = "Безакцептное списание";

        public static List<string> GetAll()
        {
            throw new NotImplementedException();
        }

        public static List<string> GetList()
        {
            return new List<string>()
            { 
                CostIdentifier.s_CBFee,
                CostIdentifier.s_TradingCommission,
                CostIdentifier.s_DepositServicesCommission,
                CostIdentifier.s_CurrencyBuySellCommission,
                CostIdentifier.s_DirectDebiting
            };
        }
    }

    public struct IncomeSecIdentifier
    {
        public const string s_CouponYield = "Купонный доход";
        public const string s_SecurityRedemption = "Погашение ЦБ";


        public const string s_DepositsPercents = "Проценты по депозитам";//Перенести в BalanceIdentifier ?
    }
}
