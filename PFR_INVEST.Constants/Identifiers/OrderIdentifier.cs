﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PFR_INVEST.Constants.Identifiers
{
    public class OrderTypeIdentifier
    {
        public const string BUYING = "покупка";
        public const string SELLING = "продажа";
        public const string RELAYING = "перекладка";
        public const string SHORT_BUYING = "К";
        public const string SHORT_SELLING = "П";

        public static bool IsBuyOrder(string p_sType)
        {
            return !string.IsNullOrEmpty(p_sType) && p_sType.Trim().ToLower().Equals(BUYING);
        }
        public static bool IsSellOrder(string p_sType)
        {
            return !string.IsNullOrEmpty(p_sType) && p_sType.Trim().ToLower().Equals(SELLING);
        }

        public static string GetOrderShortType(string oType)
        {
            return (OrderTypeIdentifier.IsBuyOrder(oType)) ? SHORT_BUYING : SHORT_SELLING;
        }
    }

    public class OrderStatusIdentifier
    {
        public const string ON_SIGNING = "на подписи";
        public const string ON_EXECUTION = "на исполнении";
        public const string EXECUTED = "исполнено";
        public const string NOT_EXECUTED = "не исполнено";
        public const string PARTIALLY_EXECUTED = "частично исполнено";

        public static bool IsStatusOnSigning(string p_Status)
        {
            if (p_Status == null)
                return false;

            return p_Status.Trim().ToLower() == ON_SIGNING;
        }

        public static bool IsStatusOnExecution(string p_Status)
        {
            if (p_Status == null)
                return false;

            return p_Status.Trim().ToLower() == ON_EXECUTION;
        }

        public static bool IsStatusExecuted(string p_Status)
        {
            if (p_Status == null)
                return false;

            return p_Status.Trim().ToLower() == EXECUTED;
        }

        public static bool IsStatusPartiallyExecuted(string p_Status)
        {
            if (p_Status == null)
                return false;

            return p_Status.Trim().ToLower() == PARTIALLY_EXECUTED;
        }

        public static List<string> GetAvailableStatuses(string current_status)
        {
            var result = new List<string>();

            if (current_status == null)
                current_status = string.Empty;

            switch (current_status.Trim().ToLower())
            {
                case "":
                    result.Add(ON_SIGNING);
                    break;
                case ON_SIGNING:
                    result.Add(ON_SIGNING);
                    result.Add(ON_EXECUTION);
                    break;
                case ON_EXECUTION:
                    result.Add(ON_EXECUTION);
                    result.Add(EXECUTED);
                    result.Add(NOT_EXECUTED);
                    result.Add(PARTIALLY_EXECUTED);
                    break;
                default:
                    result.Add(current_status);
                    break;
            }

            return result;
        }
    }

    public class OrderPlacesIdentifier
    {
        public const string SECOND_MARKET = "вторичный рынок";
        public const string AUCTION = "аукцион";

        public static bool IsAuction(string placeName)
        {
            return placeName != null && placeName.Trim().ToLower() == AUCTION;
        }

        public static bool IsSecondMarket(string placeName)
        {
            return placeName != null && placeName.Trim().ToLower() == SECOND_MARKET;
        }
    }
}
