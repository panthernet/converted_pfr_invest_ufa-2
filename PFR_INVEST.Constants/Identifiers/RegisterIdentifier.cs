﻿using System;
using System.Collections.Generic;

namespace PFR_INVEST.Constants.Identifiers
{
	public static class RegisterIdentifier
	{
		public const string PFRtoNPF = "Передача из ПФР в НПФ";
		public const string NPFtoPFR = "Возврат из НПФ в ПФР";
		public const string TempAllocation = "Передача СПН во временное размещение";
		public const string TempReturn = "Изъятие СПН из временного размещения";

		public const string TempContent = "Перевод во временное размещение";

		public static class FinregisterStatuses
		{
			/// <summary>
			/// "финреестр создан"
			/// </summary>
			public const string Created = "финреестр создан";
			/// <summary>
			/// "финреестр оформлен"
			/// </summary>
			public const string Issued = "финреестр оформлен";
			/// <summary>
			/// "финреестр передан"
			/// </summary>
			public const string Transferred = "финреестр передан";
			/// <summary>
			/// "финреестр на уточнении"
			/// </summary>
			public const string Refine = "финреестр на уточнении";
			/// <summary>
			/// "финреестр не передан"
			/// </summary>
			public const string NotTransferred = "финреестр не передан";

			public static bool IsCreated(string value)
			{
				return value != null && value.Trim().Equals(Created, StringComparison.OrdinalIgnoreCase);
			}

			public static bool IsIssued(string value)
			{
				return value != null && value.Trim().Equals(Issued, StringComparison.OrdinalIgnoreCase);
			}

			public static bool IsTransferred(string value)
			{
				return value != null && value.Trim().Equals(Transferred, StringComparison.OrdinalIgnoreCase);
			}

			public static bool IsRrefine(string value)
			{
				return value != null && value.Trim().Equals(Refine, StringComparison.OrdinalIgnoreCase);
			}

			public static bool IsNotTransferred(string value)
			{
				return value != null && value.Trim().Equals(NotTransferred, StringComparison.OrdinalIgnoreCase);
			}

			public static List<string> ToList
			{
				get
				{
					return new List<string> { Created, Issued, Transferred, Refine, NotTransferred };
				}
			}
		}

		public static class FinregisterMoveTypes
		{

			public const string LETTER = "Почтовое отправление";
			public const string COURIER = "Курьер";
			public const string COURIER_SERVICE = "Фельдъегерская служба";
			public const string EDO = "ЭДО";

			public static List<string> ToList
			{
				get
				{
					return new List<string> { LETTER, COURIER, COURIER_SERVICE, EDO };
				}
			}
		}

		public static bool IsToNPF(string value)
		{
			return value != null && value.Trim().Equals(PFRtoNPF, StringComparison.OrdinalIgnoreCase);			
		}

		public static bool IsFromNPF(string value)
		{
			return value != null && value.Trim().Equals(NPFtoPFR, StringComparison.OrdinalIgnoreCase);
		}

		public static bool IsTempAllocation(string value)
		{
			return value != null && value.Trim().Equals(TempAllocation, StringComparison.OrdinalIgnoreCase);
		}

		public static bool IsTempReturn(string value)
		{
			return value != null && value.Trim().Equals(TempReturn, StringComparison.OrdinalIgnoreCase);
		}

		public static class Operation
		{
			/// <summary>
			/// "Материнский семейный капитал (256-ФЗ)"
			/// </summary>
			[Obsolete("Use ELEMENT table")]
			public const string MotherCapital = "Материнский семейный капитал (256-ФЗ)";

			/// <summary>
			/// "Отказ ЗЛ по МСК (256-ФЗ)"
			/// </summary>
			[Obsolete("Use ELEMENT table")]
			public const string MotherCapitalRecal = "Отказ ЗЛ по МСК (256-ФЗ)";



			[Obsolete("Use ELEMENT table", true)]
			public const string InsuranceBid = "Страховые взносы (75-ФЗ)";
			[Obsolete("Use ELEMENT table", true)]
			public const string DSVQuartal = "ДСВ ежеквартально (56-ФЗ)";
			[Obsolete("Use ELEMENT table", true)]
			public const string CoFinance = "Софинансирование (56-ФЗ)";
			[Obsolete("Use ELEMENT table", true)]
			public const string ZLTransferMonthly = "Переход ЗЛ ежемесячно (56-ФЗ)";

			[Obsolete("Use ELEMENT table", true)]
			public const string ZLTransferYearly = "Переход ЗЛ ежегодно (75-ФЗ)";

			[Obsolete("Use ELEMENT table", true)]
			public static bool IsInsuranceBid(string value)
			{
				throw new NotImplementedException();
				//return value != null && value.Trim().Equals(InsuranceBid, StringComparison.OrdinalIgnoreCase);
			}

			[Obsolete("Use ELEMENT table", true)]
			public static bool IsInsuranceDSP(string value)
			{
				throw new NotImplementedException();
				//return value != null && value.Trim().Equals(DSVQuartal, StringComparison.OrdinalIgnoreCase);
			}

		}

	}
}
